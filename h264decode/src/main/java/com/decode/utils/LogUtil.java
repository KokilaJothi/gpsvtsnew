package com.decode.utils;

import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by Administrator on 2017/6/16.
 */

public class LogUtil
{
    private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private static final SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
    private static final String PACKAGE = "com.bluecam";
    public final static String LOG_PATH = Environment.getExternalStorageDirectory().getAbsolutePath() + "/"+PACKAGE+"/Log/";

    private static final boolean SYSOUT = true;
    private static final boolean WRITELOG = false;
    private static final boolean LOGE = false;
    static
    {
        File logDir = new File(LOG_PATH);
        if(!logDir.exists())
            logDir.mkdirs();
    }

    public static void printLog(String message)
    {
        if(SYSOUT)
        {
            System.out.println(message);
        }
        if(WRITELOG)
        {
            wirteLog2Disk(message);
        }
        if(LOGE)
        {
            log_E(message);
        }
    }
    public static void log_E(String message)
    {
        Log.e("E_CAMERA", message);
    }
    public static void log_I(String message)
    {
        Log.i("I_CAMERA", message);
    }

    public static  void wirteLog2Disk(String message)
    {
        if(FileUtil.isSDCardMounted())
        {
            FileWriter writer = null;
            try {

                File file = new File(LOG_PATH, sdf1.format(Calendar.getInstance().getTime()) + "-log.txt");
                if (!file.exists())
                    file.createNewFile();

                writer = new FileWriter(file, true);
                writer.append(sdf.format(Calendar.getInstance().getTime()) + ":"+ message + "\r\n");
                writer.flush();
            }catch (IOException e) {
                e.printStackTrace();
            }
            finally
            {
                if(writer != null)
                {
                    try
                    {
                        writer.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    writer = null;
                }
            }
        }
    }
}
