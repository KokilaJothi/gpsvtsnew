package com.bluecam.libs;

public class Mp4v2Helper {

    static {
        System.loadLibrary("mp4v2");
        System.loadLibrary("Mp4v2Helper");
    }

    public static native int initMp4Encoder(String outputFilePath, int width, int height);

    public static native int checkMp4();

    public static native int findMp4VideoTrack();

    public static native int mp4VEncode(byte[] data, int size);

    public static native int mp4AEncode(byte[] data, int size);

    public static native void closeMp4Encoder();

}
