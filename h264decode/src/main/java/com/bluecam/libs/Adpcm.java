package com.bluecam.libs;

public class Adpcm {
    static{
        System.loadLibrary("Adpcm");
    }

    private static class AdpcmHodler {
        static Adpcm instance = new Adpcm();
    }

    public static Adpcm getInstance() {
        return Adpcm.AdpcmHodler.instance;
    }

    public static native  int adpcm_coder(byte[]indata,int len,byte[]outdata);
    public static native  int adpcm_decoder(byte[]indata,int len);

    public static native  int adpcm_readdata(byte[]outdata,int len);

    public static native  int h264_decodesps(byte[]indata,int len,int []width,int []height,int []fps);
}
