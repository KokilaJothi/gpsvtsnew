package com.vamosys.services;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import androidx.annotation.NonNull;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.vamosys.notification.NotificationUtils;
import com.vamosys.utils.Constant;
import com.vamosys.utils.DaoHandler;
import com.vamosys.utils.UnCaughtException;
import com.vamosys.vamos.DBHelper;
import com.vamosys.vamos.NotificationListActivity;
import com.vamosys.vamos.R;

import java.util.Map;

public class MyFirebaseMessagingService extends FirebaseMessagingService {
    private static final String TAG = "GCM";

    /**
     * Called when message is received.
     *
     * @param from SenderID of the sender.
     * @param data Data bundle containing message data as key/value pairs.
     * For Set of keys use data.keySet().
     */

    DBHelper dbhelper;
    SharedPreferences sp;

    String parkingdata;
    String safetymessage,fuelmsg,fueldropmsg;
    String safemsg = "SAFETY PARKING:";
    String fuelfilledmsg = "Fuel Filled Alarm";
    String fueldroppedmsg = "Fuel Dropped Alarm";
    String fuelfilldata;
    String fueldroppeddata;
    String message;


    private NotificationUtils notificationUtils;
    Map<String, String> data;
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);

        Thread.setDefaultUncaughtExceptionHandler(new UnCaughtException(
                getApplicationContext()));



        if (remoteMessage.getData().size() > 0) {
            Log.d(TAG, "Message data payload: " + remoteMessage.getData());

            data = remoteMessage.getData();
            String title = data.get("title");
            message = data.get("body").toString();
            // message = remoteMessage.getNotification().getBody();
            // checkPushList();
            sp = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
            Log.d("messagedata", "" + data.get("body").toString());
            Log.d("notificationtitle", "" + data.get("title"));
            parkingdata = message.substring(0, 15);
            fuelfilldata = message.substring(0, 17);
            fueldroppeddata = message.substring(0, 18);
            Log.d("parkingdata", "" + parkingdata);
            Log.d("fueldatadata", "" + fuelfilldata);
            Log.d("fueldroppeddata", "" + fueldroppeddata);
        }

        if (remoteMessage.getNotification() != null) {
            Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());

        }








        boolean isLoggedIn = sp.getBoolean("is_logged_in", false);
        Log.d("fcmisLoggedIn",""+isLoggedIn);
        if (isLoggedIn) {

            try {
                DaoHandler da = new DaoHandler(getApplicationContext(), true);
                da.insertPushMsg(message);

                Log.d("fueldata1", "" + fuelfilldata);
                Log.d("fueldata2", "" + fuelfilledmsg);

                if (message.contains(fuelfilledmsg)) {
                    fuelmsg = data.get("message");
                    Log.d("fuelfillmessage", "" + fuelmsg);
                    String msg = message;
                    // String part1 = msg[1].split("is")[0];

                    String lastWord = msg.substring(msg.lastIndexOf(" ") + 1);
                    Log.d("fuelvehicle", "" + lastWord);
                    Intent intent = new Intent(getApplicationContext(), FuelFillBroadcast.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra("vehiclename", lastWord);
                    PendingIntent pendingIntent = PendingIntent.getBroadcast(
                            getApplicationContext(), 234324243, intent, 0);
                    AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
                    alarmManager.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis()
                            + (1000), pendingIntent);
                }
                if (message.contains(fueldroppedmsg)) {
                    fueldropmsg = data.get("message");
                    Log.d("fueldropmessage", "" + fueldropmsg);
//                String[] msg = message.split("vehicle");
//                String part1 = msg[1].split("is")[0];
                    String msg = message;
                    String lastWord = msg.substring(msg.lastIndexOf(" ") + 1);
                    Log.d("fueldroppedvehicle", "" + lastWord);
                    Intent intent = new Intent(getApplicationContext(), FuelDropBroadcast.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra("vehiclename", lastWord);
                    PendingIntent pendingIntent = PendingIntent.getBroadcast(
                            getApplicationContext(), 234324243, intent, 0);
                    AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
                    alarmManager.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis()
                            + (1000), pendingIntent);
                }
                if (parkingdata.equalsIgnoreCase(safemsg)) {
                    safetymessage = data.get("message");
                    Log.d("safetymessage", "" + safetymessage);
                    String[] msg = message.split("vehicle");
                    String part1 = msg[1].split("is")[0];
                    Intent intent = new Intent(getApplicationContext(), MyBroadcastReceiver.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra("vehiclename", part1);
                    intent.putExtra("safetyname", "safetyparking");
                    PendingIntent pendingIntent = PendingIntent.getBroadcast(
                            getApplicationContext(), 234324243, intent, 0);
                    AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
                    alarmManager.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis()
                            + (1000), pendingIntent);
                    //startForeground(1000,notificationUtils);
                }

                Intent intent = new Intent(getApplicationContext(), NotificationListActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), 0 /* Request code */, intent,
                        PendingIntent.FLAG_ONE_SHOT);
                showNotificationMessage(getApplicationContext(), getApplicationContext().getResources().getString(R.string.app_name), message, intent);
            } catch (Exception e) {
                e.printStackTrace();
            }



        }


    }

    @Override
    public void onNewToken(@NonNull String token) {
        super.onNewToken(token);
        Log.d("firebasenew",""+token);
        String token1 = FirebaseInstanceId.getInstance().getToken();
        Log.d("firebasenew1",""+token1);
        saveGcmToken(getApplicationContext(), token);
    }
    private void saveGcmToken(Context mContext, String token) {
        Log.d("gsmfirebase","savetoken");
        try {
            Constant.gcm_count++;
            if (token != null) {
                if (token.length() > 0 && !token.equalsIgnoreCase("SERVICE_NOT_AVAILABLE")) {
                    SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(mContext).edit();
                    editor.putString("gcmToken", token);
                    editor.commit();

                    if (Constant.is_logged_in) {
                        Log.d("gsmfirebase","success");
                        // sendGcmData(token);
                    }

                } else {
                    Log.d("gsmfirebase","failure");


                    if (Constant.gcm_count < 6) {
                        //  onTokenRefresh();
                    } else {

                    }


                }
            } else {

                Log.d("gsmfirebase","failure1");
                if (Constant.gcm_count < 6) {
                    // onTokenRefresh();
                } else {


                }
            }

        } catch (Exception e) {
            e.printStackTrace();
            Log.d("exceptionhand",""+e);
        } finally {

        }
    }


    private void showNotificationMessage(Context context, String title, String message, Intent intent) {
        notificationUtils = new NotificationUtils(context);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        notificationUtils.showNotificationMessage(title, message, intent);
    }
}
