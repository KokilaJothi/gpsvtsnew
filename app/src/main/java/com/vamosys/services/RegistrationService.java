//package com.vamosys.services;
//
//import android.app.IntentService;
//import android.content.Context;
//import android.content.Intent;
//import android.content.SharedPreferences;
//import android.preference.PreferenceManager;
//import android.util.Log;
//
//import com.google.android.gms.gcm.GcmPubSub;
//import com.google.android.gms.gcm.GoogleCloudMessaging;
//import com.google.android.gms.iid.InstanceID;
//import com.vamosys.utils.ConnectionDetector;
//import com.vamosys.utils.Constant;
//import com.vamosys.utils.HttpConfig;
//import com.vamosys.vamos.Const;
//import com.vamosys.vamos.R;
//
///**
// * Created by Smart Team.
// */
//public class RegistrationService extends IntentService {
//    private static final String TAG = "GCM";
//    private static final String[] TOPICS = {"global"};
//    ConnectionDetector cd;
//
//    String mGcmToken;
//
////    SharedPreferences pref = null;
//
//    public RegistrationService() {
//        super(TAG);
//    }
//
//    @Override
//    protected void onHandleIntent(Intent intent) {
//
////        try {
////            synchronized (TAG) {
////                InstanceID instanceID = InstanceID.getInstance(this);
////                String token = instanceID.getToken(getApplicationContext().getResources().getString(R.string.gcm_sender_id), GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);
////                Log.d(TAG, "onHandleIntent() token = " + token);
////                for (String topic : TOPICS) {
////                    GcmPubSub pubSub = GcmPubSub.getInstance(this);
////                    pubSub.subscribe(token, "/topics/" + topic, null);
////                }
////                saveGcmToken(getApplicationContext(), token);
////            }
////        } catch (Exception e) {
////            e.printStackTrace();
////        }
//        cd = new ConnectionDetector(getApplicationContext());
//        registerGcm();
//
//    }
//
//
//    public void registerGcm() {
//        try {
//            synchronized (TAG) {
//                InstanceID instanceID = InstanceID.getInstance(this);
//                String token = instanceID.getToken(getApplicationContext().getResources().getString(R.string.gcm_sender_id), GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);
//                Log.d("token",""+token);
//                Log.d(TAG, "onHandleIntent() token = " + token);
//                for (String topic : TOPICS) {
//                    GcmPubSub pubSub = GcmPubSub.getInstance(this);
//                    pubSub.subscribe(token, "/topics/" + topic, null);
//                }
//                //token = "";
//                saveGcmToken(getApplicationContext(), token);
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//
//
//    /**
//     * Store the received GCM token in shared preference.
//     *
//     * @param mContext application context.
//     * @param token    The new token.
//     */
//    private void saveGcmToken(Context mContext, String token) {
//        try {
//            Constant.gcm_count++;
//            if (token != null) {
//                if (token.length() > 0 && !token.equalsIgnoreCase("SERVICE_NOT_AVAILABLE")) {
//                    SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(mContext).edit();
//                    editor.putString("gcmToken", token);
//                    editor.commit();
//
//                    if (Constant.is_logged_in) {
//                        sendGcmData(token);
//                    }
//
//                } else {
//
//
//                    if (Constant.gcm_count < 6) {
//                        registerGcm();
//                    } else {
//                        Log.d("stopservice","success1");
////                        System.out.println("Count over 1111111111::::::::::::::");
//                        stopService(new Intent(RegistrationService.this, RegistrationService.class));
//                    }
//
//
//                }
//            } else {
//                if (Constant.gcm_count < 6) {
//                    registerGcm();
//                } else {
//                    Log.d("stopservice","success2");
////                    System.out.println("Count over 222222222::::::::::::::");
//                    stopService(new Intent(RegistrationService.this, RegistrationService.class));
//                }
//            }
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        } finally {
//            Log.d("stopservice","success");
//            stopService(new Intent(RegistrationService.this, RegistrationService.class));
//        }
//    }
//
//    public String sendGcmData(String token) {
//
//        mGcmToken = token;
//        String res = "";
//        try {
//
//            if (cd.isConnectingToInternet()) {
//                HttpConfig config = new HttpConfig();
//                res = config.httpGet(Const.API_URL + "mobile/pushGcm?userId=" + Constant.SELECTED_EMP_ID + "&password=" + Constant.SELECTED_EMP_PWD + "&gcmId="
//                        + mGcmToken + "&notifyEnable=" + Constant.SELECTED_EMP_NOTI_ENABLE_STATUS + "&type=android");
//                // res = config.doPostEcpl(data, WebConfig.GET_TM_PATCH_LIST);
//
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//
//        }
//
//        if (res != null) {
//
//            if (res.equalsIgnoreCase("Success")) {
//
//            } else {
//                sendGcmData(mGcmToken);
//            }
//
//        }else {
//            sendGcmData(mGcmToken);
//        }
//
//        //
//        return res;
//    }
//
//
//}

