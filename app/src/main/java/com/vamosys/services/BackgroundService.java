package com.vamosys.services;

import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.IBinder;
import android.preference.PreferenceManager;
import androidx.annotation.Nullable;
import android.util.Log;
import android.widget.Toast;

import com.vamosys.utils.ConnectionDetector;
import com.vamosys.vamos.Const;
import com.vamosys.vamos.DBHelper;
import com.vamosys.vamos.RegistrationPage;

import org.json.JSONObject;

import java.util.Timer;
import java.util.TimerTask;

import static android.content.ContentValues.TAG;

/**
 * Created by vamo on 13/04/18.
 */

public class BackgroundService extends Service {
    long timeInterval = 10;
    SharedPreferences sp;
    Timer timer;
    ConnectionDetector cd;
    String strUserName, passsword;
    DBHelper dbhelper;
    int registration_timeout = 5000;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        cd = new ConnectionDetector(getApplicationContext());
        sp = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        strUserName = sp.getString("user_name","");
        passsword = sp.getString("password","");
        Log.d("UserName", "" +strUserName);
        Log.d("passssword", "" +passsword);
        dbSetup();
        starTimer();
    }


    private void starTimer() {
        timer = new Timer();
        TimerTask hourlyTask = new TimerTask() {
            @Override
            public void run() {
                Log.d("starttimer","starttimer");
               new SendUserCredentials().execute();
            }
        };
        timer.schedule(hourlyTask,0l,1000*10*60);
    }
//    private void starTimer() {
//        if (timer !=null){
//            timer.cancel();
//        }
//        timer = new Timer();
//        timer.schedule(new TimerTask() {
//            @Override
//            public void run() {
//             Log.d("starttimer","starttimer");
//                new SendUserCredentials().execute(Constant.SELECTED_EMP_ID, Constant.SELECTED_EMP_PWD);
//            }
//        },0l, 1000*60*60);
//    }

    private class SendUserCredentials extends AsyncTask<String,String,String>{
        String username = null;
        String password = null;
        @Override
        protected String doInBackground(String... String) {
            String response_from_server = null;
//            username = params[0];
//            password = params[1];
            String urlstr = Const.API_URL + "mobile/verifyUser?userId=" + strUserName + "&password=" + passsword + "&appid=" + getStdTableValue("appid");
            Log.d(TAG, "doInBackground() called with: " + "params = [" + urlstr + "]");
            Const constaccess = new Const();
            try {
                response_from_server = constaccess.sendGet(urlstr, registration_timeout);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return response_from_server;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            try {
                if (result !=null && !result.isEmpty()){
                    JSONObject jsonObject = new JSONObject(result);
                    if (jsonObject.has("authUser")){
                       if (jsonObject.getString("authUser").toString().equalsIgnoreCase("success")){
                           Log.d("success","success");

                       } else if (jsonObject.getString("authUser").toString().equalsIgnoreCase("failure")){
                           Log.d("failure","failure");
                           Intent intent = new Intent().setClass(BackgroundService.this,RegistrationPage.class);
                           intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                           startActivity(intent);
                       } else{
                           Log.d("nologin","nologin");
                       }
                    }

                }

            } catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    public String getStdTableValue(String tag) {
        Cursor std_table_cur = null;
        String std_table_value = null;
        try {
            std_table_cur = dbhelper.get_std_table_info(tag);
            std_table_cur.moveToFirst();
            std_table_value = std_table_cur.getString(0);
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), "Something went wrong. Please try again", Toast.LENGTH_SHORT).show();
        } finally {
            if (std_table_cur != null) {
                std_table_cur.close();
            }
        }
        return std_table_value;
    }

    void dbSetup() {
        dbhelper = new DBHelper(getApplicationContext());
        try {
            dbhelper.createDataBase();
            dbhelper.openDataBase();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
