package com.vamosys.services;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.preference.PreferenceManager;
import android.util.Log;

import com.vamosys.utils.HttpConfig;

public class NetworkReceiver extends BroadcastReceiver {
    SharedPreferences sp;

    @Override
    public void onReceive(Context context, Intent intent) {
        sp = PreferenceManager.getDefaultSharedPreferences(context);
        if (intent.getAction().equals(ConnectivityManager.CONNECTIVITY_ACTION)) {
            NetworkInfo networkInfo = intent.getParcelableExtra(ConnectivityManager.EXTRA_NETWORK_INFO);
            if (networkInfo != null && networkInfo.getDetailedState() == NetworkInfo.DetailedState.CONNECTED) {
                Log.d("Network", "Internet YAY");
//                sendLogoutStatus();
            } else if (networkInfo != null && networkInfo.getDetailedState() == NetworkInfo.DetailedState.DISCONNECTED) {
                Log.d("Network", "No internet :(");
            }
        }
    }


    private void sendLogoutStatus() {
        if (!sp.getBoolean("is_logout_posted", false)) {
            if (sp.getString("logout_data", "") != null) {
                if (sp.getString("logout_data", "").trim().length() > 0) {
                    String mLogoutData = sp.getString("logout_data", "");
                    HttpConfig ht = new HttpConfig();

                    try {
                        if (ht.httpGet("").trim().equalsIgnoreCase("Success")) {
//update shared pref
                            updateLogoutStatus();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            }
        }
    }

    private void updateLogoutStatus() {
        SharedPreferences.Editor editor = sp.edit();
        editor.putBoolean("is_logout_posted", true);
        editor.commit();
    }
}