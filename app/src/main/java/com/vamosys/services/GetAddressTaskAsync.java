package com.vamosys.services;


import android.os.AsyncTask;

import com.vamosys.utils.Constant;

public class GetAddressTaskAsync extends AsyncTask<String, String, String> {
//    private final Context mContext;

//    public UploadAddressTask(Context context) {
//        mContext = context;
//    }

    @Override
    protected String doInBackground(String... params) {


        try {
            Constant.getAddressForServer(String.valueOf(params[0]), String.valueOf(params[1]));
        } catch (Exception e1) {
            e1.printStackTrace();
        }

        return null;
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
    }
}
