package com.vamosys.services;

import android.util.Log;

import com.vamosys.utils.Constant;
import com.vamosys.utils.UnCaughtException;
import com.vamosys.vamos.Const;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.net.InetAddress;
import java.net.Socket;
import java.util.Date;


public class TcpClient {

    //    public static String SERVER_IP = "128.199.159.130"; //your computer IP address
    public static String SERVER_IP = Const.API_URL; //your computer IP address
    public static final int SERVER_PORT = 9965;
    // message to send to the server
    private String mServerMessage;
    // sends message received notifications
    private OnMessageReceived mMessageListener = null;
    // while this is true, the server will continue running
    private boolean mRun = false;
    // used to send messages
//    private PrintWriter mBufferOut;
    // used to read messages from the server
    private BufferedReader mBufferIn;

    DataOutputStream outToServer;
    Socket socket;


    /**
     * Constructor of the class. OnMessagedReceived listens for the messages received from server
     */
    public TcpClient(OnMessageReceived listener) {
        mMessageListener = listener;
    }

    /**
     * Sends the message entered by client to the server
     *
     * @param message text entered by client
     */
//    public void sendMessage(String message) {
//        System.out.println("Hi 22 " + (message.getBytes()));
////        System.out.println("Hi 2200 " + message.getBytes(Charset.forName("UTF-8")));
//        if (mBufferOut != null && !mBufferOut.checkError()) {
//            System.out.println("Hi 33 " + (message));
////            mBufferOut.println(message);
//
//
//
//            byte[] myvarr = new byte[16];
//            myvarr[0] = 0x68; //login
//            myvarr[1] = 0x68;
//            myvarr[2] = 0x01;  // packetLength
//            myvarr[3] = 0x05;  // LAC
//            myvarr[4] = 0x09;
//            myvarr[5] = 0x01;   //TerminalId
//            myvarr[6] = 0x01;
//            myvarr[7] = 0x23;
//            myvarr[8] = 0x45;
//            myvarr[9] = 0x67;
//            myvarr[10] = 0x09;
//            myvarr[11] = 0x01;
//            myvarr[12] = 0x23;
//            myvarr[13] = 0x23;   // infs
//            myvarr[14] = 0x24;
//            myvarr[15] = 0x10; // protocol No
////		myvarr[16] = 0x24;
////		myvarr[17] = 0x25;
////		myvarr[18] = 0x10;
//            //		outToServer.writeByte(0x68);
////		outToServer.writeByte(0x68);
////		outToServer.writeByte(0x10);
////            outToServer.write(myvarr);
////            outToServer.flush();
////            DataOutputStream outToServer=null;
//////            OutputStreamWriter outToServer = null;
////            try {
////                outToServer.write(myvarr);
////                outToServer.flush();
////            } catch (IOException e) {
////                e.printStackTrace();
////            }
//
//            System.out.println("Hi 33 " + (myvarr));
//
////            mBufferOut.write(String.valueOf(myvarr));
//            mBufferOut.println(myvarr);
////            mBufferOut.println((message));
//            mBufferOut.flush();
//
//            System.out.println("Hi 44 " + (message));
//        }
//    }
    public boolean sendMessage(final byte[] message) {
        boolean isByteSent = false;
        System.out.println("Hi 22 " + (message));

        if (outToServer != null) {
//            System.out.println("Hi 33 " + (message));

//            byte[] myvarr = new byte[42];
//            myvarr[0] = 0x68; //login
//            myvarr[1] = 0x68;
//
//            myvarr[2] = 0x25;  // packetLength
//
//            myvarr[3] = 0x26;  // LAC
//            myvarr[4] = 0x6A;
//
//            myvarr[5] = 0x01;   //TerminalId
//            myvarr[6] = 0x23;
//            myvarr[7] = 0x45;
//            myvarr[8] = 0x12;
//            myvarr[9] = 0x34;
//            myvarr[10] = 0x51;
//            myvarr[11] = 0x23;
//            myvarr[12] = 0x45;
//
//            myvarr[13] = 0x00; //Information serial number
//            myvarr[14] = 0x01;
//
//            myvarr[15] = 0x10; // protocol No
//
////            0x0A 0x03 0x17 0x0F 0x32 0x17
//            myvarr[16] = 0x0A;   // Date time
//            myvarr[17] = 0x03;
//            myvarr[18] = 0x17;
//            myvarr[19] = 0x0F;
//            myvarr[20] = 0x32;
//            myvarr[21] = 0x17;
//
//            //            message="00000006";
//
////            0x02 0x6B 0x3F 0x3E lat and lng
//
////            1639F47
//
//            myvarr[22] = 0x01;   // lat
//            myvarr[23] = 0x63;
//            myvarr[24] = (byte) 0x9F;
//            myvarr[25] = 0x47;
//
//
////            896B20F
//
//            myvarr[26] = 0x08;   // lng
//            myvarr[27] = (byte) 0x96;
//            myvarr[28] = (byte) 0xB2;
//            myvarr[29] = 0x0F;
//
//            myvarr[30] = 0x00; // speed
//
////            0x15 0x4C,
//
//            myvarr[31] = 0x01; // Course
//            myvarr[32] = 0x56;
//
//            myvarr[33] = 0x00; // MNC
//
//            myvarr[34] = 0x1D; // Cell ID
//            myvarr[35] = (byte) 0xF1;
//
//
//            myvarr[36] = 0x00;   // Status
//            myvarr[37] = 0x00;
//            myvarr[38] = 0x00;
//            myvarr[39] = 0x06;
//
//
////            0x0D 0x0A
//
//            myvarr[40] = 0x0D; // Stop bit
//            myvarr[41] = 0x0A;


//            try {
            System.out.println("Hi 33 " + (message));

//                System.out.println("Hi socket inet is reachable "+socket.getInetAddress().isReachable(5000));

//                System.out.println("Hi 33 " + (message) + " socket connected " + socket.isConnected() + " bound " + socket.isBound() + " IS shutdown " + socket.isInputShutdown()
//                        + " isOutputShutdown " + socket.isOutputShutdown() + " closed " + socket.isClosed());

            if (socket != null && !socket.isClosed()) {
//                    System.out.println("Hi socket not null ");

//                Handler handler = new Handler(Looper.getMainLooper());
//                handler.post(new Runnable() {
//
//                    @Override
//                    public void run() {
//                        try {


                Thread thread = new Thread(new Runnable() {

                    @Override
                    public void run() {
                        try {
                            //Your code goes here
                            outToServer.write(message);
                            outToServer.flush();
                        } catch (Exception e) {
                            e.printStackTrace();


                            StringBuilder report = new StringBuilder();
                            Date curDate = new Date();
                            report.append("Error Report collected on : ")
                                    .append(curDate.toString()).append('\n').append('\n');
                            report.append("Informations :").append('\n');
//            addInformation(report);
                            report.append('\n').append('\n');
                            report.append("Stack:\n");
                            final Writer result = new StringWriter();
                            final PrintWriter printWriter = new PrintWriter(result);
                            e.printStackTrace(printWriter);
                            report.append(result.toString());
                            printWriter.close();
                            report.append('\n');
                            report.append("**** End of current Report ***");
                            Log.e(UnCaughtException.class.getName(),
                                    "Error while sendErrorMail" + report);
                            Constant.writeAFile(report.toString());

//                                isByteSent = false;
                            stopClient();

                        }
                    }
                });

                thread.start();


//                            outToServer.write(message);
//                            outToServer.flush();
//                        } catch (IOException e) {
//
//                            e.printStackTrace();
//
//
//                            StringBuilder report = new StringBuilder();
//                            Date curDate = new Date();
//                            report.append("Error Report collected on : ")
//                                    .append(curDate.toString()).append('\n').append('\n');
//                            report.append("Informations :").append('\n');
////            addInformation(report);
//                            report.append('\n').append('\n');
//                            report.append("Stack:\n");
//                            final Writer result = new StringWriter();
//                            final PrintWriter printWriter = new PrintWriter(result);
//                            e.printStackTrace(printWriter);
//                            report.append(result.toString());
//                            printWriter.close();
//                            report.append('\n');
//                            report.append("**** End of current Report ***");
//                            Log.e(UnCaughtException.class.getName(),
//                                    "Error while sendErrorMail" + report);
//                            Constant.writeAFile(report.toString());
//
////                                isByteSent = false;
//                            stopClient();
//                        }
//                    }
//                });


                isByteSent = true;
            } else {
                isByteSent = false;
//                    System.out.println("Hi socket closed ");
                stopClient();
            }
//            } catch (IOException e) {
//
//                e.printStackTrace();
//
//
//                StringBuilder report = new StringBuilder();
//                Date curDate = new Date();
//                report.append("Error Report collected on : ")
//                        .append(curDate.toString()).append('\n').append('\n');
//                report.append("Informations :").append('\n');
////            addInformation(report);
//                report.append('\n').append('\n');
//                report.append("Stack:\n");
//                final Writer result = new StringWriter();
//                final PrintWriter printWriter = new PrintWriter(result);
//                e.printStackTrace(printWriter);
//                report.append(result.toString());
//                printWriter.close();
//                report.append('\n');
//                report.append("**** End of current Report ***");
//                Log.e(UnCaughtException.class.getName(),
//                        "Error while sendErrorMail" + report);
//                Constant.writeAFile(report.toString());
//
//                isByteSent = false;
//                stopClient();
//            }


//            mBufferOut.write(String.valueOf(outToServer));
//            mBufferOut.println(myvarr);
//            mBufferOut.println((message));
//            mBufferOut.flush();

            System.out.println("Hi 44 " + (message));
        } else {
            System.out.println("Hi output stream null");
            isByteSent = false;
            //insert data here
            stopClient();
        }
        return isByteSent;
    }


    public boolean isConnectedWithTCP() {

        if (socket != null) {
            return socket.isClosed();
        }

        return false;
    }

    public void run(String domainName) {
//        System.out.println("Hi tcp client run method called ::::" + domainName);

        if (domainName != null) {
//            System.out.println("Hi tcp client domain name is ::::" + domainName);
            SERVER_IP = domainName;

//            SERVER_IP="puccagps.com";
            mRun = true;


            try {
                //here you must put your computer's IP address.
                InetAddress serverAddr = InetAddress.getByName(SERVER_IP);

//            Log.e("TCP Client", "C: Connecting...");

                //create a socket to make the connection with the server
                socket = new Socket(serverAddr, SERVER_PORT);


                try {

//                Log.i("TCP", "Msg received socket.getOutputStream " + socket.getOutputStream());

                    //sends the message to the server
//                mBufferOut = new PrintWriter(new BufferedWriter(new OutputStreamWriter(socket.getOutputStream())), true);

                    outToServer = new DataOutputStream((new DataOutputStream(socket.getOutputStream())));

                    //receives the message which the server sends back

//                Log.i("TCP", "Msg received socket.getInputStream " + socket.getInputStream() + " " + mRun);

                    mBufferIn = new BufferedReader(new InputStreamReader(socket.getInputStream()));


//                mServerMessage = mBufferIn.readLine();

//                Log.i("TCP", "Msg received mServerMessage 0001111 " + mBufferIn.readLine());


                    // send login name
//                sendMessage(Constants.LOGIN_NAME + PreferencesManager.getInstance().getUserName());
//                sendMessage("test");

                    //in this while the client listens for the messages sent by the server
                    while (mRun) {
//                    Log.i("TCP", "Msg received mServerMessage 111000 " + mBufferIn.readLine());
                        if (socket != null && !socket.isClosed()) {
                            mServerMessage = mBufferIn.readLine();

                            Log.i("TCP", "Msg received mServerMessage " + mServerMessage);

                            if (mServerMessage != null && mMessageListener != null) {
                                //call the method messageReceived from MyActivity class

//                        Log.i("TCP", "Msg received mServerMessage 111 " + mServerMessage);

                                mMessageListener.messageReceived(mServerMessage);
                            } else {
                                stopClient();
                            }
                        } else {
                            stopClient();
                        }

                    }

//                Log.e("RESPONSE FROM SERVER", "S: Received Message: '" + mServerMessage + "'");

                } catch (Exception e) {

                    Log.e("TCP", "S: Error", e);


                    StringBuilder report = new StringBuilder();
                    Date curDate = new Date();
                    report.append("Error Report collected on : ")
                            .append(curDate.toString()).append('\n').append('\n');
                    report.append("Informations :").append('\n');
//            addInformation(report);
                    report.append('\n').append('\n');
                    report.append("Stack:\n");
                    final Writer result = new StringWriter();
                    final PrintWriter printWriter = new PrintWriter(result);
                    e.printStackTrace(printWriter);
                    report.append(result.toString());
                    printWriter.close();
                    report.append('\n');
                    report.append("**** End of current Report ***");
                    Log.e(UnCaughtException.class.getName(),
                            "Error while sendErrorMail" + report);
                    Constant.writeAFile(report.toString());


                } finally {
                    //the socket must be closed. It is not possible to reconnect to this socket
                    // after it is closed, which means a new socket instance has to be created.
                    socket.close();
                }

            } catch (Exception e) {

                Log.e("TCP", "C: Error", e);


                StringBuilder report = new StringBuilder();
                Date curDate = new Date();
                report.append("Error Report collected on : ")
                        .append(curDate.toString()).append('\n').append('\n');
                report.append("Informations :").append('\n');
//            addInformation(report);
                report.append('\n').append('\n');
                report.append("Stack:\n");
                final Writer result = new StringWriter();
                final PrintWriter printWriter = new PrintWriter(result);
                e.printStackTrace(printWriter);
                report.append(result.toString());
                printWriter.close();
                report.append('\n');
                report.append("**** End of current Report ***");
                Log.e(UnCaughtException.class.getName(),
                        "Error while sendErrorMail" + report);
                Constant.writeAFile(report.toString());


            }
        }

    }

    public void stopClient() {
        System.out.println("Hi Tcp stop client called :::: ");


        mRun = false;


        try {
            if (outToServer != null) {
                outToServer.flush();
                outToServer.close();
            }

            if (socket != null) {
                socket.close();
            }
        } catch (IOException e) {
            e.printStackTrace();

            StringBuilder report = new StringBuilder();
            Date curDate = new Date();
            report.append("Error Report collected on : ")
                    .append(curDate.toString()).append('\n').append('\n');
            report.append("Informations :").append('\n');
//            addInformation(report);
            report.append('\n').append('\n');
            report.append("Stack:\n");
            final Writer result = new StringWriter();
            final PrintWriter printWriter = new PrintWriter(result);
            e.printStackTrace(printWriter);
            report.append(result.toString());
            printWriter.close();
            report.append('\n');
            report.append("**** End of current Report ***");
            Log.e(UnCaughtException.class.getName(),
                    "Error while sendErrorMail" + report);
            Constant.writeAFile(report.toString());

        }


//        socket.close();
        mMessageListener = null;
        mBufferIn = null;
        outToServer = null;
        mServerMessage = null;
    }

    public interface OnMessageReceived {
        public void messageReceived(String message);
    }
}
