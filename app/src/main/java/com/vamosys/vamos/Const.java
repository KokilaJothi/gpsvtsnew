package com.vamosys.vamos;

import android.location.Location;
import android.util.Log;

import com.vamosys.model.VehicleStructure;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

public class Const {

    //    public static String API_URL = "http://vamosys.com/";


    public static String API_URL = "http://gpsvts.net/";
//  public static String API_URL = "http://209.97.163.4:9000/";
//public static String API_URL = "http://188.166.244.126:9000/";
    public static String PRIVACY_URL = "http://gpsvts.net/privacy.html";

    public static String CAMERA_IMAGE_IP = "http://128.199.139.188/";

    public static String NEW_API_URL = "http://api.vamosys.com/mobile/";

    //    public static String OSM_URL="http://207.154.194.241/osm_tiles/";
    public static String OSM_URL = "http://maps.thegps.in/osm_tiles/";

    public static String IP = "http://188.166.244.126:9000";

    public static String SAVE_ADDRESS_API = NEW_API_URL + "saveAddressFromFrontend?address=";
    public static String PAYMENT_DUE_API = API_URL + "mobile/getZohoInvoice?userId=";


    VehicleStructure[] vehicleStructarr;

    public Const() {
    }

    public static String getReportTimefromserver(String datefromserver) {
        long unixSeconds = Long.parseLong(datefromserver);
        Date date = new Date(unixSeconds); // *1000 is to convert seconds to milliseconds
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd \n HH:mm:ss"); // .
        String formattedDate = sdf.format(date);
        return formattedDate;
    }

    public static String getReport24HourTimefromserver(String datefromserver) {
        long unixSeconds = Long.parseLong(datefromserver);

//        System.out.println("End date long value is :::" + unixSeconds);

        Date date = new Date(unixSeconds); // *1000 is to convert seconds to milliseconds
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd \n HH:mm:ss"); // the format of your date
        String formattedDate = sdf.format(date);
        return formattedDate;
    }

    public static String getTripTimefromserver(String datefromserver) {
        long unixSeconds = Long.parseLong(datefromserver);
        Date date = new Date(unixSeconds); // *1000 is to convert seconds to milliseconds
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); // the format of your date
        String formattedDate = sdf.format(date);
        return formattedDate;
    }

    public static String getTripTimefromserver(Long datefromserver) {
//        long unixSeconds = Long.parseLong(datefromserver);
        Date date = new Date(datefromserver); // *1000 is to convert seconds to milliseconds
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd kk:mm:ss"); // the format of your date
        String formattedDate = sdf.format(date);
        return formattedDate;
    }

    public static String getAcReportTime(Long datefromserver) {
//        long unixSeconds = Long.parseLong(datefromserver);
        Date date = new Date(datefromserver); // *1000 is to convert seconds to milliseconds
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); // the format of your date
        String formattedDate = sdf.format(date);
        return formattedDate;
    }

    public static long getDifferentValue(String currentTime, String timeOnly)
    {
        System.out.println("long valuew"+timeOnly+ " "+currentTime);
        long diffrent = 0;
        SimpleDateFormat f = new SimpleDateFormat("hh:mm:ss");
        try {
            Date d = f.parse(currentTime);
            System.out.println("long valuew 1");
            long cu=d.getTime();
            System.out.println("long valuew 2");
            Date d1=f.parse(timeOnly);
            System.out.println("long valuew 3");
            long timeonly=d1.getTime();
            System.out.println("long valuew"+timeOnly+ " "+cu);
            diffrent=cu-timeonly;

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return diffrent;
    }


    public String getGroupInfo(String groupinfo) {
        return "";
    }

    public VehicleStructure[] getVehiclesInfo(String vehicleinfo) {
        try {
            JSONArray vehiclearr = new JSONArray(vehicleinfo);
            vehicleStructarr = new VehicleStructure[vehiclearr.length()];
            for (int vehicle = 0; vehicle < vehiclearr.length(); vehicle++) {
                JSONObject vehiclejson = vehiclearr.getJSONObject(vehicle);
                vehicleStructarr[vehicle] = new VehicleStructure();
                vehicleStructarr[vehicle].setlatitude(vehiclejson.getString("latitude"));
                vehicleStructarr[vehicle].setlongitude(vehiclejson.getString("longitude"));
                vehicleStructarr[vehicle].setvehicleid(vehiclejson.getString("vehicleId"));
                vehicleStructarr[vehicle].setshortname(vehiclejson.getString("shortName"));
            }
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return vehicleStructarr;
    }

    public String getVehicleInfo(int position) {
        return "";
    }

    public String sendGet(String url, int connection_timeout) throws Exception {

        System.out.println("The get url is :::" + url);

        URL obj = new URL(url);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();
        // optional default is GET
        con.setRequestMethod("GET");
        con.setConnectTimeout(connection_timeout);
        //add request header
        int responseCode = con.getResponseCode();
        BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();
        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();
        return response.toString();
    }

    public String getDateFormat(Calendar date) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        return dateFormat.format(date.getTime());
    }

    public Calendar getCalendarDate(int year, int month, int day, int hourOfDay, int minute) {
        Calendar calNow = Calendar.getInstance();
        Calendar calSet = (Calendar) calNow.clone();
        calSet.set(year, month, day);
        calSet.set(Calendar.HOUR_OF_DAY, hourOfDay);
        calSet.set(Calendar.MINUTE, minute);
        calSet.set(Calendar.SECOND, 0);
        calSet.set(Calendar.MILLISECOND, 0);
        if (calSet.compareTo(calNow) <= 0) {
        }
        return calSet;
    }

    public String getDateandTime(String type) {
        String returndatetime = null;
        Calendar get_date = Calendar.getInstance();
        if (type.equals("yesterday")) {
            get_date.add(Calendar.DATE, -1);
            List<String> event_date_arr_items = Arrays.asList(GetCurrentDate(get_date).split("-"));
            returndatetime = getDateFormat(getCalendarDate(Integer.parseInt(event_date_arr_items.get(0)), Integer.parseInt(event_date_arr_items.get(1)) - 1, Integer.parseInt(event_date_arr_items.get(2)), 0, 0));
        } else if (type.equals("daybeforeyesterday")) {
            get_date.add(Calendar.DATE, -2);
            List<String> event_date_arr_items = Arrays.asList(GetCurrentDate(get_date).split("-"));
            returndatetime = getDateFormat(getCalendarDate(Integer.parseInt(event_date_arr_items.get(0)), Integer.parseInt(event_date_arr_items.get(1)) - 1, Integer.parseInt(event_date_arr_items.get(2)), 0, 0));
        } else if (type.equals("today")) {
            List<String> event_date_arr_items = Arrays.asList(GetCurrentDate(get_date).split("-"));
            returndatetime = getDateFormat(getCalendarDate(Integer.parseInt(event_date_arr_items.get(0)), Integer.parseInt(event_date_arr_items.get(1)) - 1, Integer.parseInt(event_date_arr_items.get(2)), 0, 0));
        }
        return returndatetime;
    }

    public String GetCurrentDate(Calendar getdate) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        return dateFormat.format(getdate.getTime());
    }

    public String GetFromCurrentDate(int i) {
        Date date1 = new Date();
        GregorianCalendar gc = new GregorianCalendar();
        gc.setTime(date1);
        gc.add(GregorianCalendar.MONTH, -(i + 1));
        Date dateBefore = gc.getTime();

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        return dateFormat.format(dateBefore.getTime());
    }

    public String GetToCurrentDate(int i) {
        Date date1 = new Date();
        GregorianCalendar gc = new GregorianCalendar();
        gc.setTime(date1);
        gc.add(GregorianCalendar.MONTH, -i);
        Date dateBefore = gc.getTime();

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        return dateFormat.format(dateBefore.getTime());
    }


    public String GetFromDateFromGiven(String mFromDate) {

        SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd");
        Date date1 = null;
        try {
            date1 = f.parse(mFromDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
//        Date date1 = new Date();
        GregorianCalendar gc = new GregorianCalendar();
        gc.setTime(date1);
        gc.add(GregorianCalendar.MONTH, -1);
        Date dateBefore = gc.getTime();

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        return dateFormat.format(dateBefore.getTime());
    }

    public String GetToDateFromGivenDate(String mToDate) {
//        Date date1 = new Date();

        SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd");
        Date date1 = null;
        try {
            date1 = f.parse(mToDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        GregorianCalendar gc = new GregorianCalendar();
        gc.setTime(date1);
        gc.add(GregorianCalendar.MONTH, 1);
        Date dateBefore = gc.getTime();

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        return dateFormat.format(dateBefore.getTime());
    }


    public static String getTimefromserver(String datefromserver) {
        long unixSeconds = Long.parseLong(datefromserver);
        Date date = new Date(unixSeconds); // *1000 is to convert seconds to milliseconds
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); // the format of your date
        String formattedDate = sdf.format(date);
        return formattedDate;
    }

    public static String getDate() {
//        long unixSeconds = Long.parseLong(mMilliSec);
//		Date date = new Date(unixSeconds); // *1000 is to convert seconds to
        // milliseconds
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyy");
//        SimpleDateFormat sdf1 = new SimpleDateFormat("HH:mm:ss");
        Date date = new Date(); // *1000 is to convert
        // secoteFormat("yyyy-MM-dd \n HH:mm:ss");
        // // the format of your date
        String formattedDate = sdf.format(date);
//        String formattedDate1 = sdf1.format(date);
        return formattedDate;
    }

    public String getHistoryTimefromserver(String datefromserver) {
        long unixSeconds = Long.parseLong(datefromserver);
        Date date = new Date(unixSeconds); // *1000 is to convert seconds to milliseconds
        SimpleDateFormat sdf = new SimpleDateFormat("EEE MMM d HH:mm:ss"); // the format of your date
        String formattedDate = sdf.format(date);
        //System.out.println(formattedDate);
        return formattedDate;
    }

    public static String getVehicleTime(String timefromserver) {
        // System.out.println("get vehicle time is :::::" + timefromserver);

        String formattedtime = null;
//        if (TimeUnit.MILLISECONDS.toDays(Long.parseLong(timefromserver)) > 0) {
//            formattedtime = String.format("%02d d %02d:%02d:%02d", TimeUnit.MILLISECONDS.toDays(Long.parseLong(timefromserver)), TimeUnit.MILLISECONDS.toHours(Long.parseLong(timefromserver)) - TimeUnit.DAYS.toHours(TimeUnit.MILLISECONDS.toHours(Long.parseLong(timefromserver))), TimeUnit.MILLISECONDS.toMinutes(Long.parseLong(timefromserver)) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(Long.parseLong(timefromserver))), TimeUnit.MILLISECONDS.toSeconds(Long.parseLong(timefromserver)) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(Long.parseLong(timefromserver))));
//        } else {
        formattedtime = String.format("%02d:%02d:%02d", TimeUnit.MILLISECONDS.toHours(Long.parseLong(timefromserver)), TimeUnit.MILLISECONDS.toMinutes(Long.parseLong(timefromserver)) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(Long.parseLong(timefromserver))), TimeUnit.MILLISECONDS.toSeconds(Long.parseLong(timefromserver)) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(Long.parseLong(timefromserver))));
//        }


        //.  System.out.println("The return get vehicle time is :::"+formattedtime);
        return formattedtime;
    }

    public static String getVehicleTimeNew(String timefromserver) {
        int SECOND = 1000;
        int MINUTE = 60 * SECOND;
        int HOUR = 60 * MINUTE;
        int DAY = 24 * HOUR;

// TODO: this is the value in ms
        long ms = Long.parseLong(timefromserver);
        StringBuffer text = new StringBuffer("");
        if (ms > DAY) {
            text.append(ms / DAY).append("d ");
            ms %= DAY;
        }
        if (ms > HOUR) {
//            text.append(ms / HOUR).append(":");

            if (ms / HOUR < 10) {
                text.append("0").append(ms / HOUR).append(":");
            } else {
                text.append(ms / HOUR).append(":");
            }

            ms %= HOUR;
        } else {
            text.append("00:");
        }
        if (ms > MINUTE) {

            if (ms / MINUTE < 10) {
                text.append("0").append(ms / MINUTE).append(":");
            } else {
                text.append(ms / MINUTE).append(":");
            }


            ms %= MINUTE;
        } else {
            text.append("00:");
        }
        if (ms > SECOND) {
//            text.append(ms / SECOND);

            if (ms / SECOND < 10) {
                text.append("0").append(ms / SECOND);
            } else {
                text.append(ms / SECOND);
            }

            ms %= SECOND;
        } else {
            text.append("00");
        }
//        text.append(ms + " ms");

        return text.toString();
    }


    public static String getTripDate(String mMilliSec) {
        long unixSeconds = Long.parseLong(mMilliSec);
//		Date date = new Date(unixSeconds); // *1000 is to convert seconds to
        // milliseconds
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
//        SimpleDateFormat sdf1 = new SimpleDateFormat("HH:mm:ss");
        Date date = new Date(unixSeconds); // *1000 is to convert
        // secoteFormat("yyyy-MM-dd \n HH:mm:ss");
        // // the format of your date
        String formattedDate = sdf.format(date);
//        String formattedDate1 = sdf1.format(date);
        return formattedDate;
    }

    public static String getCurrentDate() {
//        long unixSeconds = Long.parseLong(mMilliSec);
//		Date date = new Date(unixSeconds); // *1000 is to convert seconds to
        // milliseconds
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
//        SimpleDateFormat sdf1 = new SimpleDateFormat("HH:mm:ss");
        Date date = new Date(); // *1000 is to convert
        // secoteFormat("yyyy-MM-dd \n HH:mm:ss");
        // // the format of your date
        String formattedDate = sdf.format(date);
//        String formattedDate1 = sdf1.format(date);
        return formattedDate;
    }


    public static String getTripYesterdayDate2() {
//        long unixSeconds = Long.parseLong(mMilliSec);
//		Date date = new Date(unixSeconds); // *1000 is to convert seconds to
        // milliseconds
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
//        SimpleDateFormat sdf1 = new SimpleDateFormat("HH:mm:ss");
//        Date date = new Date(unixSeconds); // *1000 is to convert
        // secoteFormat("yyyy-MM-dd \n HH:mm:ss");
        // // the format of your date
        String formattedDate = sdf.format(yesterday());
//        String formattedDate1 = sdf1.format(date);
        return formattedDate;
    }

    private static Date yesterday() {
        final Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -1);
        return cal.getTime();
    }

    public static String getTripTime(String mMilliSec) {
        long unixSeconds = Long.parseLong(mMilliSec);
//		Date date = new Date(unixSeconds); // *1000 is to convert seconds to
        // milliseconds
//        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat sdf1 = new SimpleDateFormat("HH:mm:ss");
        Date date = new Date(unixSeconds); // *1000 is to convert
        // secoteFormat("yyyy-MM-dd \n HH:mm:ss");
        // // the format of your date
//        String formattedDate = sdf.format(date);

        System.out.println("Date :::" + date + " :: " + mMilliSec);

        String formattedDate1 = sdf1.format(date);
        return formattedDate1;
    }

    public  static  String gettimevalue(long ms){
         int SECOND = 1000;
        int MINUTE = 60 * SECOND;
       int HOUR = 60 * MINUTE;
       int DAY = 24 * HOUR;
       String mHourValue = "0",mNoOfDays = "0", mMinsValue="0", mSecsValue="0",mReturnValue = "0 ";

        StringBuffer text = new StringBuffer("");
        if (ms > DAY) {
            //text.append(ms / DAY).append(" days ");
            mNoOfDays = String.valueOf(ms / DAY);
            Log.d("mNoOfDays",""+mNoOfDays);
            ms %= DAY;
        }
        if (ms > HOUR) {
           // text.append(ms / HOUR).append(" hours ");
            mHourValue = String.valueOf(ms / HOUR);
            Log.d("mHourValue",""+mHourValue);
            ms %= HOUR;
        }
        if (ms > MINUTE) {
           // text.append(ms / MINUTE).append(" minutes ");
            mMinsValue = String.valueOf(ms / MINUTE);
            Log.d("mMinsValue",""+mMinsValue);
            ms %= MINUTE;
        }
        if (ms > SECOND) {
            //text.append(ms / SECOND).append(" seconds ");
            mSecsValue = String.valueOf(ms / SECOND);
            Log.d("mSecsValue",""+mSecsValue);
            ms %= SECOND;
        }
        text.append(ms + " ms");
        Log.d("millisoval",""+ms);
        System.out.println(text.toString());
        mReturnValue = mNoOfDays + "d " + mHourValue + "H:" + mMinsValue
                + "M:" + mSecsValue + "S";
        return mReturnValue;
    }

    public static String getTimeValue(long longVal) {
        // long longVal = biggy.longValue();
        String mHourValue, mMinsValue, mSecsValue, mReturnValue = null;
        int hours = (int) longVal / 3600000;
        int remainder = (int) longVal - hours * 3600000;

        int mins = remainder / 60000;
        remainder = remainder - mins * 60000;

        int secs = remainder / 1000;

        if (hours < 10) {
            mHourValue = "0" + String.valueOf(hours);
        } else {
            mHourValue = String.valueOf(hours);
        }

        if (mins < 10) {
            mMinsValue = "0" + String.valueOf(mins);
        } else {
            mMinsValue = String.valueOf(mins);
        }

        if (secs < 10) {
            mSecsValue = "0" + String.valueOf(secs);
        } else {
            mSecsValue = String.valueOf(secs);
        }

        int mIntegerHrValue = Integer.parseInt(mHourValue);

        // int mIntegerHrValue = 25;

        int mNoOfDays = mIntegerHrValue % (24);

        int mNewHourValue = mIntegerHrValue - (mNoOfDays * 24);

        mReturnValue = mNoOfDays + "d " + mNewHourValue + "H:" + mMinsValue
                + "M:" + mSecsValue + "S";

//		System.out.println(mHourValue + ":" + mMinsValue + ":" + mSecsValue);
//
//		System.out.println(mNoOfDays + "d " + mNewHourValue + "H:" + mMinsValue
//				+ "M:" + mSecsValue + "S");
        return mReturnValue;
    }


    public static String getreportTimeValue(long longVal) {
        // long longVal = biggy.longValue();
        String mHourValue, mMinsValue, mSecsValue, mReturnValue = "00:00:00";
        int hours = (int) longVal / 3600000;
        int remainder = (int) longVal - hours * 3600000;

        int mins = remainder / 60000;
        remainder = remainder - mins * 60000;

        int secs = remainder / 1000;

        if (hours < 10) {
            mHourValue = "0" + String.valueOf(hours);
        } else {
            mHourValue = String.valueOf(hours);
        }

        if (mins < 10) {
            mMinsValue = "0" + String.valueOf(mins);
        } else {
            mMinsValue = String.valueOf(mins);
        }

        if (secs < 10) {
            mSecsValue = "0" + String.valueOf(secs);
        } else {
            mSecsValue = String.valueOf(secs);
        }

//        int mIntegerHrValue = Integer.parseInt(mHourValue);

        // int mIntegerHrValue = 25;

//        int mNoOfDays = mIntegerHrValue % (24);

//        int mNewHourValue = mIntegerHrValue - (mNoOfDays * 24);

        mReturnValue = mHourValue + ":" + mMinsValue
                + ":" + mSecsValue;

//		System.out.println(mHourValue + ":" + mMinsValue + ":" + mSecsValue);
//
//		System.out.println(mNoOfDays + "d " + mNewHourValue + "H:" + mMinsValue
//				+ "M:" + mSecsValue + "S");
        return mReturnValue;
    }

    public static String getCurrentTime() {
        long unixSeconds = System.currentTimeMillis();

//		Date date = new Date(unixSeconds); // *1000 is to convert seconds to
        // milliseconds
//        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat sdf1 = new SimpleDateFormat("HH:mm:ss");
        Date date = new Date(unixSeconds); // *1000 is to convert
        // secoteFormat("yyyy-MM-dd \n HH:mm:ss");
        // // the format of your date
//        String formattedDate = sdf.format(date);
        String formattedDate1 = sdf1.format(date);
        return formattedDate1;
    }


    public static long daysDiff(String fromDate) {
        // fromDate="00-00-0000";
        long daysCount = 0;
        if (fromDate != null && fromDate.length() > 2) {
            SimpleDateFormat myFormat = new SimpleDateFormat("yyyy-MM-dd");
            Date date = new Date();

            String toDate = myFormat.format(date);

            long diff = 0;
            Date date1 = null;
            Date date2 = null;
            try {
                date1 = myFormat.parse(fromDate.trim());
                date2 = myFormat.parse(toDate);
                diff = ((date2.getTime() - date1.getTime()));

                if (!fromDate.trim().equalsIgnoreCase("0000-00-00")) {

                    daysCount = TimeUnit.DAYS.convert(diff,
                            TimeUnit.MILLISECONDS);

                }

            } catch (ParseException e) {
                e.printStackTrace();
            }


        } else {
            daysCount = 0;
        }

        return daysCount;
    }


    public static double distance(double lat1, double lon1, double lat2, double lon2) {
//        double theta = lon1 - lon2;
//        double dist = Math.sin(deg2rad(lat1))
//                * Math.sin(deg2rad(lat2))
//                + Math.cos(deg2rad(lat1))
//                * Math.cos(deg2rad(lat2))
//                * Math.cos(deg2rad(theta));
//        dist = Math.acos(dist);
//        dist = rad2deg(dist);
//        dist = dist * 60 * 1.1515;

        float[] distanceResults = new float[1];

        Location.distanceBetween(
                lat1, lon1,
                lat2, lon2,
                distanceResults);


        // multiplication factor to covert kilometers to meters
        final double KM_TO_M = 1000.0;

        // multiplication factor to convert meters to kilometers
        final double M_TO_KM = 1 / KM_TO_M;

        // Score tracks close to the current location higher.
        double distanceKm = distanceResults[0] * M_TO_KM;

        return (distanceKm);


//        Location startPoint=new Location("locationA");
//        startPoint.setLatitude(17.372102);
//        startPoint.setLongitude(78.484196);
//
//        Location endPoint=new Location("locationA");
//        endPoint.setLatitude(17.375775);
//        endPoint.setLongitude(78.469218);
//
//        double distance=startPoint.distanceTo(endPoint);


    }

    public static boolean canLoadInvoiceApi(String date) {
        boolean canLoad = false;


        return canLoad;
    }

    private static double deg2rad(double deg) {
        return (deg * Math.PI / 180.0);
    }

    private static double rad2deg(double rad) {
        return (rad * 180.0 / Math.PI);
    }

}
