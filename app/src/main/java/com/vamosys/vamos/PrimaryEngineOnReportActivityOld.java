//package com.vamosys.vamos;
//
//import android.app.AlertDialog;
//import android.app.DatePickerDialog;
//import android.app.Dialog;
//import android.app.ProgressDialog;
//import android.app.TimePickerDialog;
//import android.content.Context;
//import android.content.Intent;
//import android.content.SharedPreferences;
//import android.graphics.Color;
//import android.graphics.drawable.ColorDrawable;
//import android.location.Address;
//import android.location.Geocoder;
//import android.os.AsyncTask;
//import android.os.Bundle;
//import android.os.Handler;
//import android.os.Looper;
//import android.preference.PreferenceManager;
//import android.support.annotation.NonNull;
//import android.support.design.widget.BottomSheetBehavior;
//import android.support.v4.app.FragmentManager;
//import android.support.v7.app.AppCompatActivity;
//import android.support.v7.widget.LinearLayoutManager;
//import android.support.v7.widget.RecyclerView;
//import android.support.v7.widget.Toolbar;
//import android.text.SpannableString;
//import android.text.style.UnderlineSpan;
//import android.util.DisplayMetrics;
//import android.view.Gravity;
//import android.view.LayoutInflater;
//import android.view.Menu;
//import android.view.MenuItem;
//import android.view.View;
//import android.view.ViewGroup;
//import android.view.Window;
//import android.widget.BaseAdapter;
//import android.widget.Button;
//import android.widget.DatePicker;
//import android.widget.FrameLayout;
//import android.widget.ImageView;
//import android.widget.LinearLayout;
//import android.widget.ListView;
//import android.widget.RadioButton;
//import android.widget.RadioGroup;
//import android.widget.RelativeLayout;
//import android.widget.TextView;
//import android.widget.TimePicker;
//import android.widget.Toast;
//
//import com.google.android.gms.maps.CameraUpdateFactory;
//import com.google.android.gms.maps.GoogleMap;
//import com.google.android.gms.maps.OnMapReadyCallback;
//import com.google.android.gms.maps.SupportMapFragment;
//import com.google.android.gms.maps.model.BitmapDescriptorFactory;
//import com.google.android.gms.maps.model.CameraPosition;
//import com.google.android.gms.maps.model.LatLng;
//import com.google.android.gms.maps.model.Marker;
//import com.google.android.gms.maps.model.MarkerOptions;
//import com.vamosys.model.AcReportDto;
//import com.vamosys.model.EngineReportDto;
//import com.vamosys.utils.ConnectionDetector;
//import com.vamosys.utils.Constant;
//import com.vamosys.utils.HorizontalScrollView;
//import com.vamosys.utils.HttpConfig;
//import com.vamosys.utils.TypefaceUtil;
//
//import org.json.JSONArray;
//import org.json.JSONException;
//import org.json.JSONObject;
//import org.osmdroid.api.IMapController;
//import org.osmdroid.config.Configuration;
//import org.osmdroid.util.GeoPoint;
//import org.osmdroid.views.MapView;
//import org.osmdroid.views.overlay.infowindow.InfoWindow;
//
//import java.io.IOException;
//import java.text.SimpleDateFormat;
//import java.util.ArrayList;
//import java.util.Calendar;
//import java.util.Date;
//import java.util.List;
//import java.util.Locale;
//
//
//public class PrimaryEngineOnReportActivityOld extends AppCompatActivity implements View.OnClickListener, OnMapReadyCallback {
//
//    Toolbar mToolbar;
//
//    ConnectionDetector cd;
//    SharedPreferences sp;
//    String mFromDate = null, mFromTime, mToTime, mFromDateTxtValue;
//    private RecyclerView mRecyclerView;
//    View bottomSheet;
//    BottomSheetBehavior behavior;
////    boolean bottomSheetEnabled = false;
//
//    private int year, month, day;
//    static final int TIME_DIALOG_ID = 1111;
//    static final int DATE_DIALOG_ID = 2222;
//    private int hour;
//    private int minute;
//    SimpleDateFormat timeFormat = new SimpleDateFormat(
//            "hh:mm:ss aa");
//
//    SimpleDateFormat timeFormatShow = new SimpleDateFormat(
//            "hh:mm aa");
//
//    ImageView $ChangeView;
//    Button mBtnSubmit;
//    TextView mTxtFromDate, mTxtEndDate, mTxtFromTime, mTxtEndTime, mTxtSelectedVehicle, mTxtNoRecord;
//    String mStartDate, mEndDate, mStartTime, mStartTimeValue, mEndDateValue, mEndTime, mEndTimeValue;
//    boolean isFromTime = false, isFromDate = false, isMapPresent = false, isHeaderPresent = false;
//    private static ArrayList<EngineReportDto> mPrimaryEngineReportData = new ArrayList<EngineReportDto>();
//    int mFromHourValue = 0, mFromMinuteValue = 0, mToHourValue = 0, mToMinuteValue = 0;
//
//    static int width;
//    static int height;
//    TableAdapter adapter;
//    static ListView lv;
//    private static GoogleMap map;
//    static float vehicle_zoom_level = 15.5F;
//    String mSELECTED_MAP_TYPE = "Normal";
//    static double mLatitude = 0;
//    static double mLongitude = 0;
//    static LinearLayout mAcReportDataMapLayout;
//    private Menu menu;
//
//    //OSM related
//    MapView mapview;
//    private IMapController mapController;
//    LinearLayout mGoogleMapLayout;
//    boolean isOsmEnabled = true;
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        Context ctx = getApplicationContext();
//        //important! set your user agent to prevent getting banned from the osm servers
//        Configuration.getInstance().load(ctx, PreferenceManager.getDefaultSharedPreferences(ctx));
//        setContentView(R.layout.activity_primary_engine_report);
//        sp = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
//        if (sp.getString("enabled_map", "") != null) {
//            if (sp.getString("enabled_map", "").trim().length() > 0) {
//
//                System.out.println("hi enabled map is " + sp.getString("enabled_map", ""));
//
//                if (sp.getString("enabled_map", "").equalsIgnoreCase(getResources().getString(R.string.osm))) {
//                    isOsmEnabled = true;
//                } else {
//                    isOsmEnabled = false;
//                }
//            }
//        }
//
//        init();
//        mToolbar = (Toolbar) findViewById(R.id.toolbar);
//        mToolbar.setTitle(getResources().getString(R.string.primary_engine_report));
//        mToolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_action_back));
//        setSupportActionBar(mToolbar);
//
//        cd = new ConnectionDetector(getApplicationContext());
//
//
//        if (sp.getString("ip_adds", "") != null) {
//            if (sp.getString("ip_adds", "").trim().length() > 0) {
//                Const.API_URL = sp.getString("ip_adds", "");
//            }
//        }
//        adapter = new TableAdapter(this);
//        mStartDate = Const.getTripYesterdayDate2();
////        mEndDate = Const.getTripYesterdayDate2();
//
////        mStartDate = Const.getTripDate(String.valueOf(System.currentTimeMillis()));
//        mEndDate = Const.getTripDate(String.valueOf(System.currentTimeMillis()));
//
//
//        long timeInMillis = System.currentTimeMillis();
//        Calendar cal1 = Calendar.getInstance();
//        cal1.setTimeInMillis(timeInMillis);
//
//        Calendar cal = Calendar.getInstance();
////        Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
//        cal.set(Calendar.HOUR_OF_DAY, 0);
//        cal.set(Calendar.MINUTE, 0);
//        cal.set(Calendar.SECOND, 0);
//        cal.set(Calendar.MILLISECOND, 0);
//
//
//        mFromHourValue = 00;
//        mFromMinuteValue = 00;
//
////        System.out.println("Hi date is :::" + timeFormat.format(cal.getTime()) + " mFromHourValue " + mFromHourValue + " " + mFromMinuteValue);
//
////        mStartTimeValue = "12:00 AM";
//        mStartTimeValue = timeFormatShow.format(cal.getTime());
////        mStartTime = "12:00:00 am";
//        mStartTime = timeFormat.format(cal.getTime());
//
//
//        final Calendar c = Calendar.getInstance();
//        // Current Hour
//        hour = c.get(Calendar.HOUR_OF_DAY);
//        // Current Minute
//        minute = c.get(Calendar.MINUTE);
//        isFromTime = false;
//        updateTime(hour, minute);
//
//        setBottomLayoutData();
//        bottomSheet = findViewById(R.id.bottom_sheet);
//        behavior = BottomSheetBehavior.from(bottomSheet);
//        behavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
//            @Override
//            public void onStateChanged(@NonNull View bottomSheet, int newState) {
//                // React to state change
////                System.out.println("Hi state 0000" + bottomSheetEnabled);
//
//
//            }
//
//            @Override
//            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
//                // React to dragging events
//            }
//        });
//        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//
//                startActivity(new Intent(PrimaryEngineOnReportActivity.this, MapMenuActivity.class));
//                finish();
//
//
//            }
//        });
//
//
//        if (cd.isConnectingToInternet()) {
////                                mStrFromDate = fromdatevalue.getText().toString().trim();
////                                mStrToDate = todatevalue.getText().toString().trim();
//
////            System.out.println("Hi data get method calling");
//
////            mStoppageData.clear();
//            new getPrimaryEngineOnData().execute();
//        } else {
//            Toast.makeText(getApplicationContext(),
//                    "Please check your network connection",
//                    Toast.LENGTH_SHORT).show();
//        }
//
//
//    }
//
//    private void init() {
//
//
//        mAcReportDataMapLayout = (LinearLayout) findViewById(R.id.temperature_data_map_view_layout);
//        mAcReportDataMapLayout.setVisibility(View.GONE);
//        $ChangeView = (ImageView) findViewById(R.id.temperature_data_map_ViewIcon);
//        $ChangeView.setOnClickListener(this);
//
//
//        mRecyclerView = (RecyclerView) findViewById(R.id.card_recycler_view);
//        mRecyclerView.setHasFixedSize(true);
//        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
//        mRecyclerView.setLayoutManager(layoutManager);
//
//
//        lv = (ListView) findViewById(R.id.tstoppage_report_listView1);
//        lv.setVisibility(View.VISIBLE);
//        mTxtNoRecord = (TextView) findViewById(R.id.temperature_report_no_record_txt);
//        mTxtNoRecord.setVisibility(View.GONE);
//
//        mTxtFromDate = (TextView) findViewById(R.id.txt_start_date_value);
//        mTxtFromTime = (TextView) findViewById(R.id.txt_start_time_value);
//        mTxtEndDate = (TextView) findViewById(R.id.txt_end_date_value);
//        mTxtEndTime = (TextView) findViewById(R.id.txt_end_time_value);
//        mBtnSubmit = (Button) findViewById(R.id.btn_done);
//        mTxtSelectedVehicle = (TextView) findViewById(R.id.selected_vehicle_txt);
//        mTxtSelectedVehicle.setText(Constant.SELECTED_VEHICLE_SHORT_NAME);
//
//        mTxtFromTime.setOnClickListener(this);
//        mTxtFromDate.setOnClickListener(this);
//        mTxtEndDate.setOnClickListener(this);
//        mTxtEndTime.setOnClickListener(this);
//        mBtnSubmit.setOnClickListener(this);
//
//        DisplayMetrics metrics = new DisplayMetrics();
//        getWindowManager().getDefaultDisplay().getMetrics(metrics);
//        height = metrics.heightPixels;
//        width = metrics.widthPixels;
//        Constant.ScreenWidth = width;
//        Constant.ScreenHeight = height;
//
//        mGoogleMapLayout = (LinearLayout) findViewById(R.id.google_map_layout);
//        RelativeLayout rl = (RelativeLayout) findViewById(R.id.map_view_relativelayout);
//
//        if (isOsmEnabled) {
//            $ChangeView.setVisibility(View.GONE);
//            mGoogleMapLayout.setVisibility(View.GONE);
//            rl.setVisibility(View.VISIBLE);
//            mapview = new MapView(this);
//            mapview.setTilesScaledToDpi(true);
//            rl.addView(mapview, new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.FILL_PARENT,
//                    RelativeLayout.LayoutParams.FILL_PARENT));
//
//            mapview.setBuiltInZoomControls(false);
//            mapview.setMultiTouchControls(true);
//            mapController = mapview.getController();
//            mapController.setZoom(8);
//            mapview.getOverlays().clear();
//            mapview.invalidate();
//            mapview.getOverlays().add(Constant.getTilesOverlay(getApplicationContext()));
//        } else {
//            $ChangeView.setVisibility(View.VISIBLE);
//            mGoogleMapLayout.setVisibility(View.VISIBLE);
//            rl.setVisibility(View.GONE);
//
//            FragmentManager myFragmentManager = getSupportFragmentManager();
//            SupportMapFragment myMapFragment = (SupportMapFragment) myFragmentManager
//                    .findFragmentById(R.id.temperature_data_map);
//            myMapFragment.getMapAsync(this);
//        }
//
//    }
//
//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        this.menu = menu;
//        getMenuInflater().inflate(R.menu.menu_calendaricon_closeicon, menu);
//        return true;
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        switch (item.getItemId()) {
//            // action with ID action_refresh was selected
//            case R.id.action_calendar:
////                storeFavourtite();
//
//                if (!bottomSheetEnabled) {
//                    behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
//                } else {
//                    // setData();
//                    behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
//                }
//
//                if (bottomSheetEnabled) {
//                    bottomSheetEnabled = false;
//                } else {
//                    bottomSheetEnabled = true;
//                }
//
//                break;
//            case R.id.action_map_close:
//                mAcReportDataMapLayout.setVisibility(View.GONE);
////                if (isMapPresent) {
//                isMapPresent = false;
//                lv.setVisibility(View.VISIBLE);
//                item.setVisible(false);
////                mImgMapViewClose.setVisibility(View.GONE);
//
//                break;
//
//            default:
//                break;
//        }
//
//        return true;
//    }
//
//    @Override
//    public void onClick(View v) {
//        switch (v.getId()) {
//            case R.id.txt_start_date_value:
//
//                isFromDate = true;
////                showDialog(DATE_DIALOG_ID);
//
//                showDateDialog();
//                break;
//            case R.id.txt_start_time_value:
////                showDialog(DATE_DIALOG_FROMID);
//                isFromTime = true;
//                //   showDialog(TIME_DIALOG_ID);
//
//                showTimeDialog();
//
//                break;
//            case R.id.txt_end_date_value:
////                createNumberDialog(1);
//                isFromDate = false;
////                showDialog(DATE_DIALOG_ID);
//
//
//                showDateDialog();
////                show(1);
//
//                break;
//            case R.id.txt_end_time_value:
////                createNumberDialog(0);
//                isFromTime = false;
//                // showDialog(TIME_DIALOG_ID);
//
//                showTimeDialog();
////                show(0);
//                break;
//            case R.id.temperature_data_map_ViewIcon:
//
//                opptionPopUp();
//
//                break;
//
//            case R.id.btn_done:
////                createNumberDialog(0);
////                show(0);
//                behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
//
//                if (cd.isConnectingToInternet()) {
////                                mStrFromDate = fromdatevalue.getText().toString().trim();
////                                mStrToDate = todatevalue.getText().toString().trim();
////                    mStoppageData = new ArrayList<StoppageReportDto>();
//                    mPrimaryEngineReportData.clear();
//                    lv.setAdapter(null);
//                    adapter.notifyDataSetChanged();
//                    new getPrimaryEngineOnData().execute();
//                } else {
//                    Toast.makeText(getApplicationContext(),
//                            "Please check your network connection",
//                            Toast.LENGTH_SHORT).show();
//                }
//
////                new CameraReportIamgeListActivtiy.getCameraReport().execute();
//                break;
//        }
//
//    }
//
//    private void showTimeDialog() {
//        if (isFromTime) {
//            hour = mFromHourValue;
//            minute = mFromMinuteValue;
////            System.out.println("Hi clicked hour minute value " + mFromHourValue + " " + mFromMinuteValue + " :: " + hour + " " + minute);
//        } else {
//            hour = mToHourValue;
//            minute = mToMinuteValue;
////            System.out.println("Hi clicked hour minute value " + mToHourValue + " " + mToMinuteValue + " :: " + hour + " " + minute);
//        }
//
//
////                hour=20;
////                minute=33;
//
//        TimePickerDialog dialog2 = new TimePickerDialog(this, timePickerListener, hour, minute,
//                false);
//        dialog2.show();
//    }
//
//    private void showDateDialog() {
//        if (isFromDate) {
//
//            String[] dateArray = mStartDate.split("-");
//            year = Integer.parseInt(dateArray[0].trim());
//            month = Integer.parseInt(dateArray[1].trim()) - 1;
//            day = Integer.parseInt(dateArray[2].trim());
//        } else {
//
//            String[] dateArray = mEndDate.split("-");
//            year = Integer.parseInt(dateArray[0].trim());
//            month = Integer.parseInt(dateArray[1].trim()) - 1;
//            day = Integer.parseInt(dateArray[2].trim());
//
//        }
////        System.out.println("Hi year " + year + " mo " + month + " da " + day);
////        final Calendar c = Calendar.getInstance();
////        year = c.get(Calendar.YEAR);
////        month = c.get(Calendar.MONTH);
////        day = c.get(Calendar.DAY_OF_MONTH);
//        /** set date picker as current date */
//        DatePickerDialog dialog = new DatePickerDialog(this,
//                datePickerListener, year, month, day);
//        dialog.show();
//    }
//
//
//    private class getPrimaryEngineOnData extends AsyncTask<String, String, String> {
//        ProgressDialog progressDialog;
//
//        @Override
//        protected String doInBackground(String... params) {
//            // TODO Auto-generated method stub
//
//            String result = null;
//            try {
//
//                String string_from_date = mStartDate.trim() + " " + mStartTime.trim();
//                String string_to_date = mEndDate.trim() + " " + mEndTime.trim();
//                SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss aa");
//                Date d = f.parse(string_from_date);
//                long fromMilliSeconds = d.getTime();
//                Date d1 = f.parse(string_to_date);
//                long toMilliSeconds = d1.getTime();
//                HttpConfig ht = new HttpConfig();
////                result = ht.httpGet(Const.API_URL + "/mobile/getAcReport?vehicleId=" + Constant.SELECTED_VEHICLE_ID + "&fromTimeUtc=" + fromMilliSeconds + "&toTimeUtc=" + toMilliSeconds + "&userId=" + Constant.SELECTED_USER_ID);
//
//
////                http://gpsvts.net/gps/public/getVehicleHistory?vehicleId=AP16TE5667&interval=&fromDateUTC=1527100200000&toDateUTC=1527234559000
//
//                result = ht.httpGet(Const.API_URL + "/mobile/getVehicleHistory?vehicleId=" + Constant.SELECTED_VEHICLE_ID + "&fromDateUTC=" + fromMilliSeconds + "&toDateUTC="
//                        + toMilliSeconds + "&userId=" + Constant.SELECTED_USER_ID);
//
////                result = ht.httpGet("http://gpsvts.net/mobile/getAcReport?userId=sainiji&vehicleId=UP16BT9281&fromTimeUtc=1511375400000&toTimeUtc=1511548200000");
//            } catch (Exception e) {
//                e.printStackTrace();
////                System.out.println("Hi exception is ::::" + e.toString());
//            }
//
//            return result;
//        }
//
//        @Override
//        protected void onPostExecute(String result) {
//            // TODO Auto-generated method stub
//            super.onPostExecute(result);
//            progressDialog.dismiss();
//            mPrimaryEngineReportData = new ArrayList<EngineReportDto>();
//            if (result != null && result.length() > 0) {
////                System.out.println("The Fuel fill result is ::::" + result);
//                boolean engineOnSelected = false;
//
//                JSONArray jArrayEngineReport = null, jArrResult = null;
//                try {
//
//
//                    JSONObject jsonObject = new JSONObject(result.trim());
//
//                    if (jsonObject.has("vehicleLocations")) {
//                        jArrayEngineReport = jsonObject.getJSONArray("vehicleLocations");
//                        ArrayList<AcReportDto> mSData = new ArrayList<AcReportDto>();
//                        for (int i = 0; i < jArrayEngineReport.length(); i++) {
//                            JSONObject jsonStoppageObject = jArrayEngineReport.getJSONObject(i);
//                            AcReportDto f = new AcReportDto();
//
//                            if (jsonStoppageObject.has("ignitionStatus")) {
//
//                                if (!engineOnSelected) {
//                                    if ((jsonStoppageObject.getString("ignitionStatus") != null) && (jsonStoppageObject.getString("ignitionStatus").trim().equalsIgnoreCase("ON"))) {
////                                        System.out.println("Vehicle busy yess:::" + jsonStoppageObject.getString("alarmType") + " : " + i);
//
//
//                                        if (jsonStoppageObject.has("latitude")) {
//                                            f.setStartLatitude(String.valueOf(jsonStoppageObject.getDouble("latitude")));
//                                        }
//                                        if (jsonStoppageObject.has("longitude")) {
//                                            f.setStartLongitude(String.valueOf(jsonStoppageObject.getDouble("longitude")));
//                                        }
//
//                                        if (jsonStoppageObject.has("date")) {
//                                            f.setStartTime(jsonStoppageObject.getLong("date"));
//                                        }
//                                        if (jsonStoppageObject.has("ignitionStatus")) {
//                                            f.setStartStatus(jsonStoppageObject.getString("ignitionStatus"));
//                                        }
//                                        if (jsonStoppageObject.has("address")) {
////                                            System.out.println("Hi adds st ::" + jsonStoppageObject.getString("address"));
//                                            f.setStartingAddress(jsonStoppageObject.getString("address"));
//
//                                        }
//                                        engineOnSelected = true;
////                                        System.out.println("Hi adds st 222::" + f.getStartingAddress());
//                                        mSData.add(f);
//
//                                    }
//                                } else {
//                                    if ((jsonStoppageObject.getString("ignitionStatus") != null) && (jsonStoppageObject.getString("ignitionStatus").trim().equalsIgnoreCase("OFF"))) {
////                                        System.out.println("Vehicle busy noo :::" + jsonStoppageObject.getString("alarmType") + " : " + i);
//
//
//                                        if (jsonStoppageObject.has("latitude")) {
//                                            f.setEndLatitude(String.valueOf(jsonStoppageObject.getDouble("latitude")));
//                                        }
//                                        if (jsonStoppageObject.has("longitude")) {
//                                            f.setEndLongitude(String.valueOf(jsonStoppageObject.getDouble("longitude")));
//                                        }
////
//                                        if (jsonStoppageObject.has("date")) {
//                                            f.setEndTime(jsonStoppageObject.getLong("date"));
//                                        }
//                                        if (jsonStoppageObject.has("ignitionStatus")) {
//                                            f.setEndStatus(jsonStoppageObject.getString("ignitionStatus"));
//                                        }
//
//
//                                        if (jsonStoppageObject.has("address")) {
////                                            System.out.println("Hi adds ed ::" + jsonStoppageObject.getString("address"));
//                                            f.setEndAddress(jsonStoppageObject.getString("address"));
//
//                                        }
//                                        engineOnSelected = false;
////                                        System.out.println("Hi adds ed 222::" + f.getEndAddress());
//                                        mSData.add(f);
//
//                                    }
//                                }
//                            }
//
//                        }
//
//                        ArrayList<EngineReportDto> mSData2 = new ArrayList<EngineReportDto>();
////                        System.out.println("Hi ac on off list size is :::" + mSData.size());
//                        for (int i = 0; i < mSData.size(); i = i + 2) {
//                            EngineReportDto ac = new EngineReportDto();
//                            if (i < mSData.size()) {
//
////                                System.out.println("Hi start adds 00 " + mSData.get(i).getStartingAddress());
//
//                                if (mSData.get(i).getStartStatus() != null && mSData.get(i).getStartStatus().equalsIgnoreCase("ON")) {
////                                    System.out.println("Hi adds st i ::" + mSData.get(i).getStartingAddress());
//                                    ac.setStartingAddress(mSData.get(i).getStartingAddress());
//                                    ac.setStartTime(mSData.get(i).getStartTime());
//                                    ac.setStartLatitude(mSData.get(i).getStartLatitude());
//                                    ac.setStartLongitude(mSData.get(i).getStartLongitude());
//                                    ac.setStartStatus("ON");
//                                }
//                            }
//
//                            if (i + 1 < mSData.size()) {
////                                System.out.println("Hi end adds 00 " + mSData.get(i + 1).getEndAddress());
//                                if (mSData.get(i + 1).getEndStatus() != null && mSData.get(i + 1).getEndStatus().equalsIgnoreCase("OFF")) {
////                                    System.out.println("Hi adds ed i+1::" + mSData.get(i).getEndAddress());
//                                    ac.setEndAddress(mSData.get(i + 1).getEndAddress());
//                                    ac.setEndTime(mSData.get(i + 1).getEndTime());
//                                    ac.setEndLatitude(mSData.get(i + 1).getEndLatitude());
//                                    ac.setEndLongitude(mSData.get(i + 1).getEndLongitude());
//                                    ac.setEndStatus("OFF");
//                                }
//                            }
//
////                            System.out.println("Hi time 000 st ::" + ac.getStartTime() + "  ed " + ac.getEndTime());
////                            System.out.println("Hi adds st 000 ::" + ac.getStartingAddress() + "  ed " + ac.getEndAddress());
//                            mSData2.add(ac);
//
//                        }
//
//                        mPrimaryEngineReportData = mSData2;
//                    }
//
////                    if (jsonObject.has("totalFuel")) {
////                        mTotalFuelValue = String.valueOf(jsonObject.getDouble("totalFuel"));
////                    }
//
//                    // }
//
//
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//                System.out.println("Hi ac on off list size is 22222:::" + mPrimaryEngineReportData.size());
//
//
//                if (mPrimaryEngineReportData != null && mPrimaryEngineReportData.size() > 1) {
//
//                    //mAdapter = new CameraImageAdapter(mCameraReportList, CameraReportIamgeListActivtiy.this);
//
////                    AcReportNewAdapter mAadp = new AcReportNewAdapter(mAcReportData, AcReportNewActivity.this);
////                    mRecyclerView.setAdapter(mAadp);
////                    mAadp.notifyDataSetChanged();
//
//                    lv.setVisibility(View.VISIBLE);
//                    mTxtNoRecord.setVisibility(View.GONE);
//                    setTableLayoutData();
//
//
//                } else {
////                    mRecyclerView.setAdapter(null);
////                    Toast.makeText(AcReportNewActivity.this, "No record found", Toast.LENGTH_SHORT).show();
//
//                    lv.setAdapter(null);
//                    lv.setVisibility(View.GONE);
//                    mTxtNoRecord.setVisibility(View.VISIBLE);
//
//                    //show date selection layout
//                }
//
////                if (mAcReportData.size() > 0) {
//////                    lv.setVisibility(View.VISIBLE);
//////                    mTxtNoRecord.setVisibility(View.GONE);
//////                    setTableLayoutData();
////                } else {
//////                    lv.setAdapter(null);
//////                    lv.setVisibility(View.GONE);
//////                    mTxtNoRecord.setVisibility(View.VISIBLE);
////                }
//            }
//
//        }
//
//        @Override
//        protected void onPreExecute() {
//            // TODO Auto-generated method stub
//            super.onPreExecute();
//            progressDialog = new ProgressDialog(PrimaryEngineOnReportActivity.this,
//                    AlertDialog.THEME_HOLO_LIGHT);
//            progressDialog.setMessage("Please Wait...");
//            progressDialog.setProgressDrawable(new ColorDrawable(
//                    Color.BLUE));
//            progressDialog.setCancelable(true);
//            progressDialog.setCanceledOnTouchOutside(false);
//            progressDialog.show();
//        }
//
//    }
//
//    public void setTableLayoutData() {
////        System.out.println("Hi set table data called :::" + mStoppageData.size());
////        adapter.setHeader(mHeader);
////        ListView listView = (ListView) findViewById(R.id.executive_report_listView1);
//
//        if (isHeaderPresent) {
//
//        } else {
//            lv.addHeaderView(adapter.getHeaderView(lv));
//            isHeaderPresent = true;
//        }
//
//
//        lv.setAdapter(adapter);
//        adapter.setData(mPrimaryEngineReportData);
//
//        adapter.notifyDataSetChanged();
//
////        mTxtCumulativeFuelValue.setText(mTotalFuelValue + " ltrs");
//
////        parseVehicleKms();
//
//    }
//
//
//    private void setBottomLayoutData() {
//        mTxtFromDate.setText(mStartDate);
//        mTxtEndDate.setText(mEndDate);
//        mTxtFromTime.setText(mStartTimeValue);
//        mTxtEndTime.setText(mEndTimeValue);
//
//
//    }
//
//
//    private DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {
//
//        // when dialog box is closed, below method will be called.
//        public void onDateSet(DatePicker view, int selectedYear,
//                              int selectedMonth, int selectedDay) {
//
//            year = selectedYear;
//            month = selectedMonth;
//            day = selectedDay;
//            StringBuilder mDate = new StringBuilder().append(year).append(":")
//                    .append(getMonthValue(month + 1)).append(":").append(getMonthValue(day))
//                    .append("");
//
//            StringBuilder mDate2 = new StringBuilder().append(year).append("-")
//                    .append(getMonthValue(month + 1)).append("-").append(getMonthValue(day))
//                    .append(" ");
//            if (isFromDate) {
//                mStartDate = String.valueOf(mDate2);
//                //  mStartTimeValue=String.valueOf(mDate2);
//                mTxtFromDate.setText(mDate2);
//            } else {
//                mEndDate = String.valueOf(mDate2);
//
//                mTxtEndDate.setText(mDate2);
//            }
////            mFromDate = String.valueOf(mDate);
////            mFromDateTxtValue = String.valueOf(mDate2);
////            mTxtDate.setText(mDate2);
//            // }
//        }
//    };
//
//    private String getMonthValue(int month) {
//        String mMonthValue = String.valueOf(month);
//        if (mMonthValue.length() == 1) {
//            mMonthValue = "0" + mMonthValue;
//        }
//        return mMonthValue;
//    }
//
//
//    @Override
//    protected Dialog onCreateDialog(int id) {
//        switch (id) {
//            case TIME_DIALOG_ID:
//
//                // set time picker as current time
//
//                if (isFromTime) {
//                    hour = mFromHourValue;
//                    minute = mFromMinuteValue;
////                    System.out.println("Hi clicked hour minute value " + mFromHourValue + " " + mFromMinuteValue + " :: " + hour + " " + minute);
//                } else {
//                    hour = mToHourValue;
//                    minute = mToMinuteValue;
////                    System.out.println("Hi clicked hour minute value " + mToHourValue + " " + mToMinuteValue + " :: " + hour + " " + minute);
//                }
//
//
////                hour=20;
////                minute=33;
//
//                return new TimePickerDialog(this, timePickerListener, hour, minute,
//                        false);
//
//            case DATE_DIALOG_ID:
//
//                if (isFromDate) {
//
//                    String[] dateArray = mStartDate.split("-");
//                    year = Integer.parseInt(dateArray[0]);
//                    month = Integer.parseInt(dateArray[0]);
//                    day = Integer.parseInt(dateArray[0]);
//                } else {
//
//                    String[] dateArray = mEndDate.split("-");
//                    year = Integer.parseInt(dateArray[0]);
//                    month = Integer.parseInt(dateArray[0]);
//                    day = Integer.parseInt(dateArray[0]);
//
//                }
//
//                final Calendar c = Calendar.getInstance();
//                year = c.get(Calendar.YEAR);
//                month = c.get(Calendar.MONTH);
//                day = c.get(Calendar.DAY_OF_MONTH);
//                /** set date picker as current date */
//                DatePickerDialog dialog = new DatePickerDialog(this,
//                        datePickerListener, year, month, day);
//                // set time picker as current time
//                return dialog;
//
//        }
//        return null;
//    }
//
//    private TimePickerDialog.OnTimeSetListener timePickerListener = new TimePickerDialog.OnTimeSetListener() {
//
//
//        @Override
//        public void onTimeSet(TimePicker view, int hourOfDay, int minutes) {
//            // TODO Auto-generated method stub
//            hour = hourOfDay;
//            minute = minutes;
//
//            updateTime(hour, minute);
//
//
//        }
//
//    };
//
//
//    private void updateTime(int hours, int mins) {
//
////        String timeSet = "", timeSet2 = "";
////        if (hours > 12) {
////            hours -= 12;
////            timeSet = "PM";
////            timeSet2 = "p.m.";
////        } else if (hours == 0) {
////            hours += 12;
////            timeSet = "AM";
////            timeSet2 = "a.m.";
////        } else if (hours == 12) {
////            timeSet = "PM";
////            timeSet2 = "p.m.";
////        } else {
////            timeSet = "AM";
////            timeSet2 = "a.m.";
////        }
////
////
////        String minutes = "";
////        if (mins < 10)
////            minutes = "0" + mins;
////        else
////            minutes = String.valueOf(mins);
//
//// Append in a StringBuilder
//
//
//        Calendar cal = Calendar.getInstance();
//        cal.set(Calendar.HOUR_OF_DAY, hours);
//        cal.set(Calendar.MINUTE, mins);
//        cal.set(Calendar.SECOND, 0);
//        cal.set(Calendar.MILLISECOND, 0);
////        System.out.println("Hi date is 222:::" + timeFormat.format(cal.getTime()));
//
////        String aTime = new StringBuilder().append(hours).append(':')
////                .append(minutes).append(" ").append(timeSet).toString();
//        String aTime = timeFormatShow.format(cal.getTime());
////        String aTime2 = new StringBuilder().append(hours).append(':')
////                .append(minutes).append(":00").append(minutes).append(" ").append(timeSet2).toString();
//
//        String aTime2 = timeFormat.format(cal.getTime());
//
//        SimpleDateFormat hourFormat = new SimpleDateFormat(
//                "HH");
//        SimpleDateFormat minuteFormat = new SimpleDateFormat(
//                "mm");
//
//        if (isFromTime) {
//            mStartTime = aTime2;
//            mStartTimeValue = aTime;
//
//
//            mFromHourValue = Integer.valueOf(hourFormat.format(cal.getTime()));
//            mFromMinuteValue = Integer.valueOf(minuteFormat.format(cal.getTime()));
//            mTxtFromTime.setText(mStartTimeValue);
////            System.out.println("Hi from time value " + mFromHourValue + " " + mFromMinuteValue);
//        } else {
//            mEndTime = aTime2;
//            mEndTimeValue = aTime;
////            mToHourValue = cal.HOUR_OF_DAY;
////            mToMinuteValue = cal.MINUTE;
//            mTxtEndTime.setText(mEndTimeValue);
//
////            SimpleDateFormat hourFormat = new SimpleDateFormat(
////                    "hh");
////            SimpleDateFormat minuteFormat = new SimpleDateFormat(
////                    "mm");
//
//            mToHourValue = Integer.valueOf(hourFormat.format(cal.getTime()));
//            mToMinuteValue = Integer.valueOf(minuteFormat.format(cal.getTime()));
//
////            System.out.println("Hi to time value " + mToHourValue + " " + mToMinuteValue);
//        }
//
//
//    }
//
//
//    public class TableAdapter extends BaseAdapter {
//
//        // private static final String TAG = "TableAdapter";
//
//        private Context mContext;
//
//        private String[] mHeader;
//
//
//        private int mCurrentScroll;
//
//        private int[] mColResources = {R.id.engine_report_datetime_textView, R.id.engine_report_end_datetime_textView, R.id.engine_report_status_textView, R.id.engine_report_end_status_textView,
//                R.id.engine_report_duration_textView, R.id.engine_report_nearest_location_textView, R.id.engine_report_nearest_end_location_textView, R.id.engine_report_gmap_textView, R.id.engine_report_end_gmap_textView};
//
//        public TableAdapter(Context context) {
//            super();
//            mContext = context;
//        }
//
//        @Override
//        public int getCount() {
//            return mPrimaryEngineReportData != null ? mPrimaryEngineReportData.size() : 0;
//        }
//
//        @Override
//        public Object getItem(int position) {
//            return mPrimaryEngineReportData.get(position);
//        }
//
//        @Override
//        public long getItemId(int position) {
//            return position;
//        }
//
//        @Override
//        public View getView(final int position, View convertView, ViewGroup parent) {
//
//            HorizontalScrollView view = null;
//
//            if (convertView == null) {
//                LayoutInflater inflater = (LayoutInflater) mContext
//                        .getSystemService(LAYOUT_INFLATER_SERVICE);
//                view = (HorizontalScrollView) inflater.inflate(
//                        R.layout.engine_report_list_item_table_row, parent, false);
//
////                view.setOnScrollChangeListener(new OnScrollListener() {
////                    @Override
////                    public void onScrollChanged(View view, int scrollX) {
////
////                    }
////                });
//
//
//                view.setOnScrollListener(new HorizontalScrollView.OnScrollListener() {
//
//                    @Override
//                    public void onScrollChanged(View scrollView, int scrollX) {
//
//                        mCurrentScroll = scrollX;
//                        ListView listView = (ListView) scrollView.getParent();
//                        if (listView == null)
//                            return;
//
//                        for (int i = 0; i < listView.getChildCount(); i++) {
//                            View child = listView.getChildAt(i);
//                            if (child instanceof HorizontalScrollView
//                                    && child != scrollView) {
//                                HorizontalScrollView scrollView2 = (HorizontalScrollView) child;
//                                if (scrollView2.getScrollX() != mCurrentScroll) {
//                                    scrollView2.setScrollX(mCurrentScroll);
//                                }
//                            }
//                        }
//                    }
//                });
//
//            } else {
//                view = (HorizontalScrollView) convertView;
//            }
//
//            view.setScrollX(mCurrentScroll);
//
//            if (position % 2 == 0) {
//                view.setBackgroundColor(Color.WHITE);
//            } else {
//                view.setBackgroundColor(Color.LTGRAY);
//            }
//
//            EngineReportDto data = mPrimaryEngineReportData.get(position);
//
////            for (int i = 0; i < mColResources.length; i++) {
//            TextView col0 = (TextView) view.findViewById(mColResources[0]);
//            if (data.getStartTime() > 0) {
//                col0.setText(Const.getAcReportTime((data.getStartTime())));
//            }
//
//            TextView col1 = (TextView) view.findViewById(mColResources[1]);
//            if (data.getEndTime() > 0) {
//                col1.setText(Const.getAcReportTime((data.getEndTime())));
//            }
//
//            TextView col2 = (TextView) view.findViewById(mColResources[2]);
//            if (data.getStartStatus() != null) {
//                col2.setText(data.getStartStatus());
//            }
//
//            TextView col3 = (TextView) view.findViewById(mColResources[3]);
//            if (data.getEndStatus() != null) {
//                col3.setText(data.getEndStatus());
//            }
//
//            TextView col4 = (TextView) view.findViewById(mColResources[4]);
//            long timevalue = 0;
//            if(data.getEndTime()>0 && data.getStartTime()>0) {
//                if (data.getEndTime() > data.getStartTime()) {
//                    timevalue = data.getEndTime() - data.getStartTime();
//                }
//            }else{
//                timevalue=System.currentTimeMillis()-data.getStartTime();
//            }
//            col4.setText(Const.getreportTimeValue(timevalue));
//
//
////            if (data.getDuration() > 0) {
////                col4.setText(Const.getreportTimeValue(data.getDuration()));
////            }
//
//
//            TextView col5 = (TextView) view.findViewById(mColResources[5]);
//            if (data.getStartingAddress() != null) {
//                col5.setText(data.getStartingAddress());
//            }
//            TextView col6 = (TextView) view.findViewById(mColResources[6]);
//            if (data.getEndAddress() != null) {
//                col6.setText(data.getEndAddress());
//            }
//
////            TextView col1 = (TextView) view.findViewById(mColResources[1]);
////            long timevalue = 0;
////            if (data.getEndTime() > data.getStartTime()) {
////                timevalue = data.getEndTime() - data.getStartTime();
////            }
////            col1.setText(Const.getreportTimeValue(timevalue));
//
//
//            TextView col7 = (TextView) view.findViewById(mColResources[7]);
//            TextView col8 = (TextView) view.findViewById(mColResources[8]);
//
//
//            SpannableString content = new SpannableString("G-Map");
//            content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
//            col7.setText(content);
//            col8.setText(content);
//
//            LinearLayout.LayoutParams vehicleNameParams = new LinearLayout.LayoutParams(
//                    LinearLayout.LayoutParams.WRAP_CONTENT,
//                    LinearLayout.LayoutParams.WRAP_CONTENT);
//            vehicleNameParams.width = width * 35 / 100;
////            vehicleNameParams.height = height * 9 / 100;
//            col0.setLayoutParams(vehicleNameParams);
//            col0.setPadding(width * 2 / 100, 0, 0, 0);
//            col0.setGravity(Gravity.CENTER | Gravity.LEFT);
//
//            col1.setLayoutParams(vehicleNameParams);
//            col1.setPadding(width * 2 / 100, 0, 0, 0);
//            col1.setGravity(Gravity.CENTER | Gravity.LEFT);
//
//
//            LinearLayout.LayoutParams text3Params = new LinearLayout.LayoutParams(
//                    LinearLayout.LayoutParams.WRAP_CONTENT,
//                    LinearLayout.LayoutParams.WRAP_CONTENT);
//            text3Params.width = width * 30 / 100;
////            text3Params.height = height * 9 / 100;
//
//            col2.setLayoutParams(text3Params);
//            //   col3.setWidth(width * 75 / 100);
//            col2.setPadding(width * 2 / 100, 0, 0, 0);
//            col2.setGravity(Gravity.CENTER | Gravity.LEFT);
//
//            col3.setLayoutParams(text3Params);
//            //   col3.setWidth(width * 75 / 100);
//            col3.setPadding(width * 2 / 100, 0, 0, 0);
//            col3.setGravity(Gravity.CENTER | Gravity.LEFT);
//
//            col4.setLayoutParams(text3Params);
//            //  col3.setWidth(width * 75 / 100);
//            col4.setPadding(width * 2 / 100, 0, 0, 0);
//            col4.setGravity(Gravity.CENTER | Gravity.LEFT);
//
//            col7.setLayoutParams(text3Params);
//            //   col3.setWidth(width * 75 / 100);
//            col7.setPadding(width * 2 / 100, 0, 0, 0);
//            col7.setGravity(Gravity.CENTER);
//
//            col8.setLayoutParams(text3Params);
//            //   col3.setWidth(width * 75 / 100);
//            col8.setPadding(width * 2 / 100, 0, 0, 0);
//            col8.setGravity(Gravity.CENTER);
//
//
//            LinearLayout.LayoutParams text2Params = new LinearLayout.LayoutParams(
//                    LinearLayout.LayoutParams.WRAP_CONTENT,
//                    LinearLayout.LayoutParams.WRAP_CONTENT);
//            text2Params.width = width * 60 / 100;
////            text2Params.height = height * 9 / 100;
//            col5.setLayoutParams(text2Params);
//            col5.setPadding(width * 2 / 100, 0, 0, 0);
//            col5.setGravity(Gravity.CENTER | Gravity.LEFT);
//
//            col6.setLayoutParams(text2Params);
//            col6.setPadding(width * 2 / 100, 0, 0, 0);
//            col6.setGravity(Gravity.CENTER | Gravity.LEFT);
//
//
//            col7.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//
//                    MenuItem item = menu.findItem(R.id.action_map_close);
//                    item.setVisible(true);
//
//                    //  mImgMapViewClose.setVisibility(View.VISIBLE);
//                    mAcReportDataMapLayout.setVisibility(View.VISIBLE);
////                    if (isMapPresent) {
//                    isMapPresent = true;
//                    lv.setVisibility(View.GONE);
////                    }
//
//                    lv.setVisibility(View.GONE);
//                    mLatitude = Double.valueOf(mPrimaryEngineReportData.get(position).getStartLatitude());
//                    mLongitude = Double.valueOf(mPrimaryEngineReportData.get(position).getStartLongitude());
////                    setupMap(mAcReportData.get(position).getStartingAddress());
//
//                    if (isOsmEnabled) {
//                        setupOsmMap(mPrimaryEngineReportData.get(position).getStartingAddress());
//                    } else {
//                        setupGoogleMap(mPrimaryEngineReportData.get(position).getStartingAddress());
//                    }
//
//                }
//            });
//
//            col8.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
////                    mImgMapViewClose.setVisibility(View.VISIBLE);
//
//                    MenuItem item = menu.findItem(R.id.action_map_close);
//                    item.setVisible(true);
//
//                    mAcReportDataMapLayout.setVisibility(View.VISIBLE);
////                    if (isMapPresent) {
//                    isMapPresent = true;
//                    lv.setVisibility(View.GONE);
////                    }
//
//                    lv.setVisibility(View.GONE);
//                    mLatitude = Double.valueOf(mPrimaryEngineReportData.get(position).getEndLatitude());
//                    mLongitude = Double.valueOf(mPrimaryEngineReportData.get(position).getEndLongitude());
//
//                    if (isOsmEnabled) {
//                        setupOsmMap(mPrimaryEngineReportData.get(position).getEndAddress());
//                    } else {
//                        setupGoogleMap(mPrimaryEngineReportData.get(position).getEndAddress());
//                    }
//
////                    setupMap(mAcReportData.get(position).getEndAddress());
//                }
//            });
//
//            View view1 = (View) view.findViewById(R.id.view1);
//            View view2 = (View) view.findViewById(R.id.view2);
//            View view3 = (View) view.findViewById(R.id.view3);
//            View view4 = (View) view.findViewById(R.id.view4);
//
//            if (mPrimaryEngineReportData.get(position).getEndAddress() != null) {
//                col1.setVisibility(View.VISIBLE);
//                col3.setVisibility(View.VISIBLE);
//                col6.setVisibility(View.VISIBLE);
//
//                view1.setVisibility(View.VISIBLE);
//                view2.setVisibility(View.VISIBLE);
//                view3.setVisibility(View.VISIBLE);
////                view4.setVisibility(View.VISIBLE);
//            } else {
//                col1.setVisibility(View.GONE);
//                col3.setVisibility(View.GONE);
//                col6.setVisibility(View.GONE);
//
//                view1.setVisibility(View.GONE);
//                view2.setVisibility(View.GONE);
//                view3.setVisibility(View.GONE);
//            }
//
//            if (mPrimaryEngineReportData.get(position).getEndLatitude() != null && mPrimaryEngineReportData.get(position).getEndLongitude() != null) {
//                col8.setVisibility(View.VISIBLE);
//                view4.setVisibility(View.VISIBLE);
//            } else {
//                col8.setVisibility(View.GONE);
//                view4.setVisibility(View.GONE);
//            }
//
////            }
//
//            return view;
//        }
//
//        public View getHeaderView(ViewGroup parent) {
//
//            LayoutInflater inflater = (LayoutInflater) mContext
//                    .getSystemService(LAYOUT_INFLATER_SERVICE);
//            HorizontalScrollView view = (HorizontalScrollView) inflater
//                    .inflate(R.layout.engine_report_list_item_table_header, parent, false);
//
////            for (int i = 0; i < mColResources.length; i++) {
////                TextView col = (TextView) view.findViewById(mColResources[i]);
////                col.setText(mHeader[i]);
////            }
//
//
//            view.setOnScrollListener(new HorizontalScrollView.OnScrollListener() {
//
//                @Override
//                public void onScrollChanged(View scrollView, int scrollX) {
//
//                    mCurrentScroll = scrollX;
//                    ListView listView = (ListView) scrollView.getParent();
//                    if (listView == null)
//                        return;
//
//                    for (int i = 0; i < listView.getChildCount(); i++) {
//                        View child = listView.getChildAt(i);
//                        if (child instanceof HorizontalScrollView
//                                && child != scrollView) {
//                            HorizontalScrollView scrollView2 = (HorizontalScrollView) child;
//                            if (scrollView2.getScrollX() != mCurrentScroll) {
//                                scrollView2.setScrollX(mCurrentScroll);
//                            }
//                        }
//                    }
//                }
//            });
//
//
//            TextView col1 = (TextView) view.findViewById(R.id.engine_report_datetime_textView_header);
//            TextView col2 = (TextView) view.findViewById(R.id.engine_report_status_textView_header);
//            TextView col3 = (TextView) view.findViewById(R.id.engine_report_duration_textView_header);
//            TextView col4 = (TextView) view.findViewById(R.id.engine_report_nearest_location_textView_header);
//            TextView col5 = (TextView) view.findViewById(R.id.engine_report_gmap_textView_header);
//
//
//            LinearLayout.LayoutParams vehicleNameParams = new LinearLayout.LayoutParams(
//                    LinearLayout.LayoutParams.WRAP_CONTENT,
//                    LinearLayout.LayoutParams.WRAP_CONTENT);
//            vehicleNameParams.width = width * 35 / 100;
//
//            col1.setLayoutParams(vehicleNameParams);
//            col1.setPadding(width * 2 / 100, 0, 0, 0);
//            col1.setGravity(Gravity.CENTER | Gravity.LEFT);
//
//
//            LinearLayout.LayoutParams text3Params = new LinearLayout.LayoutParams(
//                    LinearLayout.LayoutParams.WRAP_CONTENT,
//                    LinearLayout.LayoutParams.WRAP_CONTENT);
//            text3Params.width = width * 30 / 100;
////            text3Params.height = height * 9 / 100;
//
//            col2.setLayoutParams(text3Params);
//            //   col3.setWidth(width * 75 / 100);
//            col2.setPadding(width * 2 / 100, 0, 0, 0);
//            col2.setGravity(Gravity.CENTER | Gravity.LEFT);
//
//            col3.setLayoutParams(text3Params);
//            //   col3.setWidth(width * 75 / 100);
//            col3.setPadding(width * 2 / 100, 0, 0, 0);
//            col3.setGravity(Gravity.CENTER | Gravity.LEFT);
//
//
//            col5.setLayoutParams(text3Params);
//            //   col3.setWidth(width * 75 / 100);
//            col5.setPadding(width * 2 / 100, 0, 0, 0);
//            col5.setGravity(Gravity.CENTER | Gravity.LEFT);
//
//
//            LinearLayout.LayoutParams text2Params = new LinearLayout.LayoutParams(
//                    LinearLayout.LayoutParams.WRAP_CONTENT,
//                    LinearLayout.LayoutParams.WRAP_CONTENT);
//            text2Params.width = width * 60 / 100;
////            text2Params.height = height * 9 / 100;
//            col4.setLayoutParams(text2Params);
//            col4.setPadding(width * 2 / 100, 0, 0, 0);
//            col4.setGravity(Gravity.CENTER | Gravity.LEFT);
//
//
//            return view;
//        }
//
////        public void setHeader(String[] header) {
////            mHeader = header;
////        }
//
//        public void setData(ArrayList<EngineReportDto> data) {
//            mPrimaryEngineReportData = data;
//            notifyDataSetChanged();
//        }
//
//    }
//
//    private void opptionPopUp() {
//        // TODO Auto-generated method stub
//        final Dialog dialog = new Dialog(PrimaryEngineOnReportActivity.this);
//        dialog.getWindow();
//        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        dialog.setContentView(R.layout.radio_popup);
//        dialog.show();
//
//        RadioGroup rg_home = (RadioGroup) dialog.findViewById(R.id.rg_home_views);
//        RadioGroup rg_history = (RadioGroup) dialog.findViewById(R.id.rg_history_views);
//        rg_history.clearCheck();
//        rg_history.setVisibility(View.GONE);
//        rg_home.setVisibility(View.VISIBLE);
//
//        RadioButton $Normal = (RadioButton) dialog
//                .findViewById(R.id.rb_home_normal);
//        RadioButton $Satelite = (RadioButton) dialog
//                .findViewById(R.id.rb_home_satellite);
//        RadioButton $Terrain = (RadioButton) dialog
//                .findViewById(R.id.rb_home_terrain);
//        RadioButton $Hybrid = (RadioButton) dialog
//                .findViewById(R.id.rb_home_hybrid);
//
//
//        if (mSELECTED_MAP_TYPE.equalsIgnoreCase("Normal")) {
//            $Normal.setChecked(true);
//            $Satelite.setChecked(false);
//            $Terrain.setChecked(false);
//            $Hybrid.setChecked(false);
//        } else if (mSELECTED_MAP_TYPE.equalsIgnoreCase("Satelite")) {
//            $Normal.setChecked(false);
//            $Satelite.setChecked(true);
//            $Terrain.setChecked(false);
//            $Hybrid.setChecked(false);
//        } else if (mSELECTED_MAP_TYPE.equalsIgnoreCase("Terrain")) {
//            $Normal.setChecked(false);
//            $Satelite.setChecked(false);
//            $Terrain.setChecked(true);
//            $Hybrid.setChecked(false);
//        } else if (mSELECTED_MAP_TYPE.equalsIgnoreCase("Hybrid")) {
//            $Normal.setChecked(false);
//            $Satelite.setChecked(false);
//            $Terrain.setChecked(false);
//            $Hybrid.setChecked(true);
//        }
//        $Normal.setOnClickListener(new View.OnClickListener() {
//
//            @Override
//            public void onClick(View v) {
//                // TODO Auto-generated method stub
//
////                mapview.setTileSource(TileSourceFactory.MAPNIK);
//
//                map.setMapType(GoogleMap.MAP_TYPE_NORMAL);
//                mSELECTED_MAP_TYPE = "Normal";
//                dialog.dismiss();
//            }
//        });
//        $Satelite.setOnClickListener(new View.OnClickListener() {
//
//            @Override
//            public void onClick(View v) {
//                // TODO Auto-generated method stub
//                mSELECTED_MAP_TYPE = "Satelite";
//                map.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
////                mapview.setTileSource(TileSourceFactory.USGS_SAT);
//                dialog.dismiss();
//            }
//        });
//        $Terrain.setOnClickListener(new View.OnClickListener() {
//
//            @Override
//            public void onClick(View v) {
//                // TODO Auto-generated method stub
//                mSELECTED_MAP_TYPE = "Terrain";
//                map.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
////                mapview.setTileSource(TileSourceFactory.HIKEBIKEMAP);
//                dialog.dismiss();
//            }
//        });
//
//        $Hybrid.setOnClickListener(new View.OnClickListener() {
//
//            @Override
//            public void onClick(View v) {
//                // TODO Auto-generated method stub
//                mSELECTED_MAP_TYPE = "Hybrid";
//                map.setMapType(GoogleMap.MAP_TYPE_HYBRID);
////                mapview.setTileSource(TileSourceFactory.USGS_TOPO);
//                dialog.dismiss();
//            }
//        });
//
//        DisplayMetrics displayMetrics = new DisplayMetrics();
//        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
//        height = displayMetrics.heightPixels;
//        width = displayMetrics.widthPixels;
//
//        LinearLayout.LayoutParams radioParama = new LinearLayout.LayoutParams(
//                FrameLayout.LayoutParams.WRAP_CONTENT,
//                FrameLayout.LayoutParams.WRAP_CONTENT);
//        radioParama.width = width * 50 / 100;
//        radioParama.height = width * 10 / 100;
//        radioParama.topMargin = height * 4 / 100;
//        radioParama.gravity = Gravity.CENTER;
//        radioParama.leftMargin = height * 4 / 100;
//        $Normal.setLayoutParams(radioParama);
//        $Satelite.setLayoutParams(radioParama);
//        $Terrain.setLayoutParams(radioParama);
//
//        LinearLayout.LayoutParams radioterrainParama = new LinearLayout.LayoutParams(
//                FrameLayout.LayoutParams.WRAP_CONTENT,
//                FrameLayout.LayoutParams.WRAP_CONTENT);
//        radioterrainParama.width = width * 50 / 100;
//        radioterrainParama.height = width * 10 / 100;
//        radioterrainParama.topMargin = height * 4 / 100;
//        radioterrainParama.gravity = Gravity.CENTER;
//        radioterrainParama.leftMargin = height * 4 / 100;
//        radioterrainParama.bottomMargin = height * 4 / 100;
//        $Hybrid.setLayoutParams(radioterrainParama);
//
//        if (width >= 600) {
//            $Normal.setTextSize(16);
//            $Satelite.setTextSize(16);
//            $Terrain.setTextSize(16);
//            $Hybrid.setTextSize(16);
//        } else if (width > 501 && width < 600) {
//            $Normal.setTextSize(15);
//            $Satelite.setTextSize(15);
//            $Terrain.setTextSize(15);
//            $Hybrid.setTextSize(15);
//        } else if (width > 260 && width < 500) {
//            $Normal.setTextSize(14);
//            $Satelite.setTextSize(14);
//            $Terrain.setTextSize(14);
//            $Hybrid.setTextSize(14);
//        } else if (width <= 260) {
//            $Normal.setTextSize(13);
//            $Satelite.setTextSize(13);
//            $Terrain.setTextSize(13);
//            $Hybrid.setTextSize(13);
//        }
//    }
//
//    public void setupOsmMap(final String addressValue) {
//
////        if (map != null) {
////            map.clear();
////        }
//
//        if (mapview != null) {
//            InfoWindow.closeAllInfoWindowsOn(mapview);
//            mapview.getOverlays().clear();
//            mapview.invalidate();
//            mapview.getOverlays().add(Constant.getTilesOverlay(getApplicationContext()));
//        }
//
////        View marker = null;
////
////        marker = ((LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.custommarker, null);
////        ImageView id_custom_marker_icon = (ImageView) marker.findViewById(R.id.id_custom_marker_icon);
////        id_custom_marker_icon.setImageResource(R.drawable.grey_custom_marker_icon);
////        ImageView id_vehicle_in_marker = (ImageView) marker.findViewById(R.id.id_vehicle_in_marker);
////        id_vehicle_in_marker.setVisibility(View.GONE);
////
////        CameraPosition INIT = new CameraPosition.Builder().target(new LatLng(mLatitude, mLongitude)).zoom(vehicle_zoom_level).build();
////        map.animateCamera(CameraUpdateFactory.newCameraPosition(INIT));
////
////
////        map.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
////
////            // Use default InfoWindow frame
////            @Override
////            public View getInfoWindow(Marker arg0) {
////                return null;
////            }
////
////            // Defines the contents of the InfoWindow
////            @Override
////            public View getInfoContents(Marker arg0) {
////
////                // Getting view from the layout file info_window_layout
////                View v = getLayoutInflater().inflate(R.layout.map_info_txt_layout, null);
////
////                // Getting the position from the marker
////                LatLng latLng = arg0.getPosition();
////
////                DisplayMetrics displayMetrics = new DisplayMetrics();
////                getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
////                height = displayMetrics.heightPixels;
////                width = displayMetrics.widthPixels;
////                LinearLayout layout = (LinearLayout) v.findViewById(R.id.map_info_layout);
////                LinearLayout.LayoutParams backImageParams = new LinearLayout.LayoutParams(
////                        LinearLayout.LayoutParams.WRAP_CONTENT,
////                        LinearLayout.LayoutParams.WRAP_CONTENT);
////                backImageParams.width = width * 80 / 100;
////                //backImageParams.height = height * 10 / 100;
////                backImageParams.gravity = Gravity.CENTER;
////                layout.setLayoutParams(backImageParams);
////
////                // Getting reference to the TextView to set latitude
////                TextView txtContent = (TextView) v.findViewById(R.id.tv_txt_content);
////                txtContent.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
////
//////                new GetAddressTask(txtContent).execute(mLatitude, mLongitude, 1.0);
////
////
////                StringBuffer addr = new StringBuffer();
////                Geocoder geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());
////                List<Address> addresses = null;
////                try {
////                    addresses = geocoder.getFromLocation(mLatitude, mLongitude, 1);
////                } catch (IOException e1) {
////                    e1.printStackTrace();
////                } catch (IllegalArgumentException e2) {
////                    // Error message to post in the log
//////                        String errorString = "Illegal arguments " +
//////                                Double.toString(params[0]) +
//////                                " , " +
//////                                Double.toString(params[1]) +
//////                                " passed to address service";
//////                        e2.printStackTrace();
//////                        return errorString;
////                } catch (NullPointerException np) {
////                    // TODO Auto-generated catch block
////                    np.printStackTrace();
////                }
////                // If the reverse geocode returned an address
////                if (addresses != null && addresses.size() > 0) {
////                    // Get the first address
////                    Address address = addresses.get(0);
////                /*
////                 * Format the first line of address (if available),
////                 * city, and country name.
////                 */
////                    String addressText = null;
////
////                    for (int i = 0; i < address.getMaxAddressLineIndex(); i++) {
////                        addressText = address.getAddressLine(i);
////                        addr.append(addressText + ",");
////                    }
////                    // Return the text
////                    // return addr.toString();
////                } else {
////                    addr.append("No address found");
////                }
////
////                // txtContent.setText(mSelectedMsg);
//////                txtContent.setText(Constant.SELECTED_VEHICLE_SHORT_NAME + " " + addr);
////                txtContent.setText(Constant.SELECTED_VEHICLE_SHORT_NAME + " \n" + addressValue);
////                //  txtContent.setVisibility(View.GONE);
////
////                return v;
////
////            }
////        });
//
//
////        MarkerOptions markerOption = new MarkerOptions().position(new LatLng(mLatitude, mLongitude));
////        // markerOption.title(mSelectedMsg);
////        // markerOption.icon(BitmapDescriptorFactory.fromBitmap(createDrawableFromView(NotificationListActivity.this, marker)));
////
////
////        markerOption.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE));
////        Marker currentMarker = map.addMarker(markerOption);
////        currentMarker.showInfoWindow();
//
//
//        View markerView = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.marker_image_view, null);
//        ImageView mImgMarkerIcon = (ImageView) markerView.findViewById(R.id.img_marker_icon);
//        mImgMarkerIcon.setImageResource(R.drawable.green_custom_marker_icon);
////        animateMarker.setIcon(createDrawableFromViewNew(AcReport.this, markerView));
//        org.osmdroid.views.overlay.Marker osmMarker = new org.osmdroid.views.overlay.Marker(mapview);
//        osmMarker.setPosition(new GeoPoint(mLatitude, mLongitude));
//        osmMarker.setAnchor(org.osmdroid.views.overlay.Marker.ANCHOR_CENTER, org.osmdroid.views.overlay.Marker.ANCHOR_BOTTOM);
////        osmMarker.setTitle(Constant.SELECTED_VEHICLE_SHORT_NAME);
//        osmMarker.setIcon(Constant.createDrawableFromViewNew(PrimaryEngineOnReportActivity.this, markerView));
//        InfoWindow infoWindow = new MyInfoWindow(R.layout.map_info_txt_layout, mapview, addressValue);
//        osmMarker.setInfoWindow(infoWindow);
//        osmMarker.showInfoWindow();
//
//        final GeoPoint newPos = new GeoPoint(mLatitude, mLongitude);
////        mapController.setCenter(newPos);
//
//        new Handler(Looper.getMainLooper()).post(
//                new Runnable() {
//                    public void run() {
//                        mapview.getController().setCenter(newPos);
//                    }
//                }
//        );
//
//        mapview.getOverlays().add(osmMarker);
//        mapview.invalidate();
//
//
////        Marker marker = map.addMarker(new MarkerOptions()
////                .position(new LatLng(mLatitude, mLongitude))
////                .title(mSelectedMsg)
////                        // .snippet("Snippet")
////                .icon(BitmapDescriptorFactory
////                        .fromResource(R.drawable.grey_custom_marker_icon)));
////
////        marker.showInfoWindow();
//
//
//    }
//
//
//    public void setupGoogleMap(final String addressValue) {
//
//        if (map != null) {
//            map.clear();
//        }
//
//        View marker = null;
//
//        marker = ((LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.custommarker, null);
//        ImageView id_custom_marker_icon = (ImageView) marker.findViewById(R.id.id_custom_marker_icon);
//        id_custom_marker_icon.setImageResource(R.drawable.grey_custom_marker_icon);
//        ImageView id_vehicle_in_marker = (ImageView) marker.findViewById(R.id.id_vehicle_in_marker);
//        id_vehicle_in_marker.setVisibility(View.GONE);
//
//        CameraPosition INIT = new CameraPosition.Builder().target(new LatLng(mLatitude, mLongitude)).zoom(vehicle_zoom_level).build();
//        map.animateCamera(CameraUpdateFactory.newCameraPosition(INIT));
//
//
//        map.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
//
//            // Use default InfoWindow frame
//            @Override
//            public View getInfoWindow(Marker arg0) {
//                return null;
//            }
//
//            // Defines the contents of the InfoWindow
//            @Override
//            public View getInfoContents(Marker arg0) {
//
//                // Getting view from the layout file info_window_layout
//                View v = getLayoutInflater().inflate(R.layout.map_info_txt_layout, null);
//
//                // Getting the position from the marker
//                LatLng latLng = arg0.getPosition();
//
//                DisplayMetrics displayMetrics = new DisplayMetrics();
//                getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
//                height = displayMetrics.heightPixels;
//                width = displayMetrics.widthPixels;
//                LinearLayout layout = (LinearLayout) v.findViewById(R.id.map_info_layout);
//                LinearLayout.LayoutParams backImageParams = new LinearLayout.LayoutParams(
//                        LinearLayout.LayoutParams.WRAP_CONTENT,
//                        LinearLayout.LayoutParams.WRAP_CONTENT);
//                backImageParams.width = width * 80 / 100;
//                //backImageParams.height = height * 10 / 100;
//                backImageParams.gravity = Gravity.CENTER;
//                layout.setLayoutParams(backImageParams);
//
//                // Getting reference to the TextView to set latitude
//                TextView txtContent = (TextView) v.findViewById(R.id.tv_txt_content);
//                txtContent.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
//
////                new GetAddressTask(txtContent).execute(mLatitude, mLongitude, 1.0);
//
//
//                StringBuffer addr = new StringBuffer();
//                Geocoder geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());
//                List<Address> addresses = null;
//                try {
//                    addresses = geocoder.getFromLocation(mLatitude, mLongitude, 1);
//                } catch (IOException e1) {
//                    e1.printStackTrace();
//                } catch (IllegalArgumentException e2) {
//                    // Error message to post in the log
////                        String errorString = "Illegal arguments " +
////                                Double.toString(params[0]) +
////                                " , " +
////                                Double.toString(params[1]) +
////                                " passed to address service";
////                        e2.printStackTrace();
////                        return errorString;
//                } catch (NullPointerException np) {
//                    // TODO Auto-generated catch block
//                    np.printStackTrace();
//                }
//                // If the reverse geocode returned an address
//                if (addresses != null && addresses.size() > 0) {
//                    // Get the first address
//                    Address address = addresses.get(0);
//                    /*
//                     * Format the first line of address (if available),
//                     * city, and country name.
//                     */
//                    String addressText = null;
//
//                    for (int i = 0; i < address.getMaxAddressLineIndex(); i++) {
//                        addressText = address.getAddressLine(i);
//                        addr.append(addressText + ",");
//                    }
//                    // Return the text
//                    // return addr.toString();
//                } else {
//                    addr.append("No address found");
//                }
//
//                // txtContent.setText(mSelectedMsg);
////                txtContent.setText(Constant.SELECTED_VEHICLE_SHORT_NAME + " " + addr);
//                txtContent.setText(Constant.SELECTED_VEHICLE_SHORT_NAME + " \n" + addressValue);
//                //  txtContent.setVisibility(View.GONE);
//
//                return v;
//
//            }
//        });
//
//
//        MarkerOptions markerOption = new MarkerOptions().position(new LatLng(mLatitude, mLongitude));
//        // markerOption.title(mSelectedMsg);
//        // markerOption.icon(BitmapDescriptorFactory.fromBitmap(createDrawableFromView(NotificationListActivity.this, marker)));
//
//
//        markerOption.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE));
//        Marker currentMarker = map.addMarker(markerOption);
//        currentMarker.showInfoWindow();
//
////        Marker marker = map.addMarker(new MarkerOptions()
////                .position(new LatLng(mLatitude, mLongitude))
////                .title(mSelectedMsg)
////                        // .snippet("Snippet")
////                .icon(BitmapDescriptorFactory
////                        .fromResource(R.drawable.grey_custom_marker_icon)));
////
////        marker.showInfoWindow();
//
//
//    }
//
//
//    @Override
//    public void onMapReady(GoogleMap googleMap) {
//        map = googleMap;
//        map.setPadding(1, 1, 1, 150);
//        map.getUiSettings().setZoomControlsEnabled(true);
//
//    }
//
//    private class MyInfoWindow extends InfoWindow {
//        String text;
//
//        public MyInfoWindow(int layoutResId, MapView mapView, String mAddressValue) {
//            super(layoutResId, mapView);
//            text = mAddressValue;
//        }
//
//        public void onClose() {
//        }
//
//        public void onOpen(Object arg0) {
//
//
//            LinearLayout layout = (LinearLayout) mView.findViewById(R.id.map_info_layout);
//            TextView txtContent = (TextView) mView.findViewById(R.id.tv_txt_content);
//            DisplayMetrics displayMetrics = new DisplayMetrics();
//            getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
//            height = displayMetrics.heightPixels;
//            width = displayMetrics.widthPixels;
////            LinearLayout layout = (LinearLayout) v.findViewById(R.id.map_info_layout);
//            LinearLayout.LayoutParams backImageParams = new LinearLayout.LayoutParams(
//                    LinearLayout.LayoutParams.WRAP_CONTENT,
//                    LinearLayout.LayoutParams.WRAP_CONTENT);
//            backImageParams.width = width * 80 / 100;
//            //backImageParams.height = height * 10 / 100;
//            backImageParams.gravity = Gravity.CENTER;
//            layout.setLayoutParams(backImageParams);
//            txtContent.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
//
//            txtContent.setText(Constant.SELECTED_VEHICLE_SHORT_NAME + " \n" + text);
//
//
//        }
//    }
//
//    @Override
//    public void onBackPressed() {
//        super.onBackPressed();
//        startActivity(new Intent(PrimaryEngineOnReportActivity.this, MapMenuActivity.class));
//        finish();
//    }
//}
