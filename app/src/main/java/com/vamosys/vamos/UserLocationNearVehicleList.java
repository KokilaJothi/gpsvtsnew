package com.vamosys.vamos;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import androidx.core.app.ActivityCompat;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.vamosys.adapter.NearByVehiclesAdapter;
import com.vamosys.model.NearestVehicle;
import com.vamosys.model.UserLocationNearestVehicle;
import com.vamosys.utils.ConnectionDetector;
import com.vamosys.utils.Constant;
import com.vamosys.utils.DaoHandler;
import com.vamosys.utils.TypefaceUtil;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

public class UserLocationNearVehicleList extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, ResultCallback<LocationSettingsResult>, com.google.android.gms.location.LocationListener {
    Toolbar mToolbar;
    SharedPreferences sp;


    protected GoogleApiClient mGoogleApiClient;
    protected LocationRequest locationRequest;
    int REQUEST_CHECK_SETTINGS = 100;
    String latLng = null;
    ConnectionDetector cd;
    Menu menuMenu;
//    Timer t;


    public static double mSELCETED_NEARBY_VEHICLE_LAT = 0.0, mSELCETED_NEARBY_VEHICLE_LNG = 0.0, mSELECTED_VEHICLE_DISTANCE;
    public static String mSELECTED_VEHICLE_SHORT_NAME, mSELECTED_VEHICLE_ID, mSELECTED_VEHICLE_ADDRESS;


    List<String> mGroupList = new ArrayList<String>();
    List<String> mGroupSpinnerList = new ArrayList<String>();

    String mSelectedGroup, mUserId;
    boolean mIsSearchEnabled = false;
    Spinner mGroupSpinner;

    ListView lv;
    //    TextView mTxtSelectedVehicle;
    ImageView mImgKmsSearch;
    //    EditText mEdtSearch;
//    Spinner groupSpinner;
//    View v3;
    TextView mTxtNoRecord;
    int width, height;
    String mNoOfKms;
    public static int mCircleMeters = 10;

    List<UserLocationNearestVehicle> mUserLocationNearestVehicleList = new ArrayList<UserLocationNearestVehicle>();
    List<NearestVehicle> mNearestVehicleList = new ArrayList<NearestVehicle>();
    List<NearestVehicle> mNearestVehicleListAdapter = new ArrayList<NearestVehicle>();
    double latitude = 0.0;
    double longitude = 0.0;
//    String latlng=null;

    BottomSheetBehavior behavior;
    boolean bottomSheetEnabled = false;
    Button mBtnDone;
    View bottomSheet;
    EditText mEdtDistance;
    boolean isOsmEnabled = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_location_near_vehicle_list);
        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        sp = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mToolbar.setTitle(getResources().getString(R.string.user_near_vehicle));
        mToolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_action_back));

        mImgKmsSearch = (ImageView) mToolbar.findViewById(R.id.img_nearby_toolbar_showalert);
        setSupportActionBar(mToolbar);

        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(UserLocationNearVehicleList.this, VehicleListActivity.class));
                finish();
            }
        });


        cd = new ConnectionDetector(getApplicationContext());
        init();
        screenArrange();
        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        mIsSearchEnabled = false;
        queryUserDB();


        if (sp.getString("ip_adds", "") != null) {
            if (sp.getString("ip_adds", "").trim().length() > 0) {
                Const.API_URL = sp.getString("ip_adds", "");
            }
        }

        if (sp.getString("enabled_map", "") != null) {
            if (sp.getString("enabled_map", "").trim().length() > 0) {

//                System.out.println("hi enabled map is " + sp.getString("enabled_map", ""));

                if (sp.getString("enabled_map", "").equalsIgnoreCase(getResources().getString(R.string.osm))) {
                    isOsmEnabled = true;
                } else {
                    isOsmEnabled = false;
                }
            }
        }

        if (!checkLocationPermission()) {
            locationCheck();
        } else {
            showP3rmAlert();
        }
//        System.out.println("Hi VEHILCE_LIST_DATA size :::" + Constant.VEHILCE_LIST_DATA.size());
//        System.out.pri

//        mTxtSelectedVehicle.setText(Constant.SELECTED_VEHICLE_SHORT_NAME);

        mGroupSpinner
                .setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                                               @Override
                                               public void onItemSelected(AdapterView<?> parent,
                                                                          View view, int pos, long id) {
                                                   // TODO Auto-generated method stub
                                                   mSelectedGroup = mGroupList.get(pos);
                                                   saveSelectedGroup(mGroupSpinnerList.get(pos));

                                                   setData();

                                               }

                                               @Override
                                               public void onNothingSelected(AdapterView<?> arg0) {
                                                   // TODO Auto-generated method stub

                                               }

                                           }

                );


        mImgKmsSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                getKmsData();

                openBottomSheet();
//                if (!bottomSheetEnabled) {
//                    behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
//                } else {
////                    setData();
//                    behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
//                }
//
//                if (bottomSheetEnabled) {
//                    bottomSheetEnabled = false;
//                } else {
//                    bottomSheetEnabled = true;
//                }

            }
        });


        mBtnDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                getKmsData();

                if (mEdtDistance.getText().toString().trim().length() > 0) {

                    mNoOfKms = mEdtDistance.getText().toString();
                    try {
                        if (mNoOfKms != null) {
                            mCircleMeters = Integer.valueOf(mNoOfKms) * 1000;
                        }

                        if (mNoOfKms.equalsIgnoreCase("0") || mNoOfKms.trim().length() == 0) {
                            Toast.makeText(getApplicationContext(), "Please enter correct data", Toast.LENGTH_SHORT).show();
//                        getKmsData();
                        } else {
//                        dialog.cancel();
                            behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);

                            if (bottomSheetEnabled) {
                                bottomSheetEnabled = false;
                            } else {
                                bottomSheetEnabled = true;
                            }

                            new getNearbyVehicleList().execute();
                        }
                    }
                    catch (Exception e){
                        e.printStackTrace();
                        Toast.makeText(getApplicationContext(), "Please enter correct data", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(getApplicationContext(), "Please enter correct data", Toast.LENGTH_SHORT).show();
                }
//                if (!bottomSheetEnabled) {
//                    behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
//                } else {
////                    setData();
//                    behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
//                }

//                if (bottomSheetEnabled) {
//                    bottomSheetEnabled = false;
//                } else {
//                    bottomSheetEnabled = true;
//                }

            }
        });


        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View v, int pos,
                                    long arg3) {
                // TODO Auto-generated method stub
                Constant.mUserLocationNearby = true;
                Constant.SELECTED_USER_LAT = String.valueOf(latitude);
                Constant.SELECTED_USER_LNG = String.valueOf(longitude);

//                28.5376492,77.1860628
//                mSELCETED_NEARBY_VEHICLE_LAT = 28.5376492;
//                mSELCETED_NEARBY_VEHICLE_LNG = 77.1860628;

//                mSELCETED_NEARBY_VEHICLE_LAT = mNearestVehicleList.get(pos).getLatitude();
//                mSELCETED_NEARBY_VEHICLE_LNG = mNearestVehicleList.get(pos).getLongitude();
//
//
//                mSELECTED_VEHICLE_DISTANCE = mNearestVehicleList.get(pos).getDistance();
//                mSELECTED_VEHICLE_SHORT_NAME = mNearestVehicleList.get(pos).getShortName();
//                mSELECTED_VEHICLE_ID = mNearestVehicleList.get(pos).getVehicleId();
//                mSELECTED_VEHICLE_ADDRESS = mNearestVehicleList.get(pos).getAddress();


                mSELCETED_NEARBY_VEHICLE_LAT = mNearestVehicleListAdapter.get(pos).getLatitude();
                mSELCETED_NEARBY_VEHICLE_LNG = mNearestVehicleListAdapter.get(pos).getLongitude();


                mSELECTED_VEHICLE_DISTANCE = mNearestVehicleListAdapter.get(pos).getDistance();
                mSELECTED_VEHICLE_SHORT_NAME = mNearestVehicleListAdapter.get(pos).getShortName();
                mSELECTED_VEHICLE_ID = mNearestVehicleListAdapter.get(pos).getVehicleId();
                mSELECTED_VEHICLE_ADDRESS = mNearestVehicleListAdapter.get(pos).getAddress();


//                showAlert();
//                getSelectedVehicleObj();
                Constant.mUserLocationNearbyActivityCalled = true;
//                startActivity(new Intent(UserLocationNearVehicleList.this, NearByMapViewActivity.class));

                if (isOsmEnabled) {
                    startActivity(new Intent(UserLocationNearVehicleList.this, NearByMapViewActivity.class));
//                    finish();
                } else {
                    startActivity(new Intent(UserLocationNearVehicleList.this, NearByGoogleMapViewActivity.class));
//                    finish();
                }

//                finish();

            }
        });


//
//        mGroupSpinner
//                .setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//
//                                               @Override
//                                               public void onItemSelected(AdapterView<?> parent,
//                                                                          View view, int pos, long id) {
//                                                   // TODO Auto-generated method stub
//                                                   mSelectedGroup = mGroupList.get(pos);
//                                                   saveSelectedGroup(mGroupSpinnerList.get(pos));
//                                                   String url = null;
//
//                                                   if (mSelectedGroup.equalsIgnoreCase("Select")) {
//                                                       url = "http://188.166.244.126:9000/mobile/getNearestVehicleFromUserLocation?userId=vantec&kms=50&userLocation=12.837703,79.941644";
//
//                                                   } else {
//                                                       url = "http://188.166.244.126:9000/mobile/getNearestVehicleFromUserLocation?userId=vantec&kms=50&userLocation=12.837703,79.941644";
//                                                   }
//                                                   Constant.SELECTED_GROUP = mSelectedGroup;
//
//                                                   if (cd.isConnectingToInternet()) {
//                                                       new PullVehicleInformation().execute(url);
//                                                   } else {
//                                                       Toast.makeText(getApplicationContext(),
//                                                               "Please check your network connection",
//                                                               Toast.LENGTH_SHORT).show();
//                                                       setData("No Record");
//                                                   }
//                                               }
//
//                                               @Override
//                                               public void onNothingSelected(AdapterView<?> arg0) {
//                                                   // TODO Auto-generated method stub
//
//                                               }
//
//                                           }
//
//                );
//

//        mLstVehicleView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//
//            @Override
//            public void onItemClick(AdapterView<?> arg0, View v, int pos,
//                                    long arg3) {
//                // TODO Auto-generated method stub
//                Constant.SELECTED_VEHICLE_LOCATION_OBJECT = mVehicleLocationListAdapter.get(pos);
//                if (Constant.SELECTED_VEHICLE_LOCATION_OBJECT != null) {
//                    Constant.SELECTED_USER_ID = mUserId;
//                    Constant.VEHILCE_LIST_DATA = mVehicleLocationListAdapter;
//
////                if (mVehicleLocationListAdapter.get(pos).getVehicleId() != null) {
//                    Constant.SELECTED_VEHICLE_ID = mVehicleLocationListAdapter.get(pos).getVehicleId();
//
//                    //   System.out.println("Hi vehicle short name is 0000" + mVehicleLocationListAdapter.get(pos).getShortName() + " :: " + mVehicleLocationListAdapter.get(pos).getVehicleId());
////                }
//                    Constant.SELECTED_VEHICLE_SHORT_NAME = mVehicleLocationListAdapter.get(pos).getShortName();
//                    Constant.SELECTED_ODO_DISTANCE = String.valueOf(mVehicleLocationListAdapter.get(pos).getOdoDistance());
//                    Constant.SELECTED_AC_STATUS = mVehicleLocationListAdapter.get(pos).getVehicleBusy();
//                    Constant.SELECTED_DRIVER_NAME = mVehicleLocationListAdapter.get(pos).getDriverName();
//                    Constant.SELECTED_DRIVER_CONTACT_NUMBER = mVehicleLocationListAdapter.get(pos).getMobileNo();
//                    Constant.SELECTED_VEHICLE_TYPE = mVehicleLocationListAdapter.get(pos).getVehicleType();
//                    Constant.SELECTED_MAIN_VEHICLE_LAT = mVehicleLocationListAdapter.get(pos).getLatitude();
//                    Constant.SELECTED_MAIN_VEHICLE_LNG = mVehicleLocationListAdapter.get(pos).getLongitude();
//                    Constant.SELECTED_VEHICLE_ADDRESS = mVehicleLocationListAdapter.get(pos).getAddress();
////                And datas for info activity
//                    if (mVehicleLocationListAdapter.get(pos).getExpired() != null) {
//
//                        if (mVehicleLocationListAdapter.get(pos).getExpired().equalsIgnoreCase("No")) {
//                            startActivity(new Intent(UserLocationNearVehicleList.this,
//                                    MapMenuActivity.class));
//                            finish();
//                        } else {
//                            Toast.makeText(UserLocationNearVehicleList.this, "Vehicle expired! Please select another vehicle.", Toast.LENGTH_SHORT).show();
//                        }
//                    } else {
//                        startActivity(new Intent(UserLocationNearVehicleList.this,
//                                MapMenuActivity.class));
//                        finish();
//                    }
//                }
//
//            }
//        });


        bottomSheet = findViewById(R.id.bottom_sheet);
        behavior = BottomSheetBehavior.from(bottomSheet);
        behavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                // React to state change
//                System.out.println("Hi state 0000" + bottomSheetEnabled);


            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
                // React to dragging events
            }
        });


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_user_nearby, menu);
//        searchItem = menu.findItem(R.id.search);
//        searchItem.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
//            @Override
//            public boolean onMenuItemClick(MenuItem item) {
//                mSearchView.display();
//                openKeyboard();
//                return true;
//            }
//        });
//        if (searchActive)
//            mSearchView.display();
//        return true;

        menuMenu = menu;
        MenuItem myActionMenuItem = menu.findItem(R.id.search);
        myActionMenuItem.setVisible(false);
        final SearchView searchView = (SearchView) myActionMenuItem.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (TextUtils.isEmpty(newText)) {
                    // adapter.filter("");
                    //  listView.clearTextFilter();
                    getSearchText(null);
                } else {
                    // adapter.filter(newText);


                    getSearchText(String.valueOf(newText).trim());


                }
                return true;
            }
        });

        return true;


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // action with ID action_refresh was selected

            case R.id.search_nearby:
                openBottomSheet();

                break;
            default:
                break;
        }

        return true;
    }


    private void openBottomSheet() {

        if (!bottomSheetEnabled) {
            behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
        } else {
//                    setData();
            behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        }

        if (bottomSheetEnabled) {
            bottomSheetEnabled = false;
        } else {
            bottomSheetEnabled = true;
        }

    }

    private void init() {


        lv = (ListView) findViewById(R.id.nearby_vehiclelist);
        mTxtNoRecord = (TextView) findViewById(R.id.nearby_no_record_txt);
        mTxtNoRecord.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        mGroupSpinner = (Spinner) findViewById(R.id.navigation_spinner);
        mBtnDone = (Button) findViewById(R.id.btn_done);
        mEdtDistance = (EditText) findViewById(R.id.edt_distance);
    }


    public void getSearchText(String ch) {


        List<NearestVehicle> list = new ArrayList<NearestVehicle>();
        // list = null;
        if (ch != null && ch.length() > 0) {


            list.clear();

            for (int i = 0; i < mNearestVehicleList.size(); i++) {
                NearestVehicle dc = new NearestVehicle();


                if (Pattern
                        .compile(Pattern.quote(ch), Pattern.CASE_INSENSITIVE)
                        .matcher(mNearestVehicleList.get(i).getShortName())
                        .find()) {
                    //  System.out.println("Hiiiiiii");
                    dc = mNearestVehicleList.get(i);
                    list.add(dc);
                }

            }

        } else {
            list = mNearestVehicleList;
        }

        System.out.println("The searched list size is :::::" + list.size());


        if (list.size() > 0) {

            mTxtNoRecord.setVisibility(View.GONE);
            lv.setVisibility(View.VISIBLE);

            mNearestVehicleListAdapter = list;
            System.out.println("List size after filtered is :::::"
                    + mNearestVehicleListAdapter.size());
            NearByVehiclesAdapter adapter = new NearByVehiclesAdapter(UserLocationNearVehicleList.this,
                    mNearestVehicleListAdapter);
            lv.setAdapter(adapter);
            adapter.notifyDataSetChanged();
        } else {

            mTxtNoRecord.setVisibility(View.GONE);
            lv.setVisibility(View.VISIBLE);

//    Set no record data
            List<NearestVehicle> vList = new ArrayList<NearestVehicle>();
            mNearestVehicleListAdapter = vList;
            NearByVehiclesAdapter adapter = new NearByVehiclesAdapter(UserLocationNearVehicleList.this,
                    mNearestVehicleListAdapter);
            lv.setAdapter(adapter);
            adapter.notifyDataSetChanged();
            // setData("No Record");

        }
        // $SearchListView.setAdapter(new DailySearchAdapter(DailySearch.this,
        // mFilteredListDailySearch));

    }


    public void setData() {
//        mKmsSummaryAdapterList = mKmsSummaryList;
        Constant.SELECTED_VEHICLE_ID = null;
        System.out.println("The vehicle list is beginning :::::" + mUserLocationNearestVehicleList.size());
        if (mUserLocationNearestVehicleList != null && mUserLocationNearestVehicleList.size() > 0) {
            mNearestVehicleList = null;
            for (int i = 0; i < mUserLocationNearestVehicleList.size(); i++) {
                if (mUserLocationNearestVehicleList.get(i).getGroup().toLowerCase().equalsIgnoreCase(mSelectedGroup.toLowerCase())) {
                    mNearestVehicleList = mUserLocationNearestVehicleList.get(i).getNearestVehicles();
                    System.out.println("Hi selected group " + mSelectedGroup + " size " + mNearestVehicleList.size());
                }
            }


            mNearestVehicleListAdapter = mNearestVehicleList;

//            System.out.println("The vehicle list is :::::" + mNearestVehicleListAdapter.size());

            if (mNearestVehicleListAdapter != null) {
                if (mNearestVehicleListAdapter.size() > 0) {

                    MenuItem item = menuMenu.findItem(R.id.search);
                    item.setVisible(true);


                    mTxtNoRecord.setVisibility(View.GONE);
                    lv.setVisibility(View.VISIBLE);
//                    mNearestVehicleListAdapter = list;
                    NearByVehiclesAdapter adapter = new NearByVehiclesAdapter(UserLocationNearVehicleList.this,
                            mNearestVehicleListAdapter);
                    lv.setAdapter(adapter);
                    adapter.notifyDataSetChanged();
                } else {
                    mTxtNoRecord.setVisibility(View.GONE);
                    lv.setVisibility(View.VISIBLE);
                    List<NearestVehicle> vList = new ArrayList<NearestVehicle>();
                    mNearestVehicleListAdapter = vList;
                    NearByVehiclesAdapter adapter = new NearByVehiclesAdapter(UserLocationNearVehicleList.this,
                            mNearestVehicleListAdapter);
                    lv.setAdapter(adapter);
                    adapter.notifyDataSetChanged();
                }
            } else {

                mTxtNoRecord.setVisibility(View.GONE);
                lv.setVisibility(View.VISIBLE);

//    Set no record data
                // setData("No Record");
                List<NearestVehicle> vList = new ArrayList<NearestVehicle>();
                mNearestVehicleListAdapter = vList;
                NearByVehiclesAdapter adapter = new NearByVehiclesAdapter(UserLocationNearVehicleList.this,
                        mNearestVehicleListAdapter);
                lv.setAdapter(adapter);
                adapter.notifyDataSetChanged();
            }


//            if (mNearestVehicleList.size() > 0) {
//                mTxtNoRecord.setVisibility(View.GONE);
//                lv.setVisibility(View.VISIBLE);
//                NearByVehiclesAdapter adapter = new NearByVehiclesAdapter(UserLocationNearVehicleList.this,
//                        mNearestVehicleList);
//                lv.setAdapter(adapter);
//                adapter.notifyDataSetChanged();
//            } else {
//                openBottomSheet();
//                mTxtNoRecord.setVisibility(View.VISIBLE);
//                lv.setVisibility(View.GONE);
//            }
        } else {
            mTxtNoRecord.setVisibility(View.VISIBLE);
            lv.setVisibility(View.GONE);

            openBottomSheet();
        }
    }


    public void getKmsData() {
        android.app.AlertDialog.Builder alertDialog = new android.app.AlertDialog.Builder(UserLocationNearVehicleList.this, R.style.MyAlertDialogStyle);
        alertDialog.setTitle("Distance");
        alertDialog.setMessage("Enter No of KMs");

//        final EditText input = new EditText(NearByVehicleListActivity.this);
//        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
//                LinearLayout.LayoutParams.MATCH_PARENT,
//                LinearLayout.LayoutParams.MATCH_PARENT);
//        input.setLayoutParams(lp);
//        input.setInputType(InputType.TYPE_CLASS_NUMBER);
//        alertDialog.setView(input);

        final EditText input;

        LayoutInflater inflater = this.getLayoutInflater();

        // Inflate and set the layout for the dialog
        // Pass null as the parent view because its going in the dialog layout
        LayoutInflater li = LayoutInflater.from(getApplicationContext());
        View promptsView = li.inflate(R.layout.textview_layout, null);
        alertDialog.setView(promptsView);

        final EditText userInput = (EditText) promptsView
                .findViewById(R.id.popup_edt_text);
//        alertDialog.setIcon(R.mipmap.ic_launcher);
        alertDialog.setCancelable(false);
//        alertDialog.set
        alertDialog.setPositiveButton("Ok",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        if (userInput.getText().toString().trim().length() > 0) {
                            mNoOfKms = userInput.getText().toString();

                            if (mNoOfKms != null) {
                                mCircleMeters = Integer.valueOf(mNoOfKms) * 1000;
                            }

                            if (mNoOfKms.equalsIgnoreCase("0") || mNoOfKms.trim().length() == 0) {
                                Toast.makeText(getApplicationContext(), "Please enter correct data", Toast.LENGTH_SHORT).show();
                                getKmsData();
                            } else {
                                dialog.cancel();
                                new getNearbyVehicleList().execute();
                            }
                            InputMethodManager imm1 = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                            imm1.hideSoftInputFromWindow(userInput.getWindowToken(), 0);
                            InputMethodManager inputManager = (InputMethodManager)
                                    getSystemService(Context.INPUT_METHOD_SERVICE);

                            inputManager.hideSoftInputFromWindow(userInput.getWindowToken(),
                                    InputMethodManager.HIDE_NOT_ALWAYS);

//                        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
//                        inputMethodManager.hideSoftInputFromWindow(NearByVehicleListActivity.this.getCurrentFocus().getWindowToken(), 0);
                        } else {
                            Toast.makeText(getApplicationContext(), "Please enter correct data", Toast.LENGTH_SHORT).show();
                            getKmsData();
                        }


                    }
                });

        alertDialog.setNegativeButton("Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        startActivity(new Intent(UserLocationNearVehicleList.this, VehicleListActivity.class));
                        finish();
                    }
                });

        alertDialog.show();


    }


    private class getNearbyVehicleList extends AsyncTask<String, String, String> {
        ProgressDialog progressDialog;

        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub

            String result = null;
            try {
                Const constaccess = new Const();
                /*
New API
 */


//                latLng = "13.0470848,80.192315";


//                String url = "http://188.166.244.126:9000/mobile/getNearestVehicleFromUserLocation?userId=" + mUserId + "&kms=" + mNoOfKms + "&userLocation=" + latLng + "&group=" + mSelectedGroup;
                String url = Const.API_URL + "/mobile/getNearestVehicleFromUserLocation?userId=" + mUserId + "&kms=" + mNoOfKms + "&userLocation=" + latLng + "&group=" + mSelectedGroup;
                result = constaccess.sendGet(url, 30000);

//                result = constaccess
//                        .sendGet(Const.API_URL + "/mobile/getNearestVehicle?userId=" + Constant.SELECTED_USER_ID + "&kms=" + mNoOfKms + "&vehicleId=" + Constant.SELECTED_VEHICLE_ID + "&groupId="
//                                + Constant.SELECTED_GROUP, 30000);


                //  + "&fromDate=2016-01-05&toDate=2016-01-30", 30000);
                        /*
                        The below is old API
                         */
//                result = constaccess
//                        .sendGet("http://vamosys.com/mobile/getExecutiveReport?userId=" + mUserId + "&groupId="
//                                + mSelectedGroup
//                                + "&fromDate=2016-01-05&toDate=2016-01-30", 30000);
                // result = ht.doPostEcpl(mLoginData, "www.test.com");
                // Constant.printMsg("Response........" + result);
            } catch (Exception e) {

            }

            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
//            getWindow().setSoftInputMode(
//                    WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
//            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
//            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
            progressDialog.dismiss();
//            if (t != null) {
//                t.cancel();
//            }
            if (result != null && result.length() > 0) {
//                System.out.println("The kms result is ::::" + result);
                Gson g = new Gson();
                InputStream is = new ByteArrayInputStream(result.getBytes());
                Reader reader = new InputStreamReader(is);
                Type fooType = new TypeToken<List<UserLocationNearestVehicle>>() {
                }.getType();
                mUserLocationNearestVehicleList = g.fromJson(reader, fooType);
                setData();
            }

        }

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            progressDialog = new ProgressDialog(UserLocationNearVehicleList.this,
                    android.app.AlertDialog.THEME_HOLO_LIGHT);
            progressDialog.setMessage("Please Wait...");
            progressDialog.setProgressDrawable(new ColorDrawable(
                    android.graphics.Color.BLUE));
            progressDialog.setCancelable(true);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.show();
        }

    }

    public void showAlert() {
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(UserLocationNearVehicleList.this, R.style.MyAlertDialogStyle);
        builder.setTitle("NearBy:");
        builder.setMessage("Please click anyone option").setCancelable(false).setPositiveButton("Map", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                //do things
//                startActivity(new Intent(UserLocationNearVehicleList.this, NearByMapViewActivity.class));
//                finish();

                if (isOsmEnabled) {
                    startActivity(new Intent(UserLocationNearVehicleList.this, NearByMapViewActivity.class));
                    finish();
                } else {
                    startActivity(new Intent(UserLocationNearVehicleList.this, NearByGoogleMapViewActivity.class));
                    finish();
                }

                dialog.cancel();
            }
        }).setNegativeButton("Info", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int i) {
                getSelectedVehicleObj();

                dialog.cancel();
            }
        });
        android.app.AlertDialog alert = builder.create();
        alert.show();
    }

    public void getSelectedVehicleObj() {
        System.out.println("Hi getSelectedVehicleObj called :::" + Constant.VEHILCE_LIST_DATA.size());
        //VehicleData mVehicleData = new VehicleData();
        Constant.SELECTED_VEHICLE_LOCATION_OBJECT = null;
        for (int i = 0; i < Constant.VEHILCE_LIST_DATA.size(); i++) {
            System.out.println("The selected vehicle id 000000 :::" + mSELECTED_VEHICLE_ID + " list vehicle id ::::" + Constant.VEHILCE_LIST_DATA.get(i).getVehicleId());
            if (Constant.VEHILCE_LIST_DATA.get(i).getVehicleId().trim().equalsIgnoreCase(mSELECTED_VEHICLE_ID)) {

                System.out.println("The selected vehicle id :::" + mSELECTED_VEHICLE_ID + " list vehicle id ::::" + Constant.VEHILCE_LIST_DATA.get(i).getVehicleId());

                Constant.SELECTED_VEHICLE_LOCATION_OBJECT = Constant.VEHILCE_LIST_DATA.get(i);

                //mVehicleData = Constant.VEHILCE_LIST_DATA.get(i);
            }

        }

        startActivity(new Intent(UserLocationNearVehicleList.this, NewVehicleInfoActivity.class));
        finish();
    }


    private void saveSelectedGroup(String mGroupName) {
        try {
            SharedPreferences.Editor editor = sp.edit();
            editor.putString("selected_group", mGroupName);
            editor.commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void queryUserDB() {

        DaoHandler da = new DaoHandler(getApplicationContext(), false);
        da.queryUserDB();
        mUserId = Constant.mDbUserId;
        Constant.SELECTED_USER_ID = Constant.mDbUserId;
        mGroupList = Constant.mUserGroupList;

        setDropDownData();
    }


    public void setDropDownData() {
        //mGroupList = new ArrayList<String>();
        // List<String> list = new ArrayList<String>();
        // list.add("Select");
        if (mGroupList.size() > 0) {

        } else {
            mGroupList.add("Select");
        }

        for (int i = 0; i < mGroupList.size(); i++) {

            String[] str_msg_data_array = mGroupList.get(i).split(":");

            mGroupSpinnerList.add(str_msg_data_array[0]);

        }

        // R.layout.spinner_row

//        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(
//                VehicleListActivity.this, android.R.layout.simple_spinner_item,
//                mGroupList) {

        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(
                UserLocationNearVehicleList.this, R.layout.spinner_row,
                mGroupSpinnerList) {

            public View getView(int position, View convertView, ViewGroup parent) {
                View v = super.getView(position, convertView, parent);
                // TextView v = (TextView) super.getView(position,
                // convertView,parent);
                // Constant.face(DailyCalls.this, (TextView) v);
                return v;
            }

            public View getDropDownView(int position, View convertView,
                                        ViewGroup parent) {
                View v = super.getDropDownView(position, convertView, parent);
                // ((TextView)
                // v).setBackgroundColor(Color.parseColor("#BBfef3da"));
                // TextView v = (TextView) super.getView(position,
                // convertView,parent);
                // Constant.face(DailyCalls.this, (TextView) v);
                return v;
            }
        };
        spinnerArrayAdapter
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mGroupSpinner.setAdapter(spinnerArrayAdapter);

        String groupName = sp.getString("selected_group", "");
        int spinnerPosition = spinnerArrayAdapter.getPosition(groupName);

//set the default according to value
        mGroupSpinner.setSelection(spinnerPosition);


    }


    private void locationCheck() {

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this).build();
        mGoogleApiClient.connect();

        locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(30 * 1000);
        locationRequest.setFastestInterval(5 * 1000);
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        locationCheck();
        System.out.println("Hi onRestart called");
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(UserLocationNearVehicleList.this, VehicleListActivity.class));
        finish();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest);
        builder.setAlwaysShow(true);
        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(
                        mGoogleApiClient,
                        builder.build()
                );

        result.setResultCallback(this);

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Toast.makeText(getApplicationContext(), "Please try again", Toast.LENGTH_LONG).show();
        startActivity(new Intent(UserLocationNearVehicleList.this, VehicleListActivity.class));
        finish();
    }

    @Override
    public void onResult(@NonNull LocationSettingsResult locationSettingsResult) {
        final Status status = locationSettingsResult.getStatus();
        switch (status.getStatusCode()) {
            case LocationSettingsStatusCodes.SUCCESS:

                // NO need to show the dialog;
                System.out.println("Hi gps already enabled true");
                startLocationUpdate();
//                getLocationData();
                break;

            case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                //  Location settings are not satisfied. Show the user a dialog

                try {
                    // Show the dialog by calling startResolutionForResult(), and check the result
                    // in onActivityResult().
                    System.out.println("Hi gps already enabled false");
                    status.startResolutionForResult(UserLocationNearVehicleList.this, REQUEST_CHECK_SETTINGS);

                } catch (IntentSender.SendIntentException e) {

                    //failed to show
                }
                break;

            case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                // Location settings are unavailable so not possible to show any dialog now
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CHECK_SETTINGS) {

            if (resultCode == RESULT_OK) {
//                getLocationData();
//                Toast.makeText(getApplicationContext(), "GPS enabled", Toast.LENGTH_LONG).show();
                startLocationUpdate();
            } else {

                Toast.makeText(getApplicationContext(), "Please enable location", Toast.LENGTH_LONG).show();
                startActivity(new Intent(UserLocationNearVehicleList.this, VehicleListActivity.class));
                finish();
            }

        }
    }


    @Override
    public void onLocationChanged(Location location) {
        if (location != null) {
//            latLng = location.getLatitude() + "," + location.getLongitude();
            latitude = location.getLatitude();
            longitude = location.getLongitude();
            latLng = latitude + "," + longitude;
            Log.i("LOGSERVICE", "Hi lng " + latLng);

            if (mUserLocationNearestVehicleList.size() > 0) {

            } else {
//                getKmsData();

//                openBottomSheet();
            }


        } else {
            Toast.makeText(getApplicationContext(), "Location data not available", Toast.LENGTH_LONG).show();
        }
        stopLocationUpdate();
    }

    private void startLocationUpdate() {
//        initLocationRequest();

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, locationRequest, this);
    }


    private void stopLocationUpdate() {

        LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);

    }

    public boolean checkLocationPermission() {
        return (ActivityCompat.checkSelfPermission(UserLocationNearVehicleList.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(UserLocationNearVehicleList.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED);

    }

    public void showP3rmAlert() {
//        AlertDialog.Builder builder = new AlertDialog.Builder(UserLocationNearVehicleList.this);

        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(UserLocationNearVehicleList.this, R.style.MyAlertDialogStyle);

        builder.setTitle("Enable app permissions");
//        builder.setMessage(getString(R.string.app_p3rm_dialog_message));
        builder.setMessage("Please enable location permission");

        String positiveText = UserLocationNearVehicleList.this.getString(android.R.string.ok);
        builder.setPositiveButton(positiveText,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // positive button logic
                        dialog.dismiss();
                        Intent intent = new Intent();
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.setAction(android.provider.Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                        Uri uri = Uri.fromParts("package", getApplicationContext().getPackageName(), null);
                        intent.setData(uri);
                        getApplicationContext().startActivity(intent);
                    }
                });

        String negativeText = UserLocationNearVehicleList.this.getString(android.R.string.cancel);
        builder.setNegativeButton(negativeText,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // negative button logic
                        dialog.dismiss();
                        Toast.makeText(getApplicationContext(), "Please enable location permission", Toast.LENGTH_LONG).show();
                        startActivity(new Intent(UserLocationNearVehicleList.this, VehicleListActivity.class));
                        finish();
                    }
                });

//        AlertDialog dialog = builder.create();
        // display dialog
        builder.show();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mGoogleApiClient != null) {
            if (mGoogleApiClient.isConnected()) {
                mGoogleApiClient.disconnect();
            }
        }
    }


    private void screenArrange() {

        // TODO Auto-generated method stub
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        height = displayMetrics.heightPixels;
        width = displayMetrics.widthPixels;
        Constant.ScreenWidth = width;
        Constant.ScreenHeight = height;


        LinearLayout.LayoutParams spinnerParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        spinnerParams.width = width * 98 / 100;
        spinnerParams.height = height * 6 / 100;
        spinnerParams.gravity = Gravity.CENTER | Gravity.TOP;
        spinnerParams.topMargin = (int) (height * 1 / 100);
        //spinnerParams.leftMargin = width * 1 / 100;
        //spinnerParams.rightMargin = width * 1 / 100;
        spinnerParams.bottomMargin = (int) (height * 0.25 / 100);
        mGroupSpinner.setLayoutParams(spinnerParams);

        Toolbar.LayoutParams imgAlertParams = new Toolbar.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
//        imgAlertParams.width = width * 12 / 100;
//        imgAlertParams.height = height * 6 / 100;
//        imgAlertParams.topMargin = (int) (height * 0.25 / 100);
        imgAlertParams.leftMargin = width * 32 / 100;
//        imgAlertParams.rightMargin = width * 2 / 100;
//        textParams.setGravity()
        mImgKmsSearch.setLayoutParams(imgAlertParams);

        LinearLayout.LayoutParams lineParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        lineParams.width = width * 98 / 100;
        lineParams.height = LinearLayout.LayoutParams.WRAP_CONTENT;

        lineParams.bottomMargin = height * 1 / 100;
        lineParams.leftMargin = width * 1 / 100;
        lineParams.rightMargin = width * 1 / 100;
        lv.setLayoutParams(lineParams);


        if (width >= 600) {
            mTxtNoRecord.setTextSize(17);
//            mTxtSelectedVehicle.setTextSize(17);

        } else if (width > 501 && width < 600) {
            mTxtNoRecord.setTextSize(16);
//            mTxtSelectedVehicle.setTextSize(16);

        } else if (width > 260 && width < 500) {
            mTxtNoRecord.setTextSize(15);
//            mTxtSelectedVehicle.setTextSize(15);


        } else if (width <= 260) {
            mTxtNoRecord.setTextSize(14);
//            mTxtSelectedVehicle.setTextSize(14);

        }
    }


}
