package com.vamosys.vamos;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.vamosys.adapter.ExecutiveHeaderAdapter;
import com.vamosys.model.ExcutiveDatum;
import com.vamosys.model.ExecutiveDataPojo;

import java.util.ArrayList;
import java.util.List;

public class ExecutiveRowAdapter extends  RecyclerView.Adapter<RecyclerView.ViewHolder> {
    ArrayList<ExcutiveDatum> mExeData=new ArrayList<>();
    private static final int TYPE_HEADER = 0;
    private static final int TYPE_ITEM = 1;
    Context mcontext;
    public ExecutiveRowAdapter(ArrayList<ExcutiveDatum> mExecutive, Context context) {
        mExeData=mExecutive;
        mcontext=context;
        Log.d("exe"," ExecutiveRowAdapter "+mExeData.size());
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Log.d("exe","ExecutiveRowAdapter oncreate");
        if (viewType == TYPE_ITEM) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.executive_report_child_row, parent, false);
            return new RowViewHolder(view);
        } else if (viewType == TYPE_HEADER) {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.executive_report_child_header, parent, false);
            return new HeaderViewHolder(itemView);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        Log.d("exe"," ExecutiveRowAdapter onBind");
        if (holder instanceof HeaderViewHolder){
           HeaderViewHolder headerViewHolder = (HeaderViewHolder) holder;
        }
        else if (holder instanceof RowViewHolder) {
            final RowViewHolder holder1 = (RowViewHolder) holder;
            ExcutiveDatum e=getItem(position);
            holder1.txt_date.setText(e.getDate());
            holder1.txt_Kms.setText(""+e.getDistanceToday());
            holder1.txt_park.setText(""+e.getParkingCount());
            holder1.txt_over.setText(""+e.getOverSpeedInstances());
            holder1.txt_Sodo.setText(""+(int)Math.round(e.getOdoOpeningReading()));
            holder1.txt_Eodo.setText(""+(int)Math.round(e.getOdoClosingReading()));
            holder1.txt_running.setText(Const.getVehicleTimeNew(String.valueOf(e.getTotalRunningTime())));
            holder1.txt_idel.setText(Const.getVehicleTimeNew(String.valueOf(e.getTotalIdleTime())));
            holder1.txt_parkTime.setText(Const.getVehicleTimeNew(String.valueOf(e.getTotalParkedTime())));
            holder1.txt_nodata.setText(Const.getVehicleTimeNew(String.valueOf(e.getTotalNoDataTime())));
        }
    }

    @Override
    public int getItemViewType(int position) {
        Log.d("exe","ExecutiveRowAdapter  "+position);
        if (isPositionHeader(position)) {
            return TYPE_HEADER;
        }
        return TYPE_ITEM;
    }
    @Override
    public int getItemCount() {
        Log.d("exe","ExecutiveRowAdapter  data "+mExeData.size());
        return mExeData.size()+1;
    }

    private boolean isPositionHeader(int position) {
        return position == 0;
    }

    private ExcutiveDatum getItem(int position) {
        System.out.println("exe "+mExeData.size());
        Log.d("exe","ExecutiveRowAdapter  data "+mExeData.size());
        return mExeData.get(position-1);
    }
    public class RowViewHolder extends RecyclerView.ViewHolder{
        TextView txt_date,txt_Kms,txt_park,txt_over,txt_Sodo,txt_Eodo,txt_running,txt_idel,txt_parkTime,txt_nodata;

        public RowViewHolder(@NonNull View itemView) {
            super(itemView);
            txt_date=itemView.findViewById(R.id.date_textView_row);
            txt_Kms=itemView.findViewById(R.id.kms_textView_row);
            txt_park=itemView.findViewById(R.id.park_count_textView_row);
            txt_over=itemView.findViewById(R.id.over_speed_count_textView_row);
            txt_Sodo=itemView.findViewById(R.id.odo_start_textView_row);
            txt_Eodo=itemView.findViewById(R.id.odo_end_textView_row);
            txt_running=itemView.findViewById(R.id.running_textView_row);
            txt_idel=itemView.findViewById(R.id.idle_textView_row);
            txt_parkTime=itemView.findViewById(R.id.park_textView_row);
            txt_nodata=itemView.findViewById(R.id.nodata_textView_row);
        }
    }

    private class HeaderViewHolder extends RecyclerView.ViewHolder {
        TextView Htxt_date,Htxt_Kms,Htxt_park,Htxt_over,Htxt_running,Htxt_idel,Htxt_Sodo,Htxt_Eodo;
        public HeaderViewHolder(View view) {
            super(view);
            Htxt_date=itemView.findViewById(R.id.date_textView_header);
            Htxt_Kms=itemView.findViewById(R.id.kms_textView_header);
            Htxt_park=itemView.findViewById(R.id.park_count_textView_header);
            Htxt_over=itemView.findViewById(R.id.over_speed_count_textView_header);
            Htxt_Eodo=itemView.findViewById(R.id.odo_end_textView_header);
            Htxt_Sodo=itemView.findViewById(R.id.odo_start_textView_header);
            Htxt_running=itemView.findViewById(R.id.running_textView_header);
            Htxt_idel=itemView.findViewById(R.id.idle_textView_header);
        }
    }
}
