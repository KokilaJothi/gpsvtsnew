package com.vamosys.vamos;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import androidx.annotation.NonNull;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.github.mikephil.charting.charts.LineChart;
import com.google.android.gms.maps.GoogleMap;
import com.vamosys.model.FuelsumDto;
import com.vamosys.utils.ConnectionDetector;
import com.vamosys.utils.Constant;
import com.vamosys.utils.HorizontalScrollView;
import com.vamosys.utils.HttpConfig;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class FuelSummary extends AppCompatActivity {
    ImageView fuel_back,fuel_calendar;

    static ListView lv;
    float totconsum=0,totalfill=0,totaltheft=0,totaldisance=0;
    TextView mTxtNoRecord, mTxtfill,mTxtconsumption,mTxttheft,mTextdist,mTxtdate,mTxtnorcrd;
    Button mButton;
    ImageView mCalendarImage, mImgMapViewClose;
    static LinearLayout mFuelDataMapLayout;
    Toolbar mToolbar;
    Spinner spinner;
    List<String> mGroupSpinnerList = new ArrayList<String>();
    List<String> mGroupList = new ArrayList<String>();
    LinearLayout card_layout;

    boolean  isFromDate = false,isToDate = false;

    //    ImageView mImgDataViewChange;
    ConnectionDetector cd;
    SharedPreferences sp;
    static Const cons;

    static int width;
    static int height;
    boolean isHeaderPresent = false, mIsLineChartEnabled = false;
    private List<FuelsumDto> mFuelFillData = new ArrayList<FuelsumDto>();

    TableAdapter adapter;

    Dialog marker_info_dialog;


    //    String mTotalFuelValue = null;
    private static GoogleMap map;
    //    ImageView $ChangeView;
    boolean mIsMapPresent = false;
    String mStrUtcFromDate, mStrUtcToDate;
    String mSELECTED_MAP_TYPE = "Normal";
    static double mLatitude = 0;
    static double mLongitude = 0;
    static float vehicle_zoom_level = 15.5F;
    static Context mFuelContext;

    SimpleDateFormat timeFormatFuelFillReport = new SimpleDateFormat(
            "HH:mm:ss");

    SimpleDateFormat timeFormat = new SimpleDateFormat(
            "hh:mm:ss aa");

    SimpleDateFormat timeFormatShow = new SimpleDateFormat(
            "hh:mm aa");

    View bottomSheet;
    BottomSheetBehavior behavior;
    boolean bottomSheetEnabled = false;

    private int year, month, day;
    static final int TIME_DIALOG_ID = 1111;
    static final int DATE_DIALOG_ID = 2222;
    private int hour;
    private int minute;
    Spinner spinnerInterval;
    Button mBtnSubmit;
    TextView mTxtFromDate, mTxtEndDate, mTxtFromTime, mTxtEndTime;
    String mStartDate, mEndDate, mStartTime, mStartTimeValue, mStartTimeFuelReport, mEndTimeFuelReport, mEndDateValue, mEndTime, mEndTimeValue, mIntervalSpinnerSelectedValue = "1";
    boolean isFromTime = false, isMapPresent = false;
    int mFromHourValue = 0, mFromMinuteValue = 0, mToHourValue = 0, mToMinuteValue = 0;
    ArrayList<String> mIntervalList = new ArrayList<String>();
    int mIntervalSpinnerSelectedPos = 0;
    private LineChart mChart;
    LinearLayout mChartLayout;
    String mSelectedGroup;

    ArrayList<Integer> xAxis;


    double mTotalFuelConsume, mTotalFuelFill, mTripDistance, mOdoStart, mOdoEnd, mTotalMileage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fuel_summary);
        cons = new Const();
        init();
        sp = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        cd = new ConnectionDetector(getApplicationContext());
        queryUserDB();
        mFuelContext = getApplicationContext();
//        adapter = new TableAdapter(this);

        if (sp.getString("ip_adds", "") != null) {
            if (sp.getString("ip_adds", "").trim().length() > 0) {
                Const.API_URL = sp.getString("ip_adds", "");
            }
        }

        mStartDate = Const.getTripYesterdayDate2();
        Log.d("mStartDate",""+mStartDate);
        long timeInMillis = System.currentTimeMillis();
        Calendar cal1 = Calendar.getInstance();
        cal1.setTimeInMillis(timeInMillis);

        Calendar cal = Calendar.getInstance();
//        Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);

        bottomSheet = findViewById(R.id.bottom_sheet);
        behavior = BottomSheetBehavior.from(bottomSheet);
        behavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                // React to state change
//                System.out.println("Hi state 0000" + bottomSheetEnabled);


            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
                // React to dragging events
            }
        });

        adapter = new TableAdapter(this);
        setBottomLayoutData();

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mSelectedGroup = mGroupList.get(position);
                saveSelectedGroup(mGroupSpinnerList.get(position));
                String url = null;

                url = Const.API_URL + "/mobile/getConsolidatedFuelReport?userId=" + Constant.SELECTED_USER_ID + "&groupName=" + mSelectedGroup + "&date=" + mStartDate;

                if (cd.isConnectingToInternet()) {
                    new getFuelSummaryData().execute(url);
                } else {
                    Toast.makeText(getApplicationContext(),
                            "Please check your network connection",
                            Toast.LENGTH_SHORT).show();
                }
                //url = Const.API_URL + "/mobile/getConsolidatedFuelReport?userId=" + Constant.SELECTED_USER_ID + "&groupName=" + mSelectedGroup + "&date=" + mStartDate;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

//        if (cd.isConnectingToInternet()) {
//
//            new getFuelSummaryData().execute(url);
//
//        } else {
//            Toast.makeText(getApplicationContext(),
//                    "Please check your network connection",
//                    Toast.LENGTH_SHORT).show();
//        }


    }

    private void queryUserDB() {
        mGroupList = Constant.mUserGroupList;

        setDropDownData();
    }

    private void setDropDownData() {
        if (mGroupList.size() > 0) {

        } else {
            mGroupList.add("Select");
        }

        for (int i = 0; i < mGroupList.size(); i++) {

            String[] str_msg_data_array = mGroupList.get(i).split(":");

            mGroupSpinnerList.add(str_msg_data_array[0]);

        }


        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(
                FuelSummary.this, R.layout.spinner_fuel,
                mGroupSpinnerList) {

            public View getView(int position, View convertView, ViewGroup parent) {
                View v = super.getView(position, convertView, parent);
                // TextView v = (TextView) super.getView(position,
                // convertView,parent);
                // Constant.face(DailyCalls.this, (TextView) v);
                return v;
            }

            public View getDropDownView(int position, View convertView,
                                        ViewGroup parent) {
                View v = super.getDropDownView(position, convertView, parent);
                // ((TextView)
                // v).setBackgroundColor(Color.parseColor("#BBfef3da"));
                // TextView v = (TextView) super.getView(position,
                // convertView,parent);
                // Constant.face(DailyCalls.this, (TextView) v);
                return v;
            }
        };
        spinnerArrayAdapter
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(spinnerArrayAdapter);

        String groupName = sp.getString("selected_group", "");
        int spinnerPosition = spinnerArrayAdapter.getPosition(groupName);

//set the default according to value
        spinner.setSelection(spinnerPosition);
    }

    private void setBottomLayoutData() {

            mTxtFromDate.setText(mStartDate);



    }

    private void saveSelectedGroup(String mGroupName) {
        try {
            SharedPreferences.Editor editor = sp.edit();
            editor.putString("selected_group", mGroupName);
            editor.commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }



    private void init() {
        fuel_back = (ImageView)findViewById(R.id.fuel_summary_view_Back);
        fuel_calendar = (ImageView)findViewById(R.id.fuel_summary_change_date);
        fuel_calendar.setColorFilter(Color.WHITE);
        mTxtNoRecord =(TextView)findViewById(R.id.temperature_report_no_record_txt);
        mTxtFromDate = (TextView)findViewById(R.id.txt_start_date_value);
        //mTxtEndDate = (TextView)findViewById(R.id.txt_end_date_value);
        mTxtconsumption = (TextView)findViewById(R.id.total_consumption);
        mTxtfill = (TextView)findViewById(R.id.total_fill);
        mTxttheft = (TextView)findViewById(R.id.total_theft);
        mTextdist = (TextView)findViewById(R.id.total_distance);
        spinner = (Spinner) findViewById(R.id.spinner);
        mTxtdate = (TextView)findViewById(R.id.total_date);
        card_layout =(LinearLayout)findViewById(R.id.card_layout);
        mButton = (Button)findViewById(R.id.btn_done);
        fuel_back.setColorFilter(Color.WHITE);
        fuel_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(FuelSummary.this,VehicleListActivity.class));
            }
        });
        fuel_calendar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!bottomSheetEnabled) {
                    behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                } else {
                    // setData();
                    behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                }

                if (bottomSheetEnabled) {
                    bottomSheetEnabled = false;
                } else {
                    bottomSheetEnabled = true;
                }
            }
        });

        mTxtFromDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isFromDate = true;
//                showDialog(DATE_DIALOG_ID);

                showDateDialog();
            }
        });

        mButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                String url = null;
                Log.d("mStartDateee",""+mStartDate);
                url = Const.API_URL + "/mobile/getConsolidatedFuelReport?userId=" + Constant.SELECTED_USER_ID + "&groupName=" + mSelectedGroup + "&date=" + mStartDate;

                if (cd.isConnectingToInternet()) {

                    new getFuelSummaryData().execute(url);
                } else {
                    Toast.makeText(getApplicationContext(),
                            "Please check your network connection",
                            Toast.LENGTH_SHORT).show();
                }
            }
        });


        mFuelDataMapLayout = (LinearLayout) findViewById(R.id.temperature_data_map_view_layout);

        lv = (ListView) findViewById(R.id.tstoppage_report_listView1);

//
//        mTxtVehicleName = (TextView) findViewById(R.id.selected_vehicle_txt);
//        mTxtVehicleName.setText(Constant.SELECTED_VEHICLE_SHORT_NAME);




        lv.setVisibility(View.VISIBLE);
    }

    private void showDateDialog() {
        if (isFromDate) {

            String[] dateArray = mStartDate.split("-");
            year = Integer.parseInt(dateArray[0].trim());
            month = Integer.parseInt(dateArray[1].trim()) - 1;
            day = Integer.parseInt(dateArray[2].trim());
        } else {

            String[] dateArray = mEndDate.split("-");
            year = Integer.parseInt(dateArray[0].trim());
            month = Integer.parseInt(dateArray[1].trim()) - 1;
            day = Integer.parseInt(dateArray[2].trim());

        }
//        System.out.println("Hi year " + year + " mo " + month + " da " + day);
//        final Calendar c = Calendar.getInstance();
//        year = c.get(Calendar.YEAR);
//        month = c.get(Calendar.MONTH);
//        day = c.get(Calendar.DAY_OF_MONTH);
        /** set date picker as current date */
        DatePickerDialog dialog = new DatePickerDialog(this,
                datePickerListener, year, month, day);
        dialog.show();
    }

    private DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {

        // when dialog box is closed, below method will be called.
        public void onDateSet(DatePicker view, int selectedYear,
                              int selectedMonth, int selectedDay) {

            year = selectedYear;
            month = selectedMonth;
            day = selectedDay;
            StringBuilder mDate = new StringBuilder().append(year).append(":")
                    .append(getMonthValue(month + 1)).append(":").append(getMonthValue(day))
                    .append("");

            StringBuilder mDate2 = new StringBuilder().append(year).append("-")
                    .append(getMonthValue(month + 1)).append("-").append(getMonthValue(day))
                    .append(" ");
            if (isFromDate) {
                mStartDate = String.valueOf(mDate2);
                //  mStartTimeValue=String.valueOf(mDate2);
                Log.d("fuelDate2",""+mDate2);
                mTxtFromDate.setText(mDate2);
                mTxtdate.setText(mDate2);
            } else {
                mEndDate = String.valueOf(mDate2);

//                mTxtEndDate.setText(mDate2);
            }
//            mFromDate = String.valueOf(mDate);
//            mFromDateTxtValue = String.valueOf(mDate2);
//            mTxtDate.setText(mDate2);
            // }
        }
    };

    private String getMonthValue(int month) {
        String mMonthValue = String.valueOf(month);
        if (mMonthValue.length() == 1) {
            mMonthValue = "0" + mMonthValue;
        }
        return mMonthValue;
    }


    private class getFuelSummaryData extends AsyncTask<String, Integer, String> {
        ProgressDialog progressDialog;

        @Override
        protected String doInBackground(String... urls) {
            // TODO Auto-generated method stub

            String result = null;
            try {
                String response_from_server = "";
                try {

                    //   System.out.println("The pull vehicle info url is ::::"+urls[0]);
                    HttpConfig ht = new HttpConfig();
                    response_from_server = ht.httpGet(urls[0]);

                } catch (Exception e) {
                    e.printStackTrace();
                }
                return response_from_server;
                //result = ht.httpGet(Const.API_URL + "/mobile/getConsolidatedFuelReport?userId=" + Constant.SELECTED_USER_ID + "&groupName=" + Constant.SELECTED_GROUP + "&date=" + mStartDate);

            } catch (Exception e) {

            }

            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            progressDialog.dismiss();
            mFuelFillData = new ArrayList<FuelsumDto>();
//            System.out.println("The Fuel fill result is ::::" + result);
            if (result != null && result.length() > 0) {
                lv.setVisibility(View.VISIBLE);
                card_layout.setVisibility(View.VISIBLE);
                mTxtNoRecord.setVisibility(View.GONE);
                mFuelFillData.clear();
                totaldisance = 0;
                totaltheft = 0;
                totalfill = 0;
                totconsum = 0;
                Log.d("smssummary","success");
   try {
       JSONArray jArr = new JSONArray(result.trim());

       for (int i = 0; i < jArr.length(); i++) {
           JSONObject jsonCameraObject = jArr.getJSONObject(i);
           FuelsumDto c = new FuelsumDto();


           if (jsonCameraObject.has("vehicleId")) {

               c.setVehicleId(jsonCameraObject.getString("vehicleId"));
           }
           if (jsonCameraObject.has("vehicleName")) {

               c.setVehicleName(jsonCameraObject.getString("vehicleName").trim());
           }
           if (jsonCameraObject.has("startFuel")) {

               c.setStartFuel(jsonCameraObject.getString("startFuel"));
           }
           if (jsonCameraObject.has("endFuel")) {

               c.setEndFuel(jsonCameraObject.getString("endFuel"));
           }
           if (jsonCameraObject.has("date")) {

               c.setDate(jsonCameraObject.getString("date"));

           }
           if (jsonCameraObject.has("fuelConsumption")) {

               c.setFuelConsumption(jsonCameraObject.getString("fuelConsumption"));
               totconsum = totconsum +Float.parseFloat(jsonCameraObject.getString("fuelConsumption"));
           }
           if (jsonCameraObject.has("fuelFilling")) {

               c.setFuelfilling(jsonCameraObject.getString("fuelFilling"));
               totalfill = totalfill +Float.parseFloat(jsonCameraObject.getString("fuelFilling"));
           }

           if (jsonCameraObject.has("fuelTheft")) {

               c.setFuelTheft(jsonCameraObject.getString("fuelTheft"));
               totaltheft = totaltheft + Float.parseFloat(jsonCameraObject.getString("fuelTheft"));
           }

           if (jsonCameraObject.has("dist")) {

               c.setDist(jsonCameraObject.getString("dist"));
               totaldisance = totaldisance + Float.parseFloat(jsonCameraObject.getString("dist"));
           }
           if (jsonCameraObject.has("kmpl")) {

               c.setKmpl(jsonCameraObject.getString("kmpl"));
           }

           mFuelFillData.add(c);


       }
       Log.d("totVal",""+totconsum);
   } catch (JSONException e){
       e.printStackTrace();
   }
                setTableLayoutData();
            } else {
                Log.d("smssummary","failure");
                card_layout.setVisibility(View.GONE);
                lv.setVisibility(View.GONE);
                mTxtNoRecord.setVisibility(View.VISIBLE);
                Toast.makeText(getApplicationContext(), "No record found.Please select different date time or group", Toast.LENGTH_SHORT).show();
            }

        }

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            progressDialog = new ProgressDialog(FuelSummary.this,
                    AlertDialog.THEME_HOLO_LIGHT);
            progressDialog.setMessage("Please Wait...");
            progressDialog.setProgressDrawable(new ColorDrawable(
                    Color.BLUE));
            progressDialog.setCancelable(true);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.show();
        }

    }


    public void setTableLayoutData() {

        if (mFuelFillData.size() > 0) {
            lv.setVisibility(View.VISIBLE);
            card_layout.setVisibility(View.VISIBLE);
            mTxtNoRecord.setVisibility(View.GONE);
            mTxttheft.setText(String.valueOf(new DecimalFormat("##.##").format(totaltheft)) + " ltrs");
            mTxtfill.setText(String.valueOf(new DecimalFormat("##.##").format(totalfill)) + " ltrs");
            mTxtconsumption.setText(String.valueOf(new DecimalFormat("##.##").format(totconsum)) + " ltrs");
            mTextdist.setText(String.valueOf(new DecimalFormat("##.##").format(totaldisance)) + " Kms");
            Log.d("mStartDate",""+mStartDate);
            mTxtdate.setText(mStartDate);

            if (isHeaderPresent) {

            } else {
                lv.addHeaderView(adapter.getHeaderView(lv));
                isHeaderPresent = true;
            }


            lv.setAdapter(adapter);
            adapter.setData(mFuelFillData);

            adapter.notifyDataSetChanged();


        } else {
            mFuelFillData.clear();
            card_layout.setVisibility(View.GONE);
            lv.setVisibility(View.GONE);

            mTxtNoRecord.setVisibility(View.VISIBLE);
            Toast.makeText(getApplicationContext(), "No record found.Please select different date time or group", Toast.LENGTH_SHORT).show();

        }


    }


    public class TableAdapter extends BaseAdapter {

        // private static final String TAG = "TableAdapter";

        private Context mContext;

        private String[] mHeader;


        private int mCurrentScroll;

        private int[] mColResources = {R.id.fuelsum_vehicle_textView,
                R.id.fuelsumm_start_textView,R.id.fuelsumm_fuel_fill_textView, R.id.fuelsumm_fuel_consume_textView, R.id.fuelsumm_end_textView, R.id.fuelsumm_fuel_theft_textView,
                R.id.fuelsumm_fuel_distance_textView,R.id.fuelsumm_fuel_milage_textView};

        public TableAdapter(Context context) {
            super();
            mContext = context;
        }

        @Override
        public int getCount() {
            return mFuelFillData != null ? mFuelFillData.size() : 0;
        }

        @Override
        public Object getItem(int position) {
            return mFuelFillData.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {

            HorizontalScrollView view = null;
//            LinearLayout view = null;

            if (convertView == null) {
                LayoutInflater inflater = (LayoutInflater) mContext
                        .getSystemService(LAYOUT_INFLATER_SERVICE);
                view = (HorizontalScrollView) inflater.inflate(
                        R.layout.fuelsummary_list_item_table_row, parent, false);

//                view = (LinearLayout) inflater.inflate(
//                        R.layout.fuelfill_list_item_table_row_new, parent, false);


//                view.setOnScrollChangeListener(new OnScrollListener() {
//                    @Override
//                    public void onScrollChanged(View view, int scrollX) {
//
//                    }
//                });


                view.setOnScrollListener(new HorizontalScrollView.OnScrollListener() {

                    @Override
                    public void onScrollChanged(View scrollView, int scrollX) {

                        mCurrentScroll = scrollX;
                        ListView listView = (ListView) scrollView.getParent();
                        if (listView == null)
                            return;

                        for (int i = 0; i < listView.getChildCount(); i++) {
                            View child = listView.getChildAt(i);
                            if (child instanceof HorizontalScrollView
                                    && child != scrollView) {
                                HorizontalScrollView scrollView2 = (HorizontalScrollView) child;
                                if (scrollView2.getScrollX() != mCurrentScroll) {
                                    scrollView2.setScrollX(mCurrentScroll);
                                }
                            }
                        }
                    }
                });

            } else {
                view = (HorizontalScrollView) convertView;
//                view = (LinearLayout) convertView;
            }

            view.setScrollX(mCurrentScroll);

            if (position % 2 == 0) {
                view.setBackgroundColor(Color.WHITE);
            } else {
                view.setBackgroundColor(Color.LTGRAY);
            }

            FuelsumDto data = mFuelFillData.get(position);

//            for (int i = 0; i < mColResources.length; i++) {
//            TextView col1 = (TextView) view.findViewById(mColResources[0]);
//            Log.d("datevlue",""+data.getDate());
//            col1.setText(data.getDate());

            TextView col2 = (TextView) view.findViewById(mColResources[0]);
            col2.setText(data.getVehicleName());
            TextView col3 = (TextView) view.findViewById(mColResources[1]);
            col3.setText(data.getStartFuel());

            TextView col4 = (TextView) view.findViewById(mColResources[4]);
            col4.setText(data.getEndFuel());

            TextView col5 = (TextView) view.findViewById(mColResources[3]);
            col5.setText(data.getFuelConsumption());

//            for (String number : numbers){
//                Integer n = Integer.valueOf(number);
//                sum += n;
//            }
            TextView col6 = (TextView) view.findViewById(mColResources[2]);
            col6.setText(data.getFuelfilling());

            TextView col7 = (TextView) view.findViewById(mColResources[5]);
            col7.setText(data.getFuelTheft());

            TextView col8 = (TextView) view.findViewById(mColResources[6]);
            col8.setText(data.getDist());

            TextView col9 = (TextView) view.findViewById(mColResources[7]);
            col9.setText(data.getKmpl());

            if (data.getDist().equalsIgnoreCase("0.0")){
                col2.setTextColor(Color.parseColor("#fa315b"));
                col3.setTextColor(Color.parseColor("#fa315b"));
                col4.setTextColor(Color.parseColor("#fa315b"));
                col5.setTextColor(Color.parseColor("#fa315b"));
                col6.setTextColor(Color.parseColor("#fa315b"));
                col7.setTextColor(Color.parseColor("#fa315b"));
                col8.setTextColor(Color.parseColor("#fa315b"));
                col9.setTextColor(Color.parseColor("#fa315b"));
            }  else {
                col2.setTextColor(Color.BLACK);
                col3.setTextColor(Color.BLACK);
                col4.setTextColor(Color.BLACK);
                col5.setTextColor(Color.BLACK);
                col6.setTextColor(Color.BLACK);
                col7.setTextColor(Color.BLACK);
                col8.setTextColor(Color.BLACK);
                col9.setTextColor(Color.BLACK);
            }




//            LinearLayout.LayoutParams col1Params = new LinearLayout.LayoutParams(
//                    LinearLayout.LayoutParams.WRAP_CONTENT,
//                    LinearLayout.LayoutParams.WRAP_CONTENT);
//          //  col1Params.width = width * 35 / 100;
////            col1.setLayoutParams(col1Params);
////            col1.setPadding(width * 2 / 100, 0, 0, 0);
////            col1.setGravity(Gravity.CENTER | Gravity.LEFT);
//
//            LinearLayout.LayoutParams col245Params = new LinearLayout.LayoutParams(
//                    LinearLayout.LayoutParams.WRAP_CONTENT,
//                    LinearLayout.LayoutParams.WRAP_CONTENT);
//            col245Params.width = width * 30 / 100;
////            col245Params.height = height * 7 / 100;
//            col2.setLayoutParams(col245Params);
//            col2.setPadding(width * 2 / 100, 0, 0, 0);
//            col2.setGravity(Gravity.CENTER | Gravity.LEFT);
//
//            col4.setLayoutParams(col245Params);
//            col4.setPadding(width * 2 / 100, 0, 0, 0);
//            col4.setGravity(Gravity.CENTER | Gravity.LEFT);
//
//            col5.setLayoutParams(col245Params);
//            col5.setPadding(width * 2 / 100, 0, 0, 0);
//            col5.setGravity(Gravity.CENTER | Gravity.LEFT);
//
//            col6.setLayoutParams(col245Params);
//            col6.setPadding(width * 2 / 100, 0, 0, 0);
//            col6.setGravity(Gravity.CENTER | Gravity.LEFT);
//
//            col7.setLayoutParams(col245Params);
//            col7.setPadding(width * 2 / 100, 0, 0, 0);
//            col7.setGravity(Gravity.CENTER | Gravity.LEFT);
//
//            col8.setLayoutParams(col245Params);
//            col8.setPadding(width * 2 / 100, 0, 0, 0);
//            col8.setGravity(Gravity.CENTER | Gravity.LEFT);
//
//            col9.setLayoutParams(col245Params);
//            col9.setPadding(width * 2 / 100, 0, 0, 0);
//            col9.setGravity(Gravity.CENTER | Gravity.LEFT);
//
//
//            LinearLayout.LayoutParams col3Params = new LinearLayout.LayoutParams(
//                    LinearLayout.LayoutParams.WRAP_CONTENT,
//                    LinearLayout.LayoutParams.WRAP_CONTENT);
//            col3Params.width = width * 40 / 100;
////            col3Params.height = height * 7 / 100;
//            col3.setLayoutParams(col3Params);
//            col3.setPadding(width * 2 / 100, 0, 0, 0);
//            col3.setGravity(Gravity.CENTER | Gravity.LEFT);


            return view;
        }

        public View getHeaderView(ViewGroup parent) {

            LayoutInflater inflater = (LayoutInflater) mContext
                    .getSystemService(LAYOUT_INFLATER_SERVICE);
            HorizontalScrollView view = (HorizontalScrollView) inflater
                    .inflate(R.layout.fuelsummary_list_item_table_header, parent, false);

//            LinearLayout view = (LinearLayout) inflater
//                    .inflate(R.layout.fuelfill_list_item_table_header_new, parent, false);

            view.setOnScrollListener(new HorizontalScrollView.OnScrollListener() {

                @Override
                public void onScrollChanged(View scrollView, int scrollX) {

                    mCurrentScroll = scrollX;
                    ListView listView = (ListView) scrollView.getParent();
                    if (listView == null)
                        return;

                    for (int i = 0; i < listView.getChildCount(); i++) {
                        View child = listView.getChildAt(i);
                        if (child instanceof HorizontalScrollView
                                && child != scrollView) {
                            HorizontalScrollView scrollView2 = (HorizontalScrollView) child;
                            if (scrollView2.getScrollX() != mCurrentScroll) {
                                scrollView2.setScrollX(mCurrentScroll);
                            }
                        }
                    }
                }
            });

            TextView col2 = (TextView) view.findViewById(R.id.fuelsumm_vehicle_textView_header);
            TextView col3 = (TextView) view.findViewById(R.id.fuelsumm_start_fue_textView_header);
            TextView col4 = (TextView) view.findViewById(R.id.fuelsumm_end_fuel_textView_header);
            TextView col5 = (TextView) view.findViewById(R.id.fuelsumm_fuel_consume_textView_header);
            TextView col6 = (TextView) view.findViewById(R.id.fuelsumm_fuel_fill_textView_header);
            TextView col7 = (TextView) view.findViewById(R.id.fuelsumm_fuel_theft_textView_header);
            TextView col8 = (TextView) view.findViewById(R.id.fuelsumm_fuel_distance_textView_header);
            TextView col9 = (TextView) view.findViewById(R.id.fuelsumm_fuel_milage_textView_header);


//            LinearLayout.LayoutParams col1Params = new LinearLayout.LayoutParams(
//                    LinearLayout.LayoutParams.WRAP_CONTENT,
//                    LinearLayout.LayoutParams.WRAP_CONTENT);
//            col1Params.width = width * 35 / 100;
////            col1Params.height = height * 7 / 100;
//            col1.setLayoutParams(col1Params);
//            col1.setPadding(width * 2 / 100, 0, 0, 0);
//            col1.setGravity(Gravity.CENTER | Gravity.LEFT);
//
//            LinearLayout.LayoutParams col245Params = new LinearLayout.LayoutParams(
//                    LinearLayout.LayoutParams.WRAP_CONTENT,
//                    LinearLayout.LayoutParams.WRAP_CONTENT);
//            col245Params.width = width * 30 / 100;
////            col245Params.height = height * 7 / 100;
//            col2.setLayoutParams(col245Params);
//            col2.setPadding(width * 2 / 100, 0, 0, 0);
//            col2.setGravity(Gravity.CENTER | Gravity.LEFT);
//
//            col4.setLayoutParams(col245Params);
//            col4.setPadding(width * 2 / 100, 0, 0, 0);
//            col4.setGravity(Gravity.CENTER | Gravity.LEFT);
//
//            col5.setLayoutParams(col245Params);
//            col5.setPadding(width * 2 / 100, 0, 0, 0);
//            col5.setGravity(Gravity.CENTER | Gravity.LEFT);
//
//            col6.setLayoutParams(col245Params);
//            col6.setPadding(width * 2 / 100, 0, 0, 0);
//            col6.setGravity(Gravity.CENTER | Gravity.LEFT);
//
//            col7.setLayoutParams(col245Params);
//            col7.setPadding(width * 2 / 100, 0, 0, 0);
//            col7.setGravity(Gravity.CENTER | Gravity.LEFT);
//
//            col8.setLayoutParams(col245Params);
//            col8.setPadding(width * 2 / 100, 0, 0, 0);
//            col8.setGravity(Gravity.CENTER | Gravity.LEFT);
//
//            col9.setLayoutParams(col245Params);
//            col9.setPadding(width * 2 / 100, 0, 0, 0);
//            col9.setGravity(Gravity.CENTER | Gravity.LEFT);
//
//
//            LinearLayout.LayoutParams col3Params = new LinearLayout.LayoutParams(
//                    LinearLayout.LayoutParams.WRAP_CONTENT,
//                    LinearLayout.LayoutParams.WRAP_CONTENT);
//            col3Params.width = width * 40 / 100;
////            col3Params.height = height * 7 / 100;
//            col3.setLayoutParams(col3Params);
//            col3.setPadding(width * 2 / 100, 0, 0, 0);
//            col3.setGravity(Gravity.CENTER | Gravity.LEFT);

            return view;
        }

//        public void setHeader(String[] header) {
//            mHeader = header;
//        }

        public void setData(List<FuelsumDto> data) {
            mFuelFillData = data;
            notifyDataSetChanged();
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();


        startActivity(new Intent(FuelSummary.this, MapMenuActivity.class));
        finish();
    }
}
