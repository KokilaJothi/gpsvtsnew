package com.vamosys.vamos;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;

import org.osmdroid.tileprovider.MapTileProviderBasic;
import org.osmdroid.tileprovider.tilesource.ITileSource;
import org.osmdroid.tileprovider.tilesource.XYTileSource;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.TilesOverlay;

/**
 * @author Alex van der Linden
 */
public class SampleWithTilesOverlayAndCustomTileSource extends Activity {

    // ===========================================================
    // Constants
    // ===========================================================

    // ===========================================================
    // Fields
    // ===========================================================
    MapView mMapView;
    // ===========================================================
    // Constructors
    // ===========================================================

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        setContentView(R.layout.osm_sample_test_tile_source);


        // Setup base map
        RelativeLayout rl = (RelativeLayout) findViewById(R.id.relativelayout);

        mMapView = new MapView(this);
        mMapView.setTilesScaledToDpi(true);
        rl.addView(mMapView, new LayoutParams(LayoutParams.FILL_PARENT,
                LayoutParams.FILL_PARENT));
        mMapView.setBuiltInZoomControls(true);
        mMapView.setMultiTouchControls(true);
        // zoom to the netherlands
        mMapView.getController().setZoom(5);
        mMapView.getController().setCenter(new GeoPoint(19.09767333, 72.91014222));
//        latitude":"19.09767333","longitude":"72.91014222
        // Add tiles layer with custom tile source
        final MapTileProviderBasic tileProvider = new MapTileProviderBasic(getApplicationContext());
//		final ITileSource tileSource = new XYTileSource("FietsRegionaal",  3, 18, 256, ".png",
//				new String[] { "http://overlay.openstreetmap.nl/openfietskaart-rcn/" });
        final ITileSource tileSource = new XYTileSource("FietsRegionaal", 1, 18, 256, ".png",
                new String[]{"http://207.154.194.241/osm_tiles/"});
        tileProvider.setTileSource(tileSource);
        final TilesOverlay tilesOverlay = new TilesOverlay(tileProvider, this.getBaseContext());
        tilesOverlay.setLoadingBackgroundColor(Color.TRANSPARENT);
        mMapView.getOverlays().add(tilesOverlay);


    }

    // ===========================================================
    // Getter & Setter
    // ===========================================================

    // ===========================================================
    // Methods from SuperClass/Interfaces
    // ===========================================================

    // ===========================================================
    // Methods
    // ===========================================================
    @Override
    public void onPause() {
        super.onPause();
//		mMapView.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
//		mMapView.onResume();
    }
    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================
}
