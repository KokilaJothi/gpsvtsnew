package com.vamosys.vamos;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.preference.PreferenceManager;
import android.provider.Settings;
import androidx.core.app.ActivityCompat;
import android.telephony.SmsMessage;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.SpannableString;
import android.text.TextWatcher;
import android.text.method.PasswordTransformationMethod;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import com.google.gson.Gson;
import com.vamosys.imageLoader.ImageLoader;
import com.vamosys.notification.NotificationUtils;
//import com.vamosys.services.RegistrationService;
import com.vamosys.utils.CommonManager;
import com.vamosys.utils.ConnectionDetector;
import com.vamosys.utils.Constant;
import com.vamosys.utils.DaoHandler;
import com.vamosys.utils.HttpConfig;
import com.vamosys.utils.MyCustomProgressDialog;
import com.vamosys.utils.TypefaceUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class RegistrationPage extends Activity {
    public static final int NETWORK_SETTINGS = 3;
    public static final String SMS_RECEIVED = "android.provider.Telephony.SMS_RECEIVED";
    private static final String TAG = "SMART";
    private static final int DIALOG1_KEY = 0;
    private static final int OTP_DIALOG = 1;
    EditText mobile_number, password, mEdtIpAdds, forget_user;
    TextView countrycodetext, label1, label2, forgot,privacypolicy;
    Button login, reset;
    ImageView mAppLogoTest, close;
    ConnectionDetector cd;
    boolean isInternetPresent = false;
    Context context = this;
    Dialog dialog;
    /***/
    DBHelper dbhelper;
    CheckBox iAgree, mChkShowPassword, mChkChangeIp;
    /* Variables Declarations**/ String imeinumber, mPhoneNumber, isocode, macid, appID, gcmToken;
    /**
     * progress dialog
     */
    ProgressDialog progressDialog, otpdialog;
    /***/
    long timeInMilliseconds = 0L;
    long timeSwapBuff = 0L;
    long updatedTime = 0L;
    int waiting_min = 0;
    String mGcmToken;
    //Boolean shown_dialog;
    SharedPreferences sp;
    TextView id_registration_text_below_logo;
    int registration_timeout = 5000;

    boolean mNotificationEnableStatus = true;
    String mAppLogo = null, mIpAddsValue = null;

    /**
     * Timer code
     */
    private long startTime = 0L;
    BroadcastReceiver receiver_SMS = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(SMS_RECEIVED)) {
                Bundle bundle = intent.getExtras();
                if (bundle != null) {
                    Object[] pdus = (Object[]) bundle.get("pdus");
                    SmsMessage[] messages = new SmsMessage[pdus.length];
                    for (int i = 0; i < pdus.length; i++)
                        messages[i] = SmsMessage.createFromPdu((byte[]) pdus[i]);
                    for (SmsMessage message : messages) {
                        if (message.getDisplayOriginatingAddress().contains("VAMOSS")) {
                            String vamos_message = message.getDisplayMessageBody();
                            Toast.makeText(context, "----" + message.getDisplayOriginatingAddress(), Toast.LENGTH_LONG).show();
                            String substr = vamos_message.substring(vamos_message.length() - 4);
                            mobile_number.setText(substr);
                            startTime = 0L;
                            timeInMilliseconds = 0L;
                            timeSwapBuff = 0L;
                            updatedTime = 0L;
                            if (mobile_number.length() == 4) {
                                new SendOTP().execute(mPhoneNumber, mobile_number.getText().toString());
                            }
                        }
                    }
                }
            }
        }
    };
    private Handler customHandler = new Handler();
    private Runnable updateTimerThread = new Runnable() {
        public void run() {
            timeInMilliseconds = SystemClock.uptimeMillis() - startTime;
            updatedTime = timeSwapBuff + timeInMilliseconds;
            int secs = (int) (updatedTime / 1000);
            waiting_min = secs / 60;
            secs = secs % 60;
            int milliseconds = (int) (updatedTime % 1000);
            if (waiting_min >= 0.5) {
                {
                    customHandler.removeCallbacks(updateTimerThread);
                    Toast.makeText(context, ""+getResources().getString(R.string.enter_otp), Toast.LENGTH_LONG).show();
                    login.setEnabled(true);
                }
            } else {
                customHandler.postDelayed(this, 0);
            }
        }
    };
    ImageLoader imgLoader;
//    String query1 = "", query2 = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration_page);
        dbSetup();
        sp = PreferenceManager.getDefaultSharedPreferences(context);
        cd = new ConnectionDetector(getApplicationContext());
        imgLoader = new ImageLoader(RegistrationPage.this);
        mNotificationEnableStatus = sp.getBoolean("notification_enable", true);
        isMyServiceRunning(VehicleTrackingService.class);
//        CommonManager com = new CommonManager(getApplicationContext());
//        com.deleteUserDB();
        NotificationUtils.clearNotifications();
        DaoHandler da = new DaoHandler(getApplicationContext(), true);
        da.deleteUserDB();


//        try {
//
//            String mobileModel = android.os.Build.MODEL + "(" + android.os.Build.PRODUCT
//                    + ")";
//            String mobilApiLevel = android.os.Build.VERSION.RELEASE + "("
//                    + android.os.Build.VERSION.SDK_INT + ")";
//            query1 = URLEncoder.encode(mobileModel, "utf-8");
//            query2 = URLEncoder.encode(mobilApiLevel, "utf-8");
//        } catch (Exception e) {
//            e.printStackTrace();
//            query1 = "";
//            query2 = "";
//        }


        mAppLogoTest = (ImageView) findViewById(R.id.id_applogo_test);
        mobile_number = (EditText) findViewById(R.id.id_mobile_number);
        password = (EditText) findViewById(R.id.id_password);

        mEdtIpAdds = (EditText) findViewById(R.id.id_edt_ip_adds);

        countrycodetext = (TextView) findViewById(R.id.id_country_code);
        label1 = (TextView) findViewById(R.id.id_login_label1);
        label2 = (TextView) findViewById(R.id.id_login_label2);
        forgot = (TextView) findViewById(R.id.forgot_password);
        privacypolicy = (TextView)findViewById(R.id.privacy_policy);
        login = (Button) findViewById(R.id.id_login_btn);
        id_registration_text_below_logo = (TextView) findViewById(R.id.id_registration_text_below_logo);
        iAgree = (CheckBox) findViewById(R.id.id_iagree);
        mChkShowPassword = (CheckBox) findViewById(R.id.reg_chk_show_pwd);
        mChkChangeIp = (CheckBox) findViewById(R.id.reg_chk_change_ipadds);
        // mChkChangeIp.setChecked(false);
        //Setting typeface
        mobile_number.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        password.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        countrycodetext.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        label1.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        label2.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        forgot.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        login.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        id_registration_text_below_logo.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        iAgree.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        mChkShowPassword.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        mChkChangeIp.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        password.setVisibility(View.GONE);
        mEdtIpAdds.setVisibility(View.GONE);
        login.setEnabled(false);
        appID = getApplicationContext().getPackageName();
//        appID = getApplicationContext().getV
        WifiManager m_wm = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        macid = m_wm.getConnectionInfo().getMacAddress();
        if (appID != null && !appID.equals("")) {
            dbhelper.update_Settings_StdTableValues("appid", appID);
        }
        if (macid != null && !macid.equals("")) {
            dbhelper.update_Settings_StdTableValues("macid", macid);
        }
        gcmToken = sp.getString("gcmToken", null);
        Log.d("logingcmToken", "" + gcmToken);

//        iAgree.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                if (iAgree.isChecked()) {
//                    startActivity(new Intent(RegistrationPage.this, WebViewActivity.class));
//                }
//            }
//        });
        SpannableString content = new SpannableString("I agree to the Terms and Conditions");
        content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
        privacypolicy.setText(content);
        privacypolicy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(RegistrationPage.this, WebViewActivity.class));
            }
        });


        /** View click listener*/
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                mIpAddsValue = mEdtIpAdds.getText().toString().trim();

                if (mIpAddsValue != null) {
                    if (mIpAddsValue.trim().length() > 0) {
                        Const.API_URL = mIpAddsValue;
                    }
                }
                if (login.getText().toString().equalsIgnoreCase(getResources().getString(R.string.continue_word))) {
                    if (!iAgree.isChecked()) {
                        showTermsAndConditionDialog();
                        return;
                    }
                    if (checkNetworkSettings()) {
                        String strUserName = mobile_number.getText().toString();
                        if (strUserName.length() > 0) {
                            strUserName = strUserName.trim();
                        }
                        saveUserName(strUserName);
                        new SendMobileNumber().execute(strUserName);
                    }
                } else if (login.getText().toString().equalsIgnoreCase(getResources().getString(R.string.signin))) {
                    if (password.getVisibility() == View.VISIBLE) {
                        if (!iAgree.isChecked()) {
                            showTermsAndConditionDialog();
                            return;
                        }
                        if (mobile_number.length() > 0 && password.length() > 0) {
                            String strUserName = mobile_number.getText().toString();
                            if (strUserName.length() > 0) {
                                strUserName = strUserName.trim();
                            }
                            saveUserName(strUserName);
                            try {
                                new SendUserCredentials().execute(strUserName, URLEncoder.encode(password.getText().toString(),"UTF-8"));
                            } catch (UnsupportedEncodingException e) {
                                e.printStackTrace();
                            }
                        } else {
                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.enter_username_password), Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        if (mobile_number.length() == 4) {
                            String strOTP = mobile_number.getText().toString();
                            if (strOTP.length() > 0) {
                                strOTP = strOTP.trim();
                            }
                            new SendOTP().execute(mPhoneNumber, strOTP);
                        }
                    }
                }
            }
        });
        /***/
        mobile_number.addTextChangedListener(new TextWatcher() {
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            public void afterTextChanged(Editable s) {
                String data = s.toString();
                if (s.length() > 0) {
                    data = data.trim();
                } else {
                    return;
                }
                // you can call or do what you want with your EditText here
//                if (GeneralUtils.isNumeric(data)) {
//                    if (label1.getText().toString().equalsIgnoreCase("Waiting for Verification code")) {
//                        login.setText(getResources().getString(R.string.signin));
//                        login.setEnabled(true);
//                    } else {
//                        login.setEnabled(false);
//                        login.setText(getResources().getString(R.string.continue_word));
//                    }
//                    countrycodetext.setVisibility(View.GONE);
//                    password.setVisibility(View.GONE);
//                    if (s.length() >= 10) {
//                        InputMethodManager imm1 = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
//                        imm1.hideSoftInputFromWindow(mobile_number.getWindowToken(), 0);
//                        mPhoneNumber = mobile_number.getText().toString();
//                        login.setEnabled(true);
//                        password.setVisibility(View.GONE);
//                    }
//                } else {
                login.setText(getResources().getString(R.string.signin));
                login.setEnabled(true);
                countrycodetext.setVisibility(View.GONE);
                password.setVisibility(View.VISIBLE);
                mChkShowPassword.setVisibility(View.VISIBLE);
                mChkShowPassword.setChecked(false);
//                }
            }
        });

        mChkShowPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final boolean isChecked = mChkShowPassword.isChecked();

                // Toast.makeText(RegistrationPage.this, String.valueOf(isChecked), Toast.LENGTH_SHORT).show();

                if (!isChecked) {
                    password.setTransformationMethod(new PasswordTransformationMethod());
                } else {
                    password.setTransformationMethod(null);
                }
            }
        });

//        mChkChangeIp.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                final boolean isChecked = mChkChangeIp.isChecked();
//
//                // Toast.makeText(RegistrationPage.this, String.valueOf(isChecked), Toast.LENGTH_SHORT).show();
//
//                if (!isChecked) {
//                    mEdtIpAdds.setVisibility(View.GONE);
//                    mEdtIpAdds.setText("");
//                } else {
//                    mEdtIpAdds.setVisibility(View.VISIBLE);
//                    mEdtIpAdds.setText("");
//                }
//            }
//        });
        forgot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog = new Dialog(RegistrationPage.this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.forgot_dialog);
                forget_user = (EditText) dialog.findViewById(R.id.input_username);
                reset = (Button) dialog.findViewById(R.id.reset_btton);
                close = (ImageView) dialog.findViewById(R.id.close);
                close.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                reset.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String username = forget_user.getText().toString();
                        if (username.length() == 0) {
                            forget_user.setError(getResources().getString(R.string.enter_username));
                        } else {
                            new forgotcredential().execute(username);
                        }

                    }
                });
                dialog.show();
            }
        });
        updatePaymentDue(true);

    }

    public void updatePaymentDue(boolean mIsEnabled) {

        try {
            SharedPreferences.Editor editor = sp.edit();

            editor.putBoolean("payment_due_load", mIsEnabled);
            editor.commit();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {

                Intent cancel_intent = new Intent(getApplicationContext(), VehicleTrackingService.class);
                PendingIntent cancel_pendingIntent = PendingIntent.getService(getApplicationContext(), sp.getInt("notif_id", 0), cancel_intent, 0);
                AlarmManager alarmManager = (AlarmManager) getApplicationContext().getSystemService(Context.ALARM_SERVICE);
                alarmManager.cancel(cancel_pendingIntent);
                stopService(cancel_intent);
                return true;
            }
        }
        return false;
    }

    private void saveUserName(String username) {
        SharedPreferences.Editor editor = sp.edit();
        editor.putString("mPhoneNumber", username);
        editor.commit();
    }

    private void showTermsAndConditionDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("");
        builder.setMessage(getResources().getString(R.string.terms_and_conditions)).setCancelable(false).setPositiveButton(getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                //do things
                dialog.cancel();
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    private void saveAppLogo(String url) {
        try {
            SharedPreferences.Editor editor = sp.edit();
            editor.putString("app_logo", url);
            editor.putString("auto_refresh_interval", "10");
            editor.putBoolean("is_auto_refresh_enabled", false);
            editor.putBoolean("is_logged_in", true);
            editor.commit();

            setDefaultGoogleMap();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setDefaultGoogleMap() {
        SharedPreferences.Editor editor = sp.edit();


        editor.putString("enabled_map", getResources().getString(R.string.google));

        editor.commit();
    }

    /**
     * Methods Definitions
     */
    public boolean checkNetworkSettings() {
        isInternetPresent = cd.isConnectingToInternet();
        if (!isInternetPresent) {
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
            alertDialogBuilder.setTitle(getResources().getString(R.string.connect_internet));
            alertDialogBuilder.setPositiveButton(getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    dialog.cancel();
                    startActivityForResult(new Intent(Settings.ACTION_WIFI_SETTINGS), NETWORK_SETTINGS);
                }
            });
            AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();
        }
        return isInternetPresent;
    }

    public String getCountryCode(String isocodevalue) {
        Cursor getcountrycodecur = null;
        String countrycode = null;
        try {
            getcountrycodecur = dbhelper.get_table_details("country_code", "countrycode", "isocode", isocodevalue);
            getcountrycodecur.moveToFirst();
            countrycode = getcountrycodecur.getString(0);
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), ""+getResources().getString(R.string.please_try_again), Toast.LENGTH_SHORT).show();
        } finally {
            if (getcountrycodecur != null) {
                getcountrycodecur.close();
            }
        }
        return countrycode;
    }

    /***/
    public String getStdTableValue(String tag) {
        Cursor std_table_cur = null;
        String std_table_value = null;
        try {
            std_table_cur = dbhelper.get_std_table_info(tag);
            std_table_cur.moveToFirst();
            std_table_value = std_table_cur.getString(0);
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.please_try_again), Toast.LENGTH_SHORT).show();
        } finally {
            if (std_table_cur != null) {
                std_table_cur.close();
            }
        }
        return std_table_value;
    }

    public void getMobileDetails() {
        TelephonyManager
                mngr = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        imeinumber = mngr.getDeviceId();
        mPhoneNumber = mngr.getLine1Number();
        isocode = mngr.getSimCountryIso();
        if (mPhoneNumber.length() == 0) {
            Toast empty_number = Toast.makeText(context, getResources().getString(R.string.mobileno_without_countryCode), Toast.LENGTH_LONG);
            empty_number.show();
        }
        if (imeinumber == null) {
            Toast empty_imei = Toast.makeText(context, getResources().getString(R.string.get_imeino), Toast.LENGTH_LONG);
            empty_imei.show();
        }
        if (isocode == null) {
            Toast empty_iso = Toast.makeText(context, getResources().getString(R.string.get_isoCode), Toast.LENGTH_LONG);
            empty_iso.show();
        } else {
            countrycodetext.setText("+" + getCountryCode(isocode) + " - ");
            Locale.getDefault().getCountry();
        }
        countrycodetext.setText("+" + getCountryCode(isocode) + " - ");
    }

    /**
     * Database Initialisation
     */
    void dbSetup() {
        dbhelper = new DBHelper(context);
        try {
            dbhelper.createDataBase();
            dbhelper.openDataBase();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Network Accessing using Background AsyncTask
     */

    private class forgotcredential extends AsyncTask<String, String, String> {
        String username = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = MyCustomProgressDialog.ctor(context);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            String response_from_server = null;
            username = params[0];

            String urlstr = "http://gpsvts.net/mobile/PasswdReset?userId=" + params[0];
            Log.d(TAG, "doInBackground() called with: " + "params = [" + urlstr + "]");
            Const constaccess = new Const();
            try {
                response_from_server = constaccess.sendGet(urlstr, registration_timeout);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return response_from_server;
        }

        @Override
        protected void onPostExecute(String result) {

            try {

                if (result != null && !result.isEmpty()) {
                    Log.d("forgotresult", "" + result);
                    Toast.makeText(context, "" + result, Toast.LENGTH_SHORT).show();

//                JSONObject mJsonObject = new JSONObject(result);
//                if (mJsonObject.has("website")){
//                    Log.d("forgetcredential","success");
//
//                    String weburl = mJsonObject.getString("website").toString();
//
//                   String url = "http://"+ weburl +"/gps/public/password/reset";
//                    Log.d("website",""+weburl);
//
//                    Intent i = new Intent(Intent.ACTION_VIEW);
//                    i.setData(Uri.parse(url));
//                    startActivity(i);
//                } else {
//                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.oops_error), Toast.LENGTH_LONG).show();
//                }
//
//                if (mJsonObject.has("error")){
//
//                    String error =  mJsonObject.getString("error").toString();
//                    if (!error.equalsIgnoreCase("null")){
//                        Toast.makeText(context, ""+error, Toast.LENGTH_SHORT).show();
//                    }
//
//
//                }
                }

            } catch (Exception e) {
                e.printStackTrace();
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.oops_error), Toast.LENGTH_LONG).show();
            }
            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
        }
    }

    private class SendMobileNumber extends AsyncTask<String, String, String> {
        @Override
        protected String doInBackground(String... f_url) {
            String response_from_server = null;
            String urlstr = Const.API_URL + "sendOTP?mobileNumber=" + f_url[0] + "&imei=" + imeinumber + "&macid=" + getStdTableValue("macid") + "&appid=" + getStdTableValue("appid") + "&gcmId=" + gcmToken;
            Const constaccess = new Const();
            try {
                response_from_server = constaccess.sendGet(urlstr, registration_timeout);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return response_from_server;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = MyCustomProgressDialog.ctor(context);
            progressDialog.show();
            mobile_number.setText("");
        }

        @Override
        protected void onPostExecute(String result) {
            try {
                if (result != null && !result.isEmpty()) {
                    if (result.equals("success")) {
                        mobile_number.setInputType(InputType.TYPE_CLASS_PHONE);
                        mobile_number.setFilters(new InputFilter[]{new InputFilter.LengthFilter(4)});
                        mobile_number.setText("");
                        mobile_number.setHint(getResources().getString(R.string.enter_otp));
                        login.setText(getResources().getString(R.string.signin));
                        label1.setText(getResources().getString(R.string.waiting_verification_code));
                        label2.setText(getResources().getString(R.string.auto_verification_code));
                        countrycodetext.setText("");
                        login.setEnabled(true);
                        password.setVisibility(View.GONE);
                        startTime = SystemClock.uptimeMillis();
                        IntentFilter filter = new IntentFilter(SMS_RECEIVED);
                        registerReceiver(receiver_SMS, filter);
                    }
                } else {
                    Toast empty_fav = Toast.makeText(context, getResources().getString(R.string.please_try_again), Toast.LENGTH_LONG);
                    empty_fav.show();
                }
            } catch (Exception e) {
                e.printStackTrace();
                Toast.makeText(context, getResources().getString(R.string.oops_error), Toast.LENGTH_LONG).show();
            }
            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
        }
    }

    private class SendOTP extends AsyncTask<String, String, String> {
        String username = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = MyCustomProgressDialog.ctor(context);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            String response_from_server = null;
            username = params[0];
//            String urlstr = Const.API_URL + "verifyOTP?mobileNumber=" + params[0] + "&otp=" + params[1] + "&imei=" + imeinumber + "&macid=" + getStdTableValue("macid") + "&appid=" + getStdTableValue("appid") + "&gcmId=" + gcmToken;

            String urlstr = Const.API_URL + "verifyOTPV2?mobileNumber=" + params[0] + "&otp=" + params[1] + "&imei=" + imeinumber + "&macid=" + getStdTableValue("macid") + "&appid=" + getStdTableValue("appid") + "&gcmId=" + gcmToken + "&notifyEnable=" + mNotificationEnableStatus;
            Const constaccess = new Const();
            try {
                response_from_server = constaccess.sendGet(urlstr, registration_timeout);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return response_from_server;
        }

        protected void onProgressUpdate(String... progress) {
            showDialog(DIALOG1_KEY);
        }

        @Override
        protected void onPostExecute(String result) {
            Log.d(TAG, "onPostExecute() called with: " + "result = [" + result + "]");
            try {
                if (result != null && !result.isEmpty()) {
                    JSONObject mJsonObject = new JSONObject(result);
                    if (mJsonObject.has("authUser")) {
                        if (mJsonObject.getString("authUser").toString().equalsIgnoreCase("success")) {
                            if (mJsonObject.has("logo")) {
                                mAppLogo = mJsonObject.getString("logo").toString();
                                saveAppLogo(mJsonObject.getString("logo").toString());
                                new DownloadImageByte().execute(mJsonObject.getString("logo").toString());
                            }
//                            ContentValues cv=new ContentValues();
//                            if (mJsonObject.has("userId")) {

//                                cv.put("","");
//                            }
//                            Toast.makeText(getApplicationContext(),"The group array is ::::::"+mJsonObject.getJSONArray("grps"),Toast.LENGTH_LONG).show();
//                            if (mJsonObject.has("grps")) {
//
//                                System.out.println("The group array is ::::::"+mJsonObject.getJSONArray("grps"));
//
//
//                            }

                            ContentValues cv = new ContentValues();
                            if (mJsonObject.has("userId")) {

                                cv.put("user_id", mJsonObject.getString("userId").toString());
                            }
                            //  Toast.makeText(getApplicationContext(),"The group array is ::::::"+mJsonObject.getJSONArray("grps"),Toast.LENGTH_LONG).show();
                            if (mJsonObject.has("grps")) {
                                List<String> mGroupList = new ArrayList<String>();
                                JSONArray Jarray = null;
                                // if(object.getString("books"))


                                try {
                                    Jarray = mJsonObject.getJSONArray("grps");
                                    // Constant.prtMsg("The menu array is :::" +
                                    // Jarray);
                                } catch (JSONException e) {
                                    // TODO Auto-generated catch block
                                    e.printStackTrace();
                                }
                                // if (Jarray.length() > 0) {
                                for (int i = 0; i < Jarray.length(); i++) {
                                    try {
                                        mGroupList.add(Jarray
                                                .getString(i));
                                    } catch (JSONException j) {

                                    }
                                }


                                cv.put("group_list", new Gson().toJson(mGroupList));


                            }
                            cv.put("refresh_rate", "10");
                            cv.put("history_interval", "1");
                            CommonManager co = new CommonManager(getApplicationContext());
//                            co.insertUserData(cv);

                            DaoHandler da = new DaoHandler(getApplicationContext(), true);
                            da.insertUserData(cv);
                                setLoginStatus();
//                            dbhelper.update_StdTableValues("login_status", true);
//                            Intent mIntent = new Intent(RegistrationPage.this, NavigationActivity.class);
//                            Intent mIntent = new Intent(RegistrationPage.this, VehicleListActivity.class);
//                            startActivity(mIntent);
//                            finish();
                            new DownloadImageUrl().execute();
                        } else {
                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.invalid_credentials), Toast.LENGTH_LONG).show();
                        }
                    } else {
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.oops_error), Toast.LENGTH_LONG).show();
                    }
                } else {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.oops_error), Toast.LENGTH_LONG).show();
                }
            } catch (Exception e) {
                e.printStackTrace();
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.oops_error), Toast.LENGTH_LONG).show();
            }
            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
        }
    }

    public void updateSharedPref(String mUserName, String mPassword, String mIpAdds) {
        try {
            SharedPreferences.Editor editor = sp.edit();
            editor.putString("user_name", mUserName);
            editor.putString("password", mPassword);
            editor.putString("ip_adds", mIpAdds);
            editor.commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void updateIPSharedPref(String mUserName, String mPassword) {
        try {
            SharedPreferences.Editor editor = sp.edit();
            editor.putString("user_name", mUserName);
            editor.putString("password", mPassword);
            editor.commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


//    public String getPhoneImeiNo() {
//        String mMobileNo = null;
//
//        TelephonyManager tMgr = (TelephonyManager) getApplicationContext().getSystemService(Context.TELEPHONY_SERVICE);
//
//        mMobileNo = tMgr.getLine1Number();
//
//        if (mMobileNo == null) {
//            try {
//                TelephonyManager telemgr = (TelephonyManager) this
//                        .getSystemService(Context.TELEPHONY_SERVICE);
//                mMobileNo = telemgr.getDeviceId();
//                // .trim() == null ? "" : telemgr.getDeviceId().trim();
//            } catch (Exception e) {
//                mMobileNo = "EX2";
//            }
//        }
//
//        return mMobileNo;
//    }

    private class SendUserCredentials extends AsyncTask<String, String, String> {
        String username = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = MyCustomProgressDialog.ctor(context);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            String response_from_server = null;
            username = params[0];

            updateSharedPref(params[0], params[1], mIpAddsValue);
            Constant.SELECTED_EMP_NOTI_ENABLE_STATUS = mNotificationEnableStatus;
            Constant.SELECTED_EMP_ID = params[0];
            Constant.SELECTED_EMP_PWD = params[1];


//            if (query1 == null) {
//                query1 = "";
//            }
//            if (query2 == null) {
//                query2 = "";
//            }

            String urlstr = Const.API_URL + "mobile/verifyUser?userId=" + params[0] + "&password=" + params[1] + "&imei=" + imeinumber + "&macid=" + getStdTableValue("macid") + "&appid=" + getStdTableValue("appid") + "&gcmId=" + gcmToken + "&notifyEnable=" + mNotificationEnableStatus;
//                    "&mobileNumber=" + getPhoneImeiNo() + "&model=" + query1 + "&osVersion=" + query2;

            Log.d(TAG, "doInBackground() called with: " + "params = [" + urlstr + "]");
            Const constaccess = new Const();
            try {
                response_from_server = constaccess.sendGet(urlstr, registration_timeout);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return response_from_server;
        }

        protected void onProgressUpdate(String... progress) {
            showDialog(DIALOG1_KEY);
        }

        @Override
        protected void onPostExecute(String result) {
            Log.d(TAG, "onPostExecute() called with: " + "result = [" + result + "]");

            Constant.is_logged_in = true;
            Constant.gcm_count = 1;
            SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences("CamerPreference",0);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putBoolean("Clickin",false);
            editor.commit();
            if (gcmToken != null) {
                Log.d("gcmlength",""+gcmToken.length());
                //  if (gcmToken.length() > 0) {
                //  Log.d("registerservice","failure");

                //  }
            //    if (gcmToken.length() > 0 && !gcmToken.equalsIgnoreCase("SERVICE_NOT_AVAILABLE")) {
                  //  Log.d("registerservice","failure");

             //   }
             //   else {
                    Log.d("registerservice","success");

//                    Intent intent = new Intent(RegistrationPage.this, MyFirebaseInstanceService.class);
//                    startService(intent);
                  //  startService(new Intent(RegistrationPage.this, MyFirebaseInstanceService.class));
               // }
               // if (Constant.is_logged_in) {
                    saveGcmToken(gcmToken);
              // }
            } else {
//                Log.d("registerservice","success1");
//                Intent intent = new Intent(RegistrationPage.this, MyFirebaseInstanceService.class);
//                startService(intent);
               // startService(new Intent(RegistrationPage.this, MyFirebaseInstanceService.class));
            }

            try {
                if (result != null && !result.isEmpty()) {
                    JSONObject mJsonObject = new JSONObject(result);
                    if (mJsonObject.has("authUser")) {
                        if (mJsonObject.getString("authUser").toString().equalsIgnoreCase("success")) {
                            if (mJsonObject.has("logo")) {
                                mAppLogo = mJsonObject.getString("logo").toString();
                                saveAppLogo(mJsonObject.getString("logo").toString());
                                new DownloadImageByte().execute(mJsonObject.getString("logo").toString());
                            }

                            ContentValues cv = new ContentValues();
                            if (mJsonObject.has("userId")) {

                                cv.put("user_id", mJsonObject.getString("userId").toString());
                            }
                            //  Toast.makeText(getApplicationContext(),"The group array is ::::::"+mJsonObject.getJSONArray("grps"),Toast.LENGTH_LONG).show();
                            if (mJsonObject.has("grps")) {
                                List<String> mGroupList = new ArrayList<String>();
                                JSONArray Jarray = null;
                                // if(object.getString("books"))


                                try {
                                    Jarray = mJsonObject.getJSONArray("grps");
                                    // Constant.prtMsg("The menu array is :::" +
                                    // Jarray);
                                } catch (JSONException e) {
                                    // TODO Auto-generated catch block
                                    e.printStackTrace();
                                }
                                // if (Jarray.length() > 0) {
                                for (int i = 0; i < Jarray.length(); i++) {
                                    try {
                                        mGroupList.add(Jarray
                                                .getString(i));
                                    } catch (JSONException j) {

                                    }
                                }

                                // String[] mGroupList=mJsonObject.getJSONArray("grps");

                                cv.put("group_list", new Gson().toJson(mGroupList));
                                //  System.out.println("The group array is ::::::" + mJsonObject.getJSONArray("grps"));


                            }

                            cv.put("refresh_rate", "10");
                            cv.put("history_interval", "1");

//                            CommonManager co = new CommonManager(getApplicationContext());
//                            co.insertUserData(cv);

                            DaoHandler da = new DaoHandler(getApplicationContext(), true);
                            da.insertUserData(cv);
                            setLoginStatus();
//                            dbhelper.update_StdTableValues("login_status", true);

//                            Intent mIntent = new Intent(RegistrationPage.this, NavigationActivity.class);
//                            Intent mIntent = new Intent(RegistrationPage.this, VehicleListActivity.class);
//                            startActivity(mIntent);
//                            finish();
                            new DownloadImageUrl().execute();
                        } else {
                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.invalid_credentials), Toast.LENGTH_LONG).show();
                        }
                    } else {
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.oops_error), Toast.LENGTH_LONG).show();
                    }
                } else {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.oops_error), Toast.LENGTH_LONG).show();
                }
            } catch (Exception e) {
                e.printStackTrace();
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.oops_error), Toast.LENGTH_LONG).show();
            }
            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
        }
    }

    private void saveGcmToken( String gcmToken) {
        mGcmToken = gcmToken;
        String res = "";
        try {

            if (cd.isConnectingToInternet()) {
                Log.d("gsmfirebase","success1");
                HttpConfig config = new HttpConfig();
                res = config.httpGet(Const.API_URL + "mobile/pushGcm?userId=" + Constant.SELECTED_EMP_ID + "&password=" + Constant.SELECTED_EMP_PWD + "&gcmId="
                        + mGcmToken + "&notifyEnable=" + Constant.SELECTED_EMP_NOTI_ENABLE_STATUS + "&type=android");
                // res = config.doPostEcpl(data, WebConfig.GET_TM_PATCH_LIST);

            }
        } catch (Exception e) {
            e.printStackTrace();

        }
//       if (res != null) {
//
//            if (res.equalsIgnoreCase("Success")) {
//
//            } else {
//                saveGcmToken(mGcmToken);
//            }
//
//        }else {
//            saveGcmToken(mGcmToken);
//        }

    }


    private class DownloadImageByte extends AsyncTask<String, String, Bitmap> {
        String username = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // progressDialog = MyCustomProgressDialog.ctor(context);
            //  progressDialog.show();
        }

        @Override
        protected Bitmap doInBackground(String... params) {
            Bitmap bitmap = null;
            try {
                // Download the image
                URL url = new URL(params[0]);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setDoInput(true);
                connection.connect();
                InputStream is = connection.getInputStream();
                // Decode image to get smaller image to save memory
                final BitmapFactory.Options options = new BitmapFactory.Options();
                options.inJustDecodeBounds = false;
                options.inSampleSize = 4;
                bitmap = BitmapFactory.decodeStream(is, null, options);
                is.close();
            } catch (IOException e) {
                return null;
            }
            return bitmap;

        }

        @Override
        protected void onPostExecute(Bitmap result) {
            Log.d(TAG, "onPostExecute() called with: " + "result = [" + result + "]");
            try {
                if (result != null) {
//insert byte array to shared pref

//                    insertBitmap(result);
                    DaoHandler da = new DaoHandler(getApplicationContext(), true);
                    da.insertBitmap(result);


                    //   System.out.println("The inserting byte array is :::::"+result);

                    //saveAppLogo(result);
                }
            } catch (Exception e) {
                e.printStackTrace();
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.oops_error), Toast.LENGTH_LONG).show();
            }
//            if (progressDialog.isShowing()) {
//                progressDialog.dismiss();
//            }
        }
    }

    private class DownloadImageUrl extends AsyncTask<String, String, Bitmap> {
        String username = null;
        // ProgressDialog progressDialog1;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
//            progressDialog1 = new ProgressDialog(RegistrationPage.this,
//                    AlertDialog.THEME_HOLO_LIGHT);
//            progressDialog1.setMessage("Logo downloading...");
//            progressDialog1.setProgressDrawable(new ColorDrawable(
//                    android.graphics.Color.BLUE));
//            progressDialog1.setCancelable(true);
//            progressDialog1.setCanceledOnTouchOutside(false);
//            progressDialog1.show();

            progressDialog = MyCustomProgressDialog.ctor(context);
            progressDialog.show();
        }

        @Override
        protected Bitmap doInBackground(String... params) {
            Bitmap bitmap = null;
//            try {
            // Download the image
//            MemoryCache memoryCache = new MemoryCache();
//            FileCache fileCache = new FileCache(getApplicationContext());
//            File f = fileCache.getFile(mAppLogo);
//
            try {
////                Bitmap bitmap = null;
////                URL imageUrl = new URL(mAppLogo);
////                HttpURLConnection conn = (HttpURLConnection) imageUrl
////                        .openConnection();
////                conn.setConnectTimeout(30000);
////                conn.setReadTimeout(30000);
////                conn.setInstanceFollowRedirects(true);
////                InputStream is = conn.getInputStream();
////                OutputStream os = new FileOutputStream(f);
////                Utils.CopyStream(is, os);
//              //  os.close();
//               // bitmap = decodeFile(f);

//                if (!mAppLogo.contains("https")) {
//                    mAppLogo = mAppLogo.replace("http", "https");
//                }

                imgLoader.DisplayImage(mAppLogo, mAppLogoTest);
//               // System.out.println("The bit map is :::::"+bitmap);
//
////                return bitmap;
            } catch (Throwable ex) {
//                ex.printStackTrace();
//                if (ex instanceof OutOfMemoryError)
//                    memoryCache.clear();
//                return null;
            }

            Bitmap map = null;
            //  for (String url : urls) {
//            map = downloadImage("http://theopentutorials.com/totwp331/wp-content/uploads/totlogo.png");
            System.out.println("The url is :::::" + mAppLogo);
            // mAppLogo="https://vamosys.com/vamo/public/assets/imgs/MACK.png";
            map = downloadImage(mAppLogo.trim());
            // }
            return map;


//            try {
//
//                //  String encodedString = URLEncoder.encode(mAppLogo, "UTF-8");
//
//                bitmap = BitmapFactory.decodeStream((InputStream) new URL(mAppLogo.trim().replace(" ", "").toLowerCase()).getContent());
//                System.out.println("The bitmap is :::::" + bitmap);
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//            return bitmap;

//            Bitmap bitmap = null;
//            try {
//                URL url = new URL("http://vamosys.com/vamo/public/assets/imgs/MACK.png");
//                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
//                connection.setDoInput(true);
//                connection.connect();
//                InputStream input = connection.getInputStream();
//                Bitmap myBitmap = BitmapFactory.decodeStream(input);
//                return myBitmap;
//            } catch (IOException e) {
//
//                System.out.println("The exception is :::" + e.toString());
//
//                e.printStackTrace();
//                return null;
//            }

//            try {
//                java.net.URL url = new java.net.URL(mAppLogo);
//                HttpURLConnection connection = (HttpURLConnection) url
//                        .openConnection();
//                connection.setDoInput(true);
//                connection.connect();
//                InputStream input = connection.getInputStream();
//                Bitmap myBitmap = BitmapFactory.decodeStream(input);
//                return myBitmap;
//            } catch (IOException e) {
//                e.printStackTrace();
//                return null;
//            }

//        }
//            catch(IOException e){
//                return null;
//            }
            //  return bitmap;
//            return map;

        }

        @Override
        protected void onPostExecute(Bitmap result) {
            Log.d(TAG, "onPostExecute() called with: " + "bitmap result = [" + result + "]");
//        try {
            if (result != null) {
////insert byte array to shared pref
                //  mAppLogoTest.setImageBitmap(result);
//                insertBitmap(result);

                DaoHandler da = new DaoHandler(getApplicationContext(), true);
                da.insertBitmap(result);

//                //   System.out.println("The inserting byte array is :::::"+result);
//
//                //saveAppLogo(result);
            }
//        } catch (Exception e) {
//            e.printStackTrace();
//            Toast.makeText(getApplicationContext(), getResources().getString(R.string.oops_error), Toast.LENGTH_LONG).show();
//        }
            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
            }

            Intent mIntent = new Intent(RegistrationPage.this, VehicleListActivity.class);
            startActivity(mIntent);
            finish();

        }

    }


//    private Bitmap downloadBitmap(String url) {
//        // initilize the default HTTP client object
//        final DefaultHttpClient client = new DefaultHttpClient();
//
//        //forming a HttoGet request
//        final HttpGet getRequest = new HttpGet(url);
//        try {
//
//            HttpResponse response = client.execute(getRequest);
//
//            //check 200 OK for success
//            final int statusCode = response.getStatusLine().getStatusCode();
//
//            if (statusCode != HttpStatus.SC_OK) {
//                Log.w("ImageDownloader", "Error " + statusCode +
//                        " while retrieving bitmap from " + url);
//                return null;
//
//            }
//
//            final HttpEntity entity = response.getEntity();
//            if (entity != null) {
//                InputStream inputStream = null;
//                try {
//                    // getting contents from the stream
//                    inputStream = entity.getContent();
//
//                    // decoding stream data back into image Bitmap that android understands
//                    final Bitmap bitmap = BitmapFactory.decodeStream(inputStream);
//
//                    return bitmap;
//                } finally {
//                    if (inputStream != null) {
//                        inputStream.close();
//                    }
//                    entity.consumeContent();
//                }
//            }
//        } catch (Exception e) {
//            // You Could provide a more explicit error message for IOException
//            getRequest.abort();
//            Log.e("ImageDownloader", "Something went wrong while" +
//                    " retrieving bitmap from " + url + e.toString());
//        }
//
//        return null;
//
//    }

    // Creates Bitmap from InputStream and returns it
    private Bitmap downloadImage(String url) {
        Bitmap bitmap = null;
        InputStream stream = null;
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inSampleSize = 1;

        try {
            stream = getHttpConnection(url);
            bitmap = BitmapFactory.
                    decodeStream(stream, null, bmOptions);
            if (stream != null) {
                stream.close();
            }
        } catch (IOException e1) {
            e1.printStackTrace();
        }
        return bitmap;
    }

    // Makes HttpURLConnection and returns InputStream
    private InputStream getHttpConnection(String urlString)
            throws IOException {
        InputStream stream = null;
        URL url = new URL(urlString);
        URLConnection connection = url.openConnection();

        try {
            HttpURLConnection httpConnection = (HttpURLConnection) connection;
            httpConnection.setRequestMethod("GET");
            httpConnection.connect();

            if (httpConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                stream = httpConnection.getInputStream();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        System.out.println("The return stream is ::::" + stream);

        return stream;
    }


//    // decodes image and scales it to reduce memory consumption
//    private Bitmap decodeFile(File f) {
//        try {
//            // decode image size
//
//            //	CommonManager co=new CommonManager(con);
//
//            //BitmapFactory.Options o = co.getBitmap();
//
//            BitmapFactory.Options o = new BitmapFactory.Options();
//            o.inJustDecodeBounds = true;
//            BitmapFactory.decodeStream(new FileInputStream(f), null, o);
//
//            // Find the correct scale value. It should be the power of 2.
//            final int REQUIRED_SIZE = 200;
//            int width_tmp = o.outWidth, height_tmp = o.outHeight;
//            int scale = 1;
//            while (true) {
////				if (width_tmp / 2 < REQUIRED_SIZE
////						|| height_tmp / 2 < REQUIRED_SIZE)
////					break;
////				width_tmp /= 2;
////				height_tmp /= 2;
////				scale *= 2;
//
//                if (width_tmp < REQUIRED_SIZE
//                        || height_tmp < REQUIRED_SIZE)
//                    break;
//                width_tmp = width_tmp;
//                height_tmp = height_tmp;
//                scale *= 1;
//
//            }
//
//            // decode with inSampleSize
//            BitmapFactory.Options o2 = new BitmapFactory.Options();
//
////			if (scale > 3) {
////				scale = 3;
////			}
//
//            o2.inSampleSize = scale;
//            return BitmapFactory.decodeStream(new FileInputStream(f), null, o2);
//        } catch (FileNotFoundException e) {
//        }
//        return null;
//    }
//

//    public void insertBitmap(Bitmap bm) {
//
//        // Convert the image into byte array
//        ByteArrayOutputStream out = new ByteArrayOutputStream();
//        bm.compress(Bitmap.CompressFormat.PNG, 100, out);
//        byte[] buffer = out.toByteArray();
//        // Open the database for writing
//        //SQLiteDatabase db = this.getWritableDatabase();
//        // Start the transaction.
//        //  db.beginTransaction();
//        DataBaseHandler db = new DataBaseHandler(getApplicationContext());
//        ContentValues values;
//
//        try {
//            values = new ContentValues();
//            values.put("app_logo_image", buffer);
//            //values.put("description", "Image description");
//            // Insert Row
//            //  long i = db.insert(TABLE_NAME, null, values);
//            // Log.i("Insert", i + "");
//            // Insert into database successfully.
//            // db.setTransactionSuccessful();
//            System.out.println("The inserting values for logo table is :::::" + values);
//            db.open().getDatabaseObj().insert(DataBaseHandler.TABLE_LOGO, null, values);
//
//        } catch (SQLiteException e) {
//            e.printStackTrace();
//
//        } finally {
//
//            db.close();
//            // Close database
//        }
//    }

    public  void setLoginStatus(){
        SharedPreferences.Editor editor= sp.edit();
        editor.putBoolean("login_status",true);
        editor.commit();
    }
}
