package com.vamosys.vamos;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Build;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class DBHelper extends SQLiteOpenHelper {
    //template data structure
    public static final String KEY_GROUP_ID = "group_id";
    public static final String KEY_GROUP_DETAILS = "group_details";
    public static final String KEY_VEHICLE_LOCATIONS_DETAILS = "vehicle_locations_details";
    //stdtable structure
    public static final String KEY_STD_TAG = "std_tag";
    public static final String KEY_STD_VALUE = "std_val";
    //current_running_vehicle info structure
    public static final String KEY_CURRENT_VEHICLE_INFO = "current_vehicle_info";
    //vehicle_histtory  info structure
    public static final String KEY_VEHICLE_HISTORY_INFO = "vehicle_history_info";
    private static final String DATABASE_NAME = "db.sqlite";
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_VEHICLE_LOCATIONS_TABLE = "vehicle_locations";
    private static final String DATABASE_STD_TABLE = "std_table";
    private static final String DATABASE_CURRENT_VEHICLE_INFO = "current_running_vehicle";
    private static final String DATABASE_VEHICLE_HISTORY = "vehicle_history";
    SQLiteDatabase db;
    Context helperContext;
    private DBHelper dbHelper;

    public DBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        helperContext = context;
    }

    public void createDataBase() {
        try {
            if (!checkDataBase()) {

                System.out.println("Hi after database check condition");

                this.getReadableDatabase();
                copyDataBase();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String getDBPath() {
        String path = null;
        try {

            path = "/data/data/" + helperContext.getPackageName() + "/databases/";
            path=helperContext.getDatabasePath(DATABASE_NAME).getAbsolutePath();
//            String filePath = mContext.getDatabasePath(Utils.getDatabaseName()).getAbsolutePath();
//            path = helperContext.getFilesDir().getPath() + helperContext.getPackageName() + "/databases/";

//            System.out.println("Hi returning sqlite path is " + path);

//        if(android.os.Build.VERSION.SDK_INT >= 4.2){
//            path = helperContext.getApplicationInfo().dataDir + "/databases/";
//        } else {
//            path = "/data/data/" + helperContext.getPackageName() + "/databases/";
//        }
//        this.mContext = context;
        } catch (Exception e) {

        }

        return path;
    }

    private boolean checkDataBase() {
        SQLiteDatabase checkDB = null;
        try {
            try {
                String myPath = getDBPath() + DATABASE_NAME;
                checkDB = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READONLY);
            } catch (Exception e) {
                e.printStackTrace();
                return checkDB != null ? true : false;
            }
            if (checkDB != null) {
                checkDB.close();
            }
            return checkDB != null ? true : false;
        } catch (Exception e) {
            e.printStackTrace();
            return checkDB != null ? true : false;
        }
    }

    private void copyDataBase() throws IOException {
        try {
            InputStream myInput = helperContext.getAssets().open(DATABASE_NAME);
            String outFileName = getDBPath() + DATABASE_NAME;
            OutputStream myOutput = new FileOutputStream(outFileName);
            byte[] buffer = new byte[1024];
            int length;
            while ((length = myInput.read(buffer)) > 0) {
                myOutput.write(buffer, 0, length);
            }
            myOutput.flush();
            myOutput.close();
            myInput.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public SQLiteDatabase openDataBase() throws SQLException {
        String myPath = getDBPath() + DATABASE_NAME;
        db = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READWRITE);
        return db;
    }

    @Override
    public synchronized void close() {
        if (db != null)
            db.close();
        super.close();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onCreate(db);
    }
    //////////////////////////////////////////////////////
    /** Close the database */
    /**
     * Open the database
     */
    public void open() throws SQLiteException {
        try {
//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
//                db.disableWriteAheadLogging();
//            }
            db = dbHelper.getWritableDatabase();
        } catch (SQLiteException ex) {
            db = dbHelper.getReadableDatabase();
        }
    }

    /**
     * Insert a new task
     */
    public Cursor get_vehicle_details() {
        return db.query(DATABASE_VEHICLE_LOCATIONS_TABLE, new String[]{KEY_GROUP_DETAILS, KEY_VEHICLE_LOCATIONS_DETAILS, KEY_GROUP_ID}, null, null, null, null, null);
    }

    public Cursor get_current_vehicle_details() {
        return db.query(DATABASE_CURRENT_VEHICLE_INFO, new String[]{KEY_CURRENT_VEHICLE_INFO}, null, null, null, null, null);
    }

    public Cursor get_std_table_info(String tag) {
        String val[] = {tag};
        return db.query(DATABASE_STD_TABLE, new String[]{KEY_STD_VALUE}, KEY_STD_TAG + "=?", val, null, null, null);
    }

    public Cursor get_table_details(String tablename, String select_value, String where_val, String value) {
        String val[] = {value};
        return db.query(tablename, new String[]{select_value}, where_val + "=?", val, null, null, null);
    }

    public void insert_vehicle_details(String group_details, String vehicle_location_details, String groupid) {
        ContentValues newcurrentaffairValues = new ContentValues();
        newcurrentaffairValues.put(KEY_GROUP_DETAILS, group_details);
        newcurrentaffairValues.put(KEY_VEHICLE_LOCATIONS_DETAILS, vehicle_location_details);
        newcurrentaffairValues.put(KEY_GROUP_ID, groupid);
        db.insert(DATABASE_VEHICLE_LOCATIONS_TABLE, null, newcurrentaffairValues);
    }

    public void insert_current_vehicle_details(String current_vehicle_info) {
        ContentValues newcurrentaffairValues = new ContentValues();
        newcurrentaffairValues.put(KEY_CURRENT_VEHICLE_INFO, current_vehicle_info);
        db.insert(DATABASE_CURRENT_VEHICLE_INFO, null, newcurrentaffairValues);
    }

    public void insert_vehicle_history_details(String vehicle_history_info) {
        ContentValues newcurrentaffairValues = new ContentValues();
        newcurrentaffairValues.put(KEY_VEHICLE_HISTORY_INFO, vehicle_history_info);
        db.insert(DATABASE_VEHICLE_HISTORY, null, newcurrentaffairValues);
    }

    /**
     * update group and vehicle informations
     */
    public void update_all_vehicle_details(String vehicle_detail, String groupid) {
        ContentValues newcurrentaffairValues = new ContentValues();
        newcurrentaffairValues.put(KEY_VEHICLE_LOCATIONS_DETAILS, vehicle_detail);
        newcurrentaffairValues.put(KEY_GROUP_ID, groupid);
        db.update(DATABASE_VEHICLE_LOCATIONS_TABLE, newcurrentaffairValues, null, null);
    }

    public void update_vehicle_details(String groupdetail, String vehicle_detail, String groupid) {
        String val[] = {groupid};
        ContentValues newcurrentaffairValues = new ContentValues();
        newcurrentaffairValues.put(KEY_GROUP_DETAILS, groupdetail);
        newcurrentaffairValues.put(KEY_VEHICLE_LOCATIONS_DETAILS, vehicle_detail);
        newcurrentaffairValues.put(KEY_GROUP_ID, groupid);
        db.update(DATABASE_VEHICLE_LOCATIONS_TABLE, newcurrentaffairValues, KEY_GROUP_ID + "=?", val);
    }

    public void update_StdTableValues(String tag, boolean value) {
        String val[] = {tag};
        ContentValues newcurrentaffairValues = new ContentValues();
        newcurrentaffairValues.put(KEY_STD_VALUE, value);
        db.update(DATABASE_STD_TABLE, newcurrentaffairValues, KEY_STD_TAG + "=?", val);
    }

    public void update_Settings_StdTableValues(String tag, String value) {
        String val[] = {tag};
        ContentValues newcurrentaffairValues = new ContentValues();
        newcurrentaffairValues.put(KEY_STD_VALUE, value);
        db.update(DATABASE_STD_TABLE, newcurrentaffairValues, KEY_STD_TAG + "=?", val);
    }

    /**
     * Delete Operations
     */
    public boolean deletevalues() {
        return db.delete(DATABASE_VEHICLE_LOCATIONS_TABLE, null, null) > 0;
    }

    public boolean deletecurrent_vehicleinfo() {
        return db.delete(DATABASE_CURRENT_VEHICLE_INFO, null, null) > 0;
    }
    /** Static Helper class for creating, upgrading, and opening
     * the database.
     */
}
