package com.vamosys.vamos;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.SQLException;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.vamosys.adapter.GeoFencesAdapter;
import com.vamosys.model.Site;
import com.vamosys.model.SiteParent;
import com.vamosys.model.ViewSite;
import com.vamosys.utils.ConnectionDetector;
import com.vamosys.utils.Constant;
import com.vamosys.utils.DaoHandler;
import com.vamosys.utils.TypefaceUtil;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

public class GeoFencesActivity extends Activity {
    Spinner groupSpinner;
    ListView lv;
    EditText mEdtSearch;
    TextView mTxtNoRecord, mHeadTitle;
    ImageView mBackArrow;
    ConnectionDetector cd;
    List<String> mSpinnerList = new ArrayList<String>();

    List<SiteParent> mSiteParentList = new ArrayList<SiteParent>();
    List<Site> mSiteList = new ArrayList<Site>();
    List<Site> mSiteAdapterList = new ArrayList<Site>();

    String mSelectedGroup, mUserId, mFcode;
    SharedPreferences sp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_geo_fences);
        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        cd = new ConnectionDetector(getApplicationContext());
        sp = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        if (sp.getString("ip_adds", "") != null) {
            if (sp.getString("ip_adds", "").trim().length() > 0) {
                Const.API_URL = sp.getString("ip_adds", "");
            }
        }
        init();
        screenArrange();

        try {
            queryUserDB();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        if (cd.isConnectingToInternet()) {
            new getGeoFencesData().execute();
        } else {
            Toast.makeText(GeoFencesActivity.this, getResources().getString(R.string.network_connection), Toast.LENGTH_SHORT).show();
        }

        mBackArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(GeoFencesActivity.this, VehicleListActivity.class));
                finish();
            }
        });

        groupSpinner
                .setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                    @Override
                    public void onItemSelected(AdapterView<?> parent,
                                               View view, int pos, long id) {
                        // TODO Auto-generated method stub
                        mSelectedGroup = mSpinnerList.get(pos);
                        mEdtSearch.setText("");
                        if (mSelectedGroup.equalsIgnoreCase("Select")) {
                            setAdapterData("No Record");
                        } else {
                            setAdapterData("ALL");
                        }

                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> arg0) {
                        // TODO Auto-generated method stub

                    }

                });


        /**
         * Enabling Search Filter
         * */
        mEdtSearch.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence cs, int arg1, int arg2,
                                      int arg3) {
                // When user changed the Text
                // DailySearch.this.arrayAutoListAdapter.getFilter().filter(cs);
                if (cs.length() > 0) {
                    getSearchText(String.valueOf(cs).trim());
                } else {
                    // setData("ALL");
                    getSearchText(null);
                }
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1,
                                          int arg2, int arg3) {
                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub
            }

        });


    }

    public void init() {
        lv = (ListView) findViewById(R.id.geo_fence_list);
        mTxtNoRecord = (TextView) findViewById(R.id.geo_fence__no_record_txt);
        mBackArrow = (ImageView) findViewById(R.id.geo_fence__view_Back);
        mHeadTitle = (TextView) findViewById(R.id.geo_fence_tvTitle);
        mHeadTitle.setVisibility(View.GONE);
        groupSpinner = (Spinner) findViewById(R.id.geo_fence_groupspinner);
        mTxtNoRecord.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        mHeadTitle.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        mEdtSearch = (EditText) findViewById(R.id.my_sites_search_view);
        mEdtSearch.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
    }


    public void queryUserDB() {
        DaoHandler da = new DaoHandler(getApplicationContext(), false);
        da.queryUserDB();

        mUserId = Constant.mDbUserId;
        Constant.SELECTED_USER_ID = mUserId;
//        mGroupList = Constant.mUserGroupList;

        for (int i = 0; i < Constant.mUserGroupList.size(); i++) {
            String strGrData = Constant.mUserGroupList.get(i).trim();

            String[] str_array = strGrData.split(":");

            // System.out.println("The lat str data is ::::" + str_array[0].trim());
            // System.out.println("The  lng str data is ::::" + str_array[1]);

            mFcode = str_array[1].trim();

//                        mFcode =
        }

        setDropDownData();
    }

//    public void queryUserDB() {
//        DataBaseHandler db = new DataBaseHandler(this);
//        Cursor c = null;
//        // String qry = "SELECT DISTINCT patch_id,patch_name,mtp_id FROM "
//        // + DbHandler.TABLE_TM_DCP;
//        try {
//            // c = db.open().getDatabaseObj().rawQuery(qry, null);
//            c = db.open()
//                    .getDatabaseObj()
//                    .query(DataBaseHandler.TABLE_USER, null, null, null, null, null,
//                            null);
//
//            int user_id_index = c.getColumnIndex("user_id");
//            int group_list_index = c.getColumnIndex("group_list");
//
//            if (c.getCount() > 0) {
//
//                while (c.moveToNext()) {
//                    mUserId = c.getString(user_id_index);
//                    Constant.SELECTED_USER_ID = mUserId;
//                    List<String> grList = null;
//                    if (c.getString(group_list_index) != null) {
//                        Gson g = new Gson();
//                        InputStream is = new ByteArrayInputStream(c.getString(
//                                group_list_index).getBytes());
//                        Reader reader = new InputStreamReader(is);
//                        Type fooType = new TypeToken<List<String>>() {
//                        }.getType();
//
//                        grList = g.fromJson(reader, fooType);
//                    }
//
//                    for (int i = 0; i < grList.size(); i++) {
//                        String strGrData = grList.get(i).trim();
//
//                        String[] str_array = strGrData.split(":");
//
//                        // System.out.println("The lat str data is ::::" + str_array[0].trim());
//                        // System.out.println("The  lng str data is ::::" + str_array[1]);
//
//                        mFcode = str_array[1].trim();
//
////                        mFcode =
//                    }
//
////
//
//                }
//            }
//
//        } finally {
//            c.close();
//            db.close();
//        }
//
//    }

    public void setDropDownData() {
        //mGroupList = new ArrayList<String>();
        List<String> list = new ArrayList<String>();
        // list.add("Select");

        // System.out.println("The site parent size is :::" + mSiteParentList.size());

        if (mSiteParentList != null && mSiteParentList.size() > 0) {
            for (int i = 0; i < mSiteParentList.size(); i++) {
                if (mSiteParentList.get(i) != null) {
                    System.out.println("Hiiiii" + i + " " + mSiteParentList.get(i).getSite() + " " + mSiteParentList.get(i).getOrgId());
                    if (mSiteParentList.get(i).getSite() != null) {
                        System.out.println("The org id is :::::" + mSiteParentList.get(i).getOrgId() + " The site size is :::" + mSiteParentList.get(i).getSite().length);

                        list.add(mSiteParentList.get(i).getOrgId());
                    }
                }
            }
        }

        mSpinnerList = list;

        //   System.out.println("The spinner list is ::::" + mSpinnerList.size());

        if (mSpinnerList.size() > 0) {

        } else {
            mSpinnerList.add("Select");
        }

        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(
                GeoFencesActivity.this, R.layout.spinner_row,
                mSpinnerList) {

            public View getView(int position, View convertView, ViewGroup parent) {
                View v = super.getView(position, convertView, parent);
                // TextView v = (TextView) super.getView(position,
                // convertView,parent);
                // Constant.face(DailyCalls.this, (TextView) v);
                return v;
            }

            public View getDropDownView(int position, View convertView,
                                        ViewGroup parent) {
                View v = super.getDropDownView(position, convertView, parent);
                // ((TextView)
                // v).setBackgroundColor(Color.parseColor("#BBfef3da"));
                // TextView v = (TextView) super.getView(position,
                // convertView,parent);
                // Constant.face(DailyCalls.this, (TextView) v);
                return v;
            }
        };
        spinnerArrayAdapter
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        groupSpinner.setAdapter(spinnerArrayAdapter);
    }


    public void setAdapterData(String type) {
        List<Site> siteList = new ArrayList<Site>();
        if (type.equalsIgnoreCase("ALL")) {
            for (int i = 0; i < mSiteParentList.size(); i++) {
                if (mSiteParentList.get(i) != null && !mSiteParentList.get(i).equals("null")) {
                    if (mSiteParentList.get(i).getOrgId().equalsIgnoreCase(mSelectedGroup)) {
                        if (mSiteParentList.get(i).getSite() != null) {

                            siteList = new ArrayList<Site>(
                                    Arrays.asList(mSiteParentList.get(i).getSite()));

                        }
                    }
                }

            }

            mSiteList = siteList;
            mSiteAdapterList = mSiteList;

            if (mSiteAdapterList != null && mSiteAdapterList.size() > 0) {
                mTxtNoRecord.setVisibility(View.GONE);
                lv.setVisibility(View.VISIBLE);

                GeoFencesAdapter adapter = new GeoFencesAdapter(GeoFencesActivity.this,
                        mSiteAdapterList);
                lv.setAdapter(adapter);
                adapter.notifyDataSetChanged();


            } else {
                mTxtNoRecord.setVisibility(View.VISIBLE);
                lv.setVisibility(View.GONE);
            }

        } else {
            mTxtNoRecord.setVisibility(View.VISIBLE);
            lv.setVisibility(View.GONE);
        }


    }


    public void getSearchText(String ch) {

//        System.out.println("List size is ::::::" + mVehicleLocationListAdapter.size()
//                + " and the character is :::::" + ch);

        List<Site> list = new ArrayList<Site>();
        // list = null;
        if (ch != null && ch.length() > 0) {


            list.clear();

            for (int i = 0; i < mSiteList.size(); i++) {
                Site dc = new Site();


                if (Pattern
                        .compile(Pattern.quote(ch), Pattern.CASE_INSENSITIVE)
                        .matcher(mSiteList.get(i).getSiteName())
                        .find() || Pattern
                        .compile(Pattern.quote(ch), Pattern.CASE_INSENSITIVE)
                        .matcher(mSiteList.get(i).getSiteType())
                        .find()) {
                    //  System.out.println("Hiiiiiii");
                    dc = mSiteList.get(i);
                    list.add(dc);
                }

            }

        } else {
            list = mSiteList;
        }

        // System.out.println("The searched list size is :::::" + list.size());


//        System.out.println("List size after filtered is :::::"
//                + mVehicleLocationListAdapter.size());

        if (list.size() > 0) {
            mSiteAdapterList = list;


            if (mSiteAdapterList.size() > 0) {

                GeoFencesAdapter notiAdapter = new GeoFencesAdapter(GeoFencesActivity.this, mSiteAdapterList);
//                lv.setAdapter(new NotificationListAdapter(
//                        NotificationListActivity.this, Constant.mPushDataList));

                lv.setAdapter(notiAdapter);
                notiAdapter.notifyDataSetChanged();

                //   lv.setVisibility(View.VISIBLE);

                mTxtNoRecord.setVisibility(View.GONE);
                lv.setVisibility(View.VISIBLE);

                //  mTxtView.setVisibility(View.GONE);
                //  mSelectAllLayout.setVisibility(View.VISIBLE);
            } else {
                lv.setVisibility(View.GONE);
                //mTxtView.setVisibility(View.VISIBLE);
                // mSelectAllLayout.setVisibility(View.GONE);
                mTxtNoRecord.setVisibility(View.VISIBLE);
                lv.setVisibility(View.GONE);
            }


        } else {
//    Set no record data
            List<Site> vList = new ArrayList<Site>();
            mSiteAdapterList = vList;
            if (mSiteAdapterList.size() > 0) {

                GeoFencesAdapter notiAdapter = new GeoFencesAdapter(GeoFencesActivity.this, mSiteAdapterList);
//                lv.setAdapter(new NotificationListAdapter(
//                        NotificationListActivity.this, Constant.mPushDataList));

                lv.setAdapter(notiAdapter);
                notiAdapter.notifyDataSetChanged();

//                lv.setVisibility(View.VISIBLE);
                //  mTxtView.setVisibility(View.GONE);
                mTxtNoRecord.setVisibility(View.GONE);
                lv.setVisibility(View.VISIBLE);
                //  mSelectAllLayout.setVisibility(View.VISIBLE);
            } else {
//                lv.setVisibility(View.GONE);

                mTxtNoRecord.setVisibility(View.VISIBLE);
                lv.setVisibility(View.GONE);
                // mTxtView.setVisibility(View.VISIBLE);
                // mSelectAllLayout.setVisibility(View.GONE);
            }
            // setData("No Record");

        }
        // $SearchListView.setAdapter(new DailySearchAdapter(DailySearch.this,
        // mFilteredListDailySearch));


    }


    private class getGeoFencesData extends AsyncTask<String, String, String> {
        ProgressDialog progressDialog;

        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub

            String result = null;
            try {
                Const constaccess = new Const();
                /*
New API
 */
                result = constaccess
                        .sendGet(Const.API_URL + "mobile/viewSite?userId=" + Constant.SELECTED_USER_ID + "&fcode=" + mFcode, 30000);

            } catch (Exception e) {

            }

            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            progressDialog.dismiss();
            if (result != null && result.length() > 0) {
                System.out.println("The kms result is ::::" + result);


//                JSONObject object = null;
//                try {
//                    object = new JSONObject(result);
//                } catch (JSONException e) {
//                    // TODO Auto-generated catch block
//                    e.printStackTrace();
//                }
//
//                JSONArray Jarray = null;
//
//                try {
//                    Jarray = object.getJSONArray("siteParent");
////                    System.out.println("The site parent array is :::" +
////                            Jarray);
//
//                   // System.out.println("The site parent array size is ::::" + Jarray.length());
//                } catch (JSONException e) {
//                    // TODO Auto-generated catch block
//                    e.printStackTrace();
//                }
//
//                List<SiteParent> siteParentList = new ArrayList<SiteParent>();
//                for (int i = 0; i < Jarray.length(); i++) {
//                    SiteParent sp = new SiteParent();
//                    try {
//                        JSONObject Jasonobject = Jarray
//                                .getJSONObject(i);
//                      //  System.out.println("The json obj is ::::" + Jasonobject);
//
//                        String mOrgId;
//
//                        mOrgId = Jasonobject.getString("orgId");
//                        sp.setOrgId(mOrgId);
//                        System.out.println("The org oooo is :::" + mOrgId);
//
//                        JSONArray JSiteArray = null;
//
//                        try {
//                            JSiteArray = Jasonobject.getJSONArray("site");
//
//                        } catch (JSONException e) {
//                            // TODO Auto-generated catch block
//                            e.printStackTrace();
//                        }
//                        List<Site> siteList = new ArrayList<Site>();
//                        for (int j = 0; j < JSiteArray.length(); i++) {
//                            try {
//                                JSONObject jSiteObj = JSiteArray
//                                        .getJSONObject(j);
//
//                                String mSiteName, mSiteType;
//                                mSiteName = jSiteObj.getString("siteName");
//                                mSiteType = jSiteObj.getString("siteType");
//
//                                Site s = new Site();
//                                s.setSiteName(mSiteName);
//                                s.setSiteType(mSiteType);
//                                siteList.add(s);
//
//                            } catch (Exception e) {
//
//                            }
//                        }
////sp.setSite(siteList);
//
//                    } catch (Exception e) {
//
//                    }
//
//                }
                try {

                    ViewSite mViewSite;

                    Gson g = new Gson();

                    mViewSite = g.fromJson(result.trim(),
                            ViewSite.class);

                    // System.out.println("The view site object is :::" + mViewSite);


//                for (
//                        int k = 0;
//                        k < mViewSite.getSiteParent().length; k++)
//
//                {
//                    System.out.println("org id :" + mViewSite.getSiteParent()[k].getOrgId());
//                }

                    if (mViewSite.getSiteParent() != null)

                    {

                        mSiteParentList = new ArrayList<SiteParent>(
                                Arrays.asList(mViewSite.getSiteParent()));

//                    for (int i = 0; i < mSiteParentList.size(); i++) {
//
//                        for (int j = 0; j < mSiteParentList.get(i).getSite().length; j++) {
//                            System.out.println(" HH" + mSiteParentList.get(i).getSite()[j].getSiteName());
//
//
//                        }
//                    }

//                    SiteParent[] mSitePar =
//                            mViewSite.getSiteParent();
//                    for (int i = 0; i < mViewSite.getSiteParent().length; i++) {
//                        System.out.println("The org id :::" + mViewSite.getSiteParent()[i].getOrgId());
//                    }


                    }
                } catch (Exception e) {

                }


                setDropDownData();
            }

        }

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            progressDialog = new ProgressDialog(GeoFencesActivity.this,
                    AlertDialog.THEME_HOLO_LIGHT);
            progressDialog.setMessage(getResources().getString(R.string.progress_dialog));
            progressDialog.setProgressDrawable(new ColorDrawable(
                    android.graphics.Color.BLUE));
            progressDialog.setCancelable(true);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.show();
        }

    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();

        startActivity(new Intent(GeoFencesActivity.this, VehicleListActivity.class));
        finish();

    }

    private void screenArrange() {
        // TODO Auto-generated method stub

		/* screen arrangements */
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        int height = metrics.heightPixels;
        int width = metrics.widthPixels;
        Constant.ScreenWidth = width;
        Constant.ScreenHeight = height;

        LinearLayout.LayoutParams backImageParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        backImageParams.width = width * 15 / 100;
        backImageParams.height = height * 10 / 100;
        backImageParams.gravity = Gravity.CENTER;
        mBackArrow.setLayoutParams(backImageParams);
        mBackArrow.setPadding(width * 1 / 100, height * 1 / 100, width * 1 / 100, height * 1 / 100);

        LinearLayout.LayoutParams headTxtParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        headTxtParams.width = width * 85 / 100;
        headTxtParams.height = height * 10 / 100;
        mHeadTitle.setLayoutParams(headTxtParams);
        mHeadTitle.setPadding(width * 2 / 100, 0, 0, 0);
        mHeadTitle.setGravity(Gravity.CENTER | Gravity.LEFT);

        LinearLayout.LayoutParams headTxtParams1 = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        headTxtParams1.width = width * 76 / 100;
        headTxtParams1.height = height * 8 / 100;
        headTxtParams1.setMargins(width * 4 / 100, width * 2 / 100, 1, width * 1 / 100);
//        headTxtParams1.setMargins();
//        $TxtTitle.setLayoutParams(headTxtParams);
//        $TxtTitle.setPadding(width * 2 / 100, 0, 0, 0);
//        $TxtTitle.setGravity(Gravity.CENTER);
        mEdtSearch.setLayoutParams(headTxtParams1);
//        mEdtSearch.setm
        mEdtSearch.setPadding(width * 2 / 100, 0, width * 4 / 100, 0);
        mEdtSearch.setGravity(Gravity.CENTER | Gravity.CENTER);


        LinearLayout.LayoutParams spinnerParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        spinnerParams.width = width * 95 / 100;
        spinnerParams.height = height * 8 / 100;
        spinnerParams.topMargin = (int) (height * 1.25 / 100);
        spinnerParams.leftMargin = width * 2 / 100;
        spinnerParams.rightMargin = width * 2 / 100;
        spinnerParams.bottomMargin = (int) (height * 0.25 / 100);
        groupSpinner.setLayoutParams(spinnerParams);

        LinearLayout.LayoutParams lineParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        lineParams.width = width * 98 / 100;
        lineParams.height = LinearLayout.LayoutParams.WRAP_CONTENT;
        lineParams.topMargin = height * 2 / 100;
        lineParams.bottomMargin = height * 1 / 100;
        lineParams.leftMargin = width * 1 / 100;
        lineParams.rightMargin = width * 1 / 100;
        lv.setLayoutParams(lineParams);

        if (width >= 600) {

            mTxtNoRecord.setTextSize(18);
            mHeadTitle.setTextSize(18);
        } else if (width > 501 && width < 600) {

            mTxtNoRecord.setTextSize(17);
            mHeadTitle.setTextSize(17);
        } else if (width > 260 && width < 500) {

            mTxtNoRecord.setTextSize(16);
            mHeadTitle.setTextSize(16);

        } else if (width <= 260) {

            mTxtNoRecord.setTextSize(15);
            mHeadTitle.setTextSize(15);
        }
    }

}
