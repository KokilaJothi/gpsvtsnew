package com.vamosys.vamos;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.Notification;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.preference.PreferenceManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.material.tabs.TabLayout;
import com.vamosys.adapter.SensorFuelAdapter;
import com.vamosys.adapter.SensorTheftAdapter;
import com.vamosys.interfaces.ShowMapInterface;
import com.vamosys.model.FuelTheftDto;
import com.vamosys.utils.ConnectionDetector;
import com.vamosys.utils.Constant;
import com.vamosys.utils.HttpConfig;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.osmdroid.api.IMapController;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.infowindow.InfoWindow;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class SensorFuelTheftFragment extends Fragment implements View.OnClickListener, OnMapReadyCallback , ShowMapInterface {
    private Menu menu;
    private int hour;
    private int minute;
    SimpleDateFormat timeFormatFuelFillReport = new SimpleDateFormat(
            "HH:mm:ss");

    SimpleDateFormat timeFormat = new SimpleDateFormat(
            "hh:mm:ss aa");

    SimpleDateFormat timeFormatShow = new SimpleDateFormat(
            "hh:mm aa");

    androidx.appcompat.app.AlertDialog dialogs;
    long from,to;

    Button mBtnSubmit;
    static TextView mTxtFromDate;
    static TextView mTxtEndDate;
    TextView mTxtstart;
    TextView mTxtend;
    TextView mTxtFromTime;
    TextView mTxtEndTime;
    static String mStartDate;
    static String mEndDate;
    String mStartTime;
    String mStartTimeValue;
    String mStartTimeFuelReport;
    String mEndTimeFuelReport;
    String mEndDateValue;
    String mEndTime;
    String mEndTimeValue;
    String mIntervalSpinnerSelectedValue;
    public static boolean isFromTime = false, isFromDate = false, isMapPresent = false;
    int mFromHourValue = 0, mFromMinuteValue = 0, mToHourValue = 0, mToMinuteValue = 0;
    ConnectionDetector cd;
    SharedPreferences sp;
    String post;
    static Const cons;
    private static ArrayList<FuelTheftDto> mFuelTheftReportData = new ArrayList<FuelTheftDto>();
    static ConstraintLayout mFuelDataMapLayout;
    static RecyclerView lv;
    LinearLayoutCompat list_layout;
    ImageView $ChangeView;
    SupportMapFragment mapFragment;

    static int width;
    static int height;
    TextView mTxtNoRecord;
    private static GoogleMap map;

    static double mLatitude = 0;
    static double mLongitude = 0;
    static float vehicle_zoom_level = 15.5F;
    private SensorTheftAdapter adapter;

    MapView mapview;
    private IMapController mapController;
    private ConstraintLayout mGoogleMapLayout;
    boolean isOsmEnabled = true;


    public SensorFuelTheftFragment(String postion) {
        if(Integer.parseInt(postion)==1){
            post="2";
        }
        else {
            post="1";
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view=inflater.inflate(R.layout.fuel_theft_report_fragment, container, false);
        try {
            cons = new Const();
            init(view);
            getTheftData();
        }catch(Exception e){
            Log.d("try","error "+e.getMessage());
        }

        return view;
    }

    private void getTheftData() {
        try {
            if (cd.isConnectingToInternet()) {
                new getFuelTheftReport().execute();
            } else {
                Toast.makeText(getContext(),
                        "Please check your network connection",
                        Toast.LENGTH_SHORT).show();
            }
        }catch(Exception e){
            Log.d("try","error "+e.getMessage());
        }

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // TODO Add your menu entries here
        this.menu = menu;
        inflater.inflate(R.menu.menu_calendaricon_closeicon, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        Log.d("fuelData","close map");
        switch (item.getItemId()) {
            case R.id.action_map_close:
                Log.d("fuelData","close map1");
                if(item.isVisible()){
                    item.setVisible(false);
                    lv.setVisibility(View.VISIBLE);
                    mFuelDataMapLayout.setVisibility(View.GONE);
                    MenuItem items = menu.findItem(R.id.action_calendar);
                    items.setVisible(true);
                }
//                MenuItem items = menu.findItem(R.id.action_map_close);
//                items.setVisible(false);
                break;
            case R.id.action_calendar:
                Log.d("fuelData","close calendar");
                dialogs.show();
                break;
        }
        return true;
    }

    private void init(View view) {

        try {
            cd = new ConnectionDetector(getContext());
            sp = PreferenceManager.getDefaultSharedPreferences(getContext());

            if (sp.getString("enabled_map", "") != null) {
                if (sp.getString("enabled_map", "").trim().length() > 0) {

                    System.out.println("hi enabled map is " + sp.getString("enabled_map", ""));

                    if (sp.getString("enabled_map", "").equalsIgnoreCase(getResources().getString(R.string.osm))) {
                        isOsmEnabled = true;
                    } else {
                        isOsmEnabled = false;
                    }
                }
            }
            showAlertDialog();

            if (Constant.TFuelStoredFromDate!=null&&Constant.TFuelStoredFromDate.length() > 1 && Constant.TFuelStoredToDate!=null&&Constant.TFuelStoredToDate.length() > 1 && Constant.TFuelStoredFromTime!=null&&Constant.TFuelStoredFromTime.length() > 1 && Constant.TFuelStoredToTime!=null&&Constant.TFuelStoredToTime.length() > 1) {
                Log.d("fueldata","old daata");
                mStartDate=Constant.TFuelStoredFromDate;
                mEndDate=Constant.TFuelStoredToDate;
                mStartTime=Constant.TFuelStoredFromTime ;
                mEndTime=Constant.TFuelStoredToTime;
                mStartTimeValue=Constant.StartTimeReport;
                mEndTimeValue=Constant.EndTimeReport;
            }
            else {
                Log.d("fueldata","new daata");
                mStartDate = Const.getCurrentDate();
                mEndDate = Const.getTripDate(String.valueOf(System.currentTimeMillis()));

                long timeInMillis = System.currentTimeMillis();
                Calendar cal1 = Calendar.getInstance();
                cal1.setTimeInMillis(timeInMillis);
                Calendar cal = Calendar.getInstance();

                cal.set(Calendar.HOUR_OF_DAY, 0);
                cal.set(Calendar.MINUTE, 0);
                cal.set(Calendar.SECOND, 0);
                cal.set(Calendar.MILLISECOND, 0);


                mFromHourValue = 00;
                mFromMinuteValue = 00;

                mStartTimeValue = timeFormatShow.format(cal.getTime());
//        mStartTime = "12:00:00 am";
                mStartTime = timeFormat.format(cal.getTime());
                mStartTimeFuelReport = timeFormatFuelFillReport.format(cal.getTime());

                final Calendar c = Calendar.getInstance();
                // Current Hour
                hour = c.get(Calendar.HOUR_OF_DAY);
                // Current Minute
                minute = c.get(Calendar.MINUTE);
                isFromTime = false;
                updateTime(hour, minute);
            }
            setBottomLayout();

            DisplayMetrics displayMetrics = new DisplayMetrics();
            getActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
            height = displayMetrics.heightPixels;
            width = displayMetrics.widthPixels;
            list_layout =  view.findViewById(R.id.list_layout);
            list_layout.setVisibility(View.VISIBLE);
            mFuelDataMapLayout =  view.findViewById(R.id.temperature_data_map_view_layout);
            mFuelDataMapLayout.setVisibility(View.GONE);
            lv = view.findViewById(R.id.tstoppage_report_listView1);
            $ChangeView = view.findViewById(R.id.temperature_data_map_ViewIcon);
            $ChangeView.setOnClickListener(this);
            lv.setVisibility(View.VISIBLE);
            Log.d("googlemap","success");
            $ChangeView.setVisibility(View.VISIBLE);
            mGoogleMapLayout=view.findViewById(R.id.google_map_layout);
            RelativeLayout rl = view.findViewById(R.id.map_view_relativelayout);

//        mGoogleMapLayout.setVisibility(View.VISIBLE);
//        rl.setVisibility(View.GONE);
            if (isOsmEnabled) {
                $ChangeView.setVisibility(View.GONE);
                mGoogleMapLayout.setVisibility(View.GONE);
                rl.setVisibility(View.VISIBLE);
                mapview = new MapView(getContext());
                mapview.setTilesScaledToDpi(true);
                rl.addView(mapview, new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.FILL_PARENT,
                        RelativeLayout.LayoutParams.FILL_PARENT));

                mapview.setBuiltInZoomControls(false);
                mapview.setMultiTouchControls(true);
                mapController = mapview.getController();
                mapController.setZoom(8);
                mapview.getOverlays().clear();
                mapview.invalidate();
                mapview.getOverlays().add(Constant.getTilesOverlay(getContext()));
            }
            else {
                $ChangeView.setVisibility(View.VISIBLE);
                mGoogleMapLayout.setVisibility(View.VISIBLE);
                rl.setVisibility(View.GONE);

                mapFragment = (SupportMapFragment) this.getChildFragmentManager().findFragmentById(R.id.temperature_data_map);
                mapFragment.getMapAsync(this);
                mTxtNoRecord = view.findViewById(R.id.report_no_record_txt);
            }

            mapFragment = (SupportMapFragment) this.getChildFragmentManager().findFragmentById(R.id.temperature_data_map);
            mapFragment.getMapAsync(this);
            mTxtNoRecord = view.findViewById(R.id.report_no_record_txt);
        }catch(Exception e){
            Log.d("try","error "+e.getMessage());
        }

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        Log.d("fueldata","onmapready");
        try {
            map = googleMap;
            map.setPadding(1, 1, 1, 150);
            map.getUiSettings().setZoomControlsEnabled(true);
        }catch(Exception e){
            Log.d("try","error "+e.getMessage());
        }


    }

    @Override
    public void showLatLng(double lat, double lng) {
        Log.d("fuelData","lat "+lat+"lng "+lng);
        try {
            mLatitude=lat;
            mLongitude=lng;
            MenuItem item = menu.findItem(R.id.action_map_close);
            item.setVisible(true);
            MenuItem items = menu.findItem(R.id.action_calendar);
            items.setVisible(false);
            lv.setVisibility(View.GONE);
            mFuelDataMapLayout.setVisibility(View.VISIBLE);
            if (isOsmEnabled) {
                setupOsmMap();
            } else {
                setupGoogleMap();
            }
        }catch(Exception e){
            Log.d("try","error "+e.getMessage());
        }

    }

    private class getFuelTheftReport extends AsyncTask<String, String, String> {
        ProgressDialog progressDialog;

        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub

            String result = null;
            try {
                String string_from_date = mStartDate.trim() + " " + mStartTime.trim();
                String string_to_date = mEndDate.trim() + " " + mEndTime.trim();
                SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss aa");
                Date d = f.parse(string_from_date);
                long fromMilliSeconds = d.getTime();
                Date d1 = f.parse(string_to_date);
                long toMilliSeconds = d1.getTime();
                HttpConfig ht = new HttpConfig();
//                result = ht.httpGet(Const.API_URL + "/mobile/getTheftReport?vehicleId=" + Constant.SELECTED_VEHICLE_ID + "&fromTimeUtc=" + fromMilliSeconds + "&toTimeUtc=" + toMilliSeconds + "&userId=" + Constant.SELECTED_USER_ID);
                result = ht.httpGet(Const.API_URL + "/mobile/getFuelDetailForMachinery?vehicleId=" + Constant.SELECTED_VEHICLE_ID + "&fromDateTime=" + fromMilliSeconds + "&toDateTime=" + toMilliSeconds + "&userId=" + Constant.SELECTED_USER_ID+"&sensor="+post);
//                result = ht.httpGet("http://gpsvts.net/mobile/getAcReport?userId=sainiji&vehicleId=UP16BT9281&fromTimeUtc=1511375400000&toTimeUtc=1511548200000");
            } catch (Exception e) {
                e.printStackTrace();
//                System.out.println("Hi exception is ::::" + e.toString());
            }
            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            progressDialog.dismiss();
            mFuelTheftReportData = new ArrayList<FuelTheftDto>();
            if (result != null && result.length() > 0) {
//                System.out.println("The Fuel fill result is ::::" + result);
                boolean acOnSelected = false;

                JSONArray jArrayFtReport = null, jArrResult = null;
                try {


                    JSONObject jsonObject = new JSONObject(result.trim());

                    if (jsonObject.has("fuelDrops")) {
                        jArrayFtReport = jsonObject.getJSONArray("fuelDrops");


                        ArrayList<FuelTheftDto> mSData = new ArrayList<FuelTheftDto>();
                        for (int i = 0; i < jArrayFtReport.length(); i++) {
                            JSONObject jsonStoppageObject = jArrayFtReport.getJSONObject(i);

                            FuelTheftDto f = new FuelTheftDto();


                            if (jsonStoppageObject.has("pFuel")) {
                                f.setPrevLitr(jsonStoppageObject.getString("pFuel"));
                            }
                            if (jsonStoppageObject.has("cFuel")) {
                                f.setCurrLitr(jsonStoppageObject.getString("cFuel"));
                            }
                            if (jsonStoppageObject.has("lt")) {
                                f.setTheftLitr(jsonStoppageObject.getDouble("lt"));
                            }
                            if (jsonStoppageObject.has("odo")) {
                                f.setOdoLitr(jsonStoppageObject.getDouble("odo"));
                            }

//                            if (jsonStoppageObject.has("theftLoc")) {
//                                f.setTheftLoc(jsonStoppageObject.getString("theftLoc"));
//                            }
                            if (jsonStoppageObject.has("time")) {
                                f.setDate(jsonStoppageObject.getString("time"));
                            }
                            if (jsonStoppageObject.has("startTime")) {
                                f.setStartdate(jsonStoppageObject.getString("startTime"));
                            }
                            if (jsonStoppageObject.has("lat")) {
                                f.setLatitude(jsonStoppageObject.getString("lat"));
                            }

                            if (jsonStoppageObject.has("lng")) {
                                f.setLongitude(jsonStoppageObject.getString("lng"));
                            }
                            Log.d("theftltr",""+jsonStoppageObject.getString("lt"));

                            mFuelTheftReportData.add(f);

                        }


                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }


                if (mFuelTheftReportData != null && mFuelTheftReportData.size() > 0) {
                    lv.setVisibility(View.VISIBLE);
                    mTxtNoRecord.setVisibility(View.GONE);
                    setTableLayoutData();
                } else {

                    lv.setAdapter(null);
                    lv.setVisibility(View.GONE);
                    mTxtNoRecord.setVisibility(View.VISIBLE);
                }
            }

        }

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            progressDialog = new ProgressDialog(getContext(),
                    AlertDialog.THEME_HOLO_LIGHT);
            progressDialog.setMessage("Please Wait...");
            progressDialog.setProgressDrawable(new ColorDrawable(
                    Color.BLUE));
            progressDialog.setCancelable(true);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.show();
        }

    }

    public void setTableLayoutData() {

        try {
            list_layout.setVisibility(View.VISIBLE);
            lv.setHasFixedSize(true);
            lv.setLayoutManager(new LinearLayoutManager(getContext()));
            adapter = new SensorTheftAdapter(getContext(),mFuelTheftReportData,this);
            lv.setAdapter(adapter);
            adapter.notifyDataSetChanged();
        }catch(Exception e){
            Log.d("try","error "+e.getMessage());
        }

    }
    public void setupGoogleMap() {
        try {
            if (map != null) {
                map.clear();
            }

            View marker = null;

            marker = ((LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.custommarker, null);
            ImageView id_custom_marker_icon = (ImageView) marker.findViewById(R.id.id_custom_marker_icon);
            id_custom_marker_icon.setImageResource(R.drawable.grey_custom_marker_icon);
            ImageView id_vehicle_in_marker = (ImageView) marker.findViewById(R.id.id_vehicle_in_marker);
            id_vehicle_in_marker.setVisibility(View.GONE);

            CameraPosition INIT = new CameraPosition.Builder().target(new LatLng(mLatitude, mLongitude)).zoom(vehicle_zoom_level).build();
            map.animateCamera(CameraUpdateFactory.newCameraPosition(INIT));

            MarkerOptions markerOption = new MarkerOptions().position(new LatLng(mLatitude, mLongitude));
            markerOption.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE));
            Marker currentMarker = map.addMarker(markerOption);
            currentMarker.showInfoWindow();
        }catch(Exception e){
            Log.d("try","error "+e.getMessage());
        }
        Log.d("fueldata","setupGoogleMap()");

    }
    public void setupOsmMap() {
        try {
            if (mapview != null) {
                InfoWindow.closeAllInfoWindowsOn(mapview);
                mapview.getOverlays().clear();
                mapview.invalidate();
                mapview.getOverlays().add(Constant.getTilesOverlay(getContext()));
            }
            LayoutInflater factory = LayoutInflater.from(getContext());
            View markerView =  factory.inflate(R.layout.marker_image_view, null);
            ImageView mImgMarkerIcon = (ImageView) markerView.findViewById(R.id.img_marker_icon);
            mImgMarkerIcon.setImageResource(R.drawable.green_custom_marker_icon);
//        animateMarker.setIcon(createDrawableFromViewNew(AcReport.this, markerView));
            org.osmdroid.views.overlay.Marker osmMarker = new org.osmdroid.views.overlay.Marker(mapview);
            osmMarker.setPosition(new GeoPoint(mLatitude, mLongitude));
            osmMarker.setAnchor(org.osmdroid.views.overlay.Marker.ANCHOR_CENTER, org.osmdroid.views.overlay.Marker.ANCHOR_BOTTOM);
//        osmMarker.setTitle(Constant.SELECTED_VEHICLE_SHORT_NAME);
            osmMarker.setIcon(Constant.createDrawableFromViewNew(getContext(), markerView));
//        InfoWindow infoWindow = new FuelTheftReportActivity.MyInfoWindow(R.layout.map_info_txt_layout, mapview, addressValue);
//        osmMarker.setInfoWindow(infoWindow);
//        osmMarker.showInfoWindow();

            final GeoPoint newPos = new GeoPoint(mLatitude, mLongitude);
//        mapController.setCenter(newPos);

            new Handler(Looper.getMainLooper()).post(
                    new Runnable() {
                        public void run() {
                            mapview.getController().setCenter(newPos);
                        }
                    }
            );

            mapview.getOverlays().add(osmMarker);
            mapview.invalidate();

        }catch(Exception e){
            Log.d("try","error "+e.getMessage());
        }


    }
    private void setBottomLayout() {
        try {
            mTxtFromDate.setText(mStartDate);
            mTxtEndDate.setText(mEndDate);
            mTxtFromTime.setText(mStartTimeValue);
            mTxtEndTime.setText(mEndTimeValue);
        }catch(Exception e){
            Log.d("try","error "+e.getMessage());
        }

    }

    private void updateTime(int hours, int mins) {

        try {
            Calendar cal = Calendar.getInstance();
            cal.set(Calendar.HOUR_OF_DAY, hours);
            cal.set(Calendar.MINUTE, mins);
            cal.set(Calendar.SECOND, 0);
            cal.set(Calendar.MILLISECOND, 0);
            String aTime = timeFormatShow.format(cal.getTime());
            String aTime2 = timeFormat.format(cal.getTime());
            String aTime3 = timeFormatFuelFillReport.format(cal.getTime());
            SimpleDateFormat hourFormat = new SimpleDateFormat(
                    "HH");
            SimpleDateFormat minuteFormat = new SimpleDateFormat(
                    "mm");
            if (isFromTime) {
                mStartTime = aTime2;
                mStartTimeValue = aTime;
                mStartTimeFuelReport = aTime3;
                mFromHourValue = Integer.valueOf(hourFormat.format(cal.getTime()));
                mFromMinuteValue = Integer.valueOf(minuteFormat.format(cal.getTime()));
                mTxtFromTime.setText(mStartTimeValue);
            }
            else {
                mEndTime = aTime2;
                mEndTimeValue = aTime;
                mEndTimeFuelReport = aTime3;
                mTxtEndTime.setText(mEndTimeValue);
                mToHourValue = Integer.valueOf(hourFormat.format(cal.getTime()));
                mToMinuteValue = Integer.valueOf(minuteFormat.format(cal.getTime()));
            }
        }catch(Exception e){
            Log.d("try","error "+e.getMessage());
        }

    }

    private void showAlertDialog(){

        try {
            Log.d("show alert","show3");
            androidx.appcompat.app.AlertDialog.Builder builders = new androidx.appcompat.app.AlertDialog.Builder(getContext(),R.style.CustomAlertDialog);
            final View customLayout = LayoutInflater.from(getContext()).inflate(R.layout.ac_new_report_bottom_layout, null);
            mTxtFromDate = customLayout.findViewById(R.id.txt_start_date_value);
            mTxtFromTime = customLayout.findViewById(R.id.txt_start_time_value);
            mTxtEndDate = customLayout.findViewById(R.id.txt_end_date_value);
            mTxtEndTime =customLayout.findViewById(R.id.txt_end_time_value);
            mTxtstart = customLayout.findViewById(R.id.txt_date_start);
            mTxtend = customLayout.findViewById(R.id.txt_date_end);
            mBtnSubmit = customLayout.findViewById(R.id.btn_done);

            mTxtFromTime.setOnClickListener(this);
            mTxtFromDate.setOnClickListener(this);
            mTxtEndDate.setOnClickListener(this);
            mTxtEndTime.setOnClickListener(this);
            mBtnSubmit.setOnClickListener(this);

            builders.setView(customLayout);
            builders.setCancelable(false);
            dialogs= builders.create();
        }catch(Exception e){
            Log.d("try","error "+e.getMessage());
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txt_start_date_value:
                isFromDate = true;
                DialogFragment dialogfragment = new DatePickerDialogTheme();
                dialogfragment.show(getActivity().getFragmentManager(), "Theme");

                break;
            case R.id.txt_start_time_value:
                isFromTime = true;
                showTimeDialog();
                break;
            case R.id.txt_end_date_value:
                isFromDate = false;
                DialogFragment dialogfragment1 = new DatePickerDialogTheme();
                dialogfragment1.show(getActivity().getFragmentManager(),"Theme");
                break;
            case R.id.txt_end_time_value:
                isFromTime = false;
                showTimeDialog();
                break;
            case R.id.btn_done:
                Constant.TFuelStoredFromDate=mStartDate;
                Constant.TFuelStoredToDate=mEndDate;
                Constant.TFuelStoredFromTime=mStartTime;
                Constant.TFuelStoredToTime=mEndTime;
                Constant.StartTimeReport=mStartTimeValue;
                Constant.EndTimeReport=mEndTimeValue;
                from=convertDataTolong(mStartDate.trim(),mStartTime.trim());
                to=convertDataTolong(mEndDate.trim(),mEndTime.trim());
                dialogs.dismiss();
                lv.setAdapter(null);
                getTheftData();
//                loadFragment(position,chart);
                break;
        }
    }

    private long convertDataTolong(String trim, String trim1) {
        long fromMilliSeconds=0;
        try {
            String string_from_date = trim + " " + trim1;
            SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss aa");
            try {
                Date d = f.parse(string_from_date);
                fromMilliSeconds = d.getTime();
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }catch(Exception e){
            Log.d("try","error "+e.getMessage());
        }

        return fromMilliSeconds;

    }

    @SuppressLint("ValidFragment")
    public static class DatePickerDialogTheme extends DialogFragment implements DatePickerDialog.OnDateSetListener{
        private boolean isFromDate=SensorFuelTheftFragment.isFromDate;
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState){
            final Calendar calendar = Calendar.getInstance();
            int year = calendar.get(Calendar.YEAR);
            int month = calendar.get(Calendar.MONTH);
            int day = calendar.get(Calendar.DAY_OF_MONTH);

            DatePickerDialog datepickerdialog = new DatePickerDialog(getActivity(),
                    AlertDialog.THEME_DEVICE_DEFAULT_DARK,this,year,month,day);
            return datepickerdialog;
        }

        public void onDateSet(DatePicker view, int year, int month, int day){
            year = year;
            month = month;
            day = day;
            Log.d("fuelData","isFromData "+isFromDate);
            if(isFromDate){
                StringBuilder mDate2 = new StringBuilder().append(year).append("-")
                        .append((month + 1)).append("-").append((day))
                        .append(" ");
                mTxtFromDate.setText(mDate2);
                mStartDate = String.valueOf(mDate2);
            }
            else {

                StringBuilder mDate2 = new StringBuilder().append(year).append("-")
                        .append((month + 1)).append("-").append((day))
                        .append(" ");
                mTxtEndDate.setText(mDate2);
                mEndDate = String.valueOf(mDate2);
            }
        }
    }

    private void showTimeDialog() {
        try {
            if (isFromTime) {
                hour = mFromHourValue;
                minute = mFromMinuteValue;
            } else {
                hour = mToHourValue;
                minute = mToMinuteValue;
            }
            TimePickerDialog dialog2 = new TimePickerDialog(getContext(), timePickerListener, hour, minute,
                    false);
            dialog2.show();
        }catch(Exception e){
            Log.d("try","error "+e.getMessage());
        }


    }

    private TimePickerDialog.OnTimeSetListener timePickerListener = new TimePickerDialog.OnTimeSetListener() {
        @Override
        public void onTimeSet(TimePicker view, int hourOfDay, int minutes) {
            // TODO Auto-generated method stub
            hour = hourOfDay;
            minute = minutes;
            updateTime(hour, minute);
        }
    };
}
