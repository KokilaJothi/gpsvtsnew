package com.vamosys.vamos;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import androidx.annotation.NonNull;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.NumberPicker;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.vamosys.adapter.DataAdapter;
import com.vamosys.model.CameraReportDto;
import com.vamosys.utils.ConnectionDetector;
import com.vamosys.utils.Constant;
import com.vamosys.utils.HttpConfig;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class CameraReportIamgeListActivtiy extends AppCompatActivity implements View.OnClickListener {
    Toolbar mToolbar;

    ConnectionDetector cd;
    SharedPreferences sp;
    String mFromDate = null, mFromTime, mToTime, mFromDateTxtValue;
    private RecyclerView mRecyclerView;
    List<CameraReportDto> mCameraReportList;
    BottomSheetBehavior behavior;
    boolean bottomSheetEnabled = false;
    TextView mTxtDate, mTxtFromTime, mTxtToTime;
    Button mBtnDone;
    //    private CameraImageAdapter mAdapter;
    View bottomSheet;

    private int year, month, day;
    Spinner spinnerFromHour, spinnerEndHour;

    static final int DATE_DIALOG_FROMID = 1;
    NumberPicker numberpicker;
    SimpleDateFormat dftime = new SimpleDateFormat("kk");
    List<Integer> mHourList = new ArrayList<Integer>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera_report_iamge_list_activtiy);
        initViews();
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mToolbar.setTitle(getResources().getString(R.string.camera_report));
        mToolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_action_back));
        setSupportActionBar(mToolbar);

        cd = new ConnectionDetector(getApplicationContext());

        sp = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        if (sp.getString("ip_adds", "") != null) {
            if (sp.getString("ip_adds", "").trim().length() > 0) {
                Const.API_URL = sp.getString("ip_adds", "");
            }
        }
        mHourList = formHourValue();

        SimpleDateFormat dfDate = new SimpleDateFormat("yyyyMMdd");


        mFromDateTxtValue = Const.getTripDate(String.valueOf(System.currentTimeMillis()));
        mFromDate = dfDate.format(new Date());
        mFromTime = dftime.format(new Date(System.currentTimeMillis() - 3600 * 1000));
        mToTime = dftime.format(new Date());
        if (cd.isConnectingToInternet()) {

            new getCameraReport().execute();
        } else {
            Toast.makeText(getApplicationContext(),
                    "Please check your network connection",
                    Toast.LENGTH_SHORT).show();
        }


        bottomSheet = findViewById(R.id.bottom_sheet);
        behavior = BottomSheetBehavior.from(bottomSheet);
        behavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                // React to state change
//                System.out.println("Hi state 0000" + bottomSheetEnabled);


            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
                // React to dragging events
            }
        });
//

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

//                System.out.println("Hi state " + bottomSheetEnabled);


                if (!bottomSheetEnabled) {
                    behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                } else {
                    setData();
                    behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                }

                if (bottomSheetEnabled) {
                    bottomSheetEnabled = false;
                } else {
                    bottomSheetEnabled = true;
                }


            }
        });
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                startActivity(new Intent(CameraReportIamgeListActivtiy.this, VehicleListActivity.class));
                finish();


            }
        });
        setData();
//        setFromSpinnerValue();


        spinnerFromHour
                .setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        mFromTime = String.valueOf(mHourList.get(position));
                        // mTxtFromTime.setText(mHourList.get(position) + ":00");
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });

        spinnerEndHour
                .setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        mToTime = String.valueOf(mHourList.get(position));
                        // mTxtFromTime.setText(mHourList.get(position) + ":00");
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });

    }

    private void setData() {
        mTxtDate.setText(mFromDateTxtValue);
        //  mTxtFromTime.setText(mFromTime + ":00");
        // mTxtToTime.setText(mToTime + ":00");

        setFromSpinnerValue();
        setEndSpinnerValue();
    }

    private void setFromSpinnerValue() {
        ArrayAdapter dataAdapter = new ArrayAdapter(this, R.layout.spinner_row, mHourList);

        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        // spinnerFromHour.setPrompt("Select From Hour");
        spinnerFromHour.setAdapter(dataAdapter);
        spinnerFromHour.setSelection(Integer.parseInt(mFromTime));
    }

    private void setEndSpinnerValue() {
        ArrayAdapter dataAdapter = new ArrayAdapter(this, R.layout.spinner_row, mHourList);

        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        // spinnerFromHour.setPrompt("Select From Hour");
        spinnerEndHour.setAdapter(dataAdapter);
        spinnerEndHour.setSelection(Integer.parseInt(mToTime));
    }




    private void initViews() {
        mRecyclerView = (RecyclerView) findViewById(R.id.card_recycler_view);
        mRecyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(layoutManager);

        mTxtDate = (TextView) findViewById(R.id.txt_date_value);
        mTxtFromTime = (TextView) findViewById(R.id.txt_start_time_value);
        mTxtToTime = (TextView) findViewById(R.id.txt_end_time_value);
        mBtnDone = (Button) findViewById(R.id.btn_done);

        numberpicker = (NumberPicker) findViewById(R.id.numberPicker1);

        spinnerFromHour = (Spinner) findViewById(R.id.spinner_from_time);
        spinnerEndHour = (Spinner) findViewById(R.id.spinner_end_time);

        mTxtDate.setOnClickListener(this);
        mTxtFromTime.setOnClickListener(this);
        mTxtToTime.setOnClickListener(this);
        mBtnDone.setOnClickListener(this);
    }

    private String getMonthValue(int month) {
        String mMonthValue = String.valueOf(month);
        if (mMonthValue.length() == 1) {
            mMonthValue = "0" + mMonthValue;
        }
        return mMonthValue;
    }

    private List<Integer> formHourValue() {
        List<Integer> mHrList = new ArrayList<Integer>();
        mHrList.add(0);
        mHrList.add(1);
        mHrList.add(2);
        mHrList.add(3);
        mHrList.add(4);
        mHrList.add(5);
        mHrList.add(6);
        mHrList.add(7);
        mHrList.add(8);
        mHrList.add(9);
        mHrList.add(10);
        mHrList.add(11);
        mHrList.add(12);
        mHrList.add(13);
        mHrList.add(14);
        mHrList.add(15);
        mHrList.add(16);
        mHrList.add(17);
        mHrList.add(18);
        mHrList.add(19);
        mHrList.add(20);
        mHrList.add(21);
        mHrList.add(22);
        mHrList.add(23);

        return mHrList;
    }

    @Override
    protected Dialog onCreateDialog(int id) {


        final Calendar c = Calendar.getInstance();
        year = c.get(Calendar.YEAR);
        month = c.get(Calendar.MONTH);
        day = c.get(Calendar.DAY_OF_MONTH);
        /** set date picker as current date */
        DatePickerDialog dialog = new DatePickerDialog(this,
                datePickerListener, year, month, day);
//        dialog.getDatePicker()
//                .setMaxDate(System.currentTimeMillis());
        return dialog;


    }

    private DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {

        // when dialog box is closed, below method will be called.
        public void onDateSet(DatePicker view, int selectedYear,
                              int selectedMonth, int selectedDay) {

            year = selectedYear;
            month = selectedMonth;
            day = selectedDay;
            StringBuilder mDate = new StringBuilder().append(year)
                    .append(getMonthValue(month + 1)).append(getMonthValue(day))
                    .append("");

            StringBuilder mDate2 = new StringBuilder().append(year).append("-")
                    .append(getMonthValue(month + 1)).append("-").append(getMonthValue(day))
                    .append(" ");
            mFromDate = String.valueOf(mDate);
            mFromDateTxtValue = String.valueOf(mDate2);
            mTxtDate.setText(mDate2);
            // }
        }
    };
    int mSelectedNumber = 0;


    public void show(final int mType) {

        final Dialog d = new Dialog(CameraReportIamgeListActivtiy.this);
        d.setTitle("Select Time");

        if (mType == 0) {
            d.setTitle("Select From Time");
        } else {
            d.setTitle("Select To Time");
        }

        d.setContentView(R.layout.pop_up_number_picker);
        Button b1 = (Button) d.findViewById(R.id.button1);
        Button b2 = (Button) d.findViewById(R.id.button2);
        final NumberPicker np = (NumberPicker) d.findViewById(R.id.numberPicker1);
        np.setMaxValue(23);
        np.setMinValue(0);
        np.setWrapSelectorWheel(false);
//        np.setOnValueChangedListener(this);

        np.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                mSelectedNumber = newVal;
            }
        });

        b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mType == 0) {
                    mFromTime = String.valueOf(mSelectedNumber);
                    mTxtFromTime.setText(mSelectedNumber + ":00");
                } else {
                    mToTime = String.valueOf(mSelectedNumber);
                    mTxtToTime.setText(mSelectedNumber + ":00");
                }
                d.dismiss();
            }
        });
        b2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                d.dismiss();
            }
        });
        d.show();


    }


//    private void showNumberDialog(int maxNumber, final int mType) {
//        // TODO Auto-generated method stubw
//        final Dialog numberPickDialog = new Dialog(CameraReportIamgeListActivtiy.this);
//        numberPickDialog.getWindow();
//        numberPickDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        numberPickDialog.setContentView(R.layout.pop_up_number_picker);
//        numberPickDialog.show();
//        numberPickDialog.setCanceledOnTouchOutside(true);
//        numberPickDialog.getWindow().setSoftInputMode(
//                WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
//
//        NumberPicker numberpicker = (NumberPicker) numberPickDialog.findViewById(R.id.numberPicker1);
//        TextView mTxtTitle = (TextView) numberPickDialog.findViewById(R.id.textView1);
//        Button mBtnDone = (Button) numberPickDialog.findViewById(R.id.number_dialog_done);
//
//        if (mType == 0) {
//            mTxtTitle.setText("Select From Time");
//        } else {
//            mTxtTitle.setText("Select To Time");
//        }
//
//        numberpicker.setMinValue(0);
//
//        numberpicker.setMaxValue(maxNumber);
//
//        numberpicker.setWrapSelectorWheel(true);
//
//        numberpicker.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
//            @Override
//            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
//                mSelectedNumber = newVal;
//            }
//        });
//
//        mBtnDone.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (mType == 0) {
//                    mFromTime = String.valueOf(mSelectedNumber);
//                    mTxtFromTime.setText(mSelectedNumber + ":00");
//                } else {
//                    mToTime = String.valueOf(mSelectedNumber);
//                    mTxtToTime.setText(mSelectedNumber + ":00");
//                }
//                numberPickDialog.dismiss();
//            }
//        });
//
//
//    }
//

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        startActivity(new Intent(CameraReportIamgeListActivtiy.this, VehicleListActivity.class));
        finish();

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_done:
                behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);

                new getCameraReport().execute();

                break;
            case R.id.txt_date_value:
                showDialog(DATE_DIALOG_FROMID);
                break;
            case R.id.txt_end_time_value:
//                createNumberDialog(1);

                show(1);

                break;
            case R.id.txt_start_time_value:
//                createNumberDialog(0);
                show(0);
                break;
        }
    }

//    private void createNumberDialog(int mType) {
//        if (Const.daysDiff(mFromDateTxtValue) > 0) {
//
//            showNumberDialog(23, mType);
//
//        } else {
//            if (mType == 0) {
//                String mToTimeValue = dftime.format(new Date());
//
//                int mTovalue = Integer.valueOf(mToTimeValue);
//                if (mTovalue > 0) {
//                    mTovalue = mTovalue - 1;
//                }
//
//                showNumberDialog(mTovalue, mType);
//            } else {
//                String mToTimeValue = dftime.format(new Date());
//
//                int mTovalue = Integer.valueOf(mToTimeValue);
//
//
//                showNumberDialog(mTovalue, mType);
//            }
//        }
//
//    }

    private class getCameraReport extends AsyncTask<String, String, String> {
        ProgressDialog progressDialog;

        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub

            String result = null;
            try {

                HttpConfig ht = new HttpConfig();
                result = ht.httpGet(Const.API_URL + "/mobile/getPictures?vehicleId=" + Constant.SELECTED_VEHICLE_ID + "&date=" + mFromDate + "&sTime=" + mFromTime + "&eTime=" + mToTime + "&userId=" + Constant.SELECTED_USER_ID);

            } catch (Exception e) {

            }

            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            progressDialog.dismiss();
            mCameraReportList = new ArrayList<CameraReportDto>();
            if (result != null && result.length() > 0) {


                try {
                    JSONArray jArr = new JSONArray(result.trim());


                    for (int i = 0; i < jArr.length(); i++) {
                        JSONObject jsonCameraObject = jArr.getJSONObject(i);
                        CameraReportDto c = new CameraReportDto();

                        if (jsonCameraObject.has("address")) {

                            c.setAddress(jsonCameraObject.getString("address"));
                        }
                        if (jsonCameraObject.has("imageName")) {

                            c.setImageName(jsonCameraObject.getString("imageName").trim());
                        }
                        if (jsonCameraObject.has("lat")) {

                            c.setLat(jsonCameraObject.getString("lat"));
                        }
                        if (jsonCameraObject.has("lng")) {

                            c.setLng(jsonCameraObject.getString("lng"));
                        }
                        if (jsonCameraObject.has("timeStamp")) {

                            c.setTimeStamp(jsonCameraObject.getLong("timeStamp"));

                        }

                        mCameraReportList.add(c);

                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }

            if (mCameraReportList != null && mCameraReportList.size() > 0) {

                //mAdapter = new CameraImageAdapter(mCameraReportList, CameraReportIamgeListActivtiy.this);

                DataAdapter mAadp = new DataAdapter(mCameraReportList, CameraReportIamgeListActivtiy.this);
                mRecyclerView.setAdapter(mAadp);
                mAadp.notifyDataSetChanged();

            } else {
                mRecyclerView.setAdapter(null);
                Toast.makeText(CameraReportIamgeListActivtiy.this, "No record found", Toast.LENGTH_SHORT).show();
                //show date selection layout
            }

        }

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            progressDialog = new ProgressDialog(CameraReportIamgeListActivtiy.this,
                    AlertDialog.THEME_HOLO_LIGHT);
            progressDialog.setMessage("Please Wait...");
            progressDialog.setProgressDrawable(new ColorDrawable(
                    android.graphics.Color.BLUE));
            progressDialog.setCancelable(true);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.show();
        }

    }

}
