package com.vamosys.vamos;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.preference.PreferenceManager;
import android.text.Html;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.listener.ChartTouchListener;
import com.github.mikephil.charting.listener.OnChartGestureListener;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.Utils;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.vamosys.adapter.SensorFuelAdapter;
import com.vamosys.interfaces.ShowMapInterface;
import com.vamosys.model.FuelFillDto;
import com.vamosys.utils.ConnectionDetector;
import com.vamosys.utils.Constant;
import com.vamosys.utils.HorizontalScrollView;
import com.vamosys.utils.HttpConfig;
import com.vamosys.utils.MyMarkerFuelView;
import com.vamosys.utils.TypefaceUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.osmdroid.api.IMapController;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.infowindow.InfoWindow;

import java.util.ArrayList;
import java.util.List;

import static android.content.Context.LAYOUT_INFLATER_SERVICE;

public class SensorFuelFillFragment  extends Fragment implements OnMapReadyCallback, View.OnClickListener, OnChartGestureListener, ShowMapInterface,
        OnChartValueSelectedListener{
    String post;
    static Const cons;
    static ConstraintLayout mFuelDataMapLayout;
    static RecyclerView lv;
    static double mLatitude = 0;
    static double mLongitude = 0;
    static float vehicle_zoom_level = 15.5F;
    static Context mFuelContext;
    private static GoogleMap map;
    static int width;
    static int height;

    private List<FuelFillDto> mFuelFillData = new ArrayList<FuelFillDto>();
    long from,to;
    String interval;
    LinearLayoutCompat list_layout;
    ImageView $ChangeView;
    private LineChart mChart;
    LinearLayout mChartLayout;
    boolean isHeaderPresent = false, mIsLineChartEnabled = false;

    ArrayList<Integer> xAxis;
    SupportMapFragment mapFragment;
    MapView mapview;
    private IMapController mapController;
    private ConstraintLayout mGoogleMapLayout;
    boolean isOsmEnabled = true;
    ConnectionDetector cd;
    SharedPreferences sp;

    double mTotalFuelConsume,myvlue, mTotalFuelFill, mTripDistance, mOdoStart, mOdoEnd, mTotalMileage;
    SensorFuelAdapter adapter;
    TextView mTxtstart,mTxtend,mTxtNoRecord, mTxtVehicleName, mTxtTotalFuelConsume, mTxtTripDistance, mTxtTotalFuelFill,mTxtstartfuel,mTxtendfuel,mTxtTotalMileage, mTxtOdoStart, mTxtOdoEnd;
    String mStartDate, mStartTimeFuelReport, mEndDate, mEndTimeFuelReport;
    public SensorFuelFillFragment(String postion, Boolean chart, String mStartDate, String mStartTime, String mEndDate, String mEndTime, long from, long to, String interval) {
        Log.d("fuelreport","pos "+postion+" chart "+chart+" from "+from+" to "+to+" interval "+interval);
        if(Integer.parseInt(postion)==1){
            post="2";
        }
        else {
            post="1";
        }
        this.mIsLineChartEnabled=chart;
        this.from=from;
        this.to=to;
        this.interval=interval;
        this.mStartDate=mStartDate;
        this.mStartTimeFuelReport =mStartTime;
        this.mEndDate=mEndDate;
        this.mEndTimeFuelReport =mEndTime;
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.sensor_fuelfill_fragment, container, false);
        try {
            cons = new Const();
            init(view);
            new getKmsSummaryData().execute();
            new getfuelmachinery().execute();
        }catch(Exception e){
            Log.d("try","error "+e.getMessage());
        }

        return view;
    }



    private void init(View view) {
        try {
            cd = new ConnectionDetector(getContext());
            sp = PreferenceManager.getDefaultSharedPreferences(getContext());

            if (sp.getString("enabled_map", "") != null) {
                if (sp.getString("enabled_map", "").trim().length() > 0) {

                    System.out.println("hi enabled map is " + sp.getString("enabled_map", ""));

                    if (sp.getString("enabled_map", "").equalsIgnoreCase(getResources().getString(R.string.osm))) {
                        isOsmEnabled = true;
                    } else {
                        isOsmEnabled = false;
                    }
                }
            }
            DisplayMetrics displayMetrics = new DisplayMetrics();
            getActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
            height = displayMetrics.heightPixels;
            width = displayMetrics.widthPixels;
            list_layout =  view.findViewById(R.id.list_layout);
            list_layout.setVisibility(View.VISIBLE);
//        mGoogleMapLayout =  view.findViewById(R.id.google_map_layout);
//        RelativeLayout rl =  view.findViewById(R.id.map_view_relativelayout);
            mFuelDataMapLayout =  view.findViewById(R.id.temperature_data_map_view_layout);
            mFuelDataMapLayout.setVisibility(View.GONE);
            lv = view.findViewById(R.id.tstoppage_report_listView1);
            $ChangeView = view.findViewById(R.id.temperature_data_map_ViewIcon);
            $ChangeView.setOnClickListener(this);

            mChart = view.findViewById(R.id.chart);
            mChart.zoom(4, 1, 1, 1);
            mChartLayout = view.findViewById(R.id.chart_layout);

            mTxtNoRecord = view.findViewById(R.id.report_no_record_txt);

            mTxtTotalFuelConsume = view.findViewById(R.id.txt_total_fuel_consume);
            mTxtTripDistance = view.findViewById(R.id.txt_trip_distance);
            mTxtTotalFuelFill = view.findViewById(R.id.txt_total_fuel_fill);
            mTxtstartfuel = view.findViewById(R.id.txt_start_fuel);
            mTxtendfuel = view.findViewById(R.id.txt_end_fuel);

            mTxtOdoStart = view.findViewById(R.id.txt_odo_start);
            mTxtOdoEnd = view.findViewById(R.id.txt_odo_end);
            mTxtstart = view.findViewById(R.id.txt_date_start);
            mTxtend = view.findViewById(R.id.txt_date_end);
            mTxtTotalFuelFill.setOnClickListener(this);
            mChartLayout.setVisibility(View.GONE);
            lv.setVisibility(View.VISIBLE);
            Log.d("googlemap","success");
            $ChangeView.setVisibility(View.VISIBLE);
            mGoogleMapLayout=view.findViewById(R.id.google_map_layout);
            RelativeLayout rl = view.findViewById(R.id.map_view_relativelayout);
//        mGoogleMapLayout.setVisibility(View.VISIBLE);
//        rl.setVisibility(View.GONE);

            if (isOsmEnabled) {
                $ChangeView.setVisibility(View.GONE);
                mGoogleMapLayout.setVisibility(View.GONE);
                rl.setVisibility(View.VISIBLE);
                mapview = new MapView(getContext());
                mapview.setTilesScaledToDpi(true);
                rl.addView(mapview, new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.FILL_PARENT,
                        RelativeLayout.LayoutParams.FILL_PARENT));

                mapview.setBuiltInZoomControls(false);
                mapview.setMultiTouchControls(true);
                mapController = mapview.getController();
                mapController.setZoom(8);
                mapview.getOverlays().clear();
                mapview.invalidate();
                mapview.getOverlays().add(Constant.getTilesOverlay(getContext()));
            }
            else {
                $ChangeView.setVisibility(View.VISIBLE);
                mGoogleMapLayout.setVisibility(View.VISIBLE);
                rl.setVisibility(View.GONE);

                mapFragment = (SupportMapFragment) this.getChildFragmentManager().findFragmentById(R.id.temperature_data_map);
                mapFragment.getMapAsync(this);
                mTxtNoRecord = view.findViewById(R.id.report_no_record_txt);
            }
        }catch(Exception e){
            Log.d("try","error "+e.getMessage());
        }


    }
    public void setupOsmMap() {
        try {
            if (mapview != null) {
                InfoWindow.closeAllInfoWindowsOn(mapview);
                mapview.getOverlays().clear();
                mapview.invalidate();
                mapview.getOverlays().add(Constant.getTilesOverlay(getContext()));
            }
            LayoutInflater factory = LayoutInflater.from(getContext());
            View markerView =  factory.inflate(R.layout.marker_image_view, null);
            ImageView mImgMarkerIcon = (ImageView) markerView.findViewById(R.id.img_marker_icon);
            mImgMarkerIcon.setImageResource(R.drawable.green_custom_marker_icon);
//        animateMarker.setIcon(createDrawableFromViewNew(AcReport.this, markerView));
            org.osmdroid.views.overlay.Marker osmMarker = new org.osmdroid.views.overlay.Marker(mapview);
            osmMarker.setPosition(new GeoPoint(mLatitude, mLongitude));
            osmMarker.setAnchor(org.osmdroid.views.overlay.Marker.ANCHOR_CENTER, org.osmdroid.views.overlay.Marker.ANCHOR_BOTTOM);
//        osmMarker.setTitle(Constant.SELECTED_VEHICLE_SHORT_NAME);
            osmMarker.setIcon(Constant.createDrawableFromViewNew(getContext(), markerView));
//        InfoWindow infoWindow = new FuelTheftReportActivity.MyInfoWindow(R.layout.map_info_txt_layout, mapview, addressValue);
//        osmMarker.setInfoWindow(infoWindow);
//        osmMarker.showInfoWindow();

            final GeoPoint newPos = new GeoPoint(mLatitude, mLongitude);
//        mapController.setCenter(newPos);

            new Handler(Looper.getMainLooper()).post(
                    new Runnable() {
                        public void run() {
                            mapview.getController().setCenter(newPos);
                        }
                    }
            );

            mapview.getOverlays().add(osmMarker);
            mapview.invalidate();

        }catch(Exception e){
            Log.d("try","error "+e.getMessage());
        }


    }
    public void setupGoogleMap() {
        try {
            if (map != null) {
                map.clear();
            }

            View marker = null;

            marker = ((LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.custommarker, null);
            ImageView id_custom_marker_icon = (ImageView) marker.findViewById(R.id.id_custom_marker_icon);
            id_custom_marker_icon.setImageResource(R.drawable.grey_custom_marker_icon);
            ImageView id_vehicle_in_marker = (ImageView) marker.findViewById(R.id.id_vehicle_in_marker);
            id_vehicle_in_marker.setVisibility(View.GONE);

            CameraPosition INIT = new CameraPosition.Builder().target(new LatLng(mLatitude, mLongitude)).zoom(vehicle_zoom_level).build();
            map.animateCamera(CameraUpdateFactory.newCameraPosition(INIT));

            MarkerOptions markerOption = new MarkerOptions().position(new LatLng(mLatitude, mLongitude));
            markerOption.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE));
            Marker currentMarker = map.addMarker(markerOption);
            currentMarker.showInfoWindow();
        }catch(Exception e){
            Log.d("try","error "+e.getMessage());
        }
        Log.d("fueldata","setupGoogleMap()");

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        Log.d("fueldata","onmapready");
        try {
            map = googleMap;
            map.setPadding(1, 1, 1, 150);
            map.getUiSettings().setZoomControlsEnabled(true);
        }catch(Exception e){
            Log.d("try","error "+e.getMessage());
        }

    }

    @Override
    public void onClick(View view) {
        try {
            if(view.getId()==R.id.txt_total_fuel_fill){
                if (mTotalFuelFill > 0) {
                    Intent in = new Intent(getContext(), FuelFillReport.class);
                    in.putExtra("start_date", mStartDate);
                    in.putExtra("start_time", mStartTimeFuelReport);
                    in.putExtra("end_date", mEndDate);
                    in.putExtra("end_time", mEndTimeFuelReport);
                    in.putExtra("is_chart_enabled", mIsLineChartEnabled);
                    in.putExtra("sensor",post);
                    startActivity(in);
//                    getActivity().finish();
                }
            }
            else if(view.getId()==R.id.temperature_data_map_ViewIcon){
                opptionPopUp();
            }
        }catch(Exception e){
            Log.d("try","error "+e.getMessage());
        }

    }

    @Override
    public void onValueSelected(Entry e, Highlight h) {

    }

    @Override
    public void onNothingSelected() {

    }

    @Override
    public void onChartGestureStart(MotionEvent me, ChartTouchListener.ChartGesture lastPerformedGesture) {
        try {
            if (lastPerformedGesture != ChartTouchListener.ChartGesture.SINGLE_TAP)
                // or highlightTouch(null) for callback to onNothingSelected(...)
                mChart.highlightValues(null);
        }catch(Exception e){
            Log.d("try","error "+e.getMessage());
        }

    }

    @Override
    public void onChartGestureEnd(MotionEvent me, ChartTouchListener.ChartGesture lastPerformedGesture) {

    }

    @Override
    public void onChartLongPressed(MotionEvent me) {

    }

    @Override
    public void onChartDoubleTapped(MotionEvent me) {

    }

    @Override
    public void onChartSingleTapped(MotionEvent me) {

    }

    @Override
    public void onChartFling(MotionEvent me1, MotionEvent me2, float velocityX, float velocityY) {

    }

    @Override
    public void onChartScale(MotionEvent me, float scaleX, float scaleY) {

    }

    @Override
    public void onChartTranslate(MotionEvent me, float dX, float dY) {

    }

    @Override
    public void showLatLng(double lat, double lng) {

        Log.d("fuelData","show map "+lat+" "+lng);
        try {
            mLatitude=lat;
            mLongitude=lng;
            lv.setVisibility(View.GONE);
            mFuelDataMapLayout.setVisibility(View.VISIBLE);
            if (isOsmEnabled) {
                setupOsmMap();
            } else {
                setupGoogleMap();
            }
        }catch(Exception e){
            Log.d("try","error "+e.getMessage());
        }

    }

    private class getfuelmachinery extends AsyncTask<String, String, String> {
        ProgressDialog progressDialog;

        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub

            String result = null;
            try {
                HttpConfig ht = new HttpConfig();
                result = ht.httpGet(Const.API_URL + "/mobile/getFuelDetailForMachinery?vehicleId=" + Constant.SELECTED_VEHICLE_ID  + "&fromDateTime=" + from + "&toDateTime=" + to + "&userId=" + Constant.SELECTED_USER_ID+"&sensor="+post);

            } catch (Exception e) {

            }

            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            progressDialog.dismiss();
            mFuelFillData = new ArrayList<>();

            if (result != null && result.length() > 0) {
                String mPrevAdds = null;
                try {
                    JSONObject jsonObject = new JSONObject(result.trim());

                    if (jsonObject.getString("tFuelCon") !=null){
                        mTxtTotalFuelConsume.setText(jsonObject.getString("tFuelCon") + " ltrs");
                        //  mTotalFuelConsume = jsonObject.getDouble("totalFuelConsume");

                    }
                    if (jsonObject.getString("tdistance") !=null){
                        mTxtTripDistance.setText(jsonObject.getString("tdistance") + " kms");
//                    mTripDistance= jsonObject.getDouble("tdistance");
                    }


                    if (jsonObject.getString("sFuelLevel") !=null){
                        mTxtstartfuel.setText(jsonObject.getString("sFuelLevel") + " ltrs");
                    }

                    if (jsonObject.getString("eFuelLevel") !=null){
                        mTxtendfuel.setText(jsonObject.getString("eFuelLevel") + " ltrs");
                    }

                    mTotalFuelFill = jsonObject.getInt("totalFuelFills");

                    if (mTotalFuelFill > 0) {

                    } else {
                        mTotalFuelFill = 0;
                    }

                    if (mTotalFuelFill > 0) {
                        String mText = "<u>" + mTotalFuelFill + "</u>";
                        mTxtTotalFuelFill.setText(Html.fromHtml(mText) + " ltrs");
                        mTxtTotalFuelFill.setTextColor(Color.BLUE);
                    } else {
                        mTxtTotalFuelFill.setText("" + mTotalFuelFill + " ltrs");
                        mTxtTotalFuelFill.setTextColor(Color.BLACK);
                    }




                    Log.d("totalFuelFill",""+jsonObject.getString("totalFuelFills"));
                    Log.d("tdistance",""+jsonObject.getString("tdistance"));
                    Log.d("tFuelCo n",""+jsonObject.getString("tFuelCon"));


                }catch (JSONException e) {
                    e.printStackTrace();
                }


            } else {
                Toast.makeText(getContext(), "No record found.Please select different date time or interval", Toast.LENGTH_SHORT).show();
            }

        }
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            progressDialog = new ProgressDialog(getContext(),
                    AlertDialog.THEME_HOLO_LIGHT);
            progressDialog.setMessage("Please Wait...");
            progressDialog.setProgressDrawable(new ColorDrawable(
                    Color.BLUE));
            progressDialog.setCancelable(true);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.show();
        }

    }
    private class getKmsSummaryData extends AsyncTask<String, String, String> {
        ProgressDialog progressDialog;

        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub

            String result = null;
            try {
                HttpConfig ht = new HttpConfig();
                Log.d("selectspinner1",""+interval);
                result = ht.httpGet(Const.API_URL + "/mobile/getFuelRawData?vehicleId=" + Constant.SELECTED_VEHICLE_ID  + "&fromTimeUtc=" + from + "&toTimeUtc=" + to + "&interval=" + interval + "&userId=" + Constant.SELECTED_USER_ID+"&sensor="+post);

            } catch (Exception e) {

            }

            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            progressDialog.dismiss();
            mFuelFillData = new ArrayList<>();

            if (result != null && result.length() > 0) {
                String mPrevAdds = null;
                String error = null;
                try {
                    JSONArray jsonArray = new JSONArray(result.trim());
                    //  mFuelFillData.clear();
                    for (int i=0; i<jsonArray.length();i++){
                        JSONObject jsonFuelObject = jsonArray.getJSONObject(i);
                        FuelFillDto f = new FuelFillDto();
                        if (jsonFuelObject.has("dt")) {
                            f.setStartTime(Long.valueOf(jsonFuelObject.getString("dt")));
                        }
                        if (jsonFuelObject.has("fuelLitr")) {
                            f.setFuelLtr(jsonFuelObject.getString("fuelLitr"));
                        }


                        if (jsonFuelObject.has("sp")) {
                            f.setNewSpeed(jsonFuelObject.getString("sp"));
                        }

                        if (jsonFuelObject.has("ignitionStatus")) {
                            f.setIgnitionStatus(jsonFuelObject.getString("ignitionStatus"));
                        }

                        if (jsonFuelObject.has("odoMeterReading")) {
                            f.setOdoMeterReading(jsonFuelObject.getDouble("odoMeterReading"));
                        }

                        if (jsonFuelObject.has("address")) {
                            if (jsonFuelObject.getString("address").equals("P")) {
                                f.setAddress(mPrevAdds);
                            } else {
                                mPrevAdds = jsonFuelObject.getString("address");
                                f.setAddress(jsonFuelObject.getString("address"));
                            }
                        }
                        if (jsonFuelObject.has("error")){
                            if(mIsLineChartEnabled){
                                mChartLayout.setVisibility(View.VISIBLE);
                                mTxtstart.setVisibility(View.VISIBLE);
                                mTxtend.setVisibility(View.VISIBLE);
                                mTxtstart.setText("Start date : " + mStartDate);
                                mTxtend.setText("End date : " + mEndDate);
                            }
                            Log.d("errorfound",""+jsonFuelObject.getString("error"));
                            if(jsonFuelObject.getString("error").equalsIgnoreCase("No Data Found.")){
                                mTxtNoRecord.setVisibility(View.VISIBLE);
                                lv.setVisibility(View.GONE);
                                mChart.clear();
                                mChart.setData(null);
                                mChart.notifyDataSetChanged();
                                Toast.makeText(getContext(), "No record found.Please select different date time or interval", Toast.LENGTH_SHORT).show();
                                progressDialog.dismiss();
                            }
                            if (jsonFuelObject.getString("error").equalsIgnoreCase("This report is not privileged for selected vehicle or group.")){
                                list_layout.setVisibility(View.GONE);
                                lv.setVisibility(View.GONE);
                                mChart.clear();
                                mChart.setData(null);
                                mChart.notifyDataSetChanged();
                                mTxtNoRecord.setVisibility(View.VISIBLE);
                                Toast.makeText(getContext(), "This report is not privileged for selected vehicle or group.", Toast.LENGTH_SHORT).show();
                                progressDialog.dismiss();
                            }
                            if (jsonFuelObject.getString("error").equalsIgnoreCase("Please select date range within 3 days.")) {
                                lv.setVisibility(View.GONE);
                                list_layout.setVisibility(View.GONE);
                                mChart.clear();
                                mChart.setData(null);
                                mChart.notifyDataSetChanged();
                                mTxtNoRecord.setVisibility(View.VISIBLE);
                                Toast.makeText(getContext(), "No record found.Please select different date time or interval", Toast.LENGTH_SHORT).show();
                                progressDialog.dismiss();
                            }
                        }
                        mFuelFillData.add(f);
                    }
                    if(mFuelFillData.size()>1){
                        setTableLayoutData();
                        progressDialog.dismiss();
                    }
                    else {
                        lv.setVisibility(View.GONE);
                        list_layout.setVisibility(View.GONE);
                        mChart.clear();
                        mChart.setData(null);
                        mChart.notifyDataSetChanged();
                        mTxtNoRecord.setVisibility(View.VISIBLE);
                        progressDialog.dismiss();
                    }

                }catch (JSONException e) {
                    e.printStackTrace();
                }
                Log.d("errormsg",""+error);
            } else {
                Toast.makeText(getContext(), "No record found.Please select different date time or interval", Toast.LENGTH_SHORT).show();
                lv.setVisibility(View.GONE);
                list_layout.setVisibility(View.GONE);
                mChart.clear();
                mChart.setData(null);
                mChart.notifyDataSetChanged();
                mTxtNoRecord.setVisibility(View.VISIBLE);
                progressDialog.dismiss();
            }

        }

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            progressDialog = new ProgressDialog(getContext(),
                    AlertDialog.THEME_HOLO_LIGHT);
            progressDialog.setMessage("Please Wait...");
            progressDialog.setProgressDrawable(new ColorDrawable(
                    Color.BLUE));
            progressDialog.setCancelable(true);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.show();
        }

    }

    public void  setTableLayoutData() {
        try {
            Log.d("mFuelFillDatasize",""+mFuelFillData.size());

            if (mFuelFillData !=null&&mFuelFillData.size() > 1) {
                Log.d("mFuelFillDatasize","1 "+mFuelFillData.size());
                mTxtNoRecord.setVisibility(View.GONE);
                list_layout.setVisibility(View.VISIBLE);
                lv.setHasFixedSize(true);
                lv.setLayoutManager(new LinearLayoutManager(getContext()));
                adapter = new SensorFuelAdapter(getContext(),mFuelFillData,this);
                lv.setAdapter(adapter);
                adapter.notifyDataSetChanged();
                if (mIsLineChartEnabled) {
                    mTxtstart.setVisibility(View.VISIBLE);
                    mTxtend.setVisibility(View.VISIBLE);
                    mTxtstart.setText("Start date : " + mStartDate);
                    mTxtend.setText("End date : " + mEndDate);
                    mChartLayout.setVisibility(View.VISIBLE);
                    lv.setVisibility(View.GONE);
                    list_layout.setVisibility(View.GONE);
                    formLineChartNew();
                }
            }
            else {
                Log.d("mFuelFillDatasize","0 "+mFuelFillData.size());
                Toast.makeText(getContext(), "No record found.Please select different date time or interval", Toast.LENGTH_SHORT).show();
                if (mIsLineChartEnabled) {
                    Log.d("mFuelFillDatasize","2 "+mFuelFillData.size());
                    mTxtstart.setVisibility(View.VISIBLE);
                    mTxtend.setVisibility(View.VISIBLE);
                    mTxtstart.setText("Start date : " + mStartDate);
                    mTxtend.setText("End date : " + mEndDate);
                    mChartLayout.setVisibility(View.GONE);
                    mChart.setVisibility(View.GONE);
                    list_layout.setVisibility(View.GONE);
                    lv.setVisibility(View.GONE);
                    mChart.clear();
                    mChart.setData(null);
                    mChart.notifyDataSetChanged();
                } else {
                    Log.d("mFuelFillDatasize","3 "+mFuelFillData.size());
                    mChartLayout.setVisibility(View.GONE);
                    lv.setVisibility(View.GONE);
                    list_layout.setVisibility(View.GONE);
                    lv.setAdapter(null);
                }
                mTxtNoRecord.setVisibility(View.VISIBLE);
            }

        }catch(Exception e){
            Log.d("try","error "+e.getMessage());
        }


    }

    private void formLineChartNew() {

//        mTxtTotalFuelConsume.setText(mTotalFuelConsume + " ltrs");
//        mTxtTripDistance.setText(mTripDistance + " kms");
        try {
            for (int i=0 ; i<mFuelFillData.size(); i++){
                mOdoEnd = mFuelFillData.get(i).getOdoMeterReading();
                mOdoStart = mFuelFillData.get(0).getOdoMeterReading();
            }

            mTxtOdoStart.setText("Odo Start : " + mOdoStart);
            mTxtOdoEnd.setText("Odo End : " + mOdoEnd);

            mChart.setOnChartValueSelectedListener(this);

            // no description text
            mChart.getDescription().setEnabled(false);

            // enable touch gestures
            mChart.setTouchEnabled(true);

            mChart.setDragDecelerationFrictionCoef(0.9f);


            mChart.setDoubleTapToZoomEnabled(false);
            mChart.setDragEnabled(true);
            mChart.setScaleXEnabled(true);
            mChart.setScaleYEnabled(false);
            mChart.setPinchZoom(false);

            mChart.setDrawGridBackground(false);
            mChart.setHighlightPerDragEnabled(true);

            // if disabled, scaling can be done on x- and y-axis separately
            mChart.setPinchZoom(true);


            Log.d("formlinechart",""+mFuelFillData.size());
            setData();


            mChart.animateX(500);

            // get the legend (only possible after setting data)
            Legend l = mChart.getLegend();

            // modify the legend ...
            l.setForm(Legend.LegendForm.SQUARE);
//        l.setTypeface(mTfLight);
            l.setTextSize(11f);
            l.setTextColor(Color.BLACK);
            l.setVerticalAlignment(Legend.LegendVerticalAlignment.BOTTOM);
            l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.LEFT);
            l.setOrientation(Legend.LegendOrientation.HORIZONTAL);
            l.setDrawInside(false);



            MyMarkerFuelView mv = new MyMarkerFuelView(getContext(), R.layout.custom_routeplayback_marker_fuel_view, getXAxisValues(), "Fuel level", mFuelFillData);
            mv.setChartView(mChart); // For bounds control
            mChart.setMarker(mv); // Set the marker to the chart

            XAxis xAxis = mChart.getXAxis();
            xAxis.enableGridDashedLine(10f, 10f, 0f);
            xAxis.setGranularity(1f);
            xAxis.setEnabled(false);
            xAxis.setDrawGridLines(false);
            xAxis.setDrawAxisLine(false);

            YAxis leftAxis = mChart.getAxisLeft();
            leftAxis.setTypeface(TypefaceUtil.getTextBold(getContext()));
            leftAxis.setTextColor(Color.rgb(247, 163, 92));
            leftAxis.setDrawGridLines(true);
            leftAxis.setGranularityEnabled(true);


            YAxis rightAxis = mChart.getAxisRight();
            rightAxis.setTypeface(TypefaceUtil.getTextBold(getContext()));
            rightAxis.setTextColor(Color.rgb(135, 206, 250));
            rightAxis.setDrawGridLines(false);
            rightAxis.setDrawZeroLine(false);
            rightAxis.setGranularityEnabled(false);
        }catch(Exception e){
            Log.d("try","error "+e.getMessage());
        }


    }
    private ArrayList<Integer> getXAxisValues() {
        try {
            xAxis = new ArrayList<>();

            if (mFuelFillData.size() == 0){

            }else{


                for (int i = 0; i < mFuelFillData.size(); i++) {
//            xAxis.add(Const.getAcReportTime(mFuelFillData.get(i).getStartTime()));
                    xAxis.add(i);
                }
            }

        }catch(Exception e){
            Log.d("try","error "+e.getMessage());
        }

        return xAxis;
    }


    private void setData() {
        try {
            String fuelvalue = null,speedvalue = null;
            if (mFuelFillData.size() == 0){
                Log.d("mFuelFillsize","success");
            }
            else if (mFuelFillData.size() > 0) {
                ArrayList<Entry> valuesRight = new ArrayList<Entry>();
                for (int i = 0; i < mFuelFillData.size(); i++) {
                    Log.d("newspeed", "" + mFuelFillData.get(i).getNewSpeed());
                    fuelvalue = mFuelFillData.get(i).getFuelLtr();
                    Log.d("fuelvalue", "" +fuelvalue);
                    if (mFuelFillData.get(i).getFuelLtr() == null){
                        Log.d("fuelllll","failure");
                    } else{
                        Entry v1e1 = new Entry(i, Float.valueOf(mFuelFillData.get(i).getFuelLtr()));
                        valuesRight.add(v1e1);
                    }


                }

                ArrayList<Entry> valuesLeft = new ArrayList<Entry>();
                for (int i = 0; i < mFuelFillData.size(); i++) {
                    speedvalue =  mFuelFillData.get(i).getNewSpeed();
                    Log.d("speedvalue", "" +speedvalue);
                    if (mFuelFillData.get(i).getNewSpeed() == null){
                        Log.d("newsppee","failure");
                    }else {
                        Entry v1e1 = new Entry(i, Float.valueOf(mFuelFillData.get(i).getNewSpeed()));
                        valuesLeft.add(v1e1);
                    }

                }

                Log.d("speedvalue1", "" +speedvalue);
                Log.d("fuelvalue1", "" +fuelvalue);
                if (speedvalue == null && fuelvalue == null){
                    Log.d("linechart","failure");
                } else {
                    Log.d("linechart","success");
                    LineDataSet set1, set2;

                    if (mChart.getData() != null &&
                            mChart.getData().getDataSetCount() > 0) {
                        set1 = (LineDataSet) mChart.getData().getDataSetByIndex(0);
                        set2 = (LineDataSet) mChart.getData().getDataSetByIndex(1);
                        set1.setValues(valuesRight);
                        set2.setValues(valuesLeft);
                        mChart.getData().notifyDataChanged();
                        mChart.notifyDataSetChanged();
                    } else {
                        // create a dataset and give it a type
                        //dataset for right
                        set1 = new LineDataSet(valuesRight, "Fuel");
                        set1.setAxisDependency(YAxis.AxisDependency.RIGHT);
                        set1.setColor(Color.rgb(135, 206, 250));
                        set1.setLineWidth(1.5f);
                        set1.setDrawCircleHole(false);
                        set1.setCircleColor(Color.rgb(135, 206, 250));
                        set1.setCircleRadius(3f);
                        set1.setDrawCircles(true);
                        set1.setValueTextSize(9f);
                        set1.setDrawFilled(true);
                        set1.setFormLineWidth(1f);
                        set1.setFormSize(15.f);
                        set1.setDrawValues(true);

                        if (Utils.getSDKInt() >= 18) {
                            // fill drawable only supported on api level 18 and above
                            Drawable drawable = ContextCompat.getDrawable(getContext(), R.drawable.fade_red);
                            set1.setFillDrawable(drawable);
                        } else {
                            set1.setFillColor(Color.BLACK);
                        }


                        //dataset for left
                        set2 = new LineDataSet(valuesLeft, "Speed");
                        set2.setAxisDependency(YAxis.AxisDependency.LEFT);
                        set2.setColor(Color.rgb(247, 163, 92));
                        set2.setLineWidth(1.5f);

                        set2.setDrawCircleHole(false);
                        set2.setCircleColor(Color.rgb(247, 163, 92));
                        set2.setCircleRadius(3f);
                        set2.setDrawCircles(true);
                        set2.setValueTextSize(9f);
                        set2.setDrawFilled(false);

                        set2.setFormLineWidth(1f);
                        set2.setFormSize(15.f);
                        set2.setDrawValues(true);

                        ArrayList<ILineDataSet> dataSets = new ArrayList<ILineDataSet>();
                        dataSets.add(set1); // add the datasets


                        LineData data = new LineData(set1, set2);
                        mChart.setData(data);
                    }
                }

            }
        }catch(Exception e){
            Log.d("try","error "+e.getMessage());
        }
        //ArrayList<Entry> valuesRight = new ArrayList<Entry>();

    }

    String mSELECTED_MAP_TYPE = "Normal";

    private void opptionPopUp() {
        try {
            final Dialog dialog = new Dialog(getContext());
            dialog.getWindow();
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.radio_popup);
            dialog.show();

            RadioGroup rg_home = (RadioGroup) dialog.findViewById(R.id.rg_home_views);
            RadioGroup rg_history = (RadioGroup) dialog.findViewById(R.id.rg_history_views);
            rg_history.clearCheck();
            rg_history.setVisibility(View.GONE);
            rg_home.setVisibility(View.VISIBLE);

            RadioButton $Normal = (RadioButton) dialog
                    .findViewById(R.id.rb_home_normal);
            RadioButton $Satelite = (RadioButton) dialog
                    .findViewById(R.id.rb_home_satellite);
            RadioButton $Terrain = (RadioButton) dialog
                    .findViewById(R.id.rb_home_terrain);
            RadioButton $Hybrid = (RadioButton) dialog
                    .findViewById(R.id.rb_home_hybrid);


            if (mSELECTED_MAP_TYPE.equalsIgnoreCase("Normal")) {
                $Normal.setChecked(true);
                $Satelite.setChecked(false);
                $Terrain.setChecked(false);
                $Hybrid.setChecked(false);
            } else if (mSELECTED_MAP_TYPE.equalsIgnoreCase("Satelite")) {
                $Normal.setChecked(false);
                $Satelite.setChecked(true);
                $Terrain.setChecked(false);
                $Hybrid.setChecked(false);
            } else if (mSELECTED_MAP_TYPE.equalsIgnoreCase("Terrain")) {
                $Normal.setChecked(false);
                $Satelite.setChecked(false);
                $Terrain.setChecked(true);
                $Hybrid.setChecked(false);
            } else if (mSELECTED_MAP_TYPE.equalsIgnoreCase("Hybrid")) {
                $Normal.setChecked(false);
                $Satelite.setChecked(false);
                $Terrain.setChecked(false);
                $Hybrid.setChecked(true);
            }
            $Normal.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub

//                mapview.setTileSource(TileSourceFactory.MAPNIK);

                    map.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                    mSELECTED_MAP_TYPE = "Normal";
                    dialog.dismiss();
                }
            });
            $Satelite.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    mSELECTED_MAP_TYPE = "Satelite";
                    map.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
//                mapview.setTileSource(TileSourceFactory.USGS_SAT);
                    dialog.dismiss();
                }
            });
            $Terrain.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    mSELECTED_MAP_TYPE = "Terrain";
                    map.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
//                mapview.setTileSource(TileSourceFactory.HIKEBIKEMAP);
                    dialog.dismiss();
                }
            });

            $Hybrid.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    mSELECTED_MAP_TYPE = "Hybrid";
                    map.setMapType(GoogleMap.MAP_TYPE_HYBRID);
//                mapview.setTileSource(TileSourceFactory.USGS_TOPO);
                    dialog.dismiss();
                }
            });

            DisplayMetrics displayMetrics = new DisplayMetrics();
            getActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
            height = displayMetrics.heightPixels;
            width = displayMetrics.widthPixels;

            LinearLayout.LayoutParams radioParama = new LinearLayout.LayoutParams(
                    FrameLayout.LayoutParams.WRAP_CONTENT,
                    FrameLayout.LayoutParams.WRAP_CONTENT);
            radioParama.width = width * 50 / 100;
            radioParama.height = width * 10 / 100;
            radioParama.topMargin = height * 4 / 100;
            radioParama.gravity = Gravity.CENTER;
            radioParama.leftMargin = height * 4 / 100;
            $Normal.setLayoutParams(radioParama);
            $Satelite.setLayoutParams(radioParama);
            $Terrain.setLayoutParams(radioParama);

            LinearLayout.LayoutParams radioterrainParama = new LinearLayout.LayoutParams(
                    FrameLayout.LayoutParams.WRAP_CONTENT,
                    FrameLayout.LayoutParams.WRAP_CONTENT);
            radioterrainParama.width = width * 50 / 100;
            radioterrainParama.height = width * 10 / 100;
            radioterrainParama.topMargin = height * 4 / 100;
            radioterrainParama.gravity = Gravity.CENTER;
            radioterrainParama.leftMargin = height * 4 / 100;
            radioterrainParama.bottomMargin = height * 4 / 100;
            $Hybrid.setLayoutParams(radioterrainParama);

            if (width >= 600) {
                $Normal.setTextSize(16);
                $Satelite.setTextSize(16);
                $Terrain.setTextSize(16);
                $Hybrid.setTextSize(16);
            } else if (width > 501 && width < 600) {
                $Normal.setTextSize(15);
                $Satelite.setTextSize(15);
                $Terrain.setTextSize(15);
                $Hybrid.setTextSize(15);
            } else if (width > 260 && width < 500) {
                $Normal.setTextSize(14);
                $Satelite.setTextSize(14);
                $Terrain.setTextSize(14);
                $Hybrid.setTextSize(14);
            } else if (width <= 260) {
                $Normal.setTextSize(13);
                $Satelite.setTextSize(13);
                $Terrain.setTextSize(13);
                $Hybrid.setTextSize(13);
            }
        }catch(Exception e){
            Log.d("try","error "+e.getMessage());
        }
        // TODO Auto-generated method stub

    }
}
