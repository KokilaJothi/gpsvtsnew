package com.vamosys.vamos;//package com.vamosys.vamos;
//
//import android.annotation.SuppressLint;
//import android.app.Activity;
//import android.app.AlarmManager;
//import android.app.AlertDialog;
//import android.app.DatePickerDialog;
//import android.app.Dialog;
//import android.app.PendingIntent;
//import android.app.ProgressDialog;
//import android.app.SearchManager;
//import android.app.TimePickerDialog;
//import android.content.Context;
//import android.content.DialogInterface;
//import android.content.Intent;
//import android.content.SharedPreferences;
//import android.database.Cursor;
//import android.graphics.Bitmap;
//import android.graphics.BitmapFactory;
//import android.graphics.Canvas;
//import android.graphics.Matrix;
//import android.graphics.drawable.BitmapDrawable;
//import android.graphics.drawable.Drawable;
//import android.location.Address;
//import android.location.Geocoder;
//import android.net.wifi.WifiManager;
//import android.os.AsyncTask;
//import android.os.Bundle;
//import android.os.CountDownTimer;
//import android.os.Handler;
//import android.preference.PreferenceManager;
//import android.support.v4.app.FragmentActivity;
//import android.support.v4.graphics.drawable.DrawableCompat;
//import android.support.v4.widget.DrawerLayout;
//import android.support.v4.widget.DrawerLayout.DrawerListener;
//import android.telephony.SmsManager;
//import android.util.DisplayMetrics;
//import android.util.Log;
//import android.view.KeyEvent;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.View.OnClickListener;
//import android.view.ViewGroup;
//import android.view.Window;
//import android.view.WindowManager;
//import android.view.animation.Animation;
//import android.view.animation.AnimationUtils;
//import android.view.inputmethod.EditorInfo;
//import android.view.inputmethod.InputMethodManager;
//import android.widget.AdapterView;
//import android.widget.BaseAdapter;
//import android.widget.BaseExpandableListAdapter;
//import android.widget.Button;
//import android.widget.CheckBox;
//import android.widget.CheckedTextView;
//import android.widget.CompoundButton;
//import android.widget.DatePicker;
//import android.widget.EditText;
//import android.widget.ExpandableListView;
//import android.widget.ImageView;
//import android.widget.LinearLayout;
//import android.widget.ListView;
//import android.widget.RadioButton;
//import android.widget.RadioGroup;
//import android.widget.RelativeLayout;
//import android.widget.ScrollView;
//import android.widget.SearchView;
//import android.widget.SeekBar;
//import android.widget.TextView;
//import android.widget.TimePicker;
//import android.widget.Toast;
//
//import com.google.android.gms.maps.CameraUpdate;
//import com.google.android.gms.maps.CameraUpdateFactory;
//import com.google.android.gms.maps.GoogleMap;
//import com.google.android.gms.maps.MapFragment;
//import com.google.android.gms.maps.model.BitmapDescriptorFactory;
//import com.google.android.gms.maps.model.CameraPosition;
//import com.google.android.gms.maps.model.LatLng;
//import com.google.android.gms.maps.model.LatLngBounds;
//import com.google.android.gms.maps.model.Marker;
//import com.google.android.gms.maps.model.MarkerOptions;
//import com.google.android.gms.maps.model.PolylineOptions;
//import com.smart.interfaces.QuickActionListener;
//import com.smart.plugins.quickaction.ActionItem;
//import com.smart.smartutils.ConfigManager;
//import com.smart.smartutils.UtilManager;
//import com.vamosys.adapter.ReportListAdapter;
//import com.vamosys.model.DataBaseHandler;
//import com.vamosys.model.ReportData;
//import com.vamosys.utils.CommonManager;
//import com.vamosys.utils.ConnectionDetector;
//import com.vamosys.utils.MyCustomProgressDialog;
//import com.vamosys.utils.TypefaceUtil;
//
//import org.codehaus.jackson.JsonFactory;
//import org.codehaus.jackson.JsonNode;
//import org.codehaus.jackson.JsonParser;
//import org.codehaus.jackson.JsonToken;
//import org.codehaus.jackson.map.MappingJsonFactory;
//import org.json.JSONArray;
//import org.json.JSONException;
//import org.json.JSONObject;
//
//import java.io.BufferedReader;
//import java.io.IOException;
//import java.io.InputStream;
//import java.io.InputStreamReader;
//import java.net.HttpURLConnection;
//import java.net.URL;
//import java.util.ArrayList;
//import java.util.Arrays;
//import java.util.Calendar;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Locale;
//import java.util.TimeZone;
//
//@SuppressLint("NewApi")
//public class NavigationActivity extends FragmentActivity implements GoogleMap.OnInfoWindowClickListener, SearchView.OnQueryTextListener, SearchView.OnCloseListener {
//    static final int DATE_PICKER_ID = 1111;
//    static final int TIME_DIALOG_ID = 999;
//    private static final int DIALOG1_KEY = 0;
//    private static final int OTP_DIALOG = 1;
//    final ArrayList<Marker> current_users_location_point = new ArrayList<Marker>();
//    final ArrayList<Marker> current_vehicle_location_marker = new ArrayList<Marker>();
//    final ArrayList<Marker> zoom_location_marker = new ArrayList<Marker>();
//    private final String TAG = "SMART";
//    private final long interval = 1000;
//    /**
//     * ImageView declarations
//     */
//    ImageView id_applogo, publicmenu, home;
//    Boolean setUpdrawerfunctioncalled;
//    /**
//     * Arraylist declarations
//     */
//    int position;
//    ArrayList<String> groupList = new ArrayList<String>();
//    ArrayList<String> vamosgroupList = new ArrayList<String>();
//    ArrayList<String> child;
//    /**Arraylist declarations*/
//    /**
//     * Track Vehicle declarations
//     */
//    LinearLayout current_vehicle_info_layout, id_speed_min_layout, id_speed_id_layout, id_speed_value_layout, id_odo_layout, id_odo_value_layout, id_vehicle_id_layout;
//    TextView id_curr_vehicle_id_value, id_curr_speed_label, id_curr_speed_value, id_curr_odo_label, id_curr_odo_value, id_curr_todaydistance_label, id_curr_todaydistance_value, id_curr_parked_label, id_curr_parked_value;
//    Button id_curr_vehicle_info_close, id_curr_vehicle_info_min;
//    TextView id_curr_vehicle_address_label, id_curr_vehicle_address_value, id_refresh_countdowntimer;
//    ImageView id_curr_vehicle_info_icon, id_refresh_tracking_icon;
//    Dialog marker_info_dialog;
//    /**Track Vehicle  declarations*/
//    /**
//     * POI declarations
//     */
//    LinearLayout id_poi_layout;
//    TextView id_poi_label;
//    Button save_poi, id_add_poi_btn, id_poi_layout_close;
//    EditText store_poi;
//    ImageView id_next_vehicle, id_previous_vehicle;
//    /**
//     * POI declarations
//     */
//    double lat, lng;
//    float vehicle_zoom_level = 15.5F;
//    float group_zoom_level = 13.5F;
//    float history_zoom_level = 11.5F;
//    /**
//     * Setting page  declarations
//     */
//    LinearLayout id_settings_layout;
//    ImageView id_done_refresh_rate, id_cancel_refresh_rate;
//    TextView id_setting_label, id_refresh_rate_label, id_refresh_rate_value, id_history_label, id_history_value, id_settings_support, id_settings_support_address_line1, id_settings_support_address_line2, id_settings_support_mobilenumber;
//    /**
//     * Google Map declarations
//     */
//    EditText id_refresh_rate_edit_value;
//    /**
//     * Hardware access  declarations
//     */
//    String appID, macid;
//    /**
//     * Network Access declarations
//     */
//    ConnectionDetector cd;
//    boolean isInternetPresent = false;
//    /**Setting page  declarations*/
//    /**
//     * History Page  declarations
//     */
//    int current_node_index;
//    /**
//     * Hardware access   declarations
//     */
//    String totalrows = null;
//    String historyRepeat;
//    /**
//     * Network Access  declarations
//     */
//    int progressChanged = 0;
//    boolean from_date, to_date, from_time, to_time, seekbarchanged = false, backButtonPressed = false;
//    SeekBar speed_seekbar, history_seekbar;
//    Runnable runnable1;
//    int loopvalue = 0, a, paused_value, map_marker_vechile_position;
//    Handler handler1;
//    ArrayList<LatLng> points;
//    ArrayList<String> speedlist;
//    ArrayList<String> vehicle = new ArrayList<String>();
//    ArrayList<String> datetimelist;
//    ArrayList<String> distancelist;
//    Dialog current_address__dialog;
//    LinearLayout history_detail_layout, id_date_time_picker_layout, id_history_show_min_layout, is_history_vehicle_layout, history_vehicle_info_layout;
//    ImageView id_history_vehicle_info_icon, id_show_vehicle_info_icon, id_done_history, id_cancel_history;
//    Button history_min_icon, history_close_icon, id_history_selection_done, id_history_selection_cancel, replay_icon, play_icon, pause_icon;
//    TextView id_show_history_vehicleid, history_start_location_label, history_end_location_label, history_distance_covered_label, history_timetaken_label;
//    TextView history_timetaken_value, id_history_type_value, id_histroy_vehicleid, history_distance_covered_value, history_start_location_value, history_end_location_value;
//    EditText id_history_edit_value;
//    Boolean pause = false, delay = true, track = false;
//    RelativeLayout id_from_datetime_layout, id_to_datetime_layout, id_history_type_layout;
//    TextView id_from_label, id_from_date, id_from_time, id_to_date, id_to_time, id_history_speed_info_label, id_history_speed_info_value, id_history_distance_label, id_history_distance_value, id_history_direction_label, id_history_direction_value;
//    TextView histroy_start_time_value, histroy_start_time_label, history_endtime_label, history_endtime_value;
//    /**
//     * Database and SharedPreference declarations
//     */
//    Context context = this;
//    DBHelper dbhelper;
//    SharedPreferences sp;
//    String snippet, selected_vehicleid, mShortName;
//    Handler handler = new Handler();
//    Const cons;
//    /**
//     * progress dialog
//     */
//    ProgressDialog progressDialog;
//    /**History Page   declarations*/
//    /**
//     * Navigation declarations
//     */
//    String navigation_status = "Vehicles";
//    NavigationBaseAdapter navigationBaseAdapter;
//    ListView navigationlist;
//    ArrayList<String> navigationList = new ArrayList<String>();
//    LinearLayout id_navigation_layout;
//    PendingIntent vehicletrackpendingIntent, cancel_pendingIntent;
//    /**
//     * Database and SharedPreference declarations
//     */
//    AlarmManager vehicletrackalarm;
//    TextView id_navigation_header_label;
//    /**
//     * HomePage  declarations
//     */
//    LinearLayout vehicle_info_layout, id_total_vehicles_layout, header_layout, id_offline_vehicles_layout, id_online_vehicles_layout, id_map_info_layout;
//    TextView id_all_vehicles_label, id_online_vehicles_label, id_offline_vehicles_label, id_all_vehicles_value, id_online_vehicles_value, id_offline_vehicles_value;
//    SearchView search;
//    RadioGroup rgViews;
//    String username = "demouser1";
//    MyExpandableAdapter adapter;
//    VamosExpandableAdapter vamosadapter;
//    ExpandableListView expListView, publicexplist;
//    /**
//     * Timeout variables
//     */
//    int report_timeout = 20000;
//    int lock_unlock_timeout = 5000;
//    /**
//     * Navigation  declarations
//     */
//    int history_timeout = 15000;
//    int pull_vehicle_timeout = 20000;
//    int poi_timeout = 1000;
//    /**
//     * ImageView declarations
//     */
//    private DrawerLayout mDrawerLayout;
//    private ArrayList<Object> vamoschildelements = new ArrayList<Object>();
//    private ArrayList<Object> childelements = new ArrayList<Object>();
//    private ArrayList<Object> originalchildelements = new ArrayList<Object>();
//    /**
//     * HomePage  declarations
//     */
//    private ArrayList<Object> jsonchildelements = new ArrayList<Object>();
//    /**
//     * Google Map declarations
//     */
//    private GoogleMap map;
//    OnClickListener homeOnclickListener = new OnClickListener() {
//        @Override
//        public void onClick(View v) {
//            if (mDrawerLayout.isDrawerOpen(id_navigation_layout)) {
//                mDrawerLayout.closeDrawer(id_navigation_layout);
//            } else {
//                mDrawerLayout.closeDrawer(publicexplist);
//                mDrawerLayout.openDrawer(id_navigation_layout);
//            }
//            if (navigation_status.equals("Home")) {
//                search.setVisibility(View.GONE);
//                expListView.setVisibility(View.GONE);
//                id_refresh_tracking_icon.setVisibility(View.VISIBLE);
//            } else if (navigation_status.equals("Vehicles")) {
//                if (history_detail_layout.getVisibility() == View.VISIBLE) {
//                    pause = !pause;
//                    if (map != null) {
//                        map.clear();
//                    }
//                    backButtonPressed = true;
//                    history_detail_layout.setVisibility(View.GONE);
//                    speed_seekbar.setVisibility(View.GONE);
//                    history_seekbar.setVisibility(View.GONE);
//                    pause_icon.setVisibility(View.GONE);
//                    play_icon.setVisibility(View.GONE);
//                    history_vehicle_info_layout.setVisibility(View.GONE);
//                    id_refresh_tracking_icon.setVisibility(View.VISIBLE);
//                }
//                search.setVisibility(View.GONE);
//                expListView.setVisibility(View.VISIBLE);
//                if (search.getVisibility() == View.GONE) {
//                    expListView.setVisibility(View.GONE);
//                }
//            } else if (navigation_status.equals("tracking")) {
//                search.setVisibility(View.GONE);
//                expListView.setVisibility(View.GONE);
//            }
//        }
//    };
//    private HashMap<Marker, JSONObject> mMarkersHashMap;
//    private HashMap<Marker, String> parker_mMarkersHashMap;
//    private int year, month, day, hour, minute;
//    private RadioGroup radioGroup;
//    private long startTime;
//    private long refresh_rate;
//    private long timeElapsed;
//    private boolean timerHasStarted = false;
//    private RefreshCountDownTimer countDownTimer;
//    /**
//     * Runnable to update the map with the seleced Vehicle and track the vehicle
//     */
//    Runnable runable = new Runnable() {
//        @Override
//        public void run() {
//            Cursor getcurrent_vehicle_info = null;
//            try {
//                vehicle_info_layout.setVisibility(View.GONE);
//                id_refresh_tracking_icon.setVisibility(View.VISIBLE);
//                id_refresh_countdowntimer.setVisibility(View.VISIBLE);
//                if (!timerHasStarted) {
//                    countDownTimer.start();
//                    timerHasStarted = true;
//                }
//                ArrayList<LatLng> points = null;
//                PolylineOptions lineOptions = null;
//                MarkerOptions markerOptions = new MarkerOptions();
//                getcurrent_vehicle_info = dbhelper.get_current_vehicle_details();
//                if (getcurrent_vehicle_info.moveToFirst()) {
//                    PolylineOptions options = new PolylineOptions().width(8).color(getResources().getColor(R.color.history_polyline_color)).geodesic(true);
//                    for (int z = 0; z < getcurrent_vehicle_info.getCount(); z++) {
//                        ParsingClass parse = new ParsingClass();
//                        parse.getCurrentVehicleInfo(getcurrent_vehicle_info.getString(0));
//                        double lat = parse.getCurr_lat();
//                        double lng = parse.getCurr_long();
//                        LatLng position = new LatLng(lat, lng);
//                        options.add(position);
//                        if (z == getcurrent_vehicle_info.getCount() - 1) {
//                            new GetAddressTask(id_curr_vehicle_address_value).execute(lat, lng, 4.0);
//                            current_vehicle_info_layout.setVisibility(View.VISIBLE);
//                            id_curr_vehicle_id_value.setText(parse.getVehicleId());
//                            id_curr_speed_value.setText(parse.getSpeed() + " KMS / HR");
//                            id_curr_odo_value.setText(parse.getodoDistance() + " KMS");
//                            id_curr_todaydistance_value.setText(parse.getDistanCovered() + " KMS");
//                            if (parse.getvehicleType().equals("Bus")) {
//                                id_curr_vehicle_info_icon.setImageResource(R.drawable.bus_east);
//                            } else if (parse.getvehicleType().equals("Car")) {
//                                id_curr_vehicle_info_icon.setImageResource(R.drawable.car_east);
//                            } else if (parse.getvehicleType().equals("Truck")) {
//                                id_curr_vehicle_info_icon.setImageResource(R.drawable.truck_east);
//                            } else if (parse.getvehicleType().equals("Cycle")) {
//                                id_curr_vehicle_info_icon.setImageResource(R.drawable.cycle_east);
//                            }
//                            View marker = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.custommarker, null);
//                            ImageView id_custom_marker_icon = (ImageView) marker.findViewById(R.id.id_custom_marker_icon);
//                            ImageView id_vehicle_in_marker = (ImageView) marker.findViewById(R.id.id_vehicle_in_marker);
//                            if (!parse.getDirection().equals("")) {
//                                vehicle_direction_in_map(parse.getvehicleType(), parse.getDirection(), id_vehicle_in_marker);
//                            }
//                            if (parse.getOverSpeed().equalsIgnoreCase("Y")) {
//                                id_custom_marker_icon.setImageResource(R.drawable.red_custom_marker_icon);
//                            } else if (parse.getGeoFence().equalsIgnoreCase("Y")) {
//                                id_custom_marker_icon.setImageResource(R.drawable.red_custom_marker_icon);
//                            } else if (parse.getPosition().equalsIgnoreCase("M")) {
//                                id_custom_marker_icon.setImageResource(R.drawable.green_custom_marker_icon);
//                            } else if (parse.getPosition().equalsIgnoreCase("P")) {
//                                id_custom_marker_icon.setImageResource(R.drawable.grey_custom_marker_icon);
//                            } else if (parse.getPosition().equalsIgnoreCase("S")) {
//                                id_custom_marker_icon.setImageResource(R.drawable.orange_custom_marker_icon);
//                            } else if (parse.getPosition().equalsIgnoreCase("N")) {
//                                id_custom_marker_icon.setImageResource(R.drawable.orange_custom_marker_icon);
//                            } else if (parse.getPosition().equalsIgnoreCase("U")) {
//                                id_custom_marker_icon.setImageResource(R.drawable.grey_custom_marker_icon);
//                            }
//                            MarkerOptions markerOption = new MarkerOptions().position(new LatLng(lat, lng));
//                            markerOption.title(parse.getshortname());
//                            markerOption.icon(BitmapDescriptorFactory.fromBitmap(createDrawableFromView(NavigationActivity.this, marker)));
//                            if (current_vehicle_location_marker.isEmpty()) {
//                                Marker currentMarker = map.addMarker(markerOption);
//                                current_vehicle_location_marker.add(currentMarker);
//                            } else {
//                                for (int m = 0; m < current_vehicle_location_marker.size(); m++) {
//                                    Marker removemarker = current_vehicle_location_marker.get(m);
//                                    removemarker.remove();
//                                }
//                                current_vehicle_location_marker.clear();
//                                Marker currentMarker = map.addMarker(markerOption);
//                                current_vehicle_location_marker.add(currentMarker);
//                            }
//                            CameraPosition INIT = new CameraPosition.Builder().target(new LatLng(lat, lng)).zoom(vehicle_zoom_level).build();
//                            map.animateCamera(CameraUpdateFactory.newCameraPosition(INIT));
//                        }
//                        getcurrent_vehicle_info.moveToNext();
//                    }
//                    Toast.makeText(getApplicationContext(), "Prabha... Please wait a moment", Toast.LENGTH_LONG).show();
//                    map.addPolyline(options);
//                    if (progressDialog.isShowing()) {
//                        progressDialog.dismiss();
//                    }
//                } else {
//                    if (sp.getBoolean("tracking_exception", false)) {
//                        if (progressDialog.isShowing()) {
//                            progressDialog.dismiss();
//                        }
//                        SharedPreferences.Editor editor = sp.edit();
//                        editor.putBoolean("tracking_exception", false);
//                        editor.commit();
//                        track = false;
//                        header_layout.setVisibility(View.VISIBLE);
//                        vehicle_info_layout.setVisibility(View.VISIBLE);
//                        cancelVehicleTracking();
//                        home_button_functionality();
//                        Toast.makeText(getApplicationContext(), "Something went wrong. Please try again", Toast.LENGTH_LONG).show();
//                        return;
//                    }
//                    Toast.makeText(getApplicationContext(), "Tracking... Please wait a moment", Toast.LENGTH_LONG).show();
//                }
//                handler.removeCallbacksAndMessages(null);
//                handler.postDelayed(this, startTime);
//            } catch (Exception e) {
//                // TODO: handle exception
//                // progressDialog.dismiss();
//                if (progressDialog.isShowing()) {
//                    progressDialog.dismiss();
//                }
//            } finally {
//                //also call the same runnable
//                if (getcurrent_vehicle_info != null) {
//                    getcurrent_vehicle_info.close();
//                }
//                handler.removeCallbacksAndMessages(null);
//                handler.postDelayed(this, startTime);
//            }
//        }
//    };
//    private int iLoopDelay;
//    private boolean isSpeedChanged = false;
//    /**
//     * DrawerListener - whic
//     */
//    private DrawerListener mDrawerListener = new DrawerListener() {
//        @Override
//        public void onDrawerSlide(View view, float slideArg) {
//        }
//
//        @Override
//        public void onDrawerOpened(View view) {
//            search.setVisibility(View.GONE);
//        }
//
//        @Override
//        public void onDrawerClosed(View view) {
//            if (navigation_status.equals("Home")) {
//                search.setVisibility(View.INVISIBLE);
//                expListView.setVisibility(View.GONE);
//                id_refresh_tracking_icon.setVisibility(View.VISIBLE);
//            } else if (navigation_status.equals("Vehicles")) {
//                search.setVisibility(View.VISIBLE);
//                expListView.setVisibility(View.VISIBLE);
//                if (history_detail_layout.getVisibility() == View.VISIBLE) {
//                }
//            } else if (navigation_status.equals("tracking")) {
//                search.setVisibility(View.GONE);
//                expListView.setVisibility(View.GONE);
//            }
//        }
//
//        @Override
//        public void onDrawerStateChanged(int status) {
//        }
//    };
//    private String strSelectedVehicle;
//    private JSONObject jSelectedVehicleObject;
//    private ScrollView svReportOption;
//    private ListView lvReportList;
//    private RelativeLayout headerLayout;
//    private ReportListAdapter mReportListAdapter;
//    /**
//     * Listener for date picker
//     */
//    private DatePickerDialog.OnDateSetListener datepickerListener = new DatePickerDialog.OnDateSetListener() {
//        // when dialog box is closed, below method will be called.
//        @Override
//        public void onDateSet(DatePicker view, int selectedYear, int selectedMonth, int selectedDay) {
//            year = selectedYear;
//            month = selectedMonth;
//            day = selectedDay;
//            if (from_date) {
//                id_from_date.setText(cons.getDateFormat(cons.getCalendarDate(year, month, day, 0, 0)));
//            } else if (to_date) {
//                id_to_date.setText(cons.getDateFormat(cons.getCalendarDate(year, month, day, 0, 0)));
//            }
//        }
//    };
//    /**
//     * Listener for Time Picker
//     */
//    private TimePickerDialog.OnTimeSetListener timePickerListener = new TimePickerDialog.OnTimeSetListener() {
//        @Override
//        public void onTimeSet(TimePicker view, int selected_hour, int selected_minute) {
//            // TODO Auto-generated method stub
//            hour = selected_hour;
//            minute = selected_minute;
//            if (from_time) {
//                id_from_time.setText(new StringBuilder().append(pad(hour)).append(":").append(pad(minute)).append(":").append("00"));
//            } else if (to_time) {
//                id_to_time.setText(new StringBuilder().append(pad(hour)).append(":").append(pad(minute)).append(":").append("00"));
//            }
//        }
//    };
//
//    /**
//     * Code to make the Marker icon adjustable for multiple screen
//     */
//    public static Bitmap createDrawableFromView(Context context, View view) {
//        DisplayMetrics displayMetrics = new DisplayMetrics();
//        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
//        view.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
//        view.measure(displayMetrics.widthPixels, displayMetrics.heightPixels);
//        view.layout(0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels);
//        view.buildDrawingCache();
//        Bitmap bitmap = Bitmap.createBitmap(view.getMeasuredWidth(), view.getMeasuredHeight(), Bitmap.Config.ARGB_8888);
//        Canvas canvas = new Canvas(bitmap);
//        view.draw(canvas);
//        return bitmap;
//    }
//
//    /**
//     * Function to add 0 for time which is lesser that 10. Ex. 08.00, 05.00
//     */
//    private static String pad(int c) {
//        if (c >= 10)
//            return String.valueOf(c);
//        else
//            return "0" + String.valueOf(c);
//    }
//
//    @SuppressLint("NewApi")
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_navigation);
//        dbSetup();
//        /** App ID and MAC ID get from the device and send it along with API*/
//        appID = getApplicationContext().getPackageName();
//        WifiManager m_wm = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
//        macid = m_wm.getConnectionInfo().getMacAddress();
//        if (appID != null && !appID.equals("")) {
//            dbhelper.update_Settings_StdTableValues("appid", appID);
//        }
//        if (macid != null && !macid.equals("")) {
//            dbhelper.update_Settings_StdTableValues("macid", macid);
//        }
//        ConfigManager.initialize(this);
//        /**Const class instance to access data from Const class*/
//        cons = new Const();
//        /**Shared Preference to access local data*/
//        sp = PreferenceManager.getDefaultSharedPreferences(context);
//        /**Connection Detector class to detect Internet Connectivity*/
//        cd = new ConnectionDetector(context);
//        /** Setting false to boolean variables and later setting this will help to find from and to date selection*/
//        from_date = false;
//        to_date = false;
//        from_time = false;
//        to_time = false;
//        /** Referencing LinearLayout with the ID in the XML layout*/
//        id_navigation_layout = (LinearLayout) findViewById(R.id.id_navigation_layout);
//        id_settings_layout = (LinearLayout) findViewById(R.id.id_settings_layout);
//        current_vehicle_info_layout = (LinearLayout) findViewById(R.id.curr_vehicle_info_layout);
//        vehicle_info_layout = (LinearLayout) findViewById(R.id.vehicle_info_layout);
//        header_layout = (LinearLayout) findViewById(R.id.header_layout);
//        history_detail_layout = (LinearLayout) findViewById(R.id.history_detail_layout);
//        id_date_time_picker_layout = (LinearLayout) findViewById(R.id.id_date_time_picker_layout);
//        id_total_vehicles_layout = (LinearLayout) findViewById(R.id.id_total_vehicles_layout);
//        id_online_vehicles_layout = (LinearLayout) findViewById(R.id.id_online_vehicles_layout);
//        id_offline_vehicles_layout = (LinearLayout) findViewById(R.id.id_offline_vehicles_layout);
//        id_poi_layout = (LinearLayout) findViewById(R.id.id_poi_layout);
//        id_speed_id_layout = (LinearLayout) findViewById(R.id.id_speed_id_layout);
//        id_odo_layout = (LinearLayout) findViewById(R.id.id_odo_layout);
//        id_odo_value_layout = (LinearLayout) findViewById(R.id.id_odo_value_layout);
//        id_speed_value_layout = (LinearLayout) findViewById(R.id.id_speed_value_layout);
//        id_vehicle_id_layout = (LinearLayout) findViewById(R.id.id_vehicle_id_layout);
//        id_speed_min_layout = (LinearLayout) findViewById(R.id.id_speed_min_layout);
//        id_history_show_min_layout = (LinearLayout) findViewById(R.id.id_history_show_min_layout);
//        is_history_vehicle_layout = (LinearLayout) findViewById(R.id.is_history_vehicle_layout);
//        history_vehicle_info_layout = (LinearLayout) findViewById(R.id.history_vehicle_info_layout);
//        /** Referencing TextView with the ID in the XML layout*/
//        id_setting_label = (TextView) findViewById(R.id.id_setting_label);
//        id_navigation_header_label = (TextView) findViewById(R.id.id_navigation_header_label);
//        id_refresh_rate_label = (TextView) findViewById(R.id.id_refresh_rate_label);
//        id_refresh_rate_value = (TextView) findViewById(R.id.id_refresh_rate_value);
//        id_history_label = (TextView) findViewById(R.id.id_history_label);
//        id_history_value = (TextView) findViewById(R.id.id_history_value);
//        id_settings_support = (TextView) findViewById(R.id.id_settings_support);
//        id_all_vehicles_label = (TextView) findViewById(R.id.id_all_vehicles_label);
//        id_online_vehicles_label = (TextView) findViewById(R.id.id_online_vehicles_label);
//        id_offline_vehicles_label = (TextView) findViewById(R.id.id_offline_vehicles_label);
//        id_poi_label = (TextView) findViewById(R.id.id_poi_label);
//        history_start_location_label = (TextView) findViewById(R.id.id_offline_vehicles_label);
//        history_end_location_label = (TextView) findViewById(R.id.history_end_location_label);
//        history_distance_covered_label = (TextView) findViewById(R.id.history_distance_covered_label);
//        history_timetaken_label = (TextView) findViewById(R.id.history_timetaken_label);
//        id_settings_support_address_line1 = (TextView) findViewById(R.id.id_settings_support_address_line1);
//        id_settings_support_address_line2 = (TextView) findViewById(R.id.id_settings_support_address_line2);
//        id_settings_support_mobilenumber = (TextView) findViewById(R.id.id_settings_support_mobilenumber);
//        id_from_label = (TextView) findViewById(R.id.id_from_label);
//        id_history_type_value = (TextView) findViewById(R.id.id_history_type_value);
//        id_all_vehicles_value = (TextView) findViewById(R.id.id_all_vehicles_value);
//        id_online_vehicles_value = (TextView) findViewById(R.id.id_online_vehicles_value);
//        id_offline_vehicles_value = (TextView) findViewById(R.id.id_offline_vehicles_value);
//        id_from_date = (TextView) findViewById(R.id.id_from_date);
//        id_from_time = (TextView) findViewById(R.id.id_from_time);
//        id_to_date = (TextView) findViewById(R.id.id_to_date);
//        id_to_time = (TextView) findViewById(R.id.id_to_time);
//        id_curr_vehicle_address_value = (TextView) findViewById(R.id.id_curr_vehicle_address_value);
//        id_refresh_countdowntimer = (TextView) findViewById(R.id.id_refresh_countdowntimer);
//        id_curr_vehicle_id_value = (TextView) findViewById(R.id.id_curr_vehicle_id_value);
//        id_curr_speed_label = (TextView) findViewById(R.id.id_curr_speed_label);
//        id_curr_speed_value = (TextView) findViewById(R.id.id_curr_speed_value);
//        id_curr_odo_label = (TextView) findViewById(R.id.id_curr_odo_label);
//        id_curr_odo_value = (TextView) findViewById(R.id.id_curr_odo_value);
//        id_curr_todaydistance_label = (TextView) findViewById(R.id.id_curr_todaydistance_label);
//        id_curr_todaydistance_value = (TextView) findViewById(R.id.id_curr_todaydistance_value);
//        id_curr_vehicle_address_label = (TextView) findViewById(R.id.id_curr_vehicle_address_label);
//        id_curr_parked_label = (TextView) findViewById(R.id.id_curr_parked_label);
//        id_curr_parked_value = (TextView) findViewById(R.id.id_curr_parked_value);
//        id_histroy_vehicleid = (TextView) findViewById(R.id.id_histroy_vehicleid);
//        history_distance_covered_value = (TextView) findViewById(R.id.history_distance_covered_value);
//        history_start_location_value = (TextView) findViewById(R.id.history_start_location_value);
//        history_end_location_value = (TextView) findViewById(R.id.history_end_location_value);
//        id_show_history_vehicleid = (TextView) findViewById(R.id.id_show_history_vehicleid);
//        history_timetaken_value = (TextView) findViewById(R.id.history_timetaken_value);
//        id_history_speed_info_label = (TextView) findViewById(R.id.id_history_speed_info_label);
//        id_history_speed_info_value = (TextView) findViewById(R.id.id_history_speed_info_value);
//        id_history_distance_label = (TextView) findViewById(R.id.id_history_distance_label);
//        id_history_distance_value = (TextView) findViewById(R.id.id_history_distance_value);
//        id_history_direction_label = (TextView) findViewById(R.id.id_history_direction_label);
//        id_history_direction_value = (TextView) findViewById(R.id.id_history_direction_value);
//        histroy_start_time_value = (TextView) findViewById(R.id.histroy_start_time_value);
//        histroy_start_time_label = (TextView) findViewById(R.id.histroy_start_time_label);
//        history_endtime_label = (TextView) findViewById(R.id.history_endtime_label);
//        history_endtime_value = (TextView) findViewById(R.id.history_endtime_value);
//        /** Refering EditText with the ID in the XML layout*/
//        store_poi = (EditText) findViewById(R.id.id_store_poi);
//        id_refresh_rate_edit_value = (EditText) findViewById(R.id.id_refresh_rate_edit_value);
//        id_history_edit_value = (EditText) findViewById(R.id.id_history_edit_value);
//        /** Refering ImageView with the ID in the XML layout*/
//        id_applogo = (ImageView) findViewById(R.id.id_applogo);
//        home = (ImageView) findViewById(R.id.home);
//        id_done_refresh_rate = (ImageView) findViewById(R.id.id_done_refresh_rate);
//        id_cancel_refresh_rate = (ImageView) findViewById(R.id.id_cancel_refresh_rate);
//        id_done_history = (ImageView) findViewById(R.id.id_done_history);
//        id_cancel_history = (ImageView) findViewById(R.id.id_cancel_history);
//        publicmenu = (ImageView) findViewById(R.id.publicmenu);
//        id_history_vehicle_info_icon = (ImageView) findViewById(R.id.id_history_vehicle_info_icon);
//        id_show_vehicle_info_icon = (ImageView) findViewById(R.id.id_show_vehicle_info_icon);
//        id_curr_vehicle_info_icon = (ImageView) findViewById(R.id.id_curr_vehicle_info_icon);
//        id_refresh_tracking_icon = (ImageView) findViewById(R.id.id_refresh_tracking_icon);
//        /**SearchView declarations to filter the vehicles list */
//        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
//        search = (SearchView) findViewById(R.id.search);
//        navigationlist = (ListView) findViewById(R.id.id_navigation_list);
//        search.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
//        search.setIconifiedByDefault(false);
//        search.setOnQueryTextListener(this);
//        search.setOnCloseListener(this);
//        /**SearchView declarations to filter the vehicles list */
//        /** Refering Button with the ID in the XML layout*/
//        history_close_icon = (Button) findViewById(R.id.history_close_icon);
//        history_min_icon = (Button) findViewById(R.id.history_min_icon);
//        replay_icon = (Button) findViewById(R.id.replay_icon);
//        play_icon = (Button) findViewById(R.id.play_icon);
//        pause_icon = (Button) findViewById(R.id.pause_icon);
//        save_poi = (Button) findViewById(R.id.id_store_poi_button);
//        id_history_selection_done = (Button) findViewById(R.id.id_history_selection_done);
//        id_history_selection_cancel = (Button) findViewById(R.id.id_history_selection_cancel);
//        id_poi_layout_close = (Button) findViewById(R.id.id_poi_layout_close);
//        id_add_poi_btn = (Button) findViewById(R.id.id_add_poi_btn);
//        id_next_vehicle = (ImageView) findViewById(R.id.id_vehicle_next);
//        id_previous_vehicle = (ImageView) findViewById(R.id.id_vehicle_previous);
//        id_curr_vehicle_info_min = (Button) findViewById(R.id.id_curr_vehicle_info_min);
//        current_vehicle_info_layout.setVisibility(View.GONE);
//        /** Refering RelativeLayout with the ID in the XML layout*/
//        id_from_datetime_layout = (RelativeLayout) findViewById(R.id.id_from_datetime_layout);
//        id_to_datetime_layout = (RelativeLayout) findViewById(R.id.id_to_datetime_layout);
//        id_history_type_layout = (RelativeLayout) findViewById(R.id.id_history_type_layout);
//        /** Refering RadioGroup,RadioButton with the ID in the XML layout*/
//        radioGroup = (RadioGroup) findViewById(R.id.id_history_time_group);
//        RadioButton custom_radio = (RadioButton) findViewById(R.id.id_custom_group);
//        RadioButton today_radio = (RadioButton) findViewById(R.id.id_today_group);
//        RadioButton yesterday_radio = (RadioButton) findViewById(R.id.id_yesterday_group);
//        RadioButton daybeforeyesterday_radio = (RadioButton) findViewById(R.id.id_daybeforeyesterday_group);
//        RadioButton lastonehour_radio = (RadioButton) findViewById(R.id.id_lastonehour_group);
//        /** Refering SeekBar with the ID in the XML layout*/
//        speed_seekbar = (SeekBar) findViewById(R.id.speed_seekbar);
//        history_seekbar = (SeekBar) findViewById(R.id.history_seekbar);
//        /**Set Typeface for TextView, EditText, Button i.e Font Styles*/
//        history_start_location_label.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
//        history_end_location_label.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
//        history_timetaken_label.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
//        history_timetaken_label.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
//        id_show_history_vehicleid.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
//        store_poi.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
//        id_curr_odo_label.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
//        id_curr_odo_value.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
//        id_curr_todaydistance_label.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
//        id_curr_todaydistance_value.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
//        id_curr_parked_label.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
//        id_curr_parked_value.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
//        history_timetaken_value.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
//        history_start_location_value.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
//        history_end_location_value.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
//        replay_icon.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
//        history_distance_covered_value.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
//        id_curr_vehicle_id_value.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
//        id_histroy_vehicleid.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
//        id_curr_speed_value.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
//        custom_radio.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
//        today_radio.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
//        yesterday_radio.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
//        daybeforeyesterday_radio.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
//        lastonehour_radio.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
//        id_add_poi_btn.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
//        id_curr_todaydistance_label.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
//        id_curr_speed_label.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
//        id_poi_layout_close.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
//        id_history_type_value.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
//        id_all_vehicles_value.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
//        id_online_vehicles_value.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
//        id_offline_vehicles_value.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
//        id_history_selection_done.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
//        id_history_selection_cancel.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
//        id_all_vehicles_label.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
//        id_online_vehicles_label.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
//        id_offline_vehicles_label.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
//        id_curr_vehicle_address_label.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
//        id_curr_vehicle_address_value.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
//        id_refresh_countdowntimer.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
//        id_poi_label.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
//        save_poi.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
//        id_from_date.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
//        id_from_time.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
//        id_to_date.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
//        id_to_time.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
//        id_from_label.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
//        id_setting_label.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
//        id_navigation_header_label.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
//        id_refresh_rate_label.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
//        id_refresh_rate_value.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
//        id_history_label.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
//        id_history_value.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
//        id_settings_support.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
//        id_settings_support_address_line1.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
//        id_settings_support_address_line2.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
//        id_settings_support_mobilenumber.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
//        id_history_speed_info_label.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
//        id_history_speed_info_value.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
//        id_history_distance_label.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
//        id_history_distance_value.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
//        id_history_direction_label.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
//        id_history_direction_value.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
//        histroy_start_time_value.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
//        histroy_start_time_label.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
//        history_endtime_label.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
//        history_endtime_value.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
//        /**Set Typeface for TextView i.e Font Styles*/
//        /**Using SharedPreference get the Phonenumber of the user and also get the refresh rate for tracking vehicle*/
//        username = sp.getString("mPhoneNumber", "9940642634");
//        id_refresh_rate_value.setText(getStdTableValue("refresh_rate") + " Seconds");
//        id_history_value.setText("Interval (" + getStdTableValue("history") + " Minute)");
//        refresh_rate = Long.parseLong(getStdTableValue("refresh_rate"));
//        startTime = (refresh_rate * 1000);
//        id_refresh_rate_value.setText(getStdTableValue("refresh_rate") + " Seconds");
//        countDownTimer = new RefreshCountDownTimer(startTime, interval - 500);
//
//        id_map_info_layout = (LinearLayout) findViewById(R.id.map_info_layout);
//        rgViews = (RadioGroup) findViewById(R.id.rg_views);
//
//        rgViews.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
//
//            @Override
//            public void onCheckedChanged(RadioGroup group, int checkedId) {
//                if (checkedId == R.id.rb_normal) {
//                    map.setMapType(GoogleMap.MAP_TYPE_NORMAL);
//                } else if (checkedId == R.id.rb_satellite) {
//                    map.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
//                } else if (checkedId == R.id.rb_terrain) {
//                    map.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
//                }
//            }
//        });
//
//        map = ((MapFragment) getFragmentManager().findFragmentById(R.id.vehicles_map)).getMap();
//
//        map.setPadding(1, 1, 1, 150);
//        map.getUiSettings().setZoomControlsEnabled(true);
//        mMarkersHashMap = new HashMap<Marker, JSONObject>();
//        parker_mMarkersHashMap = new HashMap<Marker, String>();
//        map.setOnInfoWindowClickListener(this);
//        // map.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
//        /** setUpDrawer to assign drawerlayout items*/
//        setUpDrawer();
//        /**Setting listener to the home and publicmenu navigation so that when touching or swiping the drawer layout the correspomdig listener will get called */
//        home.setOnClickListener(homeOnclickListener);
//        /** prepareListData to create list for Navigation and Public Vamos*/
//        prepareListData();
//        iLoopDelay = speed_seekbar.getProgress();
//        speed_seekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
//            @Override
//            public void onProgressChanged(SeekBar seekBar, int progress, boolean b) {
//                iLoopDelay = progress;
//            }
//
//            @Override
//            public void onStartTrackingTouch(SeekBar seekBar) {
//                isSpeedChanged = true;
//                pause = true;
//                handler1.removeCallbacks(runnable1);
//                handler.removeCallbacksAndMessages(null);
//            }
//
//            @Override
//            public void onStopTrackingTouch(SeekBar seekBar) {
//                isSpeedChanged = false;
//                pause = false;
//                marker_loop(progressChanged);
//                paused_value = 0;
//            }
//        });
//        history_seekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
//            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
//                progressChanged = progress;
//                if (pause) {
//                    play_icon.setVisibility(View.VISIBLE);
//                    pause_icon.setVisibility(View.GONE);
//                } else {
//                    play_icon.setVisibility(View.GONE);
//                    pause_icon.setVisibility(View.VISIBLE);
//                }
//                replay_icon.setVisibility(View.GONE);
//            }
//
//            public void onStartTrackingTouch(SeekBar seekBar) {
//                // TODO Auto-generated method stub
//                if (!pause) {
//                    seekbarchanged = true;
//                }
//                handler1.removeCallbacks(runnable1);
//                handler.removeCallbacksAndMessages(null);
//            }
//
//            public void onStopTrackingTouch(SeekBar seekBar) {
//                if (!pause) {
//                    Handler seekbarhandler = new Handler();
//                    seekbarhandler.postDelayed(new Runnable() {
//                        @Override
//                        public void run() {
//                            marker_loop(progressChanged);
//                        }
//                    }, 500);
//                }
//                if (progressChanged == history_seekbar.getMax()) {
//                    history_seekbar.setVisibility(View.GONE);
//                    replay_icon.setVisibility(View.VISIBLE);
//                    id_history_show_min_layout.setVisibility(View.VISIBLE);
//                    history_detail_layout.setVisibility(View.VISIBLE);
//                    pause_icon.setVisibility(View.GONE);
//                    play_icon.setVisibility(View.GONE);
//                }
//            }
//        });
//        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(RadioGroup group, int checkedId) {
//                // find which radio button is selected
//                if (checkedId == R.id.id_custom_group) {
//                    id_date_time_picker_layout.setVisibility(View.VISIBLE);
//                    id_from_datetime_layout.setVisibility(View.VISIBLE);
//                    id_to_datetime_layout.setVisibility(View.VISIBLE);
//                    radioGroup.clearCheck();
//                    radioGroup.setVisibility(View.GONE);
//                } else if (checkedId == R.id.id_today_group) {
//                    id_history_type_value.setText("Today");
//                    id_date_time_picker_layout.setVisibility(View.VISIBLE);
//                    id_from_datetime_layout.setVisibility(View.GONE);
//                    id_to_datetime_layout.setVisibility(View.GONE);
//                    radioGroup.clearCheck();
//                    radioGroup.setVisibility(View.GONE);
//                    String today_date = cons.getDateandTime("today");
//                    id_from_date.setText(today_date);
//                    id_from_time.setText("00:00:00");
//                    id_to_date.setText(today_date);
//                    id_to_time.setText("23:59:00");
//                } else if (checkedId == R.id.id_yesterday_group) {
//                    id_history_type_value.setText("Yesterday");
//                    id_date_time_picker_layout.setVisibility(View.VISIBLE);
//                    id_from_datetime_layout.setVisibility(View.GONE);
//                    id_to_datetime_layout.setVisibility(View.GONE);
//                    radioGroup.clearCheck();
//                    radioGroup.setVisibility(View.GONE);
//                    String yesterday_date = cons.getDateandTime("yesterday");
//                    id_from_date.setText(yesterday_date);
//                    id_from_time.setText("00:00:00");
//                    id_to_date.setText(yesterday_date);
//                    id_to_time.setText("23:59:00");
//                } else if (checkedId == R.id.id_daybeforeyesterday_group) {
//                    id_history_type_value.setText("Daybefore");
//                    id_date_time_picker_layout.setVisibility(View.VISIBLE);
//                    id_from_datetime_layout.setVisibility(View.GONE);
//                    id_to_datetime_layout.setVisibility(View.GONE);
//                    radioGroup.clearCheck();
//                    radioGroup.setVisibility(View.GONE);
//                    String daybeforeyesterday_date = cons.getDateandTime("daybeforeyesterday");
//                    id_from_date.setText(daybeforeyesterday_date);
//                    id_from_time.setText("00:00:00");
//                    id_to_date.setText(daybeforeyesterday_date);
//                    id_to_time.setText("23:59:00");
//                } else if (checkedId == R.id.id_lastonehour_group) {
//                    id_history_type_value.setText("Last one hour");
//                    id_date_time_picker_layout.setVisibility(View.VISIBLE);
//                    id_from_datetime_layout.setVisibility(View.GONE);
//                    id_to_datetime_layout.setVisibility(View.GONE);
//                    radioGroup.clearCheck();
//                    radioGroup.setVisibility(View.GONE);
//                    Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("GMT+5:30"));
//                    int hour = cal.get(Calendar.HOUR_OF_DAY);
//                    int min = cal.get(Calendar.MINUTE);
//                    int past_hour = cal.get(Calendar.HOUR_OF_DAY) - 1;
//                    String lastonehour = cons.getDateandTime("today");
//                    id_from_date.setText(lastonehour);
//                    id_from_time.setText(new StringBuilder().append(pad(past_hour)).append(":").append(pad(min)).append(":").append("00"));
//                    id_to_date.setText(lastonehour);
//                    id_to_time.setText(new StringBuilder().append(pad(hour)).append(":").append(pad(min)).append(":").append("00"));
//                }
//            }
//        });
//        navigationlist.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                if (navigationList.get(position).equals("Home")) {
//                    SharedPreferences.Editor editor = sp.edit();
//                    editor.putBoolean("tracking_exception", false);
//                    editor.commit();
//                    track = false;
//                    cancelVehicleTracking();
//                    check_opened_dialog();
//                    home_button_functionality();
//                } else if (navigationList.get(position).equals("Vehicles")) {
//                    check_opened_dialog();
//                    vehicle_button_functionality();
//                } else if (navigationList.get(position).equals("Settings")) {
//                   /* navigation_status = "Settings";
//                    mDrawerLayout.closeDrawer(id_navigation_layout);
//                    id_settings_layout.setVisibility(View.VISIBLE);
//                    expListView.setVisibility(View.GONE);
//                    search.setVisibility(View.GONE);
//                    current_vehicle_info_layout.setVisibility(View.GONE);
//                    vehicle_info_layout.setVisibility(View.GONE);
//                    id_refresh_tracking_icon.setVisibility(View.GONE);
//                    id_refresh_countdowntimer.setVisibility(View.GONE);
//                    history_detail_layout.setVisibility(View.GONE);
//                    header_layout.setVisibility(View.VISIBLE);
//                    history_vehicle_info_layout.setVisibility(View.GONE);
//                    SharedPreferences.Editor editor = sp.edit();
//                    editor.putBoolean("tracking_exception", false);
//                    editor.commit();
//                    track = false;
//                    cancelVehicleTracking();
//                    check_opened_dialog();*/
//
//                    startActivity(new Intent(NavigationActivity.this, Settings.class));
//                    finish();
//
//                } else if (navigationList.get(position).equalsIgnoreCase(getResources().getString(R.string.signout))) {
//                    new doSignout().execute("");
//                } else if (navigationList.get(position).equalsIgnoreCase(getResources().getString(R.string.notification))) {
//                    startActivity(new Intent(NavigationActivity.this, NotificationListActivity.class));
//                    finish();
//                } else if (navigationList.get(position).equalsIgnoreCase(getResources().getString(R.string.version_name))) {
//
//                    Toast.makeText(getApplicationContext(), "You are using version : " + getResources().getString(R.string.app_version_name) + " of " + getResources().getString(R.string.app_name) + " application", Toast.LENGTH_LONG).show();
//                } else if (navigationList.get(position).equalsIgnoreCase(getResources().getString(R.string.kms_summary))) {
//
//                    startActivity(new Intent(NavigationActivity.this, VehicleListActivity.class));
//                    finish();
//                }
//            }
//        });
//        save_poi.setOnClickListener(new OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (checkNetworkSettings()) {
//                    String url = Const.API_URL + "mobile/setPOIName?userId=" + username + "&vehicleId=" + sp.getString("current_vehicleID", "") + "&poiName=" + store_poi.getText().toString() + "&macid=" + getStdTableValue("macid") + "&appid=" + getStdTableValue("appid");
//                    new SendPOIInformation().execute(url);
//                } else {
//                    Toast internet_toast = Toast.makeText(context, "Please Check your Internet Connection", Toast.LENGTH_LONG);
//                    internet_toast.show();
//                }
//            }
//        });
//
//        id_add_poi_btn.setOnClickListener(new OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                id_poi_layout.setVisibility(View.VISIBLE);
//            }
//        });
//        id_next_vehicle.setOnClickListener(new OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                position += 1;
//                id_previous_vehicle.setVisibility(View.VISIBLE);
//                try {
//                    if (position == child.size() - 1) {
//                        id_previous_vehicle.setVisibility(View.VISIBLE);
//                        id_next_vehicle.setVisibility(View.INVISIBLE);
//                    } else if (position == 0) {
//                        id_previous_vehicle.setVisibility(View.INVISIBLE);
//                        id_next_vehicle.setVisibility(View.VISIBLE);
//                    } else {
//                        id_next_vehicle.setVisibility(View.VISIBLE);
//                        id_previous_vehicle.setVisibility(View.VISIBLE);
//                    }
//                } catch (Exception e) {
//                    if (position == vehicle.size() - 1) {
//                        id_previous_vehicle.setVisibility(View.VISIBLE);
//                        id_next_vehicle.setVisibility(View.INVISIBLE);
//                    } else if (position == 0) {
//                        id_previous_vehicle.setVisibility(View.INVISIBLE);
//                        id_next_vehicle.setVisibility(View.VISIBLE);
//                    } else {
//                        id_next_vehicle.setVisibility(View.VISIBLE);
//                        id_previous_vehicle.setVisibility(View.VISIBLE);
//                    }
//                }
//                vehicleSelection(child.get(position));
//                countDownTimer = new RefreshCountDownTimer(startTime, interval - 500);
//                vehicletracking(selected_vehicleid);
//            }
//        });
//        id_previous_vehicle.setOnClickListener(new OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                position -= 1;
//                id_next_vehicle.setVisibility(View.VISIBLE);
//                if (map_marker_vechile_position == 0) {
//                    id_previous_vehicle.setVisibility(View.INVISIBLE);
//                }
//                try {
//                    if (position == 0) {
//                        id_previous_vehicle.setVisibility(View.INVISIBLE);
//                        id_next_vehicle.setVisibility(View.VISIBLE);
//                    } else if (position == child.size() - 1) {
//                        id_previous_vehicle.setVisibility(View.VISIBLE);
//                        id_next_vehicle.setVisibility(View.INVISIBLE);
//                    } else {
//                        id_previous_vehicle.setVisibility(View.VISIBLE);
//                        id_next_vehicle.setVisibility(View.VISIBLE);
//                    }
//                } catch (Exception e) {
//                    if (position == 0) {
//                        id_previous_vehicle.setVisibility(View.INVISIBLE);
//                        id_next_vehicle.setVisibility(View.VISIBLE);
//                    } else if (position == vehicle.size() - 1) {
//                        id_previous_vehicle.setVisibility(View.VISIBLE);
//                        id_next_vehicle.setVisibility(View.INVISIBLE);
//                    } else {
//                        id_previous_vehicle.setVisibility(View.VISIBLE);
//                        id_next_vehicle.setVisibility(View.VISIBLE);
//                    }
//                }
//                vehicleSelection(child.get(position));
//                countDownTimer = new RefreshCountDownTimer(startTime, interval - 500);
//                vehicletracking(selected_vehicleid);
//            }
//        });
//        id_curr_vehicle_info_min.setOnClickListener(new OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (id_curr_vehicle_info_min.getText().equals("^")) {
//                    id_speed_min_layout.setVisibility(View.VISIBLE);
//                    id_curr_vehicle_info_min.setText("--");
//                } else {
//                    id_speed_min_layout.setVisibility(View.GONE);
//                    id_curr_vehicle_info_min.setText("^");
//                }
//            }
//        });
//        id_vehicle_id_layout.setOnClickListener(new OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                id_curr_vehicle_info_min.setVisibility(View.VISIBLE);
//                if (id_speed_min_layout.getVisibility() == View.VISIBLE) {
//                    id_speed_min_layout.setVisibility(View.GONE);
//                    id_curr_vehicle_info_min.setText("^");
//                } else {
//                    id_speed_min_layout.setVisibility(View.VISIBLE);
//                    id_curr_vehicle_info_min.setText("--");
//                }
//            }
//        });
//        history_min_icon.setOnClickListener(new OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                id_history_show_min_layout.setVisibility(View.GONE);
//                history_min_icon.setVisibility(View.GONE);
//                history_close_icon.setVisibility(View.VISIBLE);
//            }
//        });
//        is_history_vehicle_layout.setOnClickListener(new OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (id_history_show_min_layout.getVisibility() == View.VISIBLE) {
//                    id_history_show_min_layout.setVisibility(View.GONE);
//                    history_min_icon.setVisibility(View.GONE);
//                    history_close_icon.setVisibility(View.VISIBLE);
//                } else {
//                    id_history_show_min_layout.setVisibility(View.VISIBLE);
//                    history_min_icon.setVisibility(View.VISIBLE);
//                    history_close_icon.setVisibility(View.GONE);
//                }
//            }
//        });
//        id_poi_layout_close.setOnClickListener(new OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                id_poi_layout.setVisibility(View.GONE);
//            }
//        });
//        /** listener to handle when From date is clicked in history selection*/
//        id_from_date.setOnClickListener(new OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                from_date = true;
//                to_date = false;
//                from_time = false;
//                to_time = false;
//                showDialog(DATE_PICKER_ID);
//            }
//        });
//        /** listener to handle when From Time is clicked in history selection*/
//        id_from_time.setOnClickListener(new OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                from_date = false;
//                to_date = false;
//                from_time = true;
//                to_time = false;
//                showDialog(TIME_DIALOG_ID);
//            }
//        });
//        /** listener to handle when To date is clicked in history selection*/
//        id_to_date.setOnClickListener(new OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                from_date = false;
//                to_date = true;
//                from_time = false;
//                to_time = false;
//                showDialog(DATE_PICKER_ID);
//            }
//        });
//        /** listener to handle when To Time is clicked in history selection*/
//        id_to_time.setOnClickListener(new OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                from_date = false;
//                to_date = false;
//                from_time = false;
//                to_time = true;
//                showDialog(TIME_DIALOG_ID);
//            }
//        });
//        /** listener to handle history types - Custom, Yesterday, Day before yesterday, Today, Last One hour*/
//        id_history_type_layout.setOnClickListener(new OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                radioGroup.setVisibility(View.VISIBLE);
//                id_date_time_picker_layout.setVisibility(View.GONE);
//            }
//        });
//        /** listener to handle when history is selected */
//        id_history_selection_done.setOnClickListener(new OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (checkNetworkSettings()) {
//                    radioGroup.setVisibility(View.GONE);
//                    id_date_time_picker_layout.setVisibility(View.GONE);
//                    id_map_info_layout.setVisibility(View.GONE);
//                    if (map != null) {
//                        map.clear();
//                    }
//                    String url = Const.API_URL + "mobile/getVehicleHistory4MobileV2?userId=" + username + "&vehicleId=" + sp.getString("current_vehicleID", selected_vehicleid) + "&fromDate=" + id_from_date.getText().toString() + "&fromTime=" + id_from_time.getText().toString() + "&toDate=" + id_to_date.getText().toString() + "&toTime=" + id_to_time.getText().toString() + "&macid=" + getStdTableValue("macid") + "&appid=" + getStdTableValue("appid");
//                    String par[] = {Const.API_URL + "mobile/getGeoFenceView?vehicleId=" + selected_vehicleid + "&userId=" + username + "&macid=" + getStdTableValue("macid") + "&appid=" + getStdTableValue("appid")};
//                    new GetVehicleHistory().execute(url);
//                    new GetPOIInformation().execute(par);
//                } else {
//                    Toast internet_toast = Toast.makeText(context, "Please Check your Internet Connection", Toast.LENGTH_LONG);
//                    internet_toast.show();
//                }
//            }
//        });
//        /** listener to handle when history is canceled */
//        id_history_selection_cancel.setOnClickListener(new OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                radioGroup.setVisibility(View.GONE);
//                id_date_time_picker_layout.setVisibility(View.GONE);
//                header_layout.setVisibility(View.VISIBLE);
//                vehicle_info_layout.setVisibility(View.VISIBLE);
//            }
//        });
//        /** listener to handle when history page is closed and update the page with the all vehicles marker */
//        history_close_icon.setOnClickListener(new OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (!pause) {
//                    pause = true;
//                }
//                backButtonPressed = true;
//                history_detail_layout.setVisibility(View.GONE);
//                header_layout.setVisibility(View.VISIBLE);
//                vehicle_info_layout.setVisibility(View.VISIBLE);
//                speed_seekbar.setVisibility(View.GONE);
//                history_seekbar.setVisibility(View.GONE);
//                id_next_vehicle.setVisibility(View.GONE);
//                play_icon.setVisibility(View.GONE);
//                pause_icon.setVisibility(View.GONE);
//                id_previous_vehicle.setVisibility(View.GONE);
//                history_vehicle_info_layout.setVisibility(View.GONE);
//                current_vehicle_info_layout.setVisibility(View.GONE);
//                home_button_functionality();
//            }
//        });
//        /** listener to handle when the selected vehicles history to be replayed */
//        replay_icon.setOnClickListener(new OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                id_history_show_min_layout.setVisibility(View.GONE);
//                speed_seekbar.setVisibility(View.GONE);
//                history_seekbar.setVisibility(View.VISIBLE);
//                a = 0;
//                pause = false;
//                speed_seekbar.setProgress(50);
//                marker_loop(0);
//                if (points.size() > 1) {
//                    replay_icon.setVisibility(View.GONE);
//                    pause_icon.setVisibility(View.VISIBLE);
//                    play_icon.setVisibility(View.GONE);
//                    speed_seekbar.setVisibility(View.GONE);
//                    history_seekbar.setVisibility(View.VISIBLE);
//                } else {
//                    replay_icon.setVisibility(View.GONE);
//                    play_icon.setVisibility(View.GONE);
//                    pause_icon.setVisibility(View.GONE);
//                    speed_seekbar.setVisibility(View.GONE);
//                    history_seekbar.setVisibility(View.GONE);
//                }
//            }
//        });
//        play_icon.setOnClickListener(new OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                pause = false;
//                marker_loop(progressChanged);
//                pause_icon.setVisibility(View.VISIBLE);
//                play_icon.setVisibility(View.GONE);
//                paused_value = 0;
//            }
//        });
//        pause_icon.setOnClickListener(new OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                pause = true;
//                handler1.removeCallbacks(runnable1);
//                handler.removeCallbacksAndMessages(null);
//                if (progressChanged == history_seekbar.getMax()) {
//                    history_seekbar.setVisibility(View.GONE);
//                    replay_icon.setVisibility(View.VISIBLE);
//                    id_history_show_min_layout.setVisibility(View.VISIBLE);
//                    history_detail_layout.setVisibility(View.VISIBLE);
//                    pause_icon.setVisibility(View.GONE);
//                    play_icon.setVisibility(View.GONE);
//                } else {
//                    pause_icon.setVisibility(View.GONE);
//                    play_icon.setVisibility(View.VISIBLE);
//                }
//            }
//        });
//        /** listener to handle when the selected vehicles history to be replayed */
//        id_total_vehicles_layout.setOnClickListener(new OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Cursor vehicle_info = null;
//                try {
//                    // get data from cursor
//                    vehicle_info = dbhelper.get_vehicle_details();
//                    updateVehicleList(vehicle_info, "getgroup", sp.getInt("current_group_pos", 0), "all", "vehicleslist");
//                    expListView.setVisibility(View.VISIBLE);
//                    search.setVisibility(View.VISIBLE);
//                    id_refresh_tracking_icon.setVisibility(View.GONE);
//                } catch (Exception e) {
//                    Toast.makeText(getApplicationContext(), "Something went wrong. Please try again", Toast.LENGTH_SHORT).show();
//                } finally {
//                    if (vehicle_info != null) {
//                        vehicle_info.close();
//                    }
//                }
//                id_refresh_tracking_icon.setVisibility(View.VISIBLE);
//            }
//        });
//        id_offline_vehicles_layout.setOnClickListener(new OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Cursor vehicle_info = null;
//                try {
//                    vehicle_info = dbhelper.get_vehicle_details();
//                    updateVehicleList(vehicle_info, "getgroup", sp.getInt("current_group_pos", 0), "OFF", "vehicleslist");
//                    expListView.setVisibility(View.VISIBLE);
//                    search.setVisibility(View.VISIBLE);
//                    id_refresh_tracking_icon.setVisibility(View.GONE);
//                } catch (Exception e) {
//                    Toast.makeText(getApplicationContext(), "Something went wrong. Please try again", Toast.LENGTH_SHORT).show();
//                } finally {
//                    if (vehicle_info != null) {
//                        vehicle_info.close();
//                    }
//                }
//                id_refresh_tracking_icon.setVisibility(View.VISIBLE);
//            }
//        });
//        id_online_vehicles_layout.setOnClickListener(new OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Cursor vehicle_info = null;
//                try {
//                    vehicle_info = dbhelper.get_vehicle_details();
//                    updateVehicleList(vehicle_info, "getgroup", sp.getInt("current_group_pos", 0), "ON", "vehicleslist");
//                    expListView.setVisibility(View.VISIBLE);
//                    search.setVisibility(View.VISIBLE);
//                    id_refresh_tracking_icon.setVisibility(View.GONE);
//                } catch (Exception e) {
//                    Toast.makeText(getApplicationContext(), "Something went wrong. Please try again", Toast.LENGTH_SHORT).show();
//                } finally {
//                    if (vehicle_info != null) {
//                        vehicle_info.close();
//                    }
//                }
//                id_refresh_tracking_icon.setVisibility(View.VISIBLE);
//            }
//        });
//        id_refresh_rate_value.setOnClickListener(new OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                id_refresh_rate_edit_value.setVisibility(View.VISIBLE);
//                id_done_refresh_rate.setVisibility(View.VISIBLE);
//                id_cancel_refresh_rate.setVisibility(View.VISIBLE);
//                id_refresh_rate_edit_value.setText(getStdTableValue("refresh_rate"));
//                id_refresh_rate_value.setVisibility(View.GONE);
//            }
//        });
//        id_history_value.setOnClickListener(new OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                id_history_edit_value.setVisibility(View.VISIBLE);
//                id_done_history.setVisibility(View.VISIBLE);
//                id_cancel_history.setVisibility(View.VISIBLE);
//                id_history_edit_value.setText(getStdTableValue("history"));
//                id_history_value.setVisibility(View.GONE);
//            }
//        });
//        id_done_refresh_rate.setOnClickListener(new OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                SharedPreferences.Editor editor = sp.edit();
//                editor.putString("refresh_rate", id_refresh_rate_edit_value.getText().toString());
//                editor.commit();
//                dbhelper.update_Settings_StdTableValues("refresh_rate", id_refresh_rate_edit_value.getText().toString());
//                id_refresh_rate_value.setText(getStdTableValue("refresh_rate") + " Seconds");
//                id_refresh_rate_edit_value.setText(getStdTableValue("refresh_rate"));
//                id_refresh_rate_edit_value.setVisibility(View.GONE);
//                id_done_refresh_rate.setVisibility(View.GONE);
//                id_cancel_refresh_rate.setVisibility(View.GONE);
//                id_refresh_rate_value.setVisibility(View.VISIBLE);
//                refresh_rate = Long.parseLong(getStdTableValue("refresh_rate"));
//                startTime = (refresh_rate * 1000);
//                countDownTimer = new RefreshCountDownTimer(startTime, interval - 500);
//                InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
//                inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
//            }
//        });
//        id_cancel_refresh_rate.setOnClickListener(new OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                dbhelper.update_Settings_StdTableValues("refresh_rate", id_refresh_rate_edit_value.getText().toString());
//                id_refresh_rate_value.setText(getStdTableValue("refresh_rate") + " Seconds");
//                id_refresh_rate_edit_value.setText(getStdTableValue("refresh_rate"));
//                id_refresh_rate_edit_value.setVisibility(View.GONE);
//                id_done_refresh_rate.setVisibility(View.GONE);
//                id_cancel_refresh_rate.setVisibility(View.GONE);
//                id_refresh_rate_value.setVisibility(View.VISIBLE);
//            }
//        });
//        id_done_history.setOnClickListener(new OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                dbhelper.update_Settings_StdTableValues("history", id_history_edit_value.getText().toString());
//                id_history_value.setText("Interval (" + getStdTableValue("history") + " Minute)");
//                id_history_edit_value.setText(getStdTableValue("history"));
//                id_history_edit_value.setVisibility(View.GONE);
//                id_done_history.setVisibility(View.GONE);
//                id_cancel_history.setVisibility(View.GONE);
//                id_history_value.setVisibility(View.VISIBLE);
//                InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
//                inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
//            }
//        });
//        id_cancel_history.setOnClickListener(new OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                dbhelper.update_Settings_StdTableValues("history", id_history_edit_value.getText().toString());
//                id_history_value.setText("Interval (" + getStdTableValue("history") + " Minute)");
//                id_history_edit_value.setText(getStdTableValue("history"));
//                id_history_edit_value.setVisibility(View.GONE);
//                id_done_history.setVisibility(View.GONE);
//                id_cancel_history.setVisibility(View.GONE);
//                id_history_value.setVisibility(View.VISIBLE);
//            }
//        });
//        search.setOnKeyListener(new SearchView.OnKeyListener() {
//            /**
//             * This listens for the user to press the enter button on
//             * the keyboard and then hides the virtual keyboard
//             */
//            public boolean onKey(View arg0, int arg1, KeyEvent event) {
//                // If the event is a key-down event on the "enter" button
//                if (event.getAction() == KeyEvent.KEYCODE_ENTER) {
//                    InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
//                    imm.hideSoftInputFromWindow(search.getWindowToken(), 0);
//                    return true;
//                }
//                return false;
//            }
//        });
//        map.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
//            @Override
//            public boolean onMarkerClick(Marker marker) {
//                int childelement_position;
//                if (track) {
//                    id_speed_min_layout.setVisibility(View.VISIBLE);
//                    id_curr_vehicle_info_min.setVisibility(View.VISIBLE);
//                    return true;
//                } else {
//                    if (!(navigation_status.equals("Settings"))) {
//                        if (mMarkersHashMap.containsKey(marker)) {
//                            if (checkNetworkSettings()) {
//                                jSelectedVehicleObject = mMarkersHashMap.get(marker);
//                                showVehicleInfoDialog(jSelectedVehicleObject, false);
//                            } else {
//                                Toast internet_toast = Toast.makeText(context, "Please Check your Internet Connection", Toast.LENGTH_LONG);
//                                internet_toast.show();
//                            }
//                            return true;
//                        } else if (parker_mMarkersHashMap.containsKey(marker)) {
//                            List<String> parker_marker_items = Arrays.asList(parker_mMarkersHashMap.get(marker).split(","));
//                            String parked_time = parker_marker_items.get(0);
//                            String parked_duration = parker_marker_items.get(1);
//                            String lat = parker_marker_items.get(2);
//                            String lng = parker_marker_items.get(3);
//                            String marker_state = parker_marker_items.get(4);
//                            final Dialog marker_info_dialog = new Dialog(context);
//                            marker_info_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//                            marker_info_dialog.setContentView(R.layout.custom_marker_dialog);
//                            marker_info_dialog.setCancelable(true);
//                            TextView id_historyplayback_parkedtime_text_labelid = (TextView) marker_info_dialog.findViewById(R.id.id_historyplayback_parkedtime_text_labelid);
//                            TextView id_view_parkedtime = (TextView) marker_info_dialog.findViewById(R.id.id_view_parkedtime);
//                            TextView id_historyplayback_parkedduration_text_labelid = (TextView) marker_info_dialog.findViewById(R.id.id_historyplayback_parkedduration_text_labelid);
//                            TextView id_view_parkedduration = (TextView) marker_info_dialog.findViewById(R.id.id_view_parkedduration);
//                            TextView id_historyplayback_addres_text_labelid = (TextView) marker_info_dialog.findViewById(R.id.id_historyplayback_addres_text_labelid);
//                            TextView id_view_address = (TextView) marker_info_dialog.findViewById(R.id.id_view_address);
//                            RelativeLayout id_historyplayback_parkedduration_layout = (RelativeLayout) marker_info_dialog.findViewById(R.id.id_historyplayback_parkedduration_layout);
//                            id_historyplayback_parkedtime_text_labelid.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
//                            id_view_parkedtime.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
//                            id_historyplayback_parkedduration_text_labelid.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
//                            id_view_parkedduration.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
//                            id_historyplayback_addres_text_labelid.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
//                            id_view_address.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
//                            id_view_parkedtime.setText(parked_time);
//                            id_view_parkedduration.setText(parked_duration);
//                            if (marker_state.equals("start")) {
//                                id_historyplayback_parkedduration_layout.setVisibility(View.GONE);
//                                id_historyplayback_parkedtime_text_labelid.setText("Start Time");
//                            } else if (marker_state.equals("end")) {
//                                id_historyplayback_parkedduration_layout.setVisibility(View.GONE);
//                                id_historyplayback_parkedtime_text_labelid.setText("End Time");
//                            }
//                            if (!lat.equals("-") && !lng.equals("-")) {
//                                new GetAddressTask(id_view_address).execute(Double.parseDouble(lat), Double.parseDouble(lng), 5.0);
//                            }
//                            marker_info_dialog.show();
//                            return true;
//                        } else {
//                            return false;
//                        }
//                    } else {
//                        return false;
//                    }
//                }
//            }
//        });
//        id_refresh_tracking_icon.setOnClickListener(new OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (navigation_status.equals("tracking")) {
//                    if (checkNetworkSettings()) {
//                        Thread splashThread = new Thread() {
//                            @Override
//                            public void run() {
//                                String response_from_server = null;
//                                try {
//                                    runOnUiThread(new Runnable() {
//                                        @Override
//                                        public void run() {
//                                            progressDialog = MyCustomProgressDialog.ctor(context);
//                                            progressDialog.show();
//                                        }
//                                    });
//                                    String urlstr = Const.API_URL + "mobile/getSelectedVehicleLocation?userId=" + username + "&vehicleId=" + sp.getString("current_vehicleID", "") + "&macid=" + getStdTableValue("macid") + "&appid=" + getStdTableValue("appid");
//                                    response_from_server = sendGet(urlstr, 10000);
//                                    sleep(2000);
//                                } catch (Exception e) {
//                                    e.printStackTrace();
//                                } finally {
//                                    if (response_from_server != null && !response_from_server.isEmpty()) {
//                                        dbhelper.insert_current_vehicle_details(response_from_server);
//                                    }
//                                    runOnUiThread(new Runnable() {
//                                        @Override
//                                        public void run() {
//                                            if (progressDialog.isShowing()) {
//                                                progressDialog.dismiss();
//                                                Toast internet_toast = Toast.makeText(context, "Refreshed", Toast.LENGTH_LONG);
//                                                internet_toast.show();
//                                            }
//                                        }
//                                    });
//                                }
//                            }
//                        };
//                        splashThread.start();
//                    } else {
//                        Toast internet_toast = Toast.makeText(context, "Internet not connected", Toast.LENGTH_LONG);
//                        internet_toast.show();
//                        home_button_functionality();
//                    }
//                } else {
//                    home_button_functionality();
//                    Toast internet_toast = Toast.makeText(context, "Refreshed", Toast.LENGTH_LONG);
//                    internet_toast.show();
//                }
//            }
//        });
//        store_poi.setOnEditorActionListener(new TextView.OnEditorActionListener() {
//            @Override
//            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
//                if (actionId == EditorInfo.IME_ACTION_DONE) {
//                    InputMethodManager mgr = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
//                    mgr.hideSoftInputFromWindow(store_poi.getWindowToken(), 0);
//                }
//                return false;
//            }
//        });
//        //Initialize quick action view
//        doQuickMenuSetup();
//    }
//
//    /**
//     * Called just before the activity is destroyed.
//     */
//    @Override
//    public void onDestroy() {
//        super.onDestroy();
//        handler.removeCallbacksAndMessages(null);
//        SharedPreferences.Editor editor = sp.edit();
//        editor.putBoolean("tracking_exception", false);
//        editor.commit();
//        track = false;
//        cancelVehicleTracking();
//        if (!pause) {
//            pause = true;
//            if (map != null) {
//                map.clear();
//            }
//            history_detail_layout.setVisibility(View.GONE);
//        }
//        handler.removeCallbacksAndMessages(null);
//    }
//
//    public boolean onKeyDown(int keyCode, KeyEvent event) {
//        // TODO Auto-generated method stub
//        if (keyCode == KeyEvent.KEYCODE_MENU) {
//            SharedPreferences.Editor editor = sp.edit();
//            editor.putBoolean("tracking_exception", false);
//            editor.commit();
//            try {
//                if (current_address__dialog.isShowing()) {
//                    current_address__dialog.hide();
//                }
//            } catch (Exception e) {
//            }
//            if (mDrawerLayout.isDrawerOpen(id_navigation_layout)) {
//                mDrawerLayout.closeDrawer(id_navigation_layout);
//            } else {
//                mDrawerLayout.closeDrawer(publicexplist);
//                mDrawerLayout.openDrawer(id_navigation_layout);
//            }
//            return true;
//        } else if (keyCode == KeyEvent.KEYCODE_BACK) {
//            //tracking
//            backButtonPressed = true;
//            SharedPreferences.Editor editor = sp.edit();
//            editor.putBoolean("tracking_exception", false);
//            editor.commit();
//            id_next_vehicle.setVisibility(View.GONE);
//            id_previous_vehicle.setVisibility(View.GONE);
//            if (id_poi_layout_close.getVisibility() == View.VISIBLE) {
//                id_poi_layout.setVisibility(View.GONE);
//            }
//            if (current_vehicle_info_layout.getVisibility() == View.VISIBLE) {
//                current_vehicle_info_layout.setVisibility(View.GONE);
//            }
//            id_refresh_tracking_icon.setVisibility(View.VISIBLE);
//            if (track) {
//                track = false;
//                cancelVehicleTracking();
//                current_vehicle_info_layout.setVisibility(View.GONE);
//            }
//            pause_icon.setVisibility(View.GONE);
//            play_icon.setVisibility(View.GONE);
//            if (navigation_status.equals("Home")) {
//                if (history_detail_layout.getVisibility() == View.VISIBLE) {
//                    pause = !pause;
//                    if (map != null) {
//                        map.clear();
//                    }
//                    history_detail_layout.setVisibility(View.GONE);
//                    speed_seekbar.setVisibility(View.GONE);
//                    history_seekbar.setVisibility(View.GONE);
//                    history_vehicle_info_layout.setVisibility(View.GONE);
//                    pause_icon.setVisibility(View.GONE);
//                    play_icon.setVisibility(View.GONE);
//                    id_refresh_tracking_icon.setVisibility(View.VISIBLE);
//                    home_button_functionality();
//                } else if (id_date_time_picker_layout.getVisibility() == View.VISIBLE) {
//                    radioGroup.setVisibility(View.GONE);
//                    id_date_time_picker_layout.setVisibility(View.GONE);
//                    header_layout.setVisibility(View.VISIBLE);
//                    vehicle_info_layout.setVisibility(View.VISIBLE);
//                } else if (radioGroup.getVisibility() == View.VISIBLE) {
//                    radioGroup.setVisibility(View.GONE);
//                    id_date_time_picker_layout.setVisibility(View.GONE);
//                    header_layout.setVisibility(View.VISIBLE);
//                    vehicle_info_layout.setVisibility(View.VISIBLE);
//                } else {
//                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
//                    alertDialogBuilder.setMessage("Are You Want To Exit?");
//                    alertDialogBuilder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
//                        @Override
//                        public void onClick(DialogInterface arg0, int arg1) {
//                            Intent intent = new Intent(Intent.ACTION_MAIN);
//                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                            intent.addCategory(Intent.CATEGORY_HOME);
//                            startActivity(intent);
//                        }
//                    });
//                    alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
//                        @Override
//                        public void onClick(DialogInterface dialog, int which) {
//                        }
//                    });
//                    AlertDialog alertDialog = alertDialogBuilder.create();
//                    alertDialog.show();
//                }
//            } else if (radioGroup.getVisibility() == View.VISIBLE) {
//                radioGroup.setVisibility(View.GONE);
//                id_date_time_picker_layout.setVisibility(View.GONE);
//                header_layout.setVisibility(View.VISIBLE);
//                vehicle_info_layout.setVisibility(View.VISIBLE);
//            } else if (id_date_time_picker_layout.getVisibility() == View.VISIBLE) {
//                radioGroup.setVisibility(View.GONE);
//                id_date_time_picker_layout.setVisibility(View.GONE);
//                header_layout.setVisibility(View.VISIBLE);
//                vehicle_info_layout.setVisibility(View.VISIBLE);
//            } else {
//                if (history_detail_layout.getVisibility() == View.VISIBLE) {
//                    pause = !pause;
//                    if (map != null) {
//                        map.clear();
//                    }
//                    history_detail_layout.setVisibility(View.GONE);
//                    speed_seekbar.setVisibility(View.GONE);
//                    history_seekbar.setVisibility(View.GONE);
//                    pause_icon.setVisibility(View.GONE);
//                    play_icon.setVisibility(View.GONE);
//                    history_vehicle_info_layout.setVisibility(View.GONE);
//                    id_refresh_tracking_icon.setVisibility(View.VISIBLE);
//                }
//                home_button_functionality();
//            }
//            setUpdrawerfunctioncalled = false;
//            return true;
//        } else
//            return super.onKeyDown(keyCode, event);
//    }
//
//    /**
//     * DB Initialisation
//     */
//    @Override
//    protected void onPause() {
//        super.onPause();
//        //Log.i("onPause", "The onPause() event");
//        handler.removeCallbacksAndMessages(null);
//        try {
//            if (current_address__dialog.isShowing()) {
//                current_address__dialog.hide();
//            }
//        } catch (Exception e) {
//        }
//    }
//
//    /**
//     * Called when the activity has become visible.
//     */
//    @Override
//    protected void onResume() {
//        super.onResume();
//        if (track) {
//            String id = sp.getString("tracking_vehicle_id", null);
//            vehicletracking(id);
//        } else {
//            pause_icon.setVisibility(View.GONE);
//            play_icon.setVisibility(View.GONE);
//            if (!setUpdrawerfunctioncalled) {
//                home_button_functionality();
//            }
//            if (progressDialog.isShowing()) {
//                progressDialog.dismiss();
//            }
//        }
//    }
//
//    /**
//     * Called when the activity is no longer visible.
//     */
//    @Override
//    protected void onStop() {
//        super.onStop();
//        handler.removeCallbacksAndMessages(null);
//        SharedPreferences.Editor editor = sp.edit();
//        editor.putBoolean("tracking_exception", false);
//        editor.commit();
//        id_next_vehicle.setVisibility(View.GONE);
//        id_previous_vehicle.setVisibility(View.GONE);
//        current_vehicle_info_layout.setVisibility(View.GONE);
//        if (id_date_time_picker_layout.getVisibility() == View.VISIBLE) {
//            radioGroup.setVisibility(View.GONE);
//            id_date_time_picker_layout.setVisibility(View.GONE);
//            header_layout.setVisibility(View.VISIBLE);
//            vehicle_info_layout.setVisibility(View.VISIBLE);
//            id_map_info_layout.setVisibility(View.GONE);
//        }
//        if (radioGroup.getVisibility() == View.VISIBLE) {
//            radioGroup.setVisibility(View.GONE);
//        }
//        if (id_date_time_picker_layout.getVisibility() == View.VISIBLE) {
//            id_date_time_picker_layout.setVisibility(View.GONE);
//        }
//        if (track) {
//            track = false;
//            handler.removeCallbacksAndMessages(null);
//            cancelVehicleTracking();
//            id_refresh_tracking_icon.setVisibility(View.VISIBLE);
//            id_refresh_countdowntimer.setVisibility(View.GONE);
//        }
//        if (navigation_status.equals("tracking")) {
//            navigation_status = "Home";
//            search.setVisibility(View.GONE);
//            backButtonPressed = true;
//            expListView.setVisibility(View.GONE);
//            if (!pause) {
//                pause = true;
//                if (map != null) {
//                    map.clear();
//                }
//                history_detail_layout.setVisibility(View.GONE);
//            }
//        }
//        if (history_detail_layout.getVisibility() == View.VISIBLE) {
//            pause = !pause;
//            if (map != null) {
//                map.clear();
//            }
//            backButtonPressed = true;
//            history_detail_layout.setVisibility(View.GONE);
//            header_layout.setVisibility(View.VISIBLE);
//            vehicle_info_layout.setVisibility(View.VISIBLE);
//            speed_seekbar.setVisibility(View.GONE);
//            history_seekbar.setVisibility(View.GONE);
//            pause_icon.setVisibility(View.GONE);
//            play_icon.setVisibility(View.GONE);
//            history_vehicle_info_layout.setVisibility(View.GONE);
//            id_refresh_tracking_icon.setVisibility(View.VISIBLE);
//        }
//        setUpDrawer();
//    }
//    /** Expandable Adapter */
//    /**
//     * Get the names and icons references to build the drawer menu...
//     */
//    private void setUpDrawer() {
//        setUpdrawerfunctioncalled = true;
//        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
//        mDrawerLayout.setScrimColor(getResources().getColor(android.R.color.transparent));
//        mDrawerLayout.setDrawerListener(mDrawerListener);
//        expListView = (ExpandableListView) findViewById(R.id.lvExp);
//        publicexplist = (ExpandableListView) findViewById(R.id.publicexplist);
//        expListView.setVisibility(View.VISIBLE);
//        search.setVisibility(View.VISIBLE);
//        id_refresh_tracking_icon.setVisibility(View.VISIBLE);
//        vehicle_info_layout.setVisibility(View.VISIBLE);
//        id_map_info_layout.setVisibility(View.GONE);
//        /** Download app logo **/
////        String appLogoUrl = sp.getString("app_logo", null);
////        if (appLogoUrl != null) {
//////            new getAppLogo().execute(appLogoUrl);
////            byte[] prophoto = Base64.decode(appLogoUrl, Base64.DEFAULT);
////            Bitmap bmp = BitmapFactory.decodeByteArray(prophoto, 0,
////                    prophoto.length);
////            id_applogo.setImageBitmap(bmp);
////        }
//
//        CommonManager co = new CommonManager(getApplicationContext());
//        id_applogo.setImageBitmap(co.getBitmap());
//
////        setImage(id_applogo,co.getBitmap());
//        /** Get Vehicle Information **/
//        String par[] = {Const.API_URL + "mobile/getVehicleLocations?userId=" + username + "&macid=" + getStdTableValue("macid") + "&appid=" + getStdTableValue("appid"), "getall", Integer.toString(0), "all"};
//        new PullVehicleInformation().execute(par);
//    }
//
//    public void setImage(ImageView view1, Bitmap img_byte) {
//        ImageView view = view1;
//        //Constant.prtMsg("The image view is :::::::" + view);
//
//        Bitmap bitmapOrg = img_byte;
//
//        int width = bitmapOrg.getWidth();
//        int height = bitmapOrg.getHeight();
//        int newHeight = view.getLayoutParams().height;
//        int newWidth = view.getLayoutParams().width;
//        // int newWidth = 300;
//        // int newHeight = 470;
//
//        // calculate the scale - in this case = 0.4f
//        float scaleWidth = ((float) newWidth) / width;
//        float scaleHeight = ((float) newHeight) / height;
//
//        // createa matrix for the manipulation
//        Matrix matrix = new Matrix();
//        // resize the bit map
//        matrix.postScale(scaleWidth, scaleHeight);
//        // rotate the Bitmap
//        // matrix.postRotate(45);
//
//        // recreate the new Bitmap
//        Bitmap resizedBitmap = Bitmap.createBitmap(bitmapOrg, 0, 0, width,
//                height, matrix, true);
//
//        // make a Drawable from Bitmap to allow to set the BitMap
//        // to the ImageView, ImageButton or what ever
//        BitmapDrawable bmd = new BitmapDrawable(resizedBitmap);
//
//        // ImageView imageView = new ImageView(this);
//
//        // set the Drawable on the ImageView
//        view.setImageDrawable(bmd);
//
//        // center the Image
//        view.setScaleType(ImageView.ScaleType.CENTER);
//
//        // Now change ImageView's dimensions to match the scaled image
//        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) view
//                .getLayoutParams();
//        params.width = width;
//        params.height = height;
//        view.setLayoutParams(params);
//
//    }
//
//    /***/
//    private void doQuickMenuSetup() {
//        ActionItem mapItem = new ActionItem(0, "Map", getResources().getDrawable(R.mipmap.ic_map));
//        ActionItem infoItem = new ActionItem(1, "Info", getResources().getDrawable(R.mipmap.ic_info));
//        ActionItem lockItem = new ActionItem(2, "Lock", getResources().getDrawable(R.mipmap.ic_lock));
//        ActionItem reportItem = new ActionItem(3, "Report", getResources().getDrawable(R.mipmap.ic_report));
//        ActionItem historyItem = new ActionItem(4, "History", getResources().getDrawable(R.mipmap.ic_history));
//        //ActionItem liveTrackItem = new ActionItem(5, "Live", getResources().getDrawable(R.mipmap.ic_live));
//        ArrayList<ActionItem> alActionItem = new ArrayList<>();
//        alActionItem.add(mapItem);
//        alActionItem.add(infoItem);
//        alActionItem.add(lockItem);
//        alActionItem.add(reportItem);
//        alActionItem.add(historyItem);
//        //alActionItem.add(liveTrackItem);
//        QuickActionListener mOnActionItemClickListener = new QuickActionListener() {
//            @Override
//            public void onItemClick(int pos, int actionId) {
//                try {
//                    selected_vehicleid = jSelectedVehicleObject.getString("vehicleId");
//                    SharedPreferences.Editor editor = sp.edit();
//                    editor.putString("current_vehicleID", selected_vehicleid);
//                    editor.commit();
//                } catch (Exception e) {
//                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.oops_error), Toast.LENGTH_SHORT).show();
//                }
//                if (actionId == 0) {
//                    //Vehicle Map
//                    vehicleSelection(strSelectedVehicle);
//                } else if (actionId == 1) {
//                    //Vehicle Info
//                    if (jSelectedVehicleObject != null) {
//                        showVehicleInfoDialog(jSelectedVehicleObject, true);
//                    } else {
//                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.oops_error), Toast.LENGTH_SHORT).show();
//                    }
//                } else if (actionId == 2) {
//                    //Lock/Unlock
//                    AlertDialog.Builder builder = new AlertDialog.Builder(NavigationActivity.this);
//                    builder.setTitle("CAUTION:");
//                    builder.setMessage("We assume that you understand the risk associated with issuing this command. The command will send SMS from your phone to immobilise/mobilise the vehicle, please use this command with caution.\n" +
//                            "\n" +
//                            "To continue, please press yes.").setCancelable(false).setPositiveButton("Yes", new DialogInterface.OnClickListener() {
//                        public void onClick(DialogInterface dialog, int id) {
//                            //do things
//                            dialog.cancel();
//                            showLockUnlockDialog();
//                        }
//                    }).setNegativeButton("No", new DialogInterface.OnClickListener() {
//                        @Override
//                        public void onClick(DialogInterface dialog, int i) {
//                            dialog.cancel();
//                        }
//                    });
//                    AlertDialog alert = builder.create();
//                    alert.show();
//                } else if (actionId == 3) {
//                    //Report
//                    showReportDialog();
//                } else if (actionId == 4) {
//                    //History
//                    radioGroup.setVisibility(View.VISIBLE);
//                    expListView.setVisibility(View.GONE);
//                    vehicle_info_layout.setVisibility(View.GONE);
//                    id_histroy_vehicleid.setText(selected_vehicleid);
//                } else if (actionId == 5) {
//                    //Live Track
//                    expListView.setVisibility(View.GONE);
//                    track = true;
//                    vehicletracking(selected_vehicleid);
//                } else {
//                    Toast.makeText(getApplicationContext(), " Please select other options", Toast.LENGTH_SHORT).show();
//                }
//            }
//        };
//        UtilManager.quickActionSetUp(alActionItem, 1, mOnActionItemClickListener);
//    }
//
//    private void showLockUnlockDialog() {
//        AlertDialog.Builder builder = new AlertDialog.Builder(NavigationActivity.this);
//        builder.setTitle(getResources().getString(R.string.app_name));
//        builder.setMessage(strSelectedVehicle).setCancelable(true).setPositiveButton("Lock", new DialogInterface.OnClickListener() {
//            public void onClick(DialogInterface dialog, int id) {
//                dialog.cancel();
//                new FetchLockData().execute("");
//            }
//        }).setNegativeButton("UnLock", new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int i) {
//                dialog.cancel();
//                new FetchUnLockData().execute("");
//            }
//        });
//        AlertDialog alert = builder.create();
//        alert.show();
//    }
//
//    /**
//     * Map Related code
//     */
//    private boolean showVehicleInfoDialog(JSONObject getcurrentmarker, boolean isFromList) {
//        if (isFromList) {
//            marker_info_dialog = new Dialog(this, android.R.style.Theme_Light_NoTitleBar_Fullscreen);
//            marker_info_dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
//        } else {
//            marker_info_dialog = new Dialog(this);
//        }
//        marker_info_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        marker_info_dialog.setContentView(R.layout.custominfolayout);
//        marker_info_dialog.setCancelable(false);
//        ImageView id_back_icon = (ImageView) marker_info_dialog.findViewById(R.id.id_back_icon);
//        TextView id_vehicleid = (TextView) marker_info_dialog.findViewById(R.id.id_vehicleid);
//        ImageView id_vehicle_info_icon = (ImageView) marker_info_dialog.findViewById(R.id.id_vehicle_info_icon);
//        Button id_vehicle_track = (Button) marker_info_dialog.findViewById(R.id.id_vehicle_track);
//        Button vehicle_history = (Button) marker_info_dialog.findViewById(R.id.id_vehicle_history_btn);
//        Button id_close = (Button) marker_info_dialog.findViewById(R.id.id_close);
//        TextView id_view_address = (TextView) marker_info_dialog.findViewById(R.id.id_view_address);
//        TextView id_view_speed = (TextView) marker_info_dialog.findViewById(R.id.id_view_speed);
//        TextView id_view_lastseentime = (TextView) marker_info_dialog.findViewById(R.id.id_view_lastseentime);
//        TextView id_speed_limit_speed = (TextView) marker_info_dialog.findViewById(R.id.id_view_spped_lastactive);
//        TextView id_ignition_speed = (TextView) marker_info_dialog.findViewById(R.id.id_ignition_speed);
//        TextView id_lastseentime_labelid = (TextView) marker_info_dialog.findViewById(R.id.id_lastseentime_labelid);
//        TextView id_speed_labelid = (TextView) marker_info_dialog.findViewById(R.id.id_speed_labelid);
//        TextView id_ignition_labelid = (TextView) marker_info_dialog.findViewById(R.id.id_ignition_labelid);
//        TextView id_address_text_labelid = (TextView) marker_info_dialog.findViewById(R.id.id_address_text_labelid);
//        TextView id_lastseentime_label_separator = (TextView) marker_info_dialog.findViewById(R.id.id_lastseentime_label_separator);
//        TextView id_speed_labelid_separator = (TextView) marker_info_dialog.findViewById(R.id.id_speed_labelid_separator);
//        RelativeLayout id_time_label_layout = (RelativeLayout) marker_info_dialog.findViewById(R.id.id_time_label_layout);
//        TextView id_time_labelid = (TextView) marker_info_dialog.findViewById(R.id.id_time_labelid);
//        TextView id_time = (TextView) marker_info_dialog.findViewById(R.id.id_time);
//        final ImageView id_view_next = (ImageView) marker_info_dialog.findViewById(R.id.id_vehicle_next);
//        final ImageView id_view_previous = (ImageView) marker_info_dialog.findViewById(R.id.id_vehicle_previous);
//        id_time_labelid.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
//        id_time.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
//        id_lastseentime_label_separator.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
//        id_speed_labelid_separator.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
//        id_lastseentime_labelid.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
//        id_speed_labelid.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
//        id_ignition_labelid.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
//        id_address_text_labelid.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
//        id_vehicleid.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
//        id_vehicle_track.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
//        vehicle_history.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
//        id_view_address.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
//        id_view_speed.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
//        id_view_lastseentime.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
//        id_speed_limit_speed.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
//        id_ignition_speed.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
//        try {
//            double latitude = Double.parseDouble(getcurrentmarker.getString("latitude"));
//            double longitude = Double.parseDouble(getcurrentmarker.getString("longitude"));
//            new GetAddressTask(id_view_address).execute(latitude, longitude, 1.0);
//            selected_vehicleid = getcurrentmarker.getString("vehicleId");
//            if (getcurrentmarker.getString("vehicleType").equals("Bus")) {
//                id_vehicle_info_icon.setImageResource(R.drawable.bus_east);
//                id_histroy_vehicleid.setText(getcurrentmarker.getString("vehicleId"));
//                id_history_vehicle_info_icon.setImageResource(R.drawable.bus_east);
//            } else if (getcurrentmarker.getString("vehicleType").equals("Car")) {
//                id_histroy_vehicleid.setText(getcurrentmarker.getString("vehicleId"));
//                id_history_vehicle_info_icon.setImageResource(R.drawable.car_east);
//                id_vehicle_info_icon.setImageResource(R.drawable.car_east);
//            } else if (getcurrentmarker.getString("vehicleType").equals("Truck")) {
//                id_histroy_vehicleid.setText(getcurrentmarker.getString("vehicleId"));
//                id_history_vehicle_info_icon.setImageResource(R.drawable.truck_east);
//                id_vehicle_info_icon.setImageResource(R.drawable.truck_east);
//            } else if (getcurrentmarker.getString("vehicleType").equals("Cycle")) {
//                id_histroy_vehicleid.setText(getcurrentmarker.getString("vehicleId"));
//                id_history_vehicle_info_icon.setImageResource(R.drawable.cycle_east);
//                id_vehicle_info_icon.setImageResource(R.drawable.cycle_east);
//            }
//            id_vehicleid.setText(getcurrentmarker.getString("vehicleId"));
//            try {
//                map_marker_vechile_position = child.indexOf(getcurrentmarker.getString("vehicleId"));
//                if (map_marker_vechile_position == -1) {
//                    child = (ArrayList<String>) childelements.get(0);
//                    map_marker_vechile_position = child.indexOf(getcurrentmarker.getString("vehicleId"));
//                }
//            } catch (Exception e) {
//                vehicle = (ArrayList<String>) childelements.get(0);
//                map_marker_vechile_position = vehicle.indexOf(getcurrentmarker.getString("vehicleId"));
//            }
//            position = map_marker_vechile_position;
//            id_view_speed.setText(Integer.toString(getcurrentmarker.getInt("speed")) + " KMS / HR");
//            id_view_lastseentime.setText(getcurrentmarker.getString("lastSeen"));
//            id_speed_limit_speed.setText("Speed limit is " + Integer.toString(getcurrentmarker.getInt("overSpeedLimit")) + " KMS / HR");
//            id_ignition_speed.setText(getcurrentmarker.getString("ignitionStatus"));
//            if (getcurrentmarker.getString("position").equalsIgnoreCase("P")) {
//                id_time_labelid.setText("Parked Time");
//                id_time.setText(cons.getVehicleTime(getcurrentmarker.getString("parkedTime")));
//            } else if (getcurrentmarker.getString("position").equalsIgnoreCase("M")) {
//                id_time_labelid.setText("Moving Time");
//                id_time.setText(cons.getVehicleTime(getcurrentmarker.getString("movingTime")));
//            } else if (getcurrentmarker.getString("position").equalsIgnoreCase("U")) {
//                id_time_labelid.setText("No Data Time");
//                id_time.setText(cons.getVehicleTime(getcurrentmarker.getString("noDataTime")));
//            } else if (getcurrentmarker.getString("position").equalsIgnoreCase("S")) {
//                id_time_labelid.setText("Idle Time");
//                id_time.setText(cons.getVehicleTime(getcurrentmarker.getString("idleTime")));
//            } else {
//                id_time_label_layout.setVisibility(View.GONE);
//            }
//            SharedPreferences.Editor editor = sp.edit();
//            editor.putString("current_vehicleID", getcurrentmarker.getString("vehicleId"));
//            editor.commit();
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//        try {
//            if (child.size() == 1) {
//                id_view_next.setVisibility(View.GONE);
//                id_view_previous.setVisibility(View.GONE);
//                id_next_vehicle.setVisibility(View.GONE);
//                id_previous_vehicle.setVisibility(View.GONE);
//            } else {
//                if (map_marker_vechile_position == child.size() - 1) {
//                    id_view_next.setVisibility(View.INVISIBLE);
//                    id_view_previous.setVisibility(View.VISIBLE);
//                    id_next_vehicle.setVisibility(View.INVISIBLE);
//                    id_previous_vehicle.setVisibility(View.VISIBLE);
//                } else if (map_marker_vechile_position == 0) {
//                    id_next_vehicle.setVisibility(View.VISIBLE);
//                    id_previous_vehicle.setVisibility(View.INVISIBLE);
//                    id_view_next.setVisibility(View.VISIBLE);
//                    id_view_previous.setVisibility(View.INVISIBLE);
//                } else {
//                    id_view_next.setVisibility(View.VISIBLE);
//                    id_view_previous.setVisibility(View.VISIBLE);
//                    id_next_vehicle.setVisibility(View.VISIBLE);
//                    id_previous_vehicle.setVisibility(View.VISIBLE);
//                }
//            }
//        } catch (Exception e) {
//            if (vehicle.size() == 1) {
//                id_next_vehicle.setVisibility(View.GONE);
//                id_previous_vehicle.setVisibility(View.GONE);
//                id_view_next.setVisibility(View.GONE);
//                id_view_previous.setVisibility(View.GONE);
//            } else {
//                if (map_marker_vechile_position == vehicle.size() - 1) {
//                    id_next_vehicle.setVisibility(View.INVISIBLE);
//                    id_previous_vehicle.setVisibility(View.VISIBLE);
//                    id_view_next.setVisibility(View.INVISIBLE);
//                    id_view_previous.setVisibility(View.VISIBLE);
//                } else if (map_marker_vechile_position == 0) {
//                    id_next_vehicle.setVisibility(View.VISIBLE);
//                    id_previous_vehicle.setVisibility(View.INVISIBLE);
//                    id_view_next.setVisibility(View.VISIBLE);
//                    id_view_previous.setVisibility(View.INVISIBLE);
//                } else {
//                    id_next_vehicle.setVisibility(View.VISIBLE);
//                    id_previous_vehicle.setVisibility(View.VISIBLE);
//                    id_view_next.setVisibility(View.VISIBLE);
//                    id_view_previous.setVisibility(View.VISIBLE);
//                }
//            }
//        }
//        id_view_next.setOnClickListener(new OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                map_marker_vechile_position += 1;
//                id_view_previous.setVisibility(View.VISIBLE);
//                marker_info_dialog.hide();
//                try {
//                    if (map_marker_vechile_position == child.size() - 1) {
//                        id_view_next.setVisibility(View.GONE);
//                    }
//                    vehicleSelection(child.get(map_marker_vechile_position));
//                } catch (Exception e) {
//                    if (map_marker_vechile_position == vehicle.size() - 1) {
//                        id_view_next.setVisibility(View.GONE);
//                    }
//                    vehicleSelection(vehicle.get(map_marker_vechile_position));
//                }
//            }
//        });
//        id_view_previous.setOnClickListener(new OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                map_marker_vechile_position -= 1;
//                marker_info_dialog.hide();
//                id_view_next.setVisibility(View.VISIBLE);
//                try {
//                    if (map_marker_vechile_position == 0) {
//                        id_view_previous.setVisibility(View.GONE);
//                    }
//                    vehicleSelection(child.get(map_marker_vechile_position));
//                } catch (Exception e) {
//                    if (map_marker_vechile_position == 0) {
//                        id_view_previous.setVisibility(View.GONE);
//                    }
//                    vehicleSelection(vehicle.get(map_marker_vechile_position));
//                }
//                //
//            }
//        });
//        id_vehicle_track.setOnClickListener(new OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                track = true;
//                vehicletracking(selected_vehicleid);
//            }
//        });
//        vehicle_history.setOnClickListener(new OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                marker_info_dialog.hide();
//                radioGroup.setVisibility(View.VISIBLE);
//                header_layout.setVisibility(View.GONE);
//                vehicle_info_layout.setVisibility(View.GONE);
//            }
//        });
//        id_close.setOnClickListener(new OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                marker_info_dialog.hide();
//                id_view_next.setVisibility(View.GONE);
//                id_view_previous.setVisibility(View.GONE);
//                header_layout.setVisibility(View.VISIBLE);
//                vehicle_info_layout.setVisibility(View.VISIBLE);
//            }
//        });
//        id_back_icon.setOnClickListener(new OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                marker_info_dialog.hide();
//                id_view_next.setVisibility(View.GONE);
//                id_view_previous.setVisibility(View.GONE);
//                header_layout.setVisibility(View.VISIBLE);
//            }
//        });
//        if (isFromList) {
//            vehicle_history.setVisibility(View.INVISIBLE);
//            id_vehicle_track.setVisibility(View.INVISIBLE);
//            id_back_icon.setVisibility(View.VISIBLE);
//            id_close.setVisibility(View.GONE);
//            id_view_next.setVisibility(View.INVISIBLE);
//            id_view_previous.setVisibility(View.INVISIBLE);
//        } else {
//            id_back_icon.setVisibility(View.GONE);
//            id_close.setVisibility(View.VISIBLE);
//        }
//        marker_info_dialog.show();
//        return true;
//    }
//
//    private void showReportDialog() {
//        marker_info_dialog = new Dialog(this, android.R.style.Theme_Light_NoTitleBar_Fullscreen);
//        marker_info_dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
//        marker_info_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        marker_info_dialog.setContentView(R.layout.vehicle_report);
//        marker_info_dialog.setCancelable(false);
//        /** UI **/
//        svReportOption = (ScrollView) marker_info_dialog.findViewById(R.id.svReportOption);
//        headerLayout = (RelativeLayout) marker_info_dialog.findViewById(R.id.headerLayout);
//        lvReportList = (ListView) marker_info_dialog.findViewById(R.id.lvReportList);
//        svReportOption.setVisibility(View.VISIBLE);
//        headerLayout.setVisibility(View.GONE);
//        lvReportList.setVisibility(View.GONE);
//        mReportListAdapter = new ReportListAdapter(NavigationActivity.this, null);
//        lvReportList.setAdapter(mReportListAdapter);
//        TextView tvTitle = (TextView) marker_info_dialog.findViewById(R.id.tvTitle);
//        tvTitle.setText("Reports(" + strSelectedVehicle + ")");
//        /** Date Time **/
//        final TextView tvFromDate = (TextView) marker_info_dialog.findViewById(R.id.report_from_date);
//        final TextView tvFromTime = (TextView) marker_info_dialog.findViewById(R.id.report_from_time);
//        final TextView tvToDate = (TextView) marker_info_dialog.findViewById(R.id.report_to_date);
//        final TextView tvToTime = (TextView) marker_info_dialog.findViewById(R.id.report_to_time);
//        tvFromDate.setText(cons.GetCurrentDate(Calendar.getInstance()));
//        tvToDate.setText(cons.GetCurrentDate(Calendar.getInstance()));
//        tvFromDate.setOnClickListener(new OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Calendar calNow = Calendar.getInstance();
//                new DatePickerDialog(NavigationActivity.this, new DatePickerDialog.OnDateSetListener() {
//                    // when dialog box is closed, below method will be called.
//                    @Override
//                    public void onDateSet(DatePicker view, int year, int month, int day) {
//                        tvFromDate.setText(cons.getDateFormat(cons.getCalendarDate(year, month, day, 0, 0)));
//                    }
//                }, calNow.get(Calendar.YEAR), calNow.get(Calendar.MONTH), calNow.get(Calendar.DAY_OF_MONTH)).show();
//            }
//        });
//        tvFromTime.setOnClickListener(new OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Calendar calNow = Calendar.getInstance();
//                new TimePickerDialog(NavigationActivity.this, new TimePickerDialog.OnTimeSetListener() {
//                    @Override
//                    public void onTimeSet(TimePicker view, int hour, int minute) {
//                        // TODO Auto-generated method stub
//                        tvFromTime.setText(new StringBuilder().append(pad(hour)).append(":").append(pad(minute)).append(":").append("00"));
//                    }
//                }, calNow.get(Calendar.HOUR_OF_DAY), calNow.get(Calendar.MINUTE), false).show();
//            }
//        });
//        tvToDate.setOnClickListener(new OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Calendar calNow = Calendar.getInstance();
//                new DatePickerDialog(NavigationActivity.this, new DatePickerDialog.OnDateSetListener() {
//                    // when dialog box is closed, below method will be called.
//                    @Override
//                    public void onDateSet(DatePicker view, int year, int month, int day) {
//                        tvToDate.setText(cons.getDateFormat(cons.getCalendarDate(year, month, day, 0, 0)));
//                    }
//                }, calNow.get(Calendar.YEAR), calNow.get(Calendar.MONTH), calNow.get(Calendar.DAY_OF_MONTH)).show();
//            }
//        });
//        tvToTime.setOnClickListener(new OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Calendar calNow = Calendar.getInstance();
//                new TimePickerDialog(NavigationActivity.this, new TimePickerDialog.OnTimeSetListener() {
//                    @Override
//                    public void onTimeSet(TimePicker view, int hour, int minute) {
//                        // TODO Auto-generated method stub
//                        tvToTime.setText(new StringBuilder().append(pad(hour)).append(":").append(pad(minute)).append(":").append("00"));
//                    }
//                }, calNow.get(Calendar.HOUR_OF_DAY), calNow.get(Calendar.MINUTE), false).show();
//            }
//        });
//        /** Interval **/
//        final EditText etInterval = (EditText) marker_info_dialog.findViewById(R.id.etInterval);
//        /** Stoppage **/
//        final EditText etStoppage = (EditText) marker_info_dialog.findViewById(R.id.etStoppage);
//        final CheckBox cbStoppage = (CheckBox) marker_info_dialog.findViewById(R.id.cbStoppage);
//        cbStoppage.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
//                etStoppage.setEnabled(isChecked);
//            }
//        });
//        /** Idle **/
//        final EditText etIdle = (EditText) marker_info_dialog.findViewById(R.id.etIdle);
//        final CheckBox cbIdle = (CheckBox) marker_info_dialog.findViewById(R.id.cbIdle);
//        cbIdle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
//                etIdle.setEnabled(isChecked);
//            }
//        });
//        /** NotReachable **/
//        final EditText etNotReachable = (EditText) marker_info_dialog.findViewById(R.id.etNotReachable);
//        final CheckBox cbNotReachable = (CheckBox) marker_info_dialog.findViewById(R.id.cbNotReachable);
//        cbNotReachable.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
//                etNotReachable.setEnabled(isChecked);
//            }
//        });
//        /** OverSpeed **/
//        final EditText etOverSpeed = (EditText) marker_info_dialog.findViewById(R.id.etOverSpeed);
//        final CheckBox cbOverSpeed = (CheckBox) marker_info_dialog.findViewById(R.id.cbOverSpeed);
//        cbOverSpeed.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
//                etOverSpeed.setEnabled(isChecked);
//            }
//        });
//        final CheckBox cbLocation = (CheckBox) marker_info_dialog.findViewById(R.id.cbLocation);
//        final CheckBox cbSite = (CheckBox) marker_info_dialog.findViewById(R.id.cbSite);
//        marker_info_dialog.findViewById(R.id.ivBack).setOnClickListener(new OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                marker_info_dialog.dismiss();
//            }
//        });
//        marker_info_dialog.findViewById(R.id.btnDone).setOnClickListener(new OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                String strData = "&fromDate=" + tvFromDate.getText().toString() + "&fromTime=" + tvFromTime.getText().toString() + ":00&toDate=" + tvToDate.getText().toString() + "&toTime=" + tvToTime.getText().toString() + ":00&stoppage=" + cbStoppage.isChecked() + "&stopMints=" + etStoppage.getText().toString() + "&idle=" + cbIdle.isChecked() + "&idleMints=" + etIdle.getText().toString() + "&notReachable=" + cbNotReachable.isChecked() + "&notReachableMints=" + etNotReachable.getText().toString() + "&overSpeed=" + cbOverSpeed.isChecked() + "&speed=" + etOverSpeed.getText().toString() + "&location=" + cbLocation.isChecked() + "&site=" + cbSite.isChecked();
//                new GetReportData().execute(strData);
//            }
//        });
//        marker_info_dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
//        marker_info_dialog.show();
//    }
//
//    private Drawable setVehicleImage(Drawable drawable, String vehicle_position) {
//        Log.d(TAG, "setVehicleImage() called with: " + "vehicle_position = [" + vehicle_position + "]");
//        if (vehicle_position.equalsIgnoreCase("P")) {
//            return setVehicleDrawable(drawable, getResources().getColor(R.color.black));
//        } else if (vehicle_position.equalsIgnoreCase("M")) {
//            return setVehicleDrawable(drawable, getResources().getColor(R.color.green));
//        } else if (vehicle_position.equalsIgnoreCase("U")) {
//            return setVehicleDrawable(drawable, getResources().getColor(R.color.red));
//        } else if (vehicle_position.equalsIgnoreCase("S")) {
//            return setVehicleDrawable(drawable, getResources().getColor(R.color.orange));
//        } else {
//            return setVehicleDrawable(drawable, getResources().getColor(R.color.grey));
//        }
//    }
//
//    private Drawable setVehicleDrawable(Drawable d, int color) {
//        Drawable wrappedDrawable = DrawableCompat.wrap(d);
//        DrawableCompat.setTint(wrappedDrawable, color);
//        return wrappedDrawable;
//    }
//
//    /**
//     * Map Related code
//     */
//    @Override
//    public void onInfoWindowClick(Marker marker) {
//    }
//
//    public void setupMap(JSONObject vehiclejson, double lat, double lng, String vehicleid) {
//        if (map != null) {
//            map.clear();
//        }
//
//        // Toast.makeText(getApplicationContext(),"Map setup called :::::",Toast.LENGTH_SHORT).show();
//
//        CameraPosition INIT = new CameraPosition.Builder().target(new LatLng(lat, lng)).zoom(vehicle_zoom_level).build();
//        map.animateCamera(CameraUpdateFactory.newCameraPosition(INIT));
//        View marker = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.custommarker, null);
//        ImageView id_custom_marker_icon = (ImageView) marker.findViewById(R.id.id_custom_marker_icon);
//        ImageView id_vehicle_in_marker = (ImageView) marker.findViewById(R.id.id_vehicle_in_marker);
//        try {
//            if (!vehiclejson.isNull("direction")) {
//                vehicle_direction_in_map(vehiclejson.getString("vehicleType"), vehiclejson.getString("direction"), id_vehicle_in_marker);
//            }
//            if (vehiclejson.getString("isOverSpeed").equalsIgnoreCase("Y")) {
//                id_custom_marker_icon.setImageResource(R.drawable.red_custom_marker_icon);
//            } else if (vehiclejson.getString("insideGeoFence").equalsIgnoreCase("Y")) {
//                id_custom_marker_icon.setImageResource(R.drawable.blue_custom_marker_icon);
//            } else if (vehiclejson.getString("position").equalsIgnoreCase("M")) {
//                id_custom_marker_icon.setImageResource(R.drawable.green_custom_marker_icon);
//            } else if (vehiclejson.getString("position").equalsIgnoreCase("P")) {
//                id_custom_marker_icon.setImageResource(R.drawable.grey_custom_marker_icon);
//            } else if (vehiclejson.getString("position").equalsIgnoreCase("S")) {
//                id_custom_marker_icon.setImageResource(R.drawable.orange_custom_marker_icon);
//            } else if (vehiclejson.getString("position").equalsIgnoreCase("N")) {
//                id_custom_marker_icon.setImageResource(R.drawable.orange_custom_marker_icon);
//            } else if (vehiclejson.getString("position").equalsIgnoreCase("U")) {
//                id_custom_marker_icon.setImageResource(R.drawable.grey_custom_marker_icon);
//            }
//        } catch (JSONException jo) {
//            jo.printStackTrace();
//        }
//        MarkerOptions markerOption = new MarkerOptions().position(new LatLng(lat, lng));
//        markerOption.title(vehicleid);
//        markerOption.icon(BitmapDescriptorFactory.fromBitmap(createDrawableFromView(NavigationActivity.this, marker)));
//        Marker currentMarker = map.addMarker(markerOption);
//        mMarkersHashMap.put(currentMarker, vehiclejson);
//    }
//
//    public void vehicletracking(String id) {
//        navigation_status = "tracking";
//        if (map != null) {
//            map.clear();
//        }
//        Toast.makeText(getApplicationContext(), "Tracking... Please wait a moment", Toast.LENGTH_LONG).show();
//        String par[] = {Const.API_URL + "mobile/getGeoFenceView?vehicleId=" + id + "&userId=" + username + "&macid=" + getStdTableValue("macid") + "&appid=" + getStdTableValue("appid")};
//        new GetPOIInformation().execute(par);
//        progressDialog = MyCustomProgressDialog.ctor(context);
//        progressDialog.show();
//        cancelVehicleTracking();
//        vehicletrackalarm = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
//        Intent intent = new Intent(getApplicationContext(), VehicleTrackingService.class);
//        vehicletrackpendingIntent = PendingIntent.getService(getApplicationContext(), sp.getInt("notif_id", 0), intent, 0);
//        vehicletrackalarm.setRepeating(AlarmManager.RTC_WAKEUP, 0, startTime, vehicletrackpendingIntent);
//        handler.postDelayed(runable, 3000);
//        try {
//            marker_info_dialog.hide();
//            search.setVisibility(View.GONE);
//        } catch (Exception e) {
//            handler.removeCallbacksAndMessages(null);
//        }
//    }
//
//    private String sendGet(String url, int connection_timeout) throws Exception {
//
//        //  Toast.makeText(NavigationActivity.this, "The url is :::"+url, Toast.LENGTH_SHORT).show();
//
//        URL obj = new URL(url);
//        HttpURLConnection con = (HttpURLConnection) obj.openConnection();
//        // optional default is GET
//        con.setRequestMethod("GET");
//        con.setConnectTimeout(connection_timeout);
//        //add request header
//        int responseCode = con.getResponseCode();
//        BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
//        String inputLine;
//        StringBuffer response = new StringBuffer();
//        while ((inputLine = in.readLine()) != null) {
//            response.append(inputLine);
//        }
//        in.close();
//        return response.toString();
//    }
//
//    public void updateVehicleLocations(String response_from_server, String req_type, String groupname, int groupPosition) {
//        try {
//            JSONArray parentarray = new JSONArray(response_from_server);
//            for (int i = 0; i < parentarray.length(); i++) {
//                String total_vehicles, online_vehicles, offlinevehicles;
//                final JSONObject parentobj = parentarray.getJSONObject(i);
//                JSONObject groupJSON = new JSONObject();
//                groupJSON.put("group", parentobj.getString("group"));
//                groupJSON.put("distance", parentobj.getString("distance"));
//                groupJSON.put("totalVehicles", parentobj.getString("totalVehicles"));
//                groupJSON.put("online", parentobj.getString("online"));
//                groupJSON.put("alerts", parentobj.getString("alerts"));
//                groupJSON.put("attention", parentobj.getString("attention"));
//                groupJSON.put("supportDetails", parentobj.getString("supportDetails"));
//                if (req_type.equals("getall")) {
//                    if (i == 0) {
//                        lat = Double.parseDouble(parentobj.getString("latitude"));
//                        lng = Double.parseDouble(parentobj.getString("longitude"));
//                        snippet = parentobj.getString("group");
//                        runOnUiThread(new Runnable() {
//                            @Override
//                            public void run() {
//                                try {
//                                    id_all_vehicles_value.setText(parentobj.getString("totalVehicles"));
//                                    id_online_vehicles_value.setText(parentobj.getString("online"));
//                                    id_offline_vehicles_value.setText(parentobj.getString("attention"));
//                                    List<String> support_list_items = Arrays.asList(parentobj.getString("supportDetails").split(","));
//                                    if (!support_list_items.isEmpty()) {
//                                        id_settings_support_address_line1.setText(support_list_items.get(0));
//                                        id_settings_support_address_line2.setText(support_list_items.get(1));
//                                        id_settings_support_mobilenumber.setText(support_list_items.get(2));
//                                    }
//                                } catch (Exception je) {
//                                    je.printStackTrace();
//                                }
//                            }
//                        });
//                    }
//                } else if (req_type.equals("getgroup")) {
//                    if (groupname.equals(parentobj.getString("group"))) {
//                        lat = Double.parseDouble(parentobj.getString("latitude"));
//                        lng = Double.parseDouble(parentobj.getString("longitude"));
//                        snippet = parentobj.getString("group");
//                        runOnUiThread(new Runnable() {
//                            @Override
//                            public void run() {
//                                try {
//                                    id_all_vehicles_value.setText(parentobj.getString("totalVehicles"));
//                                    id_online_vehicles_value.setText(parentobj.getString("online"));
//                                    id_offline_vehicles_value.setText(parentobj.getString("attention"));
//                                    List<String> support_list_items = Arrays.asList(parentobj.getString("supportDetails").split(","));
//                                    if (!support_list_items.isEmpty()) {
//                                        id_settings_support_address_line1.setText(support_list_items.get(0));
//                                        id_settings_support_address_line2.setText(support_list_items.get(1));
//                                        id_settings_support_mobilenumber.setText(support_list_items.get(2));
//                                    }
//                                } catch (JSONException je) {
//                                    je.printStackTrace();
//                                }
//                            }
//                        });
//                    }
//                }
//                if (parentobj.isNull("vehicleLocations")) {
//                    if (req_type.equals("getall")) {
//                        dbhelper.insert_vehicle_details(groupJSON.toString(1), "null", parentobj.getString("group"));
//                    } else if (req_type.equals("getgroup")) {
//                        dbhelper.update_vehicle_details(groupJSON.toString(1), "null", parentobj.getString("group"));
//                    }
//                } else {
//                    if (req_type.equals("getall")) {
//                        dbhelper.insert_vehicle_details(groupJSON.toString(1), parentobj.getJSONArray("vehicleLocations").toString(1), parentobj.getString("group"));
//                    } else if (req_type.equals("getgroup")) {
//                        dbhelper.update_vehicle_details(groupJSON.toString(1), parentobj.getJSONArray("vehicleLocations").toString(1), parentobj.getString("group"));
//                    }
//                }
//            }
//        } catch (Exception je) {
//            je.printStackTrace();
//        }
//    }
//
//    public void updateVehicleList(Cursor vehicle_info, String result, int current_group_pos, String status, String vehicle_pull_type) {
//        if (vehicle_info.moveToFirst()) {
//            String vehicleStatus = status;
//            if (!groupList.isEmpty()) {
//                groupList.clear();
//                childelements.clear();
//                originalchildelements.clear();
//                jsonchildelements.clear();
//            }
//            if (map != null) {
//                map.clear();
//            }
//            for (int i = 0; i < vehicle_info.getCount(); i++) {
//                try {
//                    JSONObject groupJson = new JSONObject(vehicle_info.getString(0));
//                    groupList.add(groupJson.getString("group"));
//                } catch (JSONException e) {
//                    // TODO Auto-generated catch block
//                    e.printStackTrace();
//                }
//                if (!vehicle_info.getString(1).equals("null")) {
//                    try {
//                        JSONArray vehiclearr = new JSONArray(vehicle_info.getString(1));
//                        ArrayList<String> child = new ArrayList<String>();
//                        ArrayList<String> jsonchild = new ArrayList<String>();
//                        CameraPosition INIT = new CameraPosition.Builder().target(new LatLng(lat, lng)).zoom(group_zoom_level).build();
//                        map.animateCamera(CameraUpdateFactory.newCameraPosition(INIT));
//                        if (!child.isEmpty()) {
//                            child.clear();
//                            jsonchild.clear();
//                        }
//                        for (int vehicle = 0; vehicle < vehiclearr.length(); vehicle++) {
//                            final String vehicleID, vehicleType;
//                            JSONObject vehiclejson = vehiclearr.getJSONObject(vehicle);
//                            if (vehicleStatus.equals("all")) {
//                                jsonchild.add(vehiclejson.toString());
//                                child.add(vehiclejson.getString("vehicleId"));
//                                vehicleID = vehiclejson.getString("vehicleId");
//                                vehicleType = vehiclejson.getString("vehicleType");
//                                if (vehicle_pull_type.equals("homepage")) {
//                                    View marker = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.custommarker, null);
//                                    ImageView id_custom_marker_icon = (ImageView) marker.findViewById(R.id.id_custom_marker_icon);
//                                    ImageView id_vehicle_in_marker = (ImageView) marker.findViewById(R.id.id_vehicle_in_marker);
//                                    if (!vehiclejson.isNull("direction")) {
//                                        vehicle_direction_in_map(vehiclejson.getString("vehicleType"), vehiclejson.getString("direction"), id_vehicle_in_marker);
//                                    }
//                                    if (vehiclejson.getString("isOverSpeed").equalsIgnoreCase("Y")) {
//                                        id_custom_marker_icon.setImageResource(R.drawable.red_custom_marker_icon);
//                                    } else if (vehiclejson.getString("insideGeoFence").equalsIgnoreCase("Y")) {
//                                        id_custom_marker_icon.setImageResource(R.drawable.blue_custom_marker_icon);
//                                    } else if (vehiclejson.getString("position").equalsIgnoreCase("M")) {
//                                        id_custom_marker_icon.setImageResource(R.drawable.green_custom_marker_icon);
//                                    } else if (vehiclejson.getString("position").equalsIgnoreCase("P")) {
//                                        id_custom_marker_icon.setImageResource(R.drawable.grey_custom_marker_icon);
//                                    } else if (vehiclejson.getString("position").equalsIgnoreCase("S")) {
//                                        id_custom_marker_icon.setImageResource(R.drawable.orange_custom_marker_icon);
//                                    } else if (vehiclejson.getString("position").equalsIgnoreCase("N")) {
//                                        id_custom_marker_icon.setImageResource(R.drawable.orange_custom_marker_icon);
//                                    } else if (vehiclejson.getString("position").equalsIgnoreCase("U")) {
//                                        id_custom_marker_icon.setImageResource(R.drawable.grey_custom_marker_icon);
//                                    }
//                                    MarkerOptions markerOption = new MarkerOptions().position(new LatLng(Double.parseDouble(vehiclejson.getString("latitude")), Double.parseDouble(vehiclejson.getString("longitude"))));
//                                    markerOption.title(vehiclejson.getString("vehicleId"));
//                                    markerOption.icon(BitmapDescriptorFactory.fromBitmap(createDrawableFromView(NavigationActivity.this, marker)));
//                                    Marker currentMarker = map.addMarker(markerOption);
//                                    mMarkersHashMap.put(currentMarker, vehiclejson);
//                                }
//                            } else if (vehiclejson.getString("status").equals(vehicleStatus)) {
//                                jsonchild.add(vehiclejson.toString());
//                                child.add(vehiclejson.getString("vehicleId"));
//                                vehicleID = vehiclejson.getString("vehicleId");
//                                vehicleType = vehiclejson.getString("vehicleType");
//                                if (vehicle_pull_type.equals("homepage")) {
//                                    View marker = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.custommarker, null);
//                                    ImageView id_custom_marker_icon = (ImageView) marker.findViewById(R.id.id_custom_marker_icon);
//                                    ImageView id_vehicle_in_marker = (ImageView) marker.findViewById(R.id.id_vehicle_in_marker);
//                                    if (!vehiclejson.isNull("direction")) {
//                                        vehicle_direction_in_map(vehiclejson.getString("vehicleType"), vehiclejson.getString("direction"), id_vehicle_in_marker);
//                                    }
//                                    if (vehiclejson.getString("isOverSpeed").equalsIgnoreCase("Y")) {
//                                        id_custom_marker_icon.setImageResource(R.drawable.red_custom_marker_icon);
//                                    } else if (vehiclejson.getString("insideGeoFence").equalsIgnoreCase("Y")) {
//                                        id_custom_marker_icon.setImageResource(R.drawable.blue_custom_marker_icon);
//                                    } else if (vehiclejson.getString("position").equalsIgnoreCase("M")) {
//                                        id_custom_marker_icon.setImageResource(R.drawable.green_custom_marker_icon);
//                                    } else if (vehiclejson.getString("position").equalsIgnoreCase("P")) {
//                                        id_custom_marker_icon.setImageResource(R.drawable.grey_custom_marker_icon);
//                                    } else if (vehiclejson.getString("position").equalsIgnoreCase("S")) {
//                                        id_custom_marker_icon.setImageResource(R.drawable.orange_custom_marker_icon);
//                                    } else if (vehiclejson.getString("position").equalsIgnoreCase("N")) {
//                                        id_custom_marker_icon.setImageResource(R.drawable.orange_custom_marker_icon);
//                                    } else if (vehiclejson.getString("position").equalsIgnoreCase("U")) {
//                                        id_custom_marker_icon.setImageResource(R.drawable.grey_custom_marker_icon);
//                                    }
//                                    MarkerOptions markerOption = new MarkerOptions().position(new LatLng(Double.parseDouble(vehiclejson.getString("latitude")), Double.parseDouble(vehiclejson.getString("longitude"))));
//                                    markerOption.title(vehiclejson.getString("vehicleId"));
//                                    markerOption.icon(BitmapDescriptorFactory.fromBitmap(createDrawableFromView(NavigationActivity.this, marker)));
//                                    Marker currentMarker = map.addMarker(markerOption);
//                                    mMarkersHashMap.put(currentMarker, vehiclejson);
//                                }
//                            }
//                            childelements.add(child);
//                            originalchildelements.add(child);
//                            jsonchildelements.add(jsonchild);
//                        }
//                    } catch (Exception e) {
//                        // TODO Auto-generated catch block
//                        e.printStackTrace();
//                    }
//                }
//                vehicle_info.moveToNext();
//            }
//            adapter = new MyExpandableAdapter(groupList, childelements);
//            adapter.setInflater((LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE), NavigationActivity.this);
//            expListView.setAdapter(adapter);
//            expListView.expandGroup(current_group_pos);
//            adapter.notifyDataSetChanged();
//        }
//    }
//
//    public void drawPoiPoints(String poi_data) {
//        if (poi_data != null && poi_data.length() != 0) {
//            try {
//                JSONObject parentobject = new JSONObject(poi_data);
//                if (!parentobject.isNull("geoFence")) {
//                    int vehicles_history_length;
//                    final ArrayList<LatLng> points = new ArrayList<>();
//                    JSONArray vehiclearray = parentobject.getJSONArray("geoFence");
//                    vehicles_history_length = vehiclearray.length();
//                    for (int z = 0; z < vehicles_history_length; z++) {
//                        if (!vehiclearray.get(z).equals(null)) {
//                            ParsingClass parse = new ParsingClass();
//                            parse.getPoiInformation(vehiclearray.getJSONObject(z).toString());
//                            double lat = parse.getpoiLatitude();
//                            double lng = parse.getpoiLongitude();
//                            LatLng position = new LatLng(lat, lng);
//                            points.add(position);
//                            View marker = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.custompoimarker, null);
//                            MarkerOptions markerOption = new MarkerOptions().position(new LatLng(lat, lng));
//                            markerOption.icon(BitmapDescriptorFactory.fromBitmap(createDrawableFromView(NavigationActivity.this, marker)));
//                            Marker currentMarker = map.addMarker(markerOption);
//                        }
//                    }
//                }
//            } catch (JSONException je) {
//                je.printStackTrace();
//            }
//        }
//    }
//
//    public void historyPlayBack(final String result, final ProgressDialog dialog) {
//        runOnUiThread(new Runnable() {
//            @Override
//            public void run() {
//                String totalrows = null;
//                if (result != null && result.length() != 0) {
//                    if (map != null) {
//                        map.clear();
//                    }
//                    vehicle_info_layout.setVisibility(View.GONE);
//                    id_refresh_tracking_icon.setVisibility(View.GONE);
//                    id_refresh_countdowntimer.setVisibility(View.GONE);
//                    history_detail_layout.setVisibility(View.VISIBLE);
//                    id_history_show_min_layout.setVisibility(View.GONE);
//                    pause_icon.setVisibility(View.VISIBLE);
//                    play_icon.setVisibility(View.GONE);
//                    header_layout.setVisibility(View.GONE);
//                    if (zoom_location_marker != null) {
//                        zoom_location_marker.clear();
//                    }
//                    try {
//                        ParsingClass parsingClass = new ParsingClass();
//                        parsingClass.historyParse(result);
//                        int vehicles_history_length;
//                        points = new ArrayList<LatLng>();
//                        speedlist = new ArrayList<String>();
//                        datetimelist = new ArrayList<String>();
//                        distancelist = new ArrayList<String>();
//                        if (!points.isEmpty()) {
//                            points.clear();
//                            //  directionlist.clear();
//                            speedlist.clear();
//                            datetimelist.clear();
//                            distancelist.clear();
//                        }
//                        String vehicle_short_name;
//                        PolylineOptions options = new PolylineOptions().width(8);
//                        PolylineOptions overspeed_options = new PolylineOptions().width(4).geodesic(true);
//                        vehicle_short_name = parsingClass.getshortname();
//                        id_show_history_vehicleid.setText((parsingClass.getVehicleId().equals("null") ? "No Data" : parsingClass.getVehicleId()));
//                        history_distance_covered_value.setText((parsingClass.gettripDistance().equals("null") ? "No Data" : parsingClass.gettripDistance() + " KMS"));
//                        histroy_start_time_value.setText((parsingClass.getfromDateTime().equals("null") ? "No Data" : parsingClass.getfromDateTime()));
//                        history_endtime_value.setText((parsingClass.gettoDateTime().equals("null") ? "No Data" : parsingClass.gettoDateTime()));
//                        totalrows = parsingClass.gettotalRows();
//                        history_timetaken_value.setText(cons.getVehicleTime(parsingClass.gettotalRunningTime()));
//                        /** Parsing all the history data*/
//                        try {
//                            JsonFactory f = new MappingJsonFactory();
//                            JsonParser historyjsonParser = f.createJsonParser(result);
//                            JsonToken current;
//                            current = historyjsonParser.nextToken();
//                            if (current != JsonToken.START_OBJECT) {
//                                return;
//                            }
//                            while (historyjsonParser.nextToken() != JsonToken.END_OBJECT) {
//                                String fieldName = historyjsonParser.getCurrentName();
//                                if (fieldName.equals("history4Mobile")) {
//                                    current = historyjsonParser.nextToken();
//                                    if (current == JsonToken.START_ARRAY) {
//                                        int current_node_index = 0;
//                                        while (historyjsonParser.nextToken() != JsonToken.END_ARRAY) {
//                                            // read the record into a tree model,
//                                            // this moves the parsing position to the end of it
//                                            JsonNode node = historyjsonParser.readValueAsTree();
//                                            // And now we have random access to everything in the object
//                                            if (!node.isNull()) {
//                                                parsingClass.historyobjectparsing(node);
//                                                double lat = parsingClass.getCurr_lat();
//                                                double lng = parsingClass.getCurr_long();
//                                                LatLng position = new LatLng(lat, lng);
//                                                points.add(position);
//                                                MarkerOptions markerOption = new MarkerOptions().position(position);
//                                                options.add(position);
//                                                options.color(getResources().getColor(R.color.history_polyline_color));
//                                                speedlist.add(parsingClass.getSpeed());
//                                                datetimelist.add(parsingClass.getlastseen());
//                                                distancelist.add(parsingClass.getDistanCovered());
//                                                View marker = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.custommarker, null);
//                                                ImageView id_history_marker_icon = (ImageView) marker.findViewById(R.id.id_history_marker_icon);
//                                                ImageView id_custom_marker_icon = (ImageView) marker.findViewById(R.id.id_custom_marker_icon);
//                                                ImageView id_vehicle_in_marker = (ImageView) marker.findViewById(R.id.id_vehicle_in_marker);
//                                                id_history_marker_icon.setVisibility(View.VISIBLE);
//                                                id_custom_marker_icon.setVisibility(View.GONE);
//                                                id_vehicle_in_marker.setVisibility(View.GONE);
//                                                Marker currentMarker = null;
//                                                if (current_node_index == 0) {
//                                                    id_history_marker_icon.setImageResource(R.drawable.startflag);
//                                                    markerOption.icon(BitmapDescriptorFactory.fromBitmap(createDrawableFromView(NavigationActivity.this, marker)));
//                                                    currentMarker = map.addMarker(markerOption);
//                                                    parker_mMarkersHashMap.put(currentMarker, cons.getHistoryTimefromserver(parsingClass.getlastseen()) + "," + "-" + "," + Double.toString(parsingClass.getCurr_lat()) + "," + Double.toString(parsingClass.getCurr_long()) + "," + "start");
//                                                    new GetAddressTask(history_start_location_value).execute(parsingClass.getCurr_lat(), parsingClass.getCurr_long(), 2.0);
//                                                } else if (current_node_index == Integer.parseInt(totalrows) - 1) {
//                                                    id_history_marker_icon.setImageResource(R.drawable.endflag);
//                                                    markerOption.icon(BitmapDescriptorFactory.fromBitmap(createDrawableFromView(NavigationActivity.this, marker)));
//                                                    currentMarker = map.addMarker(markerOption);
//                                                    parker_mMarkersHashMap.put(currentMarker, cons.getHistoryTimefromserver(parsingClass.getlastseen()) + "," + "-" + "," + Double.toString(parsingClass.getCurr_lat()) + "," + Double.toString(parsingClass.getCurr_long()) + "," + "end");
//                                                    new GetAddressTask(history_end_location_value).execute(parsingClass.getCurr_lat(), parsingClass.getCurr_long(), 3.0);
//                                                } else if (parsingClass.getPosition().equalsIgnoreCase("P")) {
//                                                    id_history_marker_icon.setImageResource(R.drawable.id_parker_icon);
//                                                    markerOption.icon(BitmapDescriptorFactory.fromBitmap(createDrawableFromView(NavigationActivity.this, marker)));
//                                                    currentMarker = map.addMarker(markerOption);
//                                                    parker_mMarkersHashMap.put(currentMarker, cons.getHistoryTimefromserver(parsingClass.getlastseen()) + "," + parsingClass.getparked_time() + "," + Double.toString(parsingClass.getCurr_lat()) + "," + Double.toString(parsingClass.getCurr_long()) + "," + "parked");
//                                                } else {
//                                                    markerOption.alpha(0);
//                                                    currentMarker = map.addMarker(markerOption);
//                                                    currentMarker.setVisible(false);
//                                                    parker_mMarkersHashMap.put(currentMarker, cons.getHistoryTimefromserver(parsingClass.getlastseen()) + "," + "-" + "," + "-" + "," + "-" + "," + "idle");
//                                                }
//                                                zoom_location_marker.add(currentMarker);
//                                            }
//                                            current_node_index++;
//                                        }
//                                    }
//                                }
//                            }
//                        } catch (IOException e) {
//                            e.printStackTrace();
//                            if (dialog.isShowing()) {
//                                dialog.dismiss();
//                            }
//                        }
//                        /** Parsing all the history data*/
//                        map.addPolyline(options);
//                        //progressDialog.dismiss();
//                        if (!zoom_location_marker.isEmpty()) {
//                            LatLngBounds.Builder builder = new LatLngBounds.Builder();
//                            for (int marker = 0; marker < zoom_location_marker.size(); marker++) {
//                                builder.include(zoom_location_marker.get(marker).getPosition());
//                            }
//                            LatLngBounds bounds = builder.build();
//                            int padding = 100; // offset from edges of the map in pixels
//                            CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);
//                            map.animateCamera(cu);
//                        }
//                        pause = false;
//                        if (points.size() > 1) {
//                            history_seekbar.setMax(points.size());
//                            replay_icon.setVisibility(View.GONE);
//                            speed_seekbar.setVisibility(View.GONE);
//                            history_seekbar.setVisibility(View.VISIBLE);
//                        } else {
//                            history_seekbar.setMax(points.size());
//                            replay_icon.setVisibility(View.GONE);
//                            play_icon.setVisibility(View.GONE);
//                            pause_icon.setVisibility(View.GONE);
//                            speed_seekbar.setVisibility(View.GONE);
//                            history_seekbar.setVisibility(View.GONE);
//                        }
//                        backButtonPressed = false;
//                        speed_seekbar.setProgress(50);
//                        marker_loop(0);
//                    } catch (Exception je) {
//                        je.printStackTrace();
//                    } finally {
//                        if (dialog.isShowing()) {
//                            dialog.dismiss();
//                        }
//                    }
//                } else {
//                    if (dialog.isShowing()) {
//                        dialog.dismiss();
//                    }
//                    Toast.makeText(getApplicationContext(), "No History Data Available for the Selected Intervals", Toast.LENGTH_SHORT).show();
//                }
//            }
//        });
//    }
//    /**AsyncTask Background API Calls*/
//    /**
//     * Public Methods
//     */
//    public void marker_loop(int loop) {
//        a = loop;
//        int value = 0;
//        handler1 = new Handler();
//        for (a = loop; a < points.size(); a++) {
//            loopvalue = a;
//            show_marker(a, value);
//            value++;
//        }
//    }
//
//    public void show_marker(final int loop, int value) {
//        final int p = loop;
//        final View marker = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.custommarker, null);
//        final LatLng getpoints = points.get(loop);
//        final String speed = speedlist.get(loop);
//        final String datetim = datetimelist.get(loop);
//        final String distance = distancelist.get(loop);
//        try {
//            handler1.postDelayed(runnable1 = new Runnable() {
//                @Override
//                public void run() {
//                    if (pause) {
//                        handler1.removeCallbacksAndMessages(null);
//                        if (!backButtonPressed && !isSpeedChanged) {
//                            backButtonPressed = false;
//                            current_address__dialog = new Dialog(context);
//                            current_address__dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//                            current_address__dialog.setContentView(R.layout.custom_marker_dialog);
//                            current_address__dialog.setCancelable(true);
//                            TextView id_historyplayback_parkedtime_text_labelid = (TextView) current_address__dialog.findViewById(R.id.id_historyplayback_parkedtime_text_labelid);
//                            TextView id_view_parkedtime = (TextView) current_address__dialog.findViewById(R.id.id_view_parkedtime);
//                            TextView id_historyplayback_parkedduration_text_labelid = (TextView) current_address__dialog.findViewById(R.id.id_historyplayback_parkedduration_text_labelid);
//                            TextView id_view_parkedduration = (TextView) current_address__dialog.findViewById(R.id.id_view_parkedduration);
//                            TextView id_historyplayback_addres_text_labelid = (TextView) current_address__dialog.findViewById(R.id.id_historyplayback_addres_text_labelid);
//                            TextView id_view_address = (TextView) current_address__dialog.findViewById(R.id.id_view_address);
//                            id_historyplayback_parkedtime_text_labelid.setVisibility(View.GONE);
//                            id_view_parkedtime.setVisibility(View.GONE);
//                            id_historyplayback_parkedduration_text_labelid.setVisibility(View.GONE);
//                            id_view_parkedduration.setVisibility(View.GONE);
//                            id_historyplayback_addres_text_labelid.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
//                            id_view_address.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
//                            new GetAddressTask(id_view_address).execute(getpoints.latitude, getpoints.longitude, 6.0);
//                            current_address__dialog.show();
//                        }
//                        return;
//                    } else {
//                        if (seekbarchanged) {
//                            seekbarchanged = false;
//                            handler1.removeCallbacksAndMessages(null);
//                            return;
//                        } else {
//                            history_seekbar.setProgress(loop);
//                            id_history_speed_info_value.setText(speed);
//                            id_history_distance_value.setText(distance);
//                            id_history_direction_value.setText(cons.getTimefromserver(datetim));
//                            history_vehicle_info_layout.setVisibility(View.VISIBLE);
//                            if (current_users_location_point.isEmpty()) {
//                                CameraPosition INIT = new CameraPosition.Builder().target(getpoints).zoom(history_zoom_level).build();
//                                Marker ad = null;
//                                ad = map.addMarker(new MarkerOptions().position(getpoints).title("").icon(BitmapDescriptorFactory.fromResource(R.drawable.playback_nav_icon)));
//                                current_users_location_point.add(ad);
//                            } else {
//                                for (int m = 0; m < current_users_location_point.size(); m++) {
//                                    Marker removemarker = current_users_location_point.get(m);
//                                    removemarker.remove();
//                                }
//                                current_users_location_point.clear();
//                                if (p == points.size() - 1) {
//                                    speed_seekbar.setVisibility(View.GONE);
//                                    history_seekbar.setVisibility(View.GONE);
//                                    replay_icon.setVisibility(View.VISIBLE);
//                                    id_history_show_min_layout.setVisibility(View.VISIBLE);
//                                    history_detail_layout.setVisibility(View.VISIBLE);
//                                    pause_icon.setVisibility(View.GONE);
//                                }
//                                CameraPosition INIT = new CameraPosition.Builder().target(getpoints).zoom(history_zoom_level).build();
//                                Marker ad = null;
//                                ad = map.addMarker(new MarkerOptions().position(getpoints).title("").icon(BitmapDescriptorFactory.fromResource(R.drawable.playback_nav_icon)));
//                                current_users_location_point.add(ad);
//                            }
//                        }
//                    }
//                }
//            }, (iLoopDelay * 10) * value);
//        } catch (Exception e) {
//        }
//    }
//
//    public void vehicle_direction_in_map(String vehicle_type, String vehicle_direction, ImageView id_vehicle_in_marker) {
//        //Log.i("vehicle_direction", vehicle_direction);
//        if (vehicle_type.equals("Bus")) {
//            if (vehicle_direction.equals("N")) {
//                id_vehicle_in_marker.setImageResource(R.drawable.bus_north);
//            } else if (vehicle_direction.equals("S")) {
//                id_vehicle_in_marker.setImageResource(R.drawable.bus_south);
//            } else if (vehicle_direction.equals("E")) {
//                id_vehicle_in_marker.setImageResource(R.drawable.bus_east);
//            } else if (vehicle_direction.equals("W")) {
//                id_vehicle_in_marker.setImageResource(R.drawable.bus_west);
//            } else if (vehicle_direction.equals("NE")) {
//                id_vehicle_in_marker.setImageResource(R.drawable.bus_ne);
//            } else if (vehicle_direction.equals("NW")) {
//                id_vehicle_in_marker.setImageResource(R.drawable.bus_nw);
//            } else if (vehicle_direction.equals("SE")) {
//                id_vehicle_in_marker.setImageResource(R.drawable.bus_se);
//            } else if (vehicle_direction.equals("SW")) {
//                id_vehicle_in_marker.setImageResource(R.drawable.bus_sw);
//            } else if (vehicle_direction.equals("null")) {
//                id_vehicle_in_marker.setImageResource(R.drawable.bus_east);
//            }
//        } else if (vehicle_type.equals("Car")) {
//            if (vehicle_direction.equals("N")) {
//                id_vehicle_in_marker.setImageResource(R.drawable.car_north);
//            } else if (vehicle_direction.equals("S")) {
//                id_vehicle_in_marker.setImageResource(R.drawable.car_south);
//            } else if (vehicle_direction.equals("E")) {
//                id_vehicle_in_marker.setImageResource(R.drawable.car_east);
//            } else if (vehicle_direction.equals("W")) {
//                id_vehicle_in_marker.setImageResource(R.drawable.car_west);
//            } else if (vehicle_direction.equals("NE")) {
//                id_vehicle_in_marker.setImageResource(R.drawable.car_ne);
//            } else if (vehicle_direction.equals("NW")) {
//                id_vehicle_in_marker.setImageResource(R.drawable.car_nw);
//            } else if (vehicle_direction.equals("SE")) {
//                id_vehicle_in_marker.setImageResource(R.drawable.car_se);
//            } else if (vehicle_direction.equals("SW")) {
//                id_vehicle_in_marker.setImageResource(R.drawable.car_sw);
//            } else if (vehicle_direction.equals("null")) {
//                id_vehicle_in_marker.setImageResource(R.drawable.car_east);
//            }
//        } else if (vehicle_type.equals("Cycle")) {
//            if (vehicle_direction.equals("N")) {
//                id_vehicle_in_marker.setImageResource(R.drawable.cycle_north);
//            } else if (vehicle_direction.equals("S")) {
//                id_vehicle_in_marker.setImageResource(R.drawable.cycle_south);
//            } else if (vehicle_direction.equals("E")) {
//                id_vehicle_in_marker.setImageResource(R.drawable.cycle_east);
//            } else if (vehicle_direction.equals("W")) {
//                id_vehicle_in_marker.setImageResource(R.drawable.cycle_west);
//            } else if (vehicle_direction.equals("NE")) {
//                id_vehicle_in_marker.setImageResource(R.drawable.cycle_ne);
//            } else if (vehicle_direction.equals("NW")) {
//                id_vehicle_in_marker.setImageResource(R.drawable.cycle_nw);
//            } else if (vehicle_direction.equals("SE")) {
//                id_vehicle_in_marker.setImageResource(R.drawable.cycle_se);
//            } else if (vehicle_direction.equals("SW")) {
//                id_vehicle_in_marker.setImageResource(R.drawable.cycle_sw);
//            } else if (vehicle_direction.equals("null")) {
//                id_vehicle_in_marker.setImageResource(R.drawable.cycle_east);
//            }
//        } else if (vehicle_type.equals("Truck")) {
//            if (vehicle_direction.equals("N")) {
//                id_vehicle_in_marker.setImageResource(R.drawable.truck_north);
//            } else if (vehicle_direction.equals("S")) {
//                id_vehicle_in_marker.setImageResource(R.drawable.truck_south);
//            } else if (vehicle_direction.equals("E")) {
//                id_vehicle_in_marker.setImageResource(R.drawable.truck_east);
//            } else if (vehicle_direction.equals("W")) {
//                id_vehicle_in_marker.setImageResource(R.drawable.truck_west);
//            } else if (vehicle_direction.equals("NE")) {
//                id_vehicle_in_marker.setImageResource(R.drawable.truck_ne);
//            } else if (vehicle_direction.equals("NW")) {
//                id_vehicle_in_marker.setImageResource(R.drawable.truck_nw);
//            } else if (vehicle_direction.equals("SE")) {
//                id_vehicle_in_marker.setImageResource(R.drawable.truck_se);
//            } else if (vehicle_direction.equals("SW")) {
//                id_vehicle_in_marker.setImageResource(R.drawable.truck_sw);
//            } else if (vehicle_direction.equals("null")) {
//                id_vehicle_in_marker.setImageResource(R.drawable.truck_east);
//            }
//        }
//    }
//
//    public String getVehicleType(String vehicletype) {
//        String vehicleType = null;
//        try {
//            JSONObject vehicletypejson = new JSONObject(vehicletype);
//            vehicleType = vehicletypejson.getString("vehicleType");
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return vehicleType;
//    }
//
//    public String getVehicleInfo(String vehicle_json, String vehicle_tag) {
//        String vehicleTag = null;
//        try {
//            JSONObject vehicletypejson = new JSONObject(vehicle_json);
//            vehicleTag = vehicletypejson.getString(vehicle_tag);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return vehicleTag;
//    }
//
//    public String getVehicleLastSeenAddress(String vehicle_last_seen_address) {
//        String last_seen_time = "";
//        try {
//            JSONObject vehicletypejson = new JSONObject(vehicle_last_seen_address);
//            if (vehicletypejson.has("address")) {
//                last_seen_time = vehicletypejson.getString("address");
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return last_seen_time;
//    }
//
//    public String getVehicleLastSeenTime(String vehicle_last_seen_time) {
//        String last_seen_time = null;
//        try {
//            JSONObject vehicletypejson = new JSONObject(vehicle_last_seen_time);
//            last_seen_time = vehicletypejson.getString("lastSeen");
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return last_seen_time;
//    }
//
//    public String getVehicleStatus(String vehiclestatus) {
//        String vehicle_status = null;
//        try {
//            JSONObject vehicletypejson = new JSONObject(vehiclestatus);
//            vehicle_status = vehicletypejson.getString("status");
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return vehicle_status;
//    }
//
//    public boolean checkNetworkSettings() {
//        isInternetPresent = cd.isConnectingToInternet();
//        return isInternetPresent;
//    }
//
//    private void prepareListData() {
//        navigationList.add("Home");
//        navigationList.add("Vehicles");
//        navigationList.add(getResources().getString(R.string.notification));
//        navigationList.add(getResources().getString(R.string.kms_summary));
//        navigationList.add("Settings");
//
//        navigationList.add(getResources().getString(R.string.version_name));
//
//        navigationList.add(getResources().getString(R.string.signout));
//        navigationBaseAdapter = new NavigationBaseAdapter(context, navigationList);
//        navigationlist.setAdapter(navigationBaseAdapter);
//        // Adding child data
//        vamosgroupList.add("School Name");
//        // Adding child data
//        List<String> top250 = new ArrayList<String>();
//        top250.add("Route 1");
//        top250.add("Route 2");
//        top250.add("Route 3");
//        vamoschildelements.add(top250); // Header, Child data
//        vamosadapter = new VamosExpandableAdapter(vamosgroupList, vamoschildelements);
//        vamosadapter.setInflater((LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE), NavigationActivity.this);
//        publicexplist.setAdapter(vamosadapter);
//    }
//
//    public String getStdTableValue(String tag) {
//        Cursor std_table_cur = null;
//        String stdtablevalues = null;
//        try {
//            std_table_cur = dbhelper.get_std_table_info(tag);
//            std_table_cur.moveToFirst();
//            stdtablevalues = std_table_cur.getString(0);
//        } catch (Exception e) {
//            Toast.makeText(getApplicationContext(), "Something went wrong. Please try again", Toast.LENGTH_SHORT).show();
//        } finally {
//            if (std_table_cur != null) {
//                std_table_cur.close();
//            }
//        }
//        return stdtablevalues;
//    }
//
//    public void cancelVehicleTracking() {
//        Intent cancel_intent = new Intent(getApplicationContext(), VehicleTrackingService.class);
//        cancel_pendingIntent = PendingIntent.getService(getApplicationContext(), sp.getInt("notif_id", 0), cancel_intent, 0);
//        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
//        alarmManager.cancel(cancel_pendingIntent);
//        SharedPreferences.Editor editor1 = sp.edit();
//        editor1.putInt("notif_id", sp.getInt("notif_id", 0) + 1);
//        editor1.putBoolean("vehicle_track_show_progress", true);
//        editor1.putBoolean("tracking_exception", false);
//        editor1.commit();
//        stopService(cancel_intent);
//        countDownTimer.cancel();
//        timerHasStarted = false;
//        dbhelper.deletecurrent_vehicleinfo();
//        handler.removeCallbacksAndMessages(null);
//    }
//
//    public void vehicleSelection(String vehiclename) {
//        Cursor vehicle_cursor = null;
//        try {
//            SharedPreferences.Editor editor = sp.edit();
//            editor.putString("current_vehicleID", vehiclename);
//            editor.commit();
//            vehicle_cursor = dbhelper.get_vehicle_details();
//            if (vehicle_cursor.moveToFirst()) {
//                ParsingClass parse = new ParsingClass();
//                parse.getVehiclesInfo(vehicle_cursor, vehiclename);
//                setupMap(parse.getVehiclesJSON(), parse.getLatitude(), parse.getLongitude(), vehiclename);
//                cancelVehicleTracking();
//                id_map_info_layout.setVisibility(View.VISIBLE);
//                vehicle_info_layout.setVisibility(View.GONE);
//                current_vehicle_info_layout.setVisibility(View.GONE);
//                expListView.setVisibility(View.GONE);
//                search.setVisibility(View.GONE);
//            }
//        } catch (Exception e) {
//            Toast.makeText(getApplicationContext(), "Something went wrong. Please try again", Toast.LENGTH_SHORT).show();
//        } finally {
//            if (vehicle_cursor != null) {
//                vehicle_cursor.close();
//            }
//        }
//    }
//
//    public void vehicle_button_functionality() {
//        navigation_status = "Vehicles";
//        mDrawerLayout.closeDrawer(id_navigation_layout);
//        expListView.setVisibility(View.VISIBLE);
//        search.setVisibility(View.VISIBLE);
//        id_settings_layout.setVisibility(View.GONE);
//        current_vehicle_info_layout.setVisibility(View.GONE);
//        vehicle_info_layout.setVisibility(View.VISIBLE);
//        id_map_info_layout.setVisibility(View.GONE);
//        id_refresh_tracking_icon.setVisibility(View.VISIBLE);
//        id_refresh_countdowntimer.setVisibility(View.GONE);
//        history_detail_layout.setVisibility(View.GONE);
//        header_layout.setVisibility(View.VISIBLE);
//        history_vehicle_info_layout.setVisibility(View.GONE);
//        SharedPreferences.Editor editor = sp.edit();
//        editor.putBoolean("tracking_exception", false);
//        editor.commit();
//        track = false;
//        cancelVehicleTracking();
//    }
//
//    public void home_button_functionality() {
//        String grouptype, groupname;
//        navigation_status = "Home";
//        mDrawerLayout.closeDrawer(id_navigation_layout);
//        expListView.setVisibility(View.GONE);
//        search.setVisibility(View.GONE);
//        id_settings_layout.setVisibility(View.GONE);
//        current_vehicle_info_layout.setVisibility(View.GONE);
//        vehicle_info_layout.setVisibility(View.VISIBLE);
//        id_map_info_layout.setVisibility(View.GONE);
//        id_refresh_tracking_icon.setVisibility(View.VISIBLE);
//        id_refresh_countdowntimer.setVisibility(View.GONE);
//        history_detail_layout.setVisibility(View.GONE);
//        header_layout.setVisibility(View.VISIBLE);
//        history_vehicle_info_layout.setVisibility(View.GONE);
//        cancelVehicleTracking();
//        if (map != null) {
//            map.clear();
//        }
//        if (sp.getString("current_group_name", "all").equals("all")) {
//            grouptype = "getall";
//            groupname = sp.getString("current_group_name", "all");
//            String par[] = {Const.API_URL + "mobile/getVehicleLocations?userId=" + username + "&macid=" + getStdTableValue("macid") + "&appid=" + getStdTableValue("appid"), "getall", Integer.toString(0), "all"};
//            new PullVehicleInformation().execute(par);
//        } else {
//            grouptype = "getgroup";
//            groupname = sp.getString("current_group_name", "all");
//            String par[] = {Const.API_URL + "mobile/getVehicleLocations?userId=" + username + "&group=" + groupList.get(sp.getInt("current_group_pos", 0)) + "&macid=" + getStdTableValue("macid") + "&appid=" + getStdTableValue("appid"), "getgroup", Integer.toString(sp.getInt("current_group_pos", 0)), groupList.get(sp.getInt("current_group_pos", 0))};
//            new PullVehicleInformation().execute(par);
//        }
//    }
//
//    /**
//     * Date and Time picker code
//     */
//    @Override
//    protected Dialog onCreateDialog(int id) {
//        Calendar calNow = Calendar.getInstance();
//        switch (id) {
//            case DATE_PICKER_ID:
//                // open datepicker dialog.
//                // set date picker for current date
//                // add pickerListener listner to date picker
//                return new DatePickerDialog(this, datepickerListener, calNow.get(Calendar.YEAR), calNow.get(Calendar.MONTH), calNow.get(Calendar.DAY_OF_MONTH));
//            case TIME_DIALOG_ID:
//                // set time picker as current time
//                return new TimePickerDialog(this, timePickerListener, calNow.get(Calendar.HOUR_OF_DAY), calNow.get(Calendar.MINUTE), false);
//        }
//        return null;
//    }
//
//    /**
//     * Public Methods
//     */
//    @Override
//    public boolean onClose() {
//        adapter.filterData("");
//        InputMethodManager mgr = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
//        mgr.hideSoftInputFromWindow(search.getWindowToken(), 0);
//        return false;
//    }
//
//    @Override
//    public boolean onQueryTextSubmit(String query) {
//        adapter.filterData(query);
//        InputMethodManager mgr = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
//        mgr.hideSoftInputFromWindow(search.getWindowToken(), 0);
//        return true;
//    }
//
//    @Override
//    public boolean onQueryTextChange(String query) {
//        adapter.filterData(query);
//        return false;
//    }
//
//    /**
//     * DB Initialisation
//     */
//    void dbSetup() {
//        dbhelper = new DBHelper(this);
//        try {
//            dbhelper.createDataBase();
//            dbhelper.openDataBase();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//
//    void check_opened_dialog() {
//        backButtonPressed = true;
//        id_next_vehicle.setVisibility(View.GONE);
//        if (current_vehicle_info_layout.getVisibility() == View.VISIBLE) {
//            current_vehicle_info_layout.setVisibility(View.GONE);
//        }
//        id_previous_vehicle.setVisibility(View.GONE);
//        pause_icon.setVisibility(View.GONE);
//        play_icon.setVisibility(View.GONE);
//        if (id_poi_layout_close.getVisibility() == View.VISIBLE) {
//            id_poi_layout.setVisibility(View.GONE);
//        }
//        if (history_detail_layout.getVisibility() == View.VISIBLE) {
//            pause = !pause;
//            if (map != null) {
//                map.clear();
//            }
//            history_detail_layout.setVisibility(View.GONE);
//            speed_seekbar.setVisibility(View.GONE);
//            history_seekbar.setVisibility(View.GONE);
//            pause_icon.setVisibility(View.GONE);
//            play_icon.setVisibility(View.GONE);
//            history_vehicle_info_layout.setVisibility(View.GONE);
//            id_refresh_tracking_icon.setVisibility(View.VISIBLE);
//        }
//        if (id_date_time_picker_layout.getVisibility() == View.VISIBLE) {
//            radioGroup.setVisibility(View.GONE);
//            id_date_time_picker_layout.setVisibility(View.GONE);
//            header_layout.setVisibility(View.VISIBLE);
//            vehicle_info_layout.setVisibility(View.VISIBLE);
//        }
//        if (radioGroup.getVisibility() == View.VISIBLE) {
//            radioGroup.setVisibility(View.GONE);
//            id_date_time_picker_layout.setVisibility(View.GONE);
//            header_layout.setVisibility(View.VISIBLE);
//            vehicle_info_layout.setVisibility(View.VISIBLE);
//        }
//    }
//
//    /**
//     * Expandable Adapter - Listview that shows the Vehicles group and its vehicles of user
//     */
//    public class MyExpandableAdapter extends BaseExpandableListAdapter {
//        private Activity activity;
//        private ArrayList<Object> childtems;
//        private LayoutInflater inflater;
//        private ArrayList<String> parentItems, jsonChild;
//
//        public MyExpandableAdapter(ArrayList<String> parents, ArrayList<Object> childern) {
//            this.parentItems = parents;
//            this.childtems = childern;
//        }
//
//        public void setInflater(LayoutInflater inflater, Activity activity) {
//            this.inflater = inflater;
//            this.activity = activity;
//        }
//
//        @Override
//        public int getGroupCount() {
//            return parentItems.size();
//        }
//
//        @Override
//        public void onGroupCollapsed(int groupPosition) {
//            super.onGroupCollapsed(groupPosition);
//        }
//
//        @Override
//        public View getChildView(final int groupPosition, final int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
//            ViewHolder holder;
//            String vehicle_position = null, vehicle_position_timimg = null;
//            child = (ArrayList<String>) childtems.get(0);
//            jsonChild = (ArrayList<String>) jsonchildelements.get(0);
//            if (!child.isEmpty()) {
//                if (convertView == null) {
//                    convertView = inflater.inflate(R.layout.group, null);
//                    holder = new ViewHolder();
//                    holder.id_lastseen_vehicle = (TextView) convertView.findViewById(R.id.id_vehicle_name);
//                    holder.id_lastseen_address = (TextView) convertView.findViewById(R.id.id_lastseen_vehicle);
//                    holder.id_vehicle_position_timimg = (TextView) convertView.findViewById(R.id.id_vehicle_position_timimg);
//                    holder.vehicle_type = (ImageView) convertView.findViewById(R.id.id_vehicle_icon);
//                    holder.id_lastseen_address.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
//                    holder.id_lastseen_vehicle.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
//                    holder.id_vehicle_position_timimg.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
//                    holder.id_vehicles_layout = (LinearLayout) convertView.findViewById(R.id.id_vehicles_layout);
//                    holder.id_sub_vehicles_layout = (LinearLayout) convertView.findViewById(R.id.id_sub_vehicles_layout);
//                    convertView.setTag(holder);
//                } else {
//                    holder = (ViewHolder) convertView.getTag();
//                }
//                vehicle_position = getVehicleInfo(jsonChild.get(childPosition).toString(), "position");
//                if (getVehicleStatus(jsonChild.get(childPosition).toString()).equals("ON")) {
//                    holder.id_lastseen_vehicle.setTextAppearance(context, R.style.vehicles_list_style_normal);
//                } else {
//                    holder.id_lastseen_vehicle.setTextAppearance(context, R.style.offline_vehicles_list_style_normal);
//                }
//                holder.id_lastseen_vehicle.setText(child.get(childPosition) + " " + "(" + getVehicleLastSeenTime(jsonChild.get(childPosition).toString()) + ")");
//                holder.id_lastseen_address.setText(getVehicleLastSeenAddress(jsonChild.get(childPosition).toString()));
//                mShortName = getVehicleInfo(jsonChild.get(childPosition).toString(), "shortName");
//                if (vehicle_position.equalsIgnoreCase("P")) {
//                    vehicle_position_timimg = getVehicleInfo(jsonChild.get(childPosition).toString(), "parkedTime");
//                    holder.id_vehicle_position_timimg.setText("Parked " + cons.getVehicleTime(vehicle_position_timimg));
//                } else if (vehicle_position.equalsIgnoreCase("M")) {
//                    vehicle_position_timimg = getVehicleInfo(jsonChild.get(childPosition).toString(), "movingTime");
//                    holder.id_vehicle_position_timimg.setText("Moving " + cons.getVehicleTime(vehicle_position_timimg));
//                } else if (vehicle_position.equalsIgnoreCase("U")) {
//                    vehicle_position_timimg = getVehicleInfo(jsonChild.get(childPosition).toString(), "noDataTime");
//                    holder.id_vehicle_position_timimg.setText("No Data " + cons.getVehicleTime(vehicle_position_timimg));
//                } else if (vehicle_position.equalsIgnoreCase("S")) {
//                    vehicle_position_timimg = getVehicleInfo(jsonChild.get(childPosition).toString(), "idleTime");
//                    holder.id_vehicle_position_timimg.setText("Idle " + cons.getVehicleTime(vehicle_position_timimg));
//                } else {
//                    holder.id_vehicle_position_timimg.setText("-");
//                }
//                if (getVehicleType(jsonChild.get(childPosition).toString()).equalsIgnoreCase("Bus")) {
//                    holder.vehicle_type.setImageDrawable(setVehicleImage(context.getResources().getDrawable(R.drawable.bus_east), vehicle_position));
//                } else if (getVehicleType(jsonChild.get(childPosition).toString()).equalsIgnoreCase("Truck")) {
//                    holder.vehicle_type.setImageDrawable(setVehicleImage(context.getResources().getDrawable(R.drawable.truck_east), vehicle_position));
//                } else if (getVehicleType(jsonChild.get(childPosition).toString()).equalsIgnoreCase("Car")) {
//                    holder.vehicle_type.setImageDrawable(setVehicleImage(context.getResources().getDrawable(R.drawable.car_east), vehicle_position));
//                } else if (getVehicleType(jsonChild.get(childPosition).toString()).equalsIgnoreCase("Cycle")) {
//                    holder.vehicle_type.setImageDrawable(setVehicleImage(context.getResources().getDrawable(R.drawable.cycle_east), vehicle_position));
//                }
//                holder.id_lastseen_vehicle.setOnClickListener(new OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        try {
//                            strSelectedVehicle = child.get(childPosition);
//                            jSelectedVehicleObject = new JSONObject(jsonChild.get(childPosition));
//                            UtilManager.quickActionShow(view);
//                            position = childPosition;
//                        } catch (Exception e) {
//                            e.printStackTrace();
//                        }
//                    }
//                });
//                holder.id_lastseen_address.setOnClickListener(new OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        try {
//                            strSelectedVehicle = child.get(childPosition);
//                            jSelectedVehicleObject = new JSONObject(jsonChild.get(childPosition));
//                            UtilManager.quickActionShow(view);
//                            position = childPosition;
//                        } catch (Exception e) {
//                            e.printStackTrace();
//                        }
//                    }
//                });
//                holder.id_vehicle_position_timimg.setOnClickListener(new OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        try {
//                            strSelectedVehicle = child.get(childPosition);
//                            jSelectedVehicleObject = new JSONObject(jsonChild.get(childPosition));
//                            UtilManager.quickActionShow(view);
//                            position = childPosition;
//                        } catch (Exception e) {
//                            e.printStackTrace();
//                        }
//                    }
//                });
//                holder.id_sub_vehicles_layout.setOnClickListener(new OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        try {
//                            strSelectedVehicle = child.get(childPosition);
//                            jSelectedVehicleObject = new JSONObject(jsonChild.get(childPosition));
//                            UtilManager.quickActionShow(view);
//                            position = childPosition;
//                        } catch (Exception e) {
//                            e.printStackTrace();
//                        }
//                    }
//                });
//            }
//            return convertView;
//        }
//
//        public void filterData(String query) {
//            query = query.toLowerCase();
//            if (query.isEmpty()) {
//                childtems.clear();
//                for (int g = 0; g < getGroupCount(); g++) {
//                    if (g == sp.getInt("current_group_pos", 0)) {
//                        ArrayList<String> child = (ArrayList<String>) originalchildelements.get(0);
//                        ArrayList<Object> newList = new ArrayList<Object>();
//                        for (int i = 0; i < child.size(); i++) {
//                            newList.add(child.get(i).toString());
//                        }
//                        childtems.add(newList);
//                    } else {
//                        ArrayList<Object> newList = new ArrayList<Object>();
//                        newList.add("");
//                    }
//                }
//            } else {
//                childtems.clear();
//                for (int g = 0; g < getGroupCount(); g++) {
//                    if (g == sp.getInt("current_group_pos", 0)) {
//                        ArrayList<String> child = (ArrayList<String>) originalchildelements.get(0);
//                        ArrayList<Object> newList = new ArrayList<Object>();
//                        for (int i = 0; i < child.size(); i++) {
//                            if (child.get(i).toString().toLowerCase().contains(query)) {
//                                newList.add(child.get(i).toString());
//                            }
//                        }
//                        childtems.add(newList);
//                    } else {
//                        ArrayList<Object> newList = new ArrayList<Object>();
//                        newList.add("");
//                    }
//                }
//            }
//            adapter = new MyExpandableAdapter(groupList, childelements);
//            adapter.setInflater((LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE), NavigationActivity.this);
//            expListView.setAdapter(adapter);
//            expListView.expandGroup(sp.getInt("current_group_pos", 0));
//            adapter.notifyDataSetChanged();
//        }
//
//        @Override
//        public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
//            ViewHolder holder = null;
//            final int grouPos = groupPosition;
//            try {
//                if (convertView == null) {
//                    LayoutInflater infalInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//                    convertView = infalInflater.inflate(R.layout.row, null);
//                    holder = new ViewHolder();
//                    LinearLayout ll = (LinearLayout) convertView;
//                    holder.checkedtextView = (CheckedTextView) ll.findViewById(R.id.id_group_name);
//                    holder.checkedtextView.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
//                    convertView.setTag(holder);
//                } else {
//                    holder = (ViewHolder) convertView.getTag();
//                }
//                holder.checkedtextView.setText(parentItems.get(groupPosition));
//                holder.checkedtextView.setChecked(isExpanded);
//                convertView.setOnClickListener(new OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        if (checkNetworkSettings()) {
//                            /**code to cancel the previous service*/
//                            cancelVehicleTracking();
//                            current_vehicle_info_layout.setVisibility(View.GONE);
//                            String par[] = {Const.API_URL + "mobile/getVehicleLocations?userId=" + username + "&group=" + groupList.get(grouPos) + "&macid=" + getStdTableValue("macid") + "&appid=" + getStdTableValue("appid"), "getgroup", Integer.toString(grouPos), groupList.get(grouPos)};
//                            new PullVehicleInformation().execute(par);
//                        } else {
//                            Toast internet_toast = Toast.makeText(context, "Please Check your Internet Connection", Toast.LENGTH_LONG);
//                            internet_toast.show();
//                        }
//                    }
//                });
//            } catch (Exception ex) {
//                ex.printStackTrace();
//            }
//            return convertView;
//        }
//
//        private class ViewHolder {
//            public TextView id_lastseen_vehicle = null, id_lastseen_address = null, id_vehicle_position_timimg;
//            public LinearLayout id_vehicles_layout, id_sub_vehicles_layout;
//            public ImageView vehicle_type;
//            public CheckedTextView checkedtextView;
//        }
//
//        @Override
//        public Object getChild(int groupPosition, int childPosition) {
//            return childtems.get(0);
//        }
//
//        @Override
//        public long getChildId(int groupPosition, int childPosition) {
//            return childPosition;
//        }
//
//        @Override
//        public int getChildrenCount(int groupPosition) {
//            if (childtems.get(0) == null) {
//                return 0;
//            } else {
//                return ((ArrayList<Object>) childtems.get(0)).size();
//            }
//        }
//
//        @Override
//        public Object getGroup(int groupPosition) {
//            return parentItems.get(groupPosition);
//        }
//
//
//        @Override
//        public void onGroupExpanded(int groupPosition) {
//            super.onGroupExpanded(groupPosition);
//        }
//
//        @Override
//        public long getGroupId(int groupPosition) {
//            return groupPosition;
//        }
//
//        @Override
//        public boolean hasStableIds() {
//            return false;
//        }
//
//        @Override
//        public boolean isChildSelectable(int groupPosition, int childPosition) {
//            return false;
//        }
//
//    }
//
//    /**
//     * Expandable Adapter - Listview that shows the Vehicles group and its vehicles of Public Vamos
//     */
//    public class VamosExpandableAdapter extends BaseExpandableListAdapter {
//        int groupPOS;
//        private Activity activity;
//        private ArrayList<Object> vamoschildtems;
//        private LayoutInflater inflater;
//        private ArrayList<String> vamosparentItems, vamoschild;
//        private ArrayList<Object> originalList;
//
//        public VamosExpandableAdapter(ArrayList<String> parents, ArrayList<Object> childern) {
//            this.vamosparentItems = parents;
//            this.vamoschildtems = childern;
//        }
//
//        public void setInflater(LayoutInflater inflater, Activity activity) {
//            this.inflater = inflater;
//            this.activity = activity;
//        }
//
//        public boolean checkChild(int groupPosition) {
//            boolean childcount;
//            childcount = !vamoschildtems.get(groupPosition).equals("");
//            return childcount;
//        }
//
//        @Override
//        public View getChildView(final int groupPosition, final int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
//            vamoschild = (ArrayList<String>) vamoschildtems.get(groupPosition);
//            if (!vamoschild.isEmpty()) {
//                TextView textView = null;
//                ImageView vehicle_type;
//                if (convertView == null) {
//                    convertView = inflater.inflate(R.layout.group, null);
//                }
//                textView = (TextView) convertView.findViewById(R.id.id_vehicle_name);
//                vehicle_type = (ImageView) convertView.findViewById(R.id.id_vehicle_icon);
//                textView.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
//                textView.setText(vamoschild.get(childPosition));
//            }
//
//            System.out.println("The child name is ::::" + vamoschild.get(childPosition));
//            return convertView;
//        }
//
//        @Override
//        public View getGroupView(final int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
//            if (convertView == null) {
//                LayoutInflater infalInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//                convertView = infalInflater.inflate(R.layout.row, null);
//            }
//            groupPOS = groupPosition;
//            LinearLayout ll = (LinearLayout) convertView;
//            CheckedTextView textView = (CheckedTextView) ll.findViewById(R.id.id_group_name);
//            textView.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
//            textView.setText(vamosparentItems.get(groupPosition));
//            System.out.println("The Parent name is ::::" + vamosparentItems.get(groupPosition));
//            textView.setChecked(isExpanded);
//            return convertView;
//        }
//
//        @Override
//        public Object getChild(int groupPosition, int childPosition) {
//            return null;
//        }
//
//        @Override
//        public long getChildId(int groupPosition, int childPosition) {
//            return 0;
//        }
//
//        @Override
//        public int getChildrenCount(int groupPosition) {
//            return ((ArrayList<String>) vamoschildtems.get(groupPosition)).size();
//        }
//
//        @Override
//        public Object getGroup(int groupPosition) {
//            return null;
//        }
//
//        @Override
//        public int getGroupCount() {
//            return vamosparentItems.size();
//        }
//
//        @Override
//        public void onGroupCollapsed(int groupPosition) {
//            super.onGroupCollapsed(groupPosition);
//        }
//
//        @Override
//        public void onGroupExpanded(int groupPosition) {
//            super.onGroupExpanded(groupPosition);
//        }
//
//        @Override
//        public long getGroupId(int groupPosition) {
//            return 0;
//        }
//
//        @Override
//        public boolean hasStableIds() {
//            return false;
//        }
//
//        @Override
//        public boolean isChildSelectable(int groupPosition, int childPosition) {
//            return false;
//        }
//    }
//
//    /**
//     * Navigation adapter - Listview that shows the HOME, PRODUCTS and SETTINGS
//     */
//    public class NavigationBaseAdapter extends BaseAdapter {
//        Context con;
//        private LayoutInflater layoutInflater;
//        private ArrayList<String> results;
//
//        public NavigationBaseAdapter(Context context, ArrayList<String> data) {
//            this.results = data;
//            con = context;
//            layoutInflater = LayoutInflater.from(context);
//        }
//
//        @Override
//        public int getCount() {
//            // TODO Auto-generated method stub
//            return results.size();
//        }
//
//        @Override
//        public Object getItem(int position) {
//            // TODO Auto-generated method stub
//            return results.get(position);
//        }
//
//        @Override
//        public long getItemId(int position) {
//            // TODO Auto-generated method stub
//            return position;
//        }
//
//        public View getView(final int position, View convertView, ViewGroup parent) {
//            ViewHolder holder;
//            if (convertView == null) {
//                convertView = layoutInflater.inflate(R.layout.navigation_list_items, null);
//                holder = new ViewHolder();
//                holder.navigations_list_items = (TextView) convertView.findViewById(R.id.id_navigation_items);
//                holder.navigations_list_items.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
//                convertView.setTag(holder);
//            } else {
//                holder = (ViewHolder) convertView.getTag();
//            }
//            holder.navigations_list_items.setText(results.get(position));
//            return convertView;
//        }
//
//        class ViewHolder {
//            TextView navigations_list_items;
//            ImageView photo, video, audio;
//        }
//    }
//
//    /**
//     * AsyncTask Background API Calls
//     */
//    public class PullVehicleInformation extends AsyncTask<String, Integer, String> {
//        int current_group_pos;
//        String current_group_name;
//
//        @Override
//        public String doInBackground(String... urls) {
//            current_group_pos = Integer.parseInt(urls[2]);
//            current_group_name = urls[3];
//            SharedPreferences.Editor editor = sp.edit();
//            editor.putInt("current_group_pos", current_group_pos);
//            editor.putString("current_group_name", current_group_name);
//            editor.commit();
//            String response_from_server = "";
//            try {
//
//                //   System.out.println("The pull vehicle info url is ::::"+urls[0]);
//
//                response_from_server = sendGet(urls[0], pull_vehicle_timeout);
//                if (urls[1].equals("getall")) {
//                    dbhelper.deletevalues();
//                    updateVehicleLocations(response_from_server, urls[1], "getall", current_group_pos);
//                } else if (urls[1].equals("getgroup")) {
//                    updateVehicleLocations(response_from_server, urls[1], urls[3], current_group_pos);
//                }
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//            return urls[1];
//        }
//
//        @Override
//        protected void onPreExecute() {
//            super.onPreExecute();
//            progressDialog = MyCustomProgressDialog.ctor(context);
//            progressDialog.setCancelable(false);
//            progressDialog.show();
//        }
//
//        protected void onProgressUpdate(Integer... values) {
//            runOnUiThread(new Runnable() {
//                @Override
//                public void run() {
//                    publishProgress(0);
//                }
//            });
//        }
//
//        @Override
//        public void onPostExecute(String result) {
//            Cursor vehicle_info = null;
//            try {
//                /**code to display all vehicles in map*/
//                vehicle_info = dbhelper.get_vehicle_details();
//                updateVehicleList(vehicle_info, result, sp.getInt("current_group_pos", 0), "all", "homepage");
//                if (progressDialog.isShowing()) {
//                    progressDialog.dismiss();
//                }
//            } catch (Exception e) {
//            } finally {
//                if (progressDialog.isShowing()) {
//                    progressDialog.dismiss();
//                }
//                if (vehicle_info != null) {
//                    vehicle_info.close();
//                }
//            }
//        }
//    }
//
//    /**
//     * Date and Time picker code
//     */
//
//    public class GetVehicleHistory extends AsyncTask<String, Integer, String> {
//        @Override
//        protected void onPreExecute() {
//            super.onPreExecute();
//            progressDialog = MyCustomProgressDialog.ctor(context);
//            progressDialog.show();
//        }
//
//        protected void onProgressUpdate(Integer... values) {
//            progressDialog.show();
//        }
//
//        @Override
//        public String doInBackground(String... urls) {
//            String response_from_server = "";
//            if (urls[0].equals("replay")) {
//                response_from_server = sp.getString("historyplaybackdata", "");
//            } else {
//                try {
//                    response_from_server = sendGet(urls[0], history_timeout);
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            }
//            return response_from_server;
//        }
//
//        @Override
//        public void onPostExecute(String result) {
//            if (result != null && !result.isEmpty()) {
//                SharedPreferences.Editor editor = sp.edit();
//                editor.putString("historyplaybackdata", result);
//                editor.commit();
//                historyPlayBack(result, progressDialog);
//                historyRepeat = result;
//            } else {
//                if (progressDialog.isShowing()) {
//                    progressDialog.dismiss();
//                }
//                header_layout.setVisibility(View.VISIBLE);
//                vehicle_info_layout.setVisibility(View.VISIBLE);
//                id_map_info_layout.setVisibility(View.GONE);
//                home_button_functionality();
//                Toast.makeText(getApplicationContext(), "No History Data Available", Toast.LENGTH_SHORT).show();
//            }
//        }
//    }
//
//    public class SendPOIInformation extends AsyncTask<String, Void, String> {
//        @Override
//        public String doInBackground(String... urls) {
//            String response_from_server = "";
//            try {
//                response_from_server = sendGet(urls[0], poi_timeout);
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//            return response_from_server;
//        }
//
//        @Override
//        public void onPostExecute(String result) {
//            if (result != null) {
//                store_poi.setText("");
//                id_poi_layout.setVisibility(View.GONE);
//            } else {
//                id_poi_layout.setVisibility(View.GONE);
//                Toast internet_toast = Toast.makeText(context, "Something went wrong. Please add poi again", Toast.LENGTH_LONG);
//                internet_toast.show();
//            }
//        }
//    }
//
//    public class GetPOIInformation extends AsyncTask<String, Void, String> {
//        @Override
//        protected void onPreExecute() {
//            super.onPreExecute();
//        }
//
//        @Override
//        public String doInBackground(String... urls) {
//            String response_from_server = "";
//            try {
//
//                System.out.println("The POI url is :::::" + urls[0]);
//
//                response_from_server = sendGet(urls[0], poi_timeout);
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//            return response_from_server;
//        }
//
//        @Override
//        public void onPostExecute(String result) {
//            SharedPreferences.Editor editor = sp.edit();
//            editor.putString("poidata", result);
//            editor.commit();
//            drawPoiPoints(result);
//        }
//    }
//
//    private class FetchLockData extends AsyncTask<String, String, String> {
//        @Override
//        protected void onPreExecute() {
//            super.onPreExecute();
//            progressDialog = MyCustomProgressDialog.ctor(context);
//            progressDialog.show();
//        }
//
//        @Override
//        protected String doInBackground(String... params) {
//            try {
//                return sendGet(Const.API_URL + "mobile/getLock?vehicleId=" + strSelectedVehicle + "&userId=" + username, lock_unlock_timeout);
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//            return null;
//        }
//
//        @Override
//        public void onPostExecute(String result) {
//            try {
//                if (result != null && !result.isEmpty()) {
//                    JSONObject mJsonObject = new JSONObject(result);
//                    if (mJsonObject != null) {
//                        if (mJsonObject.has("mobileNo") && mJsonObject.has("smsText")) {
//                            SmsManager.getDefault().sendTextMessage(mJsonObject.getString("mobileNo"), null, mJsonObject.getString("smsText"), null, null);
//                        }
//                    }
//                } else {
//                    Toast.makeText(context, "Please Try Again", Toast.LENGTH_LONG).show();
//                }
//            } catch (Exception e) {
//                e.printStackTrace();
//                Toast.makeText(NavigationActivity.this, "Lock message not sent... Please try again later.", Toast.LENGTH_LONG).show();
//            }
//            if (progressDialog.isShowing()) {
//                progressDialog.dismiss();
//            }
//        }
//    }
//
//    private class FetchUnLockData extends AsyncTask<String, String, String> {
//        @Override
//        protected void onPreExecute() {
//            super.onPreExecute();
//            progressDialog = MyCustomProgressDialog.ctor(context);
//            progressDialog.show();
//        }
//
//        @Override
//        protected String doInBackground(String... params) {
//            try {
//                return sendGet(Const.API_URL + "mobile/getUnLock?vehicleId=" + strSelectedVehicle + "&userId=" + username, lock_unlock_timeout);
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//            return null;
//        }
//
//        @Override
//        public void onPostExecute(String result) {
//            try {
//                if (result != null && !result.isEmpty()) {
//                    JSONObject mJsonObject = new JSONObject(result);
//                    if (mJsonObject != null) {
//                        if (mJsonObject.has("mobileNo") && mJsonObject.has("smsText")) {
//                            SmsManager.getDefault().sendTextMessage(mJsonObject.getString("mobileNo"), null, mJsonObject.getString("smsText"), null, null);
//                        }
//                    }
//                } else {
//                    Toast.makeText(context, "Please Try Again", Toast.LENGTH_LONG).show();
//                }
//            } catch (Exception e) {
//                e.printStackTrace();
//                Toast.makeText(NavigationActivity.this, "UnLock message not sent... Please try again later.", Toast.LENGTH_LONG).show();
//            }
//            if (progressDialog.isShowing()) {
//                progressDialog.dismiss();
//            }
//        }
//    }
//
//    private class GetReportData extends AsyncTask<String, String, String> {
//        @Override
//        protected void onPreExecute() {
//            super.onPreExecute();
//            progressDialog = MyCustomProgressDialog.ctor(context);
//            progressDialog.setCancelable(false);
//            progressDialog.show();
//        }
//
//        @Override
//        protected String doInBackground(String... params) {
//            try {
//                String url = Const.API_URL + "mobile/getActionReport?vehicleId=" + strSelectedVehicle + "&userId=" + username + params[0];
//                Log.d(TAG, "GetReportData()" + "url = " + url + "");
//                return sendGet(url, report_timeout);
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//            return null;
//        }
//
//        @Override
//        public void onPostExecute(String result) {
//            Log.d(TAG, "GetReportData()" + "result = " + result + "");
//            if (progressDialog.isShowing()) {
//                progressDialog.dismiss();
//            }
//            try {
//                //result = "[{\"startTime\":1451025700295,\"state\":\"LocationExit\",\"address\":\"Tada, Andhra Pradesh 524401, India\",\"fuelConsume\":0.0},{\"startTime\":1451025740300,\"state\":\"LocationEntry\",\"address\":\"Tada, Andhra Pradesh 524401, India\",\"fuelConsume\":0.0},{\"startTime\":1451025790308,\"state\":\"LocationExit\",\"address\":\"Tada, Andhra Pradesh 524401, India\",\"fuelConsume\":0.0},{\"startTime\":1451029229991,\"state\":\"LocationEntry\",\"address\":\"Tada, Andhra Pradesh 524401, India\",\"fuelConsume\":0.0},{\"startTime\":1451029268311,\"state\":\"LocationExit\",\"address\":\"Tada, Andhra Pradesh 524401, India\",\"fuelConsume\":0.0},{\"startTime\":1451111892272,\"state\":\"LocationEntry\",\"address\":\"Tada, Andhra Pradesh 524401, India\",\"fuelConsume\":0.0},{\"startTime\":1451111962293,\"state\":\"LocationExit\",\"address\":\"Tada, Andhra Pradesh 524401, India\",\"fuelConsume\":0.0},{\"startTime\":1451115969411,\"state\":\"LocationEntry\",\"address\":\"Tada, Andhra Pradesh 524401, India\",\"fuelConsume\":0.0},{\"startTime\":1451116008057,\"state\":\"LocationExit\",\"address\":\"Tada, Andhra Pradesh 524401, India\",\"fuelConsume\":0.0}]";
//                ArrayList<ReportData> alReportData = new ArrayList<>();
//                if (result != null && !result.isEmpty()) {
//                    JSONArray mJSONArray = new JSONArray(result);
//                    if (mJSONArray != null) {
//                        for (int i = 0; i < mJSONArray.length(); i++) {
//                            JSONObject mJsonObject = new JSONObject(mJSONArray.getString(i));
//                            if (mJsonObject.has("startTime") && mJsonObject.has("state") && mJsonObject.has("address") && mJsonObject.has("fuelConsume")) {
//                                //Load listview data
//                                ReportData mReportData = new ReportData();
//                                mReportData.setStartTime(mJsonObject.getString("startTime"));
//                                mReportData.setState(mJsonObject.getString("state"));
//                                mReportData.setAddress(mJsonObject.getString("address"));
//                                mReportData.setFuelConsume(mJsonObject.getString("fuelConsume"));
//                                alReportData.add(mReportData);
//                            }
//                        }
//                    }
//                }
//                if (alReportData.size() > 0) {
//                    headerLayout.setVisibility(View.VISIBLE);
//                    lvReportList.setVisibility(View.VISIBLE);
//                    mReportListAdapter.setItemList(alReportData);
//                    Animation bottomUp = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.grow_from_bottom);
//                    bottomUp.setDuration(1000);
//                    lvReportList.startAnimation(bottomUp);
//                    svReportOption.setVisibility(View.GONE);
//                } else {
//                    Toast.makeText(getApplicationContext(), "No data found...", Toast.LENGTH_LONG).show();
//                }
//            } catch (Exception e) {
//                e.printStackTrace();
//                Toast.makeText(getApplicationContext(), getResources().getString(R.string.oops_error), Toast.LENGTH_SHORT).show();
//            }
//        }
//    }
//
//    private class doSignout extends AsyncTask<String, String, String> {
//        @Override
//        protected void onPreExecute() {
//            super.onPreExecute();
//            progressDialog = MyCustomProgressDialog.ctor(context);
//            progressDialog.setCancelable(false);
//            progressDialog.show();
//        }
//
//        @Override
//        protected String doInBackground(String... params) {
//            try {
//                String gcmToken = sp.getString("gcmToken", null);
//
//                System.out.println("The gcm key is :::::" + gcmToken);
//
//                return sendGet(Const.API_URL + "mobile/logOutUser?userId=" + username + "&gcmId=" + gcmToken, report_timeout);
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//            return null;
//        }
//
//        @Override
//        public void onPostExecute(String result) {
//            if (progressDialog.isShowing()) {
//                progressDialog.dismiss();
//            }
//            try {
//                /**
//                 * Perform logout by clearing the login status and start registration activity.
//                 */
//
//                CommonManager co = new CommonManager(getApplicationContext());
//                String qry = "DELETE FROM " + DataBaseHandler.TABLE_USER;
//                String qry1 = "DELETE FROM " + DataBaseHandler.TABLE_LOGO;
//                String qry2 = "DELETE FROM " + DataBaseHandler.TABLE_NOTIFICATIONS;
//                co.deleteDB(qry);
//                co.deleteDB(qry1);
//                //  co.deleteDB(qry2);
//
//                dbhelper.update_StdTableValues("login_status", false);
//                Intent register = new Intent(NavigationActivity.this, RegistrationPage.class);
//                startActivity(register);
//                finish();
//            } catch (Exception e) {
//                e.printStackTrace();
//                Toast.makeText(getApplicationContext(), getResources().getString(R.string.oops_error), Toast.LENGTH_SHORT).show();
//            }
//        }
//    }
//
//    private class getAppLogo extends AsyncTask<String, Void, Bitmap> {
//        @Override
//        protected void onPreExecute() {
//            super.onPreExecute();
//        }
//
//        @Override
//        protected Bitmap doInBackground(String... URL) {
//            String imageURL = URL[0];
//            Bitmap bitmap = null;
//            try {
//                // Download Image from URL
//                InputStream input = new java.net.URL(imageURL).openStream();
//                // Decode Bitmap
//                bitmap = BitmapFactory.decodeStream(input);
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//            return bitmap;
//        }
//
//        @Override
//        protected void onPostExecute(Bitmap result) {
//            // Set the bitmap into app logo
//            id_applogo.setImageBitmap(result);
//        }
//    }
//
//    private class GetAddressTask extends AsyncTask<Double, String, String> {
//        TextView currenta_adddress_view;
//        double getlat, getlng;
//        double address_type;
//
//        public GetAddressTask(TextView txtview) {
//            super();
//            currenta_adddress_view = txtview;
//        }
//
//        @Override
//        protected void onPreExecute() {
//            super.onPreExecute();
//        }
//
//        /**
//         * Get a Geocoder instance, get the latitude and longitude
//         * look up the address, and return it
//         *
//         * @return A string containing the address of the current
//         * location, or an empty string if no address can be found,
//         * or an error message
//         * @params params One or more Location objects
//         */
//        @Override
//        protected String doInBackground(Double... params) {
//            getlat = params[0];
//            getlng = params[1];
//            address_type = params[2];
//            Geocoder geocoder = new Geocoder(context, Locale.getDefault());
//            List<Address> addresses = null;
//            try {
//                addresses = geocoder.getFromLocation(params[0], params[1], 1);
//            } catch (IOException e1) {
//                e1.printStackTrace();
//            } catch (IllegalArgumentException e2) {
//                // Error message to post in the log
//                String errorString = "Illegal arguments " +
//                        Double.toString(params[0]) +
//                        " , " +
//                        Double.toString(params[1]) +
//                        " passed to address service";
//                e2.printStackTrace();
//                return errorString;
//            } catch (NullPointerException np) {
//                // TODO Auto-generated catch block
//                np.printStackTrace();
//            }
//            // If the reverse geocode returned an address
//            if (addresses != null && addresses.size() > 0) {
//                // Get the first address
//                Address address = addresses.get(0);
//                /*
//                 * Format the first line of address (if available),
//                 * city, and country name.
//                 */
//                String addressText = null;
//                StringBuffer addr = new StringBuffer();
//                for (int i = 0; i < address.getMaxAddressLineIndex(); i++) {
//                    addressText = address.getAddressLine(i);
//                    addr.append(addressText + ",");
//                }
//                // Return the text
//                return addr.toString();
//            } else {
//                return "No address found";
//            }
//        }
//
//        protected void onProgressUpdate(String... progress) {
//        }
//
//        protected void onPostExecute(String result) {
//            // sourceedit.setText(result);
//            if (result != null && !result.isEmpty()) {
//                if (!result.equals("IO Exception trying to get address") || !result.equals("No address found")) {
//                    /***
//                     * 1.0 - vehicles current address when a vehicle is clicked
//                     * 2.0 - vehicles origin address in history selection
//                     * 3.0 - vehicles destination address in history selection
//                     * 6.0 - vehicles current address when pause button is clicked in history selection
//                     */
//                    if (address_type == 1.0) {
//                        SharedPreferences.Editor editor = sp.edit();
//                        editor.putString("address", result);
//                        editor.commit();
//                        currenta_adddress_view.setText(result.equals("null") ? "No Data" : result);
//                    } else if (address_type == 2.0) {
//                        SharedPreferences.Editor editor = sp.edit();
//                        editor.putString("vehicle_origin_address", result);
//                        editor.commit();
//                        history_start_location_value.setText(result.equals("null") ? "No Data" : result);
//                    } else if (address_type == 3.0) {
//                        SharedPreferences.Editor editor = sp.edit();
//                        editor.putString("vehicle_destination_address", result);
//                        editor.commit();
//                        history_end_location_value.setText(result.equals("null") ? "No Data" : result);
//                    } else if (address_type == 4.0) {
//                        SharedPreferences.Editor editor = sp.edit();
//                        editor.putString("current_vehicle_address", result);
//                        editor.commit();
//                        id_curr_vehicle_address_value.setText(result.equals("null") ? "No Data" : result);
//                    } else if (address_type == 5.0) {
//                        currenta_adddress_view.setText(result.equals("null") ? "No Data" : result);
//                    } else if (address_type == 6.0) {
//                        currenta_adddress_view.setText(result.equals("null") ? "No Data" : result);
//                    }
//                }
//            } else {
//                Toast empty_fav = Toast.makeText(context, "Please Try Again", Toast.LENGTH_LONG);
//                empty_fav.show();
//            }
//        }
//    }
//
//    public class RefreshCountDownTimer extends CountDownTimer {
//        public RefreshCountDownTimer(long startTime, long interval) {
//            super(startTime, interval);
//        }
//
//        @Override
//        public void onTick(long millisUntilFinished) {
//            timeElapsed = startTime - millisUntilFinished;
//            if ((millisUntilFinished / 1000) == 0) {
//                id_refresh_countdowntimer.setText(Long.toString(refresh_rate));
//            } else {
//                id_refresh_countdowntimer.setText(String.valueOf(millisUntilFinished / 1000));
//            }
//        }
//
//        @Override
//        public void onFinish() {
//            Toast.makeText(getApplicationContext(), "Refreshed", Toast.LENGTH_SHORT).show();
//            countDownTimer.cancel();
//            timerHasStarted = false;
//        }
//    }
//}
