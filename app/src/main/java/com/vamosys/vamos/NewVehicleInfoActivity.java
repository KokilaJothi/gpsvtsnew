package com.vamosys.vamos;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.vamosys.model.VehicleData;
import com.vamosys.utils.Constant;
import com.vamosys.utils.TypefaceUtil;

import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.regex.Pattern;

public class NewVehicleInfoActivity extends Activity {

    LinearLayout mLinearHeaderLayout, mLinearLayout1, mLinearLayout2, mLinearLayout21, mLinearLayout22, mLinearLayout3, mLinearLayout4, mLinearLayout5,
            mLinearLayout6, mLinearLayout7;
    LinearLayout mSubLinearLayout1, mSubLinearLayout2, mSubLinearLayout3, mSubLinearLayout4, mSubLinearLayout5,
            mSubLinearLayout6, mSubLinearLayout7, mSubLinearLayout8;
    ImageView mBackImage, mVehicleIcon, mImgView1, mImgView2;
    View mHoriView1, mHoriView2, mHoriView3, mHoriView4, mHoriView5, mHoriView6;
    View mVeriView1, mVeriView2, mVeriView3;
    TextView mTxtHeader, mTxtView10, mTxtView11, mTxtView12, mTxtValue10, mTxtValue11, mTxtValue12, mTxtDistanceCoveredView20, mTxtDistanceCoveredValue, mTxtOdoDistance, mTxtOdoDistanceValue, mTxtLastSeen, mTxtLastSeenValue20,
            mTxtView30, mTxtView31, mTxtView32,mTxtView34, mTxtView33, mTxtValue30, mTxtValue31, mTxtValue32,mTxtValue34, mTxtValue33, mTxtView40,
            mTxtValue40, mTxtView50, mTxtView51, mTxtView52, mTxtValue50, mTxtValue51, mTxtValue52, mTxtView60, mTxtValue60,
            mTxtView70, mTxtView71, mTxtView72, mTxtValue70, mTxtValue71, mTxtValue72;

    Const cons;
    SharedPreferences sp;
    VehicleData vd;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_info_activity_layout);
        init();
        screenArrange();
        cons = new Const();

        sp = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        if (sp.getString("ip_adds", "") != null) {
            if (sp.getString("ip_adds", "").trim().length() > 0) {
                Const.API_URL = sp.getString("ip_adds", "");
            }
        }
        vd=Constant.SELECTED_VEHICLE_LOCATION_OBJECT;
        setData();


        mBackImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(NewVehicleInfoActivity.this, MapMenuActivity.class));
                finish();
            }
        });

        mImgView1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isValidMobile(Constant.SELECTED_VEHICLE_LOCATION_OBJECT.getDriverMobile())) {
                    callMethod();
                } else {
                    Toast.makeText(NewVehicleInfoActivity.this, "Mobile number is not valid", Toast.LENGTH_SHORT).show();
                }
            }
        });

        mImgView2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String sLat = Constant.SELECTED_VEHICLE_LOCATION_OBJECT.getLatitude();
                String sLng = Constant.SELECTED_VEHICLE_LOCATION_OBJECT.getLongitude();

                Intent intent = new Intent(Intent.ACTION_VIEW,
                        Uri.parse("http://maps.google.com/maps?saddr=" + sLat + "," + sLng + ""));
                startActivity(intent);
            }
        });
    }

    public void callMethod() {
//        Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + Constant.SELECTED_DRIVER_CONTACT_NUMBER));
//        startActivity(intent);
//        Intent callIntent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + Constant.SELECTED_DRIVER_CONTACT_NUMBER));
//        startActivity(callIntent);

//        if (ContextCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED) {
        Intent callIntent = new Intent(Intent.ACTION_DIAL);
        callIntent.setData(Uri.parse("tel:" + Constant.SELECTED_DRIVER_CONTACT_NUMBER));
        callIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(callIntent);
//        }

    }

    public void init() {
        mLinearHeaderLayout = (LinearLayout) findViewById(R.id.new_info_header_layout);
        mLinearLayout1 = (LinearLayout) findViewById(R.id.new_info_linear_layout1);
        mLinearLayout2 = (LinearLayout) findViewById(R.id.new_info_dist_lastseen_linear_layout2);
        mLinearLayout21 = (LinearLayout) findViewById(R.id.new_info_linear_layout21);
        mLinearLayout22 = (LinearLayout) findViewById(R.id.new_info_linear_layout22);
        mLinearLayout3 = (LinearLayout) findViewById(R.id.new_info_linear_layout3);
        mLinearLayout4 = (LinearLayout) findViewById(R.id.new_info_linear_layout4);
        mLinearLayout5 = (LinearLayout) findViewById(R.id.new_info_linear_layout5);
        mLinearLayout6 = (LinearLayout) findViewById(R.id.new_info_linear_layout6);
        mLinearLayout7 = (LinearLayout) findViewById(R.id.new_info_linear_layout7);

        mSubLinearLayout1 = (LinearLayout) findViewById(R.id.new_info_linear_layout10);
        mSubLinearLayout2 = (LinearLayout) findViewById(R.id.new_info_linear_layout11);
        mSubLinearLayout3 = (LinearLayout) findViewById(R.id.new_info_linear_layout30);
        mSubLinearLayout4 = (LinearLayout) findViewById(R.id.new_info_linear_layout31);
        mSubLinearLayout5 = (LinearLayout) findViewById(R.id.new_info_linear_layout50);
        mSubLinearLayout6 = (LinearLayout) findViewById(R.id.new_info_linear_layout51);
        mSubLinearLayout7 = (LinearLayout) findViewById(R.id.new_info_linear_layout70);
        mSubLinearLayout8 = (LinearLayout) findViewById(R.id.new_info_linear_layout71);

        mBackImage = (ImageView) findViewById(R.id.new_info_view_Back);
        mVehicleIcon = (ImageView) findViewById(R.id.new_info_icon);
        mImgView1 = (ImageView) findViewById(R.id.new_info_img_view60);
        mImgView2 = (ImageView) findViewById(R.id.new_info_img_value60);

        mHoriView1 = (View) findViewById(R.id.new_info_view10);
        mHoriView2 = (View) findViewById(R.id.new_info_view11);
        mHoriView3 = (View) findViewById(R.id.new_info_view12);
        mHoriView4 = (View) findViewById(R.id.new_info_view13);
        mHoriView5 = (View) findViewById(R.id.new_info_view14);
        mHoriView6 = (View) findViewById(R.id.new_info_view15);

        mVeriView1 = (View) findViewById(R.id.new_info_view20);
        mVeriView2 = (View) findViewById(R.id.new_info_view21);
        mVeriView3 = (View) findViewById(R.id.new_info_view22);

        mTxtHeader = (TextView) findViewById(R.id.new_info_header_txt);
        mTxtView10 = (TextView) findViewById(R.id.new_info_txt_view10);
        mTxtView11 = (TextView) findViewById(R.id.new_info_txt_view11);
        mTxtView12 = (TextView) findViewById(R.id.new_info_txt_view12);

        mTxtHeader.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        mTxtView10.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        mTxtView11.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        mTxtView12.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));

        mTxtValue10 = (TextView) findViewById(R.id.new_info_txt_value10);
        mTxtValue11 = (TextView) findViewById(R.id.new_info_txt_value11);
        mTxtValue12 = (TextView) findViewById(R.id.new_info_txt_value12);
        mTxtValue10.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        mTxtValue11.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        mTxtValue12.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));

        mTxtDistanceCoveredView20 = (TextView) findViewById(R.id.new_info_txt_dist_covered);
        mTxtDistanceCoveredValue = (TextView) findViewById(R.id.new_info_txt_dist_covered_value);
        mTxtOdoDistance = (TextView) findViewById(R.id.new_info_txt_odo_dist);
        mTxtOdoDistanceValue = (TextView) findViewById(R.id.new_info_txt_odo_dist_value);
        mTxtLastSeen = (TextView) findViewById(R.id.new_info_txt_lastseen);
        mTxtLastSeenValue20 = (TextView) findViewById(R.id.new_info_txt_lastseenvalue);

        mTxtDistanceCoveredView20.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        mTxtDistanceCoveredValue.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        mTxtOdoDistance.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        mTxtOdoDistanceValue.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        mTxtLastSeenValue20.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        mTxtLastSeen.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));

        mTxtView30 = (TextView) findViewById(R.id.new_info_txt_view30);
        mTxtView31 = (TextView) findViewById(R.id.new_info_txt_view31);
        mTxtView32 = (TextView) findViewById(R.id.new_info_txt_view32);
        mTxtView33 = (TextView) findViewById(R.id.new_info_txt_view33);
        mTxtView34 = (TextView) findViewById(R.id.new_info_txt_view34);

        mTxtView30.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        mTxtView31.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        mTxtView32.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        mTxtView34.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        mTxtView33.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));

        mTxtValue30 = (TextView) findViewById(R.id.new_info_txt_value30);
        mTxtValue31 = (TextView) findViewById(R.id.new_info_txt_value31);
        mTxtValue32 = (TextView) findViewById(R.id.new_info_txt_value32);
        mTxtValue33 = (TextView) findViewById(R.id.new_info_txt_value33);
        mTxtValue34 = (TextView) findViewById(R.id.new_info_txt_value34);
        mTxtValue30.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        mTxtValue31.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        mTxtValue32.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        mTxtValue33.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        mTxtValue34.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));


        mTxtView40 = (TextView) findViewById(R.id.new_info_txt_view40);
        mTxtValue40 = (TextView) findViewById(R.id.new_info_txt_value40);
        mTxtView40.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        mTxtValue40.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));

        mTxtView50 = (TextView) findViewById(R.id.new_info_txt_view50);
        mTxtView51 = (TextView) findViewById(R.id.new_info_txt_view51);
        mTxtView52 = (TextView) findViewById(R.id.new_info_txt_view52);
        mTxtView50.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        mTxtView51.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        mTxtView52.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));

        mTxtValue50 = (TextView) findViewById(R.id.new_info_txt_value50);
        mTxtValue51 = (TextView) findViewById(R.id.new_info_txt_value51);
        mTxtValue52 = (TextView) findViewById(R.id.new_info_txt_value52);
        mTxtValue50.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        mTxtValue51.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        mTxtValue52.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));

        mTxtView60 = (TextView) findViewById(R.id.new_info_txt_view60);
        mTxtValue60 = (TextView) findViewById(R.id.new_info_txt_value60);
        mTxtView60.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        mTxtValue60.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));


        mTxtView70 = (TextView) findViewById(R.id.new_info_txt_view70);
        mTxtView71 = (TextView) findViewById(R.id.new_info_txt_view71);
        mTxtView72 = (TextView) findViewById(R.id.new_info_txt_view72);
        mTxtView70.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        mTxtView71.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        mTxtView72.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));

        mTxtValue70 = (TextView) findViewById(R.id.new_info_txt_value70);
        mTxtValue71 = (TextView) findViewById(R.id.new_info_txt_value71);
        mTxtValue72 = (TextView) findViewById(R.id.new_info_txt_value72);
        mTxtValue70.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        mTxtValue71.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        mTxtValue72.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));

    }

    public void setData() {
        try {
            double latitude = Double.parseDouble(Constant.SELECTED_VEHICLE_LOCATION_OBJECT.getLatitude());
            double longitude = Double.parseDouble(Constant.SELECTED_VEHICLE_LOCATION_OBJECT.getLongitude());

//            new GetAddressTask(mTxtValue40).execute(latitude, longitude, 1.0);

            mTxtValue40.setText(Constant.SELECTED_VEHICLE_LOCATION_OBJECT.getAddress());

            mTxtHeader.setText(Constant.SELECTED_VEHICLE_LOCATION_OBJECT.getShortName());
            if (Constant.SELECTED_VEHICLE_LOCATION_OBJECT.getVehicleType().equalsIgnoreCase("Bus")) {
                mVehicleIcon.setImageResource(R.drawable.bus_east);

            } else if (Constant.SELECTED_VEHICLE_LOCATION_OBJECT.getVehicleType().equalsIgnoreCase("Car")) {

                mVehicleIcon.setImageResource(R.drawable.car_east);
            } else if (Constant.SELECTED_VEHICLE_LOCATION_OBJECT.getVehicleType().equalsIgnoreCase("Truck")) {

                mVehicleIcon.setImageResource(R.drawable.truck_east);
            } else if (Constant.SELECTED_VEHICLE_LOCATION_OBJECT.getVehicleType().equalsIgnoreCase("Cycle")) {

                mVehicleIcon.setImageResource(R.drawable.cycle_east);
            } else if (Constant.SELECTED_VEHICLE_LOCATION_OBJECT.getVehicleType().equalsIgnoreCase("Jcb")) {

                mVehicleIcon.setImageResource(R.drawable.jcb_icon);
            } else if (Constant.SELECTED_VEHICLE_LOCATION_OBJECT.getVehicleType().equalsIgnoreCase("Bike")) {

                mVehicleIcon.setImageResource(R.drawable.vehicle_bike);
            }else if (Constant.SELECTED_VEHICLE_LOCATION_OBJECT.getVehicleType().equalsIgnoreCase("HeavyVehicle")) {

                mVehicleIcon.setImageResource(R.drawable.truck_east);
            } else if (Constant.SELECTED_VEHICLE_LOCATION_OBJECT.getVehicleType().equalsIgnoreCase("Ambulance")) {

                mVehicleIcon.setImageResource(R.drawable.ambulance);
            }
            else if (Constant.SELECTED_VEHICLE_LOCATION_OBJECT.getVehicleType().equalsIgnoreCase("Van")) {

                mVehicleIcon.setImageResource(R.drawable.van1);
            }
            if (Constant.SELECTED_VEHICLE_LOCATION_OBJECT.getPosition().equalsIgnoreCase("P")){
                mVehicleIcon.setColorFilter(getResources().getColor(R.color.black));

            } else if (Constant.SELECTED_VEHICLE_LOCATION_OBJECT.getPosition().equalsIgnoreCase("M")){
                mVehicleIcon.setColorFilter(getResources().getColor(R.color.green));
            }else if (Constant.SELECTED_VEHICLE_LOCATION_OBJECT.getPosition().equalsIgnoreCase("U")){
                mVehicleIcon.setColorFilter(getResources().getColor(R.color.red));
            }else if (Constant.SELECTED_VEHICLE_LOCATION_OBJECT.getPosition().equalsIgnoreCase("S")){
                mVehicleIcon.setColorFilter(getResources().getColor(R.color.yellow));
            } else if (Constant.SELECTED_VEHICLE_LOCATION_OBJECT.getPosition().equalsIgnoreCase("N")){
                mVehicleIcon.setColorFilter(getResources().getColor(R.color.grey));
            }


            if (Constant.SELECTED_VEHICLE_LOCATION_OBJECT.getPosition().equalsIgnoreCase("P")) {
                mTxtView12.setText("Parked Time");
                mTxtValue12.setText(cons.getVehicleTime(String.valueOf(Constant.SELECTED_VEHICLE_LOCATION_OBJECT.getParkedTime())));
            } else if (Constant.SELECTED_VEHICLE_LOCATION_OBJECT.getPosition().equalsIgnoreCase("M")) {
                mTxtView12.setText("Moving Time");
                mTxtValue12.setText(cons.getVehicleTime(String.valueOf(Constant.SELECTED_VEHICLE_LOCATION_OBJECT.getMovingTime())));
            } else if (Constant.SELECTED_VEHICLE_LOCATION_OBJECT.getPosition().equalsIgnoreCase("U")) {
                mTxtView12.setText("No Data Time");
                mTxtValue12.setText(cons.getVehicleTime(String.valueOf(Constant.SELECTED_VEHICLE_LOCATION_OBJECT.getNoDataTime())));
            } else if (Constant.SELECTED_VEHICLE_LOCATION_OBJECT.getPosition().equalsIgnoreCase("S")) {
                mTxtView12.setText("Idle Time");
                mTxtValue12.setText(cons.getVehicleTime(String.valueOf(Constant.SELECTED_VEHICLE_LOCATION_OBJECT.getIdleTime())));
            } else {
                mTxtView12.setVisibility(View.GONE);
                mTxtValue12.setVisibility(View.GONE);
            }

           /* if (Constant.SELECTED_VEHICLE_LOCATION_OBJECT.getLastSeen() != null) {
                mTxtLastSeenValue20.setText(Constant.SELECTED_VEHICLE_LOCATION_OBJECT.getLastSeen());
            }*/


            /*
            if not working uncomment the following
             */

            if (Constant.SELECTED_VEHICLE_LOCATION_OBJECT.getDate() != 0) {
                mTxtLastSeenValue20.setText(cons.getTripTimefromserver(String.valueOf(Constant.SELECTED_VEHICLE_LOCATION_OBJECT.getDate())));
            }


            mTxtDistanceCoveredValue.setText(String.valueOf(Constant.SELECTED_VEHICLE_LOCATION_OBJECT.getDistanceCovered()) + " KM");
            mTxtOdoDistanceValue.setText(String.valueOf(Constant.SELECTED_VEHICLE_LOCATION_OBJECT.getOdoDistance()) + " KM");

            if (Constant.SELECTED_VEHICLE_LOCATION_OBJECT.getIgnitionStatus() != null) {
                mTxtValue10.setText(Constant.SELECTED_VEHICLE_LOCATION_OBJECT.getIgnitionStatus());
            }
//            if (Constant.SELECTED_VEHICLE_LOCATION_OBJECT.getFuelLitre() != null) {
            Log.d("fueldata",""+vd.getFuel()+" "+vd.getSensorCount()+" "+vd.getFuelLitres());

            if(vd.getSensorCount()>1&&vd.getFuel().equalsIgnoreCase("yes")){
                try {
                    String split[] = vd.getFuelLitres().split(":");
                    double fuel1=Double.parseDouble(split[0]);
                    double fuel2=Double.parseDouble(split[1]);
                    Log.d("fueldata"," fuel "+(int) fuel1+" "+(int) fuel2);
                    if(fuel1>0){
                        mTxtValue32.setVisibility(View.VISIBLE);
                        mTxtView32.setVisibility(View.VISIBLE);
                        mTxtView32.setText("Fuel Level (Sensor 1) " );
                        mTxtValue32.setText((int) fuel1 + " ltr ");
                    }
                    if(fuel2>0){
                        mTxtValue34.setVisibility(View.VISIBLE);
                        mTxtView34.setVisibility(View.VISIBLE);
                        mTxtView34.setText("Fuel Level (Sensor 2) " );
                        mTxtValue34.setText((int) fuel2 + " ltr " );
                    }
                }
                catch (Exception e){
                    Log.d("fueldata","er "+e.getMessage());
                }
            }
            else {
                mTxtValue32.setText(Constant.SELECTED_VEHICLE_LOCATION_OBJECT.getFuelLitre() + " Ltrs");
                mTxtValue34.setVisibility(View.GONE);
                mTxtView34.setVisibility(View.GONE);
            }
//            }
            if (Constant.SELECTED_VEHICLE_LOCATION_OBJECT.getPowerStatus() != null) {
                Log.d("powerstatus",""+Constant.SELECTED_VEHICLE_LOCATION_OBJECT.getPowerStatus());
                if (Constant.SELECTED_VEHICLE_LOCATION_OBJECT.getPowerStatus().equalsIgnoreCase("1")){
                    mTxtValue11.setText("ON");
                } else if (Constant.SELECTED_VEHICLE_LOCATION_OBJECT.getPowerStatus().equalsIgnoreCase("0")){
                    mTxtValue11.setText("OFF");
                } else if (Constant.SELECTED_VEHICLE_LOCATION_OBJECT.getPowerStatus().equalsIgnoreCase("OFF")){
                    mTxtValue11.setText("OFF");
                } else{
                    mTxtValue11.setText(Constant.SELECTED_VEHICLE_LOCATION_OBJECT.getPowerStatus());
                }
            }


            if (Constant.SELECTED_VEHICLE_LOCATION_OBJECT.getFuel() != null) {
                mTxtValue70.setText(Constant.SELECTED_VEHICLE_LOCATION_OBJECT.getFuel());
            }

            if (Constant.SELECTED_VEHICLE_LOCATION_OBJECT.getTotalTruck() != null) {
                mTxtValue71.setText(Constant.SELECTED_VEHICLE_LOCATION_OBJECT.getTotalTruck());
            }

//            if (Constant.SELECTED_VEHICLE_LOCATION_OBJECT.getTemperature() != 0) {
            mTxtValue72.setText(String.valueOf(Constant.SELECTED_VEHICLE_LOCATION_OBJECT.getTemperature()));
//            }


            mTxtValue30.setText(String.valueOf(Constant.SELECTED_VEHICLE_LOCATION_OBJECT.getOverSpeedLimit()) + " KM");
            mTxtValue31.setText(String.valueOf(Constant.SELECTED_VEHICLE_LOCATION_OBJECT.getSpeed()) + " KM");
            if (Constant.SELECTED_AC_STATUS.trim().equalsIgnoreCase("yes")) {
                mTxtValue33.setText("ON");
            } else {
                mTxtValue33.setText("OFF");
            }


            mTxtValue50.setText(cons.getVehicleTime(String.valueOf(Constant.SELECTED_VEHICLE_LOCATION_OBJECT.getParkedTime())));
            mTxtValue51.setText(cons.getVehicleTime(String.valueOf(Constant.SELECTED_VEHICLE_LOCATION_OBJECT.getIdleTime())));
            mTxtValue52.setText(cons.getVehicleTime(String.valueOf(Constant.SELECTED_VEHICLE_LOCATION_OBJECT.getMovingTime())));

//            mTxtValue70.setText(cons.getVehicleTime(String.valueOf(Constant.SELECTED_VEHICLE_LOCATION_OBJECT.gette())));
            String mDriverName = null;
            if (Constant.SELECTED_VEHICLE_LOCATION_OBJECT.getDriverName() != null) {
                mDriverName = Constant.SELECTED_VEHICLE_LOCATION_OBJECT.getDriverName();
            }

            if (Constant.SELECTED_VEHICLE_LOCATION_OBJECT.getDriverMobile() != null) {

                if (isValidMobile(Constant.SELECTED_VEHICLE_LOCATION_OBJECT.getDriverMobile())) {
                    mDriverName = "\n" + Constant.SELECTED_VEHICLE_LOCATION_OBJECT.getDriverMobile();
                }
            }
            if (mDriverName != null) {
                mTxtView60.setText(mDriverName.trim());
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private boolean isValidMobile(String phone2) {
        boolean check = false;
        if (!Pattern.matches("[a-zA-Z]+", phone2)) {
            if (phone2.length() < 6 || phone2.length() > 13) {
                check = false;
//                txtPhone.setError("Not Valid Number");
            } else {
                check = true;
            }
        } else {
            check = false;
        }
        return check;
    }


    private class GetAddressTask extends AsyncTask<Double, String, String> {
        TextView currenta_adddress_view;
        double getlat, getlng;
        double address_type;

        public GetAddressTask(TextView txtview) {
            super();
            currenta_adddress_view = txtview;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        /**
         * Get a Geocoder instance, get the latitude and longitude
         * look up the address, and return it
         *
         * @return A string containing the address of the current
         * location, or an empty string if no address can be found,
         * or an error message
         * @params params One or more Location objects
         */
        @Override
        protected String doInBackground(Double... params) {
            getlat = params[0];
            getlng = params[1];
            address_type = params[2];
            Geocoder geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());
            List<Address> addresses = null;
            try {
                addresses = geocoder.getFromLocation(params[0], params[1], 1);
            } catch (IOException e1) {
                e1.printStackTrace();
            } catch (IllegalArgumentException e2) {
                // Error message to post in the log
                String errorString = "Illegal arguments " +
                        Double.toString(params[0]) +
                        " , " +
                        Double.toString(params[1]) +
                        " passed to address service";
                e2.printStackTrace();
                return errorString;
            } catch (NullPointerException np) {
                // TODO Auto-generated catch block
                np.printStackTrace();
            }
            // If the reverse geocode returned an address
            if (addresses != null && addresses.size() > 0) {
                // Get the first address
                Address address = addresses.get(0);
                /*
                 * Format the first line of address (if available),
                 * city, and country name.
                 */
                String addressText = null;
                StringBuffer addr = new StringBuffer();
                for (int i = 0; i < address.getMaxAddressLineIndex(); i++) {
                    addressText = address.getAddressLine(i);
                    addr.append(addressText + ",");
                }
                // Return the text
                return addr.toString();
            } else {
                return "No address found";
            }
        }

        protected void onProgressUpdate(String... progress) {
        }

        protected void onPostExecute(String result) {
            // sourceedit.setText(result);
            if (result != null && !result.isEmpty()) {
                if (!result.equals("IO Exception trying to get address") || !result.equals("No address found")) {
                    /***
                     * 1.0 - vehicles current address when a vehicle is clicked
                     * 2.0 - vehicles origin address in history selection
                     * 3.0 - vehicles destination address in history selection
                     * 6.0 - vehicles current address when pause button is clicked in history selection
                     */
                    if (address_type == 1.0) {
                        currenta_adddress_view.setText(result.equals("null") ? "No Data" : result);
                    } else if (address_type == 5.0) {
                        currenta_adddress_view.setText(result.equals("null") ? "No Data" : result);
                    } else if (address_type == 6.0) {
                        currenta_adddress_view.setText(result.equals("null") ? "No Data" : result);
                    }
                }
            } else {
                Toast empty_fav = Toast.makeText(getApplicationContext(), "Please Try Again", Toast.LENGTH_LONG);
                empty_fav.show();
            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        startActivity(new Intent(NewVehicleInfoActivity.this, VehicleListActivity.class));
        finish();

    }

    public void screenArrange() {
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        int height = metrics.heightPixels;
        int width = metrics.widthPixels;
        Constant.ScreenWidth = width;
        Constant.ScreenHeight = height;

        LinearLayout.LayoutParams headerLayoutParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        headerLayoutParams.width = width;
        headerLayoutParams.height = height * 8 / 100;
        mLinearHeaderLayout.setLayoutParams(headerLayoutParams);

        LinearLayout.LayoutParams backImageParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        backImageParams.width = width * 15 / 100;
        backImageParams.height = height * 8 / 100;
        backImageParams.gravity = Gravity.CENTER;
        mBackImage.setLayoutParams(backImageParams);
        mBackImage.setPadding(width * 1 / 100, height * 1 / 100, width * 1 / 100, height * 1 / 100);

        LinearLayout.LayoutParams headTxtParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        headTxtParams.width = width * 70 / 100;
        headTxtParams.height = height * 8 / 100;
        mTxtHeader.setLayoutParams(headTxtParams);
        mTxtHeader.setPadding(width * 2 / 100, 0, 0, 0);
        mTxtHeader.setGravity(Gravity.CENTER | Gravity.LEFT);

        LinearLayout.LayoutParams vehicleIconParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        vehicleIconParams.width = width * 15 / 100;
        vehicleIconParams.height = height * 8 / 100;
        vehicleIconParams.gravity = Gravity.CENTER;
        mVehicleIcon.setLayoutParams(backImageParams);
        mVehicleIcon.setPadding(width * 1 / 100, height * 1 / 100, width * 1 / 100, height * 1 / 100);

        LinearLayout.LayoutParams linearMainParams1 = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        linearMainParams1.width = width * 94 / 100;
        linearMainParams1.height = height * 21 / 100;
        linearMainParams1.setMargins(width * 3 / 100, width * 3 / 100, width * 3 / 100, width * 3 / 2 / 100);
        mLinearLayout1.setLayoutParams(linearMainParams1);


        LinearLayout.LayoutParams linearSubParams10 = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        linearSubParams10.width = width * 47 / 100;
        linearSubParams10.height = height * 21 / 100;
        mSubLinearLayout1.setLayoutParams(linearSubParams10);

        LinearLayout.LayoutParams txtViewParams10 = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        txtViewParams10.width = width * 47 / 100;
        txtViewParams10.height = height * 7 / 100;
        mTxtView10.setLayoutParams(txtViewParams10);
        mTxtView10.setPadding(width * 3 / 100, height * 1 / 2 / 100, 0, 0);
        mTxtView10.setGravity(Gravity.CENTER | Gravity.LEFT);

        LinearLayout.LayoutParams txtViewParams11 = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        txtViewParams11.width = width * 47 / 100;
        txtViewParams11.height = height * 7 / 100;
        mTxtView11.setLayoutParams(txtViewParams11);
        mTxtView11.setPadding(width * 3 / 100, height * 1 / 2 / 100, 0, 0);
        mTxtView11.setGravity(Gravity.CENTER | Gravity.LEFT);

        LinearLayout.LayoutParams txtViewParams12 = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        txtViewParams12.width = width * 47 / 100;
        txtViewParams12.height = height * 7 / 100;
        mTxtView12.setLayoutParams(txtViewParams12);
        mTxtView12.setPadding(width * 3 / 100, height * 1 / 2 / 100, 0, 0);
        mTxtView12.setGravity(Gravity.CENTER | Gravity.LEFT);

        LinearLayout.LayoutParams linearSubParams11 = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        linearSubParams11.width = width * 47 / 100;
        linearSubParams11.height = height * 21 / 100;
        mSubLinearLayout2.setLayoutParams(linearSubParams11);

        LinearLayout.LayoutParams txtValueParams10 = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        txtValueParams10.width = width * 47 / 100;
        txtValueParams10.height = height * 7 / 100;
        mTxtValue10.setLayoutParams(txtValueParams10);
        mTxtValue10.setPadding(width * 1 / 100, height * 1 / 2 / 100, width * 2 / 100, 0);
        mTxtValue10.setGravity(Gravity.CENTER | Gravity.RIGHT);

        LinearLayout.LayoutParams txtValueParams11 = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        txtValueParams11.width = width * 47 / 100;
        txtValueParams11.height = height * 7 / 100;
        mTxtValue11.setLayoutParams(txtValueParams11);
        mTxtValue11.setPadding(width * 1 / 100, height * 1 / 2 / 100, width * 2 / 100, 0);
        mTxtValue11.setGravity(Gravity.CENTER | Gravity.RIGHT);

        LinearLayout.LayoutParams txtValueParams12 = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        txtValueParams12.width = width * 47 / 100;
        txtValueParams12.height = height * 7 / 100;
        mTxtValue12.setLayoutParams(txtValueParams12);
        mTxtValue12.setPadding(width * 1 / 100, height * 1 / 2 / 100, width * 2 / 100, 0);
        mTxtValue12.setGravity(Gravity.CENTER | Gravity.RIGHT);


        LinearLayout.LayoutParams linearMainParams2 = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        linearMainParams2.width = width * 94 / 100;
        linearMainParams2.height = height * 21 / 100;

        linearMainParams2.setMargins(width * 3 / 100, width * 3 / 2 / 100, width * 3 / 100, width * 3 / 2 / 100);

        mLinearLayout2.setLayoutParams(linearMainParams2);
        //   mLinearLayout2.setPadding(width * 2 / 100, width * 1 / 100, 0, 0);
        // mLinearLayout22.setLayoutParams(linearMainParams2);


        LinearLayout.LayoutParams linearMainParams22 = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        linearMainParams22.width = width * 47 / 100;
        linearMainParams22.height = height * 21 / 100;

        // linearMainParams2.setMargins(width * 1 / 100, width * 1 / 100, width * 1 / 100, width * 1 / 100);

        mLinearLayout21.setLayoutParams(linearMainParams22);
        mLinearLayout22.setLayoutParams(linearMainParams22);
        // mLinearLayout21.setPadding(width * 1 / 100, width * 1 / 100, width * 1 / 100, width * 1 / 100);
        // mLinearLayout22.setPadding(width * 1 / 100, width * 1 / 100, width * 1 / 100, width * 1 / 100);

        LinearLayout.LayoutParams txtViewParams20 = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        txtViewParams20.width = width * 47 / 100;
        txtViewParams20.height = height * 7 / 100;
        // txtViewParams20.setMargins(width * 1 / 100, width * 1 / 2 / 100, width * 1 / 100, width * 1 / 2 / 100);
        mTxtDistanceCoveredView20.setLayoutParams(txtViewParams20);
        mTxtDistanceCoveredView20.setGravity(Gravity.LEFT | Gravity.CENTER);
        mTxtDistanceCoveredView20.setPadding(width * 3 / 100, width * 1 / 100, width * 1 / 100, width * 1 / 100);

        mTxtDistanceCoveredValue.setLayoutParams(txtViewParams20);
        mTxtDistanceCoveredValue.setGravity(Gravity.RIGHT | Gravity.CENTER);
        mTxtDistanceCoveredValue.setPadding(width * 1 / 100, width * 1 / 100, width * 2 / 100, width * 1 / 100);

        mTxtOdoDistance.setLayoutParams(txtViewParams20);
        mTxtOdoDistance.setGravity(Gravity.LEFT | Gravity.CENTER);
        mTxtOdoDistance.setPadding(width * 3 / 100, width * 1 / 100, width * 1 / 100, width * 1 / 100);

        mTxtOdoDistanceValue.setLayoutParams(txtViewParams20);
        mTxtOdoDistanceValue.setGravity(Gravity.RIGHT | Gravity.CENTER);
        mTxtOdoDistanceValue.setPadding(width * 1 / 100, width * 1 / 100, width * 2 / 100, width * 1 / 100);

        // mTxtDistanceCoveredValue.setVisibility(View.GONE);
        // mTxtDistanceCoveredView20.setVisibility(View.GONE);
        // mTxtDistanceCoveredView20.setPadding(width * 1 / 2 / 100, height * 1 / 2 / 100, width * 1 / 100, 0);

        LinearLayout.LayoutParams txtValueParams20 = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        txtValueParams20.width = width * 47 / 100;
        txtValueParams20.height = height * 7 / 100;
        // txtValueParams20.setMargins(width * 1 / 100, width * 1 / 2 / 100, width * 1 / 100, width * 1 / 2 / 100);
        mTxtLastSeenValue20.setLayoutParams(txtValueParams20);
        mTxtLastSeen.setLayoutParams(txtValueParams20);
        mTxtLastSeenValue20.setGravity(Gravity.RIGHT | Gravity.CENTER);
        mTxtLastSeen.setGravity(Gravity.LEFT | Gravity.CENTER);
        mTxtLastSeen.setPadding(width * 3 / 100, width * 1 / 100, width * 1 / 100, width * 1 / 100);
        mTxtLastSeenValue20.setPadding(width * 1 / 100, width * 1 / 100, width * 2 / 100, width * 1 / 100);

//        mTxtLastSeenValue20.setPadding(width * 1 / 100, height * 1 / 2 / 100, width * 1 / 2 / 100, 0);

        LinearLayout.LayoutParams linearMainParams3 = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        linearMainParams3.width = width * 94 / 100;
//        linearMainParams3.height = height * 40 / 100;
        linearMainParams3.setMargins(width * 3 / 100, width * 3 / 2 / 100, width * 3 / 100, width * 3 / 2 / 100);
        mLinearLayout3.setLayoutParams(linearMainParams3);
        // mLinearLayout3.setPadding(width * 1 / 100, width * 1 / 100, width * 1 / 100, width * 1 / 100);

        LinearLayout.LayoutParams linearSubParams20 = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        linearSubParams20.width = width * 47 / 100;
        linearSubParams20.height = height * 35 / 100;
        mSubLinearLayout3.setLayoutParams(linearSubParams20);


        LinearLayout.LayoutParams txtViewParams30 = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        txtViewParams30.width = width * 47 / 100;
        txtViewParams30.height = height * 7 / 100;
        mTxtView30.setLayoutParams(txtViewParams30);
        mTxtView30.setGravity(Gravity.CENTER | Gravity.LEFT);
        // mTxtView32.setPadding(width * 1 / 2 / 100, height * 1 / 2 / 100, width * 1 / 2 / 100, 0);
        mTxtView30.setPadding(width * 3 / 100, width * 1 / 100, 0, 0);

        // mTxtView30.setPadding(width * 1 / 2 / 100, height * 1 / 2 / 100, width * 1 / 2 / 100, 0);

        LinearLayout.LayoutParams txtViewParams31 = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        txtViewParams31.width = width * 47 / 100;
        txtViewParams31.height = height * 7 / 100;
        mTxtView31.setLayoutParams(txtViewParams31);
        mTxtView31.setGravity(Gravity.CENTER | Gravity.LEFT);
        // mTxtView31.setPadding(width * 1 / 2 / 100, height * 1 / 2 / 100, width * 1 / 2 / 100, 0);
        mTxtView31.setPadding(width * 3 / 100, width * 1 / 100, 0, 0);

        LinearLayout.LayoutParams txtViewParams32 = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        txtViewParams32.width = width * 47 / 100;
        txtViewParams32.height = height * 7 / 100;
        mTxtView32.setLayoutParams(txtViewParams32);
        mTxtView32.setGravity(Gravity.CENTER | Gravity.LEFT);
        // mTxtView32.setPadding(width * 1 / 2 / 100, height * 1 / 2 / 100, width * 1 / 2 / 100, 0);
        mTxtView32.setPadding(width * 3 / 100, width * 1 / 100, 0, width * 1 / 100);

        LinearLayout.LayoutParams txtViewParams34 = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        txtViewParams34.width = width * 47 / 100;
        txtViewParams34.height = height * 7 / 100;
        mTxtView34.setLayoutParams(txtViewParams34);
        mTxtView34.setGravity(Gravity.CENTER | Gravity.LEFT);
        // mTxtView32.setPadding(width * 1 / 2 / 100, height * 1 / 2 / 100, width * 1 / 2 / 100, 0);
        mTxtView34.setPadding(width * 3 / 100, width * 1 / 100, 0, width * 1 / 100);

        LinearLayout.LayoutParams txtViewParams33 = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        txtViewParams33.width = width * 47 / 100;
        txtViewParams33.height = height * 7 / 100;
        mTxtView33.setLayoutParams(txtViewParams33);
//        mTxtView33.setGravity(Gravity.LEFT);
        mTxtView33.setGravity(Gravity.CENTER | Gravity.LEFT);
        mTxtView33.setPadding(width * 3 / 100, width * 1 / 100, 0, width * 1 / 100);
        // mTxtView33.setPadding(width * 1 / 2 / 100, height * 1 / 2 / 100, width * 1 / 2 / 100, 0);

        LinearLayout.LayoutParams linearSubParams21 = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        linearSubParams21.width = width * 47 / 100;
        linearSubParams21.height = height * 35 / 100;
        mSubLinearLayout4.setLayoutParams(linearSubParams21);


        LinearLayout.LayoutParams txtValueParams30 = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        txtValueParams30.width = width * 47 / 100;
        txtValueParams30.height = height * 7 / 100;
        mTxtValue30.setLayoutParams(txtValueParams30);
        mTxtValue30.setPadding(width * 1 / 100, width * 1 / 100, width * 2 / 100, 0);
        mTxtValue30.setGravity(Gravity.CENTER | Gravity.RIGHT);

        LinearLayout.LayoutParams txtValueParams31 = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        txtValueParams31.width = width * 47 / 100;
        txtValueParams31.height = height * 7 / 100;
        mTxtValue31.setLayoutParams(txtValueParams31);
        mTxtValue31.setPadding(width * 1 / 100, width * 1 / 100, width * 2 / 100, 0);
        mTxtValue31.setGravity(Gravity.CENTER | Gravity.RIGHT);

        LinearLayout.LayoutParams txtValueParams32 = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        txtValueParams32.width = width * 47 / 100;
        txtValueParams32.height = height * 7 / 100;
        mTxtValue32.setLayoutParams(txtValueParams32);
        mTxtValue32.setPadding(width * 1 / 100, width * 1 / 100, width * 2 / 100, width * 1 / 100);
        mTxtValue32.setGravity(Gravity.CENTER | Gravity.RIGHT);

        LinearLayout.LayoutParams txtValueParams34 = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        txtValueParams34.width = width * 47 / 100;
        txtValueParams34.height = height * 7 / 100;
        mTxtValue34.setLayoutParams(txtValueParams34);
        mTxtValue34.setPadding(width * 1 / 100, width * 1 / 100, width * 2 / 100, width * 1 / 100);
        mTxtValue34.setGravity(Gravity.CENTER | Gravity.RIGHT);

        LinearLayout.LayoutParams txtValueParams33 = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        txtValueParams33.width = width * 47 / 100;
        txtValueParams33.height = height * 7 / 100;
        mTxtValue33.setLayoutParams(txtValueParams33);
        mTxtValue33.setPadding(width * 1 / 100, width * 1 / 100, width * 2 / 100, width * 1 / 100);
        mTxtValue33.setGravity(Gravity.CENTER | Gravity.RIGHT);



        LinearLayout.LayoutParams linearMainParams4 = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        linearMainParams4.width = width * 94 / 100;
        //linearMainParams4.height = height * 7 / 100;
        linearMainParams4.setMargins(width * 3 / 100, width * 3 / 2 / 100, width * 3 / 100, width * 3 / 2 / 100);
        mLinearLayout4.setLayoutParams(linearMainParams4);

        LinearLayout.LayoutParams txtViewParams40 = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        txtViewParams40.width = width * 47 / 100;
        //  txtViewParams40.height = height * 7 / 100;
        mTxtView40.setLayoutParams(txtViewParams40);
        mTxtView40.setGravity(Gravity.CENTER);
        mTxtView40.setPadding(width * 1 / 2 / 100, width * 1 / 2 / 100, width * 1 / 2 / 100, 0);

        LinearLayout.LayoutParams txtValueParams40 = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        txtValueParams40.width = width * 47 / 100;
        // txtValueParams40.height = height * 7 / 100;
        mTxtValue40.setLayoutParams(txtValueParams40);
        mTxtValue40.setGravity(Gravity.CENTER);

        mTxtValue40.setPadding(width * 1 / 2 / 100, width * 1 / 2 / 100, width * 1 / 100, 0);

        LinearLayout.LayoutParams linearMainParams5 = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        linearMainParams5.width = width;
        linearMainParams5.height = height * 15 / 100;
        mLinearLayout5.setLayoutParams(linearMainParams5);

        LinearLayout.LayoutParams linearSubParams30 = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        linearSubParams30.width = width * 50 / 100;
        linearSubParams30.height = height * 15 / 100;
        mSubLinearLayout5.setLayoutParams(linearSubParams30);

        LinearLayout.LayoutParams txtViewParams50 = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        txtViewParams50.width = width * 50 / 100;
        txtViewParams50.height = height * 5 / 100;
        mTxtView50.setLayoutParams(txtViewParams50);
        mTxtView50.setPadding(width * 1 / 2 / 100, width * 1 / 2 / 100, width * 1 / 2 / 100, 0);

        LinearLayout.LayoutParams txtViewParams51 = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        txtViewParams51.width = width * 50 / 100;
        txtViewParams51.height = height * 5 / 100;
        mTxtView51.setLayoutParams(txtViewParams51);
        mTxtView51.setPadding(width * 1 / 2 / 100, width * 1 / 2 / 100, width * 1 / 2 / 100, 0);

        LinearLayout.LayoutParams txtViewParams52 = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        txtViewParams52.width = width * 50 / 100;
        txtViewParams52.height = height * 5 / 100;
        mTxtView52.setLayoutParams(txtViewParams52);
        mTxtView52.setPadding(width * 1 / 2 / 100, width * 1 / 2 / 100, width * 1 / 2 / 100, 0);

        LinearLayout.LayoutParams linearSubParams31 = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        linearSubParams31.width = width * 50 / 100;
        linearSubParams31.height = height * 15 / 100;
        mSubLinearLayout6.setLayoutParams(linearSubParams31);

        LinearLayout.LayoutParams txtValueParams50 = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        txtValueParams50.width = width * 50 / 100;
        txtValueParams50.height = height * 5 / 100;
        mTxtValue50.setLayoutParams(txtValueParams50);
        mTxtValue50.setPadding(width * 1 / 2 / 100, width * 1 / 2 / 100, width * 1 / 2 / 100, 0);

        LinearLayout.LayoutParams txtValueParams51 = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        txtValueParams51.width = width * 50 / 100;
        txtValueParams51.height = height * 5 / 100;
        mTxtValue51.setLayoutParams(txtValueParams51);
        mTxtValue51.setPadding(width * 1 / 2 / 100, width * 1 / 2 / 100, width * 1 / 2 / 100, 0);

        LinearLayout.LayoutParams txtValueParams52 = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        txtValueParams52.width = width * 50 / 100;
        txtValueParams52.height = height * 5 / 100;
        mTxtValue52.setLayoutParams(txtValueParams52);
        mTxtValue52.setPadding(width * 1 / 2 / 100, width * 1 / 2 / 100, width * 1 / 2 / 100, 0);


        LinearLayout.LayoutParams linearMainParams6 = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        linearMainParams6.width = width * 94 / 100;
        linearMainParams6.height = height * 7 / 100;
        linearMainParams6.setMargins(width * 3 / 100, width * 3 / 3 / 100, width * 3 / 100, width * 3 / 2 / 100);
        mLinearLayout6.setLayoutParams(linearMainParams6);

        LinearLayout.LayoutParams txtViewParams60 = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        txtViewParams60.width = width * 33 / 100;
        txtViewParams60.height = height * 7 / 100;
        mTxtView60.setLayoutParams(txtViewParams60);
        mTxtView60.setPadding(width * 1 / 2 / 100, width * 1 / 2 / 100, width * 1 / 2 / 100, 0);
        mTxtView60.setGravity(Gravity.CENTER | Gravity.LEFT);

        LinearLayout.LayoutParams imgViewParams1 = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        imgViewParams1.width = width * 14 / 100;
        imgViewParams1.height = height * 7 / 100;
        imgViewParams1.gravity = Gravity.CENTER;
        mImgView1.setLayoutParams(imgViewParams1);
        //  mImgView1.setPadding(width * 1 / 100, height * 1 / 100, width * 1 / 100, height * 1 / 100);

        LinearLayout.LayoutParams txtValueParams60 = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        txtValueParams60.width = width * 33 / 100;
        txtValueParams60.height = height * 7 / 100;
        mTxtValue60.setLayoutParams(txtValueParams60);
        mTxtValue60.setPadding(width * 1 / 2 / 100, width * 1 / 2 / 100, width * 1 / 100, 0);
        mTxtValue60.setGravity(Gravity.CENTER | Gravity.LEFT);

        LinearLayout.LayoutParams imgViewParams2 = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        imgViewParams2.width = width * 14 / 100;
        imgViewParams2.height = height * 7 / 100;
        imgViewParams2.gravity = Gravity.CENTER;
        mImgView2.setLayoutParams(imgViewParams2);
        //mImgView2.setPadding(width * 1 / 100, height * 1 / 100, width * 1 / 100, height * 1 / 100);

        LinearLayout.LayoutParams linearMainParams7 = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        linearMainParams7.width = width * 94 / 100;
        linearMainParams7.height = height * 21 / 100;
        linearMainParams7.setMargins(width * 3 / 100, width * 3 / 2 / 100, width * 3 / 100, width * 3 / 100);
        mLinearLayout7.setLayoutParams(linearMainParams7);

        LinearLayout.LayoutParams linearSubParams40 = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        linearSubParams40.width = width * 47 / 100;
        linearSubParams40.height = height * 21 / 100;
        mSubLinearLayout7.setLayoutParams(linearSubParams40);

        LinearLayout.LayoutParams txtViewParams70 = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        txtViewParams70.width = width * 47 / 100;
        txtViewParams70.height = height * 7 / 100;
        mTxtView70.setLayoutParams(txtViewParams70);
        mTxtView70.setPadding(width * 3 / 100, width * 1 / 100, width * 1 / 100, 0);
        mTxtView70.setGravity(Gravity.CENTER | Gravity.LEFT);

        LinearLayout.LayoutParams txtViewParams71 = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        txtViewParams71.width = width * 47 / 100;
        txtViewParams71.height = height * 7 / 100;
        mTxtView71.setLayoutParams(txtViewParams71);
        mTxtView71.setPadding(width * 3 / 100, width * 1 / 100, width * 1 / 100, 0);
        mTxtView71.setGravity(Gravity.CENTER | Gravity.LEFT);

        LinearLayout.LayoutParams txtViewParams72 = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        txtViewParams72.width = width * 47 / 100;
        txtViewParams72.height = height * 7 / 100;
        mTxtView72.setLayoutParams(txtViewParams72);
        mTxtView72.setPadding(width * 3 / 100, width * 1 / 100, width * 1 / 100, width * 1 / 100);
        mTxtView72.setGravity(Gravity.CENTER | Gravity.LEFT);

        LinearLayout.LayoutParams linearSubParams41 = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        linearSubParams41.width = width * 47 / 100;
        linearSubParams41.height = height * 21 / 100;
        mSubLinearLayout8.setLayoutParams(linearSubParams41);

        LinearLayout.LayoutParams txtValueParams70 = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        txtValueParams70.width = width * 47 / 100;
        txtValueParams70.height = height * 7 / 100;
        mTxtValue70.setLayoutParams(txtValueParams70);
        mTxtValue70.setPadding(width * 2 / 100, width * 1 / 100, width * 2 / 100, 0);
        mTxtValue70.setGravity(Gravity.CENTER | Gravity.RIGHT);

        LinearLayout.LayoutParams txtValueParams71 = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        txtValueParams71.width = width * 47 / 100;
        txtValueParams71.height = height * 7 / 100;
        mTxtValue71.setLayoutParams(txtValueParams71);
        mTxtValue71.setPadding(width * 2 / 100, width * 1 / 100, width * 2 / 100, 0);
        mTxtValue71.setGravity(Gravity.CENTER | Gravity.RIGHT);

        LinearLayout.LayoutParams txtValueParams72 = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        txtValueParams72.width = width * 47 / 100;
        txtValueParams72.height = height * 7 / 100;
        mTxtValue72.setLayoutParams(txtValueParams72);
        mTxtValue72.setPadding(width * 2 / 100, width * 1 / 100, width * 2 / 100, width * 1 / 100);
        mTxtValue72.setGravity(Gravity.CENTER | Gravity.RIGHT);

//
//        LinearLayout.LayoutParams horiViewParams1 = new LinearLayout.LayoutParams(
//                LinearLayout.LayoutParams.WRAP_CONTENT,
//                LinearLayout.LayoutParams.WRAP_CONTENT);
//        horiViewParams1.width = width ;
//        horiViewParams1.height = height * 1 / 3 / 100;
//        holder.mView1.setLayoutParams(horiViewParams1);
//
//        LinearLayout.LayoutParams horiViewParams2 = new LinearLayout.LayoutParams(
//                LinearLayout.LayoutParams.WRAP_CONTENT,
//                LinearLayout.LayoutParams.WRAP_CONTENT);
//        horiViewParams2.width = width ;
//        horiViewParams2.height = height * 1 / 3 / 100;
//        holder.mView1.setLayoutParams(horiViewParams2);
//
//        LinearLayout.LayoutParams horiViewParams2 = new LinearLayout.LayoutParams(
//                LinearLayout.LayoutParams.WRAP_CONTENT,
//                LinearLayout.LayoutParams.WRAP_CONTENT);
//        horiViewParams2.width = width ;
//        horiViewParams1.height = height * 1 / 3 / 100;
//        holder.mView1.setLayoutParams(horiViewParams1);
//
//        LinearLayout.LayoutParams horiViewParams1 = new LinearLayout.LayoutParams(
//                LinearLayout.LayoutParams.WRAP_CONTENT,
//                LinearLayout.LayoutParams.WRAP_CONTENT);
//        horiViewParams1.width = width ;
//        horiViewParams1.height = height * 1 / 3 / 100;
//        holder.mView1.setLayoutParams(horiViewParams1);
//
//        LinearLayout.LayoutParams horiViewParams1 = new LinearLayout.LayoutParams(
//                LinearLayout.LayoutParams.WRAP_CONTENT,
//                LinearLayout.LayoutParams.WRAP_CONTENT);
//        horiViewParams1.width = width ;
//        horiViewParams1.height = height * 1 / 3 / 100;
//        holder.mView1.setLayoutParams(horiViewParams1);
//
        LinearLayout.LayoutParams horiViewParams1 = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        horiViewParams1.width = width;
        horiViewParams1.height = height * 1 / 3 / 100;
        mVeriView1.setLayoutParams(horiViewParams1);

        LinearLayout.LayoutParams veriViewParams1 = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        veriViewParams1.width = width * 1 / 2 / 100;
//        veriViewParams1.height = height * 7 / 100;
        //   mVeriView1.setLayoutParams(veriViewParams1);
        mVeriView2.setLayoutParams(veriViewParams1);
        mVeriView3.setLayoutParams(veriViewParams1);


        if (width >= 600) {

        } else if (width > 501 && width < 600) {


        } else if (width > 260 && width < 500) {


        } else if (width <= 260) {


        }

    }

}

