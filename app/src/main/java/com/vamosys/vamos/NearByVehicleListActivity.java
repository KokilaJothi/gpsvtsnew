package com.vamosys.vamos;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.vamosys.adapter.NearByVehiclesAdapter;
import com.vamosys.model.NearestVehicle;
import com.vamosys.utils.ConnectionDetector;
import com.vamosys.utils.Constant;
import com.vamosys.utils.TypefaceUtil;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;

public class NearByVehicleListActivity extends Activity {
    ListView lv;
    TextView mTxtSelectedVehicle, mHeadTitle;
    //    EditText mEdtSearch;
//    Spinner groupSpinner;
    View v3;
    TextView mTxtNoRecord;
    ImageView mBackArrow, mImgShowAlertDialog;

    List<NearestVehicle> mNearestVehicleList = new ArrayList<NearestVehicle>();
    //  List<ExecutiveReportData> mKmsSummaryAdapterList = new ArrayList<ExecutiveReportData>();
    //List<String> mGroupList = new ArrayList<String>();
    //String mSelectedGroup, mUserId;
    //List<String> mGroupList;
    //String mSelectedGroup;
    ConnectionDetector cd;
    Timer t;
    String mNoOfKms;

    public static double mSELCETED_NEARBY_VEHICLE_LAT = 0.0, mSELCETED_NEARBY_VEHICLE_LNG = 0.0, mSELECTED_VEHICLE_DISTANCE;
    public static String mSELECTED_VEHICLE_SHORT_NAME, mSELECTED_VEHICLE_ID, mSELECTED_VEHICLE_ADDRESS;
    public static int mCircleMeters = 10;
    SharedPreferences sp;

    //    List<KMSSummary>
    boolean isOsmEnabled = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.nearby_vehiclelist_layout);
        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        init();
        screenArrange();
        cd = new ConnectionDetector(getApplicationContext());

        sp = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        if (sp.getString("ip_adds", "") != null) {
            if (sp.getString("ip_adds", "").trim().length() > 0) {
                Const.API_URL = sp.getString("ip_adds", "");
            }
        }

        if (sp.getString("enabled_map", "") != null) {
            if (sp.getString("enabled_map", "").trim().length() > 0) {

//                System.out.println("hi enabled map is " + sp.getString("enabled_map", ""));

                if (sp.getString("enabled_map", "").equalsIgnoreCase(getResources().getString(R.string.osm))) {
                    isOsmEnabled = true;
                } else {
                    isOsmEnabled = false;
                }
            }
        }

//        Calendar cal = Calendar.getInstance();
//        cal.add(Calendar.MONTH, -1);
//        Date result = cal.getTime();
//
//        SimpleDateFormat dfDate = new SimpleDateFormat("dd-MMM-yyyy");

        // String mCurrentDate = dfDate.format(new Date());
        // String mFromDate = dfDate.format(result);
        //  fromdatevalue.setText(mFromDate);
        //  todatevalue.setText(mCurrentDate);

//        try {
//            queryUserDB();
//        } catch (SQLException e) {
//            // TODO Auto-generated catch block
//            e.printStackTrace();
//        }

//        ConnectionDetector cd = new ConnectionDetector(getApplicationContext());
        mTxtSelectedVehicle.setText(Constant.SELECTED_VEHICLE_SHORT_NAME);
        if (cd.isConnectingToInternet()) {
            mNearestVehicleList.clear();
//            loadNearbyVehicles();
            getKmsData();
        } else {
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.network_connection),
                    Toast.LENGTH_SHORT).show();
        }

        mImgShowAlertDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getKmsData();
            }
        });

        mBackArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(NearByVehicleListActivity.this, VehicleListActivity.class));
                finish();
            }
        });

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View v, int pos,
                                    long arg3) {
                // TODO Auto-generated method stub
                Constant.mUserLocationNearby = false;
                mSELCETED_NEARBY_VEHICLE_LAT = mNearestVehicleList.get(pos).getLatitude();
                mSELCETED_NEARBY_VEHICLE_LNG = mNearestVehicleList.get(pos).getLongitude();
                mSELECTED_VEHICLE_DISTANCE = mNearestVehicleList.get(pos).getDistance();
                mSELECTED_VEHICLE_SHORT_NAME = mNearestVehicleList.get(pos).getShortName();
                mSELECTED_VEHICLE_ID = mNearestVehicleList.get(pos).getVehicleId();
                mSELECTED_VEHICLE_ADDRESS = mNearestVehicleList.get(pos).getAddress();
                Constant.mUserLocationNearbyActivityCalled = false;
                showAlert();


            }
        });
    }

    public void showAlert() {
        AlertDialog.Builder builder = new AlertDialog.Builder(NearByVehicleListActivity.this);
        builder.setTitle(getResources().getString(R.string.nearby));
        builder.setMessage(getResources().getString(R.string.click_anyone)).setCancelable(false).setPositiveButton(getResources().getString(R.string.map), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                //do things
//                startActivity(new Intent(NearByVehicleListActivity.this, NearByMapViewActivity.class));
//                finish();

                if (isOsmEnabled) {
                    startActivity(new Intent(NearByVehicleListActivity.this, NearByMapViewActivity.class));
//                    finish();
                } else {
                    startActivity(new Intent(NearByVehicleListActivity.this, NearByGoogleMapViewActivity.class));
//                    finish();
                }
                dialog.cancel();
            }
        }).setNegativeButton(getResources().getString(R.string.info), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int i) {
                getSelectedVehicleObj();
                startActivity(new Intent(NearByVehicleListActivity.this, NewVehicleInfoActivity.class));
                finish();
                dialog.cancel();
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    public void getSelectedVehicleObj() {
        //VehicleData mVehicleData = new VehicleData();
        Constant.SELECTED_VEHICLE_LOCATION_OBJECT = null;
        for (int i = 0; i < Constant.VEHILCE_LIST_DATA.size(); i++) {
            // System.out.println("The selected vehicle id 000000 :::" + mSELECTED_VEHICLE_ID + " list vehicle id ::::" + Constant.VEHILCE_LIST_DATA.get(i).getVehicleId());
            if (Constant.VEHILCE_LIST_DATA.get(i).getVehicleId().trim().equalsIgnoreCase(mSELECTED_VEHICLE_ID)) {

                //  System.out.println("The selected vehicle id :::" + mSELECTED_VEHICLE_ID + " list vehicle id ::::" + Constant.VEHILCE_LIST_DATA.get(i).getVehicleId());

                Constant.SELECTED_VEHICLE_LOCATION_OBJECT = Constant.VEHILCE_LIST_DATA.get(i);

                //mVehicleData = Constant.VEHILCE_LIST_DATA.get(i);
            }

        }
//        System.out.println("The static value is :::: " + Constant.SELECTED_VEHICLE_LOCATION_OBJECT);

        // System.out.println("The selected vehicle id 222222 :::" + mSELECTED_VEHICLE_ID + " list vehicle id 11111 ::::" + mVehicleData.getVehicleId());
//        return mVehicleData;
    }

//    public void getSearchText(String ch) {
//
////        System.out.println("List size is ::::::" + mVehicleLocationListAdapter.size()
////                + " and the character is :::::" + ch);
//
//        List<ExecutiveReportData> list = new ArrayList<ExecutiveReportData>();
//        // list = null;
//        if (ch != null && ch.length() > 0) {
//
//
//            list.clear();
//
//            for (int i = 0; i < mKmsSummaryList.size(); i++) {
//                ExecutiveReportData dc = new ExecutiveReportData();
//
//
//                if (Pattern
//                        .compile(Pattern.quote(ch), Pattern.CASE_INSENSITIVE)
//                        .matcher(mKmsSummaryList.get(i).getVehicleId())
//                        .find()) {
//                    //  System.out.println("Hiiiiiii");
//                    dc = mKmsSummaryList.get(i);
//                    list.add(dc);
//                }
//
//            }
//
//        } else {
//            list = mKmsSummaryList;
//        }
//
//        // System.out.println("The searched list size is :::::" + list.size());
//
//
////        System.out.println("List size after filtered is :::::"
////                + mVehicleLocationListAdapter.size());
//
//        if (list.size() > 0) {
//            mKmsSummaryAdapterList = list;
//
//
//            if (mKmsSummaryAdapterList.size() > 0) {
//
//                KMSSummaryAdapter notiAdapter = new KMSSummaryAdapter(NearByVehicleListActivity.this, mKmsSummaryAdapterList);
////                lv.setAdapter(new NotificationListAdapter(
////                        NotificationListActivity.this, Constant.mPushDataList));
//
//                lv.setAdapter(notiAdapter);
//                notiAdapter.notifyDataSetChanged();
//
//                //   lv.setVisibility(View.VISIBLE);
//
//                mTxtNoRecord.setVisibility(View.GONE);
//                lv.setVisibility(View.VISIBLE);
//
//                //  mTxtView.setVisibility(View.GONE);
//                //  mSelectAllLayout.setVisibility(View.VISIBLE);
//            } else {
//                lv.setVisibility(View.GONE);
//                //mTxtView.setVisibility(View.VISIBLE);
//                // mSelectAllLayout.setVisibility(View.GONE);
//                mTxtNoRecord.setVisibility(View.VISIBLE);
//                lv.setVisibility(View.GONE);
//            }
//
//
//        } else {
////    Set no record data
//            List<ExecutiveReportData> vList = new ArrayList<ExecutiveReportData>();
//            mKmsSummaryAdapterList = vList;
//            if (mKmsSummaryAdapterList.size() > 0) {
//
//                KMSSummaryAdapter notiAdapter = new KMSSummaryAdapter(NearByVehicleListActivity.this, mKmsSummaryAdapterList);
////                lv.setAdapter(new NotificationListAdapter(
////                        NotificationListActivity.this, Constant.mPushDataList));
//
//                lv.setAdapter(notiAdapter);
//                notiAdapter.notifyDataSetChanged();
//
////                lv.setVisibility(View.VISIBLE);
//                //  mTxtView.setVisibility(View.GONE);
//                mTxtNoRecord.setVisibility(View.GONE);
//                lv.setVisibility(View.VISIBLE);
//                //  mSelectAllLayout.setVisibility(View.VISIBLE);
//            } else {
////                lv.setVisibility(View.GONE);
//
//                mTxtNoRecord.setVisibility(View.VISIBLE);
//                lv.setVisibility(View.GONE);
//                // mTxtView.setVisibility(View.VISIBLE);
//                // mSelectAllLayout.setVisibility(View.GONE);
//            }
//            // setData("No Record");
//
//        }
//        // $SearchListView.setAdapter(new DailySearchAdapter(DailySearch.this,
//        // mFilteredListDailySearch));
//
//
//    }


//    public void setDropDownData() {
//        //mGroupList = new ArrayList<String>();
//        // List<String> list = new ArrayList<String>();
//        // list.add("Select");
//        if (mGroupList.size() > 0) {
//
//        } else {
//            mGroupList.add("Select");
//        }
//
//        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(
//                NearByVehicleListActivity.this, R.layout.spinner_row,
//                mGroupList) {
//
//            public View getView(int position, View convertView, ViewGroup parent) {
//                View v = super.getView(position, convertView, parent);
//                // TextView v = (TextView) super.getView(position,
//                // convertView,parent);
//                // Constant.face(DailyCalls.this, (TextView) v);
//                return v;
//            }
//
//            public View getDropDownView(int position, View convertView,
//                                        ViewGroup parent) {
//                View v = super.getDropDownView(position, convertView, parent);
//                // ((TextView)
//                // v).setBackgroundColor(Color.parseColor("#BBfef3da"));
//                // TextView v = (TextView) super.getView(position,
//                // convertView,parent);
//                // Constant.face(DailyCalls.this, (TextView) v);
//                return v;
//            }
//        };
//        spinnerArrayAdapter
//                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//        groupSpinner.setAdapter(spinnerArrayAdapter);
//    }

//    public void loadNearbyVehicles() {
//        if (t != null) {
//            t.cancel();
//        }
//
//        if (t == null) {
//            t = new Timer();
//        }
//        t.schedule(new TimerTask() {
//            public void run() {
////                new getNearbyVehicleList().execute();
//                getKmsData();
//            }
//        }, 200);
//    }

    public void getKmsData() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(NearByVehicleListActivity.this);
        alertDialog.setTitle(getResources().getString(R.string.distance));
        alertDialog.setMessage(getResources().getString(R.string.no_kms));

//        final EditText input = new EditText(NearByVehicleListActivity.this);
//        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
//                LinearLayout.LayoutParams.MATCH_PARENT,
//                LinearLayout.LayoutParams.MATCH_PARENT);
//        input.setLayoutParams(lp);
//        input.setInputType(InputType.TYPE_CLASS_NUMBER);
//        alertDialog.setView(input);

        final EditText input;

        LayoutInflater inflater = this.getLayoutInflater();

        // Inflate and set the layout for the dialog
        // Pass null as the parent view because its going in the dialog layout
        LayoutInflater li = LayoutInflater.from(getApplicationContext());
        View promptsView = li.inflate(R.layout.textview_layout, null);
        alertDialog.setView(promptsView);

        final EditText userInput = (EditText) promptsView
                .findViewById(R.id.popup_edt_text);
//        userInput.setInputType(R.id.text1);
//        alertDialog.setIcon(R.mipmap.ic_launcher);
        alertDialog.setCancelable(false);
//        alertDialog.set
        alertDialog.setPositiveButton(getResources().getString(R.string.ok),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        if (userInput.getText().toString().trim().length() > 0) {
                            mNoOfKms = userInput.getText().toString();
                            try {
                                if (mNoOfKms != null) {
                                    mCircleMeters = Integer.valueOf(mNoOfKms) * 1000;
                                }

                                if (mNoOfKms.equalsIgnoreCase("0") || mNoOfKms.trim().length() == 0) {
                                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.correct_data), Toast.LENGTH_SHORT).show();
                                    getKmsData();
                                } else {
                                    dialog.cancel();
                                    new getNearbyVehicleList().execute();
                                }
                                InputMethodManager imm1 = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                                imm1.hideSoftInputFromWindow(userInput.getWindowToken(), 0);
                                InputMethodManager inputManager = (InputMethodManager)
                                        getSystemService(Context.INPUT_METHOD_SERVICE);

                                inputManager.hideSoftInputFromWindow(userInput.getWindowToken(),
                                        InputMethodManager.HIDE_NOT_ALWAYS);
                            }catch (Exception e){
                                e.printStackTrace();
                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.correct_data), Toast.LENGTH_SHORT).show();
                            }
//                        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
//                        inputMethodManager.hideSoftInputFromWindow(NearByVehicleListActivity.this.getCurrentFocus().getWindowToken(), 0);
                        } else {
                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.correct_data), Toast.LENGTH_SHORT).show();
                            getKmsData();
                        }


                    }
                });

        alertDialog.setNegativeButton(getResources().getString(R.string.cancel),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        startActivity(new Intent(NearByVehicleListActivity.this, VehicleListActivity.class));
                        finish();
                    }
                });

        alertDialog.show();


    }

    public void setData() {
//        mKmsSummaryAdapterList = mKmsSummaryList;
        if (mNearestVehicleList.size() > 0) {
            mTxtNoRecord.setVisibility(View.GONE);
            lv.setVisibility(View.VISIBLE);
            NearByVehiclesAdapter adapter = new NearByVehiclesAdapter(NearByVehicleListActivity.this,
                    mNearestVehicleList);
            lv.setAdapter(adapter);
        } else {
            mTxtNoRecord.setVisibility(View.VISIBLE);
            lv.setVisibility(View.GONE);
        }
    }

    private class getNearbyVehicleList extends AsyncTask<String, String, String> {
        ProgressDialog progressDialog;

        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub

            String result = null;
            try {
                Const constaccess = new Const();
                /*
New API
 */
                result = constaccess
                        .sendGet(Const.API_URL + "/mobile/getNearestVehicle?userId=" + Constant.SELECTED_USER_ID + "&kms=" + mNoOfKms + "&vehicleId=" + Constant.SELECTED_VEHICLE_ID + "&groupId="
                                + Constant.SELECTED_GROUP, 30000);
                //  + "&fromDate=2016-01-05&toDate=2016-01-30", 30000);
                        /*
                        The below is old API
                         */
//                result = constaccess
//                        .sendGet("http://vamosys.com/mobile/getExecutiveReport?userId=" + mUserId + "&groupId="
//                                + mSelectedGroup
//                                + "&fromDate=2016-01-05&toDate=2016-01-30", 30000);
                // result = ht.doPostEcpl(mLoginData, "www.test.com");
                // Constant.printMsg("Response........" + result);
            } catch (Exception e) {

            }

            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
//            getWindow().setSoftInputMode(
//                    WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
//            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
//            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
            progressDialog.dismiss();
            if (t != null) {
                t.cancel();
            }
            if (result != null && result.length() > 0) {
//                System.out.println("The kms result is ::::" + result);
                Gson g = new Gson();
                InputStream is = new ByteArrayInputStream(result.getBytes());
                Reader reader = new InputStreamReader(is);
                Type fooType = new TypeToken<List<NearestVehicle>>() {
                }.getType();
                mNearestVehicleList = g.fromJson(reader, fooType);
                setData();
            }

        }

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            progressDialog = new ProgressDialog(NearByVehicleListActivity.this,
                    AlertDialog.THEME_HOLO_LIGHT);
            progressDialog.setMessage(getResources().getString(R.string.progress_dialog));
            progressDialog.setProgressDrawable(new ColorDrawable(
                    android.graphics.Color.BLUE));
            progressDialog.setCancelable(true);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.show();
        }

    }

//    public void queryUserDB() {
//        DataBaseHandler db = new DataBaseHandler(this);
//        Cursor c = null;
//        // String qry = "SELECT DISTINCT patch_id,patch_name,mtp_id FROM "
//        // + DbHandler.TABLE_TM_DCP;
//        try {
//            // c = db.open().getDatabaseObj().rawQuery(qry, null);
//            c = db.open()
//                    .getDatabaseObj()
//                    .query(DataBaseHandler.TABLE_USER, null, null, null, null, null,
//                            null);
//            // int reporting_date_index = c
//            // .getColumnIndex("actual_reporting_date");
//            int user_id_index = c.getColumnIndex("user_id");
//            int group_list_index = c.getColumnIndex("group_list");
//            // int mtp_id_index = c.getColumnIndex("mtp_id");
//            // int dr_ch_name_index = c.getColumnIndex("dr_ch_name");
//            // System.out.println("The coubnt of daily count is ::::::"
//            // + c.getCount());
//            if (c.getCount() > 0) {
//
//                while (c.moveToNext()) {
//                    mUserId = c.getString(user_id_index);
//
////                    System.out.println("The group list is :::::"
////                            + c.getString(group_list_index));
//
//                    if (c.getString(group_list_index) != null) {
//                        Gson g = new Gson();
//                        InputStream is = new ByteArrayInputStream(c.getString(
//                                group_list_index).getBytes());
//                        Reader reader = new InputStreamReader(is);
//                        Type fooType = new TypeToken<List<String>>() {
//                        }.getType();
//
//                        mGroupList = g.fromJson(reader, fooType);
//                    }
//
//                }
//            }
//
//        } finally {
//            c.close();
//            db.close();
//        }
////        setDropDownData();
//    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        startActivity(new Intent(NearByVehicleListActivity.this, VehicleListActivity.class));
        finish();

    }

    private void screenArrange() {
        // TODO Auto-generated method stub

		/* screen arrangements */
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        int height = metrics.heightPixels;
        int width = metrics.widthPixels;
        Constant.ScreenWidth = width;
        Constant.ScreenHeight = height;

        LinearLayout.LayoutParams backImageParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        backImageParams.width = width * 15 / 100;
        backImageParams.height = height * 10 / 100;
        backImageParams.gravity = Gravity.CENTER;
        mBackArrow.setLayoutParams(backImageParams);
        mBackArrow.setPadding(width * 1 / 100, height * 1 / 100, width * 1 / 100, height * 1 / 100);

        LinearLayout.LayoutParams headTxtParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        headTxtParams.width = width * 85 / 100;
        headTxtParams.height = height * 10 / 100;
        mHeadTitle.setLayoutParams(headTxtParams);
        mHeadTitle.setPadding(width * 2 / 100, 0, 0, 0);
        mHeadTitle.setGravity(Gravity.CENTER | Gravity.LEFT);

        LinearLayout.LayoutParams textParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        textParams.width = width * 75 / 100;
//        textParams.height = height * 6 / 100;
        textParams.topMargin = (int) (height * 0.25 / 100);
        textParams.leftMargin = width * 2 / 100;
        textParams.rightMargin = width * 2 / 100;
//        textParams.setGravity()
        mTxtSelectedVehicle.setLayoutParams(textParams);
        mTxtSelectedVehicle.setGravity(Gravity.CENTER);

        LinearLayout.LayoutParams imgAlertParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        imgAlertParams.width = width * 12 / 100;
        imgAlertParams.height = height * 6 / 100;
        imgAlertParams.topMargin = (int) (height * 0.25 / 100);
        imgAlertParams.leftMargin = width * 2 / 100;
        imgAlertParams.rightMargin = width * 2 / 100;
//        textParams.setGravity()
        mImgShowAlertDialog.setLayoutParams(imgAlertParams);
//        mImgShowAlertDialog.setGravity(Gravity.CENTER);

        ////   todate.setLayoutParams(textParams);
//        fromdate.setGravity(Gravity.CENTER);
//        todate.setGravity(Gravity.CENTER);

//        LinearLayout.LayoutParams textValueParams = new LinearLayout.LayoutParams(
//                LinearLayout.LayoutParams.WRAP_CONTENT,
//                LinearLayout.LayoutParams.WRAP_CONTENT);
//        textValueParams.width = width * 45 / 100;
//        textValueParams.height = height * 6 / 100;
//        textValueParams.leftMargin = width * 2 / 100;
//        textValueParams.rightMargin = width * 2 / 100;
//        textValueParams.bottomMargin = (int) (height * 0.25 / 100);
//        fromdatevalue.setLayoutParams(textValueParams);
//        todatevalue.setLayoutParams(textValueParams);
//        fromdatevalue.setGravity(Gravity.CENTER);
//        todatevalue.setGravity(Gravity.CENTER);

//        LinearLayout.LayoutParams ViewParams = new LinearLayout.LayoutParams(
//                LinearLayout.LayoutParams.WRAP_CONTENT,
//                LinearLayout.LayoutParams.WRAP_CONTENT);
//        ViewParams.width = width * 1 / 100;
//        ViewParams.height = LinearLayout.LayoutParams.MATCH_PARENT;
//        v1.setLayoutParams(ViewParams);
//        v2.setLayoutParams(ViewParams);

        LinearLayout.LayoutParams viewParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        viewParams.width = width;
        viewParams.height = height * 1 / 2 / 100;
        v3.setLayoutParams(viewParams);

//        LinearLayout.LayoutParams spinnerParams = new LinearLayout.LayoutParams(
//                LinearLayout.LayoutParams.WRAP_CONTENT,
//                LinearLayout.LayoutParams.WRAP_CONTENT);
//        spinnerParams.width = width * 95 / 100;
//        spinnerParams.height = height * 8 / 100;
//        spinnerParams.topMargin = (int) (height * 1.25 / 100);
//        spinnerParams.leftMargin = width * 2 / 100;
//        spinnerParams.rightMargin = width * 2 / 100;
//        spinnerParams.bottomMargin = (int) (height * 0.25 / 100);
//        groupSpinner.setLayoutParams(spinnerParams);

        LinearLayout.LayoutParams lineParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        lineParams.width = width * 98 / 100;
        lineParams.height = LinearLayout.LayoutParams.WRAP_CONTENT;

        lineParams.bottomMargin = height * 1 / 100;
        lineParams.leftMargin = width * 1 / 100;
        lineParams.rightMargin = width * 1 / 100;
        lv.setLayoutParams(lineParams);

//        LinearLayout.LayoutParams headTxtParams1 = new LinearLayout.LayoutParams(
//                LinearLayout.LayoutParams.WRAP_CONTENT,
//                LinearLayout.LayoutParams.WRAP_CONTENT);
//        headTxtParams1.width = width * 76 / 100;
//        headTxtParams1.height = height * 8 / 100;
//        headTxtParams1.setMargins(width * 4 / 100, width * 2 / 100, 1, width * 1 / 100);
////        headTxtParams1.setMargins();
//        mHeadTitle.setLayoutParams(headTxtParams);
//        mHeadTitle.setPadding(width * 2 / 100, 0, 0, 0);
//        mHeadTitle.setGravity(Gravity.CENTER);
//        mEdtSearch.setLayoutParams(headTxtParams1);
//        mEdtSearch.setm
//        mEdtSearch.setPadding(width * 2 / 100, 0, width * 4 / 100, 0);
//        mEdtSearch.setGravity(Gravity.CENTER | Gravity.CENTER);


        if (width >= 600) {
//            fromdate.setTextSize(16);
//            todate.setTextSize(16);
//            fromdatevalue.setTextSize(16);
//            todatevalue.setTextSize(16);
            mTxtNoRecord.setTextSize(17);
            mHeadTitle.setTextSize(18);
            mTxtSelectedVehicle.setTextSize(18);
        } else if (width > 501 && width < 600) {
//            fromdate.setTextSize(15);
//            todate.setTextSize(15);
//            fromdatevalue.setTextSize(15);
//            todatevalue.setTextSize(15);
            mTxtNoRecord.setTextSize(16);
            mHeadTitle.setTextSize(17);
            mTxtSelectedVehicle.setTextSize(17);
        } else if (width > 260 && width < 500) {
//            fromdate.setTextSize(14);
//            todate.setTextSize(14);
//            fromdatevalue.setTextSize(14);
//            todatevalue.setTextSize(14);
            mTxtNoRecord.setTextSize(15);
            mHeadTitle.setTextSize(16);
            mTxtSelectedVehicle.setTextSize(16);
        } else if (width <= 260) {
//            fromdate.setTextSize(13);
//            todate.setTextSize(13);
//            fromdatevalue.setTextSize(13);
//            todatevalue.setTextSize(13);
            mTxtNoRecord.setTextSize(14);
            mHeadTitle.setTextSize(15);
            mTxtSelectedVehicle.setTextSize(15);
        }
    }
//	public void insertData(ContentValues cv) throws SQLException {
//		DbHandler db = new DbHandler(getApplicationContext());
//		try {
//			int c = (int) db.open().getDatabaseObj()
//					.insert(DbHandler.TABLE_LOCATION, null, cv);
//
//			System.out.println("No of inserted rows is :::::::" + c);
//		} finally {
//
//			db.close();
//		}
//	}

    private void init() {
        // TODO Auto-generated method stub
        lv = (ListView) findViewById(R.id.nearby_vehiclelist);
//        fromdate = (TextView) findViewById(R.id.from_date_text);
//        todate = (TextView) findViewById(R.id.to_date_text);
//        fromdatevalue = (TextView) findViewById(R.id.from_date_value);
//        todatevalue = (TextView) findViewById(R.id.to_date_value);
//        groupSpinner = (Spinner) findViewById(R.id.groupspinner);
//        v1 = (View) findViewById(R.id.view_vertical);
//        v2 = (View) findViewById(R.id.view_vertical1);
        v3 = (View) findViewById(R.id.view_horizontal);
        mTxtNoRecord = (TextView) findViewById(R.id.nearby_no_record_txt);
        mTxtSelectedVehicle = (TextView) findViewById(R.id.selected_vehicle_text);
        mBackArrow = (ImageView) findViewById(R.id.nearby_vehiclelist_view_Back);
        mHeadTitle = (TextView) findViewById(R.id.tv_nearby_vehiclelist_title);
//        mEdtSearch = (EditText) findViewById(R.id.kms_list_search_activity);

//        fromdate.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
//        todate.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
//        fromdatevalue.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
//        todatevalue.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        mTxtNoRecord.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        mHeadTitle.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        mTxtSelectedVehicle.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        mImgShowAlertDialog = (ImageView) findViewById(R.id.img_nearby_showalert);
    }

}
