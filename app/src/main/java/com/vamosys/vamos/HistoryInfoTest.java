package com.vamosys.vamos;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.vamosys.utils.Constant;
import com.vamosys.utils.TypefaceUtil;

import org.achartengine.ChartFactory;
import org.achartengine.GraphicalView;
import org.achartengine.chart.PointStyle;
import org.achartengine.model.SeriesSelection;
import org.achartengine.model.XYMultipleSeriesDataset;
import org.achartengine.model.XYSeries;
import org.achartengine.renderer.BasicStroke;
import org.achartengine.renderer.XYMultipleSeriesRenderer;
import org.achartengine.renderer.XYSeriesRenderer;

import java.util.regex.Pattern;

public class HistoryInfoTest extends Activity {

    LinearLayout mLinearHeaderLayout, mLinearLayout1, mLinearLayout2, mLinearAddsLayout, mLinearPMINLayout, mLinearChartContainerLayout, mLinearBottomLayout;
    LinearLayout mSubLinearLayout1, mSubLinearLayout2, mSubLinearLayout3, mSubLinearLayout4, mSubLinearLayout5, mSubLinearLayout6;
    ImageView mPlayBackImage, mNavigationImage;

    View mTimeVerView, mAddsVerView, mHoriView1, mHoriView2;
    TextView mTxtHeader, mSpeedLimitTxtView10, mOdoDistTxtView11, mSpeedLimitTxtValue10, mOdoDistTxtValue11, mStartTimeTxtView, mEndTimeTxtView,
            mStartAddsTxtView, mEndAddsTxtView, mParkTimeTxtView, mMovingTimeTxtView, mIdleTimeTxtView, mNoDataTimeTxtView,
            mParkedTimeValueTxtView, mMovingTimeValueTxtView, mIdleTimeValueTxtView, mNoDataTimeValueTxtView, mPlayBackTxtView, mNaviTxtView;

    Const cons;
    private String[] mMonth = new String[]{"Jan", "Feb", "Mar", "Apr", "May",
            "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_history_info_layout);
        // init();

        historyInfoPopupWindow();
//        screenArrange();
        cons = new Const();
        //drawChart();
        //  setData();


//        mImgView1.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (isValidMobile(Constant.SELECTED_VEHICLE_LOCATION_OBJECT.getMobileNo())) {
//                    callMethod();
//                } else {
//                    Toast.makeText(HistoryInfoTest.this, "Mobile number is not valid", Toast.LENGTH_SHORT).show();
//                }
//            }
//        });
//
//        mImgView2.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                String sLat = Constant.SELECTED_VEHICLE_LOCATION_OBJECT.getLatitude();
//                String sLng = Constant.SELECTED_VEHICLE_LOCATION_OBJECT.getLongitude();
//
//                Intent intent = new Intent(Intent.ACTION_VIEW,
//                        Uri.parse("http://maps.google.com/maps?saddr=" + sLat + "," + sLng + ""));
//                startActivity(intent);
//            }
//        });
    }

    public void callMethod() {
//        Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + Constant.SELECTED_DRIVER_CONTACT_NUMBER));
//        startActivity(intent);
//        Intent callIntent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + Constant.SELECTED_DRIVER_CONTACT_NUMBER));
//        startActivity(callIntent);

//        if (ContextCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED) {
        Intent callIntent = new Intent(Intent.ACTION_DIAL);
        callIntent.setData(Uri.parse("tel:" + Constant.SELECTED_DRIVER_CONTACT_NUMBER));
        callIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(callIntent);
//        }

    }

    public void init() {
        mLinearHeaderLayout = (LinearLayout) findViewById(R.id.new_info_header_layout);

        mLinearLayout1 = (LinearLayout) findViewById(R.id.new_history_info_linear_layout1);
        mSubLinearLayout1 = (LinearLayout) findViewById(R.id.new_history_info_linear_layout10);
        mSubLinearLayout2 = (LinearLayout) findViewById(R.id.new_history_info_linear_layout11);


        mTxtHeader = (TextView) findViewById(R.id.new_info_header_txt);
        mSpeedLimitTxtView10 = (TextView) findViewById(R.id.new_history_info_txt_view10);
        mOdoDistTxtView11 = (TextView) findViewById(R.id.new_history_info_txt_view11);
        //mTxtView12 = (TextView) findViewById(R.id.new_info_txt_view12);

        mTxtHeader.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        mSpeedLimitTxtView10.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        mOdoDistTxtView11.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        // mTxtView12.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));

        mSpeedLimitTxtValue10 = (TextView) findViewById(R.id.new_history_info_txt_value10);
        mOdoDistTxtValue11 = (TextView) findViewById(R.id.new_history_info_txt_value11);
        //  mTxtValue12 = (TextView) findViewById(R.id.new_info_txt_value12);
        mSpeedLimitTxtValue10.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        mOdoDistTxtValue11.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        //  mTxtValue12.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));

        mLinearLayout2 = (LinearLayout) findViewById(R.id.new_history_time_linear_layout);

        mStartTimeTxtView = (TextView) findViewById(R.id.new_history_start_time_txt_view);
        mEndTimeTxtView = (TextView) findViewById(R.id.new_history_end_time_txt_view);
        mStartTimeTxtView.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        mEndTimeTxtView.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));

        mTimeVerView = (View) findViewById(R.id.new_history_time_view);

        mLinearAddsLayout = (LinearLayout) findViewById(R.id.new_history_adds_linear_layout);

        mStartAddsTxtView = (TextView) findViewById(R.id.new_history_start_adds_txt_view);
        mEndAddsTxtView = (TextView) findViewById(R.id.new_history_end_adds_txt_view);
        mStartAddsTxtView.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        mEndAddsTxtView.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));

        mAddsVerView = (View) findViewById(R.id.new_history_adds_view);


        mLinearPMINLayout = (LinearLayout) findViewById(R.id.new_history_pmin_time_linear_layout3);
        mSubLinearLayout3 = (LinearLayout) findViewById(R.id.new_history_pmin_time_linear_layout30);
        mParkTimeTxtView = (TextView) findViewById(R.id.new_history_parked_time_txt_view30);
        mMovingTimeTxtView = (TextView) findViewById(R.id.new_history_moving_time_txt_view31);
        mIdleTimeTxtView = (TextView) findViewById(R.id.new_history_idle_time_txt_view32);
        mNoDataTimeTxtView = (TextView) findViewById(R.id.new_history_nodata_time_txt_view33);

        mParkTimeTxtView.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        mMovingTimeTxtView.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        mIdleTimeTxtView.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        mNoDataTimeTxtView.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));

        mSubLinearLayout4 = (LinearLayout) findViewById(R.id.new_history_pmin_time_value_linear_layout30);
        mParkedTimeValueTxtView = (TextView) findViewById(R.id.new_history_parked_time_txt_value);
        mMovingTimeValueTxtView = (TextView) findViewById(R.id.new_history_moving_time_txt_value);
        mIdleTimeValueTxtView = (TextView) findViewById(R.id.new_history_idle_time_txt_value);
        mNoDataTimeValueTxtView = (TextView) findViewById(R.id.new_history_nodata_time_txt_value);

        mParkedTimeValueTxtView.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        mMovingTimeValueTxtView.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        mIdleTimeValueTxtView.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        mNoDataTimeValueTxtView.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));

        mLinearChartContainerLayout = (LinearLayout) findViewById(R.id.chart_container);

        mHoriView1 = (View) findViewById(R.id.new_history_info_view100);
        mHoriView2 = (View) findViewById(R.id.new_history_info_view200);

        mLinearBottomLayout = (LinearLayout) findViewById(R.id.new_history_bottom_linear_layout21);

        mPlayBackTxtView = (TextView) findViewById(R.id.new_history_txt_play_back);
        mNaviTxtView = (TextView) findViewById(R.id.new_history_txt_navigation);
        mSubLinearLayout5 = (LinearLayout) findViewById(R.id.play_back_layout);
        mSubLinearLayout6 = (LinearLayout) findViewById(R.id.navigation_layout);

        mPlayBackImage = (ImageView) findViewById(R.id.play_back_imgview);
        mNavigationImage = (ImageView) findViewById(R.id.navigation_imgview);


    }


    private boolean isValidMobile(String phone2) {
        boolean check = false;
        if (!Pattern.matches("[a-zA-Z]+", phone2)) {
            if (phone2.length() < 6 || phone2.length() > 13) {
                check = false;
//                txtPhone.setError("Not Valid Number");
            } else {
                check = true;
            }
        } else {
            check = false;
        }
        return check;
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();

        startActivity(new Intent(HistoryInfoTest.this, VehicleListActivity.class));
        finish();

    }

    public void drawChart() {
        int[] x = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11};
        int[] income = {1200, 1500, 2700, 3000, 1800, 2500, 2700, 3500, 3200, 3700,
                3000, 4000};
//        int[] expense = { 2200, 1800, 2900, 3200, 2000, 3000, 3300, 3800, 3400, 3700,
//                3500, 4200 };

        // Creating an XYSeries for Income
        XYSeries incomeSeries = new XYSeries("");
        // Creating an XYSeries for Expense
//        XYSeries expenseSeries = new XYSeries("Expense");
        // Adding data to Income and Expense Series
        for (int i = 0; i < x.length; i++) {
            incomeSeries.add(i, income[i]);
//            expenseSeries.add(i, expense[i]);
        }

        // Creating a dataset to hold each series
        XYMultipleSeriesDataset dataset = new XYMultipleSeriesDataset();
        // Adding Income Series to the dataset
        dataset.addSeries(incomeSeries);
        // Adding Expense Series to dataset
//        dataset.addSeries(expenseSeries);

        // Creating XYSeriesRenderer to customize incomeSeries
        XYSeriesRenderer incomeRenderer = new XYSeriesRenderer();
        incomeRenderer.setColor(Color.CYAN); // color of the graph set to cyan
        incomeRenderer.setFillPoints(true);
        incomeRenderer.setLineWidth(2f);
        incomeRenderer.setDisplayChartValues(true);
        // setting chart value distance
        incomeRenderer.setDisplayChartValues(true);
        // setting line graph point style to circle
        incomeRenderer.setPointStyle(PointStyle.CIRCLE);
        // setting stroke of the line chart to solid
        incomeRenderer.setStroke(BasicStroke.SOLID);
        // for filling area
        incomeRenderer.setFillBelowLine(true);
        // incomeRenderer.setFillBelowLineColor(Color.YELLOW);

        // Creating XYSeriesRenderer to customize expenseSeries
//        XYSeriesRenderer expenseRenderer = new XYSeriesRenderer();
//        expenseRenderer.setColor(Color.GREEN);
//        expenseRenderer.setFillPoints(true);
//        expenseRenderer.setLineWidth(2f);
//        expenseRenderer.setDisplayChartValues(true);
//        // setting line graph point style to circle
//        expenseRenderer.setPointStyle(PointStyle.SQUARE);
//        // setting stroke of the line chart to solid
//        expenseRenderer.setStroke(BasicStroke.SOLID);
//        // for filling area
//        expenseRenderer.setFillBelowLine(true);
        // expenseRenderer.setFillBelowLineColor(Color.RED);

        // Creating a XYMultipleSeriesRenderer to customize the whole chart
        XYMultipleSeriesRenderer multiRenderer = new XYMultipleSeriesRenderer();
        multiRenderer.setXLabels(0);
        //   multiRenderer.setChartTitle("Income vs Expense Chart");
        // multiRenderer.setXTitle("Year 2014");
        // multiRenderer.setYTitle("Amount in Dollars");

        /***
         * Customizing graphs
         */
        // setting text size of the title
        multiRenderer.setChartTitleTextSize(28);
        // setting text size of the axis title
        multiRenderer.setAxisTitleTextSize(24);
        // setting text size of the graph lable
        multiRenderer.setLabelsTextSize(24);
        // setting zoom buttons visiblity
        multiRenderer.setZoomButtonsVisible(false);
        // setting pan enablity which uses graph to move on both axis
        multiRenderer.setPanEnabled(true, true);
        // setting click false on graph
        multiRenderer.setClickEnabled(true);
        // setting zoom to false on both axis
        multiRenderer.setZoomEnabled(true, false);
        // setting lines to display on y axis
        multiRenderer.setShowGridY(true);
        // setting lines to display on x axis
        multiRenderer.setShowGridX(true);
        // setting legend to fit the screen size
        multiRenderer.setFitLegend(true);
        // setting displaying line on grid
        multiRenderer.setShowGrid(true);
        // setting zoom to false
        multiRenderer.setZoomEnabled(false);
        // setting external zoom functions to false
        multiRenderer.setExternalZoomEnabled(false);
        // setting displaying lines on graph to be formatted(like using
        // graphics)
        multiRenderer.setAntialiasing(true);
        // setting to in scroll to false
        multiRenderer.setInScroll(false);
        // setting to set legend height of the graph
        multiRenderer.setLegendHeight(30);
        // setting x axis label align
        multiRenderer.setXLabelsAlign(Paint.Align.CENTER);
        // setting y axis label to align
        multiRenderer.setYLabelsAlign(Paint.Align.LEFT);
        // setting text style
        multiRenderer.setTextTypeface("sans_serif", Typeface.NORMAL);
        // setting no of values to display in y axis
        multiRenderer.setYLabels(10);
        // setting y axis max value, Since i'm using static values inside the
        // graph so i'm setting y max value to 4000.
        // if you use dynamic values then get the max y value and set here
        multiRenderer.setYAxisMax(4000);
        // setting used to move the graph on xaxiz to .5 to the right
        multiRenderer.setXAxisMin(-0.5);
        // setting used to move the graph on xaxiz to .5 to the right
        multiRenderer.setXAxisMax(11);
        // setting bar size or space between two bars
        // multiRenderer.setBarSpacing(0.5);
        // Setting background color of the graph to transparent
        multiRenderer.setBackgroundColor(Color.TRANSPARENT);
        // Setting margin color of the graph to transparent

        multiRenderer.setApplyBackgroundColor(true);
        multiRenderer.setScale(2f);
        // setting x axis point size
        multiRenderer.setPointSize(4f);
        // setting the margin size for the graph in the order top, left, bottom,
        // right
        multiRenderer.setMargins(new int[]{0, 0, 0, 0});
        multiRenderer.setMarginsColor(R.color.trans);
//        multiRenderer.setInScroll(true);

//        multiRenderer.

        for (int i = 0; i < x.length; i++) {
            multiRenderer.addXTextLabel(i, mMonth[i]);
        }

        // Adding incomeRenderer and expenseRenderer to multipleRenderer
        // Note: The order of adding dataseries to dataset and renderers to
        // multipleRenderer
        // should be same
        multiRenderer.addSeriesRenderer(incomeRenderer);
//        multiRenderer.addSeriesRenderer(expenseRenderer);

        // this part is used to display graph on the xml
        LinearLayout chartContainer = (LinearLayout) findViewById(R.id.chart_container);
        // remove any views before u paint the chart
        chartContainer.removeAllViews();

        mLinearChartContainerLayout.removeAllViews();

        // drawing bar chart
        final GraphicalView Chart = ChartFactory.getLineChartView(HistoryInfoTest.this, dataset,
                multiRenderer);
        // adding the view to the linearlayout


        Chart.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // handle the click event on the chart
                SeriesSelection seriesSelection = Chart.getCurrentSeriesAndPoint();
                if (seriesSelection == null) {
                    //  Toast.makeText(CameraViewActivity.this, "No chart element", Toast.LENGTH_SHORT).show();
                } else {
                    // display information of the clicked point
                    Toast.makeText(
                            HistoryInfoTest.this,
                            "Vehicle Speed is : "
                                    + seriesSelection.getValue() + " Km/hr", Toast.LENGTH_SHORT).show();
                }
            }
        });


//        chartContainer.addView(Chart);
        mLinearChartContainerLayout.addView(Chart);

    }

    public void historyInfoPopupWindow() {


        Dialog history_dialog = new Dialog(HistoryInfoTest.this);
        history_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        history_dialog.setContentView(R.layout.new_history_info_layout);
        history_dialog.setCancelable(true);


        mLinearHeaderLayout = (LinearLayout) history_dialog.findViewById(R.id.new_info_header_layout);

        mLinearLayout1 = (LinearLayout) history_dialog.findViewById(R.id.new_history_info_linear_layout1);
        mSubLinearLayout1 = (LinearLayout) history_dialog.findViewById(R.id.new_history_info_linear_layout10);
        mSubLinearLayout2 = (LinearLayout) history_dialog.findViewById(R.id.new_history_info_linear_layout11);


        mTxtHeader = (TextView) history_dialog.findViewById(R.id.new_info_header_txt);
        mSpeedLimitTxtView10 = (TextView) history_dialog.findViewById(R.id.new_history_info_txt_view10);
        mOdoDistTxtView11 = (TextView) history_dialog.findViewById(R.id.new_history_info_txt_view11);
        //mTxtView12 = (TextView) findViewById(R.id.new_info_txt_view12);

        mTxtHeader.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        mSpeedLimitTxtView10.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        mOdoDistTxtView11.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        // mTxtView12.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));

        mSpeedLimitTxtValue10 = (TextView) history_dialog.findViewById(R.id.new_history_info_txt_value10);
        mOdoDistTxtValue11 = (TextView) history_dialog.findViewById(R.id.new_history_info_txt_value11);
        //  mTxtValue12 = (TextView) findViewById(R.id.new_info_txt_value12);
        mSpeedLimitTxtValue10.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        mOdoDistTxtValue11.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        //  mTxtValue12.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));

        mLinearLayout2 = (LinearLayout) history_dialog.findViewById(R.id.new_history_time_linear_layout);

        mStartTimeTxtView = (TextView) history_dialog.findViewById(R.id.new_history_start_time_txt_view);
        mEndTimeTxtView = (TextView) history_dialog.findViewById(R.id.new_history_end_time_txt_view);
        mStartTimeTxtView.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        mEndTimeTxtView.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));

        mTimeVerView = (View) history_dialog.findViewById(R.id.new_history_time_view);

        mLinearAddsLayout = (LinearLayout) history_dialog.findViewById(R.id.new_history_adds_linear_layout);

        mStartAddsTxtView = (TextView) history_dialog.findViewById(R.id.new_history_start_adds_txt_view);
        mEndAddsTxtView = (TextView) history_dialog.findViewById(R.id.new_history_end_adds_txt_view);
        mStartAddsTxtView.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        mEndAddsTxtView.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));

        mAddsVerView = (View) history_dialog.findViewById(R.id.new_history_adds_view);


        mLinearPMINLayout = (LinearLayout) history_dialog.findViewById(R.id.new_history_pmin_time_linear_layout3);
        mSubLinearLayout3 = (LinearLayout) history_dialog.findViewById(R.id.new_history_pmin_time_linear_layout30);
        mParkTimeTxtView = (TextView) history_dialog.findViewById(R.id.new_history_parked_time_txt_view30);
        mMovingTimeTxtView = (TextView) history_dialog.findViewById(R.id.new_history_moving_time_txt_view31);
        mIdleTimeTxtView = (TextView) history_dialog.findViewById(R.id.new_history_idle_time_txt_view32);
        mNoDataTimeTxtView = (TextView) history_dialog.findViewById(R.id.new_history_nodata_time_txt_view33);

        mParkTimeTxtView.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        mMovingTimeTxtView.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        mIdleTimeTxtView.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        mNoDataTimeTxtView.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));

        mSubLinearLayout4 = (LinearLayout) history_dialog.findViewById(R.id.new_history_pmin_time_value_linear_layout30);
        mParkedTimeValueTxtView = (TextView) history_dialog.findViewById(R.id.new_history_parked_time_txt_value);
        mMovingTimeValueTxtView = (TextView) history_dialog.findViewById(R.id.new_history_moving_time_txt_value);
        mIdleTimeValueTxtView = (TextView) history_dialog.findViewById(R.id.new_history_idle_time_txt_value);
        mNoDataTimeValueTxtView = (TextView) history_dialog.findViewById(R.id.new_history_nodata_time_txt_value);

        mParkedTimeValueTxtView.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        mMovingTimeValueTxtView.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        mIdleTimeValueTxtView.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        mNoDataTimeValueTxtView.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));

        mLinearChartContainerLayout = (LinearLayout) history_dialog.findViewById(R.id.chart_container);

        mHoriView1 = (View) history_dialog.findViewById(R.id.new_history_info_view100);
        mHoriView2 = (View) history_dialog.findViewById(R.id.new_history_info_view200);

        mLinearBottomLayout = (LinearLayout) history_dialog.findViewById(R.id.new_history_bottom_linear_layout21);

        mPlayBackTxtView = (TextView) history_dialog.findViewById(R.id.new_history_txt_play_back);
        mNaviTxtView = (TextView) history_dialog.findViewById(R.id.new_history_txt_navigation);
        mSubLinearLayout5 = (LinearLayout) history_dialog.findViewById(R.id.play_back_layout);
        mSubLinearLayout6 = (LinearLayout) history_dialog.findViewById(R.id.navigation_layout);

        mPlayBackImage = (ImageView) history_dialog.findViewById(R.id.play_back_imgview);
        mNavigationImage = (ImageView) history_dialog.findViewById(R.id.navigation_imgview);

        screenArrange();
        drawChart();

        history_dialog.show();


    }

    public void screenArrange() {
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        int height = metrics.heightPixels;
        int width = metrics.widthPixels;
        Constant.ScreenWidth = width;
        Constant.ScreenHeight = height;

        LinearLayout.LayoutParams headerLayoutParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        headerLayoutParams.width = width;
        headerLayoutParams.height = height * 8 / 100;
        mLinearHeaderLayout.setLayoutParams(headerLayoutParams);


        LinearLayout.LayoutParams headTxtParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        headTxtParams.width = width;
        headTxtParams.height = height * 8 / 100;
        mTxtHeader.setLayoutParams(headTxtParams);
        mTxtHeader.setPadding(width * 2 / 100, 0, width * 2 / 100, 0);
        mTxtHeader.setGravity(Gravity.CENTER);


        LinearLayout.LayoutParams linearMainParams1 = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        linearMainParams1.width = width * 94 / 100;
        linearMainParams1.height = height * 10 / 100;
        linearMainParams1.setMargins(width * 1 / 100, width * 1 / 100, width * 1 / 100, width * 1 / 2 / 100);
        mLinearLayout1.setLayoutParams(linearMainParams1);


        LinearLayout.LayoutParams linearSubParams10 = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        linearSubParams10.width = width * 47 / 100;
        linearSubParams10.height = height * 10 / 100;
        mSubLinearLayout1.setLayoutParams(linearSubParams10);
        mSubLinearLayout1.setPadding(width * 1 / 100, width * 1 / 100, width * 1 / 100, width * 1 / 100);

        LinearLayout.LayoutParams txtViewParams10 = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        txtViewParams10.width = width * 47 / 100;
        txtViewParams10.height = height * 5 / 100;
        mSpeedLimitTxtView10.setLayoutParams(txtViewParams10);
        mSpeedLimitTxtView10.setPadding(width * 1 / 2 / 100, height * 1 / 2 / 100, height * 1 / 2 / 100, height * 1 / 2 / 100);

        LinearLayout.LayoutParams txtViewParams11 = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        txtViewParams11.width = width * 47 / 100;
        txtViewParams11.height = height * 5 / 100;
        mOdoDistTxtView11.setLayoutParams(txtViewParams11);
        mOdoDistTxtView11.setPadding(width * 1 / 2 / 100, height * 1 / 2 / 100, height * 1 / 2 / 100, height * 1 / 2 / 100);


        LinearLayout.LayoutParams linearSubParams11 = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        linearSubParams11.width = width * 47 / 100;
        linearSubParams11.height = height * 10 / 100;
        mSubLinearLayout2.setLayoutParams(linearSubParams11);
        mSubLinearLayout2.setPadding(width * 1 / 100, width * 1 / 100, width * 1 / 100, width * 1 / 100);

        LinearLayout.LayoutParams txtValueParams10 = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        txtValueParams10.width = width * 47 / 100;
        txtValueParams10.height = height * 5 / 100;
        mSpeedLimitTxtValue10.setLayoutParams(txtValueParams10);
        mSpeedLimitTxtValue10.setPadding(width * 1 / 2 / 100, height * 1 / 2 / 100, height * 1 / 2 / 100, height * 1 / 2 / 100);

        LinearLayout.LayoutParams txtValueParams11 = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        txtValueParams11.width = width * 47 / 100;
        txtValueParams11.height = height * 5 / 100;
        mOdoDistTxtValue11.setLayoutParams(txtValueParams11);
        mOdoDistTxtValue11.setPadding(width * 1 / 2 / 100, height * 1 / 2 / 100, height * 1 / 2 / 100, height * 1 / 2 / 100);


        LinearLayout.LayoutParams timeLayoutParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        timeLayoutParams.width = width * 94 / 100;
//        headerLayoutParams.height = height * 8 / 100;
        timeLayoutParams.setMargins(width * 1 / 100, width * 1 / 2 / 100, width * 1 / 100, width * 1 / 2 / 100);
        mLinearLayout2.setLayoutParams(timeLayoutParams);

        LinearLayout.LayoutParams txtStartTimeParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        txtStartTimeParams.width = width * 47 / 100;
        //txtValueParams11.height = height * 5 / 100;
        mStartTimeTxtView.setLayoutParams(txtStartTimeParams);
        mStartTimeTxtView.setGravity(Gravity.CENTER);
        mStartTimeTxtView.setPadding(width * 1 / 2 / 100, height * 1 / 2 / 100, height * 1 / 2 / 100, height * 1 / 2 / 100);

        mEndTimeTxtView.setLayoutParams(txtStartTimeParams);
        mEndTimeTxtView.setGravity(Gravity.CENTER);
        mEndTimeTxtView.setPadding(width * 1 / 2 / 100, height * 1 / 2 / 100, height * 1 / 2 / 100, height * 1 / 2 / 100);


        LinearLayout.LayoutParams addsLayoutParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        addsLayoutParams.width = width * 94 / 100;
//        headerLayoutParams.height = height * 8 / 100;
        addsLayoutParams.setMargins(width * 1 / 100, width * 1 / 2 / 100, width * 1 / 100, width * 1 / 2 / 100);
        mLinearAddsLayout.setLayoutParams(addsLayoutParams);

        LinearLayout.LayoutParams txtStartAddsParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        txtStartAddsParams.width = width * 47 / 100;
        //txtValueParams11.height = height * 5 / 100;
        mStartAddsTxtView.setLayoutParams(txtStartAddsParams);
        mStartAddsTxtView.setGravity(Gravity.CENTER);
        mStartAddsTxtView.setPadding(width * 1 / 2 / 100, height * 1 / 2 / 100, height * 1 / 2 / 100, height * 1 / 2 / 100);

        mEndAddsTxtView.setLayoutParams(txtStartAddsParams);
        mEndAddsTxtView.setGravity(Gravity.CENTER);
        mEndAddsTxtView.setPadding(width * 1 / 2 / 100, height * 1 / 2 / 100, height * 1 / 2 / 100, height * 1 / 2 / 100);


        LinearLayout.LayoutParams linearPMINMainParams1 = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        linearPMINMainParams1.width = width * 94 / 100;
        linearPMINMainParams1.height = height * 20 / 100;
        linearPMINMainParams1.setMargins(width * 1 / 100, width * 1 / 100, width * 1 / 100, width * 1 / 2 / 100);
        mLinearPMINLayout.setLayoutParams(linearPMINMainParams1);


        LinearLayout.LayoutParams linearpminSubParams10 = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        linearpminSubParams10.width = width * 47 / 100;
        linearpminSubParams10.height = height * 20 / 100;
        mSubLinearLayout3.setLayoutParams(linearpminSubParams10);
        //  mSubLinearLayout3.setPadding(width * 1 / 100, width * 1 / 100, width * 1 / 100, width * 1 / 100);
        mSubLinearLayout4.setLayoutParams(linearpminSubParams10);
        //   mSubLinearLayout4.setPadding(width * 1 / 100, width * 1 / 100, width * 1 / 100, width * 1 / 100);

        LinearLayout.LayoutParams txtpminViewParams10 = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        txtpminViewParams10.width = width * 47 / 100;
        txtpminViewParams10.height = height * 5 / 100;
        mParkTimeTxtView.setLayoutParams(txtpminViewParams10);
        mParkTimeTxtView.setPadding(width * 1 / 2 / 100, height * 1 / 2 / 100, height * 1 / 2 / 100, height * 1 / 2 / 100);

        mParkedTimeValueTxtView.setLayoutParams(txtpminViewParams10);
        mParkedTimeValueTxtView.setPadding(width * 1 / 2 / 100, height * 1 / 2 / 100, height * 1 / 2 / 100, height * 1 / 2 / 100);

        mMovingTimeTxtView.setLayoutParams(txtpminViewParams10);
        mMovingTimeTxtView.setPadding(width * 1 / 2 / 100, height * 1 / 2 / 100, height * 1 / 2 / 100, height * 1 / 2 / 100);

        mMovingTimeValueTxtView.setLayoutParams(txtpminViewParams10);
        mMovingTimeValueTxtView.setPadding(width * 1 / 2 / 100, height * 1 / 2 / 100, height * 1 / 2 / 100, height * 1 / 2 / 100);

        mIdleTimeTxtView.setLayoutParams(txtpminViewParams10);
        mIdleTimeTxtView.setPadding(width * 1 / 2 / 100, height * 1 / 2 / 100, height * 1 / 2 / 100, height * 1 / 2 / 100);

        mIdleTimeValueTxtView.setLayoutParams(txtpminViewParams10);
        mIdleTimeValueTxtView.setPadding(width * 1 / 2 / 100, height * 1 / 2 / 100, height * 1 / 2 / 100, height * 1 / 2 / 100);

        mNoDataTimeTxtView.setLayoutParams(txtpminViewParams10);
        mNoDataTimeTxtView.setPadding(width * 1 / 2 / 100, height * 1 / 2 / 100, height * 1 / 2 / 100, height * 1 / 2 / 100);

        mNoDataTimeValueTxtView.setLayoutParams(txtpminViewParams10);
        mNoDataTimeValueTxtView.setPadding(width * 1 / 2 / 100, height * 1 / 2 / 100, height * 1 / 2 / 100, height * 1 / 2 / 100);


        LinearLayout.LayoutParams linearChartParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        linearChartParams.width = width * 94 / 100;
        linearChartParams.height = height * 25 / 100;
        linearChartParams.setMargins(width * 1 / 100, width * 1 / 2 / 100, width * 1 / 100, width * 1 / 2 / 100);
        mLinearChartContainerLayout.setLayoutParams(linearChartParams);
        mLinearChartContainerLayout.setGravity(Gravity.CENTER);

        LinearLayout.LayoutParams linearBottomLayoutParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        linearBottomLayoutParams.width = width * 94 / 100;
        linearBottomLayoutParams.height = height * 16 / 100;
        linearBottomLayoutParams.setMargins(width * 1 / 100, width * 1 / 2 / 100, width * 1 / 100, width * 1 / 2 / 100);
        mLinearBottomLayout.setLayoutParams(linearBottomLayoutParams);


        LinearLayout.LayoutParams linearPlayBackParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        linearPlayBackParams.width = width * 94 / 100;
        linearPlayBackParams.height = height * 8 / 100;

        mSubLinearLayout5.setLayoutParams(linearPlayBackParams);
        mSubLinearLayout6.setLayoutParams(linearPlayBackParams);

        LinearLayout.LayoutParams txtPlayNavParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        txtPlayNavParams.width = width * 78 / 100;
        txtPlayNavParams.height = height * 8 / 100;
        mPlayBackTxtView.setLayoutParams(txtPlayNavParams);
        mPlayBackTxtView.setGravity(Gravity.CENTER | Gravity.LEFT);
        mPlayBackTxtView.setPadding(width * 1 / 2 / 100, height * 1 / 2 / 100, height * 1 / 2 / 100, height * 1 / 2 / 100);
        mNaviTxtView.setLayoutParams(txtPlayNavParams);
        mNaviTxtView.setGravity(Gravity.CENTER | Gravity.LEFT);
        mNaviTxtView.setPadding(width * 1 / 2 / 100, height * 1 / 2 / 100, height * 1 / 2 / 100, height * 1 / 2 / 100);


        LinearLayout.LayoutParams txtImageParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        txtImageParams.width = width * 16 / 100;
        txtImageParams.height = height * 8 / 100;

        mPlayBackImage.setLayoutParams(txtImageParams);

        mPlayBackImage.setPadding(width * 1 / 2 / 100, height * 1 / 2 / 100, height * 1 / 2 / 100, height * 1 / 2 / 100);
        mNavigationImage.setLayoutParams(txtImageParams);
        mNavigationImage.setPadding(width * 1 / 2 / 100, height * 1 / 2 / 100, height * 1 / 2 / 100, height * 1 / 2 / 100);


//
        LinearLayout.LayoutParams horiViewParams1 = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        horiViewParams1.width = width;
        horiViewParams1.height = height * 1 / 3 / 100;
        mHoriView1.setLayoutParams(horiViewParams1);
        mHoriView2.setLayoutParams(horiViewParams1);

        LinearLayout.LayoutParams veriViewParams1 = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        veriViewParams1.width = width * 1 / 2 / 100;
//        veriViewParams1.height = height * 7 / 100;
        mTimeVerView.setLayoutParams(veriViewParams1);
        mAddsVerView.setLayoutParams(veriViewParams1);


        if (width >= 600) {

        } else if (width > 501 && width < 600) {


        } else if (width > 260 && width < 500) {


        } else if (width <= 260) {


        }

    }

}

