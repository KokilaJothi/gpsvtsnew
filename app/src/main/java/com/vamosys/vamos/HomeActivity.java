package com.vamosys.vamos;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.SQLException;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.vamosys.utils.ConnectionDetector;
import com.vamosys.utils.Constant;
import com.vamosys.utils.DaoHandler;
import com.vamosys.utils.HttpConfig;
import com.vamosys.utils.MyCustomProgressDialog;
import com.vamosys.utils.TypefaceUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.osmdroid.api.IMapController;
import org.osmdroid.config.Configuration;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.Marker;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;


public class HomeActivity extends FragmentActivity implements GoogleMap.OnInfoWindowClickListener, View.OnClickListener, OnMapReadyCallback {

    //    Popup dialog initialize
    TextView id_vehicleid, id_view_address, id_view_speed, id_view_lastseentime, id_time_labelid, id_time, id_speed_limit_speed, id_ignition_speed, id_txtview_total_km;
    RelativeLayout id_time_label_layout;
    ImageView id_vehicle_info_icon;

    int width, height;
    private GoogleMap map;
    ImageView $ImageBack, $ChangeView;
    TextView $TxtTitle, $TxtNoOfVehicle, $TxtOnlineVehicle, $TxtOfflineVehicle,
            $TxtNoOfVehicleValue, $TxtOnlineVehicleValue,
            $TxtOfflineVehicleValue, $TxtNodataVehicle, $TxtNodataVehicleValue, $TxtMovingVehicle, $TxtMovingVehicleValue;
    LinearLayout $LayoutVehicle, $LayoutOnlineVehicle, $LayoutOfflineVehicle, $LayoutNodataVehicle, $LayoutMovingVehicle;
    View $View1, $View2, $View3, $View4, $View5;
    Spinner $SpinnerNavigation;
    LinearLayout mGoogleMapLayout;
    String mSelectedUserID, mSelectedGroupId, selected_vehicleid;
    String mTotalVehicles, mOnlineVehicles, mOfflineVehicles;
    static Timer timer;
    List<String> mGroupList = new ArrayList<String>();
    List<String> mGroupSpinnerList = new ArrayList<String>();
    DBHelper dbhelper;
    MarkerOptions markerOption;

    List<Marker> mOSMMarkerList = new ArrayList<Marker>();
    List<com.google.android.gms.maps.model.Marker> mGoogleMarkerList = new ArrayList<com.google.android.gms.maps.model.Marker>();
    com.google.android.gms.maps.model.Marker currentMarker, currentMarker1, currentMarker2, currentMarker3;

    String mResultData = null;
    /**
     * POI declarations
     */
    double lat, lng;
    float vehicle_zoom_level = 4.0F;
    float group_zoom_level = 4.0F;
    private HashMap<com.google.android.gms.maps.model.Marker, JSONObject> mMarkersHashMapGoogle;
    private HashMap<Marker, JSONObject> mMarkersHashMapOSM;
    private JSONObject jSelectedVehicleObject;
    ConnectionDetector cd;
    Dialog marker_info_dialog;
    SharedPreferences sp;
    Const cons;

    ArrayList<String> child = new ArrayList<String>();
    ArrayList<String> vehicle = new ArrayList<String>();
    private ArrayList<Object> childelements = new ArrayList<Object>();

    int map_marker_vechile_position, position;
    String mSELECTED_MAP_TYPE = "Normal";
    ImageView mImgRefresh;
    String mSELECTED_VEHICLE_SHOW_TYPE = "All";

    boolean mIsAutRefreshEnabled = false;
    String mAutoRefreshValue = "10";
    boolean isFirstTime = true;

    //OSM related
    MapView mapview;
    boolean isOsmEnabled = true;
    private IMapController mapController;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Context ctx = getApplicationContext();
        //important! set your user agent to prevent getting banned from the osm servers
        Configuration.getInstance().load(ctx, PreferenceManager.getDefaultSharedPreferences(ctx));


        setContentView(R.layout.home_activity_layout);
        cons = new Const();
        cd = new ConnectionDetector(getApplicationContext());
        sp = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        if (sp.getString("enabled_map", "") != null) {
            if (sp.getString("enabled_map", "").trim().length() > 0) {

                System.out.println("hi enabled map is " + sp.getString("enabled_map", ""));

                if (sp.getString("enabled_map", "").equalsIgnoreCase(getResources().getString(R.string.osm))) {
                    isOsmEnabled = true;
                } else {
                    isOsmEnabled = false;
                }
            }
        }

        init();
        screenArrange();

//        System.out.println("Home activity created ::::::");

        dbSetup();
//        mSelectedGroupId = "SBLT:SMP";
//        mSelectedUserID = "SBLT";

        if (sp.getString("ip_adds", "") != null) {
            if (sp.getString("ip_adds", "").trim().length() > 0) {
                Const.API_URL = sp.getString("ip_adds", "");
            }
        }

        mIsAutRefreshEnabled = sp.getBoolean("is_auto_refresh_enabled", false);

        mAutoRefreshValue = sp.getString("auto_refresh_interval", "");

        try {
            queryUserDB();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }


        $SpinnerNavigation
                .setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                    @Override
                    public void onItemSelected(AdapterView<?> parent,
                                               View view, int pos, long id) {
                        // TODO Auto-generated method stub
                        mSelectedGroupId = mGroupList.get(pos);
                        saveSelectedGroup(mGroupSpinnerList.get(pos));
                        if (mSelectedGroupId.equalsIgnoreCase("Select")) {

                        } else {
                            ConnectionDetector cd = new ConnectionDetector(getApplicationContext());
                            if (cd.isConnectingToInternet()) {

                                new PullHomeVehicleInformation().execute();
                            } else {
                                Toast.makeText(getApplicationContext(),getResources().getString(R.string.network_connection),
                                        Toast.LENGTH_SHORT).show();
                            }
                        }

                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> arg0) {
                        // TODO Auto-generated method stub

                    }

                });

        loadDatas(60000);
    }

    private void loadDatas(long timeInterval) {
        // TODO Auto-generated method stub

        if (timer != null) {
            timer.cancel();
        }
        timer = new Timer();
        timer.schedule(new TimerTask() {

            public void run() {

                isFirstTime = false;
                updateListData();

            }
        }, timeInterval, timeInterval);

    }

    public void updateListData() {


        runOnUiThread(new Runnable() {
            public void run() {


                if (cd.isConnectingToInternet()) {
                    new PullHomeVehicleInformation().execute();
                } else {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.network_connection),
                            Toast.LENGTH_SHORT).show();

                }

            }
        });


    }

    public void init() {


        $ImageBack = (ImageView) findViewById(R.id.home_view_Back);
        $ChangeView = (ImageView) findViewById(R.id.changeViewIcon);


        $TxtTitle = (TextView) findViewById(R.id.home_title);
        $TxtNoOfVehicle = (TextView) findViewById(R.id.home_all_vehicles_label);
        $TxtOnlineVehicle = (TextView) findViewById(R.id.home_online_vehicles_label);
        $TxtOfflineVehicle = (TextView) findViewById(R.id.home_offline_vehicles_label);
        $TxtNoOfVehicleValue = (TextView) findViewById(R.id.home_home_all_vehicles_value);
        $TxtOnlineVehicleValue = (TextView) findViewById(R.id.home_online_vehicles_value);
        $TxtOfflineVehicleValue = (TextView) findViewById(R.id.home_home_offline_vehicles_value);

        mImgRefresh = (ImageView) findViewById(R.id.home_refresh);

        $TxtNodataVehicle = (TextView) findViewById(R.id.id_nodata_vehicles_label);
        $TxtNodataVehicleValue = (TextView) findViewById(R.id.id_nodata_vehicles_value);

        $TxtMovingVehicle = (TextView) findViewById(R.id.id_moving_vehicles_label);
        $TxtMovingVehicleValue = (TextView) findViewById(R.id.id_moving_vehicles_value);


        $TxtTitle.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        $TxtNoOfVehicle.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        $TxtOnlineVehicle.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        $TxtOfflineVehicle.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        $TxtNoOfVehicleValue.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        $TxtOnlineVehicleValue.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        $TxtOfflineVehicleValue.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));

        $TxtNodataVehicle.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        $TxtNodataVehicleValue.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        $TxtMovingVehicle.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        $TxtMovingVehicleValue.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));

        $LayoutVehicle = (LinearLayout) findViewById(R.id.home_total_vehicles_layout);
        $LayoutOnlineVehicle = (LinearLayout) findViewById(R.id.home_online_vehicles_layout);
        $LayoutOfflineVehicle = (LinearLayout) findViewById(R.id.home_offline_vehicles_layout);

        $LayoutNodataVehicle = (LinearLayout) findViewById(R.id.id_nodata_vehicles_layout);
        $LayoutMovingVehicle = (LinearLayout) findViewById(R.id.id_moving_vehicles_layout);

        $SpinnerNavigation = (Spinner) findViewById(R.id.home_spinner);
        $View1 = (View) findViewById(R.id.view1_home1);
        $View2 = (View) findViewById(R.id.view2_home2);
        $View3 = (View) findViewById(R.id.view0_home1);

        $View4 = (View) findViewById(R.id.view1_nodata_home);
        $View5 = (View) findViewById(R.id.view1_moving_home);

        $ImageBack.setOnClickListener(this);
        mImgRefresh.setOnClickListener(this);
        $LayoutVehicle.setOnClickListener(this);
        $LayoutOnlineVehicle.setOnClickListener(this);
        $LayoutOfflineVehicle.setOnClickListener(this);
        $LayoutNodataVehicle.setOnClickListener(this);
        $LayoutMovingVehicle.setOnClickListener(this);
        $ChangeView.setOnClickListener(this);

        mGoogleMapLayout = (LinearLayout) findViewById(R.id.google_map_layout);

        RelativeLayout rl = (RelativeLayout) findViewById(R.id.map_view_relativelayout);

        if (isOsmEnabled) {
            mMarkersHashMapOSM = new HashMap<Marker, JSONObject>();
//        mapview = (MapView) findViewById(R.id.vehicles_osm_home_map);
            mGoogleMapLayout.setVisibility(View.GONE);
            rl.setVisibility(View.VISIBLE);

            mapview = new MapView(this);
            mapview.setTilesScaledToDpi(true);
            rl.addView(mapview, new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.FILL_PARENT,
                    RelativeLayout.LayoutParams.FILL_PARENT));
//        mapview.setTileSource(TileSourceFactory.MAPNIK);
            mapview.setBuiltInZoomControls(false);
            mapview.setMultiTouchControls(true);
            mapController = mapview.getController();
            mapController.setZoom(8);
            mapview.getOverlays().add(Constant.getTilesOverlay(getApplicationContext()));
            $ChangeView.setVisibility(View.GONE);
        } else {
            mGoogleMapLayout.setVisibility(View.VISIBLE);
            rl.setVisibility(View.GONE);
            mMarkersHashMapGoogle = new HashMap<com.google.android.gms.maps.model.Marker, JSONObject>();
            FragmentManager myFragmentManager = getSupportFragmentManager();
            SupportMapFragment myMapFragment = (SupportMapFragment) myFragmentManager
                    .findFragmentById(R.id.vehicles_home_map);
            myMapFragment.getMapAsync(this);
            $ChangeView.setVisibility(View.VISIBLE);
        }

    }

    void dbSetup() {
        dbhelper = new DBHelper(this);
        try {
            dbhelper.createDataBase();
            dbhelper.openDataBase();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String getStdTableValue(String tag) {
        Cursor std_table_cur = null;
        String stdtablevalues = null;
        try {
            std_table_cur = dbhelper.get_std_table_info(tag);
            std_table_cur.moveToFirst();
            stdtablevalues = std_table_cur.getString(0);
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.please_try_again), Toast.LENGTH_SHORT).show();
        } finally {
            if (std_table_cur != null) {
                std_table_cur.close();
            }
        }
        return stdtablevalues;
    }

    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub
        switch (v.getId()) {
            case R.id.home_view_Back:
                startActivity(new Intent(HomeActivity.this, VehicleListActivity.class));
                finish();
                break;
            case R.id.home_total_vehicles_layout:
                mSELECTED_VEHICLE_SHOW_TYPE = "ALL";
                if (isOsmEnabled) {
                    updateVehicleListInOSM(mResultData, "ALL");
                } else {
                    updateVehicleListInGoogle(mResultData, "ALL");
                }

                break;
            case R.id.home_online_vehicles_layout:
                mSELECTED_VEHICLE_SHOW_TYPE = "P";

                if (isOsmEnabled) {
                    updateVehicleListInOSM(mResultData, "P");
                } else {
                    updateVehicleListInGoogle(mResultData, "P");
                }

//                updateVehicleList(mResultData, "P");
                break;
            case R.id.home_offline_vehicles_layout:
                mSELECTED_VEHICLE_SHOW_TYPE = "I";

                if (isOsmEnabled) {
                    updateVehicleListInOSM(mResultData, "I");
                } else {
                    updateVehicleListInGoogle(mResultData, "I");
                }
//                updateVehicleList(mResultData, "I");
                break;
            case R.id.id_nodata_vehicles_layout:
                mSELECTED_VEHICLE_SHOW_TYPE = "NO";

                if (isOsmEnabled) {
                    updateVehicleListInOSM(mResultData, "NO");
                } else {
                    updateVehicleListInGoogle(mResultData, "NO");
                }

//                updateVehicleList(mResultData, "NO");
                break;
            case R.id.id_moving_vehicles_layout:
                mSELECTED_VEHICLE_SHOW_TYPE = "M";

                if (isOsmEnabled) {
                    updateVehicleListInOSM(mResultData, "M");
                } else {
                    updateVehicleListInGoogle(mResultData, "M");
                }

//                updateVehicleList(mResultData, "M");
                break;
            case R.id.changeViewIcon:
                opptionPopUp();
                break;
            case R.id.home_refresh:
                isFirstTime = false;
                new PullHomeVehicleInformation().execute();

                break;
        }
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {

        map = googleMap;
        map.setPadding(1, 1, 1, 150);
        map.getUiSettings().setZoomControlsEnabled(true);
        mMarkersHashMapGoogle = new HashMap<com.google.android.gms.maps.model.Marker, JSONObject>();
        map.setOnInfoWindowClickListener(this);

        try {
            queryUserDB();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        map.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {


            @Override
            public boolean onMarkerClick(com.google.android.gms.maps.model.Marker marker) {
                if (mMarkersHashMapGoogle.containsKey(marker)) {
                    if (cd.isConnectingToInternet()) {
                        jSelectedVehicleObject = mMarkersHashMapGoogle.get(marker);
                        showVehicleInfoDialog(jSelectedVehicleObject, false);
                    } else {
                        Toast internet_toast = Toast.makeText(getApplicationContext(), getResources().getString(R.string.network_connection), Toast.LENGTH_LONG);
                        internet_toast.show();
                    }
                    return true;
                }
                return true;

            }
        });

    }

    @Override
    public void onInfoWindowClick(com.google.android.gms.maps.model.Marker marker) {

    }

    /**
     * AsyncTask Background API Calls
     */
    public class PullHomeVehicleInformation extends AsyncTask<String, Integer, String> {
        ProgressDialog progressDialog;
//        int current_group_pos;
//        String current_group_name;

        @Override
        public String doInBackground(String... urls) {

            String response_from_server = "";
            try {


                String url;
                if (mSelectedGroupId.equalsIgnoreCase("Select")) {

                    url = Const.API_URL + "mobile/getVehicleLocations?userId=" + mSelectedUserID + "&macid=" + getStdTableValue("macid") + "&appid=" + getStdTableValue("appid");

                } else {
                    url = Const.API_URL + "mobile/getVehicleLocations?userId=" + mSelectedUserID + "&group=" + mSelectedGroupId + "&macid=" + getStdTableValue("macid") + "&appid=" + getStdTableValue("appid");
                }

//                String url = Const.API_URL + "/mobile/getVehicleLocations?userId=" + mSelectedUserID + "&group=" + mSelectedGroupId + "&macid=90:21:81:36:12:d2&appid=com.gpsvts";
//                String url = "http://vamosys.com/mobile/getVehicleLocations?userId=" + mSelectedUserID + "&group=" + mSelectedGroupId + "&macid=90:21:81:36:12:d2&appid=com.gpsvts";
//                System.out.println("The pull vehicle info url is 00000::::" + url);
                HttpConfig ht = new HttpConfig();
                response_from_server = ht.httpGet(url);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return response_from_server;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = MyCustomProgressDialog.ctor(HomeActivity.this);
            progressDialog.setCancelable(false);
            progressDialog.show();
        }


        @Override
        public void onPostExecute(String result) {
            //Cursor vehicle_info = null;


            try {
                if (result != null && result.length() > 0) {
                    /**code to display all vehicles in map*/
                    mResultData = result;
                    if (isOsmEnabled) {
                        updateVehicleListInOSM(result, mSELECTED_VEHICLE_SHOW_TYPE);
                    } else {
                        updateVehicleListInGoogle(result, mSELECTED_VEHICLE_SHOW_TYPE);
                    }
//                    updateVehicleList(result, mSELECTED_VEHICLE_SHOW_TYPE);
//                if (progressDialog.isShowing()) {
//                    progressDialog.dismiss();
//                }
                } else {
                    Toast.makeText(getApplicationContext(), ""+getResources().getString(R.string.no_record_found), Toast.LENGTH_SHORT).show();
                }
            } catch (Exception e) {
            } finally {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

            }

        }
    }

    /*
    The below method is for OSM map
     */

    public void updateVehicleListInOSM(String data, String mSelectedVehicleType) {


//        System.out.println("The res in update list is :::::" + data);
        if (!isFirstTime) {
//            group_zoom_level = (map.getCameraPosition().zoom);
            group_zoom_level = mapview.getZoomLevel();
        }

//        LatLngBounds lb = map.getProjection().getVisibleRegion().latLngBounds;
//        System.out.println("Prev region  is :::" + map.getProjection().getVisibleRegion());

        if (data != null) {
            try {

//                if (map != null) {
//                    map.clear();
//                }

                if (mapview != null) {
                    mapview.getOverlays().clear();
                    mapview.invalidate();
                    mapview.getOverlays().add(Constant.getTilesOverlay(getApplicationContext()));
                }


                //clear the views from osm


                JSONArray parentarray = new JSONArray(data);

//            System.out.println("The json array is ::::" + parentarray.length());

                for (int i = 0; i < parentarray.length(); i++) {

                    final JSONObject parentobj = parentarray.getJSONObject(i);
                    if (parentobj.getString("group").equalsIgnoreCase(mSelectedGroupId)) {
//                    System.out.println("The selected group id ");
                        lat = Double.parseDouble(parentobj.getString("latitude"));
                        lng = Double.parseDouble(parentobj.getString("longitude"));
                        $TxtNoOfVehicleValue.setText(parentobj.getString("totalVehicles"));
                        $TxtOnlineVehicleValue.setText(parentobj.getString("online"));
                        $TxtOfflineVehicleValue.setText(parentobj.getString("attention"));


                        if (parentobj.getString("totalVehicles") != null) {
                            $TxtNoOfVehicleValue.setText(parentobj.getString("totalVehicles"));
                        }

//                            if (mVehicleGroupList.get(i).getTotalParkedVehicles() != null) {

//                            System.out.println("The total parked vehicle is ::::" + mVehicleGroupList.get(i).getTotalParkedVehicles());

                        $TxtOnlineVehicleValue.setText(String.valueOf(parentobj.getString("totalParkedVehicles")));

//                            System.out.println("The total idle vehicle is ::::" + mVehicleGroupList.get(i).getTotalIdleVehicles());
//                            }
//                            if (mVehicleGroupList.get(i).getAttention() != null) {
                        $TxtOfflineVehicleValue.setText(String.valueOf(parentobj.getString("totalIdleVehicles")));
//                            }
//                            System.out.println("The total no data vehicle is ::::" + mVehicleGroupList.get(i).getTotalNoDataVehicles());
//                            if (mVehicleGroupList.get(i).getAttention() != null) {
                        $TxtNodataVehicleValue.setText(String.valueOf(parentobj.getString("totalNoDataVehicles")));
//                            }

//                            if (mVehicleGroupList.get(i).getAttention() != null) {
//                            System.out.println("The total moving vehicle is ::::" + mVehicleGroupList.get(i).getTotalMovingVehicles());
                        $TxtMovingVehicleValue.setText(String.valueOf(parentobj.getString("totalMovingVehicles")));


                        if (!parentobj.getString("group").equals("null")) {
                            try {
                                JSONArray vehiclearr = parentobj.getJSONArray("vehicleLocations");
                                // JSONArray vehiclearr = new JSONArray(vehicle_info.getString(1));
                                //  ArrayList<String> child = new ArrayList<String>();
                                //   System.out.println("The zoom level is :::::" + group_zoom_level);
                                if (!isFirstTime) {


//                                    CameraPosition INIT = new CameraPosition.Builder().target(new LatLng(lb)).zoom(group_zoom_level).build();
//                                    CameraPosition INIT = new CameraPosition.Builder().zoom(group_zoom_level).build();
//                                    map.animateCamera(CameraUpdateFactory.newCameraPosition(INIT));
//
//                                    map.moveCamera(CameraUpdateFactory.newLatLngBounds(lb, 10));

                                } else {
//                                    CameraPosition INIT = new CameraPosition.Builder().target(new LatLng(lat, lng)).zoom(group_zoom_level).build();
////                                CameraPosition INIT = new CameraPosition.Builder().zoom(group_zoom_level).build();
//                                    map.animateCamera(CameraUpdateFactory.newCameraPosition(INIT));

                                    GeoPoint newPos = new GeoPoint(lat, lng);
                                    mapController.setCenter(newPos);

                                }
//                                CameraPosition INIT = new CameraPosition.Builder().target(new LatLng(lat, lng)).zoom(group_zoom_level).build();
////                                CameraPosition INIT = new CameraPosition.Builder().zoom(group_zoom_level).build();
//                                map.animateCamera(CameraUpdateFactory.newCameraPosition(INIT));

                                if (child != null) {
                                    if (!child.isEmpty()) {
                                        child.clear();
                                        //jsonchild.clear();
                                    }
                                }

                                if (mMarkersHashMapOSM != null) {
                                    mMarkersHashMapOSM.clear();
                                }

                                if (mOSMMarkerList != null) {
                                    if (!mOSMMarkerList.isEmpty()) {
                                        mOSMMarkerList.clear();
                                        //jsonchild.clear();
                                    }
                                }

                                for (int vehicle = 0; vehicle < vehiclearr.length(); vehicle++) {
                                    Marker osmMarker = new Marker(mapview);
                                    final String vehicleID, vehicleType;
                                    JSONObject vehiclejson = vehiclearr.getJSONObject(vehicle);

                                    child.add(vehiclejson.getString("vehicleId"));
                                    try {
                                        vehiclejson = vehiclearr.getJSONObject(vehicle);
                                    } catch (JSONException e1) {
                                        e1.printStackTrace();
                                    }


                                    try {
                                        View marker = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.custommarker_osm_info, null);
                                        ImageView id_custom_marker_icon = (ImageView) marker.findViewById(R.id.id_custom_marker_icon);
                                        ImageView id_vehicle_in_marker = (ImageView) marker.findViewById(R.id.id_vehicle_in_marker);
                                        ImageView id_custom_marker_direction_icon = (ImageView) marker.findViewById(R.id.id_custom_marker_direction_icon);
                                        TextView txt_vehiclename = (TextView) marker.findViewById(R.id.vehicle_name_txt);

//                                        System.out.println("The vehicle dir is :::" + vehiclejson.getString("direction"));

                                        if (vehiclejson.has("direction")) {
                                            id_custom_marker_direction_icon.setVisibility(View.VISIBLE);

                                            if (vehiclejson.getString("direction").equals("N")) {
                                                id_custom_marker_direction_icon.setImageResource(R.drawable.north);
                                            } else if (vehiclejson.getString("direction").equals("S")) {
                                                id_custom_marker_direction_icon.setImageResource(R.drawable.south);
                                            } else if (vehiclejson.getString("direction").equals("E")) {
                                                id_custom_marker_direction_icon.setImageResource(R.drawable.east);
                                            } else if (vehiclejson.getString("direction").equals("W")) {
                                                id_custom_marker_direction_icon.setImageResource(R.drawable.west);
                                            } else if (vehiclejson.getString("direction").equals("NE")) {
                                                id_custom_marker_direction_icon.setImageResource(R.drawable.northeast);
                                            } else if (vehiclejson.getString("direction").equals("NW")) {
                                                id_custom_marker_direction_icon.setImageResource(R.drawable.northwest);
                                            } else if (vehiclejson.getString("direction").equals("SE")) {
                                                id_custom_marker_direction_icon.setImageResource(R.drawable.southeast);
                                            } else if (vehiclejson.getString("direction").equals("SW")) {
                                                id_custom_marker_direction_icon.setImageResource(R.drawable.southwest);
                                            } else if (vehiclejson.getString("direction").equals("null")) {
                                                id_custom_marker_direction_icon.setVisibility(View.GONE);
                                            }
                                        }


                                        if (!vehiclejson.isNull("direction")) {

                                            Constant.vehicle_direction_in_map(vehiclejson.getString("vehicleType"), vehiclejson.getString("direction"), id_vehicle_in_marker);
                                            id_vehicle_in_marker.setColorFilter(Color.WHITE);

                                        }

//                                        System.out.println("The vehicle posi is :::" + vehiclejson.getString("position"));

                                        if (vehiclejson.getString("isOverSpeed").equalsIgnoreCase("Y")) {
                                            id_custom_marker_icon.setImageResource(R.drawable.red_custom_marker_icon);
                                        } else if (vehiclejson.getString("insideGeoFence").equalsIgnoreCase("Y")) {
                                            id_custom_marker_icon.setImageResource(R.drawable.red_custom_marker_icon);
                                        } else if (vehiclejson.getString("position").equalsIgnoreCase("M")) {
                                            id_custom_marker_icon.setImageResource(R.drawable.green_custom_marker_icon);
                                        } else if (vehiclejson.getString("position").equalsIgnoreCase("P")) {
                                            id_custom_marker_icon.setImageResource(R.drawable.black_custom_marker_icon);
                                        } else if (vehiclejson.getString("position").equalsIgnoreCase("S")) {
                                            id_custom_marker_icon.setImageResource(R.drawable.yellow_custom_marker_icon);
                                        } else if (vehiclejson.getString("position").equalsIgnoreCase("N")) {
                                            id_custom_marker_icon.setImageResource(R.drawable.yellow_custom_marker_icon);
                                        } else if (vehiclejson.getString("position").equalsIgnoreCase("U")) {
                                            id_custom_marker_icon.setImageResource(R.drawable.red_custom_marker_icon);
                                        }
                                        txt_vehiclename.setText(vehiclejson.getString("shortName"));

                                        if (mSelectedVehicleType.equalsIgnoreCase("ALL")) {

//                                            System.out.println("The all vehicle name is :::" + vehiclejson.getString("vehicleId"));

//                                            MarkerOptions markerOption = new MarkerOptions().position(new LatLng(Double.parseDouble(vehiclejson.getString("latitude")), Double.parseDouble(vehiclejson.getString("longitude"))));
//                                            markerOption.title(vehiclejson.getString("vehicleId"));
//                                            markerOption.icon(BitmapDescriptorFactory.fromBitmap(createDrawableFromView(HomeActivity.this, marker)));
//                                            Marker currentMarker = map.addMarker(markerOption);
//                                            mMarkersHashMap.put(currentMarker, vehiclejson);
//                                            mMarkerList.add(currentMarker);

                                            GeoPoint loc = new GeoPoint(Double.parseDouble(vehiclejson.getString("latitude")), Double.parseDouble(vehiclejson.getString("longitude")));
                                            osmMarker.setPosition(loc);
                                            osmMarker.setAnchor(Marker.ANCHOR_CENTER, Marker.ANCHOR_BOTTOM);
                                            osmMarker.setTitle(vehiclejson.getString("vehicleId"));
                                            mMarkersHashMapOSM.put(osmMarker, vehiclejson);
                                            osmMarker.setIcon(createDrawableFromViewNew(HomeActivity.this, marker));
                                            mOSMMarkerList.add(osmMarker);

                                        } else if (mSelectedVehicleType.equalsIgnoreCase("P")) {

                                            if (vehiclejson.getString("position").equalsIgnoreCase("P")) {
//                                                System.out.println("The parked vehicle name is :::" + vehiclejson.getString("vehicleId"));
//                                                MarkerOptions markerOption = new MarkerOptions().position(new LatLng(Double.parseDouble(vehiclejson.getString("latitude")), Double.parseDouble(vehiclejson.getString("longitude"))));
//                                                markerOption.title(vehiclejson.getString("vehicleId"));
//                                                markerOption.icon(BitmapDescriptorFactory.fromBitmap(createDrawableFromView(HomeActivity.this, marker)));
//                                                Marker currentMarker = map.addMarker(markerOption);

//                                                mMarkersHashMap.put(currentMarker, vehiclejson);
//                                                mMarkerList.add(currentMarker);


                                                GeoPoint loc = new GeoPoint(Double.parseDouble(vehiclejson.getString("latitude")), Double.parseDouble(vehiclejson.getString("longitude")));
                                                osmMarker.setPosition(loc);
                                                osmMarker.setAnchor(Marker.ANCHOR_CENTER, Marker.ANCHOR_BOTTOM);
                                                osmMarker.setTitle(vehiclejson.getString("vehicleId"));
                                                osmMarker.setIcon(createDrawableFromViewNew(HomeActivity.this, marker));
                                                child.add(vehiclejson.getString("vehicleId"));
                                                mMarkersHashMapOSM.put(osmMarker, vehiclejson);
                                                mOSMMarkerList.add(osmMarker);
                                            }

                                        } else if (mSelectedVehicleType.equalsIgnoreCase("M")) {
                                            if (vehiclejson.getString("position").equalsIgnoreCase("M")) {
//                                                System.out.println("The moving vehicle name is :::" + vehiclejson.getString("vehicleId"));
//                                                MarkerOptions markerOption = new MarkerOptions().position(new LatLng(Double.parseDouble(vehiclejson.getString("latitude")), Double.parseDouble(vehiclejson.getString("longitude"))));
//                                                markerOption.title(vehiclejson.getString("vehicleId"));
//                                                markerOption.icon(BitmapDescriptorFactory.fromBitmap(createDrawableFromView(HomeActivity.this, marker)));
//                                                Marker currentMarker = map.addMarker(markerOption);
//                                                mMarkersHashMap.put(currentMarker, vehiclejson);
//                                                mMarkerList.add(currentMarker);
                                                GeoPoint loc = new GeoPoint(Double.parseDouble(vehiclejson.getString("latitude")), Double.parseDouble(vehiclejson.getString("longitude")));
                                                osmMarker.setPosition(loc);
                                                osmMarker.setAnchor(Marker.ANCHOR_CENTER, Marker.ANCHOR_BOTTOM);
                                                osmMarker.setTitle(vehiclejson.getString("vehicleId"));
                                                osmMarker.setIcon(createDrawableFromViewNew(HomeActivity.this, marker));
                                                child.add(vehiclejson.getString("vehicleId"));
                                                mMarkersHashMapOSM.put(osmMarker, vehiclejson);
                                                mOSMMarkerList.add(osmMarker);

                                            }

                                        } else if (mSelectedVehicleType.equalsIgnoreCase("I")) {
                                            if (vehiclejson.getString("position").equalsIgnoreCase("S")) {
//                                                System.out.println("The idle vehicle name is :::" + vehiclejson.getString("vehicleId"));
//                                                MarkerOptions markerOption = new MarkerOptions().position(new LatLng(Double.parseDouble(vehiclejson.getString("latitude")), Double.parseDouble(vehiclejson.getString("longitude"))));
//                                                markerOption.title(vehiclejson.getString("vehicleId"));
//                                                markerOption.icon(BitmapDescriptorFactory.fromBitmap(createDrawableFromView(HomeActivity.this, marker)));
//                                                Marker currentMarker = map.addMarker(markerOption);
//                                                mMarkersHashMap.put(currentMarker, vehiclejson);
//                                                mMarkerList.add(currentMarker);



                                                GeoPoint loc = new GeoPoint(Double.parseDouble(vehiclejson.getString("latitude")), Double.parseDouble(vehiclejson.getString("longitude")));
                                                osmMarker.setPosition(loc);
                                                osmMarker.setAnchor(Marker.ANCHOR_CENTER, Marker.ANCHOR_BOTTOM);
                                                osmMarker.setTitle(vehiclejson.getString("vehicleId"));
                                                osmMarker.setIcon(createDrawableFromViewNew(HomeActivity.this, marker));
                                                child.add(vehiclejson.getString("vehicleId"));
                                                mMarkersHashMapOSM.put(osmMarker, vehiclejson);
                                                mOSMMarkerList.add(osmMarker);

                                            }
                                        } else if (mSelectedVehicleType.equalsIgnoreCase("No")) {
                                            if (vehiclejson.getString("position").equalsIgnoreCase("U")) {
//                                                System.out.println("The no data vehicle name is :::" + vehiclejson.getString("vehicleId"));
//                                                MarkerOptions markerOption = new MarkerOptions().position(new LatLng(Double.parseDouble(vehiclejson.getString("latitude")), Double.parseDouble(vehiclejson.getString("longitude"))));
//                                                markerOption.title(vehiclejson.getString("vehicleId"));
//                                                markerOption.icon(BitmapDescriptorFactory.fromBitmap(createDrawableFromView(HomeActivity.this, marker)));
//                                                Marker currentMarker = map.addMarker(markerOption);
//                                                mMarkersHashMap.put(currentMarker, vehiclejson);
//                                                mMarkerList.add(currentMarker);


                                                GeoPoint loc = new GeoPoint(Double.parseDouble(vehiclejson.getString("latitude")), Double.parseDouble(vehiclejson.getString("longitude")));
                                                osmMarker.setPosition(loc);
                                                osmMarker.setAnchor(Marker.ANCHOR_CENTER, Marker.ANCHOR_BOTTOM);
                                                osmMarker.setTitle(vehiclejson.getString("vehicleId"));
                                                osmMarker.setIcon(createDrawableFromViewNew(HomeActivity.this, marker));
                                                child.add(vehiclejson.getString("vehicleId"));
                                                mMarkersHashMapOSM.put(osmMarker, vehiclejson);
                                                mOSMMarkerList.add(osmMarker);

                                            }
                                        }

                                        mapview.getOverlays().add(osmMarker);
                                        mapview.invalidate();

                                        osmMarker.setOnMarkerClickListener(new Marker.OnMarkerClickListener() {
                                            @Override
                                            public boolean onMarkerClick(Marker marker, MapView mapView) {

//                                                Toast.makeText(getApplicationContext(), "Marker clicked ", Toast.LENGTH_SHORT).show();

                                                if (mMarkersHashMapOSM.containsKey(marker)) {
//                                                    if (cd.isConnectingToInternet()) {
                                                    jSelectedVehicleObject = mMarkersHashMapOSM.get(marker);
                                                    showVehicleInfoDialog(jSelectedVehicleObject, false);
//                                                    } else {
//                                                        Toast internet_toast = Toast.makeText(getApplicationContext(), "Please Check your Internet Connection", Toast.LENGTH_LONG);
//                                                        internet_toast.show();
//                                                    }
                                                    return true;
                                                }

                                                return false;
                                            }
                                        });


//                                    MarkerOptions markerOption = new MarkerOptions().position(new LatLng(Double.parseDouble(vehiclejson.getString("latitude")), Double.parseDouble(vehiclejson.getString("longitude"))));
//                                    markerOption.title(vehiclejson.getString("vehicleId"));
//                                    markerOption.icon(BitmapDescriptorFactory.fromBitmap(createDrawableFromView(HomeActivity.this, marker)));
//                                    Marker currentMarker = map.addMarker(markerOption);
//                                    mMarkersHashMap.put(currentMarker, vehiclejson);
//                                    mMarkerList.add(currentMarker);
                                    } catch (JSONException e1) {
                                        e1.printStackTrace();
                                    }
                                }


                            } catch (Exception e) {
                                // TODO Auto-generated catch block
                                e.printStackTrace();
                            }
                        }

                    }
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }


    public void updateVehicleListInGoogle(String data, String mSelectedVehicleType) {
        if (!isFirstTime) {
            group_zoom_level = (map.getCameraPosition().zoom);
        }

        LatLngBounds lb = map.getProjection().getVisibleRegion().latLngBounds;
//        System.out.println("Prev region  is :::" + map.getProjection().getVisibleRegion());

        if (data != null) {
            try {

                if (map != null) {
                    map.clear();
                }
                JSONArray parentarray = new JSONArray(data);

//            System.out.println("The json array is ::::" + parentarray.length());

                for (int i = 0; i < parentarray.length(); i++) {

                    final JSONObject parentobj = parentarray.getJSONObject(i);
                    if (parentobj.getString("group").equalsIgnoreCase(mSelectedGroupId)) {
//                    System.out.println("The selected group id ");
                        lat = Double.parseDouble(parentobj.getString("latitude"));
                        lng = Double.parseDouble(parentobj.getString("longitude"));
                        $TxtNoOfVehicleValue.setText(parentobj.getString("totalVehicles"));
                        $TxtOnlineVehicleValue.setText(parentobj.getString("online"));
                        $TxtOfflineVehicleValue.setText(parentobj.getString("attention"));


                        if (parentobj.getString("totalVehicles") != null) {
                            $TxtNoOfVehicleValue.setText(parentobj.getString("totalVehicles"));
                        }

//                            if (mVehicleGroupList.get(i).getTotalParkedVehicles() != null) {

//                            System.out.println("The total parked vehicle is ::::" + mVehicleGroupList.get(i).getTotalParkedVehicles());

                        $TxtOnlineVehicleValue.setText(String.valueOf(parentobj.getString("totalParkedVehicles")));

//                            System.out.println("The total idle vehicle is ::::" + mVehicleGroupList.get(i).getTotalIdleVehicles());
//                            }
//                            if (mVehicleGroupList.get(i).getAttention() != null) {
                        $TxtOfflineVehicleValue.setText(String.valueOf(parentobj.getString("totalIdleVehicles")));
//                            }
//                            System.out.println("The total no data vehicle is ::::" + mVehicleGroupList.get(i).getTotalNoDataVehicles());
//                            if (mVehicleGroupList.get(i).getAttention() != null) {
                        $TxtNodataVehicleValue.setText(String.valueOf(parentobj.getString("totalNoDataVehicles")));
//                            }

//                            if (mVehicleGroupList.get(i).getAttention() != null) {
//                            System.out.println("The total moving vehicle is ::::" + mVehicleGroupList.get(i).getTotalMovingVehicles());
                        $TxtMovingVehicleValue.setText(String.valueOf(parentobj.getString("totalMovingVehicles")));


                        if (!parentobj.getString("group").equals("null")) {
                            try {
                                JSONArray vehiclearr = parentobj.getJSONArray("vehicleLocations");
                                // JSONArray vehiclearr = new JSONArray(vehicle_info.getString(1));
                                //  ArrayList<String> child = new ArrayList<String>();
                                //   System.out.println("The zoom level is :::::" + group_zoom_level);
                                if (!isFirstTime) {


//                                    CameraPosition INIT = new CameraPosition.Builder().target(new LatLng(lb)).zoom(group_zoom_level).build();
//                                    CameraPosition INIT = new CameraPosition.Builder().zoom(group_zoom_level).build();
//                                    map.animateCamera(CameraUpdateFactory.newCameraPosition(INIT));
//
//                                    map.moveCamera(CameraUpdateFactory.newLatLngBounds(lb, 10));

                                } else {
                                    CameraPosition INIT = new CameraPosition.Builder().target(new LatLng(lat, lng)).zoom(group_zoom_level).build();
//                                CameraPosition INIT = new CameraPosition.Builder().zoom(group_zoom_level).build();
                                    map.animateCamera(CameraUpdateFactory.newCameraPosition(INIT));
                                }
//                                CameraPosition INIT = new CameraPosition.Builder().target(new LatLng(lat, lng)).zoom(group_zoom_level).build();
////                                CameraPosition INIT = new CameraPosition.Builder().zoom(group_zoom_level).build();
//                                map.animateCamera(CameraUpdateFactory.newCameraPosition(INIT));

                                if (child != null) {
                                    if (!child.isEmpty()) {
                                        child.clear();
                                        //jsonchild.clear();
                                    }
                                }

                                if (mMarkersHashMapGoogle != null) {
                                    mMarkersHashMapGoogle.clear();
                                }

                                if (mGoogleMarkerList != null) {
                                    if (!mGoogleMarkerList.isEmpty()) {
                                        mGoogleMarkerList.clear();
                                        //jsonchild.clear();
                                    }
                                }

                                for (int vehicle = 0; vehicle < vehiclearr.length(); vehicle++) {
                                    final String vehicleID, vehicleType;
                                    JSONObject vehiclejson = vehiclearr.getJSONObject(vehicle);

                               //     child.add(vehiclejson.getString("vehicleId"));
                                    try {
                                        vehiclejson = vehiclearr.getJSONObject(vehicle);
                                    } catch (JSONException e1) {
                                        e1.printStackTrace();
                                    }


                                    try {
//                                        shortName
                                        vehicleID = vehiclejson.getString("vehicleId");
                                        vehicleType = vehiclejson.getString("vehicleType");
                                    } catch (JSONException e1) {
                                        e1.printStackTrace();
                                    }

                                    try {
//                                        View marker = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.custommarker, null);
//                                        ImageView id_custom_marker_icon = (ImageView) marker.findViewById(R.id.id_custom_marker_icon);
//                                        ImageView id_vehicle_in_marker = (ImageView) marker.findViewById(R.id.id_vehicle_in_marker);
//                                        ImageView id_custom_marker_direction_icon = (ImageView) marker.findViewById(R.id.id_custom_marker_direction_icon);


                                        View marker = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.custommarker_info, null);
                                        ImageView id_custom_marker_icon = (ImageView) marker.findViewById(R.id.id_custom_marker_icon);
                                        ImageView id_vehicle_in_marker = (ImageView) marker.findViewById(R.id.id_vehicle_in_marker);
                                        ImageView id_custom_marker_direction_icon = (ImageView) marker.findViewById(R.id.id_custom_marker_direction_icon);
                                        TextView txt_vehiclename = (TextView) marker.findViewById(R.id.vehicle_name_txt);

                                        if (vehiclejson.has("direction")) {
                                            id_custom_marker_direction_icon.setVisibility(View.VISIBLE);

                                            if (vehiclejson.getString("direction").equals("N")) {
                                                id_custom_marker_direction_icon.setImageResource(R.drawable.north);
                                            } else if (vehiclejson.getString("direction").equals("S")) {
                                                id_custom_marker_direction_icon.setImageResource(R.drawable.south);
                                            } else if (vehiclejson.getString("direction").equals("E")) {
                                                id_custom_marker_direction_icon.setImageResource(R.drawable.east);
                                            } else if (vehiclejson.getString("direction").equals("W")) {
                                                id_custom_marker_direction_icon.setImageResource(R.drawable.west);
                                            } else if (vehiclejson.getString("direction").equals("NE")) {
                                                id_custom_marker_direction_icon.setImageResource(R.drawable.northeast);
                                            } else if (vehiclejson.getString("direction").equals("NW")) {
                                                id_custom_marker_direction_icon.setImageResource(R.drawable.northwest);
                                            } else if (vehiclejson.getString("direction").equals("SE")) {
                                                id_custom_marker_direction_icon.setImageResource(R.drawable.southeast);
                                            } else if (vehiclejson.getString("direction").equals("SW")) {
                                                id_custom_marker_direction_icon.setImageResource(R.drawable.southwest);
                                            } else if (vehiclejson.getString("direction").equals("null")) {
                                                id_custom_marker_direction_icon.setVisibility(View.GONE);
                                            }
                                        }


                                        if (!vehiclejson.isNull("direction")) {

                                            Constant.vehicle_direction_in_map(vehiclejson.getString("vehicleType"), vehiclejson.getString("direction"), id_vehicle_in_marker);
                                            id_vehicle_in_marker.setColorFilter(Color.WHITE);

                                        }
                                        Log.d("isOverSpeed",""+vehiclejson.getString("isOverSpeed").equalsIgnoreCase("Y"));
                                        Log.d("insideGeoFence",""+vehiclejson.getString("insideGeoFence").equalsIgnoreCase("Y"));
                                        Log.d("mmposition",""+vehiclejson.getString("position").equalsIgnoreCase("M"));
                                        Log.d("ppposition",""+vehiclejson.getString("position").equalsIgnoreCase("P"));
                                        Log.d("ssposition",""+vehiclejson.getString("position").equalsIgnoreCase("S"));
                                        Log.d("nnposition",""+vehiclejson.getString("position").equalsIgnoreCase("N"));
                                        Log.d("uuposition",""+vehiclejson.getString("position").equalsIgnoreCase("U"));

                                        if (vehiclejson.getString("isOverSpeed").equalsIgnoreCase("Y")) {
                                            id_custom_marker_icon.setImageResource(R.drawable.red_custom_marker_icon);
                                        } else if (vehiclejson.getString("insideGeoFence").equalsIgnoreCase("Y")) {
                                            id_custom_marker_icon.setImageResource(R.drawable.red_custom_marker_icon);
                                        } else if (vehiclejson.getString("position").equalsIgnoreCase("M")) {
                                            id_custom_marker_icon.setImageResource(R.drawable.green_custom_marker_icon);
                                        } else if (vehiclejson.getString("position").equalsIgnoreCase("P")) {
                                            Log.d("parkingarea","success");
                                            id_custom_marker_icon.setImageResource(R.drawable.black_custom_marker_icon);
                                        } else if (vehiclejson.getString("position").equalsIgnoreCase("S")) {
                                            id_custom_marker_icon.setImageResource(R.drawable.yellow_custom_marker_icon);
                                        } else if (vehiclejson.getString("position").equalsIgnoreCase("N")) {
                                            id_custom_marker_icon.setImageResource(R.drawable.grey_custom_marker_icon);
                                        } else if (vehiclejson.getString("position").equalsIgnoreCase("U")) {
                                            Log.d("uuparkingarea","success");
                                            id_custom_marker_icon.setImageResource(R.drawable.red_custom_marker_icon);
                                        }

                                        txt_vehiclename.setText(vehiclejson.getString("shortName"));
//                                        TextView text = new TextView(getApplicationContext());
//                                        text.setText(vehiclejson.getString("shortName"));
//                                        IconGenerator generator = new IconGenerator(getApplicationContext());
//                                        generator.setBackground(createDrawableFromViewNew(HomeActivity.this, marker));
//                                        generator.setContentView(text);
//                                        Bitmap icon = generator.makeIcon();


//                                        Bitmap.Config conf = Bitmap.Config.ARGB_8888;
//                                        Bitmap icon = Bitmap.createBitmap(80, 80, conf);
//                                        Canvas canvas1 = new Canvas(icon);
//
//// paint defines the text color, stroke width and size
//                                        Paint color = new Paint();
//                                        color.setTextSize(35);
//                                        color.setColor(Color.BLACK);
//
//// modify canvas
//                                        canvas1.drawBitmap(createDrawableFromView(HomeActivity.this, marker), 0, 0, color);
//                                        canvas1.drawText(vehiclejson.getString("shortName"), 30, 40, color);


                                        if (mSelectedVehicleType.equalsIgnoreCase("ALL")) {

                                            //    System.out.println("The all vehicle name is :::" + vehiclejson.getString("vehicleId"));

//                                            MarkerOptions tp = new MarkerOptions().position(new LatLng(Double.parseDouble(vehiclejson.getString("latitude")), Double.parseDouble(vehiclejson.getString("longitude")))).icon(BitmapDescriptorFactory.fromBitmap(icon));

//                                            MarkerOptions markerOption = new MarkerOptions().position(new LatLng(Double.parseDouble(vehiclejson.getString("latitude")), Double.parseDouble(vehiclejson.getString("longitude"))));
//                                            markerOption.title(vehiclejson.getString("vehicleId"));
//                                            markerOption.icon(BitmapDescriptorFactory.fromBitmap(createDrawableFromView(HomeActivity.this, marker)));
////                                            markerOption.icon(BitmapDescriptorFactory.fromBitmap(icon));
////                                            markerOption.anchor(0.5f, 1);
////                                            markerOption.icon(icon);
//
//                                            com.google.android.gms.maps.model.Marker currentMarker = map.addMarker(markerOption);
////                                            currentMarker.showInfoWindow();
//                                            mMarkersHashMapGoogle.put(currentMarker, vehiclejson);
//                                            mGoogleMarkerList.add(currentMarker);
                                            child.add(vehiclejson.getString("vehicleId"));
                                            MarkerOptions markerOption = new MarkerOptions().position(new LatLng(Double.parseDouble(vehiclejson.getString("latitude")), Double.parseDouble(vehiclejson.getString("longitude"))));
                                            markerOption.title(vehiclejson.getString("vehicleId"));
                                            markerOption.icon(BitmapDescriptorFactory.fromBitmap(createDrawableFromView(HomeActivity.this, marker)));
                                            com.google.android.gms.maps.model.Marker currentMarker = map.addMarker(markerOption);
                                            mMarkersHashMapGoogle.put(currentMarker, vehiclejson);
                                            mGoogleMarkerList.add(currentMarker);
                                        } else if (mSelectedVehicleType.equalsIgnoreCase("P")) {

                                            if (vehiclejson.getString("position").equalsIgnoreCase("P")) {
                                                // System.out.println("The parked vehicle name is :::" + vehiclejson.getString("vehicleId"));
//                                                MarkerOptions markerOption = new MarkerOptions().position(new LatLng(Double.parseDouble(vehiclejson.getString("latitude")), Double.parseDouble(vehiclejson.getString("longitude"))));
//                                                markerOption.title(vehiclejson.getString("vehicleId"));
//                                                markerOption.icon(BitmapDescriptorFactory.fromBitmap(createDrawableFromView(HomeActivity.this, marker)));
////                                                markerOption.icon(BitmapDescriptorFactory.fromBitmap(icon));
////                                                markerOption.anchor(0.5f, 1);
//                                                com.google.android.gms.maps.model.Marker currentMarker = map.addMarker(markerOption);
////                                                currentMarker.showInfoWindow();
//
//                                                mMarkersHashMapGoogle.put(currentMarker, vehiclejson);
//                                                mGoogleMarkerList.add(currentMarker);
                                                markerOption = new MarkerOptions().position(new LatLng(Double.parseDouble(vehiclejson.getString("latitude")), Double.parseDouble(vehiclejson.getString("longitude"))));
                                                markerOption.title(vehiclejson.getString("vehicleId"));
                                                markerOption.icon(BitmapDescriptorFactory.fromBitmap(createDrawableFromView(HomeActivity.this, marker)));
                                                currentMarker = map.addMarker(markerOption);
                                                mMarkersHashMapGoogle.put(currentMarker, vehiclejson);
                                                mGoogleMarkerList.add(currentMarker);
                                                child.add(vehiclejson.getString("vehicleId"));
                                            }

                                        } else if (mSelectedVehicleType.equalsIgnoreCase("M")) {
                                            if (vehiclejson.getString("position").equalsIgnoreCase("M")) {
                                                // System.out.println("The moving vehicle name is :::" + vehiclejson.getString("vehicleId"));
//                                                MarkerOptions markerOption = new MarkerOptions().position(new LatLng(Double.parseDouble(vehiclejson.getString("latitude")), Double.parseDouble(vehiclejson.getString("longitude"))));
//                                                markerOption.title(vehiclejson.getString("vehicleId"));
//                                                markerOption.icon(BitmapDescriptorFactory.fromBitmap(createDrawableFromView(HomeActivity.this, marker)));
////                                                markerOption.icon(BitmapDescriptorFactory.fromBitmap(icon));
////                                                markerOption.anchor(0.5f, 1);
//                                                com.google.android.gms.maps.model.Marker currentMarker = map.addMarker(markerOption);
////                                                currentMarker.showInfoWindow();
//                                                mMarkersHashMapGoogle.put(currentMarker, vehiclejson);
//                                                mGoogleMarkerList.add(currentMarker);
                                                markerOption = new MarkerOptions().position(new LatLng(Double.parseDouble(vehiclejson.getString("latitude")), Double.parseDouble(vehiclejson.getString("longitude"))));
                                                markerOption.title(vehiclejson.getString("vehicleId"));
                                                markerOption.icon(BitmapDescriptorFactory.fromBitmap(createDrawableFromView(HomeActivity.this, marker)));
//                                                        mMarkersHashMapGoogle.put(currentMarker, vehiclejson);
//                                                        mGoogleMarkerList.add(currentMarker);
                                                currentMarker1 = map.addMarker(markerOption);
                                                mMarkersHashMapGoogle.put(currentMarker1, vehiclejson);
                                                mGoogleMarkerList.add(currentMarker1);
                                                child.add(vehiclejson.getString("vehicleId"));
                                            }

                                        } else if (mSelectedVehicleType.equalsIgnoreCase("I")) {
                                            if (vehiclejson.getString("position").equalsIgnoreCase("S")) {
                                                //  System.out.println("The idle vehicle name is :::" + vehiclejson.getString("vehicleId"));
//                                                MarkerOptions markerOption = new MarkerOptions().position(new LatLng(Double.parseDouble(vehiclejson.getString("latitude")), Double.parseDouble(vehiclejson.getString("longitude"))));
//                                                markerOption.title(vehiclejson.getString("vehicleId"));
//                                                markerOption.icon(BitmapDescriptorFactory.fromBitmap(createDrawableFromView(HomeActivity.this, marker)));
////                                                markerOption.icon(BitmapDescriptorFactory.fromBitmap(icon));
////                                                markerOption.anchor(0.5f, 1);
//                                                com.google.android.gms.maps.model.Marker currentMarker = map.addMarker(markerOption);
////                                                currentMarker.showInfoWindow();
//                                                mMarkersHashMapGoogle.put(currentMarker, vehiclejson);
//                                                mGoogleMarkerList.add(currentMarker);
                                                markerOption = new MarkerOptions().position(new LatLng(Double.parseDouble(vehiclejson.getString("latitude")), Double.parseDouble(vehiclejson.getString("longitude"))));
                                                markerOption.title(vehiclejson.getString("vehicleId"));
                                                markerOption.icon(BitmapDescriptorFactory.fromBitmap(createDrawableFromView(HomeActivity.this, marker)));
                                                //  mMarkersHashMapGoogle.put(currentMarker, vehiclejson);
                                                // mGoogleMarkerList.add(currentMarker);
                                                // currentMarker2 = map.addMarker(markerOption);

                                                currentMarker2 = map.addMarker(markerOption);
                                                mMarkersHashMapGoogle.put(currentMarker2, vehiclejson);
                                                mGoogleMarkerList.add(currentMarker2);

                                                child.add(vehiclejson.getString("vehicleId"));
                                            }
                                        } else if (mSelectedVehicleType.equalsIgnoreCase("No")) {
                                            if (vehiclejson.getString("position").equalsIgnoreCase("U")) {
                                                // System.out.println("The no data vehicle name is :::" + vehiclejson.getString("vehicleId"));
                                                markerOption = new MarkerOptions().position(new LatLng(Double.parseDouble(vehiclejson.getString("latitude")), Double.parseDouble(vehiclejson.getString("longitude"))));
                                                markerOption.title(vehiclejson.getString("vehicleId"));
                                                markerOption.icon(BitmapDescriptorFactory.fromBitmap(createDrawableFromView(HomeActivity.this, marker)));
                                                // mMarkersHashMapGoogle.put(currentMarker, vehiclejson);
                                                // mGoogleMarkerList.add(currentMarker);
                                                //   currentMarker3 = map.addMarker(markerOption);


                                                currentMarker3 = map.addMarker(markerOption);
                                                mMarkersHashMapGoogle.put(currentMarker3, vehiclejson);
                                                mGoogleMarkerList.add(currentMarker3);

                                                child.add(vehiclejson.getString("vehicleId"));
                                            }
                                        }

//                                    MarkerOptions markerOption = new MarkerOptions().position(new LatLng(Double.parseDouble(vehiclejson.getString("latitude")), Double.parseDouble(vehiclejson.getString("longitude"))));
//                                    markerOption.title(vehiclejson.getString("vehicleId"));
//                                    markerOption.icon(BitmapDescriptorFactory.fromBitmap(createDrawableFromView(HomeActivity.this, marker)));
//                                    Marker currentMarker = map.addMarker(markerOption);
//                                    mMarkersHashMap.put(currentMarker, vehiclejson);
//                                    mMarkerList.add(currentMarker);
                                    } catch (JSONException e1) {
                                        e1.printStackTrace();
                                    }
                                }


                            } catch (Exception e) {
                                // TODO Auto-generated catch block
                                e.printStackTrace();
                            }
                        }

                    }
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }


    private class GetAddressTask extends AsyncTask<Double, String, String> {
        TextView currenta_adddress_view;
        double getlat, getlng;
        double address_type;

        public GetAddressTask(TextView txtview) {
            super();
            currenta_adddress_view = txtview;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        /**
         * Get a Geocoder instance, get the latitude and longitude
         * look up the address, and return it
         *
         * @return A string containing the address of the current
         * location, or an empty string if no address can be found,
         * or an error message
         * @params params One or more Location objects
         */
        @Override
        protected String doInBackground(Double... params) {
            getlat = params[0];
            getlng = params[1];
            address_type = params[2];
            Geocoder geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());
            List<Address> addresses = null;
            try {
                addresses = geocoder.getFromLocation(params[0], params[1], 1);
            } catch (IOException e1) {
                e1.printStackTrace();
            } catch (IllegalArgumentException e2) {
                // Error message to post in the log
                String errorString = "Illegal arguments " +
                        Double.toString(params[0]) +
                        " , " +
                        Double.toString(params[1]) +
                        " passed to address service";
                e2.printStackTrace();
                return errorString;
            } catch (NullPointerException np) {
                // TODO Auto-generated catch block
                np.printStackTrace();
            }
            // If the reverse geocode returned an address
            if (addresses != null && addresses.size() > 0) {
                // Get the first address
                Address address = addresses.get(0);
                /*
                 * Format the first line of address (if available),
                 * city, and country name.
                 */
                String addressText = null;
                StringBuffer addr = new StringBuffer();
                for (int i = 0; i < address.getMaxAddressLineIndex(); i++) {
                    addressText = address.getAddressLine(i);
                    addr.append(addressText + ",");
                }
                // Return the text
                return addr.toString();
            } else {
                return "No address found";
            }
        }

        protected void onProgressUpdate(String... progress) {
        }

        protected void onPostExecute(String result) {
            // sourceedit.setText(result);
            if (result != null && !result.isEmpty()) {
                if (!result.equals("IO Exception trying to get address") || !result.equals("No address found")) {
                    /***
                     * 1.0 - vehicles current address when a vehicle is clicked
                     * 2.0 - vehicles origin address in history selection
                     * 3.0 - vehicles destination address in history selection
                     * 6.0 - vehicles current address when pause button is clicked in history selection
                     */
                    if (address_type == 1.0) {
                        SharedPreferences.Editor editor = sp.edit();
                        editor.putString("address", result);
                        editor.commit();
                        currenta_adddress_view.setText(result.equals("null") ? "No Data" : result);
                    } else if (address_type == 2.0) {
                        SharedPreferences.Editor editor = sp.edit();
                        editor.putString("vehicle_origin_address", result);
                        editor.commit();
                        // history_start_location_value.setText(result.equals("null") ? "No Data" : result);
                    } else if (address_type == 3.0) {
                        SharedPreferences.Editor editor = sp.edit();
                        editor.putString("vehicle_destination_address", result);
                        editor.commit();
                        // history_end_location_value.setText(result.equals("null") ? "No Data" : result);
                    } else if (address_type == 4.0) {
                        SharedPreferences.Editor editor = sp.edit();
                        editor.putString("current_vehicle_address", result);
                        editor.commit();
                        // id_curr_vehicle_address_value.setText(result.equals("null") ? "No Data" : result);
                    } else if (address_type == 5.0) {
                        currenta_adddress_view.setText(result.equals("null") ? "No Data" : result);
                    } else if (address_type == 6.0) {
                        currenta_adddress_view.setText(result.equals("null") ? "No Data" : result);
                    }
                }
            } else {
                Toast empty_fav = Toast.makeText(getApplicationContext(), getResources().getString(R.string.please_try_again), Toast.LENGTH_LONG);
                empty_fav.show();
            }
        }
    }

    /**
     * Map Related code
     */
    private boolean showVehicleInfoDialog(final JSONObject getcurrentmarker, boolean isFromList) {
        if (isFromList) {
            marker_info_dialog = new Dialog(this, android.R.style.Theme_Light_NoTitleBar_Fullscreen);
            marker_info_dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        } else {
            marker_info_dialog = new Dialog(this);
        }
        marker_info_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        marker_info_dialog.setContentView(R.layout.custominfolayout);
        marker_info_dialog.setCancelable(false);
        ImageView id_back_icon = (ImageView) marker_info_dialog.findViewById(R.id.id_back_icon);
        id_vehicleid = (TextView) marker_info_dialog.findViewById(R.id.id_vehicleid);
        id_vehicle_info_icon = (ImageView) marker_info_dialog.findViewById(R.id.id_vehicle_info_icon);
        Button id_vehicle_track = (Button) marker_info_dialog.findViewById(R.id.id_vehicle_track);
        Button vehicle_history = (Button) marker_info_dialog.findViewById(R.id.id_vehicle_history_btn);
        Button id_close = (Button) marker_info_dialog.findViewById(R.id.id_close);
        id_view_address = (TextView) marker_info_dialog.findViewById(R.id.id_view_address);
        id_view_speed = (TextView) marker_info_dialog.findViewById(R.id.id_view_speed);
        id_view_lastseentime = (TextView) marker_info_dialog.findViewById(R.id.id_view_lastseentime);
        id_speed_limit_speed = (TextView) marker_info_dialog.findViewById(R.id.id_view_spped_lastactive);
        id_ignition_speed = (TextView) marker_info_dialog.findViewById(R.id.id_ignition_speed);
        TextView id_lastseentime_labelid = (TextView) marker_info_dialog.findViewById(R.id.id_lastseentime_labelid);
        TextView id_speed_labelid = (TextView) marker_info_dialog.findViewById(R.id.id_speed_labelid);
        TextView id_ignition_labelid = (TextView) marker_info_dialog.findViewById(R.id.id_ignition_labelid);
        TextView id_address_text_labelid = (TextView) marker_info_dialog.findViewById(R.id.id_address_text_labelid);
        TextView id_lastseentime_label_separator = (TextView) marker_info_dialog.findViewById(R.id.id_lastseentime_label_separator);
        TextView id_speed_labelid_separator = (TextView) marker_info_dialog.findViewById(R.id.id_speed_labelid_separator);
        id_time_label_layout = (RelativeLayout) marker_info_dialog.findViewById(R.id.id_time_label_layout);
        id_time_labelid = (TextView) marker_info_dialog.findViewById(R.id.id_time_labelid);
        id_time = (TextView) marker_info_dialog.findViewById(R.id.id_time);
        // ImageView id_vehicle_next = (ImageView)marker_info_dialog.findViewById(R.id.id_vehicle_next);
        // ImageView id_vehicle_previous = (ImageView)marker_info_dialog.findViewById(R.id.id_vehicle_previous);
        final ImageView id_view_next = (ImageView) marker_info_dialog.findViewById(R.id.id_vehicle_next);
        final ImageView id_view_previous = (ImageView) marker_info_dialog.findViewById(R.id.id_vehicle_previous);

        id_txtview_total_km = (TextView) marker_info_dialog.findViewById(R.id.id_txtview_total_km);
        TextView id_total_km_labelid = (TextView) marker_info_dialog.findViewById(R.id.id_total_km_labelid);

        id_time_labelid.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        id_time.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        id_lastseentime_label_separator.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        id_speed_labelid_separator.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        id_lastseentime_labelid.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        id_speed_labelid.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        id_ignition_labelid.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        id_address_text_labelid.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        id_vehicleid.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        id_vehicle_track.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        vehicle_history.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        id_view_address.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        id_view_speed.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        id_total_km_labelid.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        id_txtview_total_km.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        id_view_lastseentime.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        id_speed_limit_speed.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        id_ignition_speed.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        try {
            double latitude = Double.parseDouble(getcurrentmarker.getString("latitude"));
            double longitude = Double.parseDouble(getcurrentmarker.getString("longitude"));
//            new GetAddressTask(id_view_address).execute(latitude, longitude, 1.0);

            id_view_address.setText(getcurrentmarker.getString("address"));

            selected_vehicleid = getcurrentmarker.getString("vehicleId");
            if (getcurrentmarker.getString("vehicleType").equals("Bus")) {
                //R  id_vehicle_info_icon.setImageResource(R.drawable.bus_east);


                if (getcurrentmarker.getString("isOverSpeed").equalsIgnoreCase("Y")) {
                    id_vehicle_info_icon.setImageResource(R.drawable.bus_east);
                    id_vehicle_info_icon.setColorFilter(getResources().getColor(R.color.red));
                } else if (getcurrentmarker.getString("insideGeoFence").equalsIgnoreCase("Y")) {
                    id_vehicle_info_icon.setImageResource(R.drawable.bus_east);
                    id_vehicle_info_icon.setColorFilter(getResources().getColor(R.color.red));
                } else if (getcurrentmarker.getString("position").equalsIgnoreCase("M")) {
                    id_vehicle_info_icon.setImageResource(R.drawable.bus_east);
                    id_vehicle_info_icon.setColorFilter(getResources().getColor(R.color.green));
                } else if (getcurrentmarker.getString("position").equalsIgnoreCase("P")) {
                    id_vehicle_info_icon.setImageResource(R.drawable.bus_east);
                    id_vehicle_info_icon.setColorFilter(getResources().getColor(R.color.black));
                } else if (getcurrentmarker.getString("position").equalsIgnoreCase("S")) {
                    id_vehicle_info_icon.setImageResource(R.drawable.bus_east);
                    id_vehicle_info_icon.setColorFilter(getResources().getColor(R.color.yellow));
                } else if (getcurrentmarker.getString("position").equalsIgnoreCase("N")) {
                    id_vehicle_info_icon.setImageResource(R.drawable.bus_east);
                    id_vehicle_info_icon.setColorFilter(getResources().getColor(R.color.grey));
                } else if (getcurrentmarker.getString("position").equalsIgnoreCase("U")) {
                    id_vehicle_info_icon.setImageResource(R.drawable.bus_east);
                    id_vehicle_info_icon.setColorFilter(getResources().getColor(R.color.red));
                }
                //  id_histroy_vehicleid.setText(getcurrentmarker.getString("vehicleId"));
                // id_history_vehicle_info_icon.setImageResource(R.drawable.bus_east);
            } else if (getcurrentmarker.getString("vehicleType").equals("Car")) {
                // id_histroy_vehicleid.setText(getcurrentmarker.getString("vehicleId"));
                // id_history_vehicle_info_icon.setImageResource(R.drawable.car_east);
                // id_vehicle_info_icon.setImageResource(R.drawable.car_east);


                if (getcurrentmarker.getString("isOverSpeed").equalsIgnoreCase("Y")) {
                    id_vehicle_info_icon.setImageResource(R.drawable.car_east);
                    id_vehicle_info_icon.setColorFilter(getResources().getColor(R.color.red));
                } else if (getcurrentmarker.getString("insideGeoFence").equalsIgnoreCase("Y")) {
                    id_vehicle_info_icon.setImageResource(R.drawable.car_east);
                    id_vehicle_info_icon.setColorFilter(getResources().getColor(R.color.red));
                } else if (getcurrentmarker.getString("position").equalsIgnoreCase("M")) {
                    id_vehicle_info_icon.setImageResource(R.drawable.car_east);
                    id_vehicle_info_icon.setColorFilter(getResources().getColor(R.color.green));
                } else if (getcurrentmarker.getString("position").equalsIgnoreCase("P")) {
                    id_vehicle_info_icon.setImageResource(R.drawable.car_east);
                    id_vehicle_info_icon.setColorFilter(getResources().getColor(R.color.black));
                } else if (getcurrentmarker.getString("position").equalsIgnoreCase("S")) {
                    id_vehicle_info_icon.setImageResource(R.drawable.car_east);
                    id_vehicle_info_icon.setColorFilter(getResources().getColor(R.color.yellow));
                } else if (getcurrentmarker.getString("position").equalsIgnoreCase("N")) {
                    id_vehicle_info_icon.setImageResource(R.drawable.car_east);
                    id_vehicle_info_icon.setColorFilter(getResources().getColor(R.color.grey));
                } else if (getcurrentmarker.getString("position").equalsIgnoreCase("U")) {
                    id_vehicle_info_icon.setImageResource(R.drawable.car_east);
                    id_vehicle_info_icon.setColorFilter(getResources().getColor(R.color.red));
                }
            } else if (getcurrentmarker.getString("vehicleType").equals("Truck")) {
                //  id_histroy_vehicleid.setText(getcurrentmarker.getString("vehicleId"));
                //  id_history_vehicle_info_icon.setImageResource(R.drawable.truck_east);
                //id_vehicle_info_icon.setImageResource(R.drawable.truck_east);


                if (getcurrentmarker.getString("isOverSpeed").equalsIgnoreCase("Y")) {
                    id_vehicle_info_icon.setImageResource(R.drawable.truck_east);
                    id_vehicle_info_icon.setColorFilter(getResources().getColor(R.color.red));
                } else if (getcurrentmarker.getString("insideGeoFence").equalsIgnoreCase("Y")) {
                    id_vehicle_info_icon.setImageResource(R.drawable.truck_east);
                    id_vehicle_info_icon.setColorFilter(getResources().getColor(R.color.red));
                } else if (getcurrentmarker.getString("position").equalsIgnoreCase("M")) {
                    id_vehicle_info_icon.setImageResource(R.drawable.truck_east);
                    id_vehicle_info_icon.setColorFilter(getResources().getColor(R.color.green));
                } else if (getcurrentmarker.getString("position").equalsIgnoreCase("P")) {
                    id_vehicle_info_icon.setImageResource(R.drawable.truck_east);
                    id_vehicle_info_icon.setColorFilter(getResources().getColor(R.color.black));
                } else if (getcurrentmarker.getString("position").equalsIgnoreCase("S")) {
                    id_vehicle_info_icon.setImageResource(R.drawable.truck_east);
                    id_vehicle_info_icon.setColorFilter(getResources().getColor(R.color.yellow));
                } else if (getcurrentmarker.getString("position").equalsIgnoreCase("N")) {
                    id_vehicle_info_icon.setImageResource(R.drawable.truck_east);
                    id_vehicle_info_icon.setColorFilter(getResources().getColor(R.color.grey));
                } else if (getcurrentmarker.getString("position").equalsIgnoreCase("U")) {
                    id_vehicle_info_icon.setImageResource(R.drawable.truck_east);
                    id_vehicle_info_icon.setColorFilter(getResources().getColor(R.color.red));
                }
            } else if (getcurrentmarker.getString("vehicleType").equals("Cycle")) {
                //  id_histroy_vehicleid.setText(getcurrentmarker.getString("vehicleId"));
                //  id_history_vehicle_info_icon.setImageResource(R.drawable.cycle_east);
                // id_vehicle_info_icon.setImageResource(R.drawable.cycle_east);
                if (getcurrentmarker.getString("isOverSpeed").equalsIgnoreCase("Y")) {
                    id_vehicle_info_icon.setImageResource(R.drawable.cycle_east);
                    id_vehicle_info_icon.setColorFilter(getResources().getColor(R.color.red));
                } else if (getcurrentmarker.getString("insideGeoFence").equalsIgnoreCase("Y")) {
                    id_vehicle_info_icon.setImageResource(R.drawable.cycle_east);
                    id_vehicle_info_icon.setColorFilter(getResources().getColor(R.color.red));
                } else if (getcurrentmarker.getString("position").equalsIgnoreCase("M")) {
                    id_vehicle_info_icon.setImageResource(R.drawable.cycle_east);
                    id_vehicle_info_icon.setColorFilter(getResources().getColor(R.color.green));
                } else if (getcurrentmarker.getString("position").equalsIgnoreCase("P")) {
                    id_vehicle_info_icon.setImageResource(R.drawable.cycle_east);
                    id_vehicle_info_icon.setColorFilter(getResources().getColor(R.color.black));
                } else if (getcurrentmarker.getString("position").equalsIgnoreCase("S")) {
                    id_vehicle_info_icon.setImageResource(R.drawable.cycle_east);
                    id_vehicle_info_icon.setColorFilter(getResources().getColor(R.color.yellow));
                } else if (getcurrentmarker.getString("position").equalsIgnoreCase("N")) {
                    id_vehicle_info_icon.setImageResource(R.drawable.cycle_east);
                    id_vehicle_info_icon.setColorFilter(getResources().getColor(R.color.grey));
                } else if (getcurrentmarker.getString("position").equalsIgnoreCase("U")) {
                    id_vehicle_info_icon.setImageResource(R.drawable.cycle_east);
                    id_vehicle_info_icon.setColorFilter(getResources().getColor(R.color.red));
                }

            } else if (getcurrentmarker.getString("vehicleType").equalsIgnoreCase("Jcb")) {
                //  id_histroy_vehicleid.setText(getcurrentmarker.getString("vehicleId"));
                //  id_history_vehicle_info_icon.setImageResource(R.drawable.cycle_east);

                //  id_vehicle_info_icon.setImageResource(R.drawable.jcb_icon);


                if (getcurrentmarker.getString("isOverSpeed").equalsIgnoreCase("Y")) {
                    id_vehicle_info_icon.setImageResource(R.drawable.jcb_icon);
                    id_vehicle_info_icon.setColorFilter(getResources().getColor(R.color.red));
                } else if (getcurrentmarker.getString("insideGeoFence").equalsIgnoreCase("Y")) {
                    id_vehicle_info_icon.setImageResource(R.drawable.jcb_icon);
                    id_vehicle_info_icon.setColorFilter(getResources().getColor(R.color.red));
                } else if (getcurrentmarker.getString("position").equalsIgnoreCase("M")) {
                    id_vehicle_info_icon.setImageResource(R.drawable.jcb_icon);
                    id_vehicle_info_icon.setColorFilter(getResources().getColor(R.color.green));
                } else if (getcurrentmarker.getString("position").equalsIgnoreCase("P")) {
                    id_vehicle_info_icon.setImageResource(R.drawable.jcb_icon);
                    id_vehicle_info_icon.setColorFilter(getResources().getColor(R.color.black));
                } else if (getcurrentmarker.getString("position").equalsIgnoreCase("S")) {
                    id_vehicle_info_icon.setImageResource(R.drawable.jcb_icon);
                    id_vehicle_info_icon.setColorFilter(getResources().getColor(R.color.yellow));
                } else if (getcurrentmarker.getString("position").equalsIgnoreCase("N")) {
                    id_vehicle_info_icon.setImageResource(R.drawable.jcb_icon);
                    id_vehicle_info_icon.setColorFilter(getResources().getColor(R.color.grey));
                } else if (getcurrentmarker.getString("position").equalsIgnoreCase("U")) {
                    id_vehicle_info_icon.setImageResource(R.drawable.jcb_icon);
                    id_vehicle_info_icon.setColorFilter(getResources().getColor(R.color.red));
                }
            } else if (getcurrentmarker.getString("vehicleType").equalsIgnoreCase("Bike")) {
                //  id_histroy_vehicleid.setText(getcurrentmarker.getString("vehicleId"));
                //  id_history_vehicle_info_icon.setImageResource(R.drawable.cycle_east);
                // id_vehicle_info_icon.setImageResource(R.drawable.vehicle_bike);


                if (getcurrentmarker.getString("isOverSpeed").equalsIgnoreCase("Y")) {
                    id_vehicle_info_icon.setImageResource(R.drawable.vehicle_bike);
                    id_vehicle_info_icon.setColorFilter(getResources().getColor(R.color.red));
                } else if (getcurrentmarker.getString("insideGeoFence").equalsIgnoreCase("Y")) {
                    id_vehicle_info_icon.setImageResource(R.drawable.vehicle_bike);
                    id_vehicle_info_icon.setColorFilter(getResources().getColor(R.color.red));
                } else if (getcurrentmarker.getString("position").equalsIgnoreCase("M")) {
                    id_vehicle_info_icon.setImageResource(R.drawable.vehicle_bike);
                    id_vehicle_info_icon.setColorFilter(getResources().getColor(R.color.green));
                } else if (getcurrentmarker.getString("position").equalsIgnoreCase("P")) {
                    id_vehicle_info_icon.setImageResource(R.drawable.vehicle_bike);
                    id_vehicle_info_icon.setColorFilter(getResources().getColor(R.color.black));
                } else if (getcurrentmarker.getString("position").equalsIgnoreCase("S")) {
                    id_vehicle_info_icon.setImageResource(R.drawable.vehicle_bike);
                    id_vehicle_info_icon.setColorFilter(getResources().getColor(R.color.yellow));
                } else if (getcurrentmarker.getString("position").equalsIgnoreCase("N")) {
                    id_vehicle_info_icon.setImageResource(R.drawable.vehicle_bike);
                    id_vehicle_info_icon.setColorFilter(getResources().getColor(R.color.grey));
                } else if (getcurrentmarker.getString("position").equalsIgnoreCase("U")) {
                    id_vehicle_info_icon.setImageResource(R.drawable.vehicle_bike);
                    id_vehicle_info_icon.setColorFilter(getResources().getColor(R.color.red));
                }
            } else if (getcurrentmarker.getString("vehicleType").equalsIgnoreCase("Ambulance")) {
                //  id_histroy_vehicleid.setText(getcurrentmarker.getString("vehicleId"));
                //  id_history_vehicle_info_icon.setImageResource(R.drawable.cycle_east);
                // id_vehicle_info_icon.setImageResource(R.drawable.vehicle_bike);


                if (getcurrentmarker.getString("isOverSpeed").equalsIgnoreCase("Y")) {
                    id_vehicle_info_icon.setImageResource(R.drawable.ambulance);
                    id_vehicle_info_icon.setColorFilter(getResources().getColor(R.color.red));
                } else if (getcurrentmarker.getString("insideGeoFence").equalsIgnoreCase("Y")) {
                    id_vehicle_info_icon.setImageResource(R.drawable.ambulance);
                    id_vehicle_info_icon.setColorFilter(getResources().getColor(R.color.red));
                } else if (getcurrentmarker.getString("position").equalsIgnoreCase("M")) {
                    id_vehicle_info_icon.setImageResource(R.drawable.ambulance);
                    id_vehicle_info_icon.setColorFilter(getResources().getColor(R.color.green));
                } else if (getcurrentmarker.getString("position").equalsIgnoreCase("P")) {
                    id_vehicle_info_icon.setImageResource(R.drawable.ambulance);
                    id_vehicle_info_icon.setColorFilter(getResources().getColor(R.color.black));
                } else if (getcurrentmarker.getString("position").equalsIgnoreCase("S")) {
                    id_vehicle_info_icon.setImageResource(R.drawable.ambulance);
                    id_vehicle_info_icon.setColorFilter(getResources().getColor(R.color.yellow));
                } else if (getcurrentmarker.getString("position").equalsIgnoreCase("N")) {
                    id_vehicle_info_icon.setImageResource(R.drawable.ambulance);
                    id_vehicle_info_icon.setColorFilter(getResources().getColor(R.color.grey));
                } else if (getcurrentmarker.getString("position").equalsIgnoreCase("U")) {
                    id_vehicle_info_icon.setImageResource(R.drawable.ambulance);
                    id_vehicle_info_icon.setColorFilter(getResources().getColor(R.color.red));
                }
            }
//            if (getcurrentmarker.getString("vehicleType").equals("Bus")) {
//                id_vehicle_info_icon.setImageResource(R.drawable.bus_east);
//                //  id_histroy_vehicleid.setText(getcurrentmarker.getString("vehicleId"));
//                // id_history_vehicle_info_icon.setImageResource(R.drawable.bus_east);
//            } else if (getcurrentmarker.getString("vehicleType").equals("Car")) {
//                // id_histroy_vehicleid.setText(getcurrentmarker.getString("vehicleId"));
//                // id_history_vehicle_info_icon.setImageResource(R.drawable.car_east);
//                id_vehicle_info_icon.setImageResource(R.drawable.car_east);
//            } else if (getcurrentmarker.getString("vehicleType").equals("Truck")) {
//                //  id_histroy_vehicleid.setText(getcurrentmarker.getString("vehicleId"));
//                //  id_history_vehicle_info_icon.setImageResource(R.drawable.truck_east);
//                id_vehicle_info_icon.setImageResource(R.drawable.truck_east);
//            } else if (getcurrentmarker.getString("vehicleType").equals("Cycle")) {
//                //  id_histroy_vehicleid.setText(getcurrentmarker.getString("vehicleId"));
//                //  id_history_vehicle_info_icon.setImageResource(R.drawable.cycle_east);
//                id_vehicle_info_icon.setImageResource(R.drawable.cycle_east);
//            } else if (getcurrentmarker.getString("vehicleType").equalsIgnoreCase("Jcb")) {
//                //  id_histroy_vehicleid.setText(getcurrentmarker.getString("vehicleId"));
//                //  id_history_vehicle_info_icon.setImageResource(R.drawable.cycle_east);
//
//                id_vehicle_info_icon.setImageResource(R.drawable.jcb_icon);
//            } else if (getcurrentmarker.getString("vehicleType").equalsIgnoreCase("Bike")) {
//                //  id_histroy_vehicleid.setText(getcurrentmarker.getString("vehicleId"));
//                //  id_history_vehicle_info_icon.setImageResource(R.drawable.cycle_east);
//                id_vehicle_info_icon.setImageResource(R.drawable.vehicle_bike);
//            }

            id_vehicleid.setText(getcurrentmarker.getString("shortName"));


//            System.out.println("Hi vehicle name " + getcurrentmarker.getString("shortName") + " lat " + latitude + " longi " + longitude);
            try {
                map_marker_vechile_position = child.indexOf(getcurrentmarker.getString("vehicleId"));
                Log.d("markerpos", "" + map_marker_vechile_position);
                if (map_marker_vechile_position == -1) {
                    child = (ArrayList<String>) childelements.get(0);
                    map_marker_vechile_position = child.indexOf(getcurrentmarker.getString("vehicleId"));
                }
            } catch (Exception e) {
                vehicle = (ArrayList<String>) childelements.get(0);
                map_marker_vechile_position = vehicle.indexOf(getcurrentmarker.getString("vehicleId"));
            }
            position = map_marker_vechile_position;
            id_view_speed.setText(Integer.toString(getcurrentmarker.getInt("speed")) + " "+getResources().getString(R.string.kms_per_hr));
            id_txtview_total_km.setText(Double.toString(getcurrentmarker.getDouble("distanceCovered")) + " "+getResources().getString(R.string.kms));
            id_view_lastseentime.setText(getcurrentmarker.getString("lastSeen"));
            id_speed_limit_speed.setText(""+getResources().getString(R.string.speed_limit) + Integer.toString(getcurrentmarker.getInt("overSpeedLimit")) + " "+getResources().getString(R.string.kms_per_hr));
            id_ignition_speed.setText(getcurrentmarker.getString("ignitionStatus"));
            if (getcurrentmarker.getString("position").equalsIgnoreCase("P")) {
                id_time_labelid.setText(""+getResources().getString(R.string.parked_time));
                id_time.setText(cons.getVehicleTimeNew(getcurrentmarker.getString("parkedTime")));
            } else if (getcurrentmarker.getString("position").equalsIgnoreCase("M")) {
                id_time_labelid.setText(""+getResources().getString(R.string.moving_time));
                id_time.setText(cons.getVehicleTimeNew(getcurrentmarker.getString("movingTime")));
            } else if (getcurrentmarker.getString("position").equalsIgnoreCase("U")) {
                id_time_labelid.setText(""+getResources().getString(R.string.no_data_time));
                id_time.setText(cons.getVehicleTimeNew(getcurrentmarker.getString("noDataTime")));
            } else if (getcurrentmarker.getString("position").equalsIgnoreCase("S")) {
                id_time_labelid.setText(""+getResources().getString(R.string.idle_time));
                id_time.setText(cons.getVehicleTimeNew(getcurrentmarker.getString("idleTime")));
            } else {
                id_time_label_layout.setVisibility(View.GONE);
            }
            SharedPreferences.Editor editor = sp.edit();
            editor.putString("current_vehicleID", getcurrentmarker.getString("vehicleId"));
            editor.commit();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        try {
            if (child.size() == 1) {
                id_view_next.setVisibility(View.GONE);
                id_view_previous.setVisibility(View.GONE);
                //  id_view_next.setVisibility(View.GONE);
                //  id_view_previous.setVisibility(View.GONE);
            } else {
                if (map_marker_vechile_position == child.size() - 1) {
                    id_view_next.setVisibility(View.INVISIBLE);
                    id_view_previous.setVisibility(View.VISIBLE);
                    // id_next_vehicle.setVisibility(View.INVISIBLE);
                    //   id_previous_vehicle.setVisibility(View.VISIBLE);
                } else if (map_marker_vechile_position == 0) {
                    //  id_next_vehicle.setVisibility(View.VISIBLE);
                    //  id_previous_vehicle.setVisibility(View.INVISIBLE);
                    id_view_next.setVisibility(View.VISIBLE);
                    id_view_previous.setVisibility(View.INVISIBLE);
                } else {
                    id_view_next.setVisibility(View.VISIBLE);
                    id_view_previous.setVisibility(View.VISIBLE);
                    //  id_next_vehicle.setVisibility(View.VISIBLE);
                    //   id_previous_vehicle.setVisibility(View.VISIBLE);
                }
            }
        } catch (Exception e) {
            if (vehicle.size() == 1) {
                // id_next_vehicle.setVisibility(View.GONE);
                //  id_previous_vehicle.setVisibility(View.GONE);
                id_view_next.setVisibility(View.GONE);
                id_view_previous.setVisibility(View.GONE);
            } else {
                if (map_marker_vechile_position == vehicle.size() - 1) {
                    //  id_next_vehicle.setVisibility(View.INVISIBLE);
                    //  id_previous_vehicle.setVisibility(View.VISIBLE);
                    id_view_next.setVisibility(View.INVISIBLE);
                    id_view_previous.setVisibility(View.VISIBLE);
                } else if (map_marker_vechile_position == 0) {
                    // id_next_vehicle.setVisibility(View.VISIBLE);
                    //  id_previous_vehicle.setVisibility(View.INVISIBLE);
                    id_view_next.setVisibility(View.VISIBLE);
                    id_view_previous.setVisibility(View.INVISIBLE);
                } else {
                    // id_next_vehicle.setVisibility(View.VISIBLE);
                    //  id_previous_vehicle.setVisibility(View.VISIBLE);
                    id_view_next.setVisibility(View.VISIBLE);
                    id_view_previous.setVisibility(View.VISIBLE);
                }
            }
        }
        id_view_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                map_marker_vechile_position += 1;
                id_view_previous.setVisibility(View.VISIBLE);
                //  marker_info_dialog.hide();
                try {
//                    System.out.println("Hi vehicle name " + getcurrentmarker.getString("shortName") + " next  clicked  " + map_marker_vechile_position + " size " + child.size());
                    if (map_marker_vechile_position == child.size() - 1) {
                        id_view_next.setVisibility(View.GONE);
                    }

//                    System.out.println("Marker " + mMarkerList.get(map_marker_vechile_position));

                    if (isOsmEnabled) {
                        setDataOsm(mOSMMarkerList.get(map_marker_vechile_position));
                    } else {
                        setDataGoogle(mGoogleMarkerList.get(map_marker_vechile_position));
                    }

//                    setData(mMarkerList.get(map_marker_vechile_position));
                } catch (Exception e) {
                    e.printStackTrace();
                    if (map_marker_vechile_position == vehicle.size() - 1) {
                        id_view_next.setVisibility(View.GONE);
                    }

                    if (isOsmEnabled) {
                        if (mOSMMarkerList.size() > map_marker_vechile_position) {
                            setDataOsm(mOSMMarkerList.get(map_marker_vechile_position));
                        }
                    } else {
                        if (mGoogleMarkerList.size() > map_marker_vechile_position) {
                            setDataGoogle(mGoogleMarkerList.get(map_marker_vechile_position));
                        }
                    }

//                    if (mMarkerList.size() > map_marker_vechile_position) {
//                        setData(mMarkerList.get(map_marker_vechile_position));
//                    }
                }
            }
        });
        id_view_previous.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                map_marker_vechile_position -= 1;
                //  marker_info_dialog.hide();
                id_view_next.setVisibility(View.VISIBLE);
                try {

//                    System.out.println("Hi vehicle name " + getcurrentmarker.getString("shortName") + " previous clicked  " + map_marker_vechile_position);

                    if (map_marker_vechile_position == 0) {
                        id_view_previous.setVisibility(View.GONE);
                    }
//                    setData(mMarkerList.get(map_marker_vechile_position));
                    if (isOsmEnabled) {
                        setDataOsm(mOSMMarkerList.get(map_marker_vechile_position));
                    } else {
                        setDataGoogle(mGoogleMarkerList.get(map_marker_vechile_position));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    if (map_marker_vechile_position == 0) {
                        id_view_previous.setVisibility(View.GONE);
                    }
//                    if (mMarkerList.size() > map_marker_vechile_position) {
//                        setData(mMarkerList.get(map_marker_vechile_position));
//                    }
                    if (isOsmEnabled) {
                        if (mOSMMarkerList.size() > map_marker_vechile_position) {
                            setDataOsm(mOSMMarkerList.get(map_marker_vechile_position));
                        }
                    } else {
                        if (mGoogleMarkerList.size() > map_marker_vechile_position) {
                            setDataGoogle(mGoogleMarkerList.get(map_marker_vechile_position));
                        }
                    }

                }
                //
            }
        });
        id_vehicle_track.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                marker_info_dialog.hide();
//                Constant.SELECTED_VEHICLE_ID = selected_vehicleid;
//                Constant.SELECTED_USER_ID = mSelectedUserID;
//                startActivity(new Intent(HomeActivity.this, VehicleTrackingActivity.class));
//                finish();
                marker_info_dialog.hide();
                Constant.SELECTED_VEHICLE_ID = selected_vehicleid;
                Constant.SELECTED_USER_ID = mSelectedUserID;
                if (isOsmEnabled){
                    startActivity(new Intent(HomeActivity.this, VehicleTrackingActivity.class));
                    finish();
                } else {
                    startActivity(new Intent(HomeActivity.this, VehicleTrackingGoogleMapActivity.class));
                    finish();
                }
            }
        });
        vehicle_history.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                marker_info_dialog.hide();
//
//                Constant.SELECTED_VEHICLE_ID = selected_vehicleid;
//                Constant.SELECTED_USER_ID = mSelectedUserID;
//                startActivity(new Intent(HomeActivity.this, HistoryActivity.class));
//                finish();

                marker_info_dialog.hide();

                Constant.SELECTED_VEHICLE_ID = selected_vehicleid;
                Constant.SELECTED_USER_ID = mSelectedUserID;
                if (isOsmEnabled){
                    startActivity(new Intent(HomeActivity.this, HistoryNewActivity.class));
                    finish();
                } else {
                    startActivity(new Intent(HomeActivity.this, HistoryNewGoogleMapActivity.class));
                    finish();
                }

                }
        });
        id_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                marker_info_dialog.hide();
                id_view_next.setVisibility(View.GONE);
                id_view_previous.setVisibility(View.GONE);
                // header_layout.setVisibility(View.VISIBLE);
                // vehicle_info_layout.setVisibility(View.VISIBLE);
            }
        });
        id_back_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                marker_info_dialog.hide();
                id_view_next.setVisibility(View.GONE);
                id_view_previous.setVisibility(View.GONE);
                // header_layout.setVisibility(View.VISIBLE);
            }
        });
        if (isFromList) {
            vehicle_history.setVisibility(View.INVISIBLE);
            id_vehicle_track.setVisibility(View.INVISIBLE);
            id_back_icon.setVisibility(View.VISIBLE);
            id_close.setVisibility(View.GONE);
            id_view_next.setVisibility(View.INVISIBLE);
            id_view_previous.setVisibility(View.INVISIBLE);
        } else {
            id_back_icon.setVisibility(View.GONE);
            id_close.setVisibility(View.VISIBLE);
        }
        marker_info_dialog.show();
        return true;
    }

    public void setDataOsm(Marker mMarker) {

//        System.out.println("Hi setdata called " + mMarker.getTitle());

        JSONObject mJsonVehicleData = null, getcurrentmarker = null;
        if (mMarkersHashMapOSM != null
                && mMarkersHashMapOSM.size() > 0) {
            getcurrentmarker = mMarkersHashMapOSM.get(mMarker);


        }

        try {
//            double latitude = Double.parseDouble(getcurrentmarker.getString("latitude"));
//            double longitude = Double.parseDouble(getcurrentmarker.getString("longitude"));
//            new GetAddressTask(id_view_address).execute(latitude, longitude, 1.0);

            id_view_address.setText(getcurrentmarker.getString("address"));

            selected_vehicleid = getcurrentmarker.getString("vehicleId");
            if (getcurrentmarker.getString("vehicleType").equals("Bus")) {
                id_vehicle_info_icon.setImageResource(R.drawable.bus_east);
                //  id_histroy_vehicleid.setText(getcurrentmarker.getString("vehicleId"));
                // id_history_vehicle_info_icon.setImageResource(R.drawable.bus_east);
            } else if (getcurrentmarker.getString("vehicleType").equals("Car")) {
                // id_histroy_vehicleid.setText(getcurrentmarker.getString("vehicleId"));
                // id_history_vehicle_info_icon.setImageResource(R.drawable.car_east);
                id_vehicle_info_icon.setImageResource(R.drawable.car_east);
            } else if (getcurrentmarker.getString("vehicleType").equals("Truck")) {
                //  id_histroy_vehicleid.setText(getcurrentmarker.getString("vehicleId"));
                //  id_history_vehicle_info_icon.setImageResource(R.drawable.truck_east);
                id_vehicle_info_icon.setImageResource(R.drawable.truck_east);
            } else if (getcurrentmarker.getString("vehicleType").equals("Cycle")) {
                //  id_histroy_vehicleid.setText(getcurrentmarker.getString("vehicleId"));
                //  id_history_vehicle_info_icon.setImageResource(R.drawable.cycle_east);
                id_vehicle_info_icon.setImageResource(R.drawable.cycle_east);
            } else if (getcurrentmarker.getString("vehicleType").equalsIgnoreCase("Jcb")) {
                //  id_histroy_vehicleid.setText(getcurrentmarker.getString("vehicleId"));
                //  id_history_vehicle_info_icon.setImageResource(R.drawable.cycle_east);
                id_vehicle_info_icon.setImageResource(R.drawable.jcb_icon);
            } else if (getcurrentmarker.getString("vehicleType").equalsIgnoreCase("Bike")) {
                //  id_histroy_vehicleid.setText(getcurrentmarker.getString("vehicleId"));
                //  id_history_vehicle_info_icon.setImageResource(R.drawable.cycle_east);
                id_vehicle_info_icon.setImageResource(R.drawable.vehicle_bike);
            } else if (getcurrentmarker.getString("vehicleType").equalsIgnoreCase("Ambulance")) {
                //  id_histroy_vehicleid.setText(getcurrentmarker.getString("vehicleId"));
                //  id_history_vehicle_info_icon.setImageResource(R.drawable.cycle_east);
                id_vehicle_info_icon.setImageResource(R.drawable.ambulance);
            }
            id_vehicleid.setText(getcurrentmarker.getString("shortName"));

//            System.out.println("Hi vehicle name " + getcurrentmarker.getString("shortName") + " lat " + latitude + " longi " + longitude);
            try {
                map_marker_vechile_position = child.indexOf(getcurrentmarker.getString("vehicleId"));
                if (map_marker_vechile_position == -1) {
                    child = (ArrayList<String>) childelements.get(0);
                    map_marker_vechile_position = child.indexOf(getcurrentmarker.getString("vehicleId"));
                }
            } catch (Exception e) {
                vehicle = (ArrayList<String>) childelements.get(0);
                map_marker_vechile_position = vehicle.indexOf(getcurrentmarker.getString("vehicleId"));
            }
            position = map_marker_vechile_position;
            id_view_speed.setText(Integer.toString(getcurrentmarker.getInt("speed")) + " "+getResources().getString(R.string.kms_per_hr));

            id_txtview_total_km.setText(Double.toString(getcurrentmarker.getDouble("distanceCovered")) + " "+getResources().getString(R.string.kms));

            id_view_lastseentime.setText(getcurrentmarker.getString("lastSeen"));
            id_speed_limit_speed.setText(""+getResources().getString(R.string.speed_limit) + Integer.toString(getcurrentmarker.getInt("overSpeedLimit")) + " "+getResources().getString(R.string.kms_per_hr));
            id_ignition_speed.setText(getcurrentmarker.getString("ignitionStatus"));

//            System.out.println("The position is ::::::" + getcurrentmarker.getString("position"));

            if (getcurrentmarker.getString("position").equalsIgnoreCase("P")) {
                id_time_labelid.setText(""+getResources().getString(R.string.parked_time));
                id_time.setText(cons.getVehicleTimeNew(getcurrentmarker.getString("parkedTime")));
            } else if (getcurrentmarker.getString("position").equalsIgnoreCase("M")) {
                id_time_labelid.setText(""+getResources().getString(R.string.moving_time));
                id_time.setText(cons.getVehicleTimeNew(getcurrentmarker.getString("movingTime")));
            } else if (getcurrentmarker.getString("position").equalsIgnoreCase("U")) {
                id_time_labelid.setText(""+getResources().getString(R.string.no_data_time));
                id_time.setText(cons.getVehicleTimeNew(getcurrentmarker.getString("noDataTime")));
            } else if (getcurrentmarker.getString("position").equalsIgnoreCase("S")) {
                id_time_labelid.setText(""+getResources().getString(R.string.idle_time));
                id_time.setText(cons.getVehicleTimeNew(getcurrentmarker.getString("idleTime")));
            } else {
                id_time_label_layout.setVisibility(View.GONE);
            }
            SharedPreferences.Editor editor = sp.edit();
            editor.putString("current_vehicleID", getcurrentmarker.getString("vehicleId"));
            editor.commit();
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public void setDataGoogle(com.google.android.gms.maps.model.Marker mMarker) {

//        System.out.println("Hi setdata called " + mMarker.getTitle());

        JSONObject mJsonVehicleData = null, getcurrentmarker = null;
        if (mMarkersHashMapGoogle != null
                && mMarkersHashMapGoogle.size() > 0) {
            getcurrentmarker = mMarkersHashMapGoogle.get(mMarker);


        }

        try {
//            double latitude = Double.parseDouble(getcurrentmarker.getString("latitude"));
//            double longitude = Double.parseDouble(getcurrentmarker.getString("longitude"));
//            new GetAddressTask(id_view_address).execute(latitude, longitude, 1.0);

            id_view_address.setText(getcurrentmarker.getString("address"));

            selected_vehicleid = getcurrentmarker.getString("vehicleId");
            if (getcurrentmarker.getString("vehicleType").equals("Bus")) {
                id_vehicle_info_icon.setImageResource(R.drawable.bus_east);
                //  id_histroy_vehicleid.setText(getcurrentmarker.getString("vehicleId"));
                // id_history_vehicle_info_icon.setImageResource(R.drawable.bus_east);
            } else if (getcurrentmarker.getString("vehicleType").equals("Car")) {
                // id_histroy_vehicleid.setText(getcurrentmarker.getString("vehicleId"));
                // id_history_vehicle_info_icon.setImageResource(R.drawable.car_east);
                id_vehicle_info_icon.setImageResource(R.drawable.car_east);
            } else if (getcurrentmarker.getString("vehicleType").equals("Truck")) {
                //  id_histroy_vehicleid.setText(getcurrentmarker.getString("vehicleId"));
                //  id_history_vehicle_info_icon.setImageResource(R.drawable.truck_east);
                id_vehicle_info_icon.setImageResource(R.drawable.truck_east);
            } else if (getcurrentmarker.getString("vehicleType").equals("Cycle")) {
                //  id_histroy_vehicleid.setText(getcurrentmarker.getString("vehicleId"));
                //  id_history_vehicle_info_icon.setImageResource(R.drawable.cycle_east);
                id_vehicle_info_icon.setImageResource(R.drawable.cycle_east);
            } else if (getcurrentmarker.getString("vehicleType").equalsIgnoreCase("Jcb")) {
                //  id_histroy_vehicleid.setText(getcurrentmarker.getString("vehicleId"));
                //  id_history_vehicle_info_icon.setImageResource(R.drawable.cycle_east);
                id_vehicle_info_icon.setImageResource(R.drawable.jcb_icon);
            }  else if (getcurrentmarker.getString("vehicleType").equalsIgnoreCase("Bike")) {
                //  id_histroy_vehicleid.setText(getcurrentmarker.getString("vehicleId"));
                //  id_history_vehicle_info_icon.setImageResource(R.drawable.cycle_east);
                id_vehicle_info_icon.setImageResource(R.drawable.vehicle_bike);
            }  else if (getcurrentmarker.getString("vehicleType").equalsIgnoreCase("Ambulance")) {
                //  id_histroy_vehicleid.setText(getcurrentmarker.getString("vehicleId"));
                //  id_history_vehicle_info_icon.setImageResource(R.drawable.cycle_east);
                id_vehicle_info_icon.setImageResource(R.drawable.ambulance);
            }
            id_vehicleid.setText(getcurrentmarker.getString("shortName"));

//            System.out.println("Hi vehicle name " + getcurrentmarker.getString("shortName") + " lat " + latitude + " longi " + longitude);
            try {
                map_marker_vechile_position = child.indexOf(getcurrentmarker.getString("vehicleId"));
                if (map_marker_vechile_position == -1) {
                    child = (ArrayList<String>) childelements.get(0);
                    map_marker_vechile_position = child.indexOf(getcurrentmarker.getString("vehicleId"));
                }
            } catch (Exception e) {
                vehicle = (ArrayList<String>) childelements.get(0);
                map_marker_vechile_position = vehicle.indexOf(getcurrentmarker.getString("vehicleId"));
            }
            position = map_marker_vechile_position;
            id_view_speed.setText(Integer.toString(getcurrentmarker.getInt("speed")) + " "+getResources().getString(R.string.kms_per_hr));

            id_txtview_total_km.setText(Double.toString(getcurrentmarker.getDouble("distanceCovered")) + " "+getResources().getString(R.string.kms));

            id_view_lastseentime.setText(getcurrentmarker.getString("lastSeen"));
            id_speed_limit_speed.setText("Speed limit is " + Integer.toString(getcurrentmarker.getInt("overSpeedLimit")) + ""+getResources().getString(R.string.kms_per_hr));
            id_ignition_speed.setText(getcurrentmarker.getString("ignitionStatus"));

//            System.out.println("The position is ::::::" + getcurrentmarker.getString("position"));

            if (getcurrentmarker.getString("position").equalsIgnoreCase("P")) {
                id_time_labelid.setText(""+getResources().getString(R.string.parked_time));
                id_time.setText(cons.getVehicleTimeNew(getcurrentmarker.getString("parkedTime")));
            } else if (getcurrentmarker.getString("position").equalsIgnoreCase("M")) {
                id_time_labelid.setText(""+getResources().getString(R.string.moving_time));
                id_time.setText(cons.getVehicleTimeNew(getcurrentmarker.getString("movingTime")));
            } else if (getcurrentmarker.getString("position").equalsIgnoreCase("U")) {
                id_time_labelid.setText(""+getResources().getString(R.string.no_data_time));
                id_time.setText(cons.getVehicleTimeNew(getcurrentmarker.getString("noDataTime")));
            } else if (getcurrentmarker.getString("position").equalsIgnoreCase("S")) {
                id_time_labelid.setText(""+getResources().getString(R.string.idle_time));
                id_time.setText(cons.getVehicleTimeNew(getcurrentmarker.getString("idleTime")));
            } else {
                id_time_label_layout.setVisibility(View.GONE);
            }
            SharedPreferences.Editor editor = sp.edit();
            editor.putString("current_vehicleID", getcurrentmarker.getString("vehicleId"));
            editor.commit();
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }


    //    /**
//     * Code to make the Marker icon adjustable for multiple screen
//     */
    public static Bitmap createDrawableFromView(Context context, View view) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        view.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        view.measure(displayMetrics.widthPixels, displayMetrics.heightPixels);
        view.layout(0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels);
        view.buildDrawingCache();
        Bitmap bitmap = Bitmap.createBitmap(view.getMeasuredWidth(), view.getMeasuredHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        view.draw(canvas);
        return bitmap;

    }

    public static Drawable createDrawableFromViewNew(Context context, View view) {


        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        view.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));


        view.measure(displayMetrics.widthPixels, displayMetrics.heightPixels);
        view.layout(0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels);
        view.buildDrawingCache();
//        System.out.println("Hi view width " + displayMetrics.widthPixels + " " + view.getMeasuredWidth() + " height " + displayMetrics.heightPixels + " " + view.getMeasuredHeight());
        Bitmap bitmap = Bitmap.createBitmap(view.getMeasuredWidth(), view.getMeasuredHeight(), Bitmap.Config.ARGB_8888);
//        Bitmap bitmap = Bitmap.createBitmap(250, 250, Bitmap.Config.ARGB_8888);
//        Bitmap bitmap = Bitmap.crea(96, 96, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        view.draw(canvas);
//        return bitmap;


        return new BitmapDrawable(bitmap);
    }

//    public void queryUserDB() {
//
//        mGroupList.clear();
//        mGroupSpinnerList.clear();
//
//        DataBaseHandler db = new DataBaseHandler(this);
//        Cursor c = null;
//        // String qry = "SELECT DISTINCT patch_id,patch_name,mtp_id FROM "
//        // + DbHandler.TABLE_TM_DCP;
//        try {
//            // c = db.open().getDatabaseObj().rawQuery(qry, null);
//            c = db.open()
//                    .getDatabaseObj()
//                    .query(DataBaseHandler.TABLE_USER, null, null, null, null, null,
//                            null);
//
//            int user_id_index = c.getColumnIndex("user_id");
//            int group_list_index = c.getColumnIndex("group_list");
//
//            if (c.getCount() > 0) {
//
//                while (c.moveToNext()) {
//                    mSelectedUserID = c.getString(user_id_index);
//
////                    System.out.println("The group list is :::::"
////                            + c.getString(group_list_index));
//
//                    if (c.getString(group_list_index) != null) {
//                        Gson g = new Gson();
//                        InputStream is = new ByteArrayInputStream(c.getString(
//                                group_list_index).getBytes());
//                        Reader reader = new InputStreamReader(is);
//                        Type fooType = new TypeToken<List<String>>() {
//                        }.getType();
//
//                        mGroupList = g.fromJson(reader, fooType);
//                    }
//
//                }
//            }
//
//        } finally {
//            c.close();
//            db.close();
//        }
//        setDropDownData();
//    }

    public void queryUserDB() {

        mGroupList.clear();
        mGroupSpinnerList.clear();

        DaoHandler da = new DaoHandler(getApplicationContext(), false);
        da.queryUserDB();

        mSelectedUserID = Constant.mDbUserId;
        mGroupList = Constant.mUserGroupList;
        setDropDownData();
    }

    @Override
    protected void onPause() {
        super.onPause();


        if (timer != null) {
            timer.cancel();
        }


    }

    @Override
    protected void onStop() {
        super.onStop();
        if (timer != null) {
            timer.cancel();
        }

        // System.exit(0);
    }

    public void setDropDownData() {
        //mGroupList = new ArrayList<String>();
        // List<String> list = new ArrayList<String>();
        // list.add("Select");
        if (mGroupList.size() > 0) {

        } else {
            mGroupList.add("Select");
        }


        // System.out.println("The group list size is ::::" + mGroupList.size());

        for (int i = 0; i < mGroupList.size(); i++) {

            String[] str_msg_data_array = mGroupList.get(i).split(":");

            mGroupSpinnerList.add(str_msg_data_array[0]);

        }

        //  System.out.println("The group list size is ::::" + mGroupList.size() + " spinner list size is " + mGroupSpinnerList.size());

        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(
                HomeActivity.this, R.layout.spinner_row,
                mGroupSpinnerList) {

            public View getView(int position, View convertView, ViewGroup parent) {
                View v = super.getView(position, convertView, parent);
                // TextView v = (TextView) super.getView(position,
                // convertView,parent);
                // Constant.face(DailyCalls.this, (TextView) v);
                return v;
            }

            public View getDropDownView(int position, View convertView,
                                        ViewGroup parent) {
                View v = super.getDropDownView(position, convertView, parent);
                // ((TextView)
                // v).setBackgroundColor(Color.parseColor("#BBfef3da"));
                // TextView v = (TextView) super.getView(position,
                // convertView,parent);
                // Constant.face(DailyCalls.this, (TextView) v);
                return v;
            }
        };
        spinnerArrayAdapter
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        $SpinnerNavigation.setAdapter(spinnerArrayAdapter);

        String groupName = sp.getString("selected_group", "");
        int spinnerPosition = spinnerArrayAdapter.getPosition(groupName);

//set the default according to value
        $SpinnerNavigation.setSelection(spinnerPosition);
    }


    private void opptionPopUp() {
        // TODO Auto-generated method stub
        final Dialog dialog = new Dialog(HomeActivity.this);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.radio_popup);
        dialog.show();

        RadioGroup rg_home = (RadioGroup) dialog.findViewById(R.id.rg_home_views);
        RadioGroup rg_history = (RadioGroup) dialog.findViewById(R.id.rg_history_views);
        rg_history.clearCheck();
        rg_history.setVisibility(View.GONE);
        rg_home.setVisibility(View.VISIBLE);

        RadioButton $Normal = (RadioButton) dialog
                .findViewById(R.id.rb_home_normal);
        RadioButton $Satelite = (RadioButton) dialog
                .findViewById(R.id.rb_home_satellite);
        RadioButton $Terrain = (RadioButton) dialog
                .findViewById(R.id.rb_home_terrain);
        RadioButton $Hybrid = (RadioButton) dialog
                .findViewById(R.id.rb_home_hybrid);


        if (mSELECTED_MAP_TYPE.equalsIgnoreCase("Normal")) {
            $Normal.setChecked(true);
            $Satelite.setChecked(false);
            $Terrain.setChecked(false);
            $Hybrid.setChecked(false);
        } else if (mSELECTED_MAP_TYPE.equalsIgnoreCase("Satelite")) {
            $Normal.setChecked(false);
            $Satelite.setChecked(true);
            $Terrain.setChecked(false);
            $Hybrid.setChecked(false);
        } else if (mSELECTED_MAP_TYPE.equalsIgnoreCase("Terrain")) {
            $Normal.setChecked(false);
            $Satelite.setChecked(false);
            $Terrain.setChecked(true);
            $Hybrid.setChecked(false);
        } else if (mSELECTED_MAP_TYPE.equalsIgnoreCase("Hybrid")) {
            $Normal.setChecked(false);
            $Satelite.setChecked(false);
            $Terrain.setChecked(false);
            $Hybrid.setChecked(true);
        }
        $Normal.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

//                mapview.setTileSource(TileSourceFactory.MAPNIK);

                map.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                mSELECTED_MAP_TYPE = "Normal";
                dialog.dismiss();
            }
        });
        $Satelite.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                mSELECTED_MAP_TYPE = "Satelite";
                map.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
//                mapview.setTileSource(TileSourceFactory.USGS_SAT);
                dialog.dismiss();
            }
        });
        $Terrain.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                mSELECTED_MAP_TYPE = "Terrain";
                map.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
//                mapview.setTileSource(TileSourceFactory.HIKEBIKEMAP);
                dialog.dismiss();
            }
        });

        $Hybrid.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                mSELECTED_MAP_TYPE = "Hybrid";
                map.setMapType(GoogleMap.MAP_TYPE_HYBRID);
//                mapview.setTileSource(TileSourceFactory.USGS_TOPO);
                dialog.dismiss();
            }
        });

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        height = displayMetrics.heightPixels;
        width = displayMetrics.widthPixels;

        LinearLayout.LayoutParams radioParama = new LinearLayout.LayoutParams(
                FrameLayout.LayoutParams.WRAP_CONTENT,
                FrameLayout.LayoutParams.WRAP_CONTENT);
        radioParama.width = width * 50 / 100;
        radioParama.height = width * 10 / 100;
        radioParama.topMargin = height * 4 / 100;
        radioParama.gravity = Gravity.CENTER;
        radioParama.leftMargin = height * 4 / 100;
        $Normal.setLayoutParams(radioParama);
        $Satelite.setLayoutParams(radioParama);
        $Terrain.setLayoutParams(radioParama);

        LinearLayout.LayoutParams radioterrainParama = new LinearLayout.LayoutParams(
                FrameLayout.LayoutParams.WRAP_CONTENT,
                FrameLayout.LayoutParams.WRAP_CONTENT);
        radioterrainParama.width = width * 50 / 100;
        radioterrainParama.height = width * 10 / 100;
        radioterrainParama.topMargin = height * 4 / 100;
        radioterrainParama.gravity = Gravity.CENTER;
        radioterrainParama.leftMargin = height * 4 / 100;
        radioterrainParama.bottomMargin = height * 4 / 100;
        $Hybrid.setLayoutParams(radioterrainParama);

        if (width >= 600) {
            $Normal.setTextSize(16);
            $Satelite.setTextSize(16);
            $Terrain.setTextSize(16);
            $Hybrid.setTextSize(16);
        } else if (width > 501 && width < 600) {
            $Normal.setTextSize(15);
            $Satelite.setTextSize(15);
            $Terrain.setTextSize(15);
            $Hybrid.setTextSize(15);
        } else if (width > 260 && width < 500) {
            $Normal.setTextSize(14);
            $Satelite.setTextSize(14);
            $Terrain.setTextSize(14);
            $Hybrid.setTextSize(14);
        } else if (width <= 260) {
            $Normal.setTextSize(13);
            $Satelite.setTextSize(13);
            $Terrain.setTextSize(13);
            $Hybrid.setTextSize(13);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        if (timer != null) {
            timer.cancel();
        }

        startActivity(new Intent(HomeActivity.this, VehicleListActivity.class));
        finish();

    }

    private void saveSelectedGroup(String mGroupName) {
        try {
            SharedPreferences.Editor editor = sp.edit();
            editor.putString("selected_group", mGroupName);
            editor.commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void screenArrange() {
        // TODO Auto-generated method stub
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        height = displayMetrics.heightPixels;
        width = displayMetrics.widthPixels;

        LinearLayout.LayoutParams backImageParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        backImageParams.width = width * 15 / 100;
        backImageParams.height = height * 10 / 100;
        backImageParams.gravity = Gravity.CENTER;
        $ImageBack.setLayoutParams(backImageParams);
        $ImageBack.setPadding(width * 1 / 100, height * 1 / 100,
                width * 1 / 100, height * 1 / 100);

        LinearLayout.LayoutParams headTxtParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        headTxtParams.width = width * 70 / 100;
        headTxtParams.height = height * 10 / 100;
        $TxtTitle.setLayoutParams(headTxtParams);
        $TxtTitle.setPadding(width * 2 / 100, 0, 0, 0);
//        $TxtTitle.setGravity(Gravity.CENTER | Gravity.LEFT);
        $TxtTitle.setGravity(Gravity.CENTER);


        LinearLayout.LayoutParams imgMenu = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        imgMenu.height = height * 5 / 100;
        imgMenu.width = height * 5 / 100;
        imgMenu.gravity = (Gravity.CENTER);
        // imgMenu.rightMargin = height * 2 / 100;
        imgMenu.leftMargin = width * 1 / 100;
        imgMenu.rightMargin = width * 1 / 100;
        mImgRefresh.setLayoutParams(imgMenu);


        LinearLayout.LayoutParams txtParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        txtParams.width = width * 99 / 5 / 100;
        txtParams.height = height * 5 / 100;
        txtParams.gravity = Gravity.CENTER;
        $TxtNoOfVehicle.setLayoutParams(txtParams);
        $TxtOnlineVehicle.setLayoutParams(txtParams);
        $TxtOfflineVehicle.setLayoutParams(txtParams);
        $TxtNoOfVehicleValue.setLayoutParams(txtParams);
        $TxtOnlineVehicleValue.setLayoutParams(txtParams);
        $TxtOfflineVehicleValue.setLayoutParams(txtParams);

        $TxtNodataVehicle.setLayoutParams(txtParams);
        $TxtNodataVehicleValue.setLayoutParams(txtParams);
        $TxtMovingVehicle.setLayoutParams(txtParams);
        $TxtMovingVehicleValue.setLayoutParams(txtParams);

        $TxtNoOfVehicle.setGravity(Gravity.CENTER);
        $TxtOnlineVehicle.setGravity(Gravity.CENTER);
        $TxtOfflineVehicle.setGravity(Gravity.CENTER);
        $TxtNoOfVehicleValue.setGravity(Gravity.CENTER);
        $TxtOnlineVehicleValue.setGravity(Gravity.CENTER);
        $TxtOfflineVehicleValue.setGravity(Gravity.CENTER);

        $TxtNodataVehicle.setGravity(Gravity.CENTER);
        $TxtNodataVehicleValue.setGravity(Gravity.CENTER);
        $TxtMovingVehicle.setGravity(Gravity.CENTER);
        $TxtMovingVehicleValue.setGravity(Gravity.CENTER);

        LinearLayout.LayoutParams viewParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        viewParams.width = (int) (width * 0.5 / 100);
        viewParams.height = height * 10 / 100;
        $View1.setLayoutParams(viewParams);
        $View2.setLayoutParams(viewParams);

        $View5.setLayoutParams(viewParams);
        $View4.setLayoutParams(viewParams);


        LinearLayout.LayoutParams viewParams1 = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        viewParams1.width = (int) (width);
        viewParams1.height = (int) (height * 0.5 / 100);

        $View3.setLayoutParams(viewParams1);

        LinearLayout.LayoutParams spinnerParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        spinnerParams.width = width * 98 / 100;
        spinnerParams.height = height * 7 / 100;
        spinnerParams.gravity = Gravity.CENTER | Gravity.TOP;
        spinnerParams.topMargin = (int) (height * 1 / 100);
        //spinnerParams.leftMargin = width * 1 / 100;
        //spinnerParams.rightMargin = width * 1 / 100;
        //spinnerParams.bottomMargin = (int) (height * 0.25 / 100);
        $SpinnerNavigation.setLayoutParams(spinnerParams);

        FrameLayout.LayoutParams imgeChangeView = new FrameLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        imgeChangeView.width = width * 10 / 100;
        imgeChangeView.height = height * 15 / 100;
        imgeChangeView.gravity = Gravity.TOP | Gravity.RIGHT;
        imgeChangeView.rightMargin = (int) (width * 0.5 / 100);
        $ChangeView.setLayoutParams(imgeChangeView);

        if (width >= 600) {
            $TxtTitle.setTextSize(18);
            $TxtNoOfVehicle.setTextSize(16);
            $TxtOnlineVehicle.setTextSize(16);
            $TxtOfflineVehicle.setTextSize(16);
            $TxtNoOfVehicleValue.setTextSize(16);
            $TxtOnlineVehicleValue.setTextSize(16);
            $TxtOfflineVehicleValue.setTextSize(16);

            $TxtMovingVehicle.setTextSize(16);
            $TxtMovingVehicleValue.setTextSize(16);
            $TxtNodataVehicle.setTextSize(16);
            $TxtNodataVehicleValue.setTextSize(16);

        } else if (width > 501 && width < 600) {
            $TxtTitle.setTextSize(17);
            $TxtNoOfVehicle.setTextSize(15);
            $TxtOnlineVehicle.setTextSize(15);
            $TxtOfflineVehicle.setTextSize(15);
            $TxtNoOfVehicleValue.setTextSize(15);
            $TxtOnlineVehicleValue.setTextSize(15);
            $TxtOfflineVehicleValue.setTextSize(15);
            $TxtMovingVehicle.setTextSize(15);
            $TxtMovingVehicleValue.setTextSize(15);
            $TxtNodataVehicle.setTextSize(15);
            $TxtNodataVehicleValue.setTextSize(15);
        } else if (width > 260 && width < 500) {
            $TxtTitle.setTextSize(16);
            $TxtNoOfVehicle.setTextSize(14);
            $TxtOnlineVehicle.setTextSize(14);
            $TxtOfflineVehicle.setTextSize(14);
            $TxtNoOfVehicleValue.setTextSize(14);
            $TxtOnlineVehicleValue.setTextSize(14);
            $TxtOfflineVehicleValue.setTextSize(14);
            $TxtMovingVehicle.setTextSize(14);
            $TxtMovingVehicleValue.setTextSize(14);
            $TxtNodataVehicle.setTextSize(14);
            $TxtNodataVehicleValue.setTextSize(14);
        } else if (width <= 260) {
            $TxtTitle.setTextSize(15);
            $TxtNoOfVehicle.setTextSize(13);
            $TxtOnlineVehicle.setTextSize(13);
            $TxtOfflineVehicle.setTextSize(13);
            $TxtNoOfVehicleValue.setTextSize(13);
            $TxtOnlineVehicleValue.setTextSize(13);
            $TxtOfflineVehicleValue.setTextSize(13);
            $TxtMovingVehicle.setTextSize(13);
            $TxtMovingVehicleValue.setTextSize(13);
            $TxtNodataVehicle.setTextSize(13);
            $TxtNodataVehicleValue.setTextSize(13);
        }
    }
}
