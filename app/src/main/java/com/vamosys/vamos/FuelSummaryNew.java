package com.vamosys.vamos;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.github.mikephil.charting.charts.LineChart;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.vamosys.adapter.FuelSummaryAdapter;
import com.vamosys.adapter.SensorFuelAdapter;
import com.vamosys.interfaces.ApiClient;
import com.vamosys.interfaces.ApiInterface;
import com.vamosys.model.FuelSumReportDto;
import com.vamosys.model.FuelsumDto;
import com.vamosys.utils.ConnectionDetector;
import com.vamosys.utils.Constant;
import android.widget.HorizontalScrollView;
import com.vamosys.utils.HttpConfig;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FuelSummaryNew extends AppCompatActivity {
    FuelSummaryAdapter adapter;
    ImageView fuel_back,fuel_calendar;

    static RecyclerView lv;
    float totconsum=0,totalfill=0,totaltheft=0,totaldisance=0;
    TextView mTxtNoRecord, mTxtfill,mTxtconsumption,mTxttheft,mTextdist,mTxtdate,mTxtnorcrd;
    Button mButton;
    ImageView mCalendarImage, mImgMapViewClose;
    static LinearLayout mFuelDataMapLayout;
    Toolbar mToolbar;
    Spinner spinner;
    List<String> mGroupSpinnerList = new ArrayList<String>();
    List<String> mGroupList = new ArrayList<String>();
    LinearLayout card_layout;

    boolean  isFromDate = false,isToDate = false;
    ProgressDialog progressDialog;

    //    ImageView mImgDataViewChange;
    ConnectionDetector cd;
    SharedPreferences sp;
    static Const cons;

    static int width;
    static int height;
    boolean isHeaderPresent = false, mIsLineChartEnabled = false;
    private List<FuelsumDto> mFuelFillData = new ArrayList<FuelsumDto>();

    Dialog marker_info_dialog;


    //    String mTotalFuelValue = null;
    private static GoogleMap map;
    //    ImageView $ChangeView;
    boolean mIsMapPresent = false;
    String mStrUtcFromDate, mStrUtcToDate;
    String mSELECTED_MAP_TYPE = "Normal";
    static double mLatitude = 0;
    static double mLongitude = 0;
    static float vehicle_zoom_level = 15.5F;
    static Context mFuelContext;

    SimpleDateFormat timeFormatFuelFillReport = new SimpleDateFormat(
            "HH:mm:ss");

    SimpleDateFormat timeFormat = new SimpleDateFormat(
            "hh:mm:ss aa");

    SimpleDateFormat timeFormatShow = new SimpleDateFormat(
            "hh:mm aa");

    View bottomSheet;
    BottomSheetBehavior behavior;
    boolean bottomSheetEnabled = false;

    private int year, month, day;
    static final int TIME_DIALOG_ID = 1111;
    static final int DATE_DIALOG_ID = 2222;
    private int hour;
    private int minute;
    Spinner spinnerInterval;
    Button mBtnSubmit;
    TextView mTxtFromDate, mTxtLoading,mTxtEndDate, mTxtFromTime, mTxtEndTime;
    String mStartDate, mEndDate, mStartTime, mStartTimeValue, mStartTimeFuelReport, mEndTimeFuelReport, mEndDateValue, mEndTime, mEndTimeValue, mIntervalSpinnerSelectedValue = "1";
    boolean isFromTime = false, isMapPresent = false;
    int mFromHourValue = 0, mFromMinuteValue = 0, mToHourValue = 0, mToMinuteValue = 0;
    ArrayList<String> mIntervalList = new ArrayList<String>();
    int mIntervalSpinnerSelectedPos = 0;
    private LineChart mChart;
    LinearLayout mChartLayout;
    String mSelectedGroup;

    ArrayList<Integer> xAxis;
    HorizontalScrollView horizontalView;

    double mTotalFuelConsume, mTotalFuelFill, mTripDistance, mOdoStart, mOdoEnd, mTotalMileage;
    List<FuelSumReportDto> fuelDataList=new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fuel_summary_new);
        cons = new Const();
        progressDialog = new ProgressDialog(FuelSummaryNew.this,
                AlertDialog.THEME_HOLO_LIGHT);
        progressDialog.setMessage(getResources().getString(R.string.progress_dialog));
        progressDialog.setProgressDrawable(new ColorDrawable(
                Color.BLUE));
        progressDialog.setCancelable(true);
        progressDialog.setCanceledOnTouchOutside(false);
        init();
        sp = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        cd = new ConnectionDetector(getApplicationContext());
        queryUserDB();
        mFuelContext = getApplicationContext();
//        adapter = new TableAdapter(this);

        if (sp.getString("ip_adds", "") != null) {
            if (sp.getString("ip_adds", "").trim().length() > 0) {
                Const.API_URL = sp.getString("ip_adds", "");
            }
        }

        mStartDate = Const.getTripYesterdayDate2();
        Log.d("mStartDate",""+mStartDate);
        long timeInMillis = System.currentTimeMillis();
        Calendar cal1 = Calendar.getInstance();
        cal1.setTimeInMillis(timeInMillis);

        Calendar cal = Calendar.getInstance();
//        Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);

        bottomSheet = findViewById(R.id.bottom_sheet);
        behavior = BottomSheetBehavior.from(bottomSheet);
        behavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                // React to state change
//                System.out.println("Hi state 0000" + bottomSheetEnabled);


            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
                // React to dragging events
            }
        });
        setBottomLayoutData();

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mSelectedGroup = mGroupList.get(position);
                saveSelectedGroup(mGroupSpinnerList.get(position));
                String url = null;

//                url = Const.API_URL + "/mobile/getConsolidatedFuelReport?userId=" + Constant.SELECTED_USER_ID + "&groupName=" + mSelectedGroup + "&date=" + mStartDate;

                if (cd.isConnectingToInternet()) {
                    callConsolidate();
                } else {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.network_connection),
                            Toast.LENGTH_SHORT).show();
                }
                //url = Const.API_URL + "/mobile/getConsolidatedFuelReport?userId=" + Constant.SELECTED_USER_ID + "&groupName=" + mSelectedGroup + "&date=" + mStartDate;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

//        if (cd.isConnectingToInternet()) {
//
//            new getFuelSummaryData().execute(url);
//
//        } else {
//            Toast.makeText(getApplicationContext(),
//                    "Please check your network connection",
//                    Toast.LENGTH_SHORT).show();
//        }


    }

    private void queryUserDB() {
        mGroupList = Constant.mUserGroupList;

        setDropDownData();
    }

    private void setDropDownData() {
        if (mGroupList.size() > 0) {

        } else {
            mGroupList.add("Select");
        }

        for (int i = 0; i < mGroupList.size(); i++) {

            String[] str_msg_data_array = mGroupList.get(i).split(":");

            mGroupSpinnerList.add(str_msg_data_array[0]);

        }


        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(
                FuelSummaryNew.this, R.layout.spinner_fuel,
                mGroupSpinnerList) {

            public View getView(int position, View convertView, ViewGroup parent) {
                View v = super.getView(position, convertView, parent);
                // TextView v = (TextView) super.getView(position,
                // convertView,parent);
                // Constant.face(DailyCalls.this, (TextView) v);
                return v;
            }

            public View getDropDownView(int position, View convertView,
                                        ViewGroup parent) {
                View v = super.getDropDownView(position, convertView, parent);
                // ((TextView)
                // v).setBackgroundColor(Color.parseColor("#BBfef3da"));
                // TextView v = (TextView) super.getView(position,
                // convertView,parent);
                // Constant.face(DailyCalls.this, (TextView) v);
                return v;
            }
        };
        spinnerArrayAdapter
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(spinnerArrayAdapter);

        String groupName = sp.getString("selected_group", "");
        int spinnerPosition = spinnerArrayAdapter.getPosition(groupName);

//set the default according to value
        spinner.setSelection(spinnerPosition);
    }

    private void setBottomLayoutData() {
        mTxtFromDate.setText(mStartDate);
    }

    private void saveSelectedGroup(String mGroupName) {
        try {
            SharedPreferences.Editor editor = sp.edit();
            editor.putString("selected_group", mGroupName);
            editor.commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }



    private void init() {
        fuel_back = (ImageView)findViewById(R.id.fuel_summary_view_Back);
        fuel_calendar = (ImageView)findViewById(R.id.fuel_summary_change_date);
        fuel_calendar.setColorFilter(Color.WHITE);
        horizontalView=findViewById(R.id.horizontalView);
        mTxtLoading=findViewById(R.id.loading);
        horizontalView.setVisibility(View.GONE);
        mTxtLoading.setVisibility(View.GONE);
        mTxtNoRecord =(TextView)findViewById(R.id.temperature_report_no_record_txt);
        mTxtFromDate = (TextView)findViewById(R.id.txt_start_date_value);
        //mTxtEndDate = (TextView)findViewById(R.id.txt_end_date_value);
        mTxtconsumption = (TextView)findViewById(R.id.total_consumption);
        mTxtfill = (TextView)findViewById(R.id.total_fill);
        mTxttheft = (TextView)findViewById(R.id.total_theft);
        mTextdist = (TextView)findViewById(R.id.total_distance);
        spinner = (Spinner) findViewById(R.id.spinner);
        mTxtdate = (TextView)findViewById(R.id.total_date);
        card_layout =(LinearLayout)findViewById(R.id.card_layout);
        mButton = (Button)findViewById(R.id.btn_done);
        fuel_back.setColorFilter(Color.WHITE);
        fuel_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(FuelSummaryNew.this,VehicleListActivity.class));
            }
        });
        fuel_calendar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!bottomSheetEnabled) {
                    behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                } else {
                    // setData();
                    behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                }

                if (bottomSheetEnabled) {
                    bottomSheetEnabled = false;
                } else {
                    bottomSheetEnabled = true;
                }
            }
        });

        mTxtFromDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isFromDate = true;
//                showDialog(DATE_DIALOG_ID);

                showDateDialog();
            }
        });

        mButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                String url = null;
                Log.d("mStartDateee",""+mStartDate);
//                url = Const.API_URL + "/mobile/getConsolidatedFuelReport?userId=" + Constant.SELECTED_USER_ID + "&groupName=" + mSelectedGroup + "&date=" + mStartDate;

                if (cd.isConnectingToInternet()) {
                    callConsolidate();
                } else {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.network_connection),
                            Toast.LENGTH_SHORT).show();
                }
            }
        });


        mFuelDataMapLayout = (LinearLayout) findViewById(R.id.temperature_data_map_view_layout);

        lv =  findViewById(R.id.tstoppage_report_listView1);

//
//        mTxtVehicleName = (TextView) findViewById(R.id.selected_vehicle_txt);
//        mTxtVehicleName.setText(Constant.SELECTED_VEHICLE_SHORT_NAME);




        lv.setVisibility(View.VISIBLE);
    }

    private void showDateDialog() {
        if (isFromDate) {

            String[] dateArray = mStartDate.split("-");
            year = Integer.parseInt(dateArray[0].trim());
            month = Integer.parseInt(dateArray[1].trim()) - 1;
            day = Integer.parseInt(dateArray[2].trim());
        } else {

            String[] dateArray = mEndDate.split("-");
            year = Integer.parseInt(dateArray[0].trim());
            month = Integer.parseInt(dateArray[1].trim()) - 1;
            day = Integer.parseInt(dateArray[2].trim());

        }
//        System.out.println("Hi year " + year + " mo " + month + " da " + day);
//        final Calendar c = Calendar.getInstance();
//        year = c.get(Calendar.YEAR);
//        month = c.get(Calendar.MONTH);
//        day = c.get(Calendar.DAY_OF_MONTH);
        /** set date picker as current date */
        DatePickerDialog dialog = new DatePickerDialog(this,
                datePickerListener, year, month, day);
        dialog.getDatePicker().setMaxDate(System.currentTimeMillis());
        dialog.show();
    }

    private DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {

        // when dialog box is closed, below method will be called.
        public void onDateSet(DatePicker view, int selectedYear,
                              int selectedMonth, int selectedDay) {

            year = selectedYear;
            month = selectedMonth;
            day = selectedDay;
            StringBuilder mDate = new StringBuilder().append(year).append(":")
                    .append(getMonthValue(month + 1)).append(":").append(getMonthValue(day))
                    .append("");

            StringBuilder mDate2 = new StringBuilder().append(year).append("-")
                    .append(getMonthValue(month + 1)).append("-").append(getMonthValue(day))
                    .append(" ");
            if (isFromDate) {
                mStartDate = String.valueOf(mDate2);
                //  mStartTimeValue=String.valueOf(mDate2);
                Log.d("fuelDate2",""+mDate2);
                mTxtFromDate.setText(mDate2);
                mTxtdate.setText(mDate2);
            } else {
                mEndDate = String.valueOf(mDate2);

//                mTxtEndDate.setText(mDate2);
            }
//            mFromDate = String.valueOf(mDate);
//            mFromDateTxtValue = String.valueOf(mDate2);
//            mTxtDate.setText(mDate2);
            // }
        }
    };

    private String getMonthValue(int month) {
        String mMonthValue = String.valueOf(month);
        if (mMonthValue.length() == 1) {
            mMonthValue = "0" + mMonthValue;
        }
        return mMonthValue;
    }

    private void callConsolidate(){
        try {
            progressDialog.show();
            fuelDataList.clear();
            ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<List<FuelSumReportDto>> call = apiInterface.getConsolidatedFuelReport(Constant.SELECTED_USER_ID, mSelectedGroup, mStartDate.trim());
            Log.d("res", call.request().url().toString());
            call.enqueue(new Callback<List<FuelSumReportDto>>() {
                @Override
                public void onResponse(Call<List<FuelSumReportDto>> call, Response<List<FuelSumReportDto>> response) {
                    try {
                        fuelDataList = response.body();
                        Log.d("res", "size " + fuelDataList.size());
                        setTotalFuel();
                        setTableLayout();
                    }
                    catch (Exception e){
                        Log.d("res", "fail");
                        setTableLayout();
                    }
                    progressDialog.dismiss();
                    mTxtLoading.setVisibility(View.GONE);
                }
                @Override
                public void onFailure(Call<List<FuelSumReportDto>> call, Throwable t) {
                    Log.d("res", "fail " + t.getMessage());
                    setTableLayout();
                    progressDialog.dismiss();
                    mTxtLoading.setVisibility(View.GONE);

                }
            });

        }
        catch (Exception e){
            Log.d("res", "error "+e.getMessage() );
            setTableLayout();
            progressDialog.dismiss();
        }
    }

    private void setTableLayout() {
        try {
            if (fuelDataList.size() > 0) {
                lv.setVisibility(View.VISIBLE);
                card_layout.setVisibility(View.VISIBLE);
                horizontalView.setVisibility(View.VISIBLE);
                mTxtNoRecord.setVisibility(View.GONE);
                mTxtdate.setText(mStartDate);
                mTxttheft.setText(String.valueOf(new DecimalFormat("##.##").format(totaltheft)) + " ltrs");
                mTxtfill.setText(String.valueOf(new DecimalFormat("##.##").format(totalfill)) + " ltrs");
                mTxtconsumption.setText(String.valueOf(new DecimalFormat("##.##").format(totconsum)) + " ltrs");
                mTextdist.setText(String.valueOf(new DecimalFormat("##.##").format(totaldisance)) + " Kms");
//                lv.setLayoutManager(new LinearLayoutManager(FuelSummaryNew.this));
//                lv.setHasFixedSize(true);
                Log.d("res", "initial adapter");
                adapter = new FuelSummaryAdapter(FuelSummaryNew.this, fuelDataList);
                lv.setAdapter(adapter);
                adapter.notifyDataSetChanged();
            } else {
                mFuelFillData.clear();
                card_layout.setVisibility(View.GONE);
                lv.setVisibility(View.GONE);
                horizontalView.setVisibility(View.GONE);
                mTxtNoRecord.setVisibility(View.VISIBLE);
                Toast.makeText(getApplicationContext(), ""+getResources().getString(R.string.diff_date_time), Toast.LENGTH_SHORT).show();
            }
        }catch(Exception e){

        }
    }

    private void setTotalFuel() {
        Log.d("res","tot1 "+totconsum+" "+totalfill+" "+totaltheft+" "+totaldisance);
        totaldisance=0;
        totalfill=0;
        totaltheft=0;
        totconsum=0;
        try {
            for (int i = 0; i <= fuelDataList.size()-1; i++) {
                FuelSumReportDto f=fuelDataList.get(i);
                Log.d("res","tot1 "+f.getSensor());
                if(f.getFuelsensors().size()>0) {
                    Log.d("res","tot2 "+f.getFuelsensors().size());
                    for (int j = 0; j < f.getFuelsensors().size(); j++) {
                        FuelSumReportDto.Fuelsensor f1 = f.getFuelsensors().get(j);
                        Log.d("res","tot44 "+f1.getFuelConsumption());
                        if (f1.getFuelConsumption() != null && f1.getFuelConsumption().length() > 0) {
                            totconsum = totconsum + Float.parseFloat(f1.getFuelConsumption());
                        }
                        if (f1.getFuelFilling() != null && f1.getFuelFilling().length() > 0) {
                            totalfill = totalfill + Float.parseFloat(f1.getFuelFilling());
                        }
                        if (f1.getFuelTheft() != null && f1.getFuelTheft().length() > 0) {
                            totaltheft = totaltheft + Float.parseFloat(f1.getFuelTheft());
                        }
                    }
                }
                else {
                    Log.d("res","tot3 "+f.getSensor());
                    if (f.getFuelConsumption() != null && f.getFuelConsumption().length() > 0) {
                        totconsum = totconsum + Float.parseFloat(f.getFuelConsumption());
                    }
                    if (f.getFuelFilling() != null && f.getFuelFilling().length() > 0) {
                        totalfill = totalfill + Float.parseFloat(f.getFuelFilling());
                    }
                    if (f.getFuelTheft() != null && f.getFuelTheft().length() > 0) {
                        totaltheft = totaltheft + Float.parseFloat(f.getFuelTheft());
                    }
                }
                if(f.getDist()>0) {
                    totaldisance = totaldisance + Float.parseFloat(""+f.getDist());
                }
            }
            Log.d("res","tot "+totconsum+" "+totalfill+" "+totaltheft+" "+totaldisance);
        }
        catch (Exception e){
            Log.d("res","error1 "+e.getMessage());
        }
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(FuelSummaryNew.this, VehicleListActivity.class));
        finish();
    }
}
