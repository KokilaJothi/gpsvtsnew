package com.vamosys.vamos;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.vamosys.model.DataBaseHandler;
import com.vamosys.utils.Constant;
import com.vamosys.utils.DaoHandler;
import com.vamosys.utils.TypefaceUtil;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class SupportDetailsActivity extends Activity implements OnClickListener {

    int width, height;
    ImageView $ImageBack;
    Button needHelp,send;
    TextView $TxtTitle, $Divider2, $SupportLable, $SupportAddress1, $SupportAddress2,
            $SupportMobile,$SupportAddress3,$SupportAddress4,$SupportAddress5,$SupportAddress6;
    // EditText $RefreshRateEdit, $HistoryEdit;
    //ImageView $RefreshDone, $RefreshCanel, $HistoryDone, $HistoryCancel;
    //LinearLayout $RefreshLayout, $HistoryLayout;
    EditText body;
LinearLayout layout_need,layout_Support;
    private long refresh_rate, history_interval;
    String mSelectedUserId;
    List<String> mSupportList = new ArrayList<String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_support_details);
        intialization();
        screenArrange();
        queryUserDB();
        needHelp.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                layout_Support.setVisibility(View.GONE);
                layout_need.setVisibility(View.VISIBLE);
//                Intent intent = new Intent(SupportDetailsActivity.this,SupportMailActivity.class);
//                startActivity(intent);
            }
        });
        send.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                String fromEmail = "vehiclemonit@gmail.com";
                String fromPassword = "#VAMOsys123";
                String toEmails = "support@gpsvts.freshdesk.com";
//                String toEmails = "kokila.vamoays@gmail.com";
                String emailSubject = "User Id ";
                String emailBody = body.getText().toString();
                String AppName = getString(R.string.app_name).toString();
               new SendMailTask(SupportDetailsActivity.this).execute(fromEmail,
                        fromPassword, toEmails, emailSubject, emailBody,AppName);
                layout_need.setVisibility(View.GONE);
                layout_Support.setVisibility(View.VISIBLE);

            }
        });
    }
    @SuppressWarnings("rawtypes")
    public class SendMailTask extends AsyncTask {

        private ProgressDialog statusDialog;
        private Activity sendMailActivity;

        public SendMailTask(Activity activity) {
            sendMailActivity = activity;

        }

        @Override
        protected Object doInBackground(Object[] args) {
            try {
                Log.i("SendMailTask", "About to instantiate GMail...");
                publishProgress("Processing input....");
                GMail androidEmail = new GMail(args[0].toString(),
                        args[1].toString(),  args[2].toString(), args[3].toString(),
                        args[4].toString(),args[5].toString());
                publishProgress("Preparing mail message....");
                androidEmail.createEmailMessage();
                publishProgress("Sending email....");
                androidEmail.sendEmail();
                publishProgress("Email Sent.");
                Log.i("SendMailTask", "Mail Sent.");



            } catch (Exception e) {
                publishProgress(e.getMessage());
                Log.e("SendMailTask", e.getMessage(), e);}
            return null;
        }
        @Override
        public void onProgressUpdate(Object... values) {
            Toast.makeText(SupportDetailsActivity.this,""+getResources().getString(R.string.sending),Toast.LENGTH_LONG).show();
        }
        @Override
        public void onPostExecute(Object result) {
            Toast.makeText(SupportDetailsActivity.this,""+getResources().getString(R.string.mail_send),Toast.LENGTH_LONG).show();
        }
    }
    private void intialization() {
        // TODO Auto-generated method stub
        $ImageBack = (ImageView) findViewById(R.id.support_view_Back);
        // $RefreshDone = (ImageView) findViewById(R.id.id_done_refresh_rate);
        //  $RefreshCanel = (ImageView) findViewById(R.id.id_cancel_refresh_rate);
        //  $HistoryDone = (ImageView) findViewById(R.id.id_done_history);
        //  $HistoryCancel = (ImageView) findViewById(R.id.id_cancel_history);

        $TxtTitle = (TextView) findViewById(R.id.support_title);
        //  $RefreshRateLable = (TextView) findViewById(R.id.id_refresh_rate_label);
        // $Divider1 = (TextView) findViewById(R.id.id_divider1);
        $Divider2 = (TextView) findViewById(R.id.id_support_support_divider);
        // $HistoryLable = (TextView) findViewById(R.id.id_history_label);
        $SupportLable = (TextView) findViewById(R.id.id_support_support);
        $SupportAddress1 = (TextView) findViewById(R.id.id_support_support_address_line1);
        $SupportAddress2 = (TextView) findViewById(R.id.id_support_support_address_line2);
        $SupportMobile = (TextView) findViewById(R.id.id_support_support_mobilenumber);
        $SupportAddress3 = (TextView) findViewById(R.id.id_support_support_address_line3);
        $SupportAddress4 = (TextView) findViewById(R.id.id_support_support_address_line4);
        $SupportAddress5 = (TextView) findViewById(R.id.id_support_support_address_line5);
        $SupportAddress6 = (TextView) findViewById(R.id.id_support_support_address_line6);

        $SupportLable.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        $SupportAddress1.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        $SupportAddress2.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        $SupportMobile.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        $SupportAddress3.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        $SupportAddress4.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        $SupportAddress5.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        $SupportAddress6.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        needHelp=(Button)findViewById(R.id.need_help);
        send=(Button)findViewById(R.id.send);
        body=(EditText)findViewById(R.id.Body) ;

        layout_need=(LinearLayout)findViewById(R.id.layout_Help_center);
        layout_Support=(LinearLayout)findViewById(R.id.layout_support_center);

        //  $RefreshRateValue = (TextView) findViewById(R.id.id_refresh_rate_value);
        // $HistoryValue = (TextView) findViewById(R.id.id_history_value);

        // $RefreshRateEdit = (EditText) findViewById(R.id.id_refresh_rate_edit_value);
        //  $HistoryEdit = (EditText) findViewById(R.id.id_history_edit_value);

        //  $RefreshLayout = (LinearLayout) findViewById(R.id.id_refresh_rate_layout);
        // $HistoryLayout = (LinearLayout) findViewById(R.id.id_history_layout);

        $ImageBack.setOnClickListener(this);
        //$RefreshDone.setOnClickListener(this);
        //$RefreshCanel.setOnClickListener(this);
        // $HistoryDone.setOnClickListener(this);
        // $HistoryCancel.setOnClickListener(this);
        //$RefreshRateValue.setOnClickListener(this);
        //$HistoryValue.setOnClickListener(this);
    }

    public void setData() {
//        $RefreshRateValue.setText(String.valueOf(refresh_rate) + " Seconds");
//        $RefreshRateEdit.setText(String.valueOf(refresh_rate));
//        $HistoryEdit.setText(String.valueOf(history_interval));
//        $HistoryValue.setText(String.valueOf(history_interval) + " Minutes");

        if (!mSupportList.isEmpty()){
            if (mSupportList.size()==7){

                $SupportAddress6.setText(mSupportList.get(6));
            } else if (mSupportList.size()==6){
                $SupportAddress1.setText(mSupportList.get(0));
                $SupportAddress2.setText(mSupportList.get(1));
                $SupportMobile.setText(mSupportList.get(2));
                $SupportAddress3.setText(mSupportList.get(3));
                $SupportAddress4.setText(mSupportList.get(4));
                $SupportAddress5.setText(mSupportList.get(5));
            }else if (mSupportList.size()==5){
                $SupportAddress1.setText(mSupportList.get(0));
                $SupportAddress2.setText(mSupportList.get(1));
                $SupportMobile.setText(mSupportList.get(2));
                $SupportAddress3.setText(mSupportList.get(3));
                $SupportAddress4.setText(mSupportList.get(4));
            } else if (mSupportList.size()==4){
                $SupportAddress1.setText(mSupportList.get(0));
                $SupportAddress2.setText(mSupportList.get(1));
                $SupportMobile.setText(mSupportList.get(2));
                $SupportAddress3.setText(mSupportList.get(3));
            }else if (mSupportList.size()==3){
                $SupportAddress1.setText(mSupportList.get(0));
                $SupportAddress2.setText(mSupportList.get(1));
                $SupportMobile.setText(mSupportList.get(2));
            }else if (mSupportList.size()==2){
                $SupportAddress1.setText(mSupportList.get(0));
                $SupportAddress2.setText(mSupportList.get(1));
            }else if (mSupportList.size()==1){
                $SupportAddress1.setText(mSupportList.get(0));
            }

        }

    }

    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub
        switch (v.getId()) {
            case R.id.support_view_Back:
                startActivity(new Intent(SupportDetailsActivity.this, VehicleListActivity.class));
                finish();
                break;
//            case R.id.id_done_refresh_rate:
//                $RefreshDone.setVisibility(View.GONE);
//                $RefreshRateEdit.setVisibility(View.GONE);
//                $RefreshCanel.setVisibility(View.GONE);
//                $RefreshRateValue.setVisibility(View.VISIBLE);
////update refresh rate
//                String qry = null;
//                if ($RefreshRateEdit.getText().toString().trim().length() > 0) {
//                    qry = "UPDATE " + DataBaseHandler.TABLE_USER + " SET refresh_rate='" + $RefreshRateEdit.getText().toString().trim() + "' WHERE user_id='" + mSelectedUserId + "'";
//                } else {
//                    qry = "UPDATE " + DataBaseHandler.TABLE_USER + " SET refresh_rate='10' WHERE user_id='" + mSelectedUserId + "'";
//                }
//                updateDB(qry);
//
//
//                // $RefreshRateValue.setText($RefreshRateEdit.getText().toString().trim());
//
//                break;
//            case R.id.id_cancel_refresh_rate:
//                $RefreshDone.setVisibility(View.GONE);
//                $RefreshRateEdit.setVisibility(View.GONE);
//                $RefreshCanel.setVisibility(View.GONE);
//                // $RefreshRateValue.setText($RefreshRateEdit.getText().toString().trim());
//                $RefreshRateValue.setVisibility(View.VISIBLE);
//                break;
//            case R.id.id_done_history:
//                $HistoryDone.setVisibility(View.GONE);
//                $HistoryEdit.setVisibility(View.GONE);
//                $HistoryCancel.setVisibility(View.GONE);
//                $HistoryValue.setVisibility(View.VISIBLE);
//
////                update history value
//                String qry1;
//                if ($HistoryEdit.getText().toString().trim().length() > 0) {
//                    qry1 = "UPDATE " + DataBaseHandler.TABLE_USER + " SET history_interval='" + $HistoryEdit.getText().toString().trim() + "' WHERE user_id='" + mSelectedUserId + "'";
//                } else {
//                    qry1 = "UPDATE " + DataBaseHandler.TABLE_USER + " SET history_interval='1' WHERE user_id='" + mSelectedUserId + "'";
//
//                }
//
//                updateDB(qry1);
//                // $HistoryValue.setText($HistoryEdit.getText().toString().trim());
//                break;
//            case R.id.id_cancel_history:
//                $HistoryDone.setVisibility(View.GONE);
//                $HistoryEdit.setVisibility(View.GONE);
//                $HistoryCancel.setVisibility(View.GONE);
//                // $HistoryValue.setText($HistoryEdit.getText().toString().trim());
//                $HistoryValue.setVisibility(View.VISIBLE);
//                break;
//            case R.id.id_refresh_rate_value:
//                $RefreshRateValue.setVisibility(View.GONE);
//                $RefreshDone.setVisibility(View.VISIBLE);
//                $RefreshRateEdit.setVisibility(View.VISIBLE);
//                $RefreshCanel.setVisibility(View.VISIBLE);
//                break;
//            case R.id.id_history_value:
//                $HistoryValue.setVisibility(View.GONE);
//                $HistoryDone.setVisibility(View.VISIBLE);
//                $HistoryEdit.setVisibility(View.VISIBLE);
//                $HistoryCancel.setVisibility(View.VISIBLE);
//                break;

        }
    }

    /*
     * To delete inbox record except recent 5 from db
	 */
    public void updateDB(String qry) {

//        System.out.println("The update qry is ::::::" + qry);
//        DataBaseHandler db = new DataBaseHandler(getApplicationContext());
//        Cursor c = null;
//        try {
//
//            // db.open().getDatabaseObj()
//            // .delete(DbHandler.TABLE_REGIONAL_MANAGER, null, null);
//            c = db.open().getDatabaseObj().rawQuery(qry, null);
//            System.out.println("No of updated rows is ::::" + c.getCount());
//        } catch (Exception e) {
//            e.printStackTrace();
//        } finally {
//            if (c != null) {
//                c.close();
//            }
//            db.close();
//
//        }
        DaoHandler da = new DaoHandler(getApplicationContext(), true);
        da.updateDB(qry);
        queryUserDB();
    }

    public void queryUserDB() {

//        DataBaseHandler db = new DataBaseHandler(this);
//        Cursor c = null;
//        String qry = "SELECT * FROM "
//                + DataBaseHandler.TABLE_USER;
//        try {
//            c = db.open().getDatabaseObj().rawQuery(qry, null);
//
//            int user_id_index = c.getColumnIndex("user_id");
//            // int refresh_rate_index = c.getColumnIndex("refresh_rate");
//            // int history_interval_index = c.getColumnIndex("history_interval");
//            int support_adds_index = c.getColumnIndex("support_adds");
////System.out.pr
//            if (c.getCount() > 0) {
//
//                while (c.moveToNext()) {
////                    if (c.getString(refresh_rate_index) != null) {
//
////                    System.out.println("The user id is ::::" + c.getString(user_id_index) + " refresh rate is :::" + c.getString(refresh_rate_index) + " history interval is :::" + c.getString(history_interval_index) + " support is:::" + c.getString(support_adds_index));
//
////                    System.out.println("The refresh rate is :::::" + c.getString(refresh_rate_index));
//                    mSelectedUserId = c.getString(user_id_index);
//
////                    if (c.getString(refresh_rate_index) != null) {
////                        refresh_rate = Long.parseLong(c.getString(refresh_rate_index));
////                    } else {
////                        refresh_rate = 10;
////                    }
////                    if (c.getString(history_interval_index) != null) {
////                        history_interval = Long.parseLong(c.getString(history_interval_index));
////                    } else {
////                        history_interval = 1;
////                    }
//
//
//                    if (c.getString(support_adds_index) != null) {
//                        mSupportList = Arrays.asList(c.getString(support_adds_index).split(","));
//                    }
//
////                    } else {
//                    // refresh_rate = 10;
//                    // history_interval = 1;
////                    }
//
//
//                }
//            }
////            else {
////                refresh_rate = 10;
////                history_interval = 1;
////
////            }
//
//        } finally {
//            c.close();
//            db.close();
//        }

        DaoHandler da = new DaoHandler(getApplicationContext(), false);
        da.queryUserDB();
        mSelectedUserId = Constant.mDbUserId;
//        refresh_rate = Constant.db_refresh_rate;
//        history_interval = Constant.db_history_interval;
        mSupportList = Constant.mSupportDbList;


        setData();

    }

//    public void setData(){
//
//    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();

        startActivity(new Intent(SupportDetailsActivity.this, VehicleListActivity.class));
        finish();

    }

    private void screenArrange() {
        // TODO Auto-generated method stub
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        height = displayMetrics.heightPixels;
        width = displayMetrics.widthPixels;

        LinearLayout.LayoutParams backImageParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        backImageParams.width = width * 15 / 100;
        backImageParams.height = height * 10 / 100;
        backImageParams.gravity = Gravity.CENTER;
        $ImageBack.setLayoutParams(backImageParams);
        $ImageBack.setPadding(width * 1 / 100, height * 1 / 100,
                width * 1 / 100, height * 1 / 100);

        LinearLayout.LayoutParams headTxtParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        headTxtParams.width = width * 85 / 100;
        headTxtParams.height = height * 10 / 100;
        $TxtTitle.setLayoutParams(headTxtParams);
        $TxtTitle.setPadding(width * 2 / 100, 0, 0, 0);
        $TxtTitle.setGravity(Gravity.CENTER | Gravity.LEFT);

//        LinearLayout.LayoutParams txtrefreshLayoutParama = new LinearLayout.LayoutParams(
//                LinearLayout.LayoutParams.WRAP_CONTENT,
//                LinearLayout.LayoutParams.WRAP_CONTENT);
//        txtrefreshLayoutParama.width = width;
//        txtrefreshLayoutParama.height = (int) (height * 10 / 100);
//        $RefreshLayout.setLayoutParams(txtrefreshLayoutParama);
//        $HistoryLayout.setLayoutParams(txtrefreshLayoutParama);
//
//        LinearLayout.LayoutParams txtrefreshParama = new LinearLayout.LayoutParams(
//                LinearLayout.LayoutParams.WRAP_CONTENT,
//                LinearLayout.LayoutParams.WRAP_CONTENT);
//        txtrefreshParama.width = width * 35 / 100;
//        txtrefreshParama.height = (int) (height * 10 / 100);
//        $RefreshRateLable.setLayoutParams(txtrefreshParama);
//        $HistoryLable.setLayoutParams(txtrefreshParama);
//        $RefreshRateLable.setPadding(width * 4 / 100, 0, 0, 0);
//        $HistoryLable.setPadding(width * 4 / 100, 0, 0, 0);
//        $RefreshRateLable.setGravity(Gravity.CENTER | Gravity.LEFT);
//        $HistoryLable.setGravity(Gravity.CENTER | Gravity.LEFT);
//
//        LinearLayout.LayoutParams txtrefreshEditParama = new LinearLayout.LayoutParams(
//                LinearLayout.LayoutParams.WRAP_CONTENT,
//                LinearLayout.LayoutParams.WRAP_CONTENT);
//        txtrefreshEditParama.width = width * 35 / 100;
//        txtrefreshEditParama.height = (int) (height * 10 / 100);
//        $RefreshRateEdit.setLayoutParams(txtrefreshEditParama);
//        $HistoryEdit.setLayoutParams(txtrefreshEditParama);
//        $RefreshRateEdit.setGravity(Gravity.CENTER | Gravity.LEFT);
//        $HistoryEdit.setGravity(Gravity.CENTER | Gravity.LEFT);
//
//        LinearLayout.LayoutParams txtrefreshValueParama = new LinearLayout.LayoutParams(
//                LinearLayout.LayoutParams.WRAP_CONTENT,
//                LinearLayout.LayoutParams.WRAP_CONTENT);
//        txtrefreshValueParama.width = width * 65 / 100;
//        txtrefreshValueParama.height = (int) (height * 10 / 100);
//        $RefreshRateValue.setLayoutParams(txtrefreshValueParama);
//        $HistoryValue.setLayoutParams(txtrefreshValueParama);
//        $RefreshRateValue.setGravity(Gravity.CENTER | Gravity.RIGHT);
//        $HistoryValue.setGravity(Gravity.CENTER | Gravity.RIGHT);
//        $RefreshRateValue.setPadding(0, 0, width * 4 / 100, 0);
//        $HistoryValue.setPadding(0, 0, width * 4 / 100, 0);
//
//        LinearLayout.LayoutParams txtrefreshImageParama = new LinearLayout.LayoutParams(
//                LinearLayout.LayoutParams.WRAP_CONTENT,
//                LinearLayout.LayoutParams.WRAP_CONTENT);
//        txtrefreshImageParama.width = width * 10 / 100;
//        txtrefreshImageParama.height = (int) (height * 6 / 100);
//        txtrefreshImageParama.gravity = Gravity.CENTER;
//        $RefreshDone.setLayoutParams(txtrefreshImageParama);
//        $RefreshCanel.setLayoutParams(txtrefreshImageParama);
//        $HistoryDone.setLayoutParams(txtrefreshImageParama);
//        $HistoryCancel.setLayoutParams(txtrefreshImageParama);
//        $RefreshDone.setPadding((int) (width * 1.5 / 100), (int) (height * 1.5 / 100),
//                0, (int) (height * 1.5 / 100));
//        $RefreshCanel.setPadding(0, (int) (height * 1.5 / 100),
//                (int) (width * 1.5 / 100), (int) (height * 1.5 / 100));
//        $HistoryDone.setPadding((int) (width * 1.5 / 100), (int) (height * 1.5 / 100),
//                0, (int) (height * 1.5 / 100));
//        $HistoryCancel.setPadding(0, (int) (height * 1.5 / 100),
//                (int) (width * 1.5 / 100), (int) (height * 1.5 / 100));

        LinearLayout.LayoutParams dividerParama = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        dividerParama.width = width;
        dividerParama.height = (int) (height * 0.5 / 100);
//        $Divider1.setLayoutParams(dividerParama);
        $Divider2.setLayoutParams(dividerParama);


        LinearLayout.LayoutParams txtSupportLableParama = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        txtSupportLableParama.width = width;
        txtSupportLableParama.height = (int) (height * 6 / 100);
        $SupportLable.setLayoutParams(txtSupportLableParama);
        $SupportLable.setGravity(Gravity.CENTER | Gravity.LEFT);
        $SupportLable.setPadding(width * 4 / 100, 0, 0, 0);

        LinearLayout.LayoutParams txtSupportParama = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        txtSupportParama.width = width;
        //txtSupportParama.height = (int) (height * 5 / 100);
        txtSupportParama.topMargin = height * 1 / 100;
        $SupportAddress1.setLayoutParams(txtSupportParama);
        $SupportAddress2.setLayoutParams(txtSupportParama);
        $SupportMobile.setLayoutParams(txtSupportParama);

        $SupportAddress1.setGravity(Gravity.CENTER | Gravity.LEFT);
        $SupportAddress2.setGravity(Gravity.CENTER | Gravity.LEFT);
        $SupportMobile.setGravity(Gravity.CENTER | Gravity.LEFT);

        $SupportAddress1.setPadding(width * 4 / 100, 0, width * 2 / 100, 0);
        $SupportAddress2.setPadding(width * 4 / 100, 0, width * 2 / 100, 0);
        $SupportMobile.setPadding(width * 4 / 100, 0, width * 2 / 100, height * 3 / 100);

        if (width >= 600) {
            $TxtTitle.setTextSize(18);
//            $RefreshRateLable.setTextSize(16);
//            $Divider1.setTextSize(16);
            $Divider2.setTextSize(16);
//            $HistoryLable.setTextSize(16);
            $SupportLable.setTextSize(18);
            $SupportAddress1.setTextSize(17);
            $SupportAddress2.setTextSize(17);
            $SupportMobile.setTextSize(17);
//            $HistoryEdit.setTextSize(16);
//            $RefreshRateEdit.setTextSize(15);
        } else if (width > 501 && width < 600) {
            $TxtTitle.setTextSize(17);
//            $RefreshRateLable.setTextSize(15);
//            $Divider1.setTextSize(15);
            $Divider2.setTextSize(15);
//            $HistoryLable.setTextSize(15);
            $SupportLable.setTextSize(17);
            $SupportAddress1.setTextSize(16);
            $SupportAddress2.setTextSize(16);
            $SupportMobile.setTextSize(16);
//            $HistoryEdit.setTextSize(15);
//            $RefreshRateEdit.setTextSize(15);
        } else if (width > 260 && width < 500) {
            $TxtTitle.setTextSize(16);
//            $RefreshRateLable.setTextSize(14);
//            $Divider1.setTextSize(14);
            $Divider2.setTextSize(14);
//            $HistoryLable.setTextSize(14);
            $SupportLable.setTextSize(16);
            $SupportAddress1.setTextSize(14);
            $SupportAddress2.setTextSize(14);
            $SupportMobile.setTextSize(14);
//            $HistoryEdit.setTextSize(14);
//            $RefreshRateEdit.setTextSize(14);
        } else if (width <= 260) {
            $TxtTitle.setTextSize(15);
//            $RefreshRateLable.setTextSize(13);
//            $Divider1.setTextSize(13);
            $Divider2.setTextSize(13);
//            $HistoryLable.setTextSize(13);
            $SupportLable.setTextSize(15);
            $SupportAddress1.setTextSize(13);
            $SupportAddress2.setTextSize(13);
            $SupportMobile.setTextSize(13);
//            $HistoryEdit.setTextSize(13);
//            $RefreshRateEdit.setTextSize(13);
        }
    }
}
