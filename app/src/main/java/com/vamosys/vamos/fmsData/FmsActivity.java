package com.vamosys.vamos.fmsData;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.tabs.TabLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.github.fabtransitionactivity.SheetLayout;
import com.vamosys.adapter.FmsDataAdapter;
import com.vamosys.model.FmsDto;
import com.vamosys.utils.ConnectionDetector;
import com.vamosys.utils.Constant;
import com.vamosys.utils.HttpConfig;
import com.vamosys.utils.RecyclerItemClickListener;
import com.vamosys.utils.SingleTonClass;
import com.vamosys.vamos.Const;
import com.vamosys.vamos.MapMenuActivity;
import com.vamosys.vamos.R;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

public class FmsActivity extends AppCompatActivity implements SheetLayout.OnFabAnimationEndListener{
    Toolbar mToolbar;
    RecyclerView mRecyclerView;
    FloatingActionButton fab;
    TextView mTxtSelectedVehicle,delete;
    ConnectionDetector cd;
    SharedPreferences sp;
    SheetLayout mSheetLayout;
    private static final int REQUEST_CODE = 1;
    TextView mTxtNoRecord,remaindertxt,servicetxt;
    public static int posittion;
ImageView deleteback,remainderimage,serviceimage;
LinearLayout deletelayout,remainderlayout,servicelatyout;
    String notifinterval,odometer;
    JSONArray fmsArray;
    JSONObject jsonCameraObject;
    FmsDto f;
    FmsDataAdapter mAadp;

    private TabLayout tabLayout;
    private ViewPager viewPager;

//    List<FmsDto> mFmsReportList, mFmsReminderList;

//    ArrayList<String> mRemainingTypeList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fms);
        initViews();
        cd = new ConnectionDetector(getApplicationContext());

        sp = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        if (sp.getString("ip_adds", "") != null) {
            if (sp.getString("ip_adds", "").trim().length() > 0) {
                Const.API_URL = sp.getString("ip_adds", "");
            }
        }



        loadFmsData();




//
//        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
//            @Override
//            public void onTabSelected(TabLayout.Tab tab) {
//// get the current selected tab's position and replace the fragment accordingly
//                viewPager.setCurrentItem(tab.getPosition(), true);
//            }
//
//            @Override
//            public void onTabUnselected(TabLayout.Tab tab) {
//
//            }
//
//            @Override
//            public void onTabReselected(TabLayout.Tab tab) {
//
//            }
//        });


    }

    private void initViews() {

        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mToolbar.setTitle(getResources().getString(R.string.fms_report));
        mToolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_action_back));
        setSupportActionBar(mToolbar);

        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(FmsActivity.this, MapMenuActivity.class));
                finish();
            }
        });
        delete = (TextView)findViewById(R.id.delete);

        mTxtNoRecord = (TextView)findViewById(R.id.txt_norecord);
        remaindertxt = (TextView)findViewById(R.id.remainder_txt);
        servicetxt = (TextView)findViewById(R.id.service_txt);
        deleteback = (ImageView)findViewById(R.id.delete_back);
        deletelayout = (LinearLayout)findViewById(R.id.delete_layout);
        mRecyclerView = (RecyclerView) findViewById(R.id.card_recycler_view);
        remainderlayout = (LinearLayout)findViewById(R.id.remainder_layout);
        servicelatyout = (LinearLayout)findViewById(R.id.service_layout);
        remainderimage =(ImageView)findViewById(R.id.remainder_image);
        serviceimage =(ImageView)findViewById(R.id.service_image);
        mRecyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(layoutManager);


        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
//        mRecyclerView.setAdapter(mAdapter);

        // adding item touch helper
        // only ItemTouchHelper.LEFT added to detect Right to Left swipe
        // if you want both Right -> Left and Left -> Right
        // add pass ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT as param
//        ItemTouchHelper.SimpleCallback itemTouchHelperCallback = new RecyclerItemTouchHelper(0, ItemTouchHelper.LEFT, this);
//        new ItemTouchHelper(itemTouchHelperCallback).attachToRecyclerView(mRecyclerView);

        mRecyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(getApplicationContext(), mRecyclerView, new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {

                        System.out.println("Hi selected recycler pos is " + position);

                        // do whatever
                        Intent in = new Intent(FmsActivity.this, AddFmsActivity.class);
                        in.putExtra("selected_pos", position);
                        startActivity(in);
                        finish();

                    }

                    @Override
                    public void onLongItemClick(View view, int position) {
                        // do whatever
                        deletelayout.setVisibility(View.VISIBLE);
                        mToolbar.setVisibility(View.GONE);
                        Constant.longposition = position;
                        posittion = position;

                        Log.d("longposition",""+position);




                    }
                })
        );

deleteback.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        startActivity(new Intent(FmsActivity.this,MapMenuActivity.class));
        finish();
    }
});
        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (cd.isConnectingToInternet()) {
                  startActivity(new Intent(FmsActivity.this,FmsActivity.class));
                    new postFmsDeleteData().execute(SingleTonClass.getInstance().getFmsData().get(posittion).getDocumentType());
                }
                else {
                    FmsDataAdapter mAadp = new FmsDataAdapter(SingleTonClass.getInstance().getFmsData(), FmsActivity.this);

                    mRecyclerView.setAdapter(mAadp);
                    mAadp.notifyDataSetChanged();
                    Toast.makeText(getApplicationContext(), "Please check your network connection", Toast.LENGTH_SHORT).show();
                }

            }
        });
        fab = (FloatingActionButton) findViewById(R.id.fab);
        mSheetLayout = (SheetLayout)findViewById(R.id.bottom_sheetlayot);
        mSheetLayout.setFab(fab);
        mSheetLayout.setFabAnimationEndListener(this);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("remaininglistsize",""+ SingleTonClass.getInstance().getRemainingList().size());
          if (SingleTonClass.getInstance().getRemainingList() != null && SingleTonClass.getInstance().getRemainingList().size() > 0) {

              mSheetLayout.expandFab();
               } else {
              Toast.makeText(FmsActivity.this, "All FMS Data Entered", Toast.LENGTH_SHORT).show();
          }

            }
        });



       // viewPager = (ViewPager) findViewById(R.id.viewpager);
//
//        tabLayout = (TabLayout) findViewById(R.id.tabs);
//        tabLayout.setupWithViewPager(viewPager);

        mTxtSelectedVehicle = (TextView) findViewById(R.id.selected_vehicle_txt);
        mTxtSelectedVehicle.setText(Constant.SELECTED_VEHICLE_SHORT_NAME);

     remainderlayout.setOnClickListener(new View.OnClickListener() {
         @Override
         public void onClick(View v) {
         remainderimage.setColorFilter(getResources().getColor(R.color.yellow_dark));
         remaindertxt.setTextColor(getResources().getColor(R.color.yellow_dark));
         serviceimage.setColorFilter(getResources().getColor(R.color.black));
         servicetxt.setTextColor(getResources().getColor(R.color.black));
         startActivity(new Intent(FmsActivity.this,RemainderActivity.class));
         }
     });
     servicelatyout.setOnClickListener(new View.OnClickListener() {
         @Override
         public void onClick(View v) {
             remainderimage.setColorFilter(getResources().getColor(R.color.black));
             remaindertxt.setTextColor(getResources().getColor(R.color.black));
             serviceimage.setColorFilter(getResources().getColor(R.color.yellow_dark));
             servicetxt.setTextColor(getResources().getColor(R.color.yellow_dark));
             startActivity(new Intent(FmsActivity.this,ServiceActivity.class));
         }
     });

    }

//    @Override
//    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction, int position) {
//      Toast.makeText(FmsActivity.this, "Swiped pos is " + position, Toast.LENGTH_SHORT).show();
//        if (cd.isConnectingToInternet()) {
//         new postFmsDeleteData().execute(SingleTonClass.getInstance().getFmsData().get(position).getDocumentType());     }else {
//          FmsDataAdapter mAadp = new FmsDataAdapter(SingleTonClass.getInstance().getFmsData(), FmsActivity.this);
//
//           mRecyclerView.setAdapter(mAadp);
//            mAadp.notifyDataSetChanged();
//       Toast.makeText(getApplicationContext(), "Please check your network connection", Toast.LENGTH_SHORT).show();
//       }
//
//    }


    public void loadFmsData() {
        if (cd.isConnectingToInternet()) {

            new getFmsData().execute();
        } else {
            Toast.makeText(getApplicationContext(),
                    "Please check your network connection",
                    Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onFabAnimationEnd() {
                   // Intent in = new Intent(FmsActivity.this, AddFmsActivity.class);
//                    in.putExtra("selected_pos", position);
                   // startActivity(in);
                   // finish();
                        if (SingleTonClass.getInstance().getRemainingList() != null && SingleTonClass.getInstance().getRemainingList().size() > 0) {
                    Intent in = new Intent(FmsActivity.this, AddFmsActivity.class);
//                    in.putExtra("selected_pos", position);
                    startActivity(in);
                    finish();
                } else{
                            Toast.makeText(this, "All FMS Data Entered", Toast.LENGTH_SHORT).show();
                        }

    }



    private class getFmsData extends AsyncTask<String, String, String> {
        ProgressDialog progressDialog;

        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub

            String result = null;
            try {
                HttpConfig ht = new HttpConfig();
              Log.d("fmsvehicleid",""+ Constant.SELECTED_VEHICLE_ID);
                result = ht.httpGet(Const.API_URL + "/mobile/getFmsDetails?vehicleId=" + Constant.SELECTED_VEHICLE_ID + "&userId=" + Constant.SELECTED_USER_ID);

//                result = ht.httpGet("http://gpsvts.net/mobile/getFmsDetails?userId=DEMO&vehicleId=TVS-HR47C2298");

            } catch (Exception e) {

            }

            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            progressDialog.dismiss();
            SingleTonClass.getInstance().setFmsDataEmptyList(new ArrayList<FmsDto>());
            SingleTonClass.getInstance().setFmsReminderEmptyList(new ArrayList<FmsDto>());
            SingleTonClass.getInstance().setRemainingEmptyList(new ArrayList<String>());
            SingleTonClass.getInstance().setmServicedata(new ArrayList<FmsDto>());
            if (result != null && result.length() > 0) {


                try {

                    JSONObject fmsObj = new JSONObject(result.trim());
                    try {
                        if (fmsObj.has("getFmsData")) {
                            if (fmsObj.get("getFmsData") != null) {
                                 fmsArray = fmsObj.getJSONArray("getFmsData");
                                for (int i = 0; i < fmsArray.length(); i++) {
                                     jsonCameraObject = fmsArray.getJSONObject(i);
                                     f = new FmsDto();

                                    if (jsonCameraObject.has("documentType")) {
                                        f.setDocumentType(jsonCameraObject.getString("documentType"));
                                    }
                                    if (jsonCameraObject.has("dueDate")) {
                                        f.setDueDate(jsonCameraObject.getString("dueDate"));
                                    }
                                    if (jsonCameraObject.has("amount")) {
                                        f.setAmount(jsonCameraObject.getString("amount"));
                                    }
                                    if (jsonCameraObject.has("companyName")) {
                                        f.setCompanyName(jsonCameraObject.getString("companyName"));
                                    }
                                    if (jsonCameraObject.has("description")) {
                                        f.setDescription(jsonCameraObject.getString("description"));
                                    }

                                    if (jsonCameraObject.has("notifyInterval")) {
                                        f.setNotifyInterval(jsonCameraObject.getString("notifyInterval"));
                                    }
                                    if (jsonCameraObject.has("monthInterval")) {
                                        f.setMonthInterval(jsonCameraObject.getString("monthInterval"));
                                    }

                                    notifinterval = jsonCameraObject.getString("notifyInterval");
                                    odometer = jsonCameraObject.getString("odometerDue");
                                        SingleTonClass.getInstance().addToFmsData(f);


                                }
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                    try {
                        if (fmsObj.has("getReminderList")) {
                            if (fmsObj.get("getReminderList") != null) {
                                JSONArray reminderList = fmsObj.getJSONArray("getReminderList");
                                Log.d("remainderlist",""+fmsObj.getJSONArray("getReminderList"));

                                for (int i = 0; i < reminderList.length(); i++) {
                                    JSONObject jsonCameraObject = reminderList.getJSONObject(i);
                                    FmsDto f = new FmsDto();

                                    if (jsonCameraObject.has("documentType")) {
                                        f.setDocumentType(jsonCameraObject.getString("documentType"));
                                    }
                                    if (jsonCameraObject.has("dueDate")) {
                                        f.setDueDate(jsonCameraObject.getString("dueDate"));
                                    }
                                    if (jsonCameraObject.has("amount")) {
                                        f.setAmount(jsonCameraObject.getString("amount"));
                                    }
                                    if (jsonCameraObject.has("companyName")) {
                                        f.setCompanyName(jsonCameraObject.getString("companyName"));
                                    }
                                    if (jsonCameraObject.has("description")) {
                                        f.setDescription(jsonCameraObject.getString("description"));
                                    }

                                    if (jsonCameraObject.has("notifyInterval")) {
                                        f.setNotifyInterval(jsonCameraObject.getString("notifyInterval"));
                                    }
                                    if (jsonCameraObject.has("monthInterval")) {
                                        f.setMonthInterval(jsonCameraObject.getString("monthInterval"));
                                    }

                                    SingleTonClass.getInstance().addToFmsReminderList(f);
                                }

                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    try {
                        if (fmsObj.has("remainingList")) {
                            if (fmsObj.get("remainingList") != null) {
                                JSONArray remainingList = fmsObj.getJSONArray("remainingList");
                                for (int i = 0; i < remainingList.length(); i++) {
                                    SingleTonClass.getInstance().addToRemainingList(remainingList.get(i).toString().trim());
                                }
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    try {
                        if (fmsObj.has("getServiceList")) {
                            if (fmsObj.get("getServiceList") != null) {
                                JSONArray remainingList = fmsObj.getJSONArray("getServiceList");
                                Log.d("getservicelist",""+fmsObj.getJSONArray("getServiceList"));
                                for (int i = 0; i < remainingList.length(); i++) {
                                    SingleTonClass.getInstance().addToServiceList(remainingList.get(i).toString().trim());
                                }
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }



                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
                     Log.d("notifinterval",""+notifinterval);
                     Log.d("fmsodometer",""+odometer);
//            if ("null".equalsIgnoreCase(notifinterval)){
//                Log.d("notifinterval","success");
//            } else if (notifinterval !=null) {
//
//                Log.d("notifyinterval", "enter");
//                mRecyclerView.setVisibility(View.VISIBLE);
//                mTxtNoRecord.setVisibility(View.GONE);
//                mAadp = new FmsDataAdapter(SingleTonClass.getInstance().getFmsData(),FmsActivity.this);
//                mRecyclerView.setAdapter(mAadp);
//                mAadp.notifyDataSetChanged();
//            }
//
//            else {
//                Log.d("notifinterval","empty");
//                mRecyclerView.setVisibility(View.GONE);
//                mTxtNoRecord.setVisibility(View.VISIBLE);
//            }
                if ("null".equalsIgnoreCase(notifinterval)){
                    Log.d("notifinterval","success");

                } else{
                    if (SingleTonClass.getInstance().getFmsData().size() > 0) {

                        Log.d("notify", "enter");
                        mRecyclerView.setVisibility(View.VISIBLE);
                        mTxtNoRecord.setVisibility(View.GONE);
                        mAadp = new FmsDataAdapter(SingleTonClass.getInstance().getFmsData(), FmsActivity.this);
                        mRecyclerView.setAdapter(mAadp);
                        mAadp.notifyDataSetChanged();
                    }

                         else {
                            Log.d("notifinterval", "empty");
                            mRecyclerView.setVisibility(View.GONE);
                            mTxtNoRecord.setVisibility(View.VISIBLE);
                        }

                }



//
//            if (SingleTonClass.getInstance().getFmsData() != null && SingleTonClass.getInstance().getFmsData().size() > 0) {
//                mRecyclerView.setVisibility(View.VISIBLE);
//                mTxtNoRecord.setVisibility(View.GONE);
//                FmsDataAdapter mAadp = new FmsDataAdapter(SingleTonClass.getInstance().getFmsData(), FmsActivity.this);
//                mRecyclerView.setAdapter(mAadp);
//                mAadp.notifyDataSetChanged();
//
//            } else {
//                mRecyclerView.setVisibility(View.GONE);
//                mRecyclerView.setAdapter(null);
//                mTxtNoRecord.setVisibility(View.VISIBLE);
//                //show date selection layout
//            }

           // setupViewPager();

        }

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            progressDialog = new ProgressDialog(FmsActivity.this,
                    AlertDialog.THEME_HOLO_LIGHT);
            progressDialog.setMessage("Please Wait...");
            progressDialog.setProgressDrawable(new ColorDrawable(
                    Color.BLUE));
            progressDialog.setCancelable(true);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.show();
        }

    }




    private class postFmsDeleteData extends AsyncTask<String, String, String> {
        ProgressDialog progressDialog;

        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub

            String result = null;
            try {
                HttpConfig ht = new HttpConfig();
                String docType = params[0];
                String sEncoded = URLEncoder.encode(docType,"UTF-8");
//               http://188.166.244.126:9000/getFmsDetails?userId=DEMO&vehicleId=TVS-HR47C2298&documentType=FC

                result = ht.httpGet("http://188.166.244.126:9000/getFmsDetails?userId=" + Constant.SELECTED_USER_ID + "&vehicleId=" + Constant.SELECTED_VEHICLE_ID + "&documentType="+sEncoded);
               // result = ht.httpGet("http://puccagps.com:9000/getFmsDetails?userId=" + Constant.SELECTED_USER_ID + "&vehicleId=" + Constant.SELECTED_VEHICLE_ID + "&documentType="+sEncoded);
                Log.d("fmsdeletedoctype",""+sEncoded);
                Log.d("fmsdelete",""+docType);
                Log.d("fmsresult",""+result);

            } catch (Exception e) {

            }

            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            progressDialog.dismiss();
            if (result != null && result.length() > 0) {
//                try {
//
//
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }

                if (cd.isConnectingToInternet()) {
                   new getFmsData().execute();

                } else {
                    FmsDataAdapter mAadp = new FmsDataAdapter(SingleTonClass.getInstance().getFmsData(), FmsActivity.this);
                    mRecyclerView.setAdapter(mAadp);
                    mAadp.notifyDataSetChanged();
                }

            }


        }

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            progressDialog = new ProgressDialog(FmsActivity.this,
                    AlertDialog.THEME_HOLO_LIGHT);
            progressDialog.setMessage("Please Wait...");
            progressDialog.setProgressDrawable(new ColorDrawable(
                    Color.BLUE));
            progressDialog.setCancelable(true);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.show();
        }

    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();

        startActivity(new Intent(FmsActivity.this, MapMenuActivity.class));
        finish();
    }


    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == REQUEST_CODE){
            mSheetLayout.contractFab();
        }
    }

}
