package com.vamosys.vamos;

import android.Manifest;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.drawable.ColorDrawable;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.vamosys.model.DataBaseHandler;
import com.vamosys.services.LatLngLiveTrackingService;
import com.vamosys.utils.CommonManager;
import com.vamosys.utils.ConnectionDetector;
import com.vamosys.utils.Constant;
import com.vamosys.utils.DaoHandler;
import com.vamosys.utils.HttpConfig;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


public class Settings extends AppCompatActivity implements OnClickListener {

    int width, height;
    ImageView $ImageBack, mImgRefreshRateDone, mImgLiveTrackingRefreshRateDone;
    TextView $TxtTitle, $RefreshRateLable, $Divider1, $Divider2, $Divider3,
            $HistoryLable, $SupportLable, $SupportAddress1, $SupportAddress2,
            $SupportMobile, $RefreshRateValue, $HistoryValue, mNotificationEnableLabel, mEnableRefreshRateTxtView, mEdtRefreshRateTxtView, mEdtLiveTrackingRefreshRateTxtView, mTxt3Dmap,
            mTxtNotificationsSettingLabel, mTxtDomainSettingLabel, mTxtScreenSetting, mTxtLiveTrackingLabel, mTxtStartLiveTrackingHintText, mTxtImeiLabel, mTxtImeiValue, mTxtMapSetting;
    EditText $RefreshRateEdit, $HistoryEdit, mEditRefreshRate, mEditLiveTrackingRefreshRate;
    ImageView $RefreshDone, $RefreshCanel, $HistoryDone, $HistoryCancel, mImgNotificationSettingsArrow, mImgDomainSettingsArrow;
    LinearLayout $RefreshLayout, $HistoryLayout, mNotificationEnableLayout, mEnableRefreshRateLayout, mEditRefreshRateLayout, mEditLiveTrackingRefreshRateLayout,
            mLayout3dMap, mNotificationSettingsLayout, mDomainSettingsLayout, mLayoutLiveTrackingStart, mLayoutImei;
    CheckBox mChkEnableRefreshRate;
    RadioGroup mScreenRadioGroup,mMapRadioGroup;
    RadioButton mRadioHome, mRadioVehicleList, mRadioDashboard, mRadioOsm, mRadioGoogle;
    Switch notificationEnableSwitch, mEnable3DMapSwitch, mStartLiveTrackingSwitch;

    long refresh_rate, history_interval;
    String mSelectedUserId;
    List<String> mSupportList = new ArrayList<String>();
    ToggleButton toggle, toogle_3d_map;
    SharedPreferences sp;
    boolean mNotiStatus = true;
    DBHelper dbhelper;
    int registration_timeout = 10000;
    ProgressDialog progressDialog;
    ConnectionDetector cd;
    boolean mIsNotifiEnabled = true;
    boolean mIsAutRefreshEnabled = false;
    String mAutoRefreshValue = "10", mLiveTrackingInterval = "3";


    private Switch mySwitch;
    boolean m3Dmap = true, mIsDomainVerified = false, mCanShowNotificationEnableAlert = true, mIsLiveTrackingActive = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings);
        sp = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        intialization();
        screenArrange();
        dbSetup();
        queryUserDB();
        cd = new ConnectionDetector(getApplicationContext());

        mNotiStatus = sp.getBoolean("notification_enable", true);

        mIsAutRefreshEnabled = sp.getBoolean("is_auto_refresh_enabled", false);

        mAutoRefreshValue = sp.getString("auto_refresh_interval", "");

        m3Dmap = sp.getBoolean("animate_map_enabled", true);

        mIsLiveTrackingActive = sp.getBoolean("is_live_tracking_active", false);

        mLiveTrackingInterval = sp.getString("live_tracking_interval", "30");


//        System.out.println("Hi settings ip ads is " + sp.getString("ip_adds", null));
        if (sp.getString("ip_adds", null) != null && sp.getString("ip_adds", null).trim().length() > 0) {
            if (sp.getString("ip_adds", "").trim().length() > 0) {
                Const.API_URL = sp.getString("ip_adds", "");
            }
        } else {
            try {
//                System.out.println("Hi settings ip ads is updating to " + Const.API_URL);
                SharedPreferences.Editor editor = sp.edit();
                editor.putString("ip_adds", Const.API_URL);
                editor.commit();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }


        String mFirstActivity = sp.getString("first_activity", "Vehicle");
//        System.out.println("Hii the acti " + mFirstActivity);
        if (mFirstActivity != null && mFirstActivity.length() > 0) {
            if (mFirstActivity.equalsIgnoreCase("Vehicle")) {
                mRadioVehicleList.setChecked(true);
            } else if (mFirstActivity.equalsIgnoreCase("Home")) {
                mRadioHome.setChecked(true);
            } else if (mFirstActivity.equalsIgnoreCase("DashBoard")) {
                mRadioDashboard.setChecked(true);

            } else {
                mRadioVehicleList.setChecked(true);
            }
        } else {
            mRadioVehicleList.setChecked(true);
        }


        String mEnabledMap = sp.getString("enabled_map", "OSM");
//        System.out.println("Hii the acti " + mFirstActivity);
        if (mEnabledMap != null && mEnabledMap.length() > 0) {
            if (mEnabledMap.equalsIgnoreCase("OSM")) {
                mRadioOsm.setChecked(true);
            } else if (mEnabledMap.equalsIgnoreCase("GOOGLE")) {
                mRadioGoogle.setChecked(true);
            } else {
                mRadioOsm.setChecked(true);
            }
        } else {
            mRadioOsm.setChecked(true);
        }


        toggle.setChecked(mNotiStatus);
        toogle_3d_map.setChecked(m3Dmap);
        mChkEnableRefreshRate.setChecked(mIsAutRefreshEnabled);
        mEnable3DMapSwitch.setChecked(m3Dmap);
        notificationEnableSwitch.setChecked(mNotiStatus);
        mStartLiveTrackingSwitch.setChecked(mIsLiveTrackingActive);
        mEditLiveTrackingRefreshRate.setText(mLiveTrackingInterval);


        System.out.println("Hi settings mIsLiveTrackingActive " + mIsLiveTrackingActive + " is service running " + isMyServiceRunning(LatLngLiveTrackingService.class));

        if (mIsLiveTrackingActive) {


            if (!isMyServiceRunning(LatLngLiveTrackingService.class)) {
//                Intent in = new Intent(Settings.this, LatLngLiveTrackingService.class);
//                stopService(in);
                mIsLiveTrackingActive = false;
                mStartLiveTrackingSwitch.setChecked(mIsLiveTrackingActive);
                updateLiveTrackingEnabled(mIsLiveTrackingActive);

            }

        }


//        mySwitch.setChecked(m3Dmap);

        if (mIsAutRefreshEnabled)

        {
            mEditRefreshRateLayout.setVisibility(View.VISIBLE);
        } else

        {
            mEditRefreshRateLayout.setVisibility(View.GONE);
        }

        if (mAutoRefreshValue != null)

        {
            mEditRefreshRate.setText(mAutoRefreshValue);
        } else

        {
            mEditRefreshRate.setText("10");
        }

        notificationEnableSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean bChecked) {
                if (bChecked) {

                } else {

                }

                mIsNotifiEnabled = bChecked;
                if (mCanShowNotificationEnableAlert) {
                    showAlertDialog(bChecked);

                }
                mCanShowNotificationEnableAlert = true;

            }
        });

//        toggle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
//
//                                          {
//                                              public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                                                  // updateSharedPref(isChecked);
//                                                  mIsNotifiEnabled = isChecked;
//                                                  if (isChecked) {
////                    System.out.println("switch On");
//
//                                                      toggle.setGravity(Gravity.RIGHT | Gravity.CENTER);
//                                                      toggle.setPadding(0, 0, (int) (width * 3 / 100), 0);
//                                                  } else {
////                    System.out.println("switch Off");
//                                                      toggle.setGravity(Gravity.LEFT | Gravity.CENTER);
//                                                      toggle.setPadding((int) (width * 3.5 / 100), 0, 0, 0);
//                                                  }
//                                                  showAlertDialog(isChecked);
//                                              }
//                                          }
//
//        );

//        toogle_3d_map.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
//
//                                                 {
//                                                     public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                                                         // updateSharedPref(isChecked);
//                                                         update3DmapEnabled(isChecked);
//
//                                                         if (isChecked) {
////                    System.out.println("switch On");
//
//                                                             toogle_3d_map.setGravity(Gravity.RIGHT | Gravity.CENTER);
//                                                             toogle_3d_map.setPadding(0, 0, (int) (width * 3 / 100), 0);
//                                                         } else {
////                    System.out.println("switch Off");
//                                                             toogle_3d_map.setGravity(Gravity.LEFT | Gravity.CENTER);
//                                                             toogle_3d_map.setPadding((int) (width * 3.5 / 100), 0, 0, 0);
//                                                         }
//
//                                                     }
//                                                 }
//
//        );

        mEnable3DMapSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()

                                                      {
                                                          public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                                                              // updateSharedPref(isChecked);
                                                              update3DmapEnabled(isChecked);


                                                          }
                                                      }

        );

        mStartLiveTrackingSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()

                                                            {
                                                                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                                                                    // updateSharedPref(isChecked);

                                                                    if (isChecked) {
                                                                        //check permissions enabled
                                                                        if (checkLocationPermission(getApplicationContext()) && checkPhonePermission(getApplicationContext())
                                                                                && checkR3a4S4Ca64P3rm1ss10n(getApplicationContext()) && checkW6173S4Ca64P3rm1ss10n(getApplicationContext())) {


                                                                            LocationManager lm = (LocationManager) getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
                                                                            boolean gps_enabled = false;
                                                                            boolean network_enabled = false;

                                                                            try {
                                                                                gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
                                                                            } catch (Exception ex) {
                                                                            }

                                                                            try {
                                                                                network_enabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
                                                                            } catch (Exception ex) {
                                                                            }

                                                                            if (!gps_enabled && !network_enabled) {
                                                                                mStartLiveTrackingSwitch.setChecked(false);
                                                                                Toast.makeText(getApplicationContext(), ""+getResources().getString(R.string.location_services), Toast.LENGTH_SHORT).show();
                                                                            } else {
                                                                                updateLiveTrackingEnabled(isChecked);
                                                                                Intent in = new Intent(Settings.this, LatLngLiveTrackingService.class);
                                                                                startService(in);
                                                                            }

                                                                            // check whether location service has been turned on or not


                                                                            //start location tracking service


                                                                        } else {
                                                                            showP3rmAlert();
                                                                        }

                                                                    } else {
                                                                        updateLiveTrackingEnabled(isChecked);
                                                                        if (isMyServiceRunning(LatLngLiveTrackingService.class)) {
                                                                            Intent in = new Intent(Settings.this, LatLngLiveTrackingService.class);
                                                                            stopService(in);
                                                                        }
                                                                    }
//                                                                    update3DmapEnabled(isChecked);


                                                                }
                                                            }

        );

        mChkEnableRefreshRate.setOnClickListener(new OnClickListener()

                                                 {
                                                     @Override
                                                     public void onClick(View v) {
                                                         final boolean isChecked = mChkEnableRefreshRate.isChecked();

                                                         // Toast.makeText(RegistrationPage.this, String.valueOf(isChecked), Toast.LENGTH_SHORT).show();

                                                         if (!isChecked) {
                                                             mEditRefreshRateLayout.setVisibility(View.GONE);
                                                             updateDisabledRefreshRateSharedPref2(false);
                                                         } else {
                                                             mEditRefreshRateLayout.setVisibility(View.VISIBLE);
                                                         }
                                                     }
                                                 }

        );

        mySwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()

                                            {

                                                @Override
                                                public void onCheckedChanged(CompoundButton buttonView,
                                                                             boolean isChecked) {

                                                    update3DmapEnabled(isChecked);

                                                }
                                            }

        );

        mNotificationSettingsLayout.setOnClickListener(new

                                                               OnClickListener() {
                                                                   @Override
                                                                   public void onClick(View v) {
//                if (mIsNotifiEnabled) {
                                                                       startActivity(new Intent(Settings.this, NotificationFilterActivity.class));
                                                                       finish();
//                }

                                                                   }
                                                               }

        );

        mDomainSettingsLayout.setOnClickListener(new

                                                         OnClickListener() {
                                                             @Override
                                                             public void onClick(View v) {
                                                                 showChangeDomainDialog();

                                                             }
                                                         }

        );


        mMapRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()

                                                     {
                                                         @Override
                                                         public void onCheckedChanged(RadioGroup group, int checkedId) {
                                                             RadioButton rb = (RadioButton) findViewById(checkedId);

                                                             SharedPreferences.Editor editor = sp.edit();


                                                             if (rb.getText().toString().trim().equalsIgnoreCase(getResources().getString(R.string.osm))) {
                                                                 editor.putString("enabled_map", getResources().getString(R.string.osm));
                                                             } else if (rb.getText().toString().trim().equalsIgnoreCase(getResources().getString(R.string.google))) {
                                                                 editor.putString("enabled_map", getResources().getString(R.string.google));
                                                             }
                                                             editor.commit();
                                                         }
                                                     }

        );


        mScreenRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()

                                                     {
                                                         @Override
                                                         public void onCheckedChanged(RadioGroup group, int checkedId) {
                                                             RadioButton rb = (RadioButton) findViewById(checkedId);

                                                             SharedPreferences.Editor editor = sp.edit();


                                                             if (rb.getText().toString().trim().equalsIgnoreCase("Home")) {
                                                                 editor.putString("first_activity", "Home");
                                                             } else if (rb.getText().toString().trim().equalsIgnoreCase("Vehicle List")) {
                                                                 editor.putString("first_activity", "Vehicle");
                                                             } else if (rb.getText().toString().trim().equalsIgnoreCase("Dashboard")) {
                                                                 editor.putString("first_activity", "DashBoard");
                                                             }
                                                             editor.commit();
                                                         }
                                                     }

        );


    }

    public void showAlertDialog(final boolean isChecked) {
        System.out.println("showAlertDialog called ");

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(Settings.this, R.style.MyAlertDialogStyle);
        alertDialog.setTitle(""+getResources().getString(R.string.alert));
        if (isChecked) {
            alertDialog.setMessage(""+getResources().getString(R.string.enable_notifications));
        } else {
            alertDialog.setMessage(""+getResources().getString(R.string.disable_notifications));
        }


//        final EditText input = new EditText(CameraViewActivity.this);
//        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
//                LinearLayout.LayoutParams.MATCH_PARENT,
//                LinearLayout.LayoutParams.MATCH_PARENT);
//        input.setLayoutParams(lp);
//        alertDialog.setView(input);
        alertDialog.setIcon(R.mipmap.ic_launcher);
        alertDialog.setCancelable(false);
//        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setPositiveButton(""+getResources().getString(R.string.ok),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
//                        System.out.println("Hi 00 " + mNotiStatus + " new " + mIsNotifiEnabled);
                        if (cd.isConnectingToInternet()) {
                            new doSignout().execute();
                        } else {
                            Toast.makeText(getApplicationContext(), ""+getResources().getString(R.string.network_connection), Toast.LENGTH_SHORT).show();
//                            toggle.setChecked(mNotiStatus);
                            mCanShowNotificationEnableAlert = false;
                            notificationEnableSwitch.setChecked(mNotiStatus);
                        }
                    }
                });

        alertDialog.setNegativeButton(""+getResources().getString(R.string.cancel),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // updateSharedPref(mNotiStatus);

//                        System.out.println("Hi 11 " + mNotiStatus + " new " + mIsNotifiEnabled);

                        dialog.cancel();
//                        toggle.setChecked(mNotiStatus);
                        mCanShowNotificationEnableAlert = false;
                        notificationEnableSwitch.setChecked(mNotiStatus);
                    }
                });

        alertDialog.show();


    }


    public void showP3rmAlert() {
        mStartLiveTrackingSwitch.setChecked(false);
        androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(Settings.this, R.style.MyAlertDialogStyle);
        builder.setTitle("Enable app permissions");
//        builder.setMessage(getString(R.string.app_p3rm_dialog_message));
        builder.setMessage("Please enable all the permissions");

        String positiveText = this.getString(android.R.string.ok);
        builder.setPositiveButton(positiveText,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // positive button logic
                        dialog.dismiss();
                        Intent intent = new Intent();
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.setAction(android.provider.Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                        Uri uri = Uri.fromParts("package", getApplicationContext().getPackageName(), null);
                        intent.setData(uri);
                        getApplicationContext().startActivity(intent);
                        mStartLiveTrackingSwitch.setChecked(false);
                    }
                });

        String negativeText = this.getString(android.R.string.cancel);
        builder.setNegativeButton(negativeText,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // negative button logic
                        dialog.dismiss();
                        Toast.makeText(getApplicationContext(), ""+getResources().getString(R.string.phone_location_permissions), Toast.LENGTH_LONG).show();
                        startActivity(new Intent(Settings.this, VehicleListActivity.class));
                        finish();
                        mStartLiveTrackingSwitch.setChecked(false);
                    }
                });

        androidx.appcompat.app.AlertDialog dialog = builder.create();
        // display dialog
        dialog.show();

    }

    private void intialization() {
        // TODO Auto-generated method stub
        $ImageBack = (ImageView) findViewById(R.id.setting_view_Back);
        $RefreshDone = (ImageView) findViewById(R.id.id_done_refresh_rate);
        $RefreshCanel = (ImageView) findViewById(R.id.id_cancel_refresh_rate);
        $HistoryDone = (ImageView) findViewById(R.id.id_done_history);
        $HistoryCancel = (ImageView) findViewById(R.id.id_cancel_history);

        $TxtTitle = (TextView) findViewById(R.id.setting_title);
        $RefreshRateLable = (TextView) findViewById(R.id.id_refresh_rate_label);
        $Divider1 = (TextView) findViewById(R.id.id_divider1);
        $Divider3 = (TextView) findViewById(R.id.id_divider2);
        $Divider2 = (TextView) findViewById(R.id.id_settings_support_divider);
        $HistoryLable = (TextView) findViewById(R.id.id_history_label);
        $SupportLable = (TextView) findViewById(R.id.id_settings_support);
        $SupportAddress1 = (TextView) findViewById(R.id.id_settings_support_address_line1);
        $SupportAddress2 = (TextView) findViewById(R.id.id_settings_support_address_line2);
        $SupportMobile = (TextView) findViewById(R.id.id_settings_support_mobilenumber);
        $RefreshRateValue = (TextView) findViewById(R.id.id_refresh_rate_value);
        $HistoryValue = (TextView) findViewById(R.id.id_history_value);

        mTxtNotificationsSettingLabel = (TextView) findViewById(R.id.id_notification_settings_label);
        mImgNotificationSettingsArrow = (ImageView) findViewById(R.id.id_notification_settings_image);

        mTxtDomainSettingLabel = (TextView) findViewById(R.id.id_domain_settings_label);
        mImgDomainSettingsArrow = (ImageView) findViewById(R.id.id_doamin_settings_image);


        $RefreshRateEdit = (EditText) findViewById(R.id.id_refresh_rate_edit_value);
        $HistoryEdit = (EditText) findViewById(R.id.id_history_edit_value);

        $RefreshLayout = (LinearLayout) findViewById(R.id.id_refresh_rate_layout);
        $HistoryLayout = (LinearLayout) findViewById(R.id.id_history_layout);
        mEditRefreshRateLayout = (LinearLayout) findViewById(R.id.edit_refresh_rate_layout);
        mEditLiveTrackingRefreshRateLayout = (LinearLayout) findViewById(R.id.edit_live_tracking_refresh_rate_layout);
        mEnableRefreshRateLayout = (LinearLayout) findViewById(R.id.enable_refresh_rate_layout);

        mLayoutImei = (LinearLayout) findViewById(R.id.imei_number_layout);
        mTxtImeiLabel = (TextView) findViewById(R.id.imei_label_txtview);
        mTxtImeiValue = (TextView) findViewById(R.id.imei_value_txtview);

        mEditRefreshRate = (EditText) findViewById(R.id.edit_refresh_rate_edttext);
        mEditLiveTrackingRefreshRate = (EditText) findViewById(R.id.edit_live_tracking_refresh_rate_edttext);

        mEdtRefreshRateTxtView = (TextView) findViewById(R.id.edit_refresh_rate_txtview);
        mEdtLiveTrackingRefreshRateTxtView = (TextView) findViewById(R.id.edit_live_tracking_refresh_rate_txtview);
        mEnableRefreshRateTxtView = (TextView) findViewById(R.id.enable_refresh_rate_txtview);
        mTxtStartLiveTrackingHintText = (TextView) findViewById(R.id.txtview1);

        mImgRefreshRateDone = (ImageView) findViewById(R.id.edit_refresh_rate_done);
        mImgLiveTrackingRefreshRateDone = (ImageView) findViewById(R.id.edit_live_tracking_refresh_rate_done);

        mChkEnableRefreshRate = (CheckBox) findViewById(R.id.enable_refresh_rate_chkbox);

        mySwitch = (Switch) findViewById(R.id.enable_map_animate_switch);

        mTxtScreenSetting = (TextView) findViewById(R.id.txt_screen_setting);
        mScreenRadioGroup = (RadioGroup) findViewById(R.id.setting_radio_group);
        mRadioHome = (RadioButton) findViewById(R.id.Radio_Home);
        mRadioVehicleList = (RadioButton) findViewById(R.id.Radio_Vehicle_List);
        mRadioDashboard = (RadioButton) findViewById(R.id.Radio_Dashboard);


        mTxtMapSetting = (TextView) findViewById(R.id.txt_map_setting);
        mMapRadioGroup = (RadioGroup) findViewById(R.id.map_radio_group);
        mRadioOsm = (RadioButton) findViewById(R.id.Radio_Osm);
        mRadioGoogle = (RadioButton) findViewById(R.id.Radio_google);

        $ImageBack.setOnClickListener(this);
        $RefreshDone.setOnClickListener(this);
        $RefreshCanel.setOnClickListener(this);
        $HistoryDone.setOnClickListener(this);
        $HistoryCancel.setOnClickListener(this);
        $RefreshRateValue.setOnClickListener(this);
        $HistoryValue.setOnClickListener(this);
        mImgRefreshRateDone.setOnClickListener(this);
        mImgLiveTrackingRefreshRateDone.setOnClickListener(this);
        mLayoutImei.setOnClickListener(this);

        mTxt3Dmap = (TextView) findViewById(R.id.id_3dmap_label);
        mTxtLiveTrackingLabel = (TextView) findViewById(R.id.id_live_tracking_label);
        toggle = (ToggleButton) findViewById(R.id.toggleButton);
        toogle_3d_map = (ToggleButton) findViewById(R.id.id_3dmap_toggleButton);
        mNotificationEnableLabel = (TextView) findViewById(R.id.id_notification_alert_label);
        mNotificationEnableLayout = (LinearLayout) findViewById(R.id.id_notification_enable_layout);
        mLayout3dMap = (LinearLayout) findViewById(R.id.id_3dmap_enable_layout);
        mLayoutLiveTrackingStart = (LinearLayout) findViewById(R.id.id_live_tracking_start_layout);

        notificationEnableSwitch = (Switch) findViewById(R.id.switchButton);
        mEnable3DMapSwitch = (Switch) findViewById(R.id.switchButton_enable_3dmap);
        mNotificationSettingsLayout = (LinearLayout) findViewById(R.id.id_notification_settings_layout);
        mDomainSettingsLayout = (LinearLayout) findViewById(R.id.id_domain_settings_layout);

        mStartLiveTrackingSwitch = (Switch) findViewById(R.id.switchButton_start_live_tracking);
    }

    public void setData() {
        $RefreshRateValue.setText(String.valueOf(refresh_rate) +""+getResources().getString(R.string.seconds));
        $RefreshRateEdit.setText(String.valueOf(refresh_rate));
        $HistoryEdit.setText(String.valueOf(history_interval));
        $HistoryValue.setText(String.valueOf(history_interval) + ""+getResources().getString(R.string.minutes));

        if (!mSupportList.isEmpty()) {
            $SupportAddress1.setText(mSupportList.get(0));
            $SupportAddress2.setText(mSupportList.get(1));
            $SupportMobile.setText(mSupportList.get(2));
        }

    }

    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub
        switch (v.getId()) {
            case R.id.setting_view_Back:
                startActivity(new Intent(Settings.this, VehicleListActivity.class));
                finish();
                break;
            case R.id.id_done_refresh_rate:
                $RefreshDone.setVisibility(View.GONE);
                $RefreshRateEdit.setVisibility(View.GONE);
                $RefreshCanel.setVisibility(View.GONE);
                $RefreshRateValue.setVisibility(View.VISIBLE);
//update refresh rate
                String qry = null;
                if ($RefreshRateEdit.getText().toString().trim().length() > 0) {
                    qry = "UPDATE " + DataBaseHandler.TABLE_USER + " SET refresh_rate='" + $RefreshRateEdit.getText().toString().trim() + "' WHERE user_id='" + mSelectedUserId + "'";
                } else {
                    qry = "UPDATE " + DataBaseHandler.TABLE_USER + " SET refresh_rate='10' WHERE user_id='" + mSelectedUserId + "'";
                }
                updateDB(qry);


                // $RefreshRateValue.setText($RefreshRateEdit.getText().toString().trim());

                break;
            case R.id.id_cancel_refresh_rate:
                $RefreshDone.setVisibility(View.GONE);
                $RefreshRateEdit.setVisibility(View.GONE);
                $RefreshCanel.setVisibility(View.GONE);
                // $RefreshRateValue.setText($RefreshRateEdit.getText().toString().trim());
                $RefreshRateValue.setVisibility(View.VISIBLE);
                break;
            case R.id.id_done_history:
                $HistoryDone.setVisibility(View.GONE);
                $HistoryEdit.setVisibility(View.GONE);
                $HistoryCancel.setVisibility(View.GONE);
                $HistoryValue.setVisibility(View.VISIBLE);

//                update history value
                String qry1;
                if ($HistoryEdit.getText().toString().trim().length() > 0) {
                    qry1 = "UPDATE " + DataBaseHandler.TABLE_USER + " SET history_interval='" + $HistoryEdit.getText().toString().trim() + "' WHERE user_id='" + mSelectedUserId + "'";
                } else {
                    qry1 = "UPDATE " + DataBaseHandler.TABLE_USER + " SET history_interval='1' WHERE user_id='" + mSelectedUserId + "'";

                }

                updateDB(qry1);
                // $HistoryValue.setText($HistoryEdit.getText().toString().trim());
                break;
            case R.id.id_cancel_history:
                $HistoryDone.setVisibility(View.GONE);
                $HistoryEdit.setVisibility(View.GONE);
                $HistoryCancel.setVisibility(View.GONE);
                // $HistoryValue.setText($HistoryEdit.getText().toString().trim());
                $HistoryValue.setVisibility(View.VISIBLE);
                break;
            case R.id.id_refresh_rate_value:
                $RefreshRateValue.setVisibility(View.GONE);
                $RefreshDone.setVisibility(View.VISIBLE);
                $RefreshRateEdit.setVisibility(View.VISIBLE);
                $RefreshCanel.setVisibility(View.VISIBLE);
                break;
            case R.id.id_history_value:
                $HistoryValue.setVisibility(View.GONE);
                $HistoryDone.setVisibility(View.VISIBLE);
                $HistoryEdit.setVisibility(View.VISIBLE);
                $HistoryCancel.setVisibility(View.VISIBLE);
                break;

            case R.id.edit_refresh_rate_done:
                if (mEditRefreshRate.getText().toString().trim().length() > 0 && (Integer.valueOf(mEditRefreshRate.getText().toString().trim()) != 0)) {
                    updateRefreshRateSharedPref(mEditRefreshRate.getText().toString().trim());
                    Toast.makeText(Settings.this, ""+getResources().getString(R.string.auto_refresh_enabled), Toast.LENGTH_SHORT).show();

                    startActivity(new Intent(Settings.this, VehicleListActivity.class));
                    finish();

                } else {
                    Toast.makeText(Settings.this, ""+getResources().getString(R.string.enter_auto_refresh_interval), Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.imei_number_layout:
                if (checkPhonePermission(getApplicationContext())) {
                    TelephonyManager tma = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
//
                    String mImeiNoValue = tma.getDeviceId();
                    if (mImeiNoValue != null) {
                        mTxtImeiValue.setText(mImeiNoValue);
                    }
                } else {
                    showP3rmAlert();
                }
                break;

            case R.id.edit_live_tracking_refresh_rate_done:
                if (mEditLiveTrackingRefreshRate.getText().toString().trim().length() > 0 && (Integer.valueOf(mEditLiveTrackingRefreshRate.getText().toString().trim()) != 0)) {

                    System.out.println("Hi live tracking interval data is ::: " + mEditLiveTrackingRefreshRate.getText().toString().trim());

                    updateLiveTrackingInterval(mEditLiveTrackingRefreshRate.getText().toString().trim());
                    Toast.makeText(Settings.this, ""+getResources().getString(R.string.live_Tracking_interval_updated), Toast.LENGTH_SHORT).show();

//                    startActivity(new Intent(Settings.this, VehicleListActivity.class));
//                    finish();

                } else {
                    Toast.makeText(Settings.this, ""+getResources().getString(R.string.enter_Live_Tracking_interval), Toast.LENGTH_SHORT).show();
                }
                break;


        }
    }

    public void updateDisabledRefreshRateSharedPref2(boolean mIsEnabled) {

        try {
            SharedPreferences.Editor editor = sp.edit();

            editor.putBoolean("is_auto_refresh_enabled", mIsEnabled);
            editor.commit();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    public void update3DmapEnabled(boolean mIsEnabled) {
//        System.out.println("Is 3d map enabled ::::" + mIsEnabled);
        try {
            SharedPreferences.Editor editor = sp.edit();

            editor.putBoolean("animate_map_enabled", mIsEnabled);
            editor.commit();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void updateRefreshRateSharedPref(String mIntervalValue) {

        try {
            SharedPreferences.Editor editor = sp.edit();
            editor.putString("auto_refresh_interval", mIntervalValue);
            editor.putBoolean("is_auto_refresh_enabled", true);
            editor.commit();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void updateLiveTrackingEnabled(boolean mIsEnabled) {
//        System.out.println("Is 3d map enabled ::::" + mIsEnabled);
        try {
            SharedPreferences.Editor editor = sp.edit();

            editor.putBoolean("is_live_tracking_active", mIsEnabled);
            editor.commit();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void updateLiveTrackingInterval(String mIntervalValue) {

        try {
            SharedPreferences.Editor editor = sp.edit();
            editor.putString("live_tracking_interval", mIntervalValue);

            editor.commit();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    void dbSetup() {
        dbhelper = new DBHelper(this);
        try {
            dbhelper.createDataBase();
            dbhelper.openDataBase();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private class doSignout extends AsyncTask<String, String, String> {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            progressDialog = new ProgressDialog(Settings.this,
                    AlertDialog.THEME_HOLO_LIGHT);
            progressDialog.setMessage(""+getResources().getString(R.string.progress_dialog));
            progressDialog.setProgressDrawable(new ColorDrawable(
                    android.graphics.Color.BLUE));
            progressDialog.setCancelable(true);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                String gcmToken = sp.getString("gcmToken", null);

//                System.out.println("The gcm key is :::::" + gcmToken);
                HttpConfig ht = new HttpConfig();
                return ht.httpGet(Const.API_URL + "mobile/logOutUser?userId=" + Constant.SELECTED_USER_ID + "&gcmId=" + gcmToken);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        public void onPostExecute(String result) {
//            if (progressDialog.isShowing()) {
//                progressDialog.dismiss();
//            }
            try {
                /**
                 * Perform logout by clearing the login status and start registration activity.
                 */
                if (result != null && result.length() > 0) {
                    CommonManager co = new CommonManager(getApplicationContext());
                    // String qry = "DELETE FROM " + DataBaseHandler.TABLE_USER;
                    // String qry1 = "DELETE FROM " + DataBaseHandler.TABLE_LOGO;
                    // String qry2 = "DELETE FROM " + DataBaseHandler.TABLE_NOTIFICATIONS;
                    // co.deleteDB(qry);
                    // co.deleteDB(qry1);
                    //  co.deleteDB(qry2);
                    new SendUserCredentials().execute();

//                    dbhelper.update_StdTableValues("login_status", false);
//                    Intent register = new Intent(Settings.this, RegistrationPage.class);
//                    startActivity(register);
//                    finish();
                } else {
//                    toggle.setChecked(mNotiStatus);
                    mCanShowNotificationEnableAlert = false;
                    notificationEnableSwitch.setChecked(mNotiStatus);
                    if (progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.oops_error), Toast.LENGTH_SHORT).show();
                }
            } catch (Exception e) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                e.printStackTrace();
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.oops_error), Toast.LENGTH_SHORT).show();
//                toggle.setChecked(mNotiStatus);
                mCanShowNotificationEnableAlert = false;
                notificationEnableSwitch.setChecked(mNotiStatus);
            }
        }
    }


    private class SendUserCredentials extends AsyncTask<String, String, String> {
        String username = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected String doInBackground(String... params) {
            String response_from_server = null;
            // username = params[0];

            String username = sp.getString("user_name", null);
            String password = sp.getString("password", null);

            String gcmToken = sp.getString("gcmToken", null);
            //   mNotiStatus = sp.getBoolean("notification_enable", true);
            String urlstr = Const.API_URL + "mobile/verifyUser?userId=" + username + "&password=" + password + "&imei=" + getMobileDetails() + "&macid=" + getStdTableValue("macid") + "&appid=" + getStdTableValue("appid") + "&gcmId=" + gcmToken + "&notifyEnable=" + mIsNotifiEnabled;
//            Log.d(TAG, "doInBackground() called with: " + "params = [" + urlstr + "]");
            Const constaccess = new Const();
            try {
                response_from_server = constaccess.sendGet(urlstr, registration_timeout);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return response_from_server;
        }

//        protected void onProgressUpdate(String... progress) {
//            showDialog(DIALOG1_KEY);
//        }

        @Override
        protected void onPostExecute(String result) {
//            Log.d(TAG, "onPostExecute() called with: " + "result = [" + result + "]");
            try {
                if (result != null && !result.isEmpty()) {
                    JSONObject mJsonObject = new JSONObject(result);
                    if (mJsonObject.has("authUser")) {
                        if (mJsonObject.getString("authUser").toString().equalsIgnoreCase("success")) {
                            updateSharedPref(mIsNotifiEnabled);
                            mNotiStatus = mIsNotifiEnabled;

//                            System.out.println("Hi 22" + mNotiStatus + " new " + mIsNotifiEnabled);

//                            toggle.setChecked(mIsNotifiEnabled);
                            mCanShowNotificationEnableAlert = true;
                            notificationEnableSwitch.setChecked(mIsNotifiEnabled);


                        } else {
//                            toggle.setChecked(mNotiStatus);
                            mCanShowNotificationEnableAlert = false;
                            notificationEnableSwitch.setChecked(mNotiStatus);
                            Toast.makeText(getApplicationContext(), ""+getResources().getString(R.string.oops_try_after_sometime), Toast.LENGTH_LONG).show();
                        }
                    } else {
//                        toggle.setChecked(mNotiStatus);
                        mCanShowNotificationEnableAlert = false;
                        notificationEnableSwitch.setChecked(mNotiStatus);
                        Toast.makeText(getApplicationContext(), ""+getResources().getString(R.string.oops_try_after_sometime), Toast.LENGTH_LONG).show();
                    }
                } else {
//                    toggle.setChecked(mNotiStatus);
                    mCanShowNotificationEnableAlert = false;
                    notificationEnableSwitch.setChecked(mNotiStatus);
                    Toast.makeText(getApplicationContext(), ""+getResources().getString(R.string.oops_try_after_sometime), Toast.LENGTH_LONG).show();
                }
            } catch (Exception e) {
//                toggle.setChecked(mNotiStatus);
                mCanShowNotificationEnableAlert = false;
                notificationEnableSwitch.setChecked(mNotiStatus);
                e.printStackTrace();
                Toast.makeText(getApplicationContext(), ""+getResources().getString(R.string.oops_try_after_sometime), Toast.LENGTH_LONG).show();
            }
            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
        }
    }

    //To get IMEI no from mobile device.
    public String getMobileDetails() {
        String mMobileNo = null;
        if (ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED) {
            TelephonyManager mngr = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
            mMobileNo = mngr.getDeviceId();
        }
        return mMobileNo;

    }

    public String getPhoneImeiNo() {
        String mMobileNo = null;
        if (ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED) {
            TelephonyManager tMgr = (TelephonyManager) getApplicationContext().getSystemService(Context.TELEPHONY_SERVICE);
            mMobileNo = tMgr.getLine1Number();
        }

        if (mMobileNo == null) {
            try {
                TelephonyManager telemgr = (TelephonyManager) this
                        .getSystemService(Context.TELEPHONY_SERVICE);
                mMobileNo = telemgr.getDeviceId();
                // .trim() == null ? "" : telemgr.getDeviceId().trim();
            } catch (Exception e) {
                mMobileNo = "EX2";
            }
        }

        return mMobileNo;
    }


    /***/
    public String getStdTableValue(String tag) {
        Cursor std_table_cur = null;
        String std_table_value = null;
        try {
            std_table_cur = dbhelper.get_std_table_info(tag);
            std_table_cur.moveToFirst();
            std_table_value = std_table_cur.getString(0);
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), ""+getResources().getString(R.string.please_try_again), Toast.LENGTH_SHORT).show();
        } finally {
            if (std_table_cur != null) {
                std_table_cur.close();
            }
        }
        return std_table_value;
    }

    /*
     * To delete inbox record except recent 5 from db
	 */
    public void updateDB(String qry) {

////        System.out.println("The update qry is ::::::" + qry);
//        DataBaseHandler db = new DataBaseHandler(getApplicationContext());
//        Cursor c = null;
//        try {
//
//            // db.open().getDatabaseObj()
//            // .delete(DbHandler.TABLE_REGIONAL_MANAGER, null, null);
//            c = db.open().getDatabaseObj().rawQuery(qry, null);
//            System.out.println("No of updated rows is ::::" + c.getCount());
//        } catch (Exception e) {
//            e.printStackTrace();
//        } finally {
//            if (c != null) {
//                c.close();
//            }
//            db.close();
//
//        }

        DaoHandler da = new DaoHandler(getApplicationContext(), true);
        da.updateDB(qry);


        queryUserDB();
    }

    public void queryUserDB() {

//        DataBaseHandler db = new DataBaseHandler(this);
//        Cursor c = null;
//        String qry = "SELECT * FROM "
//                + DataBaseHandler.TABLE_USER;
//        try {
//            c = db.open().getDatabaseObj().rawQuery(qry, null);
//
//            int user_id_index = c.getColumnIndex("user_id");
//            int refresh_rate_index = c.getColumnIndex("refresh_rate");
//            int history_interval_index = c.getColumnIndex("history_interval");
//            int support_adds_index = c.getColumnIndex("support_adds");
////System.out.pr
//            if (c.getCount() > 0) {
//
//                while (c.moveToNext()) {
////                    if (c.getString(refresh_rate_index) != null) {
//
////                    System.out.println("The user id is ::::" + c.getString(user_id_index) + " refresh rate is :::" + c.getString(refresh_rate_index) + " history interval is :::" + c.getString(history_interval_index) + " support is:::" + c.getString(support_adds_index));
//
////                    System.out.println("The refresh rate is :::::" + c.getString(refresh_rate_index));
//                    mSelectedUserId = c.getString(user_id_index);
//
//                    if (c.getString(refresh_rate_index) != null) {
//                        refresh_rate = Long.parseLong(c.getString(refresh_rate_index));
//                    } else {
//                        refresh_rate = 10;
//                    }
//                    if (c.getString(history_interval_index) != null) {
//                        history_interval = Long.parseLong(c.getString(history_interval_index));
//                    } else {
//                        history_interval = 1;
//                    }
//
//
//                    if (c.getString(support_adds_index) != null) {
//                        mSupportList = Arrays.asList(c.getString(support_adds_index).split(","));
//                    }
//
////                    } else {
//                    // refresh_rate = 10;
//                    // history_interval = 1;
////                    }
//
//
//                }
//            } else {
//                refresh_rate = 10;
//                history_interval = 1;
//
//            }
//
//        } finally {
//            c.close();
//            db.close();
//        }

        DaoHandler da = new DaoHandler(getApplicationContext(), false);
        da.queryUserDB();
        mSelectedUserId = Constant.mDbUserId;
        refresh_rate = Constant.db_refresh_rate;
        history_interval = Constant.db_history_interval;
        mSupportList = Constant.mSupportDbList;

        if (refresh_rate == 0) {
            refresh_rate = 10;
        }


        if (history_interval == 0) {
            history_interval = 1;
        }


        setData();

    }

    public void updateSharedPref(boolean isEnables) {
        try {
            SharedPreferences.Editor editor = sp.edit();
            editor.putBoolean("notification_enable", isEnables);
            editor.commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    Dialog domain_adds_dialog;

    String mGetUrl = null;

    private boolean showChangeDomainDialog() {


        final EditText mEdtDoaminAdds;
        TextView txt_popup_head;
        Button btn_verify_domain, btn_update_domain;

        domain_adds_dialog = new Dialog(Settings.this);

        domain_adds_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        domain_adds_dialog.setContentView(R.layout.domain_name_change_popup);
        domain_adds_dialog.setCancelable(true);

        txt_popup_head = (TextView) domain_adds_dialog.findViewById(R.id.change_domain_popup_title_txt);
        mEdtDoaminAdds = (EditText) domain_adds_dialog.findViewById(R.id.change_domain_popup_domain_adds_edtText);
        btn_verify_domain = (Button) domain_adds_dialog.findViewById(R.id.change_domain_popup_verify_domain_btn);
        btn_update_domain = (Button) domain_adds_dialog.findViewById(R.id.change_domain_popup_update_doamin_btn);
        mEdtDoaminAdds.setText(Const.API_URL);


        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);

        Constant.ScreenWidth = width;
        Constant.ScreenHeight = height;


        LinearLayout.LayoutParams popupHeadParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        popupHeadParams.width = width * 90 / 100;
        popupHeadParams.height = height * 7 / 100;
        // popupHeadParams.height = LinearLayout.LayoutParams.WRAP_CONTENT;
        popupHeadParams.setMargins(width * 1 / 100, width * 1 / 100, width * 1 / 100, width * 1 / 100);
        txt_popup_head.setLayoutParams(popupHeadParams);
        txt_popup_head.setGravity(Gravity.CENTER);

        btn_verify_domain.setLayoutParams(popupHeadParams);
        btn_verify_domain.setGravity(Gravity.CENTER);

        btn_update_domain.setLayoutParams(popupHeadParams);
        btn_update_domain.setGravity(Gravity.CENTER);

        LinearLayout.LayoutParams backImageParams1 = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        backImageParams1.width = width * 88 / 100;
        backImageParams1.height = height * 8 / 100;
        backImageParams1.setMargins(width * 1 / 100, width * 1 / 100, width * 1 / 100, width * 1 / 100);

        mEdtDoaminAdds.setLayoutParams(backImageParams1);


        if (width >= 600) {
            txt_popup_head.setTextSize(18);
            mEdtDoaminAdds.setTextSize(17);
            btn_verify_domain.setTextSize(17);
            btn_update_domain.setTextSize(17);
        } else if (width > 501 && width < 600) {
            txt_popup_head.setTextSize(17);
            mEdtDoaminAdds.setTextSize(16);
            btn_verify_domain.setTextSize(16);
            btn_update_domain.setTextSize(16);
        } else if (width > 260 && width < 500) {
            txt_popup_head.setTextSize(16);
            mEdtDoaminAdds.setTextSize(15);
            btn_verify_domain.setTextSize(15);
            btn_update_domain.setTextSize(15);
        } else if (width <= 260) {
            txt_popup_head.setTextSize(15);
            mEdtDoaminAdds.setTextSize(14);
            btn_verify_domain.setTextSize(14);
            btn_update_domain.setTextSize(14);
        }

        mEdtDoaminAdds.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                mIsDomainVerified = false;
            }
        });


        btn_verify_domain.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {


                if (mEdtDoaminAdds.getText().toString().trim().length() > 0) {

                    mGetUrl = mEdtDoaminAdds.getText().toString().trim();

                    if (mGetUrl.startsWith("http://") || mGetUrl.startsWith("https://")) {
                        if (mGetUrl.endsWith("/")) {

                        } else {
                            mGetUrl = mGetUrl + "/";
                        }
                    } else {
                        mGetUrl = "http://" + mGetUrl;

                        if (mGetUrl.endsWith("/")) {

                        } else {
                            mGetUrl = mGetUrl + "/";
                        }

                    }

                    if (cd.isConnectingToInternet()) {
                        new checkDomainValidation().execute(mGetUrl);
                    } else {
                        Toast internet_toast = Toast.makeText(getApplicationContext(), ""+getResources().getString(R.string.network_connection), Toast.LENGTH_LONG);
                        internet_toast.show();
                    }

                } else {
                    Toast internet_toast = Toast.makeText(getApplicationContext(), ""+getResources().getString(R.string.enter_domain_address), Toast.LENGTH_LONG);
                    internet_toast.show();
                }

            }
        });

        /** listener to handle when history is selected */
        btn_update_domain.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {


                if (mIsDomainVerified) {
                    if (mGetUrl != null && mGetUrl.trim().length() > 0) {
                        try {
                            SharedPreferences.Editor editor = sp.edit();
                            editor.putString("ip_adds", mGetUrl);
                            editor.commit();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        Const.API_URL = mGetUrl;
                        domain_adds_dialog.dismiss();

                        Toast internet_toast = Toast.makeText(getApplicationContext(), ""+getResources().getString(R.string.domain_update), Toast.LENGTH_LONG);
                        internet_toast.show();
                    } else {
                        Toast internet_toast = Toast.makeText(getApplicationContext(), ""+getResources().getString(R.string.domain_address_value), Toast.LENGTH_LONG);
                        internet_toast.show();
                    }
                } else {
                    Toast internet_toast = Toast.makeText(getApplicationContext(), ""+getResources().getString(R.string.verify_domain_address), Toast.LENGTH_LONG);
                    internet_toast.show();
                }


            }
        });

        domain_adds_dialog.show();
        return true;
    }


    private class checkDomainValidation extends AsyncTask<String, String, String> {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            progressDialog = new ProgressDialog(Settings.this,
                    AlertDialog.THEME_HOLO_LIGHT);
            progressDialog.setMessage(""+getResources().getString(R.string.progress_dialog));
            progressDialog.setProgressDrawable(new ColorDrawable(
                    android.graphics.Color.BLUE));
            progressDialog.setCancelable(true);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                HttpConfig ht = new HttpConfig();
                return ht.httpDomainVerify(params[0]);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        public void onPostExecute(String result) {
            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
            System.out.println("Hi domain validate response is :::" + result);
            if (result != null && result.trim().length() > 0) {


//                if (!result.trim().equalsIgnoreCase("200")) {
//                mIsDomainVerified = false;
//                Toast internet_toast = Toast.makeText(getApplicationContext(), "Invalid Domain address", Toast.LENGTH_LONG);
//                internet_toast.show();
//                } else {
                mIsDomainVerified = true;
                Toast internet_toast = Toast.makeText(getApplicationContext(), ""+getResources().getString(R.string.valid_domain_address), Toast.LENGTH_LONG);
                internet_toast.show();
//                }
            } else {
                mIsDomainVerified = false;
                Toast internet_toast = Toast.makeText(getApplicationContext(), ""+getResources().getString(R.string.invalid_domain), Toast.LENGTH_LONG);
                internet_toast.show();
            }

        }

    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();

        startActivity(new Intent(Settings.this, VehicleListActivity.class));
        finish();

    }

    private void screenArrange() {
        // TODO Auto-generated method stub
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        height = displayMetrics.heightPixels;
        width = displayMetrics.widthPixels;

        LinearLayout.LayoutParams backImageParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        backImageParams.width = width * 15 / 100;
        backImageParams.height = height * 10 / 100;
        backImageParams.gravity = Gravity.CENTER;
        $ImageBack.setLayoutParams(backImageParams);
        $ImageBack.setPadding(width * 1 / 100, height * 1 / 100,
                width * 1 / 100, height * 1 / 100);

        LinearLayout.LayoutParams headTxtParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        headTxtParams.width = width * 85 / 100;
        headTxtParams.height = height * 8 / 100;
        $TxtTitle.setLayoutParams(headTxtParams);
        $TxtTitle.setPadding(width * 2 / 100, 0, 0, 0);
        $TxtTitle.setGravity(Gravity.CENTER | Gravity.LEFT);

        LinearLayout.LayoutParams txtrefreshLayoutParama = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        txtrefreshLayoutParama.width = width;
        txtrefreshLayoutParama.height = (int) (height * 10 / 100);
        $RefreshLayout.setLayoutParams(txtrefreshLayoutParama);
        $HistoryLayout.setLayoutParams(txtrefreshLayoutParama);
        mNotificationEnableLayout.setLayoutParams(txtrefreshLayoutParama);
        mLayout3dMap.setLayoutParams(txtrefreshLayoutParama);
        mLayoutLiveTrackingStart.setLayoutParams(txtrefreshLayoutParama);

        LinearLayout.LayoutParams txtrefreshParama = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        txtrefreshParama.width = width * 35 / 100;
        txtrefreshParama.height = (int) (height * 10 / 100);
        $RefreshRateLable.setLayoutParams(txtrefreshParama);
        $HistoryLable.setLayoutParams(txtrefreshParama);
        mNotificationEnableLabel.setLayoutParams(txtrefreshParama);
        mTxt3Dmap.setLayoutParams(txtrefreshParama);
        mTxtLiveTrackingLabel.setLayoutParams(txtrefreshParama);
        $RefreshRateLable.setPadding(width * 4 / 100, 0, 0, 0);
        $HistoryLable.setPadding(width * 4 / 100, 0, 0, 0);
        mNotificationEnableLabel.setPadding(width * 4 / 100, 0, 0, 0);
        mTxt3Dmap.setPadding(width * 4 / 100, 0, 0, 0);
        mTxtLiveTrackingLabel.setPadding(width * 4 / 100, 0, 0, 0);
        $RefreshRateLable.setGravity(Gravity.CENTER | Gravity.LEFT);
        $HistoryLable.setGravity(Gravity.CENTER | Gravity.LEFT);
        mNotificationEnableLabel.setGravity(Gravity.CENTER | Gravity.LEFT);
        mTxt3Dmap.setGravity(Gravity.CENTER | Gravity.LEFT);
        mTxtLiveTrackingLabel.setGravity(Gravity.CENTER | Gravity.LEFT);

        LinearLayout.LayoutParams txtrefreshEditParama = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        txtrefreshEditParama.width = width * 35 / 100;
        txtrefreshEditParama.height = (int) (height * 10 / 100);
        $RefreshRateEdit.setLayoutParams(txtrefreshEditParama);
        $HistoryEdit.setLayoutParams(txtrefreshEditParama);
        $RefreshRateEdit.setGravity(Gravity.CENTER | Gravity.LEFT);
        $HistoryEdit.setGravity(Gravity.CENTER | Gravity.LEFT);

        LinearLayout.LayoutParams txtrefreshValueParama = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        txtrefreshValueParama.width = width * 65 / 100;
        txtrefreshValueParama.height = (int) (height * 10 / 100);
        $RefreshRateValue.setLayoutParams(txtrefreshValueParama);
        $HistoryValue.setLayoutParams(txtrefreshValueParama);
        $RefreshRateValue.setGravity(Gravity.CENTER | Gravity.RIGHT);
        $HistoryValue.setGravity(Gravity.CENTER | Gravity.RIGHT);
        $RefreshRateValue.setPadding(0, 0, width * 4 / 100, 0);
        $HistoryValue.setPadding(0, 0, width * 4 / 100, 0);

        LinearLayout.LayoutParams txtrefreshImageParama = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        txtrefreshImageParama.width = width * 10 / 100;
        txtrefreshImageParama.height = (int) (height * 6 / 100);
        txtrefreshImageParama.gravity = Gravity.CENTER;
        $RefreshDone.setLayoutParams(txtrefreshImageParama);
        $RefreshCanel.setLayoutParams(txtrefreshImageParama);
        $HistoryDone.setLayoutParams(txtrefreshImageParama);
        $HistoryCancel.setLayoutParams(txtrefreshImageParama);
        $RefreshDone.setPadding((int) (width * 1.5 / 100), (int) (height * 1.5 / 100),
                0, (int) (height * 1.5 / 100));
        $RefreshCanel.setPadding(0, (int) (height * 1.5 / 100),
                (int) (width * 1.5 / 100), (int) (height * 1.5 / 100));
        $HistoryDone.setPadding((int) (width * 1.5 / 100), (int) (height * 1.5 / 100),
                0, (int) (height * 1.5 / 100));
        $HistoryCancel.setPadding(0, (int) (height * 1.5 / 100),
                (int) (width * 1.5 / 100), (int) (height * 1.5 / 100));

        LinearLayout.LayoutParams dividerParama = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        dividerParama.width = width;
        dividerParama.height = (int) (height * 0.5 / 100);
        $Divider1.setLayoutParams(dividerParama);
        $Divider2.setLayoutParams(dividerParama);
        $Divider3.setLayoutParams(dividerParama);

        LinearLayout.LayoutParams notificationSettingsParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        notificationSettingsParams.width = width * 80 / 100;
        notificationSettingsParams.height = height * 8 / 100;
        mTxtNotificationsSettingLabel.setLayoutParams(notificationSettingsParams);
        mTxtNotificationsSettingLabel.setPadding(width * 4 / 100, 0, 0, 0);
        mTxtNotificationsSettingLabel.setGravity(Gravity.CENTER | Gravity.LEFT);


        mTxtDomainSettingLabel.setLayoutParams(notificationSettingsParams);
        mTxtDomainSettingLabel.setPadding(width * 4 / 100, 0, 0, 0);
        mTxtDomainSettingLabel.setGravity(Gravity.CENTER | Gravity.LEFT);

        mTxtScreenSetting.setLayoutParams(notificationSettingsParams);
        mTxtScreenSetting.setPadding(width * 4 / 100, 0, 0, 0);
        mTxtScreenSetting.setGravity(Gravity.CENTER | Gravity.LEFT);

        mTxtMapSetting.setLayoutParams(notificationSettingsParams);
        mTxtMapSetting.setPadding(width * 4 / 100, 0, 0, 0);
        mTxtMapSetting.setGravity(Gravity.CENTER | Gravity.LEFT);

        LinearLayout.LayoutParams ImeiParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        ImeiParams.width = width;
        ImeiParams.height = height * 5 / 100;
        mTxtImeiLabel.setLayoutParams(ImeiParams);
        mTxtImeiLabel.setPadding(width * 4 / 100, 0, 0, 0);
        mTxtImeiLabel.setGravity(Gravity.CENTER | Gravity.LEFT);

        mTxtImeiValue.setLayoutParams(ImeiParams);
        mTxtImeiValue.setPadding(width * 4 / 100, 0, 0, 0);
        mTxtImeiValue.setGravity(Gravity.CENTER | Gravity.LEFT);


        LinearLayout.LayoutParams notificationSettingsImgParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        notificationSettingsImgParams.width = width * 12 / 100;
        notificationSettingsImgParams.height = height * 8 / 100;
        notificationSettingsImgParams.gravity = Gravity.CENTER;
        mImgNotificationSettingsArrow.setLayoutParams(notificationSettingsImgParams);
        mImgNotificationSettingsArrow.setPadding(width * 1 / 100, height * 1 / 100,
                width * 1 / 100, height * 1 / 100);

        mImgDomainSettingsArrow.setLayoutParams(notificationSettingsImgParams);
        mImgDomainSettingsArrow.setPadding(width * 1 / 100, height * 1 / 100,
                width * 1 / 100, height * 1 / 100);


        LinearLayout.LayoutParams enableRefreshRateParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        enableRefreshRateParams.width = width;
        enableRefreshRateParams.height = (int) (height * 7 / 100);
        enableRefreshRateParams.setMargins(width * 1 / 100, width * 1 / 100, width * 1 / 100, width * 1 / 100);
        mEnableRefreshRateLayout.setLayoutParams(enableRefreshRateParams);
        mEditRefreshRateLayout.setLayoutParams(enableRefreshRateParams);
        mEditLiveTrackingRefreshRateLayout.setLayoutParams(enableRefreshRateParams);

        LinearLayout.LayoutParams enableChkBoxParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        enableChkBoxParams.width = width * 10 / 100;
        enableChkBoxParams.height = (int) (height * 7 / 100);
        mChkEnableRefreshRate.setLayoutParams(enableChkBoxParams);
        mChkEnableRefreshRate.setGravity(Gravity.CENTER);

        mImgRefreshRateDone.setLayoutParams(enableChkBoxParams);


        LinearLayout.LayoutParams enableChkBoxParams1 = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        enableChkBoxParams1.width = width * 5 / 100;
        enableChkBoxParams1.height = (int) (height * 5 / 100);
        enableChkBoxParams1.leftMargin = width * 3 / 100;
        mImgLiveTrackingRefreshRateDone.setLayoutParams(enableChkBoxParams1);
//        mImgLiveTrackingRefreshRateDone.setGravity(Gravity.CENTER);


//        mImgLiveTrackingRefreshRateDone.setLayoutParams(enableChkBoxParams);
//        mImgRefreshRateDone.setGra

        LinearLayout.LayoutParams enableRefreshTxtParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        enableRefreshTxtParams.width = width * 93 / 100;
        enableRefreshTxtParams.height = (int) (height * 7 / 100);
        mEnableRefreshRateTxtView.setLayoutParams(enableRefreshTxtParams);
        mEnableRefreshRateTxtView.setPadding(0, (int) (height * 1.5 / 100),
                (int) (width * 1.5 / 100), (int) (height * 1.5 / 100));
        mEnableRefreshRateTxtView.setGravity(Gravity.CENTER | Gravity.LEFT);

        LinearLayout.LayoutParams editRefreshEditTxtParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        editRefreshEditTxtParams.width = width * 15 / 100;
        editRefreshEditTxtParams.height = (int) (height * 7 / 100);
        mEditRefreshRate.setLayoutParams(editRefreshEditTxtParams);
        mEditRefreshRate.setGravity(Gravity.CENTER | Gravity.LEFT);

        mEditLiveTrackingRefreshRate.setLayoutParams(editRefreshEditTxtParams);
        mEditLiveTrackingRefreshRate.setGravity(Gravity.CENTER | Gravity.LEFT);

        LinearLayout.LayoutParams txtEditRefreshRAteParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        txtEditRefreshRAteParams.width = width * 70 / 100;
        txtEditRefreshRAteParams.height = (int) (height * 7 / 100);
        mEdtRefreshRateTxtView.setLayoutParams(txtEditRefreshRAteParams);
        mEdtRefreshRateTxtView.setPadding(0, (int) (height * 1.5 / 100),
                (int) (width * 1.5 / 100), (int) (height * 1.5 / 100));
        mEdtRefreshRateTxtView.setGravity(Gravity.CENTER | Gravity.LEFT);

        mEdtLiveTrackingRefreshRateTxtView.setLayoutParams(txtEditRefreshRAteParams);
        mEdtLiveTrackingRefreshRateTxtView.setPadding(width * 4 / 100, (int) (height * 1.5 / 100),
                (int) (width * 1.5 / 100), (int) (height * 1.5 / 100));
        mEdtLiveTrackingRefreshRateTxtView.setGravity(Gravity.CENTER | Gravity.LEFT);


        LinearLayout.LayoutParams switchParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        switchParams.width = width;
        switchParams.height = (int) (height * 7 / 100);
        switchParams.setMargins(width * 1 / 100, width * 1 / 100, width * 1 / 100, width * 1 / 100);
        mySwitch.setLayoutParams(switchParams);
        mySwitch.setGravity(Gravity.CENTER | Gravity.LEFT);
        mySwitch.setPadding(0, (int) (height * 1.5 / 100),
                (int) (width * 1.5 / 100), (int) (height * 1.5 / 100));


        LinearLayout.LayoutParams txtSupportLableParama = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        txtSupportLableParama.width = width;
        txtSupportLableParama.height = (int) (height * 6 / 100);
        $SupportLable.setLayoutParams(txtSupportLableParama);
        $SupportLable.setGravity(Gravity.CENTER | Gravity.LEFT);
        $SupportLable.setPadding(width * 4 / 100, 0, 0, 0);

        LinearLayout.LayoutParams txtSupportParama = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        txtSupportParama.width = width;
        //txtSupportParama.height = (int) (height * 5 / 100);
        txtSupportParama.topMargin = height * 1 / 100;
        $SupportAddress1.setLayoutParams(txtSupportParama);
        $SupportAddress2.setLayoutParams(txtSupportParama);
        $SupportMobile.setLayoutParams(txtSupportParama);

        $SupportAddress1.setGravity(Gravity.CENTER | Gravity.LEFT);
        $SupportAddress2.setGravity(Gravity.CENTER | Gravity.LEFT);
        $SupportMobile.setGravity(Gravity.CENTER | Gravity.LEFT);

        $SupportAddress1.setPadding(width * 4 / 100, 0, width * 2 / 100, 0);
        $SupportAddress2.setPadding(width * 4 / 100, 0, width * 2 / 100, 0);
        $SupportMobile.setPadding(width * 4 / 100, 0, width * 2 / 100, height * 3 / 100);


        LinearLayout.LayoutParams toogleParama = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        toogleParama.width = width * 27 / 100;
        toogleParama.height = height * 6 / 100;
        toogleParama.leftMargin = width * 31 / 100;
        toggle.setLayoutParams(toogleParama);
        toggle.setGravity(Gravity.RIGHT | Gravity.CENTER);
        toggle.setPadding((int) (width * 3.5 / 100), 0, 0, 0);

        toogle_3d_map.setLayoutParams(toogleParama);
        toogle_3d_map.setGravity(Gravity.RIGHT | Gravity.CENTER);
        toogle_3d_map.setPadding((int) (width * 3.5 / 100), 0, 0, 0);


        LinearLayout.LayoutParams txtSupportLableParama2 = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        txtSupportLableParama2.width = width;
//        txtSupportLableParama.height = (int) (height * 6 / 100);
        mTxtStartLiveTrackingHintText.setLayoutParams(txtSupportLableParama2);
        mTxtStartLiveTrackingHintText.setGravity(Gravity.CENTER | Gravity.LEFT);
        mTxtStartLiveTrackingHintText.setPadding(width * 4 / 100, 0, 0, 0);


        if (width >= 600) {
            $TxtTitle.setTextSize(18);
            $RefreshRateLable.setTextSize(16);
            $Divider1.setTextSize(16);
            $Divider2.setTextSize(16);
            $HistoryLable.setTextSize(16);
            mNotificationEnableLabel.setTextSize(16);
            $SupportLable.setTextSize(18);
            $SupportAddress1.setTextSize(16);
            $SupportAddress2.setTextSize(16);
            $SupportMobile.setTextSize(16);
            $HistoryEdit.setTextSize(16);
            $RefreshRateEdit.setTextSize(15);
            mySwitch.setTextSize(16);
            mTxt3Dmap.setTextSize(16);
            mTxtLiveTrackingLabel.setTextSize(16);
            mEdtRefreshRateTxtView.setTextSize(16);
            mEdtLiveTrackingRefreshRateTxtView.setTextSize(16);
            mEnableRefreshRateTxtView.setTextSize(16);

            mTxtNotificationsSettingLabel.setTextSize(16);
            mTxtDomainSettingLabel.setTextSize(16);

            mTxtScreenSetting.setTextSize(16);

            mRadioHome.setTextSize(16);
            mRadioVehicleList.setTextSize(16);
            mRadioDashboard.setTextSize(16);

            mTxtMapSetting.setTextSize(16);
            mRadioOsm.setTextSize(16);
            mRadioGoogle.setTextSize(16);


            mTxtImeiLabel.setTextSize(16);
            mTxtImeiValue.setTextSize(16);

        } else if (width > 501 && width < 600) {
            $TxtTitle.setTextSize(17);
            $RefreshRateLable.setTextSize(15);
            $Divider1.setTextSize(15);
            $Divider2.setTextSize(15);
            $HistoryLable.setTextSize(15);
            mNotificationEnableLabel.setTextSize(15);
            $SupportLable.setTextSize(17);
            $SupportAddress1.setTextSize(15);
            $SupportAddress2.setTextSize(15);
            $SupportMobile.setTextSize(15);
            $HistoryEdit.setTextSize(15);
            $RefreshRateEdit.setTextSize(15);
            mEdtRefreshRateTxtView.setTextSize(15);
            mEdtLiveTrackingRefreshRateTxtView.setTextSize(15);
            mEnableRefreshRateTxtView.setTextSize(15);
            mySwitch.setTextSize(15);
            mTxt3Dmap.setTextSize(15);
            mTxtLiveTrackingLabel.setTextSize(15);
            mTxtNotificationsSettingLabel.setTextSize(15);
            mTxtDomainSettingLabel.setTextSize(15);

            mTxtScreenSetting.setTextSize(15);
            mRadioHome.setTextSize(15);
            mRadioVehicleList.setTextSize(15);
            mRadioDashboard.setTextSize(15);

            mTxtMapSetting.setTextSize(15);
            mRadioOsm.setTextSize(15);
            mRadioGoogle.setTextSize(15);


            mTxtImeiLabel.setTextSize(15);
            mTxtImeiValue.setTextSize(15);

        } else if (width > 260 && width < 500) {
            $TxtTitle.setTextSize(16);
            $RefreshRateLable.setTextSize(14);
            $Divider1.setTextSize(14);
            $Divider2.setTextSize(14);
            $HistoryLable.setTextSize(14);
            mNotificationEnableLabel.setTextSize(14);
            $SupportLable.setTextSize(16);
            $SupportAddress1.setTextSize(14);
            $SupportAddress2.setTextSize(14);
            $SupportMobile.setTextSize(14);
            $HistoryEdit.setTextSize(14);
            $RefreshRateEdit.setTextSize(14);
            mySwitch.setTextSize(14);
            mTxt3Dmap.setTextSize(14);
            mTxtLiveTrackingLabel.setTextSize(14);
            mEdtRefreshRateTxtView.setTextSize(14);
            mEdtLiveTrackingRefreshRateTxtView.setTextSize(14);
            mEnableRefreshRateTxtView.setTextSize(14);

            mTxtNotificationsSettingLabel.setTextSize(14);
            mTxtDomainSettingLabel.setTextSize(14);

            mTxtScreenSetting.setTextSize(14);
            mRadioHome.setTextSize(14);
            mRadioVehicleList.setTextSize(14);
            mRadioDashboard.setTextSize(14);

            mTxtMapSetting.setTextSize(14);
            mRadioOsm.setTextSize(14);
            mRadioGoogle.setTextSize(14);


            mTxtImeiLabel.setTextSize(14);
            mTxtImeiValue.setTextSize(14);

        } else if (width <= 260) {
            $TxtTitle.setTextSize(15);
            $RefreshRateLable.setTextSize(13);
            $Divider1.setTextSize(13);
            $Divider2.setTextSize(13);
            $HistoryLable.setTextSize(13);
            mNotificationEnableLabel.setTextSize(13);
            $SupportLable.setTextSize(15);
            $SupportAddress1.setTextSize(13);
            $SupportAddress2.setTextSize(13);
            $SupportMobile.setTextSize(13);
            $HistoryEdit.setTextSize(13);
            $RefreshRateEdit.setTextSize(13);
            mySwitch.setTextSize(13);
            mTxt3Dmap.setTextSize(13);
            mTxtLiveTrackingLabel.setTextSize(13);
            mEdtRefreshRateTxtView.setTextSize(13);
            mEdtLiveTrackingRefreshRateTxtView.setTextSize(13);
            mEnableRefreshRateTxtView.setTextSize(13);

            mTxtNotificationsSettingLabel.setTextSize(13);
            mTxtDomainSettingLabel.setTextSize(13);

            mTxtScreenSetting.setTextSize(13);
            mRadioHome.setTextSize(13);
            mRadioVehicleList.setTextSize(13);
            mRadioDashboard.setTextSize(13);

            mTxtMapSetting.setTextSize(13);
            mRadioOsm.setTextSize(13);
            mRadioGoogle.setTextSize(13);


            mTxtImeiLabel.setTextSize(13);
            mTxtImeiValue.setTextSize(13);

        }
    }

    public static boolean checkLocationPermission(final Context context) {
        return ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED;
    }

    public static boolean checkPhonePermission(final Context context) {
        return ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED;

    }

    public static boolean checkR3a4S4Ca64P3rm1ss10n(final Context context) {
        return ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED;

    }

    public static boolean checkW6173S4Ca64P3rm1ss10n(final Context context) {
        return ActivityCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED;

    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }
}
