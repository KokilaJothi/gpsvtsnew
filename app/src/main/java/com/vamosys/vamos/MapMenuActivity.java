package com.vamosys.vamos;


import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import androidx.appcompat.app.AppCompatActivity;
import android.text.InputType;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;


import com.bluecam.demo.CameraActivity;
import com.vamosys.utils.ConnectionDetector;
import com.vamosys.utils.Constant;
import com.vamosys.utils.HttpConfig;
import com.vamosys.utils.MyCustomProgressDialog;
import com.vamosys.utils.TypefaceUtil;
import com.vamosys.vamos.fmsData.FmsActivity;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.regex.Pattern;

//import com.vamosys.services.MessageReceiveService;

public class MapMenuActivity extends AppCompatActivity implements OnClickListener {

    int width, height;
    ImageView $ImgMap, $ImgInfo, $ImgLocate, $ImgReport, $ImgHistroy,$ImgCameraView,
            $ImgMapArrow, $ImgInfoArrow, $ImgLocateArrow, $ImgReportArrow,$ImgCameraVideoArrow,
            $ImgHistroyArrow, $BackArrow, $ImgLiveTracking, $ImgLiveTrackingArrow, $ImgNearBy, $ImgNearByArrow, $ImgShowImage, $ImgShowImageArrow,
            $ImgTripSummary, $ImgTripSummaryArrow, $ImgDriverDetails, $ImgDriverDetailsArrow, $ImgDoorLock, $ImgDoorLockArrow,
            $ImgFuelFill, $ImgFuelFillArrow, $ImgTemperature,$ImgTollgate, $ImgTemperatureArrow,$ImgTollgateArrow, $ImgAlarm, $ImgAlarmArrow, $ImgSiteAnalysis, $ImgSiteAnalysisArrow,
            $ImggetUrl, $ImgChangeName, $ImggetUrlArrow, $ImgChangeNameArrow, $ImgCamera, $ImgCameraArrow, $ImgCameraIcon, $ImgCameraViewArrow,$ImgSiteTrip, $ImgSiteTripArrow, $ImgStoppage,
            $ImgStoppageArrow, $ImgAcReport, $ImgAcReportArrow, $ImgSafeParking, $ImgFms, $ImgFmsArrow, $ImgPrimaryEngine, $ImgPrimaryEngineArrow,
            $ImgSecondaryEngine, $ImgSecondaryEngineArrow, $ImgFuelTheft, $ImgFuelTheftArrow,$ImgFuelsumm, $ImgFuelsummArrow;
    TextView $TextCamera_video,$TextMap, $TextInfo, $TextLocate, $TextReport, $TextHistroy, $TxtTitle, $txtLiveTracking, $txtNearBy, $txtShowImage, $txtTripSummary,
            $txtDriverDetails, $txtDoorLock, $txtFuelFill, $txtTemperature,$txtTollgate, $txtAlarmReport, $txtSiteAnalysisReport, $txtgetUrl, $txtChangeName,
            $txtCameraReport,$txtCameraViewReport, $txtSiteTripReport, $txtStoppageReport, $txtAcReport, $txtSafeParking, $txtFms, $txtPrimaryEngine, $txtSecondaryEngine, $txtFuelTheft, $txtFuelsumma;
    LinearLayout $LayoutCamera_video,$LayoutMap, $LayoutInfo, $LayoutLocate, $LayoutReport, $LayoutFms,
            $LayoutHistroy, $LayoutLiveTracking, $LayoutNearBy, $LayoutShowImage, $LayoutTripSummary, $LayoutDriverDetails, $LayoutDoorLock,
            $LayoutFuelFill, $LayoutTemperature,$LayoutTollgate, $LayoutAlarm, $LayoutSiteAnalysis, $LayoutGetUrl, $LayoutChangeName, $LayoutCamera,$LayoutCameraView,
            $LayoutSiteTripReport, $LayoutStoppage, $LayoutAc, $LayoutSafeParking, $LayoutPrimaryEngine, $LayoutSecondaryEngine, $LayoutFuelTheft, $LayoutFuelsummary;
    View $View1, $View2, $View3, $View4, $View5, $View6, $View7, $View8, $View9, $View10, $View11, $View12, $View13, $View14, $View15, $View16,$View26, $View17, $View18, $View19, $View20, $View21, $View22, $View23, $View24, $View25;
    Switch mEnableSafeParkingSwitch;
    Switch switchbutton;
    ProgressDialog progressDialog;
    int lock_unlock_timeout = 5000;
    String mTO_MOBILE_NUMBER, mSMS_CONTENT;

    String[] time_array = {"Days", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "1 Month", "3 Month", "1 Year"};
    String mTimeSpinnerSelectedValue = null;
    ConnectionDetector cd;
    SharedPreferences sp;

    private Timer myTimer;
    String data;
    JSONObject jsonObject;
    Dialog a_dialog, a_dialog1;
    String enable = "yes";
    String disable = "no";
    String camid,camname,usename,pwd,shorname,cameratype;

    boolean isOsmEnabled = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.map_menu_layout);
        intialization();
        screenArrange();
        cd = new ConnectionDetector(getApplicationContext());
        sp = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        if (sp.getString("ip_adds", "") != null) {
            if (sp.getString("ip_adds", "").trim().length() > 0) {
                Const.API_URL = sp.getString("ip_adds", "");
            }
        }

        if (sp.getString("enabled_map", "") != null) {
            if (sp.getString("enabled_map", "").trim().length() > 0) {

                System.out.println("hi enabled map is " + sp.getString("enabled_map", ""));

                if (sp.getString("enabled_map", "").equalsIgnoreCase(getResources().getString(R.string.osm))) {
                    isOsmEnabled = true;
                } else {
                    isOsmEnabled = false;
                }
            }
        }


        if (Constant.SELECTED_VEHICLE_LOCATION_OBJECT != null) {

            // System.out.println("Hi vehicle short name is " + Constant.SELECTED_VEHICLE_LOCATION_OBJECT.getShortName() + " :: " + Constant.SELECTED_VEHICLE_LOCATION_OBJECT.getVehicleId());
            // System.out.println("Hi vehicle short name is 1111" + Constant.SELECTED_VEHICLE_ID + " :: " + Constant.SELECTED_USER_ID);

            $TxtTitle.setText(Constant.SELECTED_VEHICLE_LOCATION_OBJECT.getShortName());
        } else {
            $TxtTitle.setText("Menu");
        }
        viewstatus();

        String drivercamera = Constant.SELECTED_DRIVER_NAME;
        if (drivercamera !=null) {
            if (drivercamera.length() > 20) {
                Log.d("SELECTED_DRIVER", "" + Constant.SELECTED_DRIVER_NAME);
                $LayoutCameraView.setVisibility(View.VISIBLE);
                $View26.setVisibility(View.VISIBLE);
                List<String> values = Arrays.asList(drivercamera.split(","));
                Log.d("driverarray", "" + values);
                for (int i=0;i<values.size();i++){
                    camid = values.get(0);
                    Log.d("drivercamid",""+camid);
                    switch (i){

                        case 0:
                            Log.d("value",""+i+" "+values.get(i));
                            camid = values.get(i);
                            break;
                        case 1:
                            Log.d("value",""+i+" "+values.get(i));
                            camname = values.get(i);
                            break;
                        case 2:
                            Log.d("value",""+i+" "+values.get(i));
                            usename = values.get(i);
                            break;
                        case 3:
                            Log.d("value",""+i+" "+values.get(i));
                            pwd = values.get(i);
                            break;
                        case 4:
                            Log.d("value",""+i+" "+values.get(i));
                            shorname = values.get(i);
                            break;
                        case 5:
                            Log.d("value",""+i+" "+values.get(i));
                            cameratype = values.get(i);
                            break;
//
//                        case 6:
//                            Log.d("value",""+i+" "+values.get(i));
//                            camid = values.get(i);
//                            break;
//                        case 7:
//                            Log.d("value",""+i+" "+values.get(i));
//                            camname = values.get(i);
//                            break;
//                        case 8:
//                            Log.d("value",""+i+" "+values.get(i));
//                            usename = values.get(i);
//                            break;
//                        case 9:
//                            Log.d("value",""+i+" "+values.get(i));
//                            pwd = values.get(i);
//                            break;
//                        case 10:
//                            Log.d("value",""+i+" "+values.get(i));
//                            shorname = values.get(i);
//                            break;
//                        case 11:
//                            Log.d("value",""+i+" "+values.get(i));
//                            cameratype = values.get(i);
//                            break;
//                        case 12:
//                            Log.d("value",""+i+" "+values.get(i));
//                            camid = values.get(i);
//                            break;
//                        case 13:
//                            Log.d("value",""+i+" "+values.get(i));
//                            camname = values.get(i);
//                            break;
//                        case 14:
//                            Log.d("value",""+i+" "+values.get(i));
//                            usename = values.get(i);
//                            break;
//                        case 15:
//                            Log.d("value",""+i+" "+values.get(i));
//                            pwd = values.get(i);
//                            break;
//                        case 16:
//                            Log.d("value",""+i+" "+values.get(i));
//                            shorname = values.get(i);
//                            break;
//                        case 17:
//                            Log.d("value",""+i+" "+values.get(i));
//                            cameratype = values.get(i);
//                            break;
                    }

                }

            } else {
                $LayoutCameraView.setVisibility(View.GONE);
                $View26.setVisibility(View.GONE);
            }
        }

    }

    private void intialization() {
        // TODO Auto-generated method stub

        $ImgMap = (ImageView) findViewById(R.id.test_mapIcon);
        $ImgInfo = (ImageView) findViewById(R.id.test_infoIcon);
        $ImgLocate = (ImageView) findViewById(R.id.test_lockIcon);
        $ImgReport = (ImageView) findViewById(R.id.test_reportIcon);
        $ImgHistroy = (ImageView) findViewById(R.id.test_historyIcon);
        $ImgMapArrow = (ImageView) findViewById(R.id.test_mapIconRight);
        $ImgInfoArrow = (ImageView) findViewById(R.id.test_InfoIconRight);
        $ImgLocateArrow = (ImageView) findViewById(R.id.test_lockIconRight);
        $ImgReportArrow = (ImageView) findViewById(R.id.test_reportIconRight);
        $ImgHistroyArrow = (ImageView) findViewById(R.id.test_historyIconRight);
//        $ImgCameraView = (ImageView) findViewById(R.id.camera_view_Icon);
//        $ImgCameraVideoArrow=(ImageView) findViewById(R.id.)

        $ImgLiveTrackingArrow = (ImageView) findViewById(R.id.test_trackingIconRight);
        $ImgLiveTracking = (ImageView) findViewById(R.id.test_trackingIcon);

        $ImgNearBy = (ImageView) findViewById(R.id.test_nearbyIcon);
        $ImgNearByArrow = (ImageView) findViewById(R.id.test_nearbyIconRight);
        $ImgShowImage = (ImageView) findViewById(R.id.test_imageIcon);
        $ImgShowImageArrow = (ImageView) findViewById(R.id.test_imageIconRight);

        $ImgTripSummary = (ImageView) findViewById(R.id.test_TripSummary_Icon);
        $ImgTripSummaryArrow = (ImageView) findViewById(R.id.test_TripSummary_IconRight);

        $ImgDriverDetails = (ImageView) findViewById(R.id.test_DriverDetails_Icon);
        $ImgDriverDetailsArrow = (ImageView) findViewById(R.id.test_DriverDetails_IconRight);

        $ImgFuelFill = (ImageView) findViewById(R.id.test_FuelFill_Icon);
        $ImgFuelFillArrow = (ImageView) findViewById(R.id.test_FuelFill_IconRight);
        $ImgTemperature = (ImageView) findViewById(R.id.test_Temperature_Icon);
        $ImgTemperatureArrow = (ImageView) findViewById(R.id.test_Temperature_IconRight);

        $ImgAlarm = (ImageView) findViewById(R.id.test_alarm_report_Icon);
        $ImgAlarmArrow = (ImageView) findViewById(R.id.test_alarm_report_IconRight);

        $ImgSiteAnalysis = (ImageView) findViewById(R.id.test_site_analysis_report_Icon);
        $ImgSiteAnalysisArrow = (ImageView) findViewById(R.id.test_site_analysis_report_IconRight);

        $ImgChangeName = (ImageView) findViewById(R.id.test_change_vehicle_name_Icon);
        $ImgChangeNameArrow = (ImageView) findViewById(R.id.test_change_vehicle_name_IconRight);

        $ImggetUrl = (ImageView) findViewById(R.id.test_get_url__Icon);
        $ImggetUrlArrow = (ImageView) findViewById(R.id.test_get_url_IconRight);

        $ImgCamera = (ImageView) findViewById(R.id.menu_camera_Icon);
        $ImgCameraArrow = (ImageView) findViewById(R.id.menu_camera_IconRight);
        $ImgCameraIcon = (ImageView) findViewById(R.id.menu_cameraview_Icon);
        $ImgCameraViewArrow = (ImageView) findViewById(R.id.menu_cameraview_IconRight);

        $ImgSiteTrip = (ImageView) findViewById(R.id.test_site_trip_report_Icon);
        $ImgSiteTripArrow = (ImageView) findViewById(R.id.test_site_trip_report_IconRight);

        $ImgStoppage = (ImageView) findViewById(R.id.test_stoppage_report_Icon);
        $ImgStoppageArrow = (ImageView) findViewById(R.id.test_stoppage_report_IconRight);

        $ImgAcReport = (ImageView) findViewById(R.id.test_ac_report_Icon);
        $ImgAcReportArrow = (ImageView) findViewById(R.id.test_ac_report_IconRight);

        $ImgFms = (ImageView) findViewById(R.id.test_fms_Icon);
        $ImgFmsArrow = (ImageView) findViewById(R.id.test_fms_IconRight);

        $ImgPrimaryEngine = (ImageView) findViewById(R.id.test_primary_engine_Icon);
        $ImgPrimaryEngineArrow = (ImageView) findViewById(R.id.test_primary_engine_IconRight);

        $ImgSecondaryEngine = (ImageView) findViewById(R.id.test_secondary_engine_Icon);
        $ImgSecondaryEngineArrow = (ImageView) findViewById(R.id.test_secondary_engine_IconRight);

        $ImgFuelTheft = (ImageView) findViewById(R.id.test_FuelTheft_Icon);
        $ImgFuelTheftArrow = (ImageView) findViewById(R.id.test_FuelTheft_IconRight);


//        $ImgFuelsumm = (ImageView) findViewById(R.id.test_Fuelsumm_Icon);
//        $ImgFuelsummArrow = (ImageView) findViewById(R.id.test_Fuelsumm_IconRight);

        $BackArrow = (ImageView) findViewById(R.id.test_view_Back);
        switchbutton = (Switch) findViewById(R.id.alaramswitch);
        switchbutton.setOnCheckedChangeListener(toggleButtonChangeListenerNew);

        $TxtTitle = (TextView) findViewById(R.id.test_title);
        $TextMap = (TextView) findViewById(R.id.test_mapTxt);
        $TextInfo = (TextView) findViewById(R.id.test_infoTxt);
        $TextLocate = (TextView) findViewById(R.id.test_lockTxt);
        $TextReport = (TextView) findViewById(R.id.test_reportTxt);
        $TextHistroy = (TextView) findViewById(R.id.test_historyTxt);
        $txtLiveTracking = (TextView) findViewById(R.id.test_trackingTxt);
        $txtNearBy = (TextView) findViewById(R.id.test_nearbyTxt);
        $txtShowImage = (TextView) findViewById(R.id.test_imageTxt);
        $txtTripSummary = (TextView) findViewById(R.id.test_TripSummaryTxt);
        $txtDriverDetails = (TextView) findViewById(R.id.test_DriverDetailsTxt);
        $txtDriverDetails = (TextView) findViewById(R.id.test_DriverDetailsTxt);
        $txtFuelFill = (TextView) findViewById(R.id.test_FuelFillTxt);
        $txtFuelTheft = (TextView) findViewById(R.id.test_FuelTheftTxt);
        //$txtFuelsumma = (TextView) findViewById(R.id.test_FuelsummTxt);
        $txtTemperature = (TextView) findViewById(R.id.test_TemperatureTxt);
        $txtAlarmReport = (TextView) findViewById(R.id.test_alarm_report_Txt);
        $txtSiteAnalysisReport = (TextView) findViewById(R.id.test_site_analysis_report_Txt);
        $txtgetUrl = (TextView) findViewById(R.id.test_get_url_Txt);
        $txtChangeName = (TextView) findViewById(R.id.test_change_vehicle_name_Txt);
        $txtCameraReport = (TextView) findViewById(R.id.menu_cameraTxt);
        $txtCameraViewReport = (TextView) findViewById(R.id.menu_cameraviewTxt);
        $txtSiteTripReport = (TextView) findViewById(R.id.test_site_trip_report_Txt);
        $txtStoppageReport = (TextView) findViewById(R.id.test_stoppage_report_Txt);
        $txtAcReport = (TextView) findViewById(R.id.test_ac_report_Txt);
        $txtFms = (TextView) findViewById(R.id.test_fms_Txt);

        $LayoutMap = (LinearLayout) findViewById(R.id.test_mapLayout);
        $LayoutInfo = (LinearLayout) findViewById(R.id.test_infoLayout);
        $LayoutLocate = (LinearLayout) findViewById(R.id.test_lockLayout);
        $LayoutReport = (LinearLayout) findViewById(R.id.test_reportLayout);
        $LayoutHistroy = (LinearLayout) findViewById(R.id.test_historyLayout);
        $LayoutLiveTracking = (LinearLayout) findViewById(R.id.test_trackingLayout);
        $LayoutNearBy = (LinearLayout) findViewById(R.id.test_nearbyLayout);
        $LayoutShowImage = (LinearLayout) findViewById(R.id.test_imageLayout);


        $LayoutTripSummary = (LinearLayout) findViewById(R.id.test_TripSummary_Layout);
        $LayoutDriverDetails = (LinearLayout) findViewById(R.id.test_DriverDetails_Layout);


        $LayoutFuelFill = (LinearLayout) findViewById(R.id.test_FuelFill_Layout);
        $LayoutFuelTheft = (LinearLayout) findViewById(R.id.test_FuelTheft_Layout);
        //$LayoutFuelsummary = (LinearLayout) findViewById(R.id.test_Fuelsumm_Layout);
        $LayoutTemperature = (LinearLayout) findViewById(R.id.test_Temperature_Layout);
        $LayoutAlarm = (LinearLayout) findViewById(R.id.test_alarm_report_Layout);
        $LayoutSiteAnalysis = (LinearLayout) findViewById(R.id.test_site_analysis_report_Layout);
        $LayoutGetUrl = (LinearLayout) findViewById(R.id.test_get_url_Layout);
        $LayoutChangeName = (LinearLayout) findViewById(R.id.test_change_vehicle_name_Layout);
        $LayoutCamera = (LinearLayout) findViewById(R.id.menu_camera_Layout);
        $LayoutCameraView = (LinearLayout) findViewById(R.id.menu_cameraview_Layout);
        $LayoutSiteTripReport = (LinearLayout) findViewById(R.id.test_site_trip_report_Layout);
        $LayoutStoppage = (LinearLayout) findViewById(R.id.test_stoppage_report_Layout);
        $LayoutAc = (LinearLayout) findViewById(R.id.test_ac_report_Layout);
        $LayoutFms = (LinearLayout) findViewById(R.id.test_fms_Layout);
        $LayoutPrimaryEngine = (LinearLayout) findViewById(R.id.test_primary_engine_Layout);
        $LayoutSecondaryEngine = (LinearLayout) findViewById(R.id.test_secondary_engine_Layout);

        $ImgSafeParking = (ImageView) findViewById(R.id.img_safe_parking);
        $txtSafeParking = (TextView) findViewById(R.id.txt_safe_parking);
        $LayoutSafeParking = (LinearLayout) findViewById(R.id.layout_safe_parking);
        mEnableSafeParkingSwitch = (Switch) findViewById(R.id.switchButton_enable_safe_parking);
        mEnableSafeParkingSwitch.setOnCheckedChangeListener(toggleButtonChangeListener);

        $txtPrimaryEngine = (TextView) findViewById(R.id.test_primary_engine_Txt);
        $txtSecondaryEngine = (TextView) findViewById(R.id.test_secondary_engine_Txt);

        $View20 = (View) findViewById(R.id.test_view_safe_parking);

        $View1 = (View) findViewById(R.id.test_viewRight);
        $View2 = (View) findViewById(R.id.test_viewInfo);
        $View3 = (View) findViewById(R.id.test_viewlock);
        $View4 = (View) findViewById(R.id.test_viewReport);
        $View5 = (View) findViewById(R.id.test_viewHistory);
        $View6 = (View) findViewById(R.id.test_viewLiveTrack);
        $View7 = (View) findViewById(R.id.test_viewNearBy);
        $View8 = (View) findViewById(R.id.test_viewShowImage);
        $View9 = (View) findViewById(R.id.test_viewTripSummary);


        $View11 = (View) findViewById(R.id.test_viewFuelFill);
        $View12 = (View) findViewById(R.id.test_viewTemperature);
        $View13 = (View) findViewById(R.id.test_view_alarm_report);
        $View14 = (View) findViewById(R.id.test_view_site_analysis_report);
        $View15 = (View) findViewById(R.id.test_view_get_url_);
        $View15.setVisibility(View.GONE);
        $View16 = (View) findViewById(R.id.menu_cameraView);
        $View26 = (View) findViewById(R.id.menu_cameraViewline);
        $View17 = (View) findViewById(R.id.test_view_site_trip_report);
        $View18 = (View) findViewById(R.id.test_view_sstoppage_report);
        $View19 = (View) findViewById(R.id.test_view_ac_report);
        $View21 = (View) findViewById(R.id.test_view_fms);

        $View22 = (View) findViewById(R.id.test_view_primary_engine);
        $View23 = (View) findViewById(R.id.test_view_secondary_engine);
        $View24 = (View) findViewById(R.id.test_viewFuelTheft);
        //$View25 = (View) findViewById(R.id.test_viewFuelsumm);

        $LayoutMap.setOnClickListener(this);
        $LayoutInfo.setOnClickListener(this);
        $LayoutLocate.setOnClickListener(this);
        $LayoutReport.setOnClickListener(this);
        $LayoutHistroy.setOnClickListener(this);
        $LayoutLiveTracking.setOnClickListener(this);
        $LayoutNearBy.setOnClickListener(this);
        $LayoutShowImage.setOnClickListener(this);
        $LayoutTripSummary.setOnClickListener(this);

        $LayoutFuelFill.setOnClickListener(this);
        $LayoutFuelTheft.setOnClickListener(this);
        // $LayoutFuelsummary.setOnClickListener(this);
        $LayoutTemperature.setOnClickListener(this);
        $LayoutAlarm.setOnClickListener(this);
        $LayoutSiteAnalysis.setOnClickListener(this);
        $LayoutGetUrl.setOnClickListener(this);
        $LayoutChangeName.setOnClickListener(this);
        $LayoutCamera.setOnClickListener(this);
        $LayoutCameraView.setOnClickListener(this);
        $LayoutSiteTripReport.setOnClickListener(this);
        $LayoutStoppage.setOnClickListener(this);
        $LayoutAc.setOnClickListener(this);
        $LayoutFms.setOnClickListener(this);
        $LayoutPrimaryEngine.setOnClickListener(this);
        $LayoutSecondaryEngine.setOnClickListener(this);
        $BackArrow.setOnClickListener(this);

        switchbutton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean bchecked) {
                if (bchecked) {
                    new gpsalaramData().execute(enable);
                } else {
                    new gpsalaramData().execute(disable);

                }
            }
        });

        $LayoutDriverDetails.setOnClickListener(this);

        if (isValidMobile(Constant.SELECTED_DRIVER_CONTACT_NUMBER)) {
            $LayoutDriverDetails.setVisibility(View.VISIBLE);
            $View15.setVisibility(View.VISIBLE);
        } else {
            $LayoutDriverDetails.setVisibility(View.GONE);
            $View15.setVisibility(View.VISIBLE);
//            $View21.setVisibility(View.GONE);
        }
    }

    private void viewstatus() {
        new buttonstatus().execute("");
        new reportrestrict().execute("");
    }

    private boolean isValidMobile(String phone2) {
        boolean check = false;
        if (phone2 != null) {
            if (!Pattern.matches("[a-zA-Z]+", phone2)) {
                if (phone2.length() < 6 || phone2.length() > 13) {
                    check = false;
//                txtPhone.setError("Not Valid Number");
                } else {
                    check = true;
                }
            } else {
                check = false;
            }
        }
        return check;
    }


    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub
        switch (v.getId()) {
            case R.id.test_mapLayout:
                startActivity(new Intent(MapMenuActivity.this, MapViewActivity.class));
                finish();
                break;
            case R.id.test_infoLayout:
                startActivity(new Intent(MapMenuActivity.this, NewVehicleInfoActivity.class));
//                startActivity(new Intent(MapMenuActivity.this, HistoryInfoTest.class));
                finish();

                break;
            case R.id.test_lockLayout:

                //Lock/Unlock
                AlertDialog.Builder builder = new AlertDialog.Builder(MapMenuActivity.this, R.style.MyAlertDialogStyle);
                builder.setTitle("CAUTION:");
                builder.setMessage("We assume that you understand the risk associated with issuing this command. The command will send SMS from your phone to immobilise/mobilise the vehicle, please use this command with caution.\n" +
                        "\n" +
                        "To continue, please press yes.").setCancelable(false).setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //do things
                        dialog.cancel();
                        showServerRSms();
                    }
                }).setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int i) {
                        dialog.cancel();
                    }
                });
                AlertDialog alert = builder.create();
                alert.show();

                break;
            case R.id.test_reportLayout:
                startActivity(new Intent(MapMenuActivity.this, ReportActivity.class));
                finish();

                break;
            case R.id.test_historyLayout:
                //  Constant.SELECTED_VEHICLE_ID


                if (isOsmEnabled) {
                    startActivity(new Intent(MapMenuActivity.this, HistoryNewActivity.class));
                    finish();
                } else {
                    startActivity(new Intent(MapMenuActivity.this, HistoryNewGoogleMapActivity.class));
                    finish();
                }

                break;
            case R.id.test_view_Back:
                startActivity(new Intent(MapMenuActivity.this, VehicleListActivity.class));
                finish();

                break;
            case R.id.test_trackingLayout:


                if (isOsmEnabled) {
                    startActivity(new Intent(MapMenuActivity.this, VehicleTrackingActivity.class));
                    finish();
                } else {
                    startActivity(new Intent(MapMenuActivity.this, VehicleTrackingGoogleMapActivity.class));
                    finish();
                }

                break;

            case R.id.test_nearbyLayout:
                startActivity(new Intent(MapMenuActivity.this, NearByVehicleListActivity.class));
                finish();

                break;
            case R.id.test_imageLayout:
//               Toast.makeText(getApplicationContext(),"")
                break;
            case R.id.test_TripSummary_Layout:
//               Toast.makeText(getApplicationContext(),"")
                startActivity(new Intent(MapMenuActivity.this, TripSummaryActivity.class));
                finish();
                break;
            case R.id.test_DriverDetails_Layout:
//               Toast.makeText(getApplicationContext(),"")
                startActivity(new Intent(MapMenuActivity.this, DriverDetails.class));
                finish();
                break;

            case R.id.test_FuelFill_Layout:
//               Toast.makeText(getApplicationContext(),"")
//                startActivity(new Intent(MapMenuActivity.this, FuelFillReport.class));

                Constant.mFuelStoredFromDate = null;
                Constant.mFuelStoredToDate = null;
                Constant.mFuelStoredFromTime = null;
                Constant.mFuelStoredToTime = null;
                Constant.mFuelInterval = null;
                Constant.mFuelIsChartView = false;
                if(Constant.SELECTED_VEHICLE_LOCATION_OBJECT.getFuel().contains("yes")) {
                    if (Constant.SELECTED_VEHICLE_LOCATION_OBJECT.getNoOfTank() > 1) {
                        startActivity(new Intent(MapMenuActivity.this, FuelFillReportSensorActivity.class));
                        finish();
                    } else {
                        startActivity(new Intent(MapMenuActivity.this, FuelFillNewReport.class));
                        finish();
                    }
                }
                else {
                    Toast.makeText(this, "Vehicle doesn't have fuel sensor", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.test_Temperature_Layout:
//               Toast.makeText(getApplicationContext(),"")
                startActivity(new Intent(MapMenuActivity.this, TemperatureReportActivity.class));
                finish();
                break;
            case R.id.test_alarm_report_Layout:
//               Toast.makeText(getApplicationContext(),"")
                startActivity(new Intent(MapMenuActivity.this, AlarmActivity.class));
                finish();
                break;
            case R.id.test_site_analysis_report_Layout:
//               Toast.makeText(getApplicationContext(),"")
                startActivity(new Intent(MapMenuActivity.this, SiteAnalysisReport.class));
                finish();
                break;
            case R.id.test_get_url_Layout:
                showGetUrlDialog();
                break;
            case R.id.test_change_vehicle_name_Layout:
                showVehicleNameChangeDialog();
                break;
            case R.id.menu_camera_Layout:
                startActivity(new Intent(MapMenuActivity.this, CameraReportIamgeListNewActivtiy.class));
//                Intent intent=new Intent(MapMenuActivity.this, CameraViewActivity.class);
//                intent.putExtra("cameraJsonString",getJsonData());
//                startActivity(intent);
//                finish();
                break;
            case R.id.menu_cameraview_Layout:

//                Intent intent=new Intent(MapMenuActivity.this, CameraActivity.class);
//                intent.putExtra("cameraJsonString",getJsonData());
//                startActivity(intent);
//                finish();
                break;
            case R.id.test_site_trip_report_Layout:
                startActivity(new Intent(MapMenuActivity.this, SiteTripReport.class));
                finish();
                break;
            case R.id.test_stoppage_report_Layout:
                startActivity(new Intent(MapMenuActivity.this, StoppageReport.class));
                finish();
                break;
            case R.id.test_ac_report_Layout:
//                startActivity(new Intent(MapMenuActivity.this, AcReport.class));
//                finish();


                startActivity(new Intent(MapMenuActivity.this, AcReportNewActivity.class));
                finish();
                break;

            case R.id.test_fms_Layout:
                startActivity(new Intent(MapMenuActivity.this, FmsActivity.class));
                finish();
                break;

            case R.id.test_primary_engine_Layout:
//                startActivity(new Intent(MapMenuActivity.this, AcReport.class));
//                finish();


                startActivity(new Intent(MapMenuActivity.this, PrimaryEngineOnReportActivity.class));
                finish();
                break;

            case R.id.test_secondary_engine_Layout:
//                startActivity(new Intent(MapMenuActivity.this, AcReport.class));
//                finish();


                startActivity(new Intent(MapMenuActivity.this, SecondaryEngineOnReportActivity.class));
                finish();
                break;


            case R.id.test_FuelTheft_Layout:
//               Toast.makeText(getApplicationContext(),"")

                if(Constant.SELECTED_VEHICLE_LOCATION_OBJECT.getFuel().contains("yes")) {
                    if(Constant.SELECTED_VEHICLE_LOCATION_OBJECT.getSensorCount()>1){
                        startActivity(new Intent(MapMenuActivity.this, FuelTheftNewReport.class));
                        finish();
                    }
                    else {
                        startActivity(new Intent(MapMenuActivity.this, FuelTheftReportActivity.class));
                        finish();
                    }
                }
                else {
                    Toast.makeText(this, "Vehicle doesn't have fuel sensor", Toast.LENGTH_SHORT).show();
                }

                break;
//            case R.id.test_Fuelsumm_Layout:
////               Toast.makeText(getApplicationContext(),"")
//                startActivity(new Intent(MapMenuActivity.this, FuelSummary.class));
//                finish();
//                break;
        }
    }
//    private String getJsonData() {
//
//        return "[\n"+
//                "{\n"+
//                "\"camId\":\"HOW-246157-FUZBB\",\n" +
//                "\"camName\":\"IPCAM\",\n" +
//                "\"userName\":\"admin\",\n" +
//                "\"pwd\":\"admin123\",\n" +
//                "\"shortName\":\"Front View\",\n" +
//                "\"cameraType\":\"BlueCam\"\n" +
//                "},\n"+
//                "\n"+
//                "{\n"+
//                "\"camId\":\"HOW-246157-FUZBB\",\n" +
//                "\"camName\":\"IPCAM\",\n" +
//                "\"userName\":\"admin\",\n" +
//                "\"pwd\":\"admin123\",\n" +
//                "\"shortName\":\"Front View\",\n" +
//                "\"cameraType\":\"BlueCam\"\n" +
//                "  \n" +
//                "},\n" +
//                "\n"+
//                "{\n"+
//                "\"camId\":\"HOW-246157-FUZBB\",\n" +
//                "\"camName\":\"IPCAM\",\n" +
//                "\"userName\":\"admin\",\n" +
//                "\"pwd\":\"admin123\",\n" +
//                "\"shortName\":\"Front View\",\n" +
//                "\"cameraType\":\"BlueCam\"\n" +
//                "}\n"+
//                "]";
//    }

    private String getJsonData() {
        Log.d("jsoncamid",""+camid);
        Log.d("jsoncamid",""+camname);
        Log.d("jsoncamid",""+usename);
        Log.d("jsoncamid",""+pwd);
        Log.d("jsoncamid",""+shorname);
        Log.d("jsoncamid",""+cameratype);

        return "[\n"+
                "{\n"+
                "\"camId\":\""+camid+"\",\n" +
                "\"camName\":\""+camname+"\",\n" +
                "\"userName\":\""+usename+"\",\n" +
                "\"pwd\":\""+pwd+"\",\n" +
                "\"shortName\":\""+shorname+"\",\n" +
                "\"cameraType\":\""+cameratype+"\"\n" +
                "},\n"+
                "\n"+
                "{\n"+
                "\"camId\":\""+camid+"\",\n" +
                "\"camName\":\""+camname+"\",\n" +
                "\"userName\":\""+usename+"\",\n" +
                "\"pwd\":\""+pwd+"\",\n" +
                "\"shortName\":\""+shorname+"\",\n" +
                "\"cameraType\":\""+cameratype+"\"\n" +
                "  \n" +
                "},\n" +
                "\n"+
                "{\n"+
                "\"camId\":\""+camid+"\",\n" +
                "\"camName\":\""+camname+"\",\n" +
                "\"userName\":\""+usename+"\",\n" +
                "\"pwd\":\""+pwd+"\",\n" +
                "\"shortName\":\""+shorname+"\",\n" +
                "\"cameraType\":\""+cameratype+"\"\n" +
                "}\n"+
                "]";
    }

    // private String getJsonData() {

//        String Json= "[\n"+
//                "{\n"+
//                "\"camId\":\""+camid+"\",\n" +
//                "\"camName\":\""+camname+"\",\n" +
//                "\"userName\":\""+usename+"\",\n" +
//                "\"pwd\":\""+pwd+"\",\n" +
//                "\"shortName\":\""+shorname+"\",\n" +
//                "\"cameraType\":\""+cameratype+"\"\n" +
//                "},\n"+
//                "\n"+
//                "{\n"+
//                "\"camId\":\""+camid+"\",\n" +
//                "\"camName\":\""+camname+"\",\n" +
//                "\"userName\":\""+usename+"\",\n" +
//                "\"pwd\":\""+pwd+"\",\n" +
//                "\"shortName\":\""+shorname+"\",\n" +
//                "\"cameraType\":\""+cameratype+"\"\n" +
//                " \n" +
//                "},\n" +
//                "\n"+
//                "{\n"+
//                "\"camId\":\""+camid+"\",\n" +
//                "\"camName\":\""+camname+"\",\n" +
//                "\"userName\":\""+usename+"\",\n" +
//                "\"pwd\":\""+pwd+"\",\n" +
//                "\"shortName\":\""+shorname+"\",\n" +
//                "\"cameraType\":\""+cameratype+"\"\n" +
//                "}\n"+
//                "]";
//        Log.d("print JSON ",Json);
//        return Json;
//    }


    public void showVehicleNameChangeDialog() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(MapMenuActivity.this, R.style.MyAlertDialogStyle);
        Dialog d = alertDialog.setView(new View(this)).create();
        // (That new View is just there to have something inside the dialog that can grow big enough to cover the whole screen.)

//        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
//        lp.copyFrom(d.getWindow().getAttributes());
//        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
//        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
//        d.show();
//        d.getWindow().setAttributes(lp);
        alertDialog.setTitle("Change Vehicle name(" + Constant.SELECTED_VEHICLE_LOCATION_OBJECT.getShortName() + ")");
        alertDialog.setMessage("Enter the new Vehicle name");

//        final EditText input = new EditText(NearByVehicleListActivity.this);
//        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
//                LinearLayout.LayoutParams.MATCH_PARENT,
//                LinearLayout.LayoutParams.MATCH_PARENT);
//        input.setLayoutParams(lp);
//        input.setInputType(InputType.TYPE_CLASS_NUMBER);
//        alertDialog.setView(input);

        final EditText input;

        LayoutInflater inflater = this.getLayoutInflater();

        // Inflate and set the layout for the dialog
        // Pass null as the parent view because its going in the dialog layout
        LayoutInflater li = LayoutInflater.from(getApplicationContext());
        View promptsView = li.inflate(R.layout.textview_layout, null);
        alertDialog.setView(promptsView);

        final EditText userInput = (EditText) promptsView
                .findViewById(R.id.popup_edt_text);
        userInput.setInputType(InputType.TYPE_CLASS_TEXT);
//        userInput.setInputType(R.id.text1);
//        alertDialog.setIcon(R.mipmap.ic_launcher);
        alertDialog.setCancelable(false);
//        alertDialog.set
        alertDialog.setPositiveButton("Ok",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        if (userInput.getText().toString().trim().length() > 0) {
                            String mNewVehicleName = userInput.getText().toString();

                            if (mNewVehicleName != null) {
                                new UpdateVehicleName().execute(mNewVehicleName);
                                dialog.cancel();
                            } else {
                                dialog.cancel();
                                Toast.makeText(getApplicationContext(), "Please enter vehicle name", Toast.LENGTH_SHORT).show();
                                showVehicleNameChangeDialog();
                            }
//
//                            if (mNoOfKms != null) {
//                                mCircleMeters = Integer.valueOf(mNoOfKms) * 1000;
//                            }
//
//                            if (mNoOfKms.equalsIgnoreCase("0") || mNoOfKms.trim().length() == 0) {
//                                Toast.makeText(getApplicationContext(), "Please enter correct data", Toast.LENGTH_SHORT).show();
//                                getKmsData();
//                            } else {
//                                dialog.cancel();
//                                new NearByVehicleListActivity.getNearbyVehicleList().execute();
//                            }
//                            InputMethodManager imm1 = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
//                            imm1.hideSoftInputFromWindow(userInput.getWindowToken(), 0);
//                            InputMethodManager inputManager = (InputMethodManager)
//                                    getSystemService(Context.INPUT_METHOD_SERVICE);
//
//                            inputManager.hideSoftInputFromWindow(userInput.getWindowToken(),
//                                    InputMethodManager.HIDE_NOT_ALWAYS);
//
////                        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
////                        inputMethodManager.hideSoftInputFromWindow(NearByVehicleListActivity.this.getCurrentFocus().getWindowToken(), 0);
                        } else {
                            dialog.cancel();
                            Toast.makeText(getApplicationContext(), "Please enter vehicle name", Toast.LENGTH_SHORT).show();
//                            getKmsData();
                            showVehicleNameChangeDialog();
                        }


                    }
                });

        alertDialog.setNegativeButton("Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
//                        startActivity(new Intent(NearByVehicleListActivity.this, VehicleListActivity.class));
//                        finish();
                    }
                });

        alertDialog.show();


    }

    private void showServerRSms(){
        final AlertDialog.Builder builder = new AlertDialog.Builder(MapMenuActivity.this, R.style.MyAlertDialogStyle);
        builder.setTitle(getResources().getString(R.string.app_name) + "                                 ");

        builder.setMessage(getResources().getString(R.string.immobil)).setCancelable(true).setPositiveButton("SERVER", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
                showLockUnlockDialogServer();

            }
        }).setNegativeButton("SMS", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int i) {
                dialog.cancel();
                showLockUnlockDialogSms();
            }
        });

        AlertDialog alert = builder.create();
        alert.show();
    }
    private void showLockUnlockDialogServer(){
        AlertDialog.Builder builder = new AlertDialog.Builder(MapMenuActivity.this, R.style.MyAlertDialogStyle);
        builder.setTitle(getResources().getString(R.string.app_name) + "                                 ");
        builder.setMessage(Constant.SELECTED_VEHICLE_SHORT_NAME).setCancelable(true).setPositiveButton("Lock", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
                new FetchLockDataServer().execute("");
            }
        }).setNegativeButton("UnLock", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int i) {
                dialog.cancel();
                new FetchUnLockDataserver().execute("");
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }
    private void showLockUnlockDialogSms() {
        AlertDialog.Builder builder = new AlertDialog.Builder(MapMenuActivity.this, R.style.MyAlertDialogStyle);
        builder.setTitle(getResources().getString(R.string.app_name) + "                                 ");
        builder.setMessage(Constant.SELECTED_VEHICLE_SHORT_NAME).setCancelable(true).setPositiveButton("Lock", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
                new FetchLockData().execute("");
            }
        }).setNegativeButton("UnLock", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int i) {
                dialog.cancel();
                new FetchUnLockData().execute("");
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    private void showOpenCloseDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(MapMenuActivity.this, R.style.MyAlertDialogStyle);
        builder.setTitle(getResources().getString(R.string.app_name) + "                                        ");
        builder.setMessage(Constant.SELECTED_VEHICLE_SHORT_NAME).setCancelable(true).setPositiveButton("Open", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
                new FetchOpenDoorData().execute("");
            }
        }).setNegativeButton("Close", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int i) {
                dialog.cancel();
                new FetchCloseDoorData().execute("");
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    //    @Override
//    public void call() {
//        switchbutton.setChecked(false);
//    }
    private class reportrestrict extends AsyncTask<String, String, String> {

        protected void onPreExecute() {
            super.onPreExecute();
//            progressDialog = MyCustomProgressDialog.ctor(MapMenuActivity.this);
//            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {
            try {
                HttpConfig httpConfig = new HttpConfig();
                return httpConfig.httpGet(Const.API_URL +"mobile/getReportsRestrictForMobile?userId=" +
                        Constant.SELECTED_USER_ID);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {

            super.onPostExecute(result);
//            if (progressDialog.isShowing()) {
//                progressDialog.dismiss();
//            }
            if (result != null && result.length() > 0) {
                //  Log.d("parkingresult", "" + result);
                try {
                    JSONObject jsonObject = new JSONObject(result.trim());
                    if (jsonObject.has("TEMPERATURE")){
                        Log.d("TEMPERATURE",""+jsonObject.getString("TEMPERATURE"));
                        if (jsonObject.getString("TEMPERATURE").equalsIgnoreCase("false")){
                            $LayoutTemperature.setVisibility(View.GONE);
                            $View12.setVisibility(View.GONE);
                        }
                        if (jsonObject.getString("TEMPERATURE").equalsIgnoreCase("true")){
                            $LayoutTemperature.setVisibility(View.VISIBLE);
                            $View12.setVisibility(View.VISIBLE);
                        }
                    }
                    if (jsonObject.has("FUEL")){
                        Log.d("FUEL",""+jsonObject.getString("FUEL"));
                        if (jsonObject.getString("FUEL").equalsIgnoreCase("false")){
                            $LayoutFuelFill.setVisibility(View.GONE);
                            $LayoutFuelTheft.setVisibility(View.GONE);
                            $View11.setVisibility(View.GONE);
                            $View24.setVisibility(View.GONE);

                        }
                        if (jsonObject.getString("FUEL").equalsIgnoreCase("true")){
                            $LayoutFuelFill.setVisibility(View.VISIBLE);
                            $LayoutFuelTheft.setVisibility(View.VISIBLE);
                            $View11.setVisibility(View.VISIBLE);
                            $View24.setVisibility(View.VISIBLE);

                        }
                    }


                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        }
    }
    private class buttonstatus extends AsyncTask<String, String, String> {

        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = MyCustomProgressDialog.ctor(MapMenuActivity.this);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {
            try {
                HttpConfig httpConfig = new HttpConfig();
                return httpConfig.httpGet(Const.API_URL +"mobile/getVehicleLocations?userId=" +
                        Constant.SELECTED_USER_ID);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {

            super.onPostExecute(result);
            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
            if (result != null && result.length() > 0) {
                //  Log.d("parkingresult", "" + result);
                try {
                    JSONArray jsonArray = new JSONArray(result.trim());
                    for (int i = 0; i < jsonArray.length(); i++) {
                        jsonObject = jsonArray.getJSONObject(i);

                        if (jsonObject.has("vehicleLocations")) {
                            JSONArray jsonArray1 = jsonObject.getJSONArray("vehicleLocations");
                            for (int j = 0; j < jsonArray1.length(); j++) {
                                JSONObject jsonObject1 = jsonArray1.getJSONObject(j);
                                String vehicleid = jsonObject1.getString("vehicleId");
                                // Log.d("vehicleid", "" + vehicleid);
                                String selectvehicle = Constant.SELECTED_VEHICLE_ID;
                                // Log.d("selectvehicle", "" + selectvehicle);
                                if (jsonObject1.has("safetyParking")) {
                                    String parkingstatus = jsonObject1.getString("safetyParking");
                                    //   Log.d("parkingstatus", "" + parkingstatus);
                                    if (selectvehicle.equalsIgnoreCase(vehicleid)) {
                                        if (parkingstatus.equalsIgnoreCase("yes")) {
                                            switchbutton.setChecked(true);
                                        } else if (parkingstatus.equalsIgnoreCase("no")) {
                                            switchbutton.setChecked(false);
                                        }
                                    }


                                }
                            }

                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
//            if (result != null && result.length() > 0) {
//                Log.d("parkingresult", "" + result);
//                try {
//                    JSONArray jsonArray = new JSONArray(result.trim());
//                    for (int i = 0; i < jsonArray.length(); i++) {
//                        jsonObject = jsonArray.getJSONObject(i);
//                    }
//
//                    if (jsonObject.has("vehicleLocations")) {
//                        JSONArray jsonArray1 = jsonObject.getJSONArray("vehicleLocations");
//                        for (int i = 0; i < jsonArray1.length(); i++) {
//                            JSONObject jsonObject1 = jsonArray1.getJSONObject(i);
//                            String vehicleid = jsonObject1.getString("vehicleId");
//                            Log.d("vehicleid", "" + vehicleid);
//                            String selectvehicle = Constant.SELECTED_VEHICLE_ID;
//                            Log.d("selectvehicle", "" + selectvehicle);
//                            if (jsonObject1.has("safetyParking")) {
//                                String parkingstatus = jsonObject1.getString("safetyParking");
//                                Log.d("parkingstatus", "" + parkingstatus);
//                                if (selectvehicle.equalsIgnoreCase(vehicleid)) {
//                                    if (parkingstatus.equalsIgnoreCase("yes")) {
//                                        switchbutton.setChecked(true);
//                                    } else if (parkingstatus.equalsIgnoreCase("no")) {
//                                        switchbutton.setChecked(false);
//                                    }
//                                }
//
//
//                            }
//                        }
//
//                    }
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            }

        }
    }

    private class gpsalaramData extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = MyCustomProgressDialog.ctor(MapMenuActivity.this);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            //   Log.d("switchstatus", "" + params[0]);
            try {
                HttpConfig httpConfig = new HttpConfig();
                return httpConfig.httpGet(Const.API_URL + "mobile/configureSafetyParkingAlarm?userId="
                        + Constant.SELECTED_USER_ID + "&vehicleId=" +
                        Constant.SELECTED_VEHICLE_ID + "&enableOrDisable=" + params[0]);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            data = result;
            Log.d("result", "" + result);
            Log.d("result", "" + data);
            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
//            if (data.equalsIgnoreCase("vehicle is not parked so we can't change...")){
//                a_dialog=new Dialog(MapMenuActivity.this);
//                a_dialog.setCancelable(false);
//                a_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//                a_dialog.setContentView(R.layout.dialog_message);
//                TextView message=(TextView)a_dialog.findViewById(R.id.dialog_msg);
//                if (data.equalsIgnoreCase("vehicle is not parked so we can't change...")){
//
//                    String upperletter=Character.toString(data.charAt(0)).toUpperCase()+data.substring(1);
//                    Log.d("upperletter",""+upperletter);
//                    message.setText(upperletter);
//                }
//
//                message.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
//                TextView ok=(TextView)a_dialog.findViewById(R.id.ok_button);
//                ok.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
//                ok.setOnClickListener(new OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        a_dialog.dismiss();
////                        if (data.equalsIgnoreCase("vehicle is not parked so we can't change...")){
////                            switchbutton.setChecked(false);
////                            Log.d("vehicleparked","vehiclenotparked");
////                        }
//                    }
//                });
//                a_dialog.show();
//            }
            if (data != null && data.length() > 0) {
                if (data.equalsIgnoreCase("vehicle is not parked so we can't change...")) {
                    a_dialog = new Dialog(MapMenuActivity.this);
                    a_dialog.setCancelable(false);
                    a_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    a_dialog.setContentView(R.layout.dialog_message);
                    TextView message = (TextView) a_dialog.findViewById(R.id.dialog_msg);
                    if (data.equalsIgnoreCase("vehicle is not parked so we can't change...")) {

                        String upperletter = Character.toString(data.charAt(0)).toUpperCase() + data.substring(1);
                        Log.d("upperletter", "" + upperletter);
                        message.setText(upperletter);
                    }

                    message.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
                    TextView ok = (TextView) a_dialog.findViewById(R.id.ok_button);
                    ok.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
                    ok.setOnClickListener(new OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            a_dialog.dismiss();
                            if (data.equalsIgnoreCase("vehicle is not parked so we can't change...")) {
//                                switchbutton.setChecked(false);

                                if (switchbutton.isChecked()) {
//                            mEnableSafeParkingSwitch.setChecked(false);
                                    setCheckedForSwitchNew(false);
                                } else {
//                            mEnableSafeParkingSwitch.setChecked(true);
                                    setCheckedForSwitchNew(true);
                                }

                            }
                        }
                    });
                    a_dialog.show();
                }
            } else {
                if (switchbutton.isChecked()) {
//                            mEnableSafeParkingSwitch.setChecked(false);
                    setCheckedForSwitchNew(false);
                } else {
//                            mEnableSafeParkingSwitch.setChecked(true);
                    setCheckedForSwitchNew(true);
                }
            }


        }
    }

    private class UpdateVehicleName extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = MyCustomProgressDialog.ctor(MapMenuActivity.this);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                HttpConfig ht = new HttpConfig();

                return ht.httpGet(Const.API_URL + "/mobile/changeVehicleShortName?vehicleId=" + Constant.SELECTED_VEHICLE_ID + "&vehicleName=" + params[0]
                        + "&userId=" + Constant.SELECTED_USER_ID);

//
//                return ht.httpGet(Const.NEW_API_URL + "changeVehicleShortName?vehicleId=" + Constant.SELECTED_VEHICLE_ID + "&vehicleName=" + params[0]
//                        + "&userId=" + Constant.SELECTED_USER_ID);

//                return ht.httpGet(Const.NEW_API_URL + "getLock?vehicleId=" + Constant.SELECTED_VEHICLE_ID + "&userId=" + Constant.SELECTED_USER_ID);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        public void onPostExecute(String result) {

            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
            }

            try {
                if (result != null && !result.isEmpty()) {
                    if (result.equalsIgnoreCase("success")) {
                        Toast.makeText(getApplicationContext(), "Vehicle name successfully updated", Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(getApplicationContext(), "Failure! Please Try Again", Toast.LENGTH_LONG).show();
                    }
                } else {
                    Toast.makeText(getApplicationContext(), "Failure! Please Try Again", Toast.LENGTH_LONG).show();
                }
            } catch (Exception e) {
                e.printStackTrace();
                Toast.makeText(MapMenuActivity.this, "Lock message not sent... Please try again later.", Toast.LENGTH_LONG).show();
            }
//            if (progressDialog.isShowing()) {
//                progressDialog.dismiss();
//            }
        }
    }
    private class FetchUnLockDataserver extends AsyncTask<String,String,String> {
        @Override
        protected String doInBackground(String... strings) {
            try {
                HttpConfig ht = new HttpConfig();
                System.out.println(Const.API_URL + "mobile/sentImmoblizerFromServer?userId=" + Constant.SELECTED_USER_ID+ "&vehicleId="+ Constant.SELECTED_VEHICLE_ID+ "&lockOrUnlock=false");
                return ht.httpGet(Const.API_URL + "mobile/sentImmoblizerFromServer?userId=" + Constant.SELECTED_USER_ID+ "&vehicleId="+ Constant.SELECTED_VEHICLE_ID+ "&lockOrUnlock=false");
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = MyCustomProgressDialog.ctor(MapMenuActivity.this);
            progressDialog.show();
        }

        @Override
        protected void onPostExecute(String s) {
            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
            Toast.makeText(MapMenuActivity.this,s,Toast.LENGTH_LONG).show();
        }
    }

    private class FetchLockDataServer extends AsyncTask<String,String,String> {
        @Override
        protected String doInBackground(String... strings) {
            try {
                HttpConfig ht = new HttpConfig();
                System.out.println(Const.API_URL + "mobile/sentImmoblizerFromServer?userId=" + Constant.SELECTED_USER_ID+ "&vehicleId="+ Constant.SELECTED_VEHICLE_ID+ "&lockOrUnlock=true");
                return ht.httpGet(Const.API_URL + "mobile/sentImmoblizerFromServer?userId=" + Constant.SELECTED_USER_ID+ "&vehicleId="+ Constant.SELECTED_VEHICLE_ID+ "&lockOrUnlock=true");
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = MyCustomProgressDialog.ctor(MapMenuActivity.this);
            progressDialog.show();
        }

        @Override
        protected void onPostExecute(String s) {
            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
            Toast.makeText(MapMenuActivity.this,s,Toast.LENGTH_LONG).show();
        }
    }

    private class FetchLockData extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = MyCustomProgressDialog.ctor(MapMenuActivity.this);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                HttpConfig ht = new HttpConfig();
                return ht.httpGet(Const.API_URL + "mobile/getLock?vehicleId=" + Constant.SELECTED_VEHICLE_ID + "&userId=" + Constant.SELECTED_USER_ID);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        public void onPostExecute(String result) {

            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
            }

            try {
                if (result != null && !result.isEmpty()) {
                    JSONObject mJsonObject = new JSONObject(result);
                    if (mJsonObject != null) {
                        if (mJsonObject.has("mobileNo") && mJsonObject.has("smsText")) {
//                            SmsManager.getDefault().sendTextMessage(mJsonObject.getString("mobileNo"), null, mJsonObject.getString("smsText"), null, null);

                            mTO_MOBILE_NUMBER = mJsonObject.getString("mobileNo");
                            mSMS_CONTENT = mJsonObject.getString("smsText");
                            sendSmsWithUserPermission();
                        }
                    }
                } else {
                    Toast.makeText(getApplicationContext(), "Please Try Again", Toast.LENGTH_LONG).show();
                }
            } catch (Exception e) {
                e.printStackTrace();
                Toast.makeText(MapMenuActivity.this, "Lock message not sent... Please try again later.", Toast.LENGTH_LONG).show();
            }
//            if (progressDialog.isShowing()) {
//                progressDialog.dismiss();
//            }
        }
    }

    private class FetchUnLockData extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = MyCustomProgressDialog.ctor(MapMenuActivity.this);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                HttpConfig ht = new HttpConfig();
                return ht.httpGet(Const.API_URL + "mobile/getUnLock?vehicleId=" + Constant.SELECTED_VEHICLE_ID + "&userId=" + Constant.SELECTED_USER_ID);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        public void onPostExecute(String result) {

            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
            try {
                if (result != null && !result.isEmpty()) {
                    JSONObject mJsonObject = new JSONObject(result);
                    if (mJsonObject != null) {
                        if (mJsonObject.has("mobileNo") && mJsonObject.has("smsText")) {
//                            SmsManager.getDefault().sendTextMessage(mJsonObject.getString("mobileNo"), null, mJsonObject.getString("smsText"), null, null);
                            mTO_MOBILE_NUMBER = mJsonObject.getString("mobileNo");
                            mSMS_CONTENT = mJsonObject.getString("smsText");
                            sendSmsWithUserPermission();
                        }
                    }
                } else {
                    Toast.makeText(getApplicationContext(), "Please Try Again", Toast.LENGTH_LONG).show();
                }
            } catch (Exception e) {
                e.printStackTrace();
                Toast.makeText(MapMenuActivity.this, "UnLock message not sent... Please try again later.", Toast.LENGTH_LONG).show();
            }
//            if (progressDialog.isShowing()) {
//                progressDialog.dismiss();
//            }
        }
    }


    private class FetchOpenDoorData extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = MyCustomProgressDialog.ctor(MapMenuActivity.this);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                HttpConfig ht = new HttpConfig();
                return ht.httpGet(Const.API_URL + "mobile/openDoor?vehicleId=" + Constant.SELECTED_VEHICLE_ID + "&userId=" + Constant.SELECTED_USER_ID + "&fcode=true");
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        public void onPostExecute(String result) {

            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
            }

            try {
                if (result != null && !result.isEmpty()) {
//                    JSONObject mJsonObject = new JSONObject(result);
//                    if (mJsonObject != null) {
//                        if (mJsonObject.has("mobileNo") && mJsonObject.has("smsText")) {
////                            SmsManager.getDefault().sendTextMessage(mJsonObject.getString("mobileNo"), null, mJsonObject.getString("smsText"), null, null);
//
//                            mTO_MOBILE_NUMBER = mJsonObject.getString("mobileNo");
//                            mSMS_CONTENT = mJsonObject.getString("smsText");
//                            sendSmsWithUserPermission();
//                        }
//                    }

                    if (result.trim().equalsIgnoreCase("Yes")) {
                        Toast.makeText(getApplicationContext(), "Successfully Door Opened", Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(getApplicationContext(), "Failure", Toast.LENGTH_LONG).show();
                    }

                } else {
                    Toast.makeText(getApplicationContext(), "Please Try Again", Toast.LENGTH_LONG).show();
                }
            } catch (Exception e) {
                e.printStackTrace();
                Toast.makeText(MapMenuActivity.this, "Please try again later.", Toast.LENGTH_LONG).show();
            }
//            if (progressDialog.isShowing()) {
//                progressDialog.dismiss();
//            }
        }
    }

    private class FetchCloseDoorData extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = MyCustomProgressDialog.ctor(MapMenuActivity.this);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                HttpConfig ht = new HttpConfig();
                return ht.httpGet(Const.API_URL + "mobile/closeDoor?vehicleId=" + Constant.SELECTED_VEHICLE_ID + "&userId=" + Constant.SELECTED_USER_ID + "&fcode=true");
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        public void onPostExecute(String result) {

            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
            try {
                if (result != null && !result.isEmpty()) {
//                    JSONObject mJsonObject = new JSONObject(result);
//                    if (mJsonObject != null) {
//                        if (mJsonObject.has("mobileNo") && mJsonObject.has("smsText")) {
////                            SmsManager.getDefault().sendTextMessage(mJsonObject.getString("mobileNo"), null, mJsonObject.getString("smsText"), null, null);
//                            mTO_MOBILE_NUMBER = mJsonObject.getString("mobileNo");
//                            mSMS_CONTENT = mJsonObject.getString("smsText");
//                            sendSmsWithUserPermission();
//                        }
//                    }

                    if (result.trim().equalsIgnoreCase("Yes")) {
                        Toast.makeText(getApplicationContext(), "Successfully Door Closed", Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(getApplicationContext(), "Failure", Toast.LENGTH_LONG).show();
                    }

                } else {
                    Toast.makeText(getApplicationContext(), "Please Try Again", Toast.LENGTH_LONG).show();
                }
            } catch (Exception e) {
                e.printStackTrace();
                Toast.makeText(MapMenuActivity.this, "Please try again later.", Toast.LENGTH_LONG).show();
            }
//            if (progressDialog.isShowing()) {
//                progressDialog.dismiss();
//            }
        }
    }


    final private int REQUEST_CODE_ASK_PERMISSIONS = 123;


    private void sendSmsWithUserPermission() {
        int hasWriteContactsPermission = 0;
//        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
//            hasWriteContactsPermission = ContextCompat.checkSelfPermission(MapMenuActivity.this, android.Manifest.permission.SEND_SMS);
//
//            if (hasWriteContactsPermission != PackageManager.PERMISSION_GRANTED) {
//
//                ActivityCompat.requestPermissions(MapMenuActivity.this, new String[]{android.Manifest.permission.SEND_SMS},
//                        REQUEST_CODE_ASK_PERMISSIONS);
//                return;
//            } else {
//                sendSms();
//            }
//        } else {
        sendSms();
        // }
//        insertDummyContact();


    }

    public void sendSms() {
        Uri uri = Uri.parse("smsto:"+mTO_MOBILE_NUMBER);
        Intent intent = new Intent(Intent.ACTION_SENDTO, uri);
        intent.putExtra("sms_body", mSMS_CONTENT);
        startActivity(intent);
        // SmsManager.getDefault().sendTextMessage(mTO_MOBILE_NUMBER, null, mSMS_CONTENT, null, null);
    }

    final CompoundButton.OnCheckedChangeListener toggleButtonChangeListenerNew = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            // The user changed the button, do something
            if (isChecked) {
                new gpsalaramData().execute(enable);
            } else {
                new gpsalaramData().execute(disable);

            }
        }
    };


    final CompoundButton.OnCheckedChangeListener toggleButtonChangeListener = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            // The user changed the button, do something
            sendSafeParking(isChecked);
        }
    };

    private void sendSafeParking(boolean isChecked) {
//        if (Constant.SELECTED_VEHICLE_LOCATION_OBJECT.getPosition().equalsIgnoreCase("P")) {
        //stop the timer after 40 seconds and disable the safe parking toggle
        String mEnable = null;
        if (isChecked) {
            mEnable = "Yes";
        } else {
            mEnable = "No";
        }

//        HttpConfig ht = new HttpConfig();
        String url = Const.NEW_API_URL + "configureSafetyParkingAlarm?userId=" + Constant.SELECTED_USER_ID + "&vehicleId=" + Constant.SELECTED_VEHICLE_ID + "&enableOrDisable=" + mEnable;
//        String mResult = null;
        try {
//            mResult = ht.httpGet(url);
//
//            if (mResult.trim().equalsIgnoreCase("Success")) {
//                //start timer
//            } else {
//                Toast.makeText(getApplicationContext(), mResult, Toast.LENGTH_SHORT).show();
//            }
            if (cd.isConnectingToInternet()) {
                new sendSafeParkingStatus().execute(url);
            } else {
                Toast.makeText(getApplicationContext(), "Please check your internet connection", Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

//        System.out.println("Hi the vehicle position is " + Constant.SELECTED_VEHICLE_LOCATION_OBJECT.getPosition());


//        } else {
//            setCheckedForSwitch(false);
//            Toast.makeText(getApplicationContext(), "Vehicle is not parked.Please try other vehicle", Toast.LENGTH_SHORT).show();
//        }

    }

    private void safeParkingTimer() {
        myTimer = new Timer();
        myTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                if (myTimer != null) {
                    myTimer.cancel();
                }
//                System.out.println("Hi timer called");
//                mEnableSafeParkingSwitch.setChecked(false);
                setCheckedForSwitch(false);
            }

        }, 40000);
    }


    private class sendSafeParkingStatus extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = MyCustomProgressDialog.ctor(MapMenuActivity.this);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                HttpConfig ht = new HttpConfig();

                return ht.httpGet(params[0]);
//                return ht.httpGet(Const.API_URL + "mobile/getLock?vehicleId=" + Constant.SELECTED_VEHICLE_ID + "&userId=" + Constant.SELECTED_USER_ID);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        public void onPostExecute(String result) {

            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
            }

            try {
                if (result != null && !result.isEmpty()) {
                    if (result.trim().equalsIgnoreCase("Success")) {
                        //start timer
                        if (mEnableSafeParkingSwitch.isChecked()) {
                            safeParkingTimer();
                        }

                    } else {
                        if (mEnableSafeParkingSwitch.isChecked()) {
//                            mEnableSafeParkingSwitch.setChecked(false);
                            setCheckedForSwitch(false);
                        } else {
//                            mEnableSafeParkingSwitch.setChecked(true);
                            setCheckedForSwitch(true);
                        }

                        Toast.makeText(getApplicationContext(), result, Toast.LENGTH_SHORT).show();
                    }
                } else {

                    if (mEnableSafeParkingSwitch.isChecked()) {
//                        mEnableSafeParkingSwitch.setChecked(false);
                        setCheckedForSwitch(false);
                    } else {
//                        mEnableSafeParkingSwitch.setChecked(true);
                        setCheckedForSwitch(true);
                    }

                    Toast.makeText(getApplicationContext(), "Failure! Please Try Again", Toast.LENGTH_LONG).show();
                }
            } catch (Exception e) {
                e.printStackTrace();
                if (mEnableSafeParkingSwitch.isChecked()) {
//                    mEnableSafeParkingSwitch.setChecked(false);
                    setCheckedForSwitch(false);
                } else {
//                    mEnableSafeParkingSwitch.setChecked(true);
                    setCheckedForSwitch(true);
                }

                Toast.makeText(getApplicationContext(), "Failure! Please Try Again", Toast.LENGTH_LONG).show();
            }
//            if (progressDialog.isShowing()) {
//                progressDialog.dismiss();
//            }
        }
    }


    private void setCheckedForSwitch(final boolean isEnable) {

        runOnUiThread(new Runnable() {
            @Override
            public void run() {


                mEnableSafeParkingSwitch.setOnCheckedChangeListener(null);
                mEnableSafeParkingSwitch.setChecked(isEnable);
                mEnableSafeParkingSwitch.setOnCheckedChangeListener(toggleButtonChangeListener);
            }
        });
    }

    private void setCheckedForSwitchNew(final boolean isEnable) {

        runOnUiThread(new Runnable() {
            @Override
            public void run() {


                switchbutton.setOnCheckedChangeListener(null);
                switchbutton.setChecked(isEnable);
                switchbutton.setOnCheckedChangeListener(toggleButtonChangeListenerNew);
            }
        });
    }


    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_ASK_PERMISSIONS: {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! do the
                    // calendar task you need to do.
                    sendSms();
                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Toast.makeText(MapMenuActivity.this, "Sms permission denied", Toast.LENGTH_SHORT)
                            .show();
                }
                return;
            }

            // other 'switch' lines to check for other
            // permissions this app might request
        }
    }


    Dialog marker_info_dialog;

    private boolean showGetUrlDialog() {


        Spinner mVehiSpinner, mTimeSpinner;
        final EditText mEdtEmail, mEdtPhoneNo;
        TextView id_txt_divider1, id_txt_divider2, id_histroy_vehicleid;
        Button id_history_selection_done, id_history_selection_cancel;

        marker_info_dialog = new Dialog(MapMenuActivity.this);

        marker_info_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        marker_info_dialog.setContentView(R.layout.get_url_popup);
        marker_info_dialog.setCancelable(false);

        id_histroy_vehicleid = (TextView) marker_info_dialog.findViewById(R.id.get_url_popup_title_txt);

        id_txt_divider1 = (TextView) marker_info_dialog.findViewById(R.id.get_url_popup_divider_txt_1);
        id_txt_divider2 = (TextView) marker_info_dialog.findViewById(R.id.get_url_popup_divider_txt_2);


        mEdtEmail = (EditText) marker_info_dialog.findViewById(R.id.get_url_popup_email_id_edtText);
        mEdtPhoneNo = (EditText) marker_info_dialog.findViewById(R.id.get_url_popup_phone_number_edtText);

        mVehiSpinner = (Spinner) marker_info_dialog.findViewById(R.id.get_url_popup_vehicle_id_spinner);
        mTimeSpinner = (Spinner) marker_info_dialog.findViewById(R.id.get_url_popup_time_spinner);


        id_history_selection_done = (Button) marker_info_dialog.findViewById(R.id.get_url_popup_selection_done);
        id_history_selection_cancel = (Button) marker_info_dialog.findViewById(R.id.get_url_popup_selection_cancel);


        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);

        Constant.ScreenWidth = width;
        Constant.ScreenHeight = height;


        LinearLayout.LayoutParams popupHeadParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        popupHeadParams.width = width * 90 / 100;
        popupHeadParams.height = height * 7 / 100;
        // popupHeadParams.height = LinearLayout.LayoutParams.WRAP_CONTENT;
        popupHeadParams.setMargins(width * 1 / 100, width * 1 / 100, width * 1 / 100, width * 1 / 100);
        id_histroy_vehicleid.setLayoutParams(popupHeadParams);
        id_histroy_vehicleid.setGravity(Gravity.CENTER);

        LinearLayout.LayoutParams backImageParams1 = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        backImageParams1.width = width * 88 / 100;
        backImageParams1.height = height * 8 / 100;
        backImageParams1.setMargins(width * 1 / 100, width * 1 / 100, width * 1 / 100, width * 1 / 100);
        mTimeSpinner.setLayoutParams(backImageParams1);
        mVehiSpinner.setLayoutParams(backImageParams1);
        mEdtEmail.setLayoutParams(backImageParams1);
        mEdtPhoneNo.setLayoutParams(backImageParams1);
        //   LinearLayout.LayoutParams backImageParams1 = new LinearLayout.LayoutParams()


        if (width >= 600) {
            id_histroy_vehicleid.setTextSize(18);
            mEdtEmail.setTextSize(17);
            mEdtPhoneNo.setTextSize(17);

        } else if (width > 501 && width < 600) {
            id_histroy_vehicleid.setTextSize(17);
            mEdtEmail.setTextSize(16);
            mEdtPhoneNo.setTextSize(16);
        } else if (width > 260 && width < 500) {
            id_histroy_vehicleid.setTextSize(16);
            mEdtEmail.setTextSize(15);
            mEdtPhoneNo.setTextSize(15);
        } else if (width <= 260) {
            id_histroy_vehicleid.setTextSize(15);
            mEdtEmail.setTextSize(14);
            mEdtPhoneNo.setTextSize(14);
        }


        ArrayAdapter<String> adapter = new ArrayAdapter<String>(MapMenuActivity.this,
                android.R.layout.simple_spinner_item, time_array);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mTimeSpinner.setAdapter(adapter);

        id_histroy_vehicleid.setText(Constant.SELECTED_VEHICLE_SHORT_NAME);


        mTimeSpinner
                .setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                    @Override
                    public void onItemSelected(AdapterView<?> parent,
                                               View view, int pos, long id) {
                        // TODO Auto-generated method stub
                        mTimeSpinnerSelectedValue = time_array[pos];

                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> arg0) {
                        // TODO Auto-generated method stub
                    }
                });


        /** listener to handle when history is canceled */
        id_history_selection_cancel.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
//                opptionPopUp();
                marker_info_dialog.hide();
            }
        });

        /** listener to handle when history is selected */
        id_history_selection_done.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {


                if (mTimeSpinnerSelectedValue != null) {
                    if (!mTimeSpinnerSelectedValue.equalsIgnoreCase("Days")) {

                        if (mEdtEmail.getText().toString().trim().length() > 0) {

                            // if (mEdtPhoneNo.getText().toString().trim().length() > 0) {
                            // System.out.println("The values are spinner ::" + mTimeSpinnerSelectedValue + " :: email id ::::" + mEdtEmail.getText().toString() + " :: phone number ::::" + mEdtPhoneNo.getText().toString());


                            if (cd.isConnectingToInternet()) {

                                String mGroupName = null;
                                if (Constant.SELECTED_GROUP.contains(":")) {
                                    String[] str_msg_data_array = Constant.SELECTED_GROUP.split(":");
                                    mGroupName = str_msg_data_array[1];
                                } else {
                                    mGroupName = Constant.SELECTED_GROUP;
                                }
                                if (mTimeSpinnerSelectedValue.equalsIgnoreCase("1 Month")){
                                    mTimeSpinnerSelectedValue = Integer.toString(30);
                                } else if (mTimeSpinnerSelectedValue.equalsIgnoreCase("3 Month")){
                                    mTimeSpinnerSelectedValue = Integer.toString(90);
                                } else if (mTimeSpinnerSelectedValue.equalsIgnoreCase("1 Year")){
                                    mTimeSpinnerSelectedValue = Integer.toString(365);
                                }


                                Log.d("mTimeSpinner",""+mTimeSpinnerSelectedValue);

                                String mGetUrl = Const.API_URL + "mobile/getVehicleExp?vehicleId=" + Constant.SELECTED_VEHICLE_ID + "&days=" + mTimeSpinnerSelectedValue + "&fcode=" + mGroupName + "&mailId=" + mEdtEmail.getText().toString().trim() + "&phone=" + mEdtPhoneNo.getText().toString().trim() + "&userId=" + Constant.SELECTED_USER_ID;

                                new sendUrlDataToMobile().execute(mGetUrl);


                            } else {
                                Toast internet_toast = Toast.makeText(getApplicationContext(), "Please Check your Internet Connection", Toast.LENGTH_LONG);
                                internet_toast.show();
                            }

//                            } else {
//                                Toast internet_toast = Toast.makeText(getApplicationContext(), "Please enter value for phone number", Toast.LENGTH_LONG);
//                                internet_toast.show();
//                            }

                        } else {
                            Toast internet_toast = Toast.makeText(getApplicationContext(), "Please enter value for email id", Toast.LENGTH_LONG);
                            internet_toast.show();
                        }


                    } else {
                        Toast internet_toast = Toast.makeText(getApplicationContext(), "Please Select days value", Toast.LENGTH_LONG);
                        internet_toast.show();
                    }
                } else {
                    Toast internet_toast = Toast.makeText(getApplicationContext(), "Please Select days value", Toast.LENGTH_LONG);
                    internet_toast.show();
                }

            }
        });

        marker_info_dialog.show();
        return true;
    }



    private class sendUrlDataToMobile extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = MyCustomProgressDialog.ctor(MapMenuActivity.this);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                HttpConfig ht = new HttpConfig();
                return ht.httpGet(params[0]);
//                return ht.httpGet(Const.API_URL + "mobile/getLock?vehicleId=" + Constant.SELECTED_VEHICLE_ID + "&userId=" + Constant.SELECTED_USER_ID);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        public void onPostExecute(String result) {

            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
            }

            try {
                if (result != null && !result.isEmpty() && !result.contains("<html>")) {
                    marker_info_dialog.hide();
                    Toast.makeText(getApplicationContext(), "Mail send to your email i'd", Toast.LENGTH_LONG).show();

                } else {
                    Toast.makeText(getApplicationContext(), "Please Try Again", Toast.LENGTH_LONG).show();
                }
            } catch (Exception e) {
                e.printStackTrace();

            }

        }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();

        startActivity(new Intent(MapMenuActivity.this, VehicleListActivity.class));
        finish();

    }

    private void screenArrange() {
        // TODO Auto-generated method stub
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        height = displayMetrics.heightPixels;
        width = displayMetrics.widthPixels;

        LinearLayout.LayoutParams backImageParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        backImageParams.width = width * 15 / 100;
        backImageParams.height = height * 10 / 100;
        backImageParams.gravity = Gravity.CENTER;
        $BackArrow.setLayoutParams(backImageParams);
        $BackArrow.setPadding(width * 1 / 100, height * 1 / 100, width * 1 / 100, height * 1 / 100);

        LinearLayout.LayoutParams headTxtParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        headTxtParams.width = width * 85 / 100;
        headTxtParams.height = height * 10 / 100;
        $TxtTitle.setLayoutParams(headTxtParams);
        $TxtTitle.setPadding(width * 2 / 100, 0, 0, 0);
//        $TxtTitle.setGravity(Gravity.CENTER | Gravity.LEFT);
        $TxtTitle.setGravity(Gravity.CENTER | Gravity.LEFT);

        LinearLayout.LayoutParams mainLayoutParama = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        mainLayoutParama.width = width;
        mainLayoutParama.height = (int) (height * 10 / 100);
        $LayoutMap.setLayoutParams(mainLayoutParama);
        $LayoutHistroy.setLayoutParams(mainLayoutParama);
        $LayoutInfo.setLayoutParams(mainLayoutParama);
        $LayoutLocate.setLayoutParams(mainLayoutParama);
        $LayoutReport.setLayoutParams(mainLayoutParama);
        $LayoutLiveTracking.setLayoutParams(mainLayoutParama);
        $LayoutNearBy.setLayoutParams(mainLayoutParama);
        $LayoutShowImage.setLayoutParams(mainLayoutParama);
        $LayoutTripSummary.setLayoutParams(mainLayoutParama);
        $LayoutDriverDetails.setLayoutParams(mainLayoutParama);
        $LayoutFuelFill.setLayoutParams(mainLayoutParama);
        $LayoutFuelTheft.setLayoutParams(mainLayoutParama);
        //$LayoutFuelsummary.setLayoutParams(mainLayoutParama);
        $LayoutTemperature.setLayoutParams(mainLayoutParama);
        $LayoutAlarm.setLayoutParams(mainLayoutParama);
        $LayoutSiteAnalysis.setLayoutParams(mainLayoutParama);
        $LayoutGetUrl.setLayoutParams(mainLayoutParama);
        $LayoutChangeName.setLayoutParams(mainLayoutParama);
        $LayoutCamera.setLayoutParams(mainLayoutParama);
        $LayoutCameraView.setLayoutParams(mainLayoutParama);
        $LayoutSiteTripReport.setLayoutParams(mainLayoutParama);
        $LayoutStoppage.setLayoutParams(mainLayoutParama);
        $LayoutAc.setLayoutParams(mainLayoutParama);
        $LayoutSafeParking.setLayoutParams(mainLayoutParama);
        $LayoutFms.setLayoutParams(mainLayoutParama);

        LinearLayout.LayoutParams imgParama = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        imgParama.width = width * 13 / 100;
        imgParama.height = height * 8 / 100;
        imgParama.gravity = Gravity.CENTER | Gravity.LEFT;
        imgParama.leftMargin = width * 5 / 100;
        $ImgMap.setLayoutParams(imgParama);
        $ImgInfo.setLayoutParams(imgParama);
        $ImgLocate.setLayoutParams(imgParama);
        $ImgReport.setLayoutParams(imgParama);
        $ImgHistroy.setLayoutParams(imgParama);
        $ImgLiveTracking.setLayoutParams(imgParama);
        $ImgShowImage.setLayoutParams(imgParama);
        $ImgNearBy.setLayoutParams(imgParama);
        $ImgTripSummary.setLayoutParams(imgParama);
        $ImgDriverDetails.setLayoutParams(imgParama);
        $ImgFuelFill.setLayoutParams(imgParama);
        $ImgTemperature.setLayoutParams(imgParama);
        $ImgAlarm.setLayoutParams(imgParama);
        $ImgSiteAnalysis.setLayoutParams(imgParama);
        $ImggetUrl.setLayoutParams(imgParama);
        $ImgChangeName.setLayoutParams(imgParama);
        $ImgCamera.setLayoutParams(imgParama);
        $ImgCameraIcon.setLayoutParams(imgParama);
        $ImgSiteTrip.setLayoutParams(imgParama);
        $ImgStoppage.setLayoutParams(imgParama);
        $ImgAcReport.setLayoutParams(imgParama);
        $ImgSafeParking.setLayoutParams(imgParama);
        $ImgFms.setLayoutParams(imgParama);

        $ImgPrimaryEngine.setLayoutParams(imgParama);
        $ImgSecondaryEngine.setLayoutParams(imgParama);

        $ImgFuelTheft.setLayoutParams(imgParama);
        //$ImgFuelsumm.setLayoutParams(imgParama);

        LinearLayout.LayoutParams txtParama = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        txtParama.width = width * 65 / 100;
        txtParama.height = (int) (height * 10 / 100);
        $TextMap.setLayoutParams(txtParama);
        $TextHistroy.setLayoutParams(txtParama);
        $TextInfo.setLayoutParams(txtParama);
        $TextLocate.setLayoutParams(txtParama);
        $TextReport.setLayoutParams(txtParama);
        $txtLiveTracking.setLayoutParams(txtParama);
        $txtNearBy.setLayoutParams(txtParama);
        $txtShowImage.setLayoutParams(txtParama);
        $txtTripSummary.setLayoutParams(txtParama);
        $txtDriverDetails.setLayoutParams(txtParama);
        $txtFuelFill.setLayoutParams(txtParama);
        $txtFuelTheft.setLayoutParams(txtParama);
        //$txtFuelsumma.setLayoutParams(txtParama);
        $txtTemperature.setLayoutParams(txtParama);
        $txtAlarmReport.setLayoutParams(txtParama);
        $txtSiteAnalysisReport.setLayoutParams(txtParama);
        $txtgetUrl.setLayoutParams(txtParama);
        $txtChangeName.setLayoutParams(txtParama);
        $txtCameraReport.setLayoutParams(txtParama);
        $txtCameraViewReport.setLayoutParams(txtParama);
        $txtSiteTripReport.setLayoutParams(txtParama);
        $txtStoppageReport.setLayoutParams(txtParama);
        $txtAcReport.setLayoutParams(txtParama);
        $txtSafeParking.setLayoutParams(txtParama);
        $txtFms.setLayoutParams(txtParama);
        $txtPrimaryEngine.setLayoutParams(txtParama);
        $txtSecondaryEngine.setLayoutParams(txtParama);

        $TextMap.setGravity(Gravity.CENTER | Gravity.LEFT);
        $TextHistroy.setGravity(Gravity.CENTER | Gravity.LEFT);
        $TextInfo.setGravity(Gravity.CENTER | Gravity.LEFT);
        $TextLocate.setGravity(Gravity.CENTER | Gravity.LEFT);
        $TextReport.setGravity(Gravity.CENTER | Gravity.LEFT);
        $txtLiveTracking.setGravity(Gravity.CENTER | Gravity.LEFT);
        $txtNearBy.setGravity(Gravity.CENTER | Gravity.LEFT);
        $txtShowImage.setGravity(Gravity.CENTER | Gravity.LEFT);
        $txtTripSummary.setGravity(Gravity.CENTER | Gravity.LEFT);
        $txtDriverDetails.setGravity(Gravity.CENTER | Gravity.LEFT);
        $txtFuelFill.setGravity(Gravity.CENTER | Gravity.LEFT);
        $txtFuelTheft.setGravity(Gravity.CENTER | Gravity.LEFT);
        //$txtFuelsumma.setGravity(Gravity.CENTER | Gravity.LEFT);
        $txtTemperature.setGravity(Gravity.CENTER | Gravity.LEFT);
        $txtAlarmReport.setGravity(Gravity.CENTER | Gravity.LEFT);
        $txtSiteAnalysisReport.setGravity(Gravity.CENTER | Gravity.LEFT);
        $txtgetUrl.setGravity(Gravity.CENTER | Gravity.LEFT);
        $txtChangeName.setGravity(Gravity.CENTER | Gravity.LEFT);
        $txtCameraReport.setGravity(Gravity.CENTER | Gravity.LEFT);
        $txtCameraViewReport.setGravity(Gravity.CENTER | Gravity.LEFT);
        $txtSiteTripReport.setGravity(Gravity.CENTER | Gravity.LEFT);
        $txtStoppageReport.setGravity(Gravity.CENTER | Gravity.LEFT);
        $txtAcReport.setGravity(Gravity.CENTER | Gravity.LEFT);
        $txtSafeParking.setGravity(Gravity.CENTER | Gravity.LEFT);
        $txtFms.setGravity(Gravity.CENTER | Gravity.LEFT);
        $txtPrimaryEngine.setGravity(Gravity.CENTER | Gravity.LEFT);
        $txtSecondaryEngine.setGravity(Gravity.CENTER | Gravity.LEFT);

        $TextMap.setPadding(width * 10 / 100, 0, 0, 0);
        $TextHistroy.setPadding(width * 10 / 100, 0, 0, 0);
        $TextInfo.setPadding(width * 10 / 100, 0, 0, 0);
        $TextLocate.setPadding(width * 10 / 100, 0, 0, 0);
        $TextReport.setPadding(width * 10 / 100, 0, 0, 0);
        $txtLiveTracking.setPadding(width * 10 / 100, 0, 0, 0);
        $txtNearBy.setPadding(width * 10 / 100, 0, 0, 0);
        $txtShowImage.setPadding(width * 10 / 100, 0, 0, 0);
        $txtTripSummary.setPadding(width * 10 / 100, 0, 0, 0);
        $txtDriverDetails.setPadding(width * 10 / 100, 0, 0, 0);
        $txtFuelFill.setPadding(width * 10 / 100, 0, 0, 0);
        $txtFuelTheft.setPadding(width * 10 / 100, 0, 0, 0);
        //$txtFuelsumma.setPadding(width * 10 / 100, 0, 0, 0);
        $txtTemperature.setPadding(width * 10 / 100, 0, 0, 0);
        $txtAlarmReport.setPadding(width * 10 / 100, 0, 0, 0);
        $txtSiteAnalysisReport.setPadding(width * 10 / 100, 0, 0, 0);
        $txtgetUrl.setPadding(width * 10 / 100, 0, 0, 0);
        $txtChangeName.setPadding(width * 10 / 100, 0, 0, 0);
        $txtCameraReport.setPadding(width * 10 / 100, 0, 0, 0);
        $txtCameraViewReport.setPadding(width * 10 / 100, 0, 0, 0);
        $txtSiteTripReport.setPadding(width * 10 / 100, 0, 0, 0);
        $txtStoppageReport.setPadding(width * 10 / 100, 0, 0, 0);
        $txtAcReport.setPadding(width * 10 / 100, 0, 0, 0);
        $txtSafeParking.setPadding(width * 10 / 100, 0, 0, 0);
        $txtFms.setPadding(width * 10 / 100, 0, 0, 0);
        $txtPrimaryEngine.setPadding(width * 10 / 100, 0, 0, 0);
        $txtSecondaryEngine.setPadding(width * 10 / 100, 0, 0, 0);

        LinearLayout.LayoutParams imgArrow = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        imgArrow.width = width * 10 / 100;
        imgArrow.height = height * 5 / 100;
        imgArrow.gravity = Gravity.CENTER | Gravity.LEFT;
        $ImgMapArrow.setLayoutParams(imgArrow);
        $ImgHistroyArrow.setLayoutParams(imgArrow);
        $ImgInfoArrow.setLayoutParams(imgArrow);
        $ImgLocateArrow.setLayoutParams(imgArrow);
        $ImgReportArrow.setLayoutParams(imgArrow);
        $ImgLiveTrackingArrow.setLayoutParams(imgArrow);
        $ImgNearByArrow.setLayoutParams(imgArrow);
        $ImgShowImageArrow.setLayoutParams(imgArrow);
        $ImgTripSummaryArrow.setLayoutParams(imgArrow);
        $ImgFuelFillArrow.setLayoutParams(imgArrow);
        $ImgTemperatureArrow.setLayoutParams(imgArrow);
        $ImgAlarmArrow.setLayoutParams(imgArrow);
        $ImgSiteAnalysisArrow.setLayoutParams(imgArrow);
        $ImggetUrlArrow.setLayoutParams(imgArrow);
        $ImgChangeNameArrow.setLayoutParams(imgArrow);
        $ImgCameraArrow.setLayoutParams(imgArrow);
        $ImgCameraViewArrow.setLayoutParams(imgArrow);
        $ImgSiteTripArrow.setLayoutParams(imgArrow);
        $ImgStoppageArrow.setLayoutParams(imgArrow);
        $ImgAcReportArrow.setLayoutParams(imgArrow);
        $ImgFmsArrow.setLayoutParams(imgArrow);
        $ImgPrimaryEngineArrow.setLayoutParams(imgArrow);
        $ImgSecondaryEngineArrow.setLayoutParams(imgArrow);

        $ImgFuelTheftArrow.setLayoutParams(imgArrow);
        //$ImgFuelsummArrow.setLayoutParams(imgArrow);

        $ImgDriverDetailsArrow.setLayoutParams(imgArrow);
        LinearLayout.LayoutParams viewParama = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        viewParama.width = width;
        viewParama.height = height * 1 / 2 / 100;
        $View1.setLayoutParams(viewParama);
        $View2.setLayoutParams(viewParama);
        $View3.setLayoutParams(viewParama);
        $View4.setLayoutParams(viewParama);
        $View5.setLayoutParams(viewParama);
        $View6.setLayoutParams(viewParama);
        $View7.setLayoutParams(viewParama);
        $View8.setLayoutParams(viewParama);
        $View9.setLayoutParams(viewParama);
        $View11.setLayoutParams(viewParama);
        $View12.setLayoutParams(viewParama);
        $View13.setLayoutParams(viewParama);
        $View14.setLayoutParams(viewParama);
        $View15.setLayoutParams(viewParama);
        $View16.setLayoutParams(viewParama)      ;
        $View26.setLayoutParams(viewParama);
        $View17.setLayoutParams(viewParama);
        $View18.setLayoutParams(viewParama);
        $View19.setLayoutParams(viewParama);
        $View20.setLayoutParams(viewParama);
        $View21.setLayoutParams(viewParama);
        $View22.setLayoutParams(viewParama);
        $View23.setLayoutParams(viewParama);
        $View24.setLayoutParams(viewParama);
        //$View25.setLayoutParams(viewParama);

        if (width >= 600) {
            $TextMap.setTextSize(16);
            $TextInfo.setTextSize(16);
            $TextLocate.setTextSize(16);
            $TextReport.setTextSize(16);
            $TextHistroy.setTextSize(16);
            $txtLiveTracking.setTextSize(16);
            $txtNearBy.setTextSize(16);
            $txtShowImage.setTextSize(16);
            $txtTripSummary.setTextSize(16);
            $txtDriverDetails.setTextSize(16);
            $txtFuelFill.setTextSize(16);
            $txtFuelTheft.setTextSize(16);
            // $txtFuelsumma.setTextSize(16);
            $txtTemperature.setTextSize(16);
            $txtAlarmReport.setTextSize(16);
            $txtSiteAnalysisReport.setTextSize(16);
            $txtgetUrl.setTextSize(16);
            $txtChangeName.setTextSize(16);
            $txtCameraReport.setTextSize(16);
            $txtCameraViewReport.setTextSize(16);
            $txtSiteTripReport.setTextSize(16);
            $txtStoppageReport.setTextSize(16);
            $txtAcReport.setTextSize(16);
            $txtSafeParking.setTextSize(16);
            $txtFms.setTextSize(16);
            $txtPrimaryEngine.setTextSize(16);
            $txtSecondaryEngine.setTextSize(16);
        } else if (width > 501 && width < 600) {
            $TextMap.setTextSize(15);
            $TextInfo.setTextSize(15);
            $TextLocate.setTextSize(15);
            $TextReport.setTextSize(15);
            $TextHistroy.setTextSize(15);
            $txtLiveTracking.setTextSize(15);
            $txtNearBy.setTextSize(15);
            $txtShowImage.setTextSize(15);
            $txtTripSummary.setTextSize(15);
            $txtDriverDetails.setTextSize(15);
            $txtFuelFill.setTextSize(15);
            $txtFuelTheft.setTextSize(15);
            // $txtFuelsumma.setTextSize(15);
            $txtTemperature.setTextSize(15);
            $txtAlarmReport.setTextSize(15);
            $txtSiteAnalysisReport.setTextSize(15);
            $txtgetUrl.setTextSize(15);
            $txtChangeName.setTextSize(15);
            $txtCameraReport.setTextSize(15);
            $txtCameraViewReport.setTextSize(15);
            $txtSiteTripReport.setTextSize(15);
            $txtStoppageReport.setTextSize(15);
            $txtAcReport.setTextSize(15);
            $txtSafeParking.setTextSize(15);
            $txtFms.setTextSize(15);
            $txtPrimaryEngine.setTextSize(15);
            $txtSecondaryEngine.setTextSize(15);
        } else if (width > 260 && width < 500) {
            $TextMap.setTextSize(14);
            $TextInfo.setTextSize(14);
            $TextLocate.setTextSize(14);
            $TextReport.setTextSize(14);
            $TextHistroy.setTextSize(14);
            $txtLiveTracking.setTextSize(14);
            $txtNearBy.setTextSize(14);
            $txtShowImage.setTextSize(14);
            $txtTripSummary.setTextSize(14);
            $txtDriverDetails.setTextSize(14);
            $txtFuelFill.setTextSize(14);
            $txtFuelTheft.setTextSize(14);
            // $txtFuelsumma.setTextSize(14);
            $txtTemperature.setTextSize(14);
            $txtAlarmReport.setTextSize(14);
            $txtSiteAnalysisReport.setTextSize(14);
            $txtgetUrl.setTextSize(14);
            $txtChangeName.setTextSize(14);
            $txtCameraReport.setTextSize(14);
            $txtCameraViewReport.setTextSize(14);
            $txtSiteTripReport.setTextSize(14);
            $txtStoppageReport.setTextSize(14);
            $txtAcReport.setTextSize(14);
            $txtSafeParking.setTextSize(14);
            $txtFms.setTextSize(14);
            $txtPrimaryEngine.setTextSize(14);
            $txtSecondaryEngine.setTextSize(14);
        } else if (width <= 260) {
            $TextMap.setTextSize(13);
            $TextInfo.setTextSize(13);
            $TextLocate.setTextSize(13);
            $TextReport.setTextSize(13);
            $TextHistroy.setTextSize(13);
            $txtLiveTracking.setTextSize(13);
            $txtNearBy.setTextSize(13);
            $txtShowImage.setTextSize(13);
            $txtTripSummary.setTextSize(13);
            $txtDriverDetails.setTextSize(13);
            $txtFuelFill.setTextSize(13);
            $txtFuelTheft.setTextSize(13);
            //  $txtFuelsumma.setTextSize(13);
            $txtTemperature.setTextSize(13);
            $txtAlarmReport.setTextSize(13);
            $txtSiteAnalysisReport.setTextSize(13);
            $txtgetUrl.setTextSize(13);
            $txtChangeName.setTextSize(13);
            $txtCameraReport.setTextSize(13);
            $txtCameraViewReport.setTextSize(13);
            $txtSiteTripReport.setTextSize(13);
            $txtStoppageReport.setTextSize(13);
            $txtAcReport.setTextSize(13);
            $txtSafeParking.setTextSize(13);
            $txtFms.setTextSize(13);
            $txtPrimaryEngine.setTextSize(13);
            $txtSecondaryEngine.setTextSize(13);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (Constant.SELECTED_VEHICLE_ID == null) {
            startActivity(new Intent(MapMenuActivity.this, VehicleListActivity.class));
            finish();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();

        if (myTimer != null) {
            myTimer.cancel();
        }

    }


}
