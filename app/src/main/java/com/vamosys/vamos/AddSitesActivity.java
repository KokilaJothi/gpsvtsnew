package com.vamosys.vamos;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import androidx.annotation.NonNull;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import androidx.fragment.app.FragmentManager;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.vamosys.adapter.SiteListAdapterNew;
import com.vamosys.model.Site;
import com.vamosys.utils.ConnectionDetector;
import com.vamosys.utils.Constant;
import com.vamosys.utils.HttpConfig;
import com.vamosys.utils.LetterAlphabeticalOrder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.osmdroid.api.IMapController;
import org.osmdroid.events.MapEventsReceiver;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.MapEventsOverlay;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.regex.Pattern;

public class AddSitesActivity extends AppCompatActivity implements View.OnClickListener, OnMapReadyCallback {


    private static GoogleMap map;
    static float vehicle_zoom_level = 15.5F;
    String mSELECTED_MAP_TYPE = "Normal";

    ImageView $ChangeView;
    LinearLayout mGoogleMapLayout;
    Toolbar mToolbar;

    ConnectionDetector cd;
    SharedPreferences sp;
    static int width;
    static int height;

    EditText mEdtSiteName;
    Spinner mSpinnerSiteType, mSpinnerOrgId;
    Button mBtnAdd, mBtnUpdate, mBtnClearMap;
    ListView mLstSites;

    String mSiteType = null, mOrgType = null, mSiteName = null, mSiteNameOld = null, mSiteLatLngOld = null;

    List<LatLng> polylinePoints = null;

    List<GeoPoint> osmPolylinePoints = new ArrayList<>();
    Menu menu;

    TextView mTxtNoRecord;
    LinearLayoutManager layoutManager5;
    RecyclerView mRVSiteList;
    View bottomSheetSiteList;
    BottomSheetBehavior behaviorSiteList;
    LinearLayout mLayoutAddUpdateSite;
    boolean bsSiteListEnabled = false;

    SiteListAdapterNew siteListAdapter;

    String[] mSiteTypeArray = {"Select Type", "Home Site", "Client Site", "Restricted Site", "Other"};

    ArrayList<String> mOrgList = new ArrayList<>();
    ArrayList<Site> mSiteList = new ArrayList<>();
    ArrayList<Site> mSiteListAdapter = new ArrayList<>();
    String[] group;

    MapView mapview;
    boolean isOsmEnabled = true;
    private IMapController mapController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_sites);
        sp = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        cd = new ConnectionDetector(getApplicationContext());

        if (sp.getString("enabled_map", "") != null) {
            if (sp.getString("enabled_map", "").trim().length() > 0) {

                System.out.println("hi enabled map is " + sp.getString("enabled_map", ""));

                if (sp.getString("enabled_map", "").equalsIgnoreCase(getResources().getString(R.string.osm))) {
                    isOsmEnabled = true;
                } else {
                    isOsmEnabled = false;
                }
            }
        }


        if (sp.getString("ip_adds", "") != null) {
            if (sp.getString("ip_adds", "").trim().length() > 0) {
                Const.API_URL = sp.getString("ip_adds", "");
            }
        }

        init();
        group = Constant.SELECTED_GROUP.split(":");
        //set spinner adapter
        setSiteTypeDropDownData(null);

        if (cd.isConnectingToInternet()) {
            new getSiteList().execute();
        } else {
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.network_connection),
                    Toast.LENGTH_SHORT).show();
        }


        mSpinnerOrgId
                .setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                    @Override
                    public void onItemSelected(AdapterView<?> parent,
                                               View view, int pos, long id) {
                        // TODO Auto-generated method stub

                        if (pos == 0) {
                            mOrgType = null;
                        } else {
                            mOrgType = mOrgList.get(pos).trim();
                        }

                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> arg0) {
                        // TODO Auto-generated method stub

                    }

                });


        mSpinnerSiteType
                .setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                    @Override
                    public void onItemSelected(AdapterView<?> parent,
                                               View view, int pos, long id) {
                        // TODO Auto-generated method stub

                        if (pos == 0) {
                            mSiteType = null;
                        } else {
                            mSiteType = mSiteTypeArray[pos];
                        }

                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> arg0) {
                        // TODO Auto-generated method stub

                    }

                });


    }

    private void init() {


        polylinePoints = new ArrayList<LatLng>();
        siteListAdapter = new SiteListAdapterNew(AddSitesActivity.this, mSiteListAdapter, new SiteListAdapterNew.OnItemClickListener() {
            @Override
            public void onClicked(int i) {
//                System.out.println("Hi onclicked " + i);
                //delete the data


                String mUrl = Const.API_URL + "mobile/deleteSite?fcode=" + group[1] + "&orgId=" + mSiteListAdapter.get(i).getOrgId() + "&siteName=" + mSiteListAdapter.get(i).getSiteName() + "&userId=" + Constant.SELECTED_USER_ID;

                showDeleteDialog(mUrl);

//                new deleteSite().execute(mUrl);

            }

            @Override
            public void onLongClicked(int i) {
//                System.out.println("Hi onLongClicked " + i);
                closeBottomSheet();
                setUpdateData(mSiteListAdapter.get(i));
                //edit the data
            }
        });

        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        height = metrics.heightPixels;
        width = metrics.widthPixels;
        Constant.ScreenWidth = width;
        Constant.ScreenHeight = height;

        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mToolbar.setTitle(getResources().getString(R.string.add_site));
        mToolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_action_back));
        setSupportActionBar(mToolbar);

        mLayoutAddUpdateSite = (LinearLayout) findViewById(R.id.layout_add_update_site);
        mLayoutAddUpdateSite.setVisibility(View.VISIBLE);
        mTxtNoRecord = (TextView) findViewById(R.id.txt_no_sites);

        $ChangeView = (ImageView) findViewById(R.id.site_data_map_ViewIcon);


        mEdtSiteName = (EditText) findViewById(R.id.edttxt_site_name);
        mSpinnerSiteType = (Spinner) findViewById(R.id.spinner_site_type);
        mSpinnerOrgId = (Spinner) findViewById(R.id.spinner_site_orgid);
        mBtnAdd = (Button) findViewById(R.id.btn_add_sites);
        mBtnUpdate = (Button) findViewById(R.id.btn_update_sites);
        mBtnClearMap = (Button) findViewById(R.id.btn_clear_map);
        mLstSites = (ListView) findViewById(R.id.site_list_view);

        $ChangeView.setOnClickListener(this);
        mBtnAdd.setOnClickListener(this);
        mBtnUpdate.setOnClickListener(this);
        mBtnClearMap.setOnClickListener(this);

        mBtnAdd.setVisibility(View.VISIBLE);
        mBtnUpdate.setVisibility(View.GONE);

        $ChangeView.setVisibility(View.VISIBLE);
//        mGoogleMapLayout.setVisibility(View.VISIBLE);


        mGoogleMapLayout = (LinearLayout) findViewById(R.id.google_map_layout);
        RelativeLayout rl = (RelativeLayout) findViewById(R.id.map_view_relativelayout);
        if (isOsmEnabled) {
            $ChangeView.setVisibility(View.GONE);
            mGoogleMapLayout.setVisibility(View.GONE);
            rl.setVisibility(View.VISIBLE);
            mapview = new MapView(this);
            mapview.setTilesScaledToDpi(true);
            rl.addView(mapview, new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.FILL_PARENT,
                    RelativeLayout.LayoutParams.FILL_PARENT));

            mapview.setBuiltInZoomControls(false);
            mapview.setMultiTouchControls(true);
            mapController = mapview.getController();

            GeoPoint newPos = new GeoPoint(22.4209204, 79.7445752);
            mapController.setCenter(newPos);
            mapController.setZoom(5);
            setupOsmMap();


        } else {
            $ChangeView.setVisibility(View.VISIBLE);
            mGoogleMapLayout.setVisibility(View.VISIBLE);
            rl.setVisibility(View.GONE);

            FragmentManager myFragmentManager = getSupportFragmentManager();
            SupportMapFragment myMapFragment = (SupportMapFragment) myFragmentManager
                    .findFragmentById(R.id.add_site_map);
            myMapFragment.getMapAsync(this);
        }

//        FragmentManager myFragmentManager = getSupportFragmentManager();
//        SupportMapFragment myMapFragment = (SupportMapFragment) myFragmentManager
//                .findFragmentById(R.id.add_site_map);
//        myMapFragment.getMapAsync(this);


        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(AddSitesActivity.this, VehicleListActivity.class));
                finish();


            }
        });

        layoutManager5 = new LinearLayoutManager(this);
        mRVSiteList = (RecyclerView) findViewById(R.id.recyclerview_site_list);
        mRVSiteList.setHasFixedSize(true);
        mRVSiteList.setLayoutManager(layoutManager5);
        mRVSiteList.setAdapter(siteListAdapter);


        bottomSheetSiteList = findViewById(R.id.bottom_sheet_site_list);
        behaviorSiteList = BottomSheetBehavior.from(bottomSheetSiteList);
        behaviorSiteList.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                // React to state change
//                System.out.println("Hi state 0000" + bottomSheetEnabled);
                if (newState == 4 || newState == 5) {
                    bsSiteListEnabled = false;

                    MenuItem item3 = menu.findItem(R.id.action_site_list);
                    MenuItem item4 = menu.findItem(R.id.action_add_site);
                    MenuItem item5 = menu.findItem(R.id.search);

                    item3.setVisible(true);
                    item4.setVisible(false);
                    item5.setVisible(false);

                    mToolbar.collapseActionView();
                    if (!searchView.isIconified()) {
                        searchView.onActionViewCollapsed();
//                        item6.setVisible(false);
                    }

                    mLayoutAddUpdateSite.setVisibility(View.VISIBLE);
                    mTxtNoRecord.setVisibility(View.GONE);
                    mRVSiteList.setVisibility(View.GONE);

                } else if (newState == 3) {
                    bsSiteListEnabled = true;

                    MenuItem item3 = menu.findItem(R.id.action_site_list);
                    MenuItem item4 = menu.findItem(R.id.action_add_site);
                    MenuItem item5 = menu.findItem(R.id.search);

                    item3.setVisible(false);
                    item4.setVisible(true);
                    item5.setVisible(true);
                }

            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
                // React to dragging events
            }
        });

        mEdtSiteName.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
//                Log.i("Test", "Enter 111 pressed " + event.getKeyCode());
                if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (actionId == EditorInfo.IME_ACTION_DONE)) {
//                    Log.i("Test", "Enter pressed");
                    if (mEdtSiteName.getText().toString().trim().length() > 0) {
                        hideKeyboard();
                        mEdtSiteName.clearFocus();
                        mSpinnerSiteType.requestFocus();
                        mSpinnerSiteType.performClick();
                    } else {
                        mEdtSiteName.setError(getResources().getString(R.string.hint_site_name));
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.enter_site_name), Toast.LENGTH_SHORT).show();
                    }
                }
                return true;
            }
        });

    }

    private void hideKeyboard() {
        InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        inputManager.hideSoftInputFromWindow(this.getCurrentFocus().getWindowToken(),
                InputMethodManager.HIDE_NOT_ALWAYS);
    }

    private void setupOsmMap() {

        mapview.getOverlays().clear();


        MapEventsReceiver mReceive = new MapEventsReceiver() {
            @Override
            public boolean singleTapConfirmedHelper(GeoPoint p) {
//                    Toast.makeText(getBaseContext(),p.getLatitude() + " - "+p.getLongitude(),Toast.LENGTH_LONG).show();
                updateOsmPolyLine(p.getLatitude(), p.getLongitude());
                return false;
            }

            @Override
            public boolean longPressHelper(GeoPoint p) {
                return false;
            }
        };


        MapEventsOverlay OverlayEvents = new MapEventsOverlay(getBaseContext(), mReceive);
        mapview.getOverlays().add(OverlayEvents);
        mapview.invalidate();
        mapview.getOverlays().add(Constant.getTilesOverlay(getApplicationContext()));
    }

    private void setUpdateData(Site site) {
        if (site != null) {

            mBtnAdd.setVisibility(View.GONE);
            mBtnUpdate.setVisibility(View.VISIBLE);

            mEdtSiteName.setText(site.getSiteName());
            mBtnAdd.setVisibility(View.GONE);

            mSiteNameOld = site.getSiteName();
            mSiteLatLngOld = site.getLatLng();

            setSiteTypeDropDownData(site.getSiteType());
            setOrgIdDropDownData(site.getOrgId());
            mToolbar.setTitle(getResources().getString(R.string.update_site));

            if (isOsmEnabled) {
//                mapController.setCenter(newPos);
                osmPolylinePoints = new ArrayList<>();
                mapController.setZoom(12);
//                mapview.getOverlays().clear();
//                mapview.invalidate();

                setupOsmMap();
            } else {
                if (map != null) {
                    polylinePoints = new ArrayList<>();
                    map.clear();
                }
            }
            String mLatLng = site.getLatLng();

            String[] latLngArray = mLatLng.split(",");

            for (int i = 0; i < latLngArray.length; i++) {


                String[] latLng = latLngArray[i].split(":");

                Double la = Double.parseDouble(latLng[0]);
                Double ln = Double.parseDouble(latLng[1]);

                if (isOsmEnabled) {


                    if (osmPolylinePoints.size() == 0) {
                        updateOsmPolyLine(la, ln);
                        GeoPoint newPos = new GeoPoint(la, ln);
                        mapController.setCenter(newPos);
                        mapController.setCenter(newPos);
                    } else {
                        updateOsmPolyLine(la, ln);
                    }

                } else {

                    LatLng laln = new LatLng(la, ln);

                    if (polylinePoints.size() == 0) {

                        map.animateCamera(CameraUpdateFactory.newLatLngZoom(laln, 12.0f));

                        polyLine = initializePolyLine(laln);
                    } else {
                        updatePolyLine(laln);
                    }
                }

            }


        }
        mLayoutAddUpdateSite.setVisibility(View.VISIBLE);
        mTxtNoRecord.setVisibility(View.GONE);
        mRVSiteList.setVisibility(View.GONE);

    }


    private void optionPopUp() {
        // TODO Auto-generated method stub
        final Dialog dialog = new Dialog(AddSitesActivity.this);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.radio_popup);
        dialog.show();

        RadioGroup rg_home = (RadioGroup) dialog.findViewById(R.id.rg_home_views);
        RadioGroup rg_history = (RadioGroup) dialog.findViewById(R.id.rg_history_views);
        rg_history.clearCheck();
        rg_history.setVisibility(View.GONE);
        rg_home.setVisibility(View.VISIBLE);

        RadioButton $Normal = (RadioButton) dialog
                .findViewById(R.id.rb_home_normal);
        RadioButton $Satelite = (RadioButton) dialog
                .findViewById(R.id.rb_home_satellite);
        RadioButton $Terrain = (RadioButton) dialog
                .findViewById(R.id.rb_home_terrain);
        RadioButton $Hybrid = (RadioButton) dialog
                .findViewById(R.id.rb_home_hybrid);


        if (mSELECTED_MAP_TYPE.equalsIgnoreCase("Normal")) {
            $Normal.setChecked(true);
            $Satelite.setChecked(false);
            $Terrain.setChecked(false);
            $Hybrid.setChecked(false);
        } else if (mSELECTED_MAP_TYPE.equalsIgnoreCase("Satelite")) {
            $Normal.setChecked(false);
            $Satelite.setChecked(true);
            $Terrain.setChecked(false);
            $Hybrid.setChecked(false);
        } else if (mSELECTED_MAP_TYPE.equalsIgnoreCase("Terrain")) {
            $Normal.setChecked(false);
            $Satelite.setChecked(false);
            $Terrain.setChecked(true);
            $Hybrid.setChecked(false);
        } else if (mSELECTED_MAP_TYPE.equalsIgnoreCase("Hybrid")) {
            $Normal.setChecked(false);
            $Satelite.setChecked(false);
            $Terrain.setChecked(false);
            $Hybrid.setChecked(true);
        }
        $Normal.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

//                mapview.setTileSource(TileSourceFactory.MAPNIK);

                map.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                mSELECTED_MAP_TYPE = "Normal";
                dialog.dismiss();
            }
        });
        $Satelite.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                mSELECTED_MAP_TYPE = "Satelite";
                map.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
//                mapview.setTileSource(TileSourceFactory.USGS_SAT);
                dialog.dismiss();
            }
        });
        $Terrain.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                mSELECTED_MAP_TYPE = "Terrain";
                map.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
//                mapview.setTileSource(TileSourceFactory.HIKEBIKEMAP);
                dialog.dismiss();
            }
        });

        $Hybrid.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                mSELECTED_MAP_TYPE = "Hybrid";
                map.setMapType(GoogleMap.MAP_TYPE_HYBRID);
//                mapview.setTileSource(TileSourceFactory.USGS_TOPO);
                dialog.dismiss();
            }
        });

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        height = displayMetrics.heightPixels;
        width = displayMetrics.widthPixels;

        LinearLayout.LayoutParams radioParama = new LinearLayout.LayoutParams(
                FrameLayout.LayoutParams.WRAP_CONTENT,
                FrameLayout.LayoutParams.WRAP_CONTENT);
        radioParama.width = width * 50 / 100;
        radioParama.height = width * 10 / 100;
        radioParama.topMargin = height * 4 / 100;
        radioParama.gravity = Gravity.CENTER;
        radioParama.leftMargin = height * 4 / 100;
        $Normal.setLayoutParams(radioParama);
        $Satelite.setLayoutParams(radioParama);
        $Terrain.setLayoutParams(radioParama);

        LinearLayout.LayoutParams radioterrainParama = new LinearLayout.LayoutParams(
                FrameLayout.LayoutParams.WRAP_CONTENT,
                FrameLayout.LayoutParams.WRAP_CONTENT);
        radioterrainParama.width = width * 50 / 100;
        radioterrainParama.height = width * 10 / 100;
        radioterrainParama.topMargin = height * 4 / 100;
        radioterrainParama.gravity = Gravity.CENTER;
        radioterrainParama.leftMargin = height * 4 / 100;
        radioterrainParama.bottomMargin = height * 4 / 100;
        $Hybrid.setLayoutParams(radioterrainParama);

        if (width >= 600) {
            $Normal.setTextSize(16);
            $Satelite.setTextSize(16);
            $Terrain.setTextSize(16);
            $Hybrid.setTextSize(16);
        } else if (width > 501 && width < 600) {
            $Normal.setTextSize(15);
            $Satelite.setTextSize(15);
            $Terrain.setTextSize(15);
            $Hybrid.setTextSize(15);
        } else if (width > 260 && width < 500) {
            $Normal.setTextSize(14);
            $Satelite.setTextSize(14);
            $Terrain.setTextSize(14);
            $Hybrid.setTextSize(14);
        } else if (width <= 260) {
            $Normal.setTextSize(13);
            $Satelite.setTextSize(13);
            $Terrain.setTextSize(13);
            $Hybrid.setTextSize(13);
        }
    }

    private Polyline polyLine;

    private Polyline initializePolyLine(LatLng mStartLocation) {
        PolylineOptions rectOptions = new PolylineOptions();
        rectOptions.add(mStartLocation);
        polylinePoints.add(mStartLocation);
        rectOptions.color(getResources().getColor(R.color.history_polyline_color));
        return map.addPolyline(rectOptions);
    }

    /**
     * Add the marker to the polyline.
     */
    private void updatePolyLine(LatLng latLng) {
//        List<LatLng> points = polyLine.getPoints();
//        points.add(latLng);
//        polylinePoints = polyLine.getPoints();
        polylinePoints.add(latLng);
//        polyLine.setPoints(points);
        polyLine.setPoints(polylinePoints);
    }


    private void updateOsmPolyLine(double lat, double lng) {

        GeoPoint newPos = new GeoPoint(lat, lng);
        osmPolylinePoints.add(newPos);
//            System.out.println("Hi polyline points list is " + polylinePoints.size() + " latlng to add in list lat " + lat + " lng " + lng);
        org.osmdroid.views.overlay.Polyline myPath = new org.osmdroid.views.overlay.Polyline();
        myPath.setColor(getResources().getColor(R.color.history_polyline_color));
        myPath.setPoints(osmPolylinePoints);

        mapview.getOverlays().add(myPath);
//
        mapview.invalidate();


    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        map.setPadding(1, 1, 1, 150);
        map.getUiSettings().setZoomControlsEnabled(true);

        map.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(20.4434884, 78.4085662), 12.0f));

//        map.setOnMapClickListener(new GoogleMap.OnMapClickListener(){
//            void onMapClick(LatLng point){
//                Toast.makeText(getApplicationContext(),
//                        point.latitude + ", " + point.longitude,
//                        Toast.LENGTH_SHORT).show();
//            }
//        });


        map.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng point) {
//                android.util.Log.i("onMapClick", "Horray!  " + point.latitude + ", " + point.longitude + " polyline size is" + polylinePoints.size());


                if (polylinePoints.size() == 0) {

//                    System.out.println("Hi inistialize polyline called ");

                    polyLine = initializePolyLine(point);
                } else {
                    updatePolyLine(point);
                }

//                Toast.makeText(getApplicationContext(),
//                        point.latitude + ", " + point.longitude,
//                        Toast.LENGTH_SHORT).show();
//            }

            }
        });

    }

    SearchView searchView;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.menu = menu;
        getMenuInflater().inflate(R.menu.menu_addsite, menu);

        MenuItem myActionMenuItem = menu.findItem(R.id.search);
        searchView = (SearchView) myActionMenuItem.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {

                System.out.println("Hi query text change is " + newText);

                if (TextUtils.isEmpty(newText)) {
                    // adapter.filter("");
                    //  listView.clearTextFilter();
                    getSearchText(null);
                } else {
                    // adapter.filter(newText);


                    getSearchText(String.valueOf(newText).trim());


                }
                return true;
            }
        });

        return true;
    }

    //    MenuItem item1,item2;
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // action with ID action_refresh was selected

            case R.id.action_site_list:
//                storeFavourtite();

                MenuItem item1 = menu.findItem(R.id.action_site_list);
                MenuItem item2 = menu.findItem(R.id.action_add_site);
                MenuItem item5 = menu.findItem(R.id.search);

                item1.setVisible(false);
                item2.setVisible(true);
                item5.setVisible(true);

                openCloseBSLayout();

                if (mSiteListAdapter.size() == 0) {
                    mLayoutAddUpdateSite.setVisibility(View.GONE);
                    mTxtNoRecord.setVisibility(View.VISIBLE);
                    mRVSiteList.setVisibility(View.GONE);
                } else {
//                        mLayoutAddUpdateSite.setVisibility(View.VISIBLE);
                    mLayoutAddUpdateSite.setVisibility(View.GONE);
                    mTxtNoRecord.setVisibility(View.GONE);
                    mRVSiteList.setVisibility(View.VISIBLE);
                }


                break;
            case R.id.action_add_site:
                mToolbar.setTitle(getResources().getString(R.string.add_site));
                closeBottomSheet();
                mBtnAdd.setVisibility(View.VISIBLE);
                mBtnUpdate.setVisibility(View.GONE);
                mEdtSiteName.setText("");
                setOrgIdDropDownData(null);
                setSiteTypeDropDownData(null);

                MenuItem item3 = menu.findItem(R.id.action_site_list);
                MenuItem item4 = menu.findItem(R.id.action_add_site);
                MenuItem item6 = menu.findItem(R.id.search);

                item3.setVisible(true);
                item4.setVisible(false);
                item6.setVisible(false);

                mLayoutAddUpdateSite.setVisibility(View.VISIBLE);
                mTxtNoRecord.setVisibility(View.GONE);
                mRVSiteList.setVisibility(View.GONE);

                mToolbar.collapseActionView();
                if (!searchView.isIconified()) {
                    searchView.onActionViewCollapsed();
                    item6.setVisible(false);
                }

                if (isOsmEnabled) {
//                mapController.setCenter(newPos);
                    osmPolylinePoints = new ArrayList<>();
                    mapController.setZoom(12);
//                mapview.getOverlays().clear();
//                mapview.invalidate();

                    setupOsmMap();
                } else {
                    if (map != null) {
                        polylinePoints = new ArrayList<>();
                        map.clear();
                    }
                }

//                mAcReportDataMapLayout.setVisibility(View.GONE);
////                if (isMapPresent) {
//                isMapPresent = false;
//                lv.setVisibility(View.VISIBLE);
//                item.setVisible(false);
//                mImgMapViewClose.setVisibility(View.GONE);

                break;

            default:
                break;
        }

        return true;
    }

    private void closeBottomSheet() {
        behaviorSiteList.setState(BottomSheetBehavior.STATE_COLLAPSED);
        bsSiteListEnabled = false;
    }

    private void openCloseBSLayout() {
//        setSiteListData();
        if (!bsSiteListEnabled) {
            behaviorSiteList.setState(BottomSheetBehavior.STATE_EXPANDED);

        } else {
            // setData();
            behaviorSiteList.setState(BottomSheetBehavior.STATE_COLLAPSED);


        }

        if (bsSiteListEnabled) {
            bsSiteListEnabled = false;
        } else {
            bsSiteListEnabled = true;
        }
    }


//    private void setSiteListData() {
//        rcrAdapter = new RateCutRsAdapter(AddReceipt.this, AddReceiptSingleTon.getInstance().getRcrList());
//        mRVSiteList.setAdapter(rcrAdapter);
//        fmAdapter.notifyDataSetChanged();
//    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.site_data_map_ViewIcon:
                optionPopUp();
                break;
            case R.id.btn_clear_map:
                if (isOsmEnabled) {
                    osmPolylinePoints = new ArrayList<>();
//                    mapview.getOverlays().clear();
//                    mapview.invalidate();
                    setupOsmMap();
                } else {
                    if (map != null) {
                        polylinePoints = new ArrayList<>();
                        map.clear();
                    }
                }
                break;
            case R.id.btn_update_sites:
                if (validateFields()) {
//save the site
//                    String mUrl = Const.API_URL + "mobile/updateGeoFences?siteName=" + mEdtSiteName.getText().toString().trim() + "&fcode=" + group[1] + "&orgId=" + mOrgType + "&siteType=" + mSiteType + "&userId=" + Constant.SELECTED_USER_ID;
//                    String mUrl = Const.API_URL + "mobile/saveSite?latLng=" + mSiteLatLngOld + "&fcode=" + group[1] + "&orgId=" + mOrgType + "&siteName=" + mEdtSiteName.getText().toString().trim() + "&siteType=" + mSiteType
//                            + "&userId=" + Constant.SELECTED_USER_ID + "&type=update&siteNameOld=" + mSiteNameOld;
                    if (isPolylinePointsValid()) {
                        String mUrl = Const.API_URL + "mobile/saveSite?latLng=" + getLatLngList() + "&fcode=" + group[1] + "&orgId=" + mOrgType + "&siteName=" + mEdtSiteName.getText().toString().trim() + "&siteType=" + mSiteType
                                + "&userId=" + Constant.SELECTED_USER_ID + "&type=update&siteNameOld=" + mSiteNameOld;

                        if (cd.isConnectingToInternet()) {
                            new addSite().execute(mUrl);
                        } else {
                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.network_connection), Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(this, getResources().getString(R.string.area_in_map), Toast.LENGTH_SHORT).show();
                    }
                }
                break;

            case R.id.btn_add_sites:

                if (validateFields()) {
//                    if (polylinePoints != null && polylinePoints.size() > 0) {

                    if (isPolylinePointsValid()) {
                        String mUrl = Const.API_URL + "mobile/saveSite?latLng=" + getLatLngList() + "&fcode=" + group[1] + "&orgId=" + mOrgType + "&siteName=" + mEdtSiteName.getText().toString().trim() + "&siteType=" + mSiteType + "&userId=" + Constant.SELECTED_USER_ID;
                        if (cd.isConnectingToInternet()) {
                            new addSite().execute(mUrl);
                        } else {
                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.network_connection), Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(this, getResources().getString(R.string.area_in_map), Toast.LENGTH_SHORT).show();
                    }
                }

                break;

        }
    }


    private void showDeleteDialog(final String mUrl) {
        AlertDialog.Builder builder = new AlertDialog.Builder(AddSitesActivity.this, R.style.MyAlertDialogStyle);
        builder.setTitle(getResources().getString(R.string.alert));
        builder.setMessage(getResources().getString(R.string.site_delete_confirm_message)).setCancelable(true).setPositiveButton(getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();


                new deleteSite().execute(mUrl);

            }
        }).setNegativeButton(getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int i) {
                dialog.cancel();

            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    private boolean isPolylinePointsValid() {
        if (isOsmEnabled && osmPolylinePoints.size() > 0) {
            return true;
        } else if (!isOsmEnabled && polylinePoints.size() > 0) {

            return true;
        } else {
            Toast.makeText(this, getResources().getString(R.string.area_in_map), Toast.LENGTH_SHORT).show();
        }
        return false;
    }


//    private boolean isPolylinePointsValid() {
//        if (polylinePoints.size() > 0) {
//            return true;
//        } else {
//            Toast.makeText(this, "Please select area in map", Toast.LENGTH_SHORT).show();
//        }
//        return false;
//    }

    private boolean validateFields() {
//        if (polylinePoints.size() > 0) {
        if (mSiteType != null) {
            if (mOrgType != null) {
                if (this.mEdtSiteName.getText().toString().trim().length() != 0) {
                    this.mEdtSiteName.setError(null);


                    return true;

                } else {
                    this.mEdtSiteName.setError(getResources().getString(R.string.area_in_map));
                }

            } else {
                Toast.makeText(this, getResources().getString(R.string.select_organisation), Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(this, getResources().getString(R.string.select_site_type), Toast.LENGTH_SHORT).show();
        }
//        } else {
//            Toast.makeText(this, "Please select area in map", Toast.LENGTH_SHORT).show();
//        }
        return false;
    }

    private String getLatLngList() {

        String mReturnLatLngList = "";

        if (isOsmEnabled) {

            if (osmPolylinePoints.size() > 0) {
                for (int i = 0; i < osmPolylinePoints.size(); i++) {
                    String mLatLng = osmPolylinePoints.get(i).getLatitude() + ":" + osmPolylinePoints.get(i).getLongitude();
                    if (mLatLng != null) {
                        if ((osmPolylinePoints.size() == 1) || (i == osmPolylinePoints.size() - 1)) {
                            mReturnLatLngList = mReturnLatLngList + mLatLng;
                        } else {
                            mReturnLatLngList = mReturnLatLngList + mLatLng + ",";
                        }
                    }
                }
            }

        } else {
            if (polylinePoints.size() > 0) {
                for (int i = 0; i < polylinePoints.size(); i++) {
                    String mLatLng = polylinePoints.get(i).latitude + ":" + polylinePoints.get(i).longitude;
                    if (mLatLng != null) {
                        if ((polylinePoints.size() == 1) || (i == polylinePoints.size() - 1)) {
                            mReturnLatLngList = mReturnLatLngList + mLatLng;
                        } else {
                            mReturnLatLngList = mReturnLatLngList + mLatLng + ",";
                        }
                    }
                }
            }
        }

        return mReturnLatLngList;
    }


    public void setOrgIdDropDownData(String data) {

        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(
                AddSitesActivity.this, R.layout.spinner_row,
                mOrgList) {

            public View getView(int position, View convertView, ViewGroup parent) {
                View v = super.getView(position, convertView, parent);

                return v;
            }

            public View getDropDownView(int position, View convertView,
                                        ViewGroup parent) {
                View v = super.getDropDownView(position, convertView, parent);

                return v;
            }
        };
        spinnerArrayAdapter
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mSpinnerOrgId.setAdapter(spinnerArrayAdapter);

        if (data != null) {
            int spinnerPosition = spinnerArrayAdapter.getPosition(data);
            mSpinnerOrgId.setSelection(spinnerPosition);
        }

    }

    public void setSiteTypeDropDownData(String data) {

        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(
                AddSitesActivity.this, R.layout.spinner_row,
                mSiteTypeArray) {

            public View getView(int position, View convertView, ViewGroup parent) {
                View v = super.getView(position, convertView, parent);

                return v;
            }

            public View getDropDownView(int position, View convertView,
                                        ViewGroup parent) {
                View v = super.getDropDownView(position, convertView, parent);

                return v;
            }
        };
        spinnerArrayAdapter
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mSpinnerSiteType.setAdapter(spinnerArrayAdapter);
        if (data != null) {
            int spinnerPosition = spinnerArrayAdapter.getPosition(data);
            mSpinnerSiteType.setSelection(spinnerPosition);
        }
    }


    public void getSearchText(String ch) {

//        System.out.println("Hi search text is " + ch);
        mSiteListAdapter.clear();
        ArrayList<Site> list = new ArrayList<Site>();
        if (ch != null && ch.length() > 0) {


            list.clear();

            for (int i = 0; i < mSiteList.size(); i++) {
                Site dc = new Site();

                if (mSiteList.get(i).getSiteName() != null) {
                    if (Pattern
                            .compile(Pattern.quote(ch), Pattern.CASE_INSENSITIVE)
                            .matcher(mSiteList.get(i).getSiteName())
                            .find()) {
//                        System.out.println("Hiiiiiii " + mSiteList.get(i).getSiteName());
                        dc = mSiteList.get(i);
                        list.add(dc);
                        mSiteListAdapter.add(dc);
                    }
                }

            }

        } else {
            list = mSiteList;
            mSiteListAdapter.addAll(mSiteList);
        }

//        System.out.println("The searched list size is :::::" + list.size());


//        System.out.println("List size after filtered is :::::"
//                + mVehicleLocationListAdapter.size());

//        if (list.size() > 0) {
//            mSiteListAdapter = list;


        Collections.sort(mSiteListAdapter, new LetterAlphabeticalOrder());

        siteListAdapter.notifyDataSetChanged();
        System.out.println("Hi site adapter list size is " + mSiteListAdapter.size());
        if (mSiteListAdapter.size() == 0) {
            mLayoutAddUpdateSite.setVisibility(View.GONE);
            mTxtNoRecord.setVisibility(View.VISIBLE);
            mRVSiteList.setVisibility(View.GONE);
        } else {
            mLayoutAddUpdateSite.setVisibility(View.GONE);
            mTxtNoRecord.setVisibility(View.GONE);
            mRVSiteList.setVisibility(View.VISIBLE);
        }
//        } else {
//    Set no record data
//            ArrayList<Site> vList = new ArrayList<Site>();
//            mSiteListAdapter = vList;

//            Collections.sort(mSiteListAdapter, new LetterAlphabeticalOrder());

//            siteListAdapter.notifyDataSetChanged();
//        }
        // $SearchListView.setAdapter(new DailySearchAdapter(DailySearch.this,
        // mFilteredListDailySearch));

    }


    private class getSiteList extends AsyncTask<String, String, String> {
        ProgressDialog progressDialog;

        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub

            String result = null;
            try {

                HttpConfig ht = new HttpConfig();
                result = ht.httpGet(Const.API_URL + "mobile/viewSite?userId=" + Constant.SELECTED_USER_ID);
            } catch (Exception e) {
                e.printStackTrace();
            }

            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            progressDialog.dismiss();

            if (result != null && result.length() > 0) {
                mSiteList.clear();
                mSiteListAdapter.clear();

                try {
                    JSONObject jObj = new JSONObject(result.trim());

                    if (jObj.has("orgIds")) {
                        JSONArray orgIdArray = jObj.getJSONArray("orgIds");
                        mOrgList = new ArrayList<>();
                        mOrgList.add("Select Organisation");
                        for (int i = 0; i < orgIdArray.length(); i++) {
                            mOrgList.add(orgIdArray.getString(i));
                        }
//set org id spinner
                        setOrgIdDropDownData(null);
                    }

                    if (jObj.has("siteParent")) {
                        JSONArray sitesArray = jObj.getJSONArray("siteParent");

                        for (int i = 0; i < sitesArray.length(); i++) {
                            JSONObject siteParentObj = sitesArray.getJSONObject(i);

                            if (siteParentObj.has("orgId")) {


                                if (siteParentObj.has("site")) {
                                    JSONArray siteDataArray = siteParentObj.getJSONArray("site");

                                    for (int j = 0; j < siteDataArray.length(); j++) {
                                        Site site = new Site();
                                        site.setOrgId(siteParentObj.getString("orgId"));
                                        JSONObject siteDetailObj = siteDataArray.getJSONObject(j);

                                        if (siteDetailObj.has("siteName")) {

//                                            System.out.println("Hi site name is " + siteDetailObj.getString("siteName"));

                                            site.setSiteName(siteDetailObj.getString("siteName"));
                                        }
                                        if (siteDetailObj.has("siteType")) {
                                            site.setSiteType(siteDetailObj.getString("siteType"));
                                        }
                                        if (siteDetailObj.has("latLng")) {
                                            site.setLatLng(siteDetailObj.getString("latLng"));
                                        }

                                        mSiteList.add(site);
                                        mSiteListAdapter.add(site);
                                    }

                                }

                            }

                        }

                    }

                    //set site list in adapter
//                    siteListAdapter = new SiteListAdapterNew(AddSitesActivity.this, mSiteList);
//                    mRVSiteList.setAdapter(siteListAdapter);
//                    siteListAdapter.updateList(mSiteList);
//                    mSiteListAdapter = mSiteList;

                    System.out.println("Hi site list adapter " + mSiteListAdapter.size() + " sitelist " + mSiteList.size());

                    Collections.sort(mSiteListAdapter, new LetterAlphabeticalOrder());

                    siteListAdapter.notifyDataSetChanged();

//                    if (mSiteListAdapter.size() == 0) {
//                        mLayoutAddUpdateSite.setVisibility(View.GONE);
//                        mTxtNoRecord.setVisibility(View.VISIBLE);
//                        mRVSiteList.setVisibility(View.GONE);
//                    } else {
////                        mLayoutAddUpdateSite.setVisibility(View.VISIBLE);
//                        mLayoutAddUpdateSite.setVisibility(View.GONE);
//                        mTxtNoRecord.setVisibility(View.GONE);
//                        mRVSiteList.setVisibility(View.VISIBLE);
//                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }

            } else {
                Toast.makeText(getApplicationContext(), ""+getResources().getString(R.string.error_fetching_sites), Toast.LENGTH_SHORT).show();
            }

        }

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            progressDialog = new ProgressDialog(AddSitesActivity.this,
                    AlertDialog.THEME_HOLO_LIGHT);
            progressDialog.setMessage(getResources().getString(R.string.progress_dialog));
            progressDialog.setProgressDrawable(new ColorDrawable(
                    Color.BLUE));
            progressDialog.setCancelable(true);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.show();
        }

    }


    private class addSite extends AsyncTask<String, String, String> {
        ProgressDialog progressDialog;

        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub

            String result = null;
            try {
                HttpConfig ht = new HttpConfig();
                result = ht.httpGet(params[0].replaceAll(" ", "%20"));
            } catch (Exception e) {
                e.printStackTrace();
//                System.out.println("Hi exception is ::::" + e.toString());
            }

            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            progressDialog.dismiss();

            if (result != null && result.length() > 0) {
                if (result.equalsIgnoreCase("Success")) {
                    String mUrl = Const.API_URL + "mobile/updateGeoFences?siteName=" + mEdtSiteName.getText().toString().trim() + "&fcode=" + group[1]
                            + "&orgId=" + mOrgType + "&siteType=" + mSiteType + "&userId=" + Constant.SELECTED_USER_ID;
                    new updateSite().execute(mUrl);

                } else {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.please_try_again), Toast.LENGTH_SHORT).show();
                }

            } else {
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.please_try_again), Toast.LENGTH_SHORT).show();
            }

        }

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            progressDialog = new ProgressDialog(AddSitesActivity.this,
                    AlertDialog.THEME_HOLO_LIGHT);
            progressDialog.setMessage(getResources().getString(R.string.progress_dialog));
            progressDialog.setProgressDrawable(new ColorDrawable(
                    Color.BLUE));
            progressDialog.setCancelable(true);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.show();
        }

    }


    private class updateSite extends AsyncTask<String, String, String> {
        ProgressDialog progressDialog;

        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub

            String result = null;
            try {
                HttpConfig ht = new HttpConfig();
                result = ht.httpGet(params[0].replaceAll(" ", "%20"));

//                result = ht.httpGet(Const.API_URL + "/mobile/getAcReport?vehicleId=" + Constant.SELECTED_VEHICLE_ID + "&fromTimeUtc=" + fromMilliSeconds + "&toTimeUtc=" + toMilliSeconds + "&userId=" + Constant.SELECTED_USER_ID);


//                result = ht.httpGet("http://gpsvts.net/mobile/getAcReport?userId=sainiji&vehicleId=UP16BT9281&fromTimeUtc=1511375400000&toTimeUtc=1511548200000");
            } catch (Exception e) {
                e.printStackTrace();
//                System.out.println("Hi exception is ::::" + e.toString());
            }

            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            progressDialog.dismiss();

            if (result != null && result.length() > 0) {
              //   if (result.equalsIgnoreCase("Sucess")) {
                    mBtnAdd.setVisibility(View.VISIBLE);
                    mBtnUpdate.setVisibility(View.GONE);
                    mEdtSiteName.setText("");
                    setOrgIdDropDownData(null);
                    setSiteTypeDropDownData(null);

                    if (isOsmEnabled) {
                        setupOsmMap();
                    } else {
                        if (map != null) {
                            polylinePoints = new ArrayList<>();
                            map.clear();
                        }
                    }
                    if (cd.isConnectingToInternet()) {
                        new getSiteList().execute();
                    } else {
                        Toast.makeText(getApplicationContext(),
                                getResources().getString(R.string.network_connection),
                                Toast.LENGTH_SHORT).show();
                    }

//                } else {
//                    Toast.makeText(getApplicationContext(), "Please try again", Toast.LENGTH_SHORT).show();
//                }

            } else {
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.please_try_again), Toast.LENGTH_SHORT).show();
            }

        }

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            progressDialog = new ProgressDialog(AddSitesActivity.this,
                    AlertDialog.THEME_HOLO_LIGHT);
            progressDialog.setMessage(""+getResources().getString(R.string.progress_dialog));
            progressDialog.setProgressDrawable(new ColorDrawable(
                    Color.BLUE));
            progressDialog.setCancelable(true);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.show();
        }

    }


    private class deleteSite extends AsyncTask<String, String, String> {
        ProgressDialog progressDialog;

        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub

            String result = null;
            try {
                HttpConfig ht = new HttpConfig();
                result = ht.httpGet(params[0].replaceAll(" ", "%20"));
            } catch (Exception e) {
                e.printStackTrace();
            }

            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            progressDialog.dismiss();

            if (result != null && result.length() > 0) {
                if (result.equalsIgnoreCase("Success")) {
                    mBtnAdd.setVisibility(View.VISIBLE);
                    mBtnUpdate.setVisibility(View.GONE);
                    mToolbar.setTitle(getResources().getString(R.string.add_site));

                    if (isOsmEnabled) {
                        setupOsmMap();
                    } else {
                        if (map != null) {
                            polylinePoints = new ArrayList<>();
                            map.clear();
                        }
                    }

                    if (cd.isConnectingToInternet()) {
                        new getSiteList().execute();
                    } else {
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.network_connection),
                                Toast.LENGTH_SHORT).show();
                    }

                } else {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.please_try_again), Toast.LENGTH_SHORT).show();
                }

            } else {
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.please_try_again), Toast.LENGTH_SHORT).show();
            }

        }

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            progressDialog = new ProgressDialog(AddSitesActivity.this,
                    AlertDialog.THEME_HOLO_LIGHT);
            progressDialog.setMessage(getResources().getString(R.string.progress_dialog));
            progressDialog.setProgressDrawable(new ColorDrawable(
                    Color.BLUE));
            progressDialog.setCancelable(true);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.show();
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(AddSitesActivity.this, VehicleListActivity.class));
        finish();
    }
}
