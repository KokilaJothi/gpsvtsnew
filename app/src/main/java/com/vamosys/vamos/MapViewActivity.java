package com.vamosys.vamos;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.vamosys.model.VehicleData;
import com.vamosys.model.VehicleLocationsEnv;
import com.vamosys.utils.ConnectionDetector;
import com.vamosys.utils.Constant;
import com.vamosys.utils.HttpConfig;
import com.vamosys.utils.MyCustomProgressDialog;
import com.vamosys.utils.TypefaceUtil;

import org.osmdroid.api.IMapController;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.Marker;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * Created by prabhakaran on 1/23/2016.
 */
public class MapViewActivity extends FragmentActivity implements GoogleMap.OnInfoWindowClickListener, View.OnClickListener, OnMapReadyCallback {

    //    Popup dialog initialize
    TextView id_vehicleid, id_view_address, id_view_speed, id_view_lastseentime, id_time_labelid, id_time, id_speed_limit_speed, id_ignition_speed;
    RelativeLayout id_time_label_layout;
    ImageView id_vehicle_info_icon;
    int width, height;
    ImageView $ImageBack, $ChangeView;
    //  TextView $TxtTitle;
    Spinner $SpinnerNavigation;

    //String[] mSpinnerVehicleId = {"Select"};

    private GoogleMap map;
    TextView id_all_vehicles_value, id_online_vehicles_value, id_offline_vehicles_value, mTxtHeader;

    String mSelectedUserID, mSelectedGroupId, selected_vehicleid;
    String mTotalVehicles, mOnlineVehicles, mOfflineVehicles;

    List<String> mSpinnerVehicleIdList = new ArrayList<String>();

    List<String> mSpinnerVehicleShortNameList = new ArrayList<String>();

//    List<Marker> mMarkerList = new ArrayList<Marker>();
    /**
     * POI declarations
     */
    double lat, lng;
    float vehicle_zoom_level = 7.5F;
    float group_zoom_level = 13.5F;
    private HashMap<Marker, VehicleData> mMarkersOsmHashMap;
    private HashMap<com.google.android.gms.maps.model.Marker, VehicleData> mMarkersGoogleHashMap;
    //    private JSONObject jSelectedVehicleObject;
    ConnectionDetector cd;
    Dialog marker_info_dialog;
    SharedPreferences sp;
    Const cons;
    ImageView mImgRefresh;

    ArrayList<String> child = new ArrayList<String>();
    ArrayList<String> vehicle = new ArrayList<String>();
    private ArrayList<Object> childelements = new ArrayList<Object>();

    int map_marker_vechile_position, position;

    VehicleData mVehicleObj = new VehicleData();
    String mSELECTED_MAP_TYPE = "Normal";
    ProgressDialog progressDialog;

    //OSM related
    MapView mapview;
    LinearLayout mGoogleMapLayout;
    //    MapView mMapViewNew;
    private IMapController mapController;
    boolean isOsmEnabled = true;

    private static final String TAG = MapViewActivity.class.getName();
    private static final int WRITE_EXTERNAL_STORAGE_REQUEST_CODE = 101;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        Context ctx = getApplicationContext();
        //important! set your user agent to prevent getting banned from the osm servers
//        Configuration.getInstance().load(ctx, PreferenceManager.getDefaultSharedPreferences(ctx));
        setContentView(R.layout.map_view_activity_layout);


//        System.out.println("Home activity created ::::::");
        cons = new Const();
        cd = new ConnectionDetector(getApplicationContext());
        sp = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

        if (sp.getString("ip_adds", "") != null) {
            if (sp.getString("ip_adds", "").trim().length() > 0) {
                Const.API_URL = sp.getString("ip_adds", "");
            }
        }

        if (sp.getString("enabled_map", "") != null) {
            if (sp.getString("enabled_map", "").trim().length() > 0) {

//                System.out.println("hi enabled map is " + sp.getString("enabled_map", ""));

                if (sp.getString("enabled_map", "").equalsIgnoreCase(getResources().getString(R.string.osm))) {
                    isOsmEnabled = true;
                } else {
                    isOsmEnabled = false;
                }
            }
        }
        init();
        screenArrange();


        $SpinnerNavigation
                .setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                    @Override
                    public void onItemSelected(AdapterView<?> parent,
                                               View view, int pos, long id) {
                        // TODO Auto-generated method stub
                        selected_vehicleid = mSpinnerVehicleIdList.get(pos);
                        Log.d("selected_vehicleid",""+selected_vehicleid);
                        Log.d("selected_vehicleid",""+mSpinnerVehicleIdList.size());

                        if (selected_vehicleid.equalsIgnoreCase("Select")) {
//
                            if (isOsmEnabled) {
                                if (mapview != null) {
                                    mapview.getOverlays().clear();
                                    mapview.invalidate();
                                    mapview.getOverlays().add(Constant.getTilesOverlay(getApplicationContext()));
                                }
                            } else {
                                if (map != null) {
                                    map.clear();
                                }
                            }

                        } else {
//                            System.out.println("The selected vehicle pos is:::" + pos);
                            try {
                                for (int i = 0; i < Constant.SELECTED_VEHICLE_LOCATION_LIST_OBJECT.size(); i++) {
                                    Log.d("mVehicleObj", "" + i + " " + Constant.SELECTED_VEHICLE_LOCATION_LIST_OBJECT.get(i).getVehicleId());
                                    if (Constant.SELECTED_VEHICLE_LOCATION_LIST_OBJECT.get(i).getVehicleId().equalsIgnoreCase(selected_vehicleid)) {
                                        mVehicleObj = Constant.SELECTED_VEHICLE_LOCATION_LIST_OBJECT.get(i);
                                        Log.d("mVehicleObj", "" + mVehicleObj);
                                        if (isOsmEnabled) {
                                            setupOsmMap();
                                        } else {
                                            // Toast.makeText(MapViewActivity.this, "success", Toast.LENGTH_SHORT).show();
                                            setupGoogleMap();
                                        }
                                    }
                                }
                            }catch (Exception e){
                                Log.d("mVehicleObj", "" + e.getMessage());
                            }
                        }

                    }
                    @Override
                    public void onNothingSelected(AdapterView<?> arg0) {
                        // TODO Auto-generated method stub
                    }

                });


    }

    public void init() {


        $ChangeView = (ImageView) findViewById(R.id.map_changeViewIcon);

        mTxtHeader = (TextView) findViewById(R.id.map_title);

        $ImageBack = (ImageView) findViewById(R.id.map_view_Back);

        // $TxtTitle = (TextView) findViewById(R.id.map_title);
        $SpinnerNavigation = (Spinner) findViewById(R.id.map_spinner);

        mImgRefresh = (ImageView) findViewById(R.id.map_view_refresh);

        $ImageBack.setOnClickListener(this);
        mImgRefresh.setOnClickListener(this);
        $ChangeView.setOnClickListener(this);
        mGoogleMapLayout = (LinearLayout) findViewById(R.id.google_map_layout);
        RelativeLayout rl = (RelativeLayout) findViewById(R.id.map_view_relativelayout);
        if (isOsmEnabled) {
            //open street map enabled by the user
            $ChangeView.setVisibility(View.GONE);
            mGoogleMapLayout.setVisibility(View.GONE);
            rl.setVisibility(View.VISIBLE);
//            System.out.println("Hi osm map setup");


            mapview = new MapView(this);
            mapview.setTilesScaledToDpi(true);
            rl.addView(mapview, new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.FILL_PARENT,
                    RelativeLayout.LayoutParams.FILL_PARENT));

            mapview.setBuiltInZoomControls(false);
            mapview.setMultiTouchControls(true);
            mapController = mapview.getController();
            mapController.setZoom(10);
            mapview.getOverlays().add(Constant.getTilesOverlay(getApplicationContext()));
            mMarkersOsmHashMap = new HashMap<Marker, VehicleData>();
            $ChangeView.setVisibility(View.GONE);
//            Toast.makeText(getApplicationContext(), "In case map is not loading.Please enable the storage the permission if disabled", Toast.LENGTH_SHORT).show();


            if (isStoragePermissionGranted()) {
                setDropDownData();
            }
        } else {
            $ChangeView.setVisibility(View.VISIBLE);
            mGoogleMapLayout.setVisibility(View.VISIBLE);
            rl.setVisibility(View.GONE);
//            System.out.println("Hi google map setup");
            mMarkersGoogleHashMap = new HashMap<com.google.android.gms.maps.model.Marker, VehicleData>();
            //google map enabled by the user
            FragmentManager myFragmentManager = getSupportFragmentManager();
            SupportMapFragment myMapFragment = (SupportMapFragment) myFragmentManager
                    .findFragmentById(R.id.mapView_layout_map);
            myMapFragment.getMapAsync(this);
//            $ChangeView.setVisibility(View.VISIBLE);
        }


    }


    public boolean isStoragePermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            if ((checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) && (checkSelfPermission(android.Manifest.permission.READ_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED)) {
//                Log.v(TAG, "Permission is granted");
                return true;
            } else {

//                Log.v(TAG, "Permission is revoked");
                ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE, android.Manifest.permission.READ_EXTERNAL_STORAGE}, WRITE_EXTERNAL_STORAGE_REQUEST_CODE);
                return false;
            }
        } else { //permission is automatically granted on sdk<23 upon installation

//            Log.v(TAG, "Permission is granted");
            return true;
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {


        switch (requestCode) {

            case WRITE_EXTERNAL_STORAGE_REQUEST_CODE:
//                if (permissions.length != 1 || grantResults.length != 1) {
//                    throw new RuntimeException("Error on requesting file write permission.");
//                }
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                    Toast.makeText(this, "File write permission granted",
//                            Toast.LENGTH_SHORT).show();
                   // Toast.makeText(MapViewActivity.this, "success1", Toast.LENGTH_SHORT).show();
                    setDropDownData();
//                    callCameraPermission();
                } else {
                    Toast.makeText(getApplicationContext(), "Please grand permission for storage to access Map", Toast.LENGTH_SHORT).show();

                    isStoragePermissionGranted();

                }
                // No need to start camera here; it is handled by onResume
                break;

        }


    }


    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub
        switch (v.getId()) {
            case R.id.map_view_Back:
                startActivity(new Intent(MapViewActivity.this, MapMenuActivity.class));
                finish();
                break;
            case R.id.map_changeViewIcon:
                opptionPopUp();
                break;
            case R.id.map_view_refresh:
                new PullVehicleInformation().execute();
                break;
        }
    }

    @Override
    public void onInfoWindowClick(com.google.android.gms.maps.model.Marker marker) {

    }

//    @Override
//    public void onInfoWindowClick(Marker marker) {
//
//    }
//
//    @Override
//    public void onMapReady(GoogleMap googleMap) {
//
//        map = googleMap;
//        map.setPadding(1, 1, 1, 150);
//        map.setOnInfoWindowClickListener(this);
//
//        setDropDownData();
//
//        map.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
//            @Override
//            public boolean onMarkerClick(Marker marker) {
//
//                if (mMarkersHashMap.containsKey(marker)) {
//                    if (cd.isConnectingToInternet()) {
//                        mVehicleObj = mMarkersHashMap.get(marker);
//                        showVehicleInfoDialog(mVehicleObj, false);
//                    } else {
//                        Toast internet_toast = Toast.makeText(getApplicationContext(), "Please Check your Internet Connection", Toast.LENGTH_LONG);
//                        internet_toast.show();
//                    }
//                    return true;
//                }
//
//                return true;

//            }
//        });
//    }

//    @Override
//    public void onMapReady(GoogleMap googleMap) {
//        sdfdfdsfdsf
//    }


    /**
     * AsyncTask Background API Calls
     */
    public class PullVehicleInformation extends AsyncTask<String, Integer, String> {
        int current_group_pos;
        String current_group_name;

        @Override
        public String doInBackground(String... urls) {


            String response_from_server = "";
            try {

                String url = Const.API_URL + "mobile/getVehicleLocations?userId=" + Constant.SELECTED_USER_ID + "&group=" + Constant.SELECTED_GROUP + "&macid=00000&appid=00000";
                HttpConfig ht = new HttpConfig();
                response_from_server = ht.httpGet(url);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return response_from_server;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = MyCustomProgressDialog.ctor(MapViewActivity.this);
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        protected void onProgressUpdate(Integer... values) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    publishProgress(0);
                }
            });
        }

        @Override
        public void onPostExecute(String result) {
            Cursor vehicle_info = null;
            try {
                Date d = new Date();
//                String mTimeData = d.getDate() + "-" + d.getMonth() + 1 + "-" + d.getYear()
//                        + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();

                List<VehicleLocationsEnv> mVehicleGroupList = new ArrayList<VehicleLocationsEnv>();
                if (result != null && result.length() > 0) {
                    Gson g = new Gson();
                    InputStream is = new ByteArrayInputStream(result.getBytes());
                    Reader reader = new InputStreamReader(is);
                    Type fooType = new TypeToken<List<VehicleLocationsEnv>>() {
                    }.getType();

                    mVehicleGroupList = g.fromJson(reader, fooType);

                    for (int i = 0; i < mVehicleGroupList.size(); i++) {


                        if (mVehicleGroupList.get(i).getGroup().equalsIgnoreCase(Constant.SELECTED_GROUP)) {


                            if (mVehicleGroupList.get(i).getVehicleLocations() != null) {
                                Constant.SELECTED_VEHICLE_LOCATION_LIST_OBJECT = new ArrayList<VehicleData>(
                                        Arrays.asList(mVehicleGroupList.get(i).getVehicleLocations()));


                            } else {

                            }


                        }


                    }

                }
                // if (progressDialog.isShowing()) {
                progressDialog.dismiss();
                //Toast.makeText(MapViewActivity.this, "success2", Toast.LENGTH_SHORT).show();
                setDropDownData();
                // }

                // stopping swipe refresh
//                swipeRefreshLayout.setRefreshing(false);

            } catch (Exception e) {
            } finally {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                if (vehicle_info != null) {
                    vehicle_info.close();
                }
            }
        }
    }


    public void setDropDownData() {
//        System.out.println("Hi setdropdown");
        //mGroupList = new ArrayList<String>();
        // List<String> list = new ArrayList<String>();
        // list.add("Select");

        for (int i = 0; i < Constant. SELECTED_VEHICLE_LOCATION_LIST_OBJECT.size(); i++) {
            if (Constant.SELECTED_VEHICLE_LOCATION_LIST_OBJECT.get(i).getVehicleId() != null) {
                mSpinnerVehicleIdList.add(Constant.SELECTED_VEHICLE_LOCATION_LIST_OBJECT.get(i).getVehicleId());
                mSpinnerVehicleShortNameList.add(Constant.SELECTED_VEHICLE_LOCATION_LIST_OBJECT.get(i).getShortName());
            }
        }

        if (mSpinnerVehicleIdList.size() > 0) {

        } else {
            mSpinnerVehicleIdList.add("Select");
        }

        if (mSpinnerVehicleShortNameList.size() > 0) {

        } else {
            mSpinnerVehicleShortNameList.add("Select");
        }

        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(
                MapViewActivity.this, R.layout.spinner_row,
                mSpinnerVehicleShortNameList) {

            public View getView(int position, View convertView, ViewGroup parent) {
                View v = super.getView(position, convertView, parent);
                // TextView v = (TextView) super.getView(position,
                // convertView,parent);
                // Constant.face(DailyCalls.this, (TextView) v);
                return v;
            }

            public View getDropDownView(int position, View convertView,
                                        ViewGroup parent) {
                View v = super.getDropDownView(position, convertView, parent);
                // ((TextView)
                // v).setBackgroundColor(Color.parseColor("#BBfef3da"));
                // TextView v = (TextView) super.getView(position,
                // convertView,parent);
                // Constant.face(DailyCalls.this, (TextView) v);
                return v;
            }
        };
        spinnerArrayAdapter
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        $SpinnerNavigation.setAdapter(spinnerArrayAdapter);
        ArrayAdapter myNewAdap = (ArrayAdapter) $SpinnerNavigation.getAdapter();
        int spinnerPosition = myNewAdap.getPosition(Constant.SELECTED_VEHICLE_SHORT_NAME);

//set the default according to value
        $SpinnerNavigation.setSelection(spinnerPosition);

    }


    /**
     * Map Related code
     */
    private boolean showVehicleInfoDialog(VehicleData mVehiObj, boolean isFromList) {
        if (isFromList) {
            marker_info_dialog = new Dialog(this, android.R.style.Theme_Light_NoTitleBar_Fullscreen);
            marker_info_dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        } else {
            marker_info_dialog = new Dialog(this);
        }
        marker_info_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        marker_info_dialog.setContentView(R.layout.custominfolayout);
        marker_info_dialog.setCancelable(false);
        ImageView id_back_icon = (ImageView) marker_info_dialog.findViewById(R.id.id_back_icon);
        id_vehicleid = (TextView) marker_info_dialog.findViewById(R.id.id_vehicleid);
        id_vehicle_info_icon = (ImageView) marker_info_dialog.findViewById(R.id.id_vehicle_info_icon);
        Button id_vehicle_track = (Button) marker_info_dialog.findViewById(R.id.id_vehicle_track);
        Button vehicle_history = (Button) marker_info_dialog.findViewById(R.id.id_vehicle_history_btn);
        Button id_close = (Button) marker_info_dialog.findViewById(R.id.id_close);
        id_view_address = (TextView) marker_info_dialog.findViewById(R.id.id_view_address);
        id_view_speed = (TextView) marker_info_dialog.findViewById(R.id.id_view_speed);
        id_view_lastseentime = (TextView) marker_info_dialog.findViewById(R.id.id_view_lastseentime);
        id_speed_limit_speed = (TextView) marker_info_dialog.findViewById(R.id.id_view_spped_lastactive);
        id_ignition_speed = (TextView) marker_info_dialog.findViewById(R.id.id_ignition_speed);
        TextView id_lastseentime_labelid = (TextView) marker_info_dialog.findViewById(R.id.id_lastseentime_labelid);
        TextView id_speed_labelid = (TextView) marker_info_dialog.findViewById(R.id.id_speed_labelid);
        TextView id_ignition_labelid = (TextView) marker_info_dialog.findViewById(R.id.id_ignition_labelid);
        TextView id_address_text_labelid = (TextView) marker_info_dialog.findViewById(R.id.id_address_text_labelid);
        TextView id_lastseentime_label_separator = (TextView) marker_info_dialog.findViewById(R.id.id_lastseentime_label_separator);
        TextView id_speed_labelid_separator = (TextView) marker_info_dialog.findViewById(R.id.id_speed_labelid_separator);
        id_time_label_layout = (RelativeLayout) marker_info_dialog.findViewById(R.id.id_time_label_layout);
        id_time_labelid = (TextView) marker_info_dialog.findViewById(R.id.id_time_labelid);
        id_time = (TextView) marker_info_dialog.findViewById(R.id.id_time);
        ImageView id_vehicle_next = (ImageView) marker_info_dialog.findViewById(R.id.id_vehicle_next);
        ImageView id_vehicle_previous = (ImageView) marker_info_dialog.findViewById(R.id.id_vehicle_previous);
        id_vehicle_next.setVisibility(View.GONE);
        id_vehicle_previous.setVisibility(View.GONE);
        final ImageView id_view_next = (ImageView) marker_info_dialog.findViewById(R.id.id_vehicle_next);
        final ImageView id_view_previous = (ImageView) marker_info_dialog.findViewById(R.id.id_vehicle_previous);

        TextView id_txtview_total_km = (TextView) marker_info_dialog.findViewById(R.id.id_txtview_total_km);
        TextView id_total_km_labelid = (TextView) marker_info_dialog.findViewById(R.id.id_total_km_labelid);


        id_view_next.setVisibility(View.GONE);
        id_view_previous.setVisibility(View.GONE);
        id_time_labelid.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        id_time.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        id_lastseentime_label_separator.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        id_speed_labelid_separator.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        id_lastseentime_labelid.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        id_speed_labelid.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        id_ignition_labelid.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        id_address_text_labelid.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        id_vehicleid.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        id_vehicle_track.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        vehicle_history.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        id_view_address.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        id_view_speed.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        id_view_lastseentime.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        id_speed_limit_speed.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        id_ignition_speed.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        id_total_km_labelid.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        id_txtview_total_km.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        try {
            id_view_address.setText(mVehiObj.getAddress());

            selected_vehicleid = mVehiObj.getVehicleId();
//            if (mVehiObj.getVehicleType().equalsIgnoreCase("Bus")) {
//                id_vehicle_info_icon.setImageResource(R.drawable.bus_east);
//            } else if (mVehiObj.getVehicleType().equalsIgnoreCase("Car")) {
//                id_vehicle_info_icon.setImageResource(R.drawable.car_east);
//            } else if (mVehiObj.getVehicleType().equalsIgnoreCase("Truck")) {
//                id_vehicle_info_icon.setImageResource(R.drawable.truck_east);
//            } else if (mVehiObj.getVehicleType().equalsIgnoreCase("Cycle")) {
//                id_vehicle_info_icon.setImageResource(R.drawable.cycle_east);
//            } else if (mVehiObj.getVehicleType().equalsIgnoreCase("Jcb")) {
//                id_vehicle_info_icon.setImageResource(R.drawable.jcb_icon);
//            } else if (mVehiObj.getVehicleType().equalsIgnoreCase("Bike")) {
//                id_vehicle_info_icon.setImageResource(R.drawable.vehicle_bike);
//            }

            if (mVehiObj.getPosition().equalsIgnoreCase("M")) {
                id_vehicle_info_icon.setImageDrawable(Constant.setVehicleImage("M", getApplicationContext(), mVehiObj.getVehicleType()));

            } else if (mVehiObj.getPosition().equalsIgnoreCase("P")) {
                id_vehicle_info_icon.setImageDrawable(Constant.setVehicleImage("P", getApplicationContext(), mVehiObj.getVehicleType()));

            } else if (mVehiObj.getPosition().equalsIgnoreCase("S")) {
                id_vehicle_info_icon.setImageDrawable(Constant.setVehicleImage("S", getApplicationContext(), mVehiObj.getVehicleType()));

            } else if (mVehiObj.getPosition().equalsIgnoreCase("N")) {
                id_vehicle_info_icon.setImageDrawable(Constant.setVehicleImage("N", getApplicationContext(), mVehiObj.getVehicleType()));

            } else if (mVehiObj.getPosition().equalsIgnoreCase("U")) {
                id_vehicle_info_icon.setImageDrawable(Constant.setVehicleImage("U", getApplicationContext(), mVehiObj.getVehicleType()));
            }


            id_vehicleid.setText(mVehiObj.getShortName());
            id_view_speed.setText(Integer.toString(mVehiObj.getSpeed()) + " KMS / HR");
            id_view_lastseentime.setText(cons.getTripTimefromserver(String.valueOf(mVehiObj.getLastComunicationTime())));

            id_speed_limit_speed.setText("Speed limit is " + mVehiObj.getOverSpeedLimit() + " KMS / HR");
            id_txtview_total_km.setText(Double.toString(mVehiObj.getDistanceCovered()) + " KMS");
            id_ignition_speed.setText(mVehiObj.getIgnitionStatus());
            if (mVehiObj.getPosition().equalsIgnoreCase("P")) {
                id_time_labelid.setText("Parked Time");
                id_time.setText(cons.getVehicleTime(String.valueOf(mVehiObj.getParkedTime())));
            } else if (mVehiObj.getPosition().equalsIgnoreCase("M")) {
                id_time_labelid.setText("Moving Time");
                id_time.setText(cons.getVehicleTime(String.valueOf(mVehiObj.getMovingTime())));
            } else if (mVehiObj.getPosition().equalsIgnoreCase("U")) {
                id_time_labelid.setText("No Data Time");
                id_time.setText(cons.getVehicleTime(String.valueOf(mVehiObj.getNoDataTime())));
            } else if (mVehiObj.getPosition().equalsIgnoreCase("S")) {
                id_time_labelid.setText("Idle Time");
                id_time.setText(cons.getVehicleTime(String.valueOf(mVehiObj.getIdleTime())));
            } else {
                id_time_label_layout.setVisibility(View.GONE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            if (child.size() == 1) {
            } else {
                if (map_marker_vechile_position == child.size() - 1) {
                } else if (map_marker_vechile_position == 0) {
                } else {
                }
            }
        } catch (Exception e) {
            if (vehicle.size() == 1) {
            } else {
                if (map_marker_vechile_position == vehicle.size() - 1) {
                } else if (map_marker_vechile_position == 0) {
                } else {
                }
            }
        }
        id_vehicle_track.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //  track = true;
                //  vehicletracking(selected_vehicleid);
                marker_info_dialog.hide();
                Constant.SELECTED_VEHICLE_ID = selected_vehicleid;
                //  Constant.SELECTED_USER_ID = mSelectedUserID;
//                startActivity(new Intent(MapViewActivity.this, VehicleTrackingActivity.class));
//                finish();

                if (isOsmEnabled) {
                    startActivity(new Intent(MapViewActivity.this, VehicleTrackingActivity.class));
                    finish();
                } else {
                    startActivity(new Intent(MapViewActivity.this, VehicleTrackingGoogleMapActivity.class));
                    finish();
                }

            }
        });
        vehicle_history.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                marker_info_dialog.hide();

                Constant.SELECTED_VEHICLE_ID = selected_vehicleid;
                // Constant.SELECTED_USER_ID = mSelectedUserID;
//                startActivity(new Intent(MapViewActivity.this, VehicleHistoryActivity.class));

                if (isOsmEnabled) {
                    startActivity(new Intent(MapViewActivity.this, HistoryNewActivity.class));
                    finish();
                } else {
                    startActivity(new Intent(MapViewActivity.this, HistoryNewGoogleMapActivity.class));
                    finish();
                }


                // radioGroup.setVisi
            }
        });
        id_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                marker_info_dialog.hide();
            }
        });
        id_back_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                marker_info_dialog.hide();
            }
        });
        if (isFromList) {
            vehicle_history.setVisibility(View.INVISIBLE);
            id_vehicle_track.setVisibility(View.INVISIBLE);
            id_back_icon.setVisibility(View.VISIBLE);
            id_close.setVisibility(View.GONE);
        } else {
            id_back_icon.setVisibility(View.GONE);
            id_close.setVisibility(View.VISIBLE);
        }
        marker_info_dialog.show();
        return true;
    }


    /**
     * Code to make the Marker icon adjustable for multiple screen
     */
    public static Bitmap createDrawableFromView(Context context, View view) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        view.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        view.measure(displayMetrics.widthPixels, displayMetrics.heightPixels);
        view.layout(0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels);
        view.buildDrawingCache();
        Bitmap bitmap = Bitmap.createBitmap(view.getMeasuredWidth(), view.getMeasuredHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        view.draw(canvas);
        return bitmap;

    }

    public static Drawable createDrawableFromViewNew(Context context, View view) {


        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        view.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));


        view.measure(displayMetrics.widthPixels, displayMetrics.heightPixels);
        view.layout(0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels);
        view.buildDrawingCache();
//        System.out.println("Hi view width " + displayMetrics.widthPixels + " " + view.getMeasuredWidth() + " height " + displayMetrics.heightPixels + " " + view.getMeasuredHeight());
        Bitmap bitmap = Bitmap.createBitmap(view.getMeasuredWidth(), view.getMeasuredHeight(), Bitmap.Config.ARGB_8888);
//        Bitmap bitmap = Bitmap.createBitmap(250, 250, Bitmap.Config.ARGB_8888);
//        Bitmap bitmap = Bitmap.crea(96, 96, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        view.draw(canvas);
//        return bitmap;


        return new BitmapDrawable(bitmap);
    }

    public void setupGoogleMap() {
        //  JSONObject vehiclejson = Constant.SELECTED_VEHICLE_JSON_OBJECT;


        // System.out.println("The selected vehicle object is :::::" + Constant.SELECTED_VEHICLE_JSON_OBJECT);

        if (mMarkersGoogleHashMap != null) {
            mMarkersGoogleHashMap.clear();
        }
        if (map != null) {
            map.clear();
        }
        double latitude = 0, longitude = 0;
        String mVehicleId = null;
//        try {
//
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }


        // Toast.makeText(getApplicationContext(),"Map setup called :::::",Toast.LENGTH_SHORT).show();

        View marker = null;
        try {

            latitude = Double.parseDouble(mVehicleObj.getLatitude());

            // System.out.println("The latitude is ::::" + vehiclejson.getString("latitude"));

            longitude = Double.parseDouble(mVehicleObj.getLongitude());
            mVehicleId = mVehicleObj.getShortName();
            mTxtHeader.setText(mVehicleId);

            // System.out.println("The lati ::::"+latitude+" longi :::"+longitude);

            CameraPosition INIT = new CameraPosition.Builder().target(new LatLng(latitude, longitude)).zoom(vehicle_zoom_level).build();
            map.animateCamera(CameraUpdateFactory.newCameraPosition(INIT));
            marker = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.custommarker, null);
            ImageView id_custom_marker_icon = (ImageView) marker.findViewById(R.id.id_custom_marker_icon);
            ImageView id_vehicle_in_marker = (ImageView) marker.findViewById(R.id.id_vehicle_in_marker);

            if (mVehicleObj.getDirection() != null) {
                Constant.vehicle_direction_in_map(mVehicleObj.getVehicleType(), mVehicleObj.getDirection(), id_vehicle_in_marker);
                id_vehicle_in_marker.setColorFilter(Color.WHITE);
            }
//
//            if (mVehicleObj.getIsOverSpeed().equalsIgnoreCase("Y")) {
//                id_custom_marker_icon.setImageResource(R.drawable.red_custom_marker_icon);
//            } else if (mVehicleObj.getInsideGeoFence().equalsIgnoreCase("Y")) {
//                id_custom_marker_icon.setImageResource(R.drawable.blue_custom_marker_icon);
//            } else if (mVehicleObj.getPosition().equalsIgnoreCase("M")) {
//                id_custom_marker_icon.setImageResource(R.drawable.green_custom_marker_icon);
//            } else if (mVehicleObj.getPosition().equalsIgnoreCase("P")) {
//                id_custom_marker_icon.setImageResource(R.drawable.grey_custom_marker_icon);
//            } else if (mVehicleObj.getPosition().equalsIgnoreCase("S")) {
//                id_custom_marker_icon.setImageResource(R.drawable.orange_custom_marker_icon);
//            } else if (mVehicleObj.getPosition().equalsIgnoreCase("N")) {
//                id_custom_marker_icon.setImageResource(R.drawable.orange_custom_marker_icon);
//            } else if (mVehicleObj.getPosition().equalsIgnoreCase("U")) {
//                id_custom_marker_icon.setImageResource(R.drawable.grey_custom_marker_icon);
//            }
            if (mVehicleObj.getIsOverSpeed().equalsIgnoreCase("Y")) {
                id_custom_marker_icon.setImageResource(R.drawable.red_custom_marker_icon);
            } else if (mVehicleObj.getInsideGeoFence().equalsIgnoreCase("Y")) {
                id_custom_marker_icon.setImageResource(R.drawable.red_custom_marker_icon);
            } else if (mVehicleObj.getPosition().equalsIgnoreCase("M")) {
                id_custom_marker_icon.setImageResource(R.drawable.green_custom_marker_icon);
            } else if (mVehicleObj.getPosition().equalsIgnoreCase("P")) {
                id_custom_marker_icon.setImageResource(R.drawable.black_custom_marker_icon);
            } else if (mVehicleObj.getPosition().equalsIgnoreCase("S")) {
                id_custom_marker_icon.setImageResource(R.drawable.yellow_custom_marker_icon);
            } else if (mVehicleObj.getPosition().equalsIgnoreCase("N")) {
                id_custom_marker_icon.setImageResource(R.drawable.grey_custom_marker_icon);
            } else if (mVehicleObj.getPosition().equalsIgnoreCase("U")) {
                id_custom_marker_icon.setImageResource(R.drawable.red_custom_marker_icon);
            }
        } catch (Exception jo) {
            jo.printStackTrace();
        }
        MarkerOptions markerOption = new MarkerOptions().position(new LatLng(latitude, longitude));
        markerOption.title(mVehicleId);
        markerOption.icon(BitmapDescriptorFactory.fromBitmap(createDrawableFromView(MapViewActivity.this, marker)));
        com.google.android.gms.maps.model.Marker currentMarker = map.addMarker(markerOption);
        mMarkersGoogleHashMap.put(currentMarker, mVehicleObj);
    }

    public void setupOsmMap() {
        //  JSONObject vehiclejson = Constant.SELECTED_VEHICLE_JSON_OBJECT;


        // System.out.println("The selected vehicle object is :::::" + Constant.SELECTED_VEHICLE_JSON_OBJECT);

        if (mMarkersOsmHashMap != null) {
            mMarkersOsmHashMap.clear();
        }
//        if (map != null) {
//            map.clear();
//        }

        if (mapview != null) {
            mapview.getOverlays().clear();
            mapview.invalidate();
            mapview.getOverlays().add(Constant.getTilesOverlay(getApplicationContext()));
        }
        double latitude = 0, longitude = 0;
        String mVehicleId = null;
        View marker = null;
        try {

            latitude = Double.parseDouble(mVehicleObj.getLatitude());

            // System.out.println("The latitude is ::::" + vehiclejson.getString("latitude"));

            longitude = Double.parseDouble(mVehicleObj.getLongitude());
            mVehicleId = mVehicleObj.getShortName();
            mTxtHeader.setText(mVehicleId);

            // System.out.println("The lati ::::"+latitude+" longi :::"+longitude);

//            CameraPosition INIT = new CameraPosition.Builder().target(new LatLng(latitude, longitude)).zoom(vehicle_zoom_level).build();
//            map.animateCamera(CameraUpdateFactory.newCameraPosition(INIT));

            GeoPoint newPos = new GeoPoint(latitude, longitude);
            mapController.setCenter(newPos);

            marker = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.custommarker_osm, null);
            ImageView id_custom_marker_icon = (ImageView) marker.findViewById(R.id.id_custom_marker_icon);
            ImageView id_vehicle_in_marker = (ImageView) marker.findViewById(R.id.id_vehicle_in_marker);

            if (mVehicleObj.getDirection() != null) {
                Constant.vehicle_direction_in_map(mVehicleObj.getVehicleType(), mVehicleObj.getDirection(), id_vehicle_in_marker);
                id_vehicle_in_marker.setColorFilter(Color.WHITE);
            }


//            System.out.println("Hi is over speed " + mVehicleObj.getIsOverSpeed() + " " + mVehicleObj.getInsideGeoFence() + " pos " + mVehicleObj.getPosition());

            if (mVehicleObj.getIsOverSpeed().equalsIgnoreCase("Y")) {
                id_custom_marker_icon.setImageResource(R.drawable.red_custom_marker_icon);
            } else if (mVehicleObj.getInsideGeoFence().equalsIgnoreCase("Y")) {
                id_custom_marker_icon.setImageResource(R.drawable.red_custom_marker_icon);
            } else if (mVehicleObj.getPosition().equalsIgnoreCase("M")) {
                id_custom_marker_icon.setImageResource(R.drawable.green_custom_marker_icon);
            } else if (mVehicleObj.getPosition().equalsIgnoreCase("P")) {
                id_custom_marker_icon.setImageResource(R.drawable.black_custom_marker_icon);
            } else if (mVehicleObj.getPosition().equalsIgnoreCase("S")) {
                id_custom_marker_icon.setImageResource(R.drawable.yellow_custom_marker_icon);
            } else if (mVehicleObj.getPosition().equalsIgnoreCase("N")) {
                id_custom_marker_icon.setImageResource(R.drawable.yellow_custom_marker_icon);
            } else if (mVehicleObj.getPosition().equalsIgnoreCase("U")) {
                id_custom_marker_icon.setImageResource(R.drawable.red_custom_marker_icon);
            }
        } catch (Exception jo) {
            jo.printStackTrace();
        }
//        MarkerOptions markerOption = new MarkerOptions().position(new LatLng(latitude, longitude));
//        markerOption.title(mVehicleId);
//        markerOption.icon(BitmapDescriptorFactory.fromBitmap(createDrawableFromView(MapViewActivity.this, marker)));
//        Marker currentMarker = map.addMarker(markerOption);
//        mMarkersHashMap.put(currentMarker, mVehicleObj);

        Marker osmMarker = new Marker(mapview);
        GeoPoint loc = new GeoPoint(latitude, longitude);
        osmMarker.setPosition(loc);
        osmMarker.setAnchor(Marker.ANCHOR_CENTER, Marker.ANCHOR_BOTTOM);
        osmMarker.setTitle(mVehicleId);
        mMarkersOsmHashMap.put(osmMarker, mVehicleObj);
        osmMarker.setIcon(createDrawableFromViewNew(MapViewActivity.this, marker));
//        mMarkerList.add(osmMarker);

        mapview.getOverlays().add(osmMarker);
        mapview.invalidate();

        osmMarker.setOnMarkerClickListener(new Marker.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker, MapView mapView) {

//                Toast.makeText(getApplicationContext(), "Marker clicked ", Toast.LENGTH_SHORT).show();


                if (mMarkersOsmHashMap.containsKey(marker)) {
//                    if (cd.isConnectingToInternet()) {
                    mVehicleObj = mMarkersOsmHashMap.get(marker);
                    showVehicleInfoDialog(mVehicleObj, false);
//                    } else {
//                        Toast internet_toast = Toast.makeText(getApplicationContext(), "Please Check your Internet Connection", Toast.LENGTH_LONG);
//                        internet_toast.show();
//                    }
                    return true;
                }
//


                return false;
            }
        });


    }

    private void opptionPopUp() {
        // TODO Auto-generated method stub
        final Dialog dialog = new Dialog(MapViewActivity.this);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.radio_popup);
        dialog.show();

        RadioGroup rg_home = (RadioGroup) dialog.findViewById(R.id.rg_home_views);
        RadioGroup rg_history = (RadioGroup) dialog.findViewById(R.id.rg_history_views);
        rg_history.clearCheck();
        rg_history.setVisibility(View.GONE);
        rg_home.setVisibility(View.VISIBLE);

        RadioButton $Normal = (RadioButton) dialog
                .findViewById(R.id.rb_home_normal);
        RadioButton $Satelite = (RadioButton) dialog
                .findViewById(R.id.rb_home_satellite);
        RadioButton $Terrain = (RadioButton) dialog
                .findViewById(R.id.rb_home_terrain);
        RadioButton $Hybrid = (RadioButton) dialog
                .findViewById(R.id.rb_home_hybrid);


        if (mSELECTED_MAP_TYPE.equalsIgnoreCase("Normal")) {
            $Normal.setChecked(true);
            $Satelite.setChecked(false);
            $Terrain.setChecked(false);
            $Hybrid.setChecked(false);
        } else if (mSELECTED_MAP_TYPE.equalsIgnoreCase("Satelite")) {
            $Normal.setChecked(false);
            $Satelite.setChecked(true);
            $Terrain.setChecked(false);
            $Hybrid.setChecked(false);
        } else if (mSELECTED_MAP_TYPE.equalsIgnoreCase("Terrain")) {
            $Normal.setChecked(false);
            $Satelite.setChecked(false);
            $Terrain.setChecked(true);
            $Hybrid.setChecked(false);
        } else if (mSELECTED_MAP_TYPE.equalsIgnoreCase("Hybrid")) {
            $Normal.setChecked(false);
            $Satelite.setChecked(false);
            $Terrain.setChecked(false);
            $Hybrid.setChecked(true);
        }
        $Normal.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

//                mapview.setTileSource(TileSourceFactory.MAPNIK);

                map.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                mSELECTED_MAP_TYPE = "Normal";
                dialog.dismiss();
            }
        });
        $Satelite.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                mSELECTED_MAP_TYPE = "Satelite";
                map.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
//                mapview.setTileSource(TileSourceFactory.USGS_SAT);
                dialog.dismiss();
            }
        });
        $Terrain.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                mSELECTED_MAP_TYPE = "Terrain";
                map.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
//                mapview.setTileSource(TileSourceFactory.HIKEBIKEMAP);
                dialog.dismiss();
            }
        });

        $Hybrid.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                mSELECTED_MAP_TYPE = "Hybrid";
                map.setMapType(GoogleMap.MAP_TYPE_HYBRID);
//                mapview.setTileSource(TileSourceFactory.USGS_TOPO);
                dialog.dismiss();
            }
        });

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        height = displayMetrics.heightPixels;
        width = displayMetrics.widthPixels;

        LinearLayout.LayoutParams radioParama = new LinearLayout.LayoutParams(
                FrameLayout.LayoutParams.WRAP_CONTENT,
                FrameLayout.LayoutParams.WRAP_CONTENT);
        radioParama.width = width * 50 / 100;
        radioParama.height = width * 10 / 100;
        radioParama.topMargin = height * 4 / 100;
        radioParama.gravity = Gravity.CENTER;
        radioParama.leftMargin = height * 4 / 100;
        $Normal.setLayoutParams(radioParama);
        $Satelite.setLayoutParams(radioParama);
        $Terrain.setLayoutParams(radioParama);

        LinearLayout.LayoutParams radioterrainParama = new LinearLayout.LayoutParams(
                FrameLayout.LayoutParams.WRAP_CONTENT,
                FrameLayout.LayoutParams.WRAP_CONTENT);
        radioterrainParama.width = width * 50 / 100;
        radioterrainParama.height = width * 10 / 100;
        radioterrainParama.topMargin = height * 4 / 100;
        radioterrainParama.gravity = Gravity.CENTER;
        radioterrainParama.leftMargin = height * 4 / 100;
        radioterrainParama.bottomMargin = height * 4 / 100;
        $Hybrid.setLayoutParams(radioterrainParama);

        if (width >= 600) {
            $Normal.setTextSize(16);
            $Satelite.setTextSize(16);
            $Terrain.setTextSize(16);
            $Hybrid.setTextSize(16);
        } else if (width > 501 && width < 600) {
            $Normal.setTextSize(15);
            $Satelite.setTextSize(15);
            $Terrain.setTextSize(15);
            $Hybrid.setTextSize(15);
        } else if (width > 260 && width < 500) {
            $Normal.setTextSize(14);
            $Satelite.setTextSize(14);
            $Terrain.setTextSize(14);
            $Hybrid.setTextSize(14);
        } else if (width <= 260) {
            $Normal.setTextSize(13);
            $Satelite.setTextSize(13);
            $Terrain.setTextSize(13);
            $Hybrid.setTextSize(13);
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        map = googleMap;
        map.setPadding(1, 1, 1, 150);
        map.setOnInfoWindowClickListener(this);
        System.out.println("Hi google map setup on ready");
     //   Toast.makeText(MapViewActivity.this, "succes3", Toast.LENGTH_SHORT).show();
        setDropDownData();

        map.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(com.google.android.gms.maps.model.Marker marker) {

                if (mMarkersGoogleHashMap.containsKey(marker)) {
                    if (cd.isConnectingToInternet()) {
                        mVehicleObj = mMarkersGoogleHashMap.get(marker);
                        showVehicleInfoDialog(mVehicleObj, false);
                    } else {
                        Toast internet_toast = Toast.makeText(getApplicationContext(), "Please Check your Internet Connection", Toast.LENGTH_LONG);
                        internet_toast.show();
                    }
                    return true;
                }

                return true;

            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        startActivity(new Intent(MapViewActivity.this, VehicleListActivity.class));
        finish();

    }

    private void screenArrange() {
        // TODO Auto-generated method stub
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        height = displayMetrics.heightPixels;
        width = displayMetrics.widthPixels;

        LinearLayout.LayoutParams backImageParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        backImageParams.width = width * 15 / 100;
        backImageParams.height = height * 10 / 100;
        backImageParams.gravity = Gravity.CENTER;
        $ImageBack.setLayoutParams(backImageParams);
        $ImageBack.setPadding(width * 1 / 100, height * 1 / 100,
                width * 1 / 100, height * 1 / 100);

        LinearLayout.LayoutParams headTxtParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        headTxtParams.width = width * 70 / 100;
        headTxtParams.height = height * 10 / 100;
        mTxtHeader.setLayoutParams(headTxtParams);
        mTxtHeader.setPadding(width * 2 / 100, 0, 0, 0);
        mTxtHeader.setGravity(Gravity.CENTER);

        LinearLayout.LayoutParams imgMenu = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        imgMenu.height = height * 5 / 100;
        imgMenu.width = height * 5 / 100;
        imgMenu.gravity = (Gravity.CENTER);
        // imgMenu.rightMargin = height * 2 / 100;
        imgMenu.leftMargin = width * 1 / 100;
        imgMenu.rightMargin = width * 1 / 100;
        mImgRefresh.setLayoutParams(imgMenu);

        LinearLayout.LayoutParams spinnerParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        spinnerParams.width = width;
        spinnerParams.height = height * 7 / 100;
        spinnerParams.gravity = Gravity.CENTER | Gravity.TOP;
        spinnerParams.topMargin = (int) (height * 0.6 / 100);
        $SpinnerNavigation.setLayoutParams(spinnerParams);

        FrameLayout.LayoutParams imgeChangeView = new FrameLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        imgeChangeView.width = width * 10 / 100;
        imgeChangeView.height = height * 15 / 100;
        imgeChangeView.gravity = Gravity.TOP | Gravity.RIGHT;
        imgeChangeView.rightMargin = (int) (width * 0.5 / 100);
        $ChangeView.setLayoutParams(imgeChangeView);

        if (width >= 600) {
            mTxtHeader.setTextSize(18);
        } else if (width > 501 && width < 600) {
            mTxtHeader.setTextSize(17);
        } else if (width > 260 && width < 500) {
            mTxtHeader.setTextSize(16);
        } else if (width <= 260) {
            mTxtHeader.setTextSize(15);
        }
    }
}
