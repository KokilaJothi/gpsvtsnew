package com.vamosys.vamos;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.vamosys.adapter.NotificationFilterAdapter;
import com.vamosys.model.NotificationFilterDto;
import com.vamosys.utils.ConnectionDetector;
import com.vamosys.utils.Constant;
import com.vamosys.utils.HttpConfig;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

public class NotificationFilterActivity extends Activity {

    TextView mTxtHeader, mTxtListTitle;
    Button mBtnUpdate;
    ImageView mImgBack;
    ListView mNotiFilterLstView;
    public static int width, height;
    ConnectionDetector cd;
    List<String> mNotiList = new ArrayList<String>();
    List<String> mSelectedNotiList = new ArrayList<String>();

    String mSelectedFilterValueStr = null;
    SharedPreferences sp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification_filter);
        init();
        screenArrange();
        cd = new ConnectionDetector(getApplicationContext());

        sp = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        if (sp.getString("ip_adds", "") != null) {
            if (sp.getString("ip_adds", "").trim().length() > 0) {
                Const.API_URL = sp.getString("ip_adds", "");
            }
        }


        if (cd.isConnectingToInternet()) {
//                                mStrFromDate = fromdatevalue.getText().toString().trim();
//                                mStrToDate = todatevalue.getText().toString().trim();

            new getNotiFilterData().execute();
        } else {
            Toast.makeText(getApplicationContext(),
                    "Please check your network connection",
                    Toast.LENGTH_SHORT).show();
        }


        mBtnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSelectedFilterValueStr = collectSelectedFilterValue();
                if (mSelectedFilterValueStr != null) {

                    if (cd.isConnectingToInternet()) {
//                                mStrFromDate = fromdatevalue.getText().toString().trim();
//                                mStrToDate = todatevalue.getText().toString().trim();

                        new updateNotiFilterData().execute();
                    } else {
                        Toast.makeText(getApplicationContext(),
                                ""+getResources().getString(R.string.network_connection),
                                Toast.LENGTH_SHORT).show();
                    }

                } else {
                    Toast.makeText(getApplicationContext(),
                            ""+getResources().getString(R.string.select_any_value),
                            Toast.LENGTH_SHORT).show();
                }

            }
        });


        mImgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(NotificationFilterActivity.this, Settings.class));
                finish();


            }
        });


    }

    public void init() {
        mNotiFilterLstView = (ListView) findViewById(R.id.noti_filter_listView1);
        mTxtHeader = (TextView) findViewById(R.id.noti_filter_tvTitle);
        mTxtListTitle = (TextView) findViewById(R.id.noti_filter_select_txt_view);
        mImgBack = (ImageView) findViewById(R.id.noti_filter_view_Back);
        mBtnUpdate = (Button) findViewById(R.id.btn_noti_filter_update);

    }


    private class getNotiFilterData extends AsyncTask<String, String, String> {
        ProgressDialog progressDialog;

        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub

            String result = null;
            try {

                HttpConfig ht = new HttpConfig();

                result = ht.httpGet(Const.API_URL + "/mobile/getUserNotification?userId=" + Constant.SELECTED_USER_ID);

            } catch (Exception e) {

            }

            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            progressDialog.dismiss();
            if (result != null && result.length() > 0) {
//                System.out.println("The Fuel fill result is ::::" + result);

                JSONArray jArraynotification = null, jArrayselectedNotification = null;
                try {
//                    jsonArray = new JSONArray(result);

                    // for (int i = 0; i < jsonArray.length(); i++) {
//                    JSONObject jsonObject = jsonArray.getJSONObject(1);

                    JSONObject jsonObject = new JSONObject(result.trim());

                    if (jsonObject.has("notification")) {
                        jArraynotification = jsonObject.getJSONArray("notification");
                        for (int i = 0; i < jArraynotification.length(); i++) {
                            mNotiList.add(jArraynotification.getString(i).trim());


                        }
                    }

                    if (jsonObject.has("selectedNotification")) {
                        jArrayselectedNotification = jsonObject.getJSONArray("selectedNotification");
                        for (int i = 0; i < jArrayselectedNotification.length(); i++) {
                            mSelectedNotiList.add(jArrayselectedNotification.getString(i).trim());


                        }
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }


                setListData();
            }

        }

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            progressDialog = new ProgressDialog(NotificationFilterActivity.this,
                    AlertDialog.THEME_HOLO_LIGHT);
            progressDialog.setMessage("Please Wait...");
            progressDialog.setProgressDrawable(new ColorDrawable(
                    android.graphics.Color.BLUE));
            progressDialog.setCancelable(true);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.show();
        }

    }

    private class updateNotiFilterData extends AsyncTask<String, String, String> {
        ProgressDialog progressDialog;

        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub

            String result = null;
            try {

                HttpConfig ht = new HttpConfig();

                String query = URLEncoder.encode(mSelectedFilterValueStr, "utf-8");
                String url = Const.API_URL + "/mobile/updateNotification?userId=" + Constant.SELECTED_USER_ID + "&defaultValue=" + query;

//                result = ht.httpGet(Const.API_URL + "/mobile/updateNotification?userId=" + Constant.SELECTED_USER_ID + "&defaultValue=" + mSelectedFilterValueStr);
                result = ht.httpGet(url);

            } catch (Exception e) {

            }

            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);

           // System.out.println("The notification response is :::" + result);

            progressDialog.dismiss();
            if (result != null && result.length() > 0) {
                if (result.equalsIgnoreCase("success")) {
                    Toast.makeText(NotificationFilterActivity.this, ""+getResources().getString(R.string.notification_filter_updated), Toast.LENGTH_SHORT).show();

                    startActivity(new Intent(NotificationFilterActivity.this, Settings.class));
                    finish();

                } else {
                    Toast.makeText(NotificationFilterActivity.this, ""+getResources().getString(R.string.failure_try_again), Toast.LENGTH_SHORT).show();
                }


            }

        }

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            progressDialog = new ProgressDialog(NotificationFilterActivity.this,
                    AlertDialog.THEME_HOLO_LIGHT);
            progressDialog.setMessage(""+getResources().getString(R.string.progress_dialog));
            progressDialog.setProgressDrawable(new ColorDrawable(
                    android.graphics.Color.BLUE));
            progressDialog.setCancelable(true);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.show();
        }

    }


    public void setListData() {
        Constant.mNotificationFilterList.clear();

        for (int i = 0; i < mNotiList.size(); i++) {

            NotificationFilterDto n = new NotificationFilterDto();

            n.setFilterName(mNotiList.get(i).trim());

            for (int j = 0; j < mSelectedNotiList.size(); j++) {


                if (mSelectedNotiList.get(j).trim().equalsIgnoreCase(mNotiList.get(i).trim())) {
                    n.setSelected(true);
                }


            }
//            getPrevSelectedValues(mNotiList.get(i).trim());

//            n.setSelected(true);
//            if (getPrevSelectedValues(mNotiList.get(i).trim())) ;

            Constant.mNotificationFilterList.add(n);
        }

        mNotiFilterLstView.setAdapter(new NotificationFilterAdapter(NotificationFilterActivity.this, Constant.mNotificationFilterList));

    }


    public String collectSelectedFilterValue() {
        String mSelFilValue = null;
        for (int i = 0; i < Constant.mNotificationFilterList.size(); i++) {

            if (Constant.mNotificationFilterList.get(i).isSelected()) {


                if (mSelFilValue == null) {
                    mSelFilValue = Constant.mNotificationFilterList.get(i).getFilterName();


                } else {
                    mSelFilValue = mSelFilValue + "," + Constant.mNotificationFilterList.get(i).getFilterName();


                }

            }

        }


        return mSelFilValue;
    }


    public void screenArrange() {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        height = displayMetrics.heightPixels;
        width = displayMetrics.widthPixels;
        Constant.ScreenHeight = height;
        Constant.ScreenWidth = width;

        LinearLayout.LayoutParams backImageParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        backImageParams.width = width * 15 / 100;
        backImageParams.height = height * 7 / 100;
        backImageParams.gravity = Gravity.CENTER;
        mImgBack.setLayoutParams(backImageParams);
        mImgBack.setPadding(width * 1 / 100, height * 1 / 100, width * 1 / 100, height * 1 / 100);

        LinearLayout.LayoutParams headTxtParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        headTxtParams.width = width * 80 / 100;
        headTxtParams.height = height * 7 / 100;
        mTxtHeader.setLayoutParams(headTxtParams);
        mTxtHeader.setPadding(width * 2 / 100, 0, 0, 0);
        mTxtHeader.setGravity(Gravity.CENTER | Gravity.LEFT);

        LinearLayout.LayoutParams listHeadTxtParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        listHeadTxtParams.width = width;
        listHeadTxtParams.height = height * 5 / 100;
        mTxtListTitle.setLayoutParams(listHeadTxtParams);
        mTxtListTitle.setPadding(width * 2 / 100, 0, width * 2 / 100, 0);
        mTxtListTitle.setGravity(Gravity.CENTER);

        LinearLayout.LayoutParams btnParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        btnParams.width = width;
        btnParams.height = height * 7 / 100;
        mBtnUpdate.setLayoutParams(btnParams);
        mBtnUpdate.setPadding(width * 2 / 100, 0, width * 2 / 100, 0);
        mBtnUpdate.setGravity(Gravity.CENTER);

        if (width >= 600) {

            mTxtListTitle.setTextSize(16);
            mBtnUpdate.setTextSize(16);
            mTxtHeader.setTextSize(18);

        } else if (width > 501 && width < 600) {
            mTxtListTitle.setTextSize(15);
            mBtnUpdate.setTextSize(15);
            mTxtHeader.setTextSize(17);

        } else if (width > 260 && width < 500) {
            mTxtListTitle.setTextSize(14);
            mBtnUpdate.setTextSize(14);
            mTxtHeader.setTextSize(16);
        } else if (width <= 260) {
            mTxtListTitle.setTextSize(13);
            mBtnUpdate.setTextSize(13);
            mTxtHeader.setTextSize(15);
        }


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(NotificationFilterActivity.this, Settings.class));
        finish();
    }
}
