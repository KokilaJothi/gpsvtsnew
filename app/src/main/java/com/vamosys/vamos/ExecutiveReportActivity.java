package com.vamosys.vamos;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.SQLException;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import androidx.appcompat.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.google.gson.Gson;
import com.vamosys.model.ExecutiveReportData;
import com.vamosys.model.ExecutiveReportDataEnv;
import com.vamosys.utils.ConnectionDetector;
import com.vamosys.utils.Constant;
import com.vamosys.utils.DaoHandler;
import com.vamosys.utils.HorizontalScrollView;
import com.vamosys.utils.HorizontalScrollView.OnScrollListener;
import com.vamosys.utils.MyMarkerView;
import com.vamosys.utils.TypefaceUtil;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class ExecutiveReportActivity extends AppCompatActivity {


    ListView lv;
    //    TextView fromdate, todate, fromdatevalue, todatevalue, mHeadTitle;
    //    EditText mEdtSearch;
    TextView mHeadTitle;
    Spinner groupSpinner;
    View v1, v2, v3;
    TextView mTxtNoRecord, fromdate, todate, fromdatevalue, todatevalue;
    ImageView mBackArrow, mImgChangeDate, mImgDataViewChange;
    private int year, month, day;
    boolean from_date, to_date;
    static Const cons;

    List<String> mGroupList = new ArrayList<String>();
    List<String> mGroupSpinnerList = new ArrayList<String>();
    String mSelectedGroup, mUserId;
    String mStrFromDate, mStrToDate;

    ConnectionDetector cd;
    SharedPreferences sp;

    TableAdapter adapter;

    private static List<ExecutiveReportData> mData = new ArrayList<ExecutiveReportData>();
    List<ExecutiveReportData> mChartList = new ArrayList<ExecutiveReportData>();

    Dialog marker_info_dialog;
    TextView id_from_date, id_to_date;
    static int width;
    static int height;

    boolean isHeaderPresent = false, mIsBarChartEnabled = false;

//    private DatePickerDialog.OnDateSetListener datepickerListener = new DatePickerDialog.OnDateSetListener() {
//        // when dialog box is closed, below method will be called.
//        @Override
//        public void onDateSet(DatePicker view, int selectedYear, int selectedMonth, int selectedDay) {
//            year = selectedYear;
//            month = selectedMonth;
//            day = selectedDay;
//
//
//            if (from_date) {
//                fromdatevalue.setText(cons.getDateFormat(cons.getCalendarDate(year, month, day, 0, 0)));
//            } else if (to_date) {
//                todatevalue.setText(cons.getDateFormat(cons.getCalendarDate(year, month, day, 0, 0)));
//            }
//        }
//    };

    BarChart chart;
    private static final int SERIES_NR = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_xecutive_report);

        cons = new Const();
        init();
        screenArrange();

        sp = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        cd = new ConnectionDetector(getApplicationContext());


        if (sp.getString("ip_adds", "") != null) {
            if (sp.getString("ip_adds", "").trim().length() > 0) {
                Const.API_URL = sp.getString("ip_adds", "");
            }
        }

        adapter = new TableAdapter(this);

        Calendar cal = Calendar.getInstance();
//        cal.add(Calendar.MONTH, -1);

        cal.add(Calendar.DATE, -7);
        Date result = cal.getTime();

        SimpleDateFormat dfDate = new SimpleDateFormat("yyyy-MM-dd");

        String mCurrentDate = dfDate.format(new Date());
        final String mFromDate = dfDate.format(result);
//        mStrFromDate = mCurrentDate;
        mStrFromDate = mFromDate;
        mStrToDate = mCurrentDate;
//        fromdatevalue.setText(mFromDate);
//        todatevalue.setText(mCurrentDate);

        fromdatevalue.setText(mStrFromDate);
        todatevalue.setText(mStrToDate);

        try {
            queryUserDB();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }


//        chart.setVisibility(View.GONE);
//        BarData data = new BarData(getXAxisValues(), getDataSet());
//        data.setGroupSpace(1f);
//        data.setValueTextSize(10f);
//        chart.setData(data);
//
//        chart.animateXY(2000, 2000);
//        chart.invalidate();
//        chart.setDrawBarShadow(false);
//        chart.setDrawValueAboveBar(true);
//
//        chart.setDescription("");
//
//        // if more than 60 entries are displayed in the chart, no values will be
//        // drawn
//        chart.setMaxVisibleValueCount(60);
//        chart.setScaleMinima(8f, 1f);
//        // scaling can now only be done on x- and y-axis separately
//        chart.setPinchZoom(false);
//        chart.setDoubleTapToZoomEnabled(true);
//        chart.setDrawGridBackground(false);
//
//        XAxis xAxis = chart.getXAxis();
//        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
////        xAxis.setTypeface(mTfLight);
//        xAxis.setDrawGridLines(false);
////        xAxis.setGranularity(1f); // only intervals of 1 day
////        xAxis.setLabelCount(7);
//
//
//        YAxis leftAxis = chart.getAxisLeft();
////        leftAxis.setTypeface(mTfLight);
////        leftAxis.setLabelCount(8, false);
////        leftAxis.setValueFormatter(custom);
//        leftAxis.setPosition(YAxis.YAxisLabelPosition.OUTSIDE_CHART);
//        leftAxis.setSpaceTop(15f);
////        leftAxis.setAxisMinimum(0f); // this replaces setStartAtZero(true)
//
//        YAxis rightAxis = chart.getAxisRight();
//        rightAxis.setDrawGridLines(false);
////        rightAxis.setTypeface(mTfLight);
////        rightAxis.setLabelCount(8, false);
//        //   rightAxis.setValueFormatter("Kms");
//        rightAxis.setSpaceTop(15f);
//        rightAxis.setAxisMinimum(0f); //


        // Setting up chart
//        setupChart();

        // Start plotting chart
        //  new ChartTask().execute();


//        XAxis xAxis = chart.getXAxis();
//        // xAxis.setTypeface(tf);
//        xAxis.setTextSize(12f);
//        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
//        xAxis.setTextColor(ColorTemplate.getHoloBlue());
//        xAxis.setEnabled(true);
//        xAxis.disableGridDashedLine();
//        xAxis.setSpaceBetweenLabels(5);
//        xAxis.setDrawGridLines(false);
//        xAxis.setAvoidFirstLastClipping(true);
////
////
//// - Y Axis
//        YAxis leftAxis = chart.getAxisLeft();
//        leftAxis.removeAllLimitLines();
//        //leftAxis.setTypeface(tf);
//        leftAxis.setPosition(YAxis.YAxisLabelPosition.OUTSIDE_CHART);
//        leftAxis.setTextColor(ColorTemplate.getHoloBlue());
//        leftAxis.setAxisMaxValue(1000f);
//        leftAxis.setAxisMinValue(0f); // to set minimum yAxis
//        leftAxis.setStartAtZero(false);
//        leftAxis.enableGridDashedLine(10f, 10f, 0f);
//        leftAxis.setDrawLimitLinesBehindData(true);
//        leftAxis.setDrawGridLines(true);
//        chart.getAxisRight().setEnabled(false);
//
//
////-----------------
//        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
//        leftAxis.setPosition(YAxis.YAxisLabelPosition.OUTSIDE_CHART);


        mBackArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ExecutiveReportActivity.this, VehicleListActivity.class));
                finish();
            }
        });


        mImgDataViewChange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (mIsBarChartEnabled) {
//                    mImgDataViewChange.setBackgroundResource(R.drawable.bar_chart);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        mImgDataViewChange.setImageDrawable(getResources().getDrawable(R.drawable.bar_chart, getApplicationContext().getTheme()));
                    } else {
                        mImgDataViewChange.setImageDrawable(getResources().getDrawable(R.drawable.bar_chart));
                    }
                    mIsBarChartEnabled = false;
//                    setupBarChart();
                    chart.setVisibility(View.GONE);
                    lv.setVisibility(View.VISIBLE);

                    chart.clear();
                    chart.setData(null);
                    // chart.getData().notifyDataChanged();
                    chart.notifyDataSetChanged();

                } else {
//                    mImgDataViewChange.setBackgroundResource(R.drawable.data_table);

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        mImgDataViewChange.setImageDrawable(getResources().getDrawable(R.drawable.data_table, getApplicationContext().getTheme()));
                    } else {
                        mImgDataViewChange.setImageDrawable(getResources().getDrawable(R.drawable.data_table));
                    }

                    mIsBarChartEnabled = true;

                    chart.setVisibility(View.VISIBLE);
                    lv.setVisibility(View.GONE);


                    setupBarChart();
                }


            }
        });

        mImgChangeDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showVehicleInfoDialog();
            }
        });

        groupSpinner
                .setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                    @Override
                    public void onItemSelected(AdapterView<?> parent,
                                               View view, int pos, long id) {
                        // TODO Auto-generated method stub
                        mSelectedGroup = mGroupList.get(pos);
                        saveSelectedGroup(mGroupSpinnerList.get(pos));
                        if (mSelectedGroup.equalsIgnoreCase("Select")) {

                        } else {
//                            mEdtSearch.setText("");
                            ConnectionDetector cd = new ConnectionDetector(getApplicationContext());
                            if (cd.isConnectingToInternet()) {
//                                mStrFromDate = fromdatevalue.getText().toString().trim();
//                                mStrToDate = todatevalue.getText().toString().trim();

                                new getKmsSummaryData().execute();
                            } else {
                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.network_connection),
                                        Toast.LENGTH_SHORT).show();
                            }
                        }

                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> arg0) {
                        // TODO Auto-generated method stub

                    }

                });


    }

    public void init() {
        lv = (ListView) findViewById(R.id.executive_report_listView1);
//        lv.setVisibility(View.GONE);
        chart = (BarChart) findViewById(R.id.chart);
//        fromdate = (TextView) findViewById(R.id.executive_report_from_date_text);
//        todate = (TextView) findViewById(R.id.executive_report_to_date_text);
//        fromdatevalue = (TextView) findViewById(R.id.executive_report_from_date_value);
//        todatevalue = (TextView) findViewById(R.id.executive_report_to_date_value);
        groupSpinner = (Spinner) findViewById(R.id.executive_report_groupspinner);

        fromdate = (TextView) findViewById(R.id.from_date_text);
        todate = (TextView) findViewById(R.id.to_date_text);
        fromdatevalue = (TextView) findViewById(R.id.from_date_value);
        todatevalue = (TextView) findViewById(R.id.to_date_value);

//        v1 = (View) findViewById(R.id.executive_report_view_vertical);
//        v2 = (View) findViewById(R.id.executive_report_view_vertical1);
//        v3 = (View) findViewById(R.id.executive_report_view_horizontal);
        v1 = (View) findViewById(R.id.view_vertical);
        v2 = (View) findViewById(R.id.view_vertical1);
        mTxtNoRecord = (TextView) findViewById(R.id.executive_report_no_record_txt);
        mBackArrow = (ImageView) findViewById(R.id.executive_report_view_Back);
        mHeadTitle = (TextView) findViewById(R.id.executive_report_tvTitle);
        mImgChangeDate = (ImageView) findViewById(R.id.executive_report_change_date);
        mImgDataViewChange = (ImageView) findViewById(R.id.executive_report_change_data_view);

        fromdate.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        todate.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        fromdatevalue.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        todatevalue.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        mHeadTitle.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mImgDataViewChange.setImageDrawable(getResources().getDrawable(R.drawable.bar_chart, getApplicationContext().getTheme()));
        } else {
            mImgDataViewChange.setImageDrawable(getResources().getDrawable(R.drawable.bar_chart));
        }

    }

    private void saveSelectedGroup(String mGroupName) {
        try {
            SharedPreferences.Editor editor = sp.edit();
            editor.putString("selected_group", mGroupName);
            editor.commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private boolean showVehicleInfoDialog() {

        TextView
                id__popupto_date_label,
                id_popup_from_label,
                id_txt_divider1, id_txt_divider2, id_histroy_vehicleid;
        Button id_history_selection_done, id_history_selection_cancel;
        LinearLayout date_time_lay;
//        if (isFromList) {
//            marker_info_dialog = new Dialog(this, android.R.style.Theme_Light_NoTitleBar_Fullscreen);
//            marker_info_dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
//        } else {
        marker_info_dialog = new Dialog(ExecutiveReportActivity.this);
//        }
        marker_info_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        marker_info_dialog.setContentView(R.layout.trip_summary_popup_layout);
        marker_info_dialog.setCancelable(false);
        //LinearLayout id_date_time_picker_layout;
        LinearLayout id_from_datetime_layout;
//        ImageView id_history_vehicle_info_icon;
//        final TextView id_histroy_vehicleid, id_history_type_value, id_from_label, id_from_date, id_from_time, id_to_date, id_to_time;
        // Button id_history_selection_done, id_history_selection_cancel;

        id_from_datetime_layout = (LinearLayout) marker_info_dialog.findViewById(R.id.id_popup_trip_summ_from_datetime_layout);


        // id_history_vehicle_info_icon = (ImageView) marker_info_dialog.findViewById(R.id.id_popup_history_vehicle_info_icon);
        id_histroy_vehicleid = (TextView) marker_info_dialog.findViewById(R.id.id_popup_trip_summ_vehicleid);

        id_txt_divider1 = (TextView) marker_info_dialog.findViewById(R.id.trip_summ_divider_txt_1);
        id_txt_divider2 = (TextView) marker_info_dialog.findViewById(R.id.trip_summ_divider_txt_2);
        date_time_lay = marker_info_dialog.findViewById(R.id.id_date_time_from_time_layout);
        date_time_lay.setVisibility(View.GONE);


        // id_popup_history_labelid = (TextView) marker_info_dialog.findViewById(R.id.id_popup_history_labelid);
        //   id_popup_history_type_value = (TextView) marker_info_dialog.findViewById(R.id.id_popup_history_type_value);
        id_popup_from_label = (TextView) marker_info_dialog.findViewById(R.id.id_popup_trip_summ_from_label);

        id_from_date = (TextView) marker_info_dialog.findViewById(R.id.id_popup_trip_summ_from_date);
        id__popupto_date_label = (TextView) marker_info_dialog.findViewById(R.id.id_popup_trip_summ_to_date_label);
        id_to_date = (TextView) marker_info_dialog.findViewById(R.id.id_popup_trip_summ_to_date);


//        id__popupto_date_label = (TextView) marker_info_dialog.findViewById(R.id.id__popupto_date_label);
//        id_to_date = (TextView) marker_info_dialog.findViewById(R.id.id_popup_to_date);
//        id_popup_to_time_label = (TextView) marker_info_dialog.findViewById(R.id.id_popup_to_time_label);
//        id_to_time = (TextView) marker_info_dialog.findViewById(R.id.id_popup_to_time);

        id_history_selection_done = (Button) marker_info_dialog.findViewById(R.id.id_popup_trip_summ_selection_done);
        id_history_selection_cancel = (Button) marker_info_dialog.findViewById(R.id.id_popup_trip_summ_selection_cancel);


//        id_from_date.setText(cons.GetCurrentDate(Calendar.getInstance()));
//        id_to_date.setText(cons.GetCurrentDate(Calendar.getInstance()));


        id_from_date.setText(mStrFromDate);
        id_to_date.setText(mStrToDate);

        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        // int height = metrics.heightPixels;
        //  int width = metrics.widthPixels;
        Constant.ScreenWidth = width;
        Constant.ScreenHeight = height;

//        LinearLayout.LayoutParams history_for_layout = new LinearLayout.LayoutParams(
//                LinearLayout.LayoutParams.WRAP_CONTENT,
//                LinearLayout.LayoutParams.WRAP_CONTENT);
//        history_for_layout.width = width * 90 / 100;
//        history_for_layout.height = LinearLayout.LayoutParams.WRAP_CONTENT;
//        history_for_layout.gravity = Gravity.CENTER;
//        history_for_layout.setMargins(width * 5 / 100, 0, width * 5 / 100, 0);
//        id_history_type_layout.setLayoutParams(history_for_layout);

//        LinearLayout.LayoutParams backImageParams = new LinearLayout.LayoutParams(
//                LinearLayout.LayoutParams.WRAP_CONTENT,
//                LinearLayout.LayoutParams.WRAP_CONTENT);
//        backImageParams.width = width * 80 / 100;
//        backImageParams.height = LinearLayout.LayoutParams.WRAP_CONTENT;
//        backImageParams.gravity = Gravity.CENTER;
//
//        id_popup_history_labelid.setLayoutParams(backImageParams);
//        id_popup_history_type_value.setLayoutParams(backImageParams);

        LinearLayout.LayoutParams backImageParams1 = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        backImageParams1.width = width * 40 / 100;
        backImageParams1.height = LinearLayout.LayoutParams.WRAP_CONTENT;

        id_popup_from_label.setLayoutParams(backImageParams1);
        id_from_date.setLayoutParams(backImageParams1);
        id__popupto_date_label.setLayoutParams(backImageParams1);
        id_to_date.setLayoutParams(backImageParams1);

        id__popupto_date_label.setLayoutParams(backImageParams1);
        id_to_date.setLayoutParams(backImageParams1);
        id__popupto_date_label.setLayoutParams(backImageParams1);
        id_to_date.setLayoutParams(backImageParams1);


        if (width >= 600) {
            // id_popup_history_labelid.setTextSize(18);
            // id_popup_history_type_value.setTextSize(17);
            id_from_date.setTextSize(17);
            //  id_from_time.setTextSize(17);
            id_to_date.setTextSize(17);
            // id_to_time.setTextSize(17);
            //  $TxtOfflineVehicleValue.setTextSize(16);
        } else if (width > 501 && width < 600) {
            // id_popup_history_labelid.setTextSize(17);
            // id_popup_history_type_value.setTextSize(16);
            id_from_date.setTextSize(16);
            //id_from_time.setTextSize(16);
            id_to_date.setTextSize(16);
            // id_to_time.setTextSize(16);
            // $TxtOfflineVehicleValue.setTextSize(15);
        } else if (width > 260 && width < 500) {
            //id_popup_history_labelid.setTextSize(16);
            // id_popup_history_type_value.setTextSize(15);
            id_from_date.setTextSize(15);
            // id_from_time.setTextSize(15);
            id_to_date.setTextSize(15);
            //  id_to_time.setTextSize(15);
            // $TxtOfflineVehicleValue.setTextSize(14);
        } else if (width <= 260) {
            //  id_popup_history_labelid.setTextSize(15);
            //  id_popup_history_type_value.setTextSize(14);
            id_from_date.setTextSize(14);
            //  id_from_time.setTextSize(14);
            id_to_date.setTextSize(14);
            // id_to_time.setTextSize(14);
            //  $TxtOfflineVehicleValue.setTextSize(13);
        }


//        id_histroy_vehicleid.setText(Constant.SELECTED_VEHICLE_SHORT_NAME);
        id_histroy_vehicleid.setText(getResources().getString(R.string.executive_report));


        /** listener to handle when history is canceled */
        id_history_selection_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                opptionPopUp();
                marker_info_dialog.hide();
            }
        });

        /** listener to handle when history is selected */
        id_history_selection_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (cd.isConnectingToInternet()) {
                    marker_info_dialog.hide();
//                    id_date_time_picker_layout.setVisibility(View.GONE);
//                    id_map_info_layout.setVisibility(View.GONE); "http://188.166.244.126:9000/
//
                    adapter = null;
                    adapter = new TableAdapter(ExecutiveReportActivity.this);
//     String url = Const.API_URL + "/getTripHistorySummary?userId=" + Constant.SELECTED_USER_ID + "&vehicleId=" + Constant.SELECTED_VEHICLE_ID + "&fromDate=" + id_from_date.getText().toString() + "&toDate=" + id_to_date.getText().toString();
                    mData.clear();
                    lv.setAdapter(null);

                    mStrFromDate = id_from_date.getText().toString().trim();
                    mStrToDate = id_to_date.getText().toString().trim();
//                    String url = Const.API_URL + "mobile/getTripHistorySummary?userId=" + Constant.SELECTED_USER_ID + "&vehicleId=" + Constant.SELECTED_VEHICLE_ID + "&fromDate=" + id_from_date.getText().toString() + "&toDate=" + id_to_date.getText().toString();
                    // String url = Const.API_URL + "mobile/getVehicleHistory4MobileV2?userId=" + Constant.SELECTED_USER_ID + "&vehicleId=" + Constant.SELECTED_VEHICLE_ID + "&fromDate=" + id_from_date.getText().toString() + "&fromTime=" + id_from_time.getText().toString() + "&toDate=" + id_to_date.getText().toString() + "&toTime=" + id_to_time.getText().toString() + "&macid=" + getStdTableValue("macid") + "&appid=" + getStdTableValue("appid");
                    // String par[] = {Const.API_URL + "mobile/getGeoFenceView?vehicleId=" + Constant.SELECTED_VEHICLE_ID + "&userId=" + Constant.SELECTED_USER_ID + "&macid=" + getStdTableValue("macid") + "&appid=" + getStdTableValue("appid")};
                    new getKmsSummaryData().execute();
                    //  new GetPOIInformation().execute(par);
                } else {
                    Toast internet_toast = Toast.makeText(getApplicationContext(), ""+getResources().getString(R.string.network_connection), Toast.LENGTH_LONG);
                    internet_toast.show();
                }
            }
        });

        id_from_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar calNow = Calendar.getInstance();

                if (mStrFromDate != null) {
                    String[] dateArr = mStrFromDate.split("-");
                    int yearValue = 0, monthValue = 0, dateValue = 0;
                    if (dateArr.length > 0) {


                        yearValue = Integer.valueOf(dateArr[0]);
                        monthValue = Integer.valueOf(dateArr[1]);
                        dateValue = Integer.valueOf(dateArr[2]);

                        if (monthValue > 0) {
                            monthValue = monthValue - 1;
                        }

                    }
                    calNow.set(yearValue, monthValue, dateValue);
                }

                new DatePickerDialog(ExecutiveReportActivity.this, new DatePickerDialog.OnDateSetListener() {
                    // when dialog box is closed, below method will be called.
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int day) {
                        id_from_date.setText(cons.getDateFormat(cons.getCalendarDate(year, month, day, 0, 0)));
                    }
                }, calNow.get(Calendar.YEAR), calNow.get(Calendar.MONTH), calNow.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        id_to_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar calNow = Calendar.getInstance();

                if (mStrToDate != null) {
                    String[] dateArr = mStrToDate.split("-");
                    int yearValue = 0, monthValue = 0, dateValue = 0;
                    if (dateArr.length > 0) {


                        yearValue = Integer.valueOf(dateArr[0]);
                        monthValue = Integer.valueOf(dateArr[1]);
                        dateValue = Integer.valueOf(dateArr[2]);

                        if (monthValue > 0) {
                            monthValue = monthValue - 1;
                        }

                    }
                    calNow.set(yearValue, monthValue, dateValue);
                }

                new DatePickerDialog(ExecutiveReportActivity.this, new DatePickerDialog.OnDateSetListener() {
                    // when dialog box is closed, below method will be called.
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int day) {
                        id_to_date.setText(cons.getDateFormat(cons.getCalendarDate(year, month, day, 0, 0)));
                    }
                }, calNow.get(Calendar.YEAR), calNow.get(Calendar.MONTH), calNow.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        /** listener to handle when To date is clicked in history selection*/
//        id_to_date.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                from_date = false;
//                to_date = true;
//                // from_time = false;
//                // to_time = false;
//                showDialog(DATE_PICKER_ID);
//            }
//        });
        /** listener to handle when To Time is clicked in history selection*/
//        id_to_time.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                from_date = false;
//                to_date = false;
//                from_time = false;
//                to_time = true;
//                showDialog(TIME_DIALOG_ID);
//            }
//        });


        marker_info_dialog.show();
        return true;
    }

    public void queryUserDB() {
        DaoHandler da = new DaoHandler(getApplicationContext(), false);
        da.queryUserDB();

        mUserId = Constant.mDbUserId;
        mGroupList = Constant.mUserGroupList;
        setDropDownData();
    }

//    public void queryUserDB() {
//        DataBaseHandler db = new DataBaseHandler(this);
//        Cursor c = null;
//        // String qry = "SELECT DISTINCT patch_id,patch_name,mtp_id FROM "
//        // + DbHandler.TABLE_TM_DCP;
//        try {
//            // c = db.open().getDatabaseObj().rawQuery(qry, null);
//            c = db.open()
//                    .getDatabaseObj()
//                    .query(DataBaseHandler.TABLE_USER, null, null, null, null, null,
//                            null);
//            // int reporting_date_index = c
//            // .getColumnIndex("actual_reporting_date");
//            int user_id_index = c.getColumnIndex("user_id");
//            int group_list_index = c.getColumnIndex("group_list");
//            // int mtp_id_index = c.getColumnIndex("mtp_id");
//            // int dr_ch_name_index = c.getColumnIndex("dr_ch_name");
//            // System.out.println("The coubnt of daily count is ::::::"
//            // + c.getCount());
//            if (c.getCount() > 0) {
//
//                while (c.moveToNext()) {
//                    mUserId = c.getString(user_id_index);
//
////                    System.out.println("The group list is :::::"
////                            + c.getString(group_list_index));
//
//                    if (c.getString(group_list_index) != null) {
//                        Gson g = new Gson();
//                        InputStream is = new ByteArrayInputStream(c.getString(
//                                group_list_index).getBytes());
//                        Reader reader = new InputStreamReader(is);
//                        Type fooType = new TypeToken<List<String>>() {
//                        }.getType();
//
//                        mGroupList = g.fromJson(reader, fooType);
//                    }
//
//                }
//            }
//
//        } finally {
//            c.close();
//            db.close();
//        }
//        setDropDownData();
//    }


    public void setDropDownData() {
        //mGroupList = new ArrayList<String>();
        // List<String> list = new ArrayList<String>();
        // list.add("Select");
        if (mGroupList.size() > 0) {

        } else {
            mGroupList.add("Select");
        }


        for (int i = 0; i < mGroupList.size(); i++) {

            String[] str_msg_data_array = mGroupList.get(i).split(":");

            mGroupSpinnerList.add(str_msg_data_array[0]);

        }


        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(
                ExecutiveReportActivity.this, R.layout.spinner_row,
                mGroupSpinnerList) {

            public View getView(int position, View convertView, ViewGroup parent) {
                View v = super.getView(position, convertView, parent);
                // TextView v = (TextView) super.getView(position,
                // convertView,parent);
                // Constant.face(DailyCalls.this, (TextView) v);
                return v;
            }

            public View getDropDownView(int position, View convertView,
                                        ViewGroup parent) {
                View v = super.getDropDownView(position, convertView, parent);
                // ((TextView)
                // v).setBackgroundColor(Color.parseColor("#BBfef3da"));
                // TextView v = (TextView) super.getView(position,
                // convertView,parent);
                // Constant.face(DailyCalls.this, (TextView) v);
                return v;
            }
        };
        spinnerArrayAdapter
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        groupSpinner.setAdapter(spinnerArrayAdapter);
        String groupName = sp.getString("selected_group", "");
        int spinnerPosition = spinnerArrayAdapter.getPosition(groupName);

//set the default according to value
        groupSpinner.setSelection(spinnerPosition);
    }


    private void screenArrange() {
        // TODO Auto-generated method stub

		/* screen arrangements */
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        height = metrics.heightPixels;
        width = metrics.widthPixels;
        Constant.ScreenWidth = width;
        Constant.ScreenHeight = height;

        LinearLayout.LayoutParams backImageParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        backImageParams.width = width * 15 / 100;
        backImageParams.height = height * 7 / 100;
        backImageParams.gravity = Gravity.CENTER;
        mBackArrow.setLayoutParams(backImageParams);
        mBackArrow.setPadding(width * 1 / 100, height * 1 / 100, width * 1 / 100, height * 1 / 100);

        LinearLayout.LayoutParams headTxtParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        headTxtParams.width = width * 62 / 100;
        headTxtParams.height = height * 7 / 100;
        mHeadTitle.setLayoutParams(headTxtParams);
        mHeadTitle.setPadding(width * 2 / 100, 0, 0, 0);
        mHeadTitle.setGravity(Gravity.CENTER | Gravity.LEFT);


        LinearLayout.LayoutParams changeDataImageParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        changeDataImageParams.width = width * 9 / 100;
        changeDataImageParams.height = height * 7 / 100;
        changeDataImageParams.gravity = Gravity.CENTER;
        mImgDataViewChange.setLayoutParams(changeDataImageParams);
        mImgDataViewChange.setPadding(width * 1 / 100, height * 1 / 100,
                width * 1 / 100, height * 1 / 100);


        LinearLayout.LayoutParams changeDateImageParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        changeDateImageParams.width = width * 9 / 100;
        changeDateImageParams.height = height * 7 / 100;
        changeDateImageParams.gravity = Gravity.CENTER;
        mImgChangeDate.setLayoutParams(changeDateImageParams);
        mImgChangeDate.setPadding(width * 1 / 100, height * 1 / 100,
                width * 1 / 100, height * 1 / 100);

//        LinearLayout.LayoutParams textParams = new LinearLayout.LayoutParams(
//                LinearLayout.LayoutParams.WRAP_CONTENT,
//                LinearLayout.LayoutParams.WRAP_CONTENT);
//        textParams.width = width * 45 / 100;
//        textParams.height = height * 9 / 2 / 100;
//        textParams.topMargin = (int) (height * 0.25 / 100);
//        textParams.leftMargin = width * 2 / 100;
//        textParams.rightMargin = width * 2 / 100;
//        fromdate.setLayoutParams(textParams);
//        todate.setLayoutParams(textParams);
//        fromdate.setGravity(Gravity.CENTER);
//        todate.setGravity(Gravity.CENTER);

//        LinearLayout.LayoutParams textValueParams = new LinearLayout.LayoutParams(
//                LinearLayout.LayoutParams.WRAP_CONTENT,
//                LinearLayout.LayoutParams.WRAP_CONTENT);
//        textValueParams.width = width * 45 / 100;
//        textValueParams.height = height * 9 / 2 / 100;
//        textValueParams.leftMargin = width * 2 / 100;
//        textValueParams.rightMargin = width * 2 / 100;
//        textValueParams.bottomMargin = (int) (height * 0.25 / 100);
//        fromdatevalue.setLayoutParams(textValueParams);
//        todatevalue.setLayoutParams(textValueParams);
//        fromdatevalue.setGravity(Gravity.CENTER);
//        todatevalue.setGravity(Gravity.CENTER);

//        LinearLayout.LayoutParams ViewParams = new LinearLayout.LayoutParams(
//                LinearLayout.LayoutParams.WRAP_CONTENT,
//                LinearLayout.LayoutParams.WRAP_CONTENT);
//        ViewParams.width = width * 1 / 100;
//        ViewParams.height = LinearLayout.LayoutParams.MATCH_PARENT;
//        v1.setLayoutParams(ViewParams);
//        v2.setLayoutParams(ViewParams);
//
//        LinearLayout.LayoutParams viewParams = new LinearLayout.LayoutParams(
//                LinearLayout.LayoutParams.WRAP_CONTENT,
//                LinearLayout.LayoutParams.WRAP_CONTENT);
//        viewParams.width = width;
//        viewParams.height = height * 1 / 2 / 100;
//        v3.setLayoutParams(viewParams);

        LinearLayout.LayoutParams spinnerParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        spinnerParams.width = width * 95 / 100;
        spinnerParams.height = height * 6 / 100;
        spinnerParams.topMargin = (int) (height * 1.25 / 100);
        spinnerParams.leftMargin = width * 2 / 100;
        spinnerParams.rightMargin = width * 2 / 100;
        spinnerParams.bottomMargin = (int) (height * 0.25 / 100);
        groupSpinner.setLayoutParams(spinnerParams);


        LinearLayout.LayoutParams textParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        textParams.width = width * 45 / 100;
        textParams.height = height * 5 / 100;
        textParams.topMargin = (int) (height * 0.25 / 100);
        textParams.leftMargin = width * 2 / 100;
        textParams.rightMargin = width * 2 / 100;
        fromdate.setLayoutParams(textParams);
        todate.setLayoutParams(textParams);
        fromdate.setGravity(Gravity.CENTER);
        todate.setGravity(Gravity.CENTER);

        LinearLayout.LayoutParams textValueParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        textValueParams.width = width * 45 / 100;
        textValueParams.height = height * 5 / 100;
        textValueParams.leftMargin = width * 2 / 100;
        textValueParams.rightMargin = width * 2 / 100;
        textValueParams.bottomMargin = (int) (height * 0.25 / 100);
        fromdatevalue.setLayoutParams(textValueParams);
        todatevalue.setLayoutParams(textValueParams);
        fromdatevalue.setGravity(Gravity.CENTER);
        todatevalue.setGravity(Gravity.CENTER);


        LinearLayout.LayoutParams lineParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        lineParams.width = width * 98 / 100;
        lineParams.height = LinearLayout.LayoutParams.WRAP_CONTENT;
        lineParams.topMargin = (int) (height * 0.25 / 100);
        lineParams.bottomMargin = height * 1 / 100;
        lineParams.leftMargin = width * 1 / 100;
        lineParams.rightMargin = width * 1 / 100;
        lv.setLayoutParams(lineParams);


        LinearLayout.LayoutParams ViewParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        ViewParams.width = width * 1 / 100;
        ViewParams.height = LinearLayout.LayoutParams.MATCH_PARENT;
        v1.setLayoutParams(ViewParams);
        v2.setLayoutParams(ViewParams);


//        LinearLayout.LayoutParams headTxtParams1 = new LinearLayout.LayoutParams(
//                LinearLayout.LayoutParams.WRAP_CONTENT,
//                LinearLayout.LayoutParams.WRAP_CONTENT);
//        headTxtParams1.width = width * 76 / 100;
//        headTxtParams1.height = height * 8 / 100;
//        headTxtParams1.setMargins(width * 4 / 100, width * 2 / 100, 1, width * 1 / 100);
////        headTxtParams1.setMargins();
////        $TxtTitle.setLayoutParams(headTxtParams);
////        $TxtTitle.setPadding(width * 2 / 100, 0, 0, 0);
////        $TxtTitle.setGravity(Gravity.CENTER);
//        mEdtSearch.setLayoutParams(headTxtParams1);
////        mEdtSearch.setm
//        mEdtSearch.setPadding(width * 2 / 100, 0, width * 4 / 100, 0);
//        mEdtSearch.setGravity(Gravity.CENTER | Gravity.CENTER);


        if (width >= 600) {
            fromdate.setTextSize(16);
            todate.setTextSize(16);
            fromdatevalue.setTextSize(15);
            todatevalue.setTextSize(15);
            mTxtNoRecord.setTextSize(18);
            mHeadTitle.setTextSize(18);
            //mEdtSearch.setTextSize(18);
        } else if (width > 501 && width < 600) {
            fromdate.setTextSize(15);
            todate.setTextSize(15);
            fromdatevalue.setTextSize(14);
            todatevalue.setTextSize(14);
            mTxtNoRecord.setTextSize(17);
            mHeadTitle.setTextSize(17);
            //  mEdtSearch.setTextSize(17);
        } else if (width > 260 && width < 500) {
            fromdate.setTextSize(14);
            todate.setTextSize(14);
            fromdatevalue.setTextSize(13);
            todatevalue.setTextSize(13);
            mTxtNoRecord.setTextSize(16);
            mHeadTitle.setTextSize(16);
            //  mEdtSearch.setTextSize(16);
        } else if (width <= 260) {
            fromdate.setTextSize(13);
            todate.setTextSize(13);
            fromdatevalue.setTextSize(12);
            todatevalue.setTextSize(12);
            mTxtNoRecord.setTextSize(15);
            mHeadTitle.setTextSize(15);
            //  mEdtSearch.setTextSize(15);
        }
    }

    private class getKmsSummaryData extends AsyncTask<String, String, String> {
        ProgressDialog progressDialog;

        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub

            String result = null;
            try {
                Const constaccess = new Const();

//New API

                result = constaccess
                        .sendGet(Const.API_URL + "/mobile/getExecutiveReport?groupId=" + mSelectedGroup + "&fromDate=" + mStrFromDate + "&toDate=" + mStrToDate + "&userId=" + mUserId, 30000);


            } catch (Exception e) {

            }

            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            progressDialog.dismiss();
            if (result != null && result.length() > 0) {
                System.out.println("The kms result is ::::" + result);
                mData = new ArrayList<ExecutiveReportData>();
                ExecutiveReportDataEnv kmsResponse;

                Gson g = new Gson();

                kmsResponse = g.fromJson(result.trim(),
                        ExecutiveReportDataEnv.class);
                if (kmsResponse.getExecReportData() != null) {

                    mData = new ArrayList<ExecutiveReportData>(
                            Arrays.asList(kmsResponse.getExecReportData()));

                    fromdatevalue.setText(mStrFromDate);
                    todatevalue.setText(mStrToDate);

                }
                setTableLayoutData();
            }

        }

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            progressDialog = new ProgressDialog(ExecutiveReportActivity.this,
                    AlertDialog.THEME_HOLO_LIGHT);
            progressDialog.setMessage(getResources().getString(R.string.progress_dialog));
            progressDialog.setProgressDrawable(new ColorDrawable(
                    Color.BLUE));
            progressDialog.setCancelable(true);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.show();
        }

    }


    public void setTableLayoutData() {

//        adapter.setHeader(mHeader);
//        ListView listView = (ListView) findViewById(R.id.executive_report_listView1);

        if (isHeaderPresent) {

        } else {

            lv.addHeaderView(adapter.getHeaderView(lv));
            isHeaderPresent = true;
        }


        lv.setAdapter(adapter);
        adapter.setData(mData);

        adapter.notifyDataSetChanged();

        parseVehicleKms();

    }


    public static class TableAdapter extends BaseAdapter {

        // private static final String TAG = "TableAdapter";

        private Context mContext;

        private String[] mHeader;


        private int mCurrentScroll;

        private int[] mColResources = {R.id.vehicle_name_textView, R.id.date_textView,
                R.id.kms_textView, R.id.park_count_textView, R.id.over_speed_count_textView, R.id.odo_start_textView,
                R.id.odo_end_textView, R.id.running_textView, R.id.idle_textView};

        public TableAdapter(Context context) {
            super();
            mContext = context;
        }

        @Override
        public int getCount() {
            return mData != null ? mData.size() : 0;
        }

        @Override
        public Object getItem(int position) {
            return mData.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            HorizontalScrollView view = null;

            if (convertView == null) {
                LayoutInflater inflater = (LayoutInflater) mContext
                        .getSystemService(LAYOUT_INFLATER_SERVICE);
                view = (HorizontalScrollView) inflater.inflate(
                        R.layout.executive_list_item_table_row, parent, false);

//                view.setOnScrollChangeListener(new OnScrollListener() {
//                    @Override
//                    public void onScrollChanged(View view, int scrollX) {
//
//                    }
//                });


                view.setOnScrollListener(new OnScrollListener() {

                    @Override
                    public void onScrollChanged(View scrollView, int scrollX) {

                        mCurrentScroll = scrollX;
                        ListView listView = (ListView) scrollView.getParent();
                        if (listView == null)
                            return;

                        for (int i = 0; i < listView.getChildCount(); i++) {
                            View child = listView.getChildAt(i);
                            if (child instanceof HorizontalScrollView
                                    && child != scrollView) {
                                HorizontalScrollView scrollView2 = (HorizontalScrollView) child;
                                if (scrollView2.getScrollX() != mCurrentScroll) {
                                    scrollView2.setScrollX(mCurrentScroll);
                                }
                            }
                        }
                    }
                });

            } else {
                view = (HorizontalScrollView) convertView;
            }

            view.setScrollX(mCurrentScroll);

            if (position % 2 == 0) {
                view.setBackgroundColor(Color.WHITE);
            } else {
                view.setBackgroundColor(Color.LTGRAY);
            }

            ExecutiveReportData data = mData.get(position);

//            for (int i = 0; i < mColResources.length; i++) {
            TextView col1 = (TextView) view.findViewById(mColResources[0]);

            col1.setText(data.getShortName());
            TextView col2 = (TextView) view.findViewById(mColResources[1]);
            col2.setText(data.getDate());
            TextView col3 = (TextView) view.findViewById(mColResources[2]);
            col3.setText(String.valueOf(data.getDistanceToday()) + " "+view.getResources().getString(R.string.kms));
            TextView col4 = (TextView) view.findViewById(mColResources[3]);
            col4.setText(String.valueOf(data.getParkingCount()));
            TextView col5 = (TextView) view.findViewById(mColResources[4]);
            col5.setText(String.valueOf(data.getOverSpeedInstances()));
            TextView col6 = (TextView) view.findViewById(mColResources[5]);
            col6.setText(String.valueOf(data.getOdoOpeningReading()));
            TextView col7 = (TextView) view.findViewById(mColResources[6]);
            col7.setText(String.valueOf(data.getOdoClosingReading()));
            TextView col8 = (TextView) view.findViewById(mColResources[7]);
            col8.setText(cons.getVehicleTime(String.valueOf(data.getTotalRunningTime())));
            TextView col9 = (TextView) view.findViewById(mColResources[8]);
            col9.setText(cons.getVehicleTime(String.valueOf(data.getTotalIdleTime())));


            LinearLayout.LayoutParams vehicleNameParams = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT);
            vehicleNameParams.width = width * 35 / 100;
            vehicleNameParams.height = height * 7 / 100;
            col1.setLayoutParams(vehicleNameParams);
            col1.setPadding(width * 2 / 100, 0, 0, 0);
            col1.setGravity(Gravity.CENTER | Gravity.LEFT);

            col5.setLayoutParams(vehicleNameParams);
            col5.setPadding(width * 2 / 100, 0, 0, 0);
            col5.setGravity(Gravity.CENTER | Gravity.LEFT);

            LinearLayout.LayoutParams text2Params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT);
            text2Params.width = width * 25 / 100;
            text2Params.height = height * 7 / 100;
            col2.setLayoutParams(text2Params);
            col2.setPadding(width * 2 / 100, 0, 0, 0);
            col2.setGravity(Gravity.CENTER | Gravity.LEFT);

            col3.setLayoutParams(text2Params);
            col3.setPadding(width * 2 / 100, 0, 0, 0);
            col3.setGravity(Gravity.CENTER | Gravity.LEFT);

            col4.setLayoutParams(text2Params);
            col4.setPadding(width * 2 / 100, 0, 0, 0);
            col4.setGravity(Gravity.CENTER | Gravity.LEFT);


            col6.setLayoutParams(text2Params);
            col6.setPadding(width * 2 / 100, 0, 0, 0);
            col6.setGravity(Gravity.CENTER | Gravity.LEFT);

            col7.setLayoutParams(text2Params);
            col7.setPadding(width * 2 / 100, 0, 0, 0);
            col7.setGravity(Gravity.CENTER | Gravity.LEFT);

            col8.setLayoutParams(text2Params);
            col8.setPadding(width * 2 / 100, 0, 0, 0);
            col8.setGravity(Gravity.CENTER | Gravity.LEFT);

            col9.setLayoutParams(text2Params);
            col9.setPadding(width * 2 / 100, 0, 0, 0);
            col9.setGravity(Gravity.CENTER | Gravity.LEFT);


//            }

            return view;
        }

        public View getHeaderView(ViewGroup parent) {

            LayoutInflater inflater = (LayoutInflater) mContext
                    .getSystemService(LAYOUT_INFLATER_SERVICE);
            HorizontalScrollView view = (HorizontalScrollView) inflater
                    .inflate(R.layout.executive_list_item_table_header, parent, false);

//            for (int i = 0; i < mColResources.length; i++) {
//                TextView col = (TextView) view.findViewById(mColResources[i]);
//                col.setText(mHeader[i]);
//            }


            view.setOnScrollListener(new OnScrollListener() {

                @Override
                public void onScrollChanged(View scrollView, int scrollX) {

                    mCurrentScroll = scrollX;
                    ListView listView = (ListView) scrollView.getParent();
                    if (listView == null)
                        return;

                    for (int i = 0; i < listView.getChildCount(); i++) {
                        View child = listView.getChildAt(i);
                        if (child instanceof HorizontalScrollView
                                && child != scrollView) {
                            HorizontalScrollView scrollView2 = (HorizontalScrollView) child;
                            if (scrollView2.getScrollX() != mCurrentScroll) {
                                scrollView2.setScrollX(mCurrentScroll);
                            }
                        }
                    }
                }
            });


            TextView col1 = (TextView) view.findViewById(R.id.vehicle_name_textView_header);
            TextView col2 = (TextView) view.findViewById(R.id.date_textView_header);
            TextView col3 = (TextView) view.findViewById(R.id.kms_textView_header);
            TextView col4 = (TextView) view.findViewById(R.id.park_count_textView_header);
            TextView col5 = (TextView) view.findViewById(R.id.over_speed_count_textView_header);
            TextView col6 = (TextView) view.findViewById(R.id.odo_start_textView_header);
            TextView col7 = (TextView) view.findViewById(R.id.odo_end_textView_header);
            TextView col8 = (TextView) view.findViewById(R.id.running_textView_header);
            TextView col9 = (TextView) view.findViewById(R.id.idle_textView_header);


            LinearLayout.LayoutParams vehicleNameParams = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT);
            vehicleNameParams.width = width * 35 / 100;
            vehicleNameParams.height = height * 7 / 100;
            col1.setLayoutParams(vehicleNameParams);
            col1.setPadding(width * 2 / 100, 0, 0, 0);
            col1.setGravity(Gravity.CENTER | Gravity.LEFT);

            col5.setLayoutParams(vehicleNameParams);
            col5.setPadding(width * 2 / 100, 0, 0, 0);
            col5.setGravity(Gravity.CENTER | Gravity.LEFT);

            LinearLayout.LayoutParams text2Params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT);
            text2Params.width = width * 25 / 100;
            text2Params.height = height * 7 / 100;
            col2.setLayoutParams(text2Params);
            col2.setPadding(width * 2 / 100, 0, 0, 0);
            col2.setGravity(Gravity.CENTER | Gravity.LEFT);

            col3.setLayoutParams(text2Params);
            col3.setPadding(width * 2 / 100, 0, 0, 0);
            col3.setGravity(Gravity.CENTER | Gravity.LEFT);

            col4.setLayoutParams(text2Params);
            col4.setPadding(width * 2 / 100, 0, 0, 0);
            col4.setGravity(Gravity.CENTER | Gravity.LEFT);


            col6.setLayoutParams(text2Params);
            col6.setPadding(width * 2 / 100, 0, 0, 0);
            col6.setGravity(Gravity.CENTER | Gravity.LEFT);

            col7.setLayoutParams(text2Params);
            col7.setPadding(width * 2 / 100, 0, 0, 0);
            col7.setGravity(Gravity.CENTER | Gravity.LEFT);

            col8.setLayoutParams(text2Params);
            col8.setPadding(width * 2 / 100, 0, 0, 0);
            col8.setGravity(Gravity.CENTER | Gravity.LEFT);

            col9.setLayoutParams(text2Params);
            col9.setPadding(width * 2 / 100, 0, 0, 0);
            col9.setGravity(Gravity.CENTER | Gravity.LEFT);


            return view;
        }

//        public void setHeader(String[] header) {
//            mHeader = header;
//        }

        public void setData(List<ExecutiveReportData> data) {
            mData = data;
            notifyDataSetChanged();
        }

    }

    public void setupBarChart() {
        if (mChartList.size() > 0) {
            chart.clear();
            chart.setData(null);
            // chart.notifyDataSetChanged();
//        BarData data = new BarData(getXAxisValues(), getDataSet());
//        data.setGroupSpace(1f);
//        data.setValueTextSize(10f);
//        chart.setData(data);

            BarData data = new BarData(getDataSet());
            data.setValueTextSize(10f);
            data.setDrawValues(false);
            chart.setData(data);

            chart.animateXY(2000, 2000);
            chart.invalidate();
            chart.setDrawBarShadow(false);
            chart.setDrawValueAboveBar(true);


//        chart.setDescription("");
            chart.getDescription().setEnabled(false);

            // if more than 60 entries are displayed in the chart, no values will be
            // drawn
            chart.setMaxVisibleValueCount(60);
            chart.setScaleMinima(2f, 1f);
            // scaling can now only be done on x- and y-axis separately
            chart.setPinchZoom(false);
            chart.setDoubleTapToZoomEnabled(true);
            chart.setDrawGridBackground(false);


            MyMarkerView mv = new MyMarkerView(this, R.layout.custom_routeplayback_marker_view, getXAxisValues(), "Distance Travelled ");
            mv.setChartView(chart); // For bounds control
            chart.setMarker(mv);


            XAxis xAxis = chart.getXAxis();
            xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
//        xAxis.setTypeface(mTfLight);
            xAxis.setDrawGridLines(false);
//            xAxis.setValueFormatter(new LabelFormatter(getXAxisValues()));
            xAxis.setGranularity(1f);
            xAxis.setGranularityEnabled(true);
            xAxis.setEnabled(false);
            // xAxis.setGranularity(1f); // only intervals of 1 day
//        xAxis.setLabelCount(7);


            YAxis leftAxis = chart.getAxisLeft();
//        leftAxis.setTypeface(mTfLight);
//        leftAxis.setLabelCount(8, false);
//        leftAxis.setValueFormatter(custom);
            leftAxis.setPosition(YAxis.YAxisLabelPosition.OUTSIDE_CHART);
            leftAxis.setSpaceTop(15f);
//        leftAxis.setAxisMinimum(0f); // this replaces setStartAtZero(true)

            YAxis rightAxis = chart.getAxisRight();
            rightAxis.setDrawGridLines(false);
//        rightAxis.setTypeface(mTfLight);
//        rightAxis.setLabelCount(8, false);
            //   rightAxis.setValueFormatter("Kms");
            rightAxis.setSpaceTop(15f);

            chart.getData().notifyDataChanged();
            chart.notifyDataSetChanged();
        } else {
            chart.clear();

            chart.setData(null);
            //  chart.getData().notifyDataChanged();
            //  chart.notifyDataSetChanged();
        }
    }


    private ArrayList<IBarDataSet> getDataSet() {
        ArrayList<IBarDataSet> dataSets = null;

        ArrayList<BarEntry> valueSet1 = new ArrayList<>();
        for (int i = 0; i < mChartList.size(); i++) {
//            Log.i("Distance today ", "Value " + i + " " + mChartList.get(i).getDistanceToday());
            BarEntry v1e1 = new BarEntry(i, (float) mChartList.get(i).getDistanceToday());


            valueSet1.add(v1e1);
        }

        BarDataSet barDataSet1 = new BarDataSet(valueSet1, "X axis - Vehicles Vs Y axis - Kms");
        //barDataSet1.setValueFormatter(new PiechartLabelFormatter(getXAxisValues()));
        barDataSet1.setColors(ColorTemplate.MATERIAL_COLORS);
        dataSets = new ArrayList<>();
        dataSets.add(barDataSet1);
//        dataSets.add(barDataSet2);
        return dataSets;
    }

    private ArrayList<String> getXAxisValues() {
        ArrayList<String> xAxis = new ArrayList<>();

        for (int i = 0; i < mChartList.size(); i++) {

//            BarEntry v1e1 = new BarEntry((float) mChartList.get(i).getDistanceToday(), i);
            xAxis.add(mChartList.get(i).getShortName());
        }
        return xAxis;
    }


    public void parseVehicleKms() {
        mChartList.clear();
        for (int i = 0; i < mData.size(); i++) {

            if (mChartList != null && mChartList.size() > 0) {

                List<String> mPrevList = getPrevData(mData.get(i).getShortName());
                if (mPrevList != null && mPrevList.size() > 0) {

                    int pos = Integer.valueOf(mPrevList.get(0));
                    double mPrevKms = Double.valueOf(mPrevList.get(1));

                    ExecutiveReportData e = new ExecutiveReportData();
                    e.setShortName(mData.get(i).getShortName());
                    e.setDistanceToday(mData.get(i).getDistanceToday() + mPrevKms);
                    mChartList.set(pos, e);
                } else {
                    ExecutiveReportData e = new ExecutiveReportData();
                    e.setShortName(mData.get(i).getShortName());
                    e.setDistanceToday(mData.get(i).getDistanceToday());
                    mChartList.add(e);
                }


            } else {
                ExecutiveReportData e = new ExecutiveReportData();
                e.setShortName(mData.get(i).getShortName());
                e.setDistanceToday(mData.get(i).getDistanceToday());
                mChartList.add(e);
            }


        }
    }


    public List<String> getPrevData(String mVehiName) {
        List<String> mPrevListData = new ArrayList<String>();
        for (int i = 0; i < mChartList.size(); i++) {

            if (mChartList.get(i).getShortName().equalsIgnoreCase(mVehiName)) {
                mPrevListData.add(String.valueOf(i));
                mPrevListData.add(String.valueOf(mChartList.get(i).getDistanceToday()));
            }

        }
        return mPrevListData;
    }


//    private ArrayList<IBarDataSet> getDataSet() {
//        ArrayList<IBarDataSet> dataSets = null;
//
//        ArrayList<BarEntry> valueSet1 = new ArrayList<>();
//        for (int i = 0; i < mChartList.size(); i++) {
//
//            BarEntry v1e1 = new BarEntry((float) mChartList.get(i).getDistanceToday(), i);
//            valueSet1.add(v1e1);
//        }
//
//        BarDataSet barDataSet1 = new BarDataSet(valueSet1, "X axis - Vehicles Vs Y axis - Kms");
//        barDataSet1.setColors(ColorTemplate.COLORFUL_COLORS);
//        dataSets = new ArrayList<>();
//        dataSets.add(barDataSet1);
////        dataSets.add(barDataSet2);
//        return dataSets;
//    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(ExecutiveReportActivity.this, VehicleListActivity.class));
        finish();
    }
}
