package com.vamosys.vamos;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import androidx.fragment.app.FragmentActivity;

import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.vamosys.model.SiteAnalysisDto;
import com.vamosys.utils.ConnectionDetector;
import com.vamosys.utils.Constant;
import com.vamosys.utils.HorizontalScrollView;
import com.vamosys.utils.HttpConfig;
import com.vamosys.utils.TypefaceUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.osmdroid.api.IMapController;
import org.osmdroid.config.Configuration;
import org.osmdroid.tileprovider.tilesource.TileSourceFactory;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.infowindow.InfoWindow;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class SiteAnalysisReport extends FragmentActivity implements OnMapReadyCallback {
    static ListView lv;
    TextView mHeadTitle, mTxtNoRecord, mTxtVehicleName, mTxtVehicleNameValue, mTxtCumulativeFuel, mTxtCumulativeFuelValue, mTxtSiteExit, mTxtSiteExitValue, mTxtSiteEntry, mTxtSiteEntryValue;
    static LinearLayout mFuelDataMapLayout;
    View v1, v2;
    ImageView mBackArrow, mImgChangeDate, mImgDataViewChange, mImgDataTableMapViewChange, mImgSiteEntry, mImgSiteExit;
    ConnectionDetector cd;
    SharedPreferences sp;
    static Const cons;

    static int width;
    static int height;

    private static List<SiteAnalysisDto> mTemperatureData = new ArrayList<SiteAnalysisDto>();

    TableAdapter adapter;
    boolean isHeaderPresent = false, mIsBarChartEnabled = false;
    Dialog marker_info_dialog;
    TextView id_from_date, id_to_date, id_from_time, id_to_time;
    String mStrFromDate, mStrToDate, mStrFromTime, mStrToTime;
    String mTotalFuelValue = null;
    private static GoogleMap map;
    ImageView $ChangeView;
    boolean mIsMapPresent = false;
    String mStrUtcFromDate, mStrUtcToDate;
    String mSELECTED_MAP_TYPE = "Normal";
    static double mLatitude = 0;
    static double mLongitude = 0;
    static float vehicle_zoom_level = 15.5F;
    static Context mFuelContext;

    int mSiteEntryValue = 0, mSiteExitValue = 0;

    String mIntervalValue = "10";

    String[] mStrIntervalValue = {"Select", "1", "2", "5", "10", "15", "30"};

    //OSM related
    MapView mapview;
    LinearLayout mGoogleMapLayout;
    //    MapView mMapViewNew;
    private IMapController mapController;
    boolean isOsmEnabled = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Context ctx = getApplicationContext();
        //important! set your user agent to prevent getting banned from the osm servers
        Configuration.getInstance().load(ctx, PreferenceManager.getDefaultSharedPreferences(ctx));

        setContentView(R.layout.activity_site_analysis_report);
        cons = new Const();
        sp = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

        if (sp.getString("enabled_map", "") != null) {
            if (sp.getString("enabled_map", "").trim().length() > 0) {

//                System.out.println("hi enabled map is " + sp.getString("enabled_map", ""));

                if (sp.getString("enabled_map", "").equalsIgnoreCase(getResources().getString(R.string.osm))) {
                    isOsmEnabled = true;
                } else {
                    isOsmEnabled = false;
                }
            }
        }

        init();
        screenArrange();
        cd = new ConnectionDetector(getApplicationContext());

        if (sp.getString("ip_adds", "") != null) {
            if (sp.getString("ip_adds", "").trim().length() > 0) {
                Const.API_URL = sp.getString("ip_adds", "");
            }
        }

        mFuelContext = getApplicationContext();
        adapter = new TableAdapter(this);

        Calendar cal = Calendar.getInstance();
//        cal.add(Calendar.MONTH, -1);
        cal.add(Calendar.DATE, -7);
        Date result = cal.getTime();

        SimpleDateFormat dfDate = new SimpleDateFormat("yyyy-MM-dd");

        SimpleDateFormat timeDate = new SimpleDateFormat("HH:mm:ss");


        String mCurrentDate = dfDate.format(new Date());
        final String mFromDate = dfDate.format(result);
//        mStrFromDate = mCurrentDate;
        mStrFromDate = mFromDate;
        mStrToDate = mCurrentDate;

        String mCurrentTime = timeDate.format(new Date());
        mStrFromTime = "00:00:00";
        mStrToTime = mCurrentTime;


        if (cd.isConnectingToInternet()) {
//                                mStrFromDate = fromdatevalue.getText().toString().trim();
//                                mStrToDate = todatevalue.getText().toString().trim();

            new getKmsSummaryData().execute();
        } else {
            Toast.makeText(getApplicationContext(),
                    "Please check your network connection",
                    Toast.LENGTH_SHORT).show();
        }

        mBackArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(SiteAnalysisReport.this, MapMenuActivity.class));
                finish();
            }
        });

        mImgChangeDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showVehicleInfoDialog();
            }
        });

        $ChangeView.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                opptionPopUp();
            }
        });

        mImgDataViewChange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//               mIsMapPresent=false;
                setListData();


            }
        });


        mImgDataTableMapViewChange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mImgDataTableMapViewChange.setVisibility(View.GONE);
                if (mIsBarChartEnabled) {
//                    mImgDataViewChange.setBackgroundResource(R.drawable.bar_chart);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        mImgDataTableMapViewChange.setImageDrawable(getResources().getDrawable(R.drawable.bar_chart, getApplicationContext().getTheme()));
                    } else {
                        mImgDataTableMapViewChange.setImageDrawable(getResources().getDrawable(R.drawable.bar_chart));
                    }
                    mIsBarChartEnabled = false;
//                    setupBarChart();
                    //  chart.setVisibility(View.GONE);
                    mFuelDataMapLayout.setVisibility(View.GONE);
                    lv.setVisibility(View.VISIBLE);

                } else {
//                    mImgDataViewChange.setBackgroundResource(R.drawable.data_table);

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        mImgDataTableMapViewChange.setImageDrawable(getResources().getDrawable(R.drawable.data_table, getApplicationContext().getTheme()));
                    } else {
                        mImgDataTableMapViewChange.setImageDrawable(getResources().getDrawable(R.drawable.data_table));
                    }

                    mIsBarChartEnabled = true;

                    // chart.setVisibility(View.VISIBLE);
                    lv.setVisibility(View.GONE);

                    // setupBarChart();
                }


            }
        });


    }

    public void init() {

//        FragmentManager myFragmentManager = getSupportFragmentManager();
//        SupportMapFragment myMapFragment = (SupportMapFragment) myFragmentManager
//                .findFragmentById(R.id.temperature_data_map);
//        myMapFragment.getMapAsync(this);

//        mapview = (MapView) findViewById(R.id.osm_site_analysis_report_map);


        $ChangeView = (ImageView) findViewById(R.id.temperature_data_map_ViewIcon);
        $ChangeView.setVisibility(View.GONE);
        mFuelDataMapLayout = (LinearLayout) findViewById(R.id.temperature_data_map_view_layout);

        lv = (ListView) findViewById(R.id.temperature_report_listView1);
        mImgDataViewChange = (ImageView) findViewById(R.id.temperature_report_change_to_data_view);
        mImgDataViewChange.setVisibility(View.GONE);
//        lv.setVisibility(View.GONE);
//        chart = (BarChart) findViewById(R.id.chart);
//        fromdate = (TextView) findViewById(R.id.executive_report_from_date_text);
//        todate = (TextView) findViewById(R.id.executive_report_to_date_text);
//        fromdatevalue = (TextView) findViewById(R.id.executive_report_from_date_value);
//        todatevalue = (TextView) findViewById(R.id.executive_report_to_date_value);
//        groupSpinner = (Spinner) findViewById(R.id.executive_report_groupspinner);


        v1 = (View) findViewById(R.id.temperature_vehicle_name_view);
        v2 = (View) findViewById(R.id.temperature_cumulative_fuel_view);
//        v3 = (View) findViewById(R.id.executive_report_view_horizontal);
        //  mTxtNoRecord = (TextView) findViewById(R.id.fuelFill_report_tvTitle);
        mTxtVehicleName = (TextView) findViewById(R.id.temperature_vehicle_name_txt);
        mTxtVehicleNameValue = (TextView) findViewById(R.id.temperature_vehicle_name_value_txt);

        mTxtSiteEntry = (TextView) findViewById(R.id.site_analysis_site_entry_txt);
        mTxtSiteEntryValue = (TextView) findViewById(R.id.site_analysis_site_entry_value_txt);
        mTxtSiteExit = (TextView) findViewById(R.id.site_analysis_site_exit_txt);
        mTxtSiteExitValue = (TextView) findViewById(R.id.site_analysis_site_exit_value_txt);
        mImgSiteEntry = (ImageView) findViewById(R.id.img_site_analysis_site_entry);
        mImgSiteExit = (ImageView) findViewById(R.id.img_site_analysis_site_exit);


        mTxtVehicleNameValue.setText(Constant.SELECTED_VEHICLE_SHORT_NAME);
        mTxtCumulativeFuel = (TextView) findViewById(R.id.temperature_cumulative_fuel_txt);
        mTxtCumulativeFuelValue = (TextView) findViewById(R.id.temperature_cumulative_fuel_value_txt);
        mTxtNoRecord = (TextView) findViewById(R.id.temperature_report_no_record_txt);
        mBackArrow = (ImageView) findViewById(R.id.temperature_report_view_Back);
        mHeadTitle = (TextView) findViewById(R.id.temperature_report_tvTitle);
        mImgChangeDate = (ImageView) findViewById(R.id.temperature_report_change_date);

        mImgDataTableMapViewChange = (ImageView) findViewById(R.id.temperature_report_change_to_table_view);
        mImgDataTableMapViewChange.setVisibility(View.GONE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mImgDataTableMapViewChange.setImageDrawable(getResources().getDrawable(R.drawable.bar_chart, getApplicationContext().getTheme()));
        } else {
            mImgDataTableMapViewChange.setImageDrawable(getResources().getDrawable(R.drawable.bar_chart));
        }

//        mGoogleMapLayout = (LinearLayout) findViewById(R.id.google_map_layout);
//        RelativeLayout rl = (RelativeLayout) findViewById(R.id.map_view_relativelayout);
//        if (isOsmEnabled) {
//            $ChangeView.setVisibility(View.GONE);
//            mGoogleMapLayout.setVisibility(View.GONE);
//            rl.setVisibility(View.VISIBLE);
//            mapview = new MapView(this);
//            mapview.setTilesScaledToDpi(true);
//            rl.addView(mapview, new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.FILL_PARENT,
//                    RelativeLayout.LayoutParams.FILL_PARENT));
////        mapview.setTileSource(TileSourceFactory.MAPNIK);
//            mapview.setBuiltInZoomControls(false);
//            mapview.setMultiTouchControls(true);
//            mapController = mapview.getController();
//            mapController.setZoom(8);
//            mapview.getOverlays().clear();
//            mapview.invalidate();
//            mapview.getOverlays().add(Constant.getTilesOverlay(getApplicationContext()));
//        } else {
//            $ChangeView.setVisibility(View.VISIBLE);
//            mGoogleMapLayout.setVisibility(View.VISIBLE);
//            rl.setVisibility(View.GONE);
//            FragmentManager myFragmentManager = getSupportFragmentManager();
//            SupportMapFragment myMapFragment = (SupportMapFragment) myFragmentManager
//                    .findFragmentById(R.id.temperature_data_map);
//            myMapFragment.getMapAsync(this);
//        }

    }


    private void opptionPopUp() {
        // TODO Auto-generated method stub
        final Dialog dialog = new Dialog(SiteAnalysisReport.this);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.radio_popup);
        dialog.show();

        RadioGroup rg_home = (RadioGroup) dialog.findViewById(R.id.rg_home_views);
        RadioGroup rg_history = (RadioGroup) dialog.findViewById(R.id.rg_history_views);
        rg_history.clearCheck();
        rg_history.setVisibility(View.GONE);
        rg_home.setVisibility(View.VISIBLE);

        RadioButton $Normal = (RadioButton) dialog
                .findViewById(R.id.rb_home_normal);
        RadioButton $Satelite = (RadioButton) dialog
                .findViewById(R.id.rb_home_satellite);
        RadioButton $Terrain = (RadioButton) dialog
                .findViewById(R.id.rb_home_terrain);
        RadioButton $Hybrid = (RadioButton) dialog
                .findViewById(R.id.rb_home_hybrid);


        if (mSELECTED_MAP_TYPE.equalsIgnoreCase("Normal")) {
            $Normal.setChecked(true);
            $Satelite.setChecked(false);
            $Terrain.setChecked(false);
            $Hybrid.setChecked(false);
        } else if (mSELECTED_MAP_TYPE.equalsIgnoreCase("Satelite")) {
            $Normal.setChecked(false);
            $Satelite.setChecked(true);
            $Terrain.setChecked(false);
            $Hybrid.setChecked(false);
        } else if (mSELECTED_MAP_TYPE.equalsIgnoreCase("Terrain")) {
            $Normal.setChecked(false);
            $Satelite.setChecked(false);
            $Terrain.setChecked(true);
            $Hybrid.setChecked(false);
        } else if (mSELECTED_MAP_TYPE.equalsIgnoreCase("Hybrid")) {
            $Normal.setChecked(false);
            $Satelite.setChecked(false);
            $Terrain.setChecked(false);
            $Hybrid.setChecked(true);
        }
        $Normal.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                mapview.setTileSource(TileSourceFactory.MAPNIK);

//                map.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                mSELECTED_MAP_TYPE = "Normal";
                dialog.dismiss();
            }
        });
        $Satelite.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                mSELECTED_MAP_TYPE = "Satelite";
//                map.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
                mapview.setTileSource(TileSourceFactory.USGS_SAT);
                dialog.dismiss();
            }
        });
        $Terrain.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                mSELECTED_MAP_TYPE = "Terrain";
//                map.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
                mapview.setTileSource(TileSourceFactory.HIKEBIKEMAP);
                dialog.dismiss();
            }
        });

        $Hybrid.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                mSELECTED_MAP_TYPE = "Hybrid";
//                map.setMapType(GoogleMap.MAP_TYPE_HYBRID);
                mapview.setTileSource(TileSourceFactory.USGS_TOPO);
                dialog.dismiss();
            }
        });

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        height = displayMetrics.heightPixels;
        width = displayMetrics.widthPixels;

        LinearLayout.LayoutParams radioParama = new LinearLayout.LayoutParams(
                FrameLayout.LayoutParams.WRAP_CONTENT,
                FrameLayout.LayoutParams.WRAP_CONTENT);
        radioParama.width = width * 50 / 100;
        radioParama.height = width * 10 / 100;
        radioParama.topMargin = height * 4 / 100;
        radioParama.gravity = Gravity.CENTER;
        radioParama.leftMargin = height * 4 / 100;
        $Normal.setLayoutParams(radioParama);
        $Satelite.setLayoutParams(radioParama);
        $Terrain.setLayoutParams(radioParama);

        LinearLayout.LayoutParams radioterrainParama = new LinearLayout.LayoutParams(
                FrameLayout.LayoutParams.WRAP_CONTENT,
                FrameLayout.LayoutParams.WRAP_CONTENT);
        radioterrainParama.width = width * 50 / 100;
        radioterrainParama.height = width * 10 / 100;
        radioterrainParama.topMargin = height * 4 / 100;
        radioterrainParama.gravity = Gravity.CENTER;
        radioterrainParama.leftMargin = height * 4 / 100;
        radioterrainParama.bottomMargin = height * 4 / 100;
        $Hybrid.setLayoutParams(radioterrainParama);

        if (width >= 600) {
            $Normal.setTextSize(16);
            $Satelite.setTextSize(16);
            $Terrain.setTextSize(16);
            $Hybrid.setTextSize(16);
        } else if (width > 501 && width < 600) {
            $Normal.setTextSize(15);
            $Satelite.setTextSize(15);
            $Terrain.setTextSize(15);
            $Hybrid.setTextSize(15);
        } else if (width > 260 && width < 500) {
            $Normal.setTextSize(14);
            $Satelite.setTextSize(14);
            $Terrain.setTextSize(14);
            $Hybrid.setTextSize(14);
        } else if (width <= 260) {
            $Normal.setTextSize(13);
            $Satelite.setTextSize(13);
            $Terrain.setTextSize(13);
            $Hybrid.setTextSize(13);
        }
    }

    public void setupMap(String mAddress) {

//        if (map != null) {
//            map.clear();
//        }

        if (mapview != null) {
            InfoWindow.closeAllInfoWindowsOn(mapview);
            mapview.getOverlays().clear();
            mapview.invalidate();
            mapview.getOverlays().add(Constant.getTilesOverlay(getApplicationContext()));
        }

//        View marker = null;
//
//        marker = ((LayoutInflater) mFuelContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.custommarker, null);
//        ImageView id_custom_marker_icon = (ImageView) marker.findViewById(R.id.id_custom_marker_icon);
//        id_custom_marker_icon.setImageResource(R.drawable.grey_custom_marker_icon);
//        ImageView id_vehicle_in_marker = (ImageView) marker.findViewById(R.id.id_vehicle_in_marker);
//        id_vehicle_in_marker.setVisibility(View.GONE);
//
//        CameraPosition INIT = new CameraPosition.Builder().target(new LatLng(mLatitude, mLongitude)).zoom(vehicle_zoom_level).build();
//        map.animateCamera(CameraUpdateFactory.newCameraPosition(INIT));
//
//
//        map.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
//
//            // Use default InfoWindow frame
//            @Override
//            public View getInfoWindow(Marker arg0) {
//                return null;
//            }
//
//            // Defines the contents of the InfoWindow
//            @Override
//            public View getInfoContents(Marker arg0) {
//
//                // Getting view from the layout file info_window_layout
//                View v = getLayoutInflater().inflate(R.layout.map_info_txt_layout, null);
//
//                // Getting the position from the marker
//                LatLng latLng = arg0.getPosition();
//
//                DisplayMetrics displayMetrics = new DisplayMetrics();
//                getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
//                height = displayMetrics.heightPixels;
//                width = displayMetrics.widthPixels;
//                LinearLayout layout = (LinearLayout) v.findViewById(R.id.map_info_layout);
//                LinearLayout.LayoutParams backImageParams = new LinearLayout.LayoutParams(
//                        LinearLayout.LayoutParams.WRAP_CONTENT,
//                        LinearLayout.LayoutParams.WRAP_CONTENT);
//                backImageParams.width = width * 80 / 100;
//                //backImageParams.height = height * 10 / 100;
//                backImageParams.gravity = Gravity.CENTER;
//                layout.setLayoutParams(backImageParams);
//
//                // Getting reference to the TextView to set latitude
//                TextView txtContent = (TextView) v.findViewById(R.id.tv_txt_content);
//                txtContent.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
//
////                new GetAddressTask(txtContent).execute(mLatitude, mLongitude, 1.0);
//
//
//                StringBuffer addr = new StringBuffer();
//                Geocoder geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());
//                List<Address> addresses = null;
//                try {
//                    addresses = geocoder.getFromLocation(mLatitude, mLongitude, 1);
//                } catch (IOException e1) {
//                    e1.printStackTrace();
//                } catch (IllegalArgumentException e2) {
//                    // Error message to post in the log
////                        String errorString = "Illegal arguments " +
////                                Double.toString(params[0]) +
////                                " , " +
////                                Double.toString(params[1]) +
////                                " passed to address service";
////                        e2.printStackTrace();
////                        return errorString;
//                } catch (NullPointerException np) {
//                    // TODO Auto-generated catch block
//                    np.printStackTrace();
//                }
//                // If the reverse geocode returned an address
//                if (addresses != null && addresses.size() > 0) {
//                    // Get the first address
//                    Address address = addresses.get(0);
//                /*
//                 * Format the first line of address (if available),
//                 * city, and country name.
//                 */
//                    String addressText = null;
//
//                    for (int i = 0; i < address.getMaxAddressLineIndex(); i++) {
//                        addressText = address.getAddressLine(i);
//                        addr.append(addressText + ",");
//                    }
//                    // Return the text
//                    // return addr.toString();
//                } else {
//                    addr.append("No address found");
//                }
//
//                // txtContent.setText(mSelectedMsg);
//                txtContent.setText(Constant.SELECTED_VEHICLE_SHORT_NAME + " " + addr);
//                //  txtContent.setVisibility(View.GONE);
//
//                return v;
//
//            }
//        });


//        MarkerOptions markerOption = new MarkerOptions().position(new LatLng(mLatitude, mLongitude));
        // markerOption.title(mSelectedMsg);
        // markerOption.icon(BitmapDescriptorFactory.fromBitmap(createDrawableFromView(NotificationListActivity.this, marker)));

//
//        markerOption.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE));
//        Marker currentMarker = map.addMarker(markerOption);
//        currentMarker.showInfoWindow();

//        Marker marker = map.addMarker(new MarkerOptions()
//                .position(new LatLng(mLatitude, mLongitude))
//                .title(mSelectedMsg)
//                        // .snippet("Snippet")
//                .icon(BitmapDescriptorFactory
//                        .fromResource(R.drawable.grey_custom_marker_icon)));
//
//        marker.showInfoWindow();

        View markerView = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.marker_image_view, null);
        ImageView mImgMarkerIcon = (ImageView) markerView.findViewById(R.id.img_marker_icon);
        mImgMarkerIcon.setImageResource(R.drawable.green_custom_marker_icon);
//        animateMarker.setIcon(createDrawableFromViewNew(AcReport.this, markerView));
        org.osmdroid.views.overlay.Marker osmMarker = new org.osmdroid.views.overlay.Marker(mapview);
        osmMarker.setPosition(new GeoPoint(mLatitude, mLongitude));
        osmMarker.setAnchor(org.osmdroid.views.overlay.Marker.ANCHOR_CENTER, org.osmdroid.views.overlay.Marker.ANCHOR_BOTTOM);
//        osmMarker.setTitle(Constant.SELECTED_VEHICLE_SHORT_NAME);
        osmMarker.setIcon(Constant.createDrawableFromViewNew(SiteAnalysisReport.this, markerView));
        InfoWindow infoWindow = new MyInfoWindow(R.layout.map_info_txt_layout, mapview, mAddress);
        osmMarker.setInfoWindow(infoWindow);
        osmMarker.showInfoWindow();

        GeoPoint newPos = new GeoPoint(mLatitude, mLongitude);
        mapController.setCenter(newPos);
        mapview.getOverlays().add(osmMarker);
        mapview.invalidate();


    }

    private class MyInfoWindow extends InfoWindow {
        String text;

        public MyInfoWindow(int layoutResId, MapView mapView, String mAddressValue) {
            super(layoutResId, mapView);
            text = mAddressValue;
        }

        public void onClose() {
        }

        public void onOpen(Object arg0) {


            LinearLayout layout = (LinearLayout) mView.findViewById(R.id.map_info_layout);
            TextView txtContent = (TextView) mView.findViewById(R.id.tv_txt_content);
            DisplayMetrics displayMetrics = new DisplayMetrics();
            getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
            height = displayMetrics.heightPixels;
            width = displayMetrics.widthPixels;
//            LinearLayout layout = (LinearLayout) v.findViewById(R.id.map_info_layout);
            LinearLayout.LayoutParams backImageParams = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT);
            backImageParams.width = width * 80 / 100;
            //backImageParams.height = height * 10 / 100;
            backImageParams.gravity = Gravity.CENTER;
            layout.setLayoutParams(backImageParams);
            txtContent.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));

            txtContent.setText(Constant.SELECTED_VEHICLE_SHORT_NAME + " \n" + text);


        }
    }

    private boolean showVehicleInfoDialog() {

        TextView
                id__popupto_date_label,
                id_popup_from_label, id__popupto_time_label,
                id_popup_from_time_label,
                id_txt_divider1, id_txt_divider2, id_txt_divider3, id_histroy_vehicleid;
        Spinner mIntervalSpinner = null;
        Button id_history_selection_done, id_history_selection_cancel;
//        if (isFromList) {
//            marker_info_dialog = new Dialog(this, android.R.style.Theme_Light_NoTitleBar_Fullscreen);
//            marker_info_dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
//        } else {
        marker_info_dialog = new Dialog(SiteAnalysisReport.this);
//        }
        marker_info_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        marker_info_dialog.setContentView(R.layout.site_analysis_popup_layout);
        marker_info_dialog.setCancelable(false);
        //LinearLayout id_date_time_picker_layout;
        LinearLayout id_from_datetime_layout;
//        ImageView id_history_vehicle_info_icon;
//        final TextView id_histroy_vehicleid, id_history_type_value, id_from_label, id_from_date, id_from_time, id_to_date, id_to_time;
        // Button id_history_selection_done, id_history_selection_cancel;

        id_from_datetime_layout = (LinearLayout) marker_info_dialog.findViewById(R.id.id_popup_date_time_from_datetime_layout);


        // id_history_vehicle_info_icon = (ImageView) marker_info_dialog.findViewById(R.id.id_popup_history_vehicle_info_icon);
        id_histroy_vehicleid = (TextView) marker_info_dialog.findViewById(R.id.id_popup_date_time_vehicleid);

        id_txt_divider1 = (TextView) marker_info_dialog.findViewById(R.id.date_time_divider_txt_1);
        id_txt_divider2 = (TextView) marker_info_dialog.findViewById(R.id.date_time_divider_txt_2);
        id_txt_divider2 = (TextView) marker_info_dialog.findViewById(R.id.date_time_divider_txt_3);

        mIntervalSpinner = (Spinner) marker_info_dialog.findViewById(R.id.site_analysis_popup_report_groupspinner);


        // id_popup_history_labelid = (TextView) marker_info_dialog.findViewById(R.id.id_popup_history_labelid);
        //   id_popup_history_type_value = (TextView) marker_info_dialog.findViewById(R.id.id_popup_history_type_value);

        id_popup_from_label = (TextView) marker_info_dialog.findViewById(R.id.id_date_time_from_label);

        id_from_date = (TextView) marker_info_dialog.findViewById(R.id.id_date_time_from_date);
        id__popupto_date_label = (TextView) marker_info_dialog.findViewById(R.id.id_date_time_to_date_label);
        id_to_date = (TextView) marker_info_dialog.findViewById(R.id.id_date_time_to_date);


        id_popup_from_time_label = (TextView) marker_info_dialog.findViewById(R.id.id_date_time_from_time_label);

        id_from_time = (TextView) marker_info_dialog.findViewById(R.id.id_date_time_from_time);
        id__popupto_time_label = (TextView) marker_info_dialog.findViewById(R.id.id_date_time_to_time_label);
        id_to_time = (TextView) marker_info_dialog.findViewById(R.id.id_date_time_to_time);


//        id__popupto_date_label = (TextView) marker_info_dialog.findViewById(R.id.id__popupto_date_label);
//        id_to_date = (TextView) marker_info_dialog.findViewById(R.id.id_popup_to_date);
//        id_popup_to_time_label = (TextView) marker_info_dialog.findViewById(R.id.id_popup_to_time_label);
//        id_to_time = (TextView) marker_info_dialog.findViewById(R.id.id_popup_to_time);

        id_history_selection_done = (Button) marker_info_dialog.findViewById(R.id.id_popup_trip_summ_selection_done);
        id_history_selection_cancel = (Button) marker_info_dialog.findViewById(R.id.id_popup_trip_summ_selection_cancel);


        id_from_date.setText(cons.GetCurrentDate(Calendar.getInstance()));
        id_to_date.setText(cons.GetCurrentDate(Calendar.getInstance()));

        SimpleDateFormat timeDate = new SimpleDateFormat("HH:mm:ss");

        String mCurrentTime = timeDate.format(new Date());

        id_from_time.setText("00:00:00");
        id_to_time.setText(mCurrentTime);


        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        // int height = metrics.heightPixels;
        //  int width = metrics.widthPixels;
        Constant.ScreenWidth = width;
        Constant.ScreenHeight = height;

//        LinearLayout.LayoutParams history_for_layout = new LinearLayout.LayoutParams(
//                LinearLayout.LayoutParams.WRAP_CONTENT,
//                LinearLayout.LayoutParams.WRAP_CONTENT);
//        history_for_layout.width = width * 90 / 100;
//        history_for_layout.height = LinearLayout.LayoutParams.WRAP_CONTENT;
//        history_for_layout.gravity = Gravity.CENTER;
//        history_for_layout.setMargins(width * 5 / 100, 0, width * 5 / 100, 0);
//        id_history_type_layout.setLayoutParams(history_for_layout);

//        LinearLayout.LayoutParams backImageParams = new LinearLayout.LayoutParams(
//                LinearLayout.LayoutParams.WRAP_CONTENT,
//                LinearLayout.LayoutParams.WRAP_CONTENT);
//        backImageParams.width = width * 80 / 100;
//        backImageParams.height = LinearLayout.LayoutParams.WRAP_CONTENT;
//        backImageParams.gravity = Gravity.CENTER;
//
//        id_popup_history_labelid.setLayoutParams(backImageParams);
//        id_popup_history_type_value.setLayoutParams(backImageParams);

        LinearLayout.LayoutParams backImageParams1 = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        backImageParams1.width = width * 40 / 100;
        backImageParams1.height = LinearLayout.LayoutParams.WRAP_CONTENT;

        id_popup_from_label.setLayoutParams(backImageParams1);
        id_from_date.setLayoutParams(backImageParams1);
        id__popupto_date_label.setLayoutParams(backImageParams1);
        id_to_date.setLayoutParams(backImageParams1);

        id__popupto_date_label.setLayoutParams(backImageParams1);
        id_to_date.setLayoutParams(backImageParams1);
        id__popupto_date_label.setLayoutParams(backImageParams1);
        id_to_date.setLayoutParams(backImageParams1);


        id_popup_from_time_label.setLayoutParams(backImageParams1);
        id_from_time.setLayoutParams(backImageParams1);
        id__popupto_time_label.setLayoutParams(backImageParams1);
        id_to_time.setLayoutParams(backImageParams1);


        if (width >= 600) {
            // id_popup_history_labelid.setTextSize(18);
            // id_popup_history_type_value.setTextSize(17);
            id_from_date.setTextSize(17);
            //  id_from_time.setTextSize(17);
            id_to_date.setTextSize(17);

            id_from_time.setTextSize(17);
            id_to_time.setTextSize(17);

            // id_to_time.setTextSize(17);
            //  $TxtOfflineVehicleValue.setTextSize(16);
        } else if (width > 501 && width < 600) {
            // id_popup_history_labelid.setTextSize(17);
            // id_popup_history_type_value.setTextSize(16);
            id_from_date.setTextSize(16);
            //id_from_time.setTextSize(16);
            id_to_date.setTextSize(16);

            id_from_time.setTextSize(16);
            id_to_time.setTextSize(16);
            // id_to_time.setTextSize(16);
            // $TxtOfflineVehicleValue.setTextSize(15);
        } else if (width > 260 && width < 500) {
            //id_popup_history_labelid.setTextSize(16);
            // id_popup_history_type_value.setTextSize(15);
            id_from_date.setTextSize(15);
            // id_from_time.setTextSize(15);
            id_to_date.setTextSize(15);

            id_from_time.setTextSize(15);
            id_to_time.setTextSize(15);
            //  id_to_time.setTextSize(15);
            // $TxtOfflineVehicleValue.setTextSize(14);
        } else if (width <= 260) {
            //  id_popup_history_labelid.setTextSize(15);
            //  id_popup_history_type_value.setTextSize(14);
            id_from_date.setTextSize(14);
            //  id_from_time.setTextSize(14);
            id_to_date.setTextSize(14);

            id_from_time.setTextSize(14);
            id_to_time.setTextSize(14);
            // id_to_time.setTextSize(14);
            //  $TxtOfflineVehicleValue.setTextSize(13);
        }


        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(
                SiteAnalysisReport.this, R.layout.spinner_row,
                mStrIntervalValue) {

            public View getView(int position, View convertView, ViewGroup parent) {
                View v = super.getView(position, convertView, parent);
                // TextView v = (TextView) super.getView(position,
                // convertView,parent);
                // Constant.face(DailyCalls.this, (TextView) v);
                return v;
            }

            public View getDropDownView(int position, View convertView,
                                        ViewGroup parent) {
                View v = super.getDropDownView(position, convertView, parent);
                // ((TextView)
                // v).setBackgroundColor(Color.parseColor("#BBfef3da"));
                // TextView v = (TextView) super.getView(position,
                // convertView,parent);
                // Constant.face(DailyCalls.this, (TextView) v);
                return v;
            }
        };
        spinnerArrayAdapter
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mIntervalSpinner.setAdapter(spinnerArrayAdapter);


        id_histroy_vehicleid.setText(Constant.SELECTED_VEHICLE_SHORT_NAME);


        /** listener to handle when history is canceled */
        id_history_selection_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                opptionPopUp();
                marker_info_dialog.hide();
            }
        });


        mIntervalSpinner
                .setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                    @Override
                    public void onItemSelected(AdapterView<?> parent,
                                               View view, int pos, long id) {
                        // TODO Auto-generated method stub
                        mIntervalValue = mStrIntervalValue[pos];

                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> arg0) {
                        // TODO Auto-generated method stub

                    }

                });


        /** listener to handle when history is selected */
        id_history_selection_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (cd.isConnectingToInternet()) {
                    marker_info_dialog.hide();
//                    id_date_time_picker_layout.setVisibility(View.GONE);
//                    id_map_info_layout.setVisibility(View.GONE);
//
                    adapter = null;
                    adapter = new TableAdapter(SiteAnalysisReport.this);
//     String url = Const.API_URL + "/getTripHistorySummary?userId=" + Constant.SELECTED_USER_ID + "&vehicleId=" + Constant.SELECTED_VEHICLE_ID + "&fromDate=" + id_from_date.getText().toString() + "&toDate=" + id_to_date.getText().toString();
                    mTemperatureData.clear();
                    lv.setAdapter(null);

                    mStrFromDate = id_from_date.getText().toString().trim();
                    mStrToDate = id_to_date.getText().toString().trim();
                    mStrFromTime = id_from_time.getText().toString().trim();
                    mStrToTime = id_to_time.getText().toString().trim();
//                    String url = Const.API_URL + "mobile/getTripHistorySummary?userId=" + Constant.SELECTED_USER_ID + "&vehicleId=" + Constant.SELECTED_VEHICLE_ID + "&fromDate=" + id_from_date.getText().toString() + "&toDate=" + id_to_date.getText().toString();
                    // String url = Const.API_URL + "mobile/getVehicleHistory4MobileV2?userId=" + Constant.SELECTED_USER_ID + "&vehicleId=" + Constant.SELECTED_VEHICLE_ID + "&fromDate=" + id_from_date.getText().toString() + "&fromTime=" + id_from_time.getText().toString() + "&toDate=" + id_to_date.getText().toString() + "&toTime=" + id_to_time.getText().toString() + "&macid=" + getStdTableValue("macid") + "&appid=" + getStdTableValue("appid");
                    // String par[] = {Const.API_URL + "mobile/getGeoFenceView?vehicleId=" + Constant.SELECTED_VEHICLE_ID + "&userId=" + Constant.SELECTED_USER_ID + "&macid=" + getStdTableValue("macid") + "&appid=" + getStdTableValue("appid")};
                    new getKmsSummaryData().execute();
                    //  new GetPOIInformation().execute(par);
                } else {
                    Toast internet_toast = Toast.makeText(getApplicationContext(), "Please Check your Internet Connection", Toast.LENGTH_LONG);
                    internet_toast.show();
                }
            }
        });

        id_from_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar calNow = Calendar.getInstance();
                new DatePickerDialog(SiteAnalysisReport.this, new DatePickerDialog.OnDateSetListener() {
                    // when dialog box is closed, below method will be called.
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int day) {
                        id_from_date.setText(cons.getDateFormat(cons.getCalendarDate(year, month, day, 0, 0)));
                    }
                }, calNow.get(Calendar.YEAR), calNow.get(Calendar.MONTH), calNow.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

//        /** listener to handle when From date is clicked in history selection*/
//        id_from_date.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Toast.makeText(TripSummaryActivity.this, "Hi from date clicked ::::", Toast.LENGTH_SHORT).show();
//                from_date = true;
//                to_date = false;
//                //   from_time = false;
//                //   to_time = false;
//                showDialog(DATE_PICKER_ID);
//            }
//        });
        /** listener to handle when From Time is clicked in history selection*/
//        id_from_time.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                from_date = false;
//                to_date = false;
//                from_time = true;
//                to_time = false;
//                showDialog(TIME_DIALOG_ID);
//            }
//        });


        id_to_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar calNow = Calendar.getInstance();
                new DatePickerDialog(SiteAnalysisReport.this, new DatePickerDialog.OnDateSetListener() {
                    // when dialog box is closed, below method will be called.
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int day) {
                        id_to_date.setText(cons.getDateFormat(cons.getCalendarDate(year, month, day, 0, 0)));
                    }
                }, calNow.get(Calendar.YEAR), calNow.get(Calendar.MONTH), calNow.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        id_from_time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar calNow = Calendar.getInstance();
                new TimePickerDialog(SiteAnalysisReport.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hour, int minute) {
                        // TODO Auto-generated method stub
                        id_from_time.setText(new StringBuilder().append(pad(hour)).append(":").append(pad(minute)).append(":").append("00"));
                    }
                }, calNow.get(Calendar.HOUR_OF_DAY), calNow.get(Calendar.MINUTE), false).show();
            }
        });

        id_to_time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar calNow = Calendar.getInstance();
                new TimePickerDialog(SiteAnalysisReport.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hour, int minute) {
                        // TODO Auto-generated method stub
                        id_to_time.setText(new StringBuilder().append(pad(hour)).append(":").append(pad(minute)).append(":").append("00"));
                    }
                }, calNow.get(Calendar.HOUR_OF_DAY), calNow.get(Calendar.MINUTE), false).show();
            }
        });


        /** listener to handle when To date is clicked in history selection*/
//        id_to_date.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                from_date = false;
//                to_date = true;
//                // from_time = false;
//                // to_time = false;
//                showDialog(DATE_PICKER_ID);
//            }
//        });
        /** listener to handle when To Time is clicked in history selection*/
//        id_to_time.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                from_date = false;
//                to_date = false;
//                from_time = false;
//                to_time = true;
//                showDialog(TIME_DIALOG_ID);
//            }
//        });


        marker_info_dialog.show();
        return true;
    }

    private static String pad(int c) {
        if (c >= 10)
            return String.valueOf(c);
        else
            return "0" + String.valueOf(c);
    }


    private class getKmsSummaryData extends AsyncTask<String, String, String> {
        ProgressDialog progressDialog;

        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub

            String result = null;
            try {
                //  Const constaccess = new Const();

                //  System.out.println("Helllooooooooooooo testttttt");
                mSiteEntryValue = 0;
                mSiteExitValue = 0;
                String string_from_date = mStrFromDate.trim() + " " + mStrFromTime.trim();
                String string_to_date = mStrToDate.trim() + " " + mStrToTime.trim();

                //  System.out.println("The from date  :::::" + string_from_date);

                //  System.out.println("The to date  :::::" + string_to_date);

                SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                Date d = f.parse(string_from_date);

                // System.out.println("The from date after converted  :::::" + d);

                long fromMilliSeconds = d.getTime();
                // System.out.println("The from time in millis :::::" + fromMilliSeconds);
                Date d1 = f.parse(string_to_date);
                long toMilliSeconds = d1.getTime();
//                long fromMilliSeconds =1470767400000;
//                long toMilliSeconds=1471685033000;


                // System.out.println("The to time in millis :::::" + toMilliSeconds);


//                result = constaccess
//                        .sendGet(Const.API_URL + "/mobile/getExecutiveReport?groupId=" + mSelectedGroup + "&fromDate=" + mStrFromDate + "&toDate=" + mStrToDate + "&userId=" + mUserId, 30000);
                HttpConfig ht = new HttpConfig();
//                result = ht.httpGet(Const.API_URL + "/mobile/getFuelDropFillReport?vehicleId=" + Constant.SELECTED_VEHICLE_ID + "&interval=1&fromDate=" + mStrFromDate + "&fromTime=" + mStrFromTime + "&toDate=" + mStrToDate + "&toTime=" + mStrToTime + "&fuelDrop=true&fuelFill=true&fromDateUTC=" + fromMilliSeconds + "&toDateUTC=" + toMilliSeconds + "&userId=" + Constant.SELECTED_USER_ID);

//                http://gpsvts.net/mobile/getAlarmReport?vehicleId=SBE-TN20BE8239&fromDate=2016-08-17&fromTime=0:0:0&toDate=2016-08-27&toTime=11:58:58&fromDateUTC=1471372200000&toDateUTC=1472279338000&userId=SBLT

//                http:
////gpsvts.net/gps/public/getSiteReport?vehicleId=SBE-TN20BE8239&fromDate=2016-08-27&fromTime=0:0:0&toDate=2016-08-27&toTime=15:24:24&interval=10&site=true&fromDateUTC=1472236200000&toDateUTC=1472291664000

                result = ht.httpGet(Const.API_URL + "/mobile/getSiteReport?vehicleId=" + Constant.SELECTED_VEHICLE_ID + "&fromDate=" + mStrFromDate + "&fromTime=" + mStrFromTime + "&toDate=" + mStrToDate + "&toTime=" + mStrToTime + "&interval=" + mIntervalValue + "&site=true&fromDateUTC=" + fromMilliSeconds + "&toDateUTC=" + toMilliSeconds + "&userId=" + Constant.SELECTED_USER_ID);

            } catch (Exception e) {

            }

            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            progressDialog.dismiss();
            if (result != null && result.length() > 0) {
//                System.out.println("The Fuel fill result is ::::" + result);

                JSONArray jArrayFuel = null, jsonArray = null;
                try {
                    jArrayFuel = new JSONArray(result);

                    // for (int i = 0; i < jsonArray.length(); i++) {
//                    JSONObject jsonObject = jsonArray.getJSONObject(1);

//                    JSONObject jsonObject = new JSONObject(result.trim());

//                    if (jsonObject.has("alarmList")) {
//                        jArrayFuel = jsonObject.getJSONArray("alarmList");
                    for (int i = 0; i < jArrayFuel.length(); i++) {
                        JSONObject jsonFuelObject = jArrayFuel.getJSONObject(i);
                        SiteAnalysisDto f = new SiteAnalysisDto();

                        if (jsonFuelObject.has("startTime")) {
                            f.setStartTime(jsonFuelObject.getLong("startTime"));
                        }
                        if (jsonFuelObject.has("state")) {
                            f.setStateValue(jsonFuelObject.getString("state"));
                            if (jsonFuelObject.getString("state").trim().equalsIgnoreCase("Site Exit")) {
                                mSiteExitValue++;
                            } else if (jsonFuelObject.getString("state").trim().equalsIgnoreCase("Site Entry")) {
                                mSiteEntryValue++;

                            }

                        }


                        if (jsonFuelObject.has("address")) {
                            f.setAddress(jsonFuelObject.getString("address"));
                        }

                        if (jsonFuelObject.has("tduration")) {
                            f.setDurationTime(jsonFuelObject.getLong("tduration"));
                            Log.d("tduration",""+jsonFuelObject.getLong("tduration"));
                        }

                        mTemperatureData.add(f);

                    }


                    mTxtSiteEntryValue.setText(String.valueOf(mSiteEntryValue));
                    mTxtSiteExitValue.setText(String.valueOf(mSiteExitValue));
//                    }

//                    if (jsonObject.has("totalFuel")) {
//                        mTotalFuelValue = String.valueOf(jsonObject.getDouble("totalFuel"));
//                    }

                    // }


                } catch (JSONException e) {
                    e.printStackTrace();
                }


                setTableLayoutData();
            }

        }

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            progressDialog = new ProgressDialog(SiteAnalysisReport.this,
                    AlertDialog.THEME_HOLO_LIGHT);
            progressDialog.setMessage("Please Wait...");
            progressDialog.setProgressDrawable(new ColorDrawable(
                    Color.BLUE));
            progressDialog.setCancelable(true);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.show();
        }

    }


    public void setTableLayoutData() {

//        adapter.setHeader(mHeader);
//        ListView listView = (ListView) findViewById(R.id.executive_report_listView1);

        if (isHeaderPresent) {

        } else {
            lv.addHeaderView(adapter.getHeaderView(lv));
            isHeaderPresent = true;
        }


        lv.setAdapter(adapter);
        adapter.setData(mTemperatureData);

        adapter.notifyDataSetChanged();

//        mTxtCumulativeFuelValue.setText(mTotalFuelValue + " ltrs");

//        parseVehicleKms();

    }

    public class TableAdapter extends BaseAdapter {

        // private static final String TAG = "TableAdapter";

        private Context mContext;

        private String[] mHeader;


        private int mCurrentScroll;

        private int[] mColResources = {R.id.temperature_date_textView, R.id.temperature_value_textView,
                R.id.temperature_adds_textView, R.id.temperature_location_link_textView};

        public TableAdapter(Context context) {
            super();
            mContext = context;
        }

        @Override
        public int getCount() {
            return mTemperatureData != null ? mTemperatureData.size() : 0;
        }

        @Override
        public Object getItem(int position) {
            return mTemperatureData.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {

            HorizontalScrollView view = null;

            if (convertView == null) {
                LayoutInflater inflater = (LayoutInflater) mContext
                        .getSystemService(LAYOUT_INFLATER_SERVICE);
                view = (HorizontalScrollView) inflater.inflate(
                        R.layout.temperature_list_item_table_row, parent, false);

//                view.setOnScrollChangeListener(new OnScrollListener() {
//                    @Override
//                    public void onScrollChanged(View view, int scrollX) {
//
//                    }
//                });


                view.setOnScrollListener(new HorizontalScrollView.OnScrollListener() {

                    @Override
                    public void onScrollChanged(View scrollView, int scrollX) {

                        mCurrentScroll = scrollX;
                        ListView listView = (ListView) scrollView.getParent();
                        if (listView == null)
                            return;

                        for (int i = 0; i < listView.getChildCount(); i++) {
                            View child = listView.getChildAt(i);
                            if (child instanceof HorizontalScrollView
                                    && child != scrollView) {
                                HorizontalScrollView scrollView2 = (HorizontalScrollView) child;
                                if (scrollView2.getScrollX() != mCurrentScroll) {
                                    scrollView2.setScrollX(mCurrentScroll);
                                }
                            }
                        }
                    }
                });

            } else {
                view = (HorizontalScrollView) convertView;
            }

            view.setScrollX(mCurrentScroll);

            if (position % 2 == 0) {
                view.setBackgroundColor(Color.WHITE);
            } else {
                view.setBackgroundColor(Color.LTGRAY);
            }

            SiteAnalysisDto data = mTemperatureData.get(position);

//            for (int i = 0; i < mColResources.length; i++) {
            TextView col1 = (TextView) view.findViewById(mColResources[0]);

            col1.setText(cons.getReportTimefromserver(String.valueOf(data.getStartTime())));
            TextView col2 = (TextView) view.findViewById(mColResources[1]);
            col2.setText(String.valueOf(data.getStateValue()));
            TextView col3 = (TextView) view.findViewById(mColResources[2]);
            col3.setText(Const.gettimevalue(data.getDurationTime()));
            long value = data.getDurationTime()/1000;
            Log.d("durationsecs",""+value);
            //col3.setText(Const.getTimeValue(value));
            Log.d("durationtime",""+data.getDurationTime());
            Log.d("durationtimevalue",""+Const.gettimevalue(data.getDurationTime()));
            TextView col4 = (TextView) view.findViewById(mColResources[3]);
            col4.setText(String.valueOf(data.getAddress()));
            col4.setTextColor(Color.BLACK);
            // TextView col5 = (TextView) view.findViewById(mColResources[4]);
//            col5.setText("Location");

//            SpannableString content = new SpannableString("Location");
//            content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
//            col4.setText(content);


            LinearLayout.LayoutParams vehicleNameParams = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT);
            vehicleNameParams.width = width * 35 / 100;
            vehicleNameParams.height = height * 9 / 100;
            col1.setLayoutParams(vehicleNameParams);
            col1.setPadding(width * 2 / 100, 0, 0, 0);
            col1.setGravity(Gravity.CENTER | Gravity.LEFT);


            LinearLayout.LayoutParams text3Params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT);
            text3Params.width = width * 35 / 100;
            text3Params.height = height * 9 / 100;

            col3.setLayoutParams(text3Params);
            //   col3.setWidth(width * 75 / 100);
            col3.setPadding(width * 2 / 100, 0, 0, 0);
            col3.setGravity(Gravity.CENTER | Gravity.LEFT);


//            col3.setLayoutParams(vehicleNameParams);
//            col3.setWidth(width * 75 / 100);
//            col3.setPadding(width * 2 / 100, 0, 0, 0);
//            col3.setGravity(Gravity.CENTER | Gravity.LEFT);


            LinearLayout.LayoutParams text2Params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT);
            text2Params.width = width * 30 / 100;
            text2Params.height = height * 9 / 100;
            col2.setLayoutParams(text2Params);
            col2.setPadding(width * 2 / 100, 0, 0, 0);
            col2.setGravity(Gravity.CENTER | Gravity.LEFT);


            LinearLayout.LayoutParams text4Params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT);
            text4Params.width = width * 50 / 100;
            text4Params.height = height * 9 / 100;

            col4.setLayoutParams(text4Params);
            //   col3.setWidth(width * 75 / 100);
            col4.setPadding(width * 2 / 100, 0, 0, 0);
            col4.setGravity(Gravity.CENTER | Gravity.LEFT);

//            col4.setLayoutParams(text2Params);
//            col4.setPadding(width * 2 / 100, 0, 0, 0);
//            col4.setGravity(Gravity.CENTER | Gravity.LEFT);


//            col4.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    mImgDataTableMapViewChange.setVisibility(View.VISIBLE);
//
//                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//                        mImgDataTableMapViewChange.setImageDrawable(getResources().getDrawable(R.drawable.data_table, getApplicationContext().getTheme()));
//                    } else {
//                        mImgDataTableMapViewChange.setImageDrawable(getResources().getDrawable(R.drawable.data_table));
//                    }
//                    mIsBarChartEnabled = true;
//
//                    mIsMapPresent = true;
//                    mFuelDataMapLayout.setVisibility(View.VISIBLE);
//                    lv.setVisibility(View.GONE);
//                    mLatitude = mTemperatureData.get(position).getLatitude();
//                    mLongitude = mTemperatureData.get(position).getLongitude();
//                    setupMap();
//                }
//            });

//            }

            return view;
        }

        public View getHeaderView(ViewGroup parent) {

            LayoutInflater inflater = (LayoutInflater) mContext
                    .getSystemService(LAYOUT_INFLATER_SERVICE);
            HorizontalScrollView view = (HorizontalScrollView) inflater
                    .inflate(R.layout.site_analysis_list_item_table_header, parent, false);

//            for (int i = 0; i < mColResources.length; i++) {
//                TextView col = (TextView) view.findViewById(mColResources[i]);
//                col.setText(mHeader[i]);
//            }


            view.setOnScrollListener(new HorizontalScrollView.OnScrollListener() {

                @Override
                public void onScrollChanged(View scrollView, int scrollX) {

                    mCurrentScroll = scrollX;
                    ListView listView = (ListView) scrollView.getParent();
                    if (listView == null)
                        return;

                    for (int i = 0; i < listView.getChildCount(); i++) {
                        View child = listView.getChildAt(i);
                        if (child instanceof HorizontalScrollView
                                && child != scrollView) {
                            HorizontalScrollView scrollView2 = (HorizontalScrollView) child;
                            if (scrollView2.getScrollX() != mCurrentScroll) {
                                scrollView2.setScrollX(mCurrentScroll);
                            }
                        }
                    }
                }
            });


            TextView col1 = (TextView) view.findViewById(R.id.alarm_date_textView_header);
            TextView col2 = (TextView) view.findViewById(R.id.alarm_value_textView_header);
            TextView col3 = (TextView) view.findViewById(R.id.alarm_adds_textView_header);
            TextView col4 = (TextView) view.findViewById(R.id.alarm_link_textView_header);
            //  TextView col5 = (TextView) view.findViewById(R.id.link_textView_header);
//            col1.setText("Date & Time");
//            col2.setText("Alarm Type");
//            col3.setText("Nearest Location");
//            col4.setText("G-Map");


            LinearLayout.LayoutParams vehicleNameParams = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT);
            vehicleNameParams.width = width * 35 / 100;
            vehicleNameParams.height = height * 9 / 100;
            col1.setLayoutParams(vehicleNameParams);
            col1.setPadding(width * 2 / 100, 0, 0, 0);
            col1.setGravity(Gravity.CENTER | Gravity.LEFT);


            LinearLayout.LayoutParams text3Params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT);
            text3Params.width = width * 35 / 100;
            text3Params.height = height * 9 / 100;

            col3.setLayoutParams(text3Params);
            //   col3.setWidth(width * 75 / 100);
            col3.setPadding(width * 2 / 100, 0, 0, 0);
            col3.setGravity(Gravity.CENTER | Gravity.LEFT);


            LinearLayout.LayoutParams text2Params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT);
            text2Params.width = width * 30 / 100;
            text2Params.height = height * 9 / 100;
            col2.setLayoutParams(text2Params);
            col2.setPadding(width * 2 / 100, 0, 0, 0);
            col2.setGravity(Gravity.CENTER | Gravity.LEFT);
            // col2.setText("Temperature " + (char) 0x00B0 + "C");


            LinearLayout.LayoutParams text4Params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT);
            text4Params.width = width * 50 / 100;
            text4Params.height = height * 9 / 100;

            col4.setLayoutParams(text4Params);
            //   col3.setWidth(width * 75 / 100);
            col4.setPadding(width * 2 / 100, 0, 0, 0);
            col4.setGravity(Gravity.CENTER | Gravity.LEFT);

//            col4.setLayoutParams(text2Params);
//            col4.setPadding(width * 2 / 100, 0, 0, 0);
//            col4.setGravity(Gravity.CENTER | Gravity.LEFT);

//            col4.setLayoutParams(text2Params);
//            col4.setPadding(width * 2 / 100, 0, 0, 0);
//            col4.setGravity(Gravity.CENTER | Gravity.LEFT);


            return view;
        }

//        public void setHeader(String[] header) {
//            mHeader = header;
//        }

        public void setData(List<SiteAnalysisDto> data) {
            mTemperatureData = data;
            notifyDataSetChanged();
        }

    }

    public void setListData() {
        mImgDataViewChange.setVisibility(View.GONE);
        mIsMapPresent = false;
        lv.setVisibility(View.VISIBLE);
        mFuelDataMapLayout.setVisibility(View.GONE);
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();

//
//        if (mIsMapPresent) {
//            System.out.println("The map is present ::::::" + mIsMapPresent);
//            setListData();
//
//           // new getKmsSummaryData().execute();
//        } else {
        startActivity(new Intent(SiteAnalysisReport.this, MapMenuActivity.class));
        finish();
//        }
    }

    private void screenArrange() {
        // TODO Auto-generated method stub

		/* screen arrangements */
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        height = metrics.heightPixels;
        width = metrics.widthPixels;
        Constant.ScreenWidth = width;
        Constant.ScreenHeight = height;

        LinearLayout.LayoutParams backImageParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        backImageParams.width = width * 15 / 100;
        backImageParams.height = height * 7 / 100;
        backImageParams.gravity = Gravity.CENTER;
        mBackArrow.setLayoutParams(backImageParams);
        mBackArrow.setPadding(width * 1 / 100, height * 1 / 100, width * 1 / 100, height * 1 / 100);

        LinearLayout.LayoutParams headTxtParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        headTxtParams.width = width * 62 / 100;
        headTxtParams.height = height * 7 / 100;
        mHeadTitle.setLayoutParams(headTxtParams);
        mHeadTitle.setPadding(width * 2 / 100, 0, 0, 0);
        mHeadTitle.setGravity(Gravity.CENTER | Gravity.LEFT);

        LinearLayout.LayoutParams changeDataImageParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        changeDataImageParams.width = width * 9 / 100;
        changeDataImageParams.height = height * 7 / 100;
        changeDataImageParams.gravity = Gravity.CENTER;
        mImgDataViewChange.setLayoutParams(changeDataImageParams);
        mImgDataViewChange.setPadding(width * 1 / 100, height * 1 / 100,
                width * 1 / 100, height * 1 / 100);


        LinearLayout.LayoutParams changeDateImageParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        changeDateImageParams.width = width * 9 / 100;
        changeDateImageParams.height = height * 7 / 100;
        changeDateImageParams.gravity = Gravity.CENTER;
        mImgChangeDate.setLayoutParams(changeDateImageParams);
        mImgChangeDate.setPadding(width * 1 / 100, height * 1 / 100,
                width * 1 / 100, height * 1 / 100);

        LinearLayout.LayoutParams textParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        textParams.width = width * 45 / 100;
        textParams.height = height * 9 / 2 / 100;
        textParams.topMargin = (int) (height * 0.25 / 100);
        textParams.leftMargin = width * 2 / 100;
        textParams.rightMargin = width * 2 / 100;
        mTxtVehicleName.setLayoutParams(textParams);
        mTxtCumulativeFuel.setLayoutParams(textParams);
        mTxtVehicleName.setGravity(Gravity.CENTER);
        mTxtCumulativeFuel.setGravity(Gravity.CENTER);

        LinearLayout.LayoutParams textValueParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        textValueParams.width = width * 45 / 100;
        textValueParams.height = height * 9 / 2 / 100;
        textValueParams.leftMargin = width * 2 / 100;
        textValueParams.rightMargin = width * 2 / 100;
        textValueParams.bottomMargin = (int) (height * 0.25 / 100);
        mTxtVehicleNameValue.setLayoutParams(textValueParams);
        mTxtCumulativeFuelValue.setLayoutParams(textValueParams);
        mTxtVehicleNameValue.setGravity(Gravity.CENTER);
        mTxtCumulativeFuelValue.setGravity(Gravity.CENTER);


        LinearLayout.LayoutParams mSiteImageParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        mSiteImageParams.width = width * 15 / 100;
        mSiteImageParams.height = height * 5 / 100;
        mSiteImageParams.topMargin = (int) (height * 0.25 / 100);
        mSiteImageParams.leftMargin = width * 2 / 100;
        mSiteImageParams.rightMargin = width * 2 / 100;
        mSiteImageParams.gravity = Gravity.CENTER;
        mImgSiteEntry.setLayoutParams(mSiteImageParams);
        mImgSiteExit.setLayoutParams(mSiteImageParams);
        // mImgSiteEntry.setGravity(Gravity.CENTER);
        // mTxtCumulativeFuel.setGravity(Gravity.CENTER);

        LinearLayout.LayoutParams txtSiteValueParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        txtSiteValueParams.width = width * 45 / 100;
        txtSiteValueParams.height = height * 5 / 100;
        txtSiteValueParams.leftMargin = width * 2 / 100;
        txtSiteValueParams.rightMargin = width * 2 / 100;
        txtSiteValueParams.bottomMargin = (int) (height * 0.25 / 100);
        mTxtSiteEntry.setLayoutParams(txtSiteValueParams);
        mTxtSiteExit.setLayoutParams(txtSiteValueParams);
        mTxtSiteEntryValue.setLayoutParams(txtSiteValueParams);
        mTxtSiteExitValue.setLayoutParams(txtSiteValueParams);
        mTxtSiteEntry.setGravity(Gravity.CENTER | Gravity.LEFT);
        mTxtSiteExit.setGravity(Gravity.CENTER | Gravity.LEFT);
        mTxtSiteEntryValue.setGravity(Gravity.CENTER | Gravity.LEFT);
        mTxtSiteExitValue.setGravity(Gravity.CENTER | Gravity.LEFT);


//        LinearLayout.LayoutParams ViewParams = new LinearLayout.LayoutParams(
//                LinearLayout.LayoutParams.WRAP_CONTENT,
//                LinearLayout.LayoutParams.WRAP_CONTENT);
//        ViewParams.width = width * 1 / 100;
//        ViewParams.height = LinearLayout.LayoutParams.MATCH_PARENT;
//        v1.setLayoutParams(ViewParams);
//        v2.setLayoutParams(ViewParams);
//
        LinearLayout.LayoutParams viewParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        viewParams.width = width;
        viewParams.height = height * 1 / 2 / 100;
        v1.setLayoutParams(viewParams);
        v2.setLayoutParams(viewParams);

//        LinearLayout.LayoutParams spinnerParams = new LinearLayout.LayoutParams(
//                LinearLayout.LayoutParams.WRAP_CONTENT,
//                LinearLayout.LayoutParams.WRAP_CONTENT);
//        spinnerParams.width = width * 95 / 100;
//        spinnerParams.height = height * 6 / 100;
//        spinnerParams.topMargin = (int) (height * 1.25 / 100);
//        spinnerParams.leftMargin = width * 2 / 100;
//        spinnerParams.rightMargin = width * 2 / 100;
//        spinnerParams.bottomMargin = (int) (height * 0.25 / 100);
//        groupSpinner.setLayoutParams(spinnerParams);

        LinearLayout.LayoutParams lineParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        lineParams.width = width * 98 / 100;
        lineParams.height = LinearLayout.LayoutParams.WRAP_CONTENT;
        lineParams.topMargin = height * 2 / 100;
        lineParams.bottomMargin = height * 1 / 100;
        lineParams.leftMargin = width * 1 / 100;
        lineParams.rightMargin = width * 1 / 100;
        lv.setLayoutParams(lineParams);

//        LinearLayout.LayoutParams headTxtParams1 = new LinearLayout.LayoutParams(
//                LinearLayout.LayoutParams.WRAP_CONTENT,
//                LinearLayout.LayoutParams.WRAP_CONTENT);
//        headTxtParams1.width = width * 76 / 100;
//        headTxtParams1.height = height * 8 / 100;
//        headTxtParams1.setMargins(width * 4 / 100, width * 2 / 100, 1, width * 1 / 100);
////        headTxtParams1.setMargins();
////        $TxtTitle.setLayoutParams(headTxtParams);
////        $TxtTitle.setPadding(width * 2 / 100, 0, 0, 0);
////        $TxtTitle.setGravity(Gravity.CENTER);
//        mEdtSearch.setLayoutParams(headTxtParams1);
////        mEdtSearch.setm
//        mEdtSearch.setPadding(width * 2 / 100, 0, width * 4 / 100, 0);
//        mEdtSearch.setGravity(Gravity.CENTER | Gravity.CENTER);

        FrameLayout.LayoutParams imgeChangeView = new FrameLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        imgeChangeView.width = width * 10 / 100;
        imgeChangeView.height = height * 15 / 100;
        imgeChangeView.gravity = Gravity.TOP | Gravity.RIGHT;
        imgeChangeView.rightMargin = (int) (width * 0.5 / 100);
        $ChangeView.setLayoutParams(imgeChangeView);


        if (width >= 600) {
            mTxtVehicleNameValue.setTextSize(16);
            mTxtVehicleName.setTextSize(16);
            mTxtCumulativeFuelValue.setTextSize(16);
            mTxtCumulativeFuel.setTextSize(16);
            mTxtNoRecord.setTextSize(18);
            mHeadTitle.setTextSize(18);

            mTxtSiteEntry.setTextSize(16);
            mTxtSiteEntryValue.setTextSize(16);
            mTxtSiteExit.setTextSize(16);
            mTxtSiteExitValue.setTextSize(16);

            //mEdtSearch.setTextSize(18);
        } else if (width > 501 && width < 600) {
            mTxtVehicleNameValue.setTextSize(15);
            mTxtVehicleName.setTextSize(15);
            mTxtCumulativeFuelValue.setTextSize(15);
            mTxtCumulativeFuel.setTextSize(15);
            mTxtNoRecord.setTextSize(17);
            mHeadTitle.setTextSize(17);

            mTxtSiteEntry.setTextSize(15);
            mTxtSiteEntryValue.setTextSize(15);
            mTxtSiteExit.setTextSize(15);
            mTxtSiteExitValue.setTextSize(15);
            //  mEdtSearch.setTextSize(17);
        } else if (width > 260 && width < 500) {
            mTxtVehicleNameValue.setTextSize(14);
            mTxtVehicleName.setTextSize(14);
            mTxtCumulativeFuelValue.setTextSize(14);
            mTxtCumulativeFuel.setTextSize(14);
            mTxtNoRecord.setTextSize(16);
            mHeadTitle.setTextSize(16);

            mTxtSiteEntry.setTextSize(14);
            mTxtSiteEntryValue.setTextSize(14);
            mTxtSiteExit.setTextSize(14);
            mTxtSiteExitValue.setTextSize(14);
            //  mEdtSearch.setTextSize(16);
        } else if (width <= 260) {
            mTxtVehicleNameValue.setTextSize(13);
            mTxtVehicleName.setTextSize(13);
            mTxtCumulativeFuelValue.setTextSize(13);
            mTxtCumulativeFuel.setTextSize(13);
            mTxtNoRecord.setTextSize(15);
            mHeadTitle.setTextSize(15);

            mTxtSiteEntry.setTextSize(13);
            mTxtSiteEntryValue.setTextSize(13);
            mTxtSiteExit.setTextSize(13);
            mTxtSiteExitValue.setTextSize(13);
            //  mEdtSearch.setTextSize(15);
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        map.setPadding(1, 1, 1, 150);
        map.getUiSettings().setZoomControlsEnabled(true);
    }
}
