package com.vamosys.vamos;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.SQLException;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.github.mikephil.charting.charts.BarChart;
import com.vamosys.adapter.ExecutiveHeaderAdapter;
import com.vamosys.interfaces.ApiClient;
import com.vamosys.interfaces.ApiInterface;
import com.vamosys.model.ExecutiveDataPojo;
import com.vamosys.model.ExecutiveReportData;
import com.vamosys.model.FuelSumReportDto;
import com.vamosys.utils.ConnectionDetector;
import com.vamosys.utils.Constant;
import com.vamosys.utils.DaoHandler;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.bluecam.demo.MyApplication.getContext;

public class ExecutiveReportNew extends AppCompatActivity {
    RecyclerView lv;
    AppCompatTextView mHeadTitle;
    Spinner groupSpinner;
    View v1, v2, v3;
    AppCompatTextView mTxtNoRecord, fromdate, todate, fromdatevalue, todatevalue;
    AppCompatImageView mBackArrow, mImgChangeDate, mImgDataViewChange;
    private int year, month, day;
    boolean from_date, to_date;
    static Const cons;

    List<String> mGroupList = new ArrayList<String>();
    List<String> mGroupSpinnerList = new ArrayList<String>();
    String mSelectedGroup, mUserId;
    String mStrFromDate, mStrToDate;

    ConnectionDetector cd;
    SharedPreferences sp;
    ExecutiveHeaderAdapter eadapter;

    private static ArrayList<ExecutiveDataPojo> mData = new ArrayList<ExecutiveDataPojo>();

    Dialog marker_info_dialog;
    TextView id_from_date, id_to_date;

    private static final int SERIES_NR = 2;
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_executive_report_new);
        cons = new Const();
        init();
    }

    private void init() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        mBackArrow=findViewById(R.id.navigation_icon);
        mImgChangeDate=findViewById(R.id.imgDynamic);
        lv=findViewById(R.id.executive_report_listView1);
        lv.setHasFixedSize(true);
        lv.setLayoutManager(new LinearLayoutManager(getContext()));
        mBackArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(ExecutiveReportNew.this, VehicleListActivity.class));
                finish();
            }
        });
        mImgChangeDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showVehicleInfoDialog();
            }
        });

        fromdatevalue=findViewById(R.id.from_date_value);
        todatevalue=findViewById(R.id.to_date_value);
        mTxtNoRecord=findViewById(R.id.executive_report_no_record_txt);
       groupSpinner=findViewById(R.id.executive_report_groupspinner);

        sp = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        cd = new ConnectionDetector(getApplicationContext());

        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -7);
        Date result = cal.getTime();
        SimpleDateFormat dfDate = new SimpleDateFormat("yyyy-MM-dd");
        String mCurrentDate = dfDate.format(new Date());
        final String mFromDate = dfDate.format(result);
        mStrFromDate = mFromDate;
        mStrToDate = mCurrentDate;
        fromdatevalue.setText(mStrFromDate);
        todatevalue.setText(mStrToDate);

        try {
            queryUserDB();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        groupSpinner
                .setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                    @Override
                    public void onItemSelected(AdapterView<?> parent,
                                               View view, int pos, long id) {
                        // TODO Auto-generated method stub
                        mSelectedGroup = mGroupList.get(pos);
                        saveSelectedGroup(mGroupSpinnerList.get(pos));
                        if (mSelectedGroup.equalsIgnoreCase("Select")) {

                        } else {
//                            mEdtSearch.setText("");
                            ConnectionDetector cd = new ConnectionDetector(getApplicationContext());
                            if (cd.isConnectingToInternet()) {

                                callExecutive();
                            } else {
                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.network_connection),
                                        Toast.LENGTH_SHORT).show();
                            }
                        }

                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> arg0) {
                        // TODO Auto-generated method stub

                    }

                });


    }
    private void saveSelectedGroup(String mGroupName) {
        try {
            SharedPreferences.Editor editor = sp.edit();
            editor.putString("selected_group", mGroupName);
            editor.commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void queryUserDB() {
        DaoHandler da = new DaoHandler(getApplicationContext(), false);
        da.queryUserDB();

        mUserId = Constant.mDbUserId;
        mGroupList = Constant.mUserGroupList;
        setDropDownData();
    }
    public void setDropDownData() {
        if (mGroupList.size() > 0) {
        } else {
            mGroupList.add("Select");
        }
        for (int i = 0; i < mGroupList.size(); i++) {

            String[] str_msg_data_array = mGroupList.get(i).split(":");

            mGroupSpinnerList.add(str_msg_data_array[0]);

        }
        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(
                ExecutiveReportNew.this, R.layout.spinner_row,
                mGroupSpinnerList) {

            public View getView(int position, View convertView, ViewGroup parent) {
                View v = super.getView(position, convertView, parent);
                return v;
            }

            public View getDropDownView(int position, View convertView,
                                        ViewGroup parent) {
                View v = super.getDropDownView(position, convertView, parent);
                return v;
            }
        };
        spinnerArrayAdapter
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        groupSpinner.setAdapter(spinnerArrayAdapter);
        String groupName = sp.getString("selected_group", "");
        int spinnerPosition = spinnerArrayAdapter.getPosition(groupName);
        groupSpinner.setSelection(spinnerPosition);
    }

    private boolean showVehicleInfoDialog() {

        TextView
                id__popupto_date_label,

                id_txt_divider1, id_txt_divider2, id_histroy_vehicleid;
        Button id_history_selection_done, id_history_selection_cancel;
        LinearLayout date_time_lay;
        marker_info_dialog = new Dialog(ExecutiveReportNew.this);
        marker_info_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        marker_info_dialog.setContentView(R.layout.layout_executive_date);
        marker_info_dialog.setCancelable(false);
        id_histroy_vehicleid = (TextView) marker_info_dialog.findViewById(R.id.id_popup_trip_summ_vehicleid);


        id_from_date = (TextView) marker_info_dialog.findViewById(R.id.id_popup_trip_summ_from_date);
        id_to_date = (TextView) marker_info_dialog.findViewById(R.id.id_popup_trip_summ_to_date);


        id_history_selection_done = (Button) marker_info_dialog.findViewById(R.id.id_popup_trip_summ_selection_done);
        id_history_selection_cancel = (Button) marker_info_dialog.findViewById(R.id.id_popup_trip_summ_selection_cancel);



        id_from_date.setText(mStrFromDate);
        id_to_date.setText(mStrToDate);



//        id_histroy_vehicleid.setText(Constant.SELECTED_VEHICLE_SHORT_NAME);
        id_histroy_vehicleid.setText(""+getResources().getString(R.string.executive_report));


        /** listener to handle when history is canceled */
        id_history_selection_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                opptionPopUp();
                marker_info_dialog.hide();
            }
        });

        /** listener to handle when history is selected */
        id_history_selection_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (cd.isConnectingToInternet()) {
                    marker_info_dialog.hide();
                    eadapter = null;
                    mData.clear();
                    lv.setAdapter(null);


                    mStrFromDate = id_from_date.getText().toString().trim();
                    mStrToDate = id_to_date.getText().toString().trim();
                    callExecutive();
//                    new ExecutiveReportActivity.getKmsSummaryData().execute();
                } else {
                    Toast internet_toast = Toast.makeText(getApplicationContext(), getResources().getString(R.string.network_connection), Toast.LENGTH_LONG);
                    internet_toast.show();
                }
            }
        });

        id_from_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar calNow = Calendar.getInstance();

                if (mStrFromDate != null) {
                    String[] dateArr = mStrFromDate.split("-");
                    int yearValue = 0, monthValue = 0, dateValue = 0;
                    if (dateArr.length > 0) {


                        yearValue = Integer.valueOf(dateArr[0]);
                        monthValue = Integer.valueOf(dateArr[1]);
                        dateValue = Integer.valueOf(dateArr[2]);

                        if (monthValue > 0) {
                            monthValue = monthValue - 1;
                        }

                    }
                    calNow.set(yearValue, monthValue, dateValue);
                }

                new DatePickerDialog(ExecutiveReportNew.this, new DatePickerDialog.OnDateSetListener() {
                    // when dialog box is closed, below method will be called.
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int day) {
                        id_from_date.setText(cons.getDateFormat(cons.getCalendarDate(year, month, day, 0, 0)));
                    }
                }, calNow.get(Calendar.YEAR), calNow.get(Calendar.MONTH), calNow.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        id_to_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar calNow = Calendar.getInstance();

                if (mStrToDate != null) {
                    String[] dateArr = mStrToDate.split("-");
                    int yearValue = 0, monthValue = 0, dateValue = 0;
                    if (dateArr.length > 0) {


                        yearValue = Integer.valueOf(dateArr[0]);
                        monthValue = Integer.valueOf(dateArr[1]);
                        dateValue = Integer.valueOf(dateArr[2]);

                        if (monthValue > 0) {
                            monthValue = monthValue - 1;
                        }

                    }
                    calNow.set(yearValue, monthValue, dateValue);
                }

              DatePickerDialog datePickerDialog =  new DatePickerDialog(ExecutiveReportNew.this, new DatePickerDialog.OnDateSetListener() {
                    // when dialog box is closed, below method will be called.
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int day) {
                        id_to_date.setText(cons.getDateFormat(cons.getCalendarDate(year, month, day, 0, 0)));
                    }
                }, calNow.get(Calendar.YEAR), calNow.get(Calendar.MONTH), calNow.get(Calendar.DAY_OF_MONTH));
                datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
                datePickerDialog.show();
            }
        });

        marker_info_dialog.show();
        return true;
    }

    private void callExecutive(){
        ProgressDialog progressDialog;
        progressDialog = new ProgressDialog(ExecutiveReportNew.this,
                AlertDialog.THEME_HOLO_LIGHT);
        progressDialog.setMessage(getResources().getString(R.string.progress_dialog));
        progressDialog.setProgressDrawable(new ColorDrawable(
                Color.BLUE));
        progressDialog.setCancelable(true);
        progressDialog.setCanceledOnTouchOutside(false);
        try {
            progressDialog.show();
            mData.clear();
            fromdatevalue.setText(mStrFromDate);
            todatevalue.setText(mStrToDate);
            ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<ArrayList<ExecutiveDataPojo>> call = apiInterface.getPerformanceReport(mSelectedGroup,mStrFromDate,mStrToDate,mUserId);
            Log.d("res", call.request().url().toString());
            call.enqueue(new Callback<ArrayList<ExecutiveDataPojo>>() {
                @Override
                public void onResponse(Call<ArrayList<ExecutiveDataPojo>> call, Response<ArrayList<ExecutiveDataPojo>> response) {
                    Log.d("res", "sucess " +response.body());
                    if(response.isSuccessful()){
                        Log.d("res", "sucess1 "+mData.size());
                        mData=response.body();

                        if(mData!=null&&mData.size()>0){
                            List<Boolean> expand=new ArrayList<>();
                            expand.add(true);
                            for (int i=1;i<mData.size()+1;i++){
                                Log.d("res", "kok "+i);
                                expand.add(false);
                                Log.d("res", "kok "+expand.get(i));
                            }
                            Log.d("res", "call adapter "+mData.size());
                            eadapter=new ExecutiveHeaderAdapter(mData,getContext(),expand);
                            lv.setAdapter(eadapter);
                            eadapter.notifyDataSetChanged();
                        }
                    }
                    progressDialog.dismiss();
                }

                @Override
                public void onFailure(Call<ArrayList<ExecutiveDataPojo>> call, Throwable t) {
                    Log.d("res", "fail "+t.getMessage());
                    progressDialog.dismiss();
                    lv.setVisibility(View.GONE);
                }
            });
        }
        catch (Exception e){
            Log.d("res", "error "+e.getMessage() );
            progressDialog.dismiss();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(ExecutiveReportNew.this, VehicleListActivity.class));
        finish();
    }
}