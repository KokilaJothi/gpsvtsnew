package com.vamosys.vamos;


import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.preference.PreferenceManager;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.vamosys.model.VehicleData;
import com.vamosys.utils.ConnectionDetector;
import com.vamosys.utils.Constant;
import com.vamosys.utils.HttpConfig;
import com.vamosys.utils.MyCustomProgressDialog;
import com.vamosys.utils.TypefaceUtil;

import org.achartengine.ChartFactory;
import org.achartengine.GraphicalView;
import org.achartengine.chart.PointStyle;
import org.achartengine.model.SeriesSelection;
import org.achartengine.model.XYMultipleSeriesDataset;
import org.achartengine.model.XYSeries;
import org.achartengine.renderer.BasicStroke;
import org.achartengine.renderer.XYMultipleSeriesRenderer;
import org.achartengine.renderer.XYSeriesRenderer;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonToken;
import org.codehaus.jackson.map.MappingJsonFactory;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

public class HistoryGoogleMapActivity extends FragmentActivity implements OnMapReadyCallback {


    private static int ANIMATE_SPEEED = 3000;
    private static int ANIMATE_SPEEED_TURN = 3000;
    private static int BEARING_OFFSET = 20;

    int width, height;
    static final int DATE_PICKER_ID = 1111;
    static final int TIME_DIALOG_ID = 999;

    TextView id_show_history_vehicleid, history_distance_covered_value, history_start_location_value, history_end_location_value,
            histroy_start_time_value, history_endtime_value, history_timetaken_value, id_history_speed_info_value, id_history_distance_value, id_history_direction_value,
            history_parked_count_value, history_parked_count_label, history_over_speed_count_label, history_over_speed_count_value;

    LinearLayout history_detail_layout, id_history_show_min_layout, history_vehicle_info_layout, is_history_vehicle_layout;
    Button pause_icon, replay_icon, play_icon, mBtnClose;
    SeekBar history_seekbar, speed_seekbar;
    ImageView $ChangeView;
    LinearLayout mGoogleMapLayout;
    // GoogleMap map;
    Dialog marker_info_dialog;
    ProgressDialog progressDialog;
    String historyRepeat;

    private GoogleMap map;
    Handler handler1;
    Runnable runnable1;
    float history_zoom_level = 13.5F;

    List<LatLng> geoPoints = new ArrayList<>();
    List<String> mPosList = new ArrayList<String>();
    List<String> mAddsList = new ArrayList<String>();
    ArrayList<String> speedlist;
    ArrayList<String> vehicle = new ArrayList<String>();
    ArrayList<String> datetimelist;
    ArrayList<String> mIdleTimeList;
    ArrayList<String> distancelist;

    ArrayList<String> mHistoryForData = new ArrayList<>();

    //    final ArrayList<Marker> current_users_location_point = new ArrayList<Marker>();
//    final ArrayList<Marker> current_vehicle_location_marker = new ArrayList<Marker>();
//    Handler handler = new Handler();
    private long startTime = 3000;
    int a = 0;
    private Marker selectedMarker;
    String mSELECTED_MAP_TYPE = "Normal";

    private List<Marker> markers = new ArrayList<Marker>();

    private final Handler mHandler = new Handler();

    boolean isPauseCalled = false, isStopCalled = false;
    int currentIndex = 0;
    Button btnPlayPause;
    Dialog current_address__dialog;
    DBHelper dbhelper;
    SharedPreferences sp;
    ConnectionDetector cd;
    Const cons;
    private int year, month, day, hour, minute;
    boolean from_date, to_date, from_time, to_time, seekbarchanged = false, backButtonPressed = false;
    int progressChanged = 0;
    Boolean pause = false;
    int stepSize = 10;
    //For popup window
    String mHistoryResult, mSpeedLimit;

    String mUserId, mVehicleId;
    VehicleData mSelectedJsonObj = new VehicleData();

    TextView id_histroy_vehicleid, id_history_type_value, id_from_label, id_from_date, id_from_time, id_to_date, id_to_time;

    private HashMap<Marker, MarkerOptions> mMarkersHashMap;
    private HashMap<Marker, Integer> mMarkersPositionHashMap;

    /**
     * Listener for date picker
     */
    private DatePickerDialog.OnDateSetListener datepickerListener = new DatePickerDialog.OnDateSetListener() {
        // when dialog box is closed, below method will be called.
        @Override
        public void onDateSet(DatePicker view, int selectedYear, int selectedMonth, int selectedDay) {
            year = selectedYear;
            month = selectedMonth;
            day = selectedDay;
            if (from_date) {
                id_from_date.setText(cons.getDateFormat(cons.getCalendarDate(year, month, day, 0, 0)));
            } else if (to_date) {
                id_to_date.setText(cons.getDateFormat(cons.getCalendarDate(year, month, day, 0, 0)));
            }
        }
    };
    /**
     * Listener for Time Picker
     */
    private TimePickerDialog.OnTimeSetListener timePickerListener = new TimePickerDialog.OnTimeSetListener() {
        @Override
        public void onTimeSet(TimePicker view, int selected_hour, int selected_minute) {
            // TODO Auto-generated method stub
            hour = selected_hour;
            minute = selected_minute;
            if (from_time) {
                id_from_time.setText(new StringBuilder().append(pad(hour)).append(":").append(pad(minute)).append(":").append("00"));
            } else if (to_time) {
                id_to_time.setText(new StringBuilder().append(pad(hour)).append(":").append(pad(minute)).append(":").append("00"));
            }
        }
    };
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;

    View mCustomViewMarker = null;




    /*
    For History customised popup design
     */

    LinearLayout mLinearHeaderLayout, mLinearLayout1, mLinearLayout2, mLinearAddsLayout, mLinearPMINLayout, mLinearChartContainerLayout, mLinearBottomLayout;
    LinearLayout mSubLinearLayout1, mSubLinearLayout2, mSubLinearLayout3, mSubLinearLayout4, mSubLinearLayout5, mSubLinearLayout6;
    ImageView mPlayBackImage, mNavigationImage, id_show_vehicle_info_icon;

    View mTimeVerView, mAddsVerView, mHoriView1, mHoriView2;
    TextView mTxtHeader, mSpeedLimitTxtView10, mOdoDistTxtView11, mSpeedLimitTxtValue10, mOdoDistTxtValue11, mStartTimeTxtView, mEndTimeTxtView,
            mStartAddsTxtView, mEndAddsTxtView, mParkTimeTxtView, mMovingTimeTxtView, mIdleTimeTxtView, mNoDataTimeTxtView, mTxtDistanceTravelledView, mTxtDistanceTravelledValue,
            mParkedTimeValueTxtView, mMovingTimeValueTxtView, mIdleTimeValueTxtView, mNoDataTimeValueTxtView, mPlayBackTxtView, mNaviTxtView;
    String mParkedTime, mMovingTime, mIdleTime, mNoDataTime, mOdoDistance, mDistanceTravelled;
    double mPopupStartLat = 0.0, mPopupEndLat = 0.0, mPopupStartLng = 0.0, mPopupEndLng = 0.0;

//    private String[] mMonth = new String[]{"Jan", "Feb", "Mar", "Apr", "May",
//            "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};

    boolean m3Dmap = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.vehicle_history_layout);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
//        map = ((MapFragment) getFragmentManager().findFragmentById(R.id.mapView_layout_map_test)).getMap();
//        map.setPadding(1, 1, 1, 150);
//        map.getUiSettings().setZoomControlsEnabled(true);
//        btnPlayPause = (Button) findViewById(R.id.btn_pause_play);
        handler1 = new Handler();

        dbSetup();
        init();

        dbhelper.deletecurrent_vehicleinfo();
        sp = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        cd = new ConnectionDetector(getApplicationContext());
        cons = new Const();
        mCustomViewMarker = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.custommarker, null);

        if (sp.getString("ip_adds", "") != null) {
            if (sp.getString("ip_adds", "").trim().length() > 0) {
                Const.API_URL = sp.getString("ip_adds", "");
            }
        }

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            Constant.SELECTED_TRIP_FROM_DATE = extras.getString("from_date");
            Constant.SELECTED_TRIP_TO_DATE = extras.getString("to_date");
            mUserId = Constant.SELECTED_USER_ID;
            mVehicleId = Constant.SELECTED_VEHICLE_ID;
            mSelectedJsonObj = Constant.SELECTED_VEHICLE_LOCATION_OBJECT;
            if (cd.isConnectingToInternet()) {

                // System.out.println("The from date is ::::"+Constant.SELECTED_TRIP_FROM_DATE);
                // System.out.println("The to date is ::::"+Constant.SELECTED_TRIP_TO_DATE);

                String string_from_date = Const.getTripDate(Constant.SELECTED_TRIP_FROM_DATE) + " " + Const.getTripTime(Constant.SELECTED_TRIP_FROM_DATE);
                String string_to_date = Const.getTripDate(Constant.SELECTED_TRIP_TO_DATE) + " " + Const.getTripTime(Constant.SELECTED_TRIP_TO_DATE);

                SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                Date d = null, d1 = null;
                try {
                    d = f.parse(string_from_date);

                    // System.out.println("The from time in millis :::::" + fromMilliSeconds);
                    d1 = f.parse(string_to_date);

                } catch (ParseException e) {
                    e.printStackTrace();
                }
                long fromMilliSeconds = d.getTime();
                long toMilliSeconds = d1.getTime();


                String url = Const.API_URL + "mobile/getVehicleHistory4MobileV2?userId=" + Constant.SELECTED_USER_ID + "&vehicleId=" + Constant.SELECTED_VEHICLE_ID + "&fromDate=" + Const.getTripDate(Constant.SELECTED_TRIP_FROM_DATE) + "&fromTime=" + Const.getTripTime(Constant.SELECTED_TRIP_FROM_DATE) + "&toDate=" + Const.getTripDate(Constant.SELECTED_TRIP_TO_DATE) + "&toTime=" + Const.getTripTime(Constant.SELECTED_TRIP_TO_DATE) + "&fromDateUTC=" + fromMilliSeconds + "&toDateUTC=" + toMilliSeconds + "&macid=" + getStdTableValue("macid") + "&appid=" + getStdTableValue("appid");
                String par[] = {Const.API_URL + "mobile/getGeoFenceView?vehicleId=" + Constant.SELECTED_VEHICLE_ID + "&userId=" + Constant.SELECTED_USER_ID + "&macid=" + getStdTableValue("macid") + "&appid=" + getStdTableValue("appid")};
                new GetVehicleHistory().execute(url);
                new GetPOIInformation().execute(par);
            } else {
                Toast internet_toast = Toast.makeText(getApplicationContext(), "Please Check your Internet Connection", Toast.LENGTH_LONG);
                internet_toast.show();
            }


        } else {
            opptionPopUp();
        }


        dbhelper.deletecurrent_vehicleinfo();
        sp = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        cd = new ConnectionDetector(getApplicationContext());
        cons = new Const();

        m3Dmap = sp.getBoolean("animate_map_enabled", true);

        // System.out.println("The 3d map is :::::" + m3Dmap);


//        opptionPopUp();

//        LatLng l1 = new LatLng(13.708904, 80.010631);
//        LatLng l2 = new LatLng(13.727606, 79.998009);
//        LatLng l3 = new LatLng(13.728001, 79.997636);
//        LatLng l4 = new LatLng(13.723644, 79.999004);
//
//        geoPoints.add(l1);
//        geoPoints.add(l2);
//        geoPoints.add(l3);
//        geoPoints.add(l4);
        //  shoeMarker();
//        for (int a = 0; a < geoPoints.size(); a++) {
//            //   loopvalue = a;
//            // System.out.println("Hi  clled ::::" + a);
//            // shoeMarker(geoPoints.get(a));
//
//            //  show_marker(geoPoints.get(a));
//            addMarkerToMap(geoPoints.get(a));
//            //value++;
//        }
        // handler.postDelayed(runable, 3000);
        $ChangeView.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        changeMapView();
                    }
                });
//        btnPlayPause.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (isPauseCalled) {
//                    isPauseCalled = false;
//                    btnPlayPause.setText("Play");
//                    animator.stopAnimation();
//                } else {
//                    isPauseCalled = true;
//                    btnPlayPause.setText("Pause");
//                    if (currentIndex != geoPoints.size()) {
//                        animator.startAnimation(true);
//                    } else {
//                        currentIndex = 0;
//                    }
//                }
//            }
//        });

//        animator.startAnimation(true);


        history_seekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                progressChanged = progress;

                // progressChanged = ((int) Math.round(progress / stepSize)) * stepSize;
                // history_seekbar.setProgress(progress);
                // Toast.makeText(getApplicationContext(),progressChanged,Toast.LENGTH_SHORT).show();
                if (pause) {
                    play_icon.setVisibility(View.VISIBLE);
                    pause_icon.setVisibility(View.GONE);
                } else {
                    play_icon.setVisibility(View.GONE);
                    pause_icon.setVisibility(View.VISIBLE);
                }
                replay_icon.setVisibility(View.GONE);
            }

            public void onStartTrackingTouch(SeekBar seekBar) {
                // TODO Auto-generated method stub
                if (!pause) {
                    seekbarchanged = true;
                }
                //  handler1.removeCallbacks(runnable1);
                animator.stopAnimation();
                // handler.removeCallbacksAndMessages(null);
            }

            public void onStopTrackingTouch(SeekBar seekBar) {
                //  Toast.makeText(getApplicationContext(),"Stopeed in :::"+progressChanged,Toast.LENGTH_SHORT).show();
                if (!pause) {
                    if (currentIndex == 0 || (currentIndex == geoPoints.size())) {
                        if (map != null) {
                            map.clear();
                        }
                    }
                    setAnimateTime();

                    Handler seekbarhandler = new Handler();
                    seekbarhandler.postDelayed(new Runnable() {
                        @Override
                        public void run() {


//                            System.out.println("The current map zoom level is :::::" + map.getCameraPosition().zoom);

                            if (map.getCameraPosition().zoom <= 3.5) {
                                history_zoom_level = history_zoom_level;
                            } else {
                                history_zoom_level = (map.getCameraPosition().zoom);
                            }


                            animator.startAnimation(true);
                            //marker_loop(progressChanged);
                        }
                    }, 500);
                }
//                if (progressChanged == history_seekbar.getMax()) {
//                    history_seekbar.setVisibility(View.GONE);
//                    replay_icon.setVisibility(View.VISIBLE);
//                    id_history_show_min_layout.setVisibility(View.VISIBLE);
//                    history_detail_layout.setVisibility(View.VISIBLE);
//                    pause_icon.setVisibility(View.GONE);
//                    play_icon.setVisibility(View.GONE);
//                }
            }
        });


        replay_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                map.clear();
                id_history_show_min_layout.setVisibility(View.GONE);
                speed_seekbar.setVisibility(View.GONE);
                history_seekbar.setVisibility(View.VISIBLE);
                //  a = 0;
                pause = false;
                // history_seekbar.setProgress(5);
                //  setAnimateTime();
                currentIndex = 0;
                history_zoom_level = (map.getCameraPosition().zoom);
                animator.startAnimation(true);
//                marker_loop(0);
//                if (points.size() > 1) {
                replay_icon.setVisibility(View.GONE);
                pause_icon.setVisibility(View.VISIBLE);
                play_icon.setVisibility(View.GONE);
//                    speed_seekbar.setVisibility(View.GONE);
//                    history_seekbar.setVisibility(View.VISIBLE);
//                } else {
//                    replay_icon.setVisibility(View.GONE);
//                    play_icon.setVisibility(View.GONE);
//                    pause_icon.setVisibility(View.GONE);
//                    speed_seekbar.setVisibility(View.GONE);
//                    history_seekbar.setVisibility(View.GONE);
//                }
            }
        });
        play_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pause = false;
                // marker_loop(progressChanged);
                animator.startAnimation(true);
                pause_icon.setVisibility(View.VISIBLE);
                play_icon.setVisibility(View.GONE);
                // paused_value = 0;
            }
        });
        pause_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pause = true;
                //handler1.removeCallbacks(runnable1);
                animator.stopAnimation();
                showCurrentAddress();
                // handler.removeCallbacksAndMessages(null);
//                if (progressChanged == history_seekbar.getMax()) {
//                    history_seekbar.setVisibility(View.GONE);
//                    replay_icon.setVisibility(View.VISIBLE);
//                    id_history_show_min_layout.setVisibility(View.VISIBLE);
//                    history_detail_layout.setVisibility(View.VISIBLE);
//                    pause_icon.setVisibility(View.GONE);
//                    play_icon.setVisibility(View.GONE);
//                } else {
                // System.out.println("The current index while clicking play icon is ::::" + currentIndex);
                if (currentIndex == geoPoints.size()) {
                    currentIndex = 0;
                }

                pause_icon.setVisibility(View.GONE);
                play_icon.setVisibility(View.VISIBLE);
//                }
            }
        });


        is_history_vehicle_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (id_history_show_min_layout.getVisibility() == View.VISIBLE) {
                    id_history_show_min_layout.setVisibility(View.GONE);
                } else {
                    id_history_show_min_layout.setVisibility(View.VISIBLE);
                }
            }
        });

        mBtnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                animator.stopAnimation();
                startActivity(new Intent(HistoryGoogleMapActivity.this, VehicleListActivity.class));
                finish();
            }
        });
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }

    public void init() {
//        map = ((MapFragment) getFragmentManager().findFragmentById(R.id.vehicle_history_map)).getMap();
//        map.setPadding(1, 1, 1, 150);
//        map.getUiSettings().setZoomControlsEnabled(true);


        mGoogleMapLayout=(LinearLayout)findViewById(R.id.google_map_layout);
        mGoogleMapLayout.setVisibility(View.VISIBLE);
        RelativeLayout rl = (RelativeLayout) findViewById(R.id.map_view_relativelayout);
        rl.setVisibility(View.GONE);

        FragmentManager myFragmentManager = getSupportFragmentManager();
        SupportMapFragment myMapFragment = (SupportMapFragment) myFragmentManager
                .findFragmentById(R.id.vehicle_history_map);
        myMapFragment.getMapAsync(this);


        //parker_mMarkersHashMap = new HashMap<Marker, String>();

        id_show_history_vehicleid = (TextView) findViewById(R.id.id_show_history_vehicleid);

      //  id_show_vehicle_info_icon = (ImageView) findViewById(R.id.id_show_vehicle_info_icon);

        history_detail_layout = (LinearLayout) findViewById(R.id.history_detail_layout);
        is_history_vehicle_layout = (LinearLayout) findViewById(R.id.is_history_vehicle_layout);
        id_history_show_min_layout = (LinearLayout) findViewById(R.id.id_history_show_min_layout);

        mBtnClose = (Button) findViewById(R.id.history_close_icon);
        // replay_icon = (Button) findViewById(R.id.replay_icon);

        pause_icon = (Button) findViewById(R.id.pause_icon);
        replay_icon = (Button) findViewById(R.id.replay_icon);
        play_icon = (Button) findViewById(R.id.play_icon);
        history_distance_covered_value = (TextView) findViewById(R.id.history_distance_covered_value);
        history_start_location_value = (TextView) findViewById(R.id.history_start_location_value);
        history_end_location_value = (TextView) findViewById(R.id.history_end_location_value);
        histroy_start_time_value = (TextView) findViewById(R.id.histroy_start_time_value);
        history_endtime_value = (TextView) findViewById(R.id.history_endtime_value);
        history_timetaken_value = (TextView) findViewById(R.id.history_timetaken_value);
        history_seekbar = (SeekBar) findViewById(R.id.history_seekbar);
        speed_seekbar = (SeekBar) findViewById(R.id.speed_seekbar);

        history_parked_count_value = (TextView) findViewById(R.id.history_parked_count_value);
        history_parked_count_label = (TextView) findViewById(R.id.history_parked_count_label);
        history_over_speed_count_label = (TextView) findViewById(R.id.history_over_speed_count_label);
        history_over_speed_count_value = (TextView) findViewById(R.id.history_over_speed_count_value);

        history_seekbar.setMax(10);
        history_seekbar.setProgress(5);
        // iLoopDelay = speed_seekbar.getProgress();

        id_history_speed_info_value = (TextView) findViewById(R.id.id_history_speed_info_value);
        id_history_distance_value = (TextView) findViewById(R.id.id_history_distance_value);
        id_history_direction_value = (TextView) findViewById(R.id.id_history_direction_value);
        history_vehicle_info_layout = (LinearLayout) findViewById(R.id.history_vehicle_info_layout);
        history_vehicle_info_layout.setVisibility(View.VISIBLE);
        $ChangeView = (ImageView) findViewById(R.id.map_history_changeViewIcon);

        id_history_speed_info_value.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        id_history_distance_value.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        id_history_direction_value.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        history_distance_covered_value.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        history_start_location_value.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        history_end_location_value.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        histroy_start_time_value.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        history_endtime_value.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        history_timetaken_value.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        id_show_history_vehicleid.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));

        history_parked_count_value.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        history_parked_count_label.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        history_over_speed_count_label.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        history_over_speed_count_value.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        height = displayMetrics.heightPixels;
        width = displayMetrics.widthPixels;

        LinearLayout.LayoutParams imgeChangeView = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        imgeChangeView.width = width * 10 / 100;
        imgeChangeView.height = height * 15 / 100;
        imgeChangeView.gravity = Gravity.RIGHT;
        imgeChangeView.rightMargin = (int) (width * 4 / 100);
        imgeChangeView.topMargin = (int) (height * 24 / 100);
        $ChangeView.setLayoutParams(imgeChangeView);
    }

    public void addMarkerToMap() {
        // play_icon.setVisibility(View.VISIBLE);
        //  pause_icon.setVisibility(View.VISIBLE);
        // replay_icon.setVisibility(View.VISIBLE);
        progressChanged = 5;
        setAnimateTime();
        if (geoPoints.size() > 0) {
            for (int a = 0; a < geoPoints.size(); a++) {

                addMarkerToMap(geoPoints.get(a));

            }
        }
    }

    public void setAnimateTime() {


        // System.out.println("Progress changed is :::::" + progressChanged + " history max size is :::::" + history_seekbar.getMax());
        switch (progressChanged) {

            case 1:
                ANIMATE_SPEEED = 7000;
                ANIMATE_SPEEED_TURN = 7000;
                break;
            case 2:
                ANIMATE_SPEEED = 6000;
                ANIMATE_SPEEED_TURN = 6000;
                break;
            case 3:
                ANIMATE_SPEEED = 5000;
                ANIMATE_SPEEED_TURN = 5000;
                break;
            case 4:
                ANIMATE_SPEEED = 4000;
                ANIMATE_SPEEED_TURN = 4000;
                break;
            case 5:
                ANIMATE_SPEEED = 3000;
                ANIMATE_SPEEED_TURN = 3000;
                break;
            case 6:
                ANIMATE_SPEEED = 2000;
                ANIMATE_SPEEED_TURN = 2000;
                break;
            case 7:
                ANIMATE_SPEEED = 1500;
                ANIMATE_SPEEED_TURN = 1500;
                break;
            case 8:
                ANIMATE_SPEEED = 1000;
                ANIMATE_SPEEED_TURN = 1000;
                break;
            case 9:
                ANIMATE_SPEEED = 500;
                ANIMATE_SPEEED_TURN = 500;
                break;
            case 10:
                ANIMATE_SPEEED = 200;
                ANIMATE_SPEEED_TURN = 200;
                break;
        }


    }

    private void opptionPopUp() {
        // TODO Auto-generated method stub
        final Dialog dialog = new Dialog(HistoryGoogleMapActivity.this);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.radio_popup);
        dialog.show();
        dialog.setCanceledOnTouchOutside(false);

        RadioGroup rg_home = (RadioGroup) dialog.findViewById(R.id.rg_home_views);
        RadioGroup rg_history = (RadioGroup) dialog.findViewById(R.id.rg_history_views);
        rg_history.clearCheck();
        rg_history.setVisibility(View.VISIBLE);
        rg_home.setVisibility(View.GONE);

        RadioButton $Custom = (RadioButton) dialog
                .findViewById(R.id.rb_history_custom);
        RadioButton $Today = (RadioButton) dialog
                .findViewById(R.id.rb_history_today);
        RadioButton $Yesterday = (RadioButton) dialog
                .findViewById(R.id.rb_history_yesterday);
        RadioButton $DayBeforeYesterday = (RadioButton) dialog
                .findViewById(R.id.rb_history_day_before_yesterday);
        RadioButton $LastOneHour = (RadioButton) dialog
                .findViewById(R.id.rb_history_last_one_hour);
        $Custom.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                // map.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                dialog.dismiss();
                showVehicleInfoDialog("Custom");
            }
        });
        $Today.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                //  map.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
                dialog.dismiss();
                showVehicleInfoDialog("Today");
            }
        });
        $Yesterday.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                // map.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
                dialog.dismiss();
                showVehicleInfoDialog("Yesterday");
            }
        });

        $DayBeforeYesterday.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                //  map.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
                dialog.dismiss();
                showVehicleInfoDialog("Day Before Yesterday");
            }
        });
        $LastOneHour.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                // map.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
                dialog.dismiss();
                showVehicleInfoDialog("Last One Hour");
            }
        });

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        height = displayMetrics.heightPixels;
        width = displayMetrics.widthPixels;

        LinearLayout.LayoutParams radioParama = new LinearLayout.LayoutParams(
                FrameLayout.LayoutParams.WRAP_CONTENT,
                FrameLayout.LayoutParams.WRAP_CONTENT);
        radioParama.width = width * 60 / 100;
        radioParama.height = width * 10 / 100;
        radioParama.topMargin = height * 4 / 100;
        radioParama.gravity = Gravity.CENTER;
        radioParama.leftMargin = height * 4 / 100;
        //  $Custom.setLayoutParams(radioParama);
        $DayBeforeYesterday.setLayoutParams(radioParama);
        $Today.setLayoutParams(radioParama);
        $Yesterday.setLayoutParams(radioParama);
        $LastOneHour.setLayoutParams(radioParama);

        LinearLayout.LayoutParams radioterrainParama = new LinearLayout.LayoutParams(
                FrameLayout.LayoutParams.WRAP_CONTENT,
                FrameLayout.LayoutParams.WRAP_CONTENT);
        radioterrainParama.width = width * 60 / 100;
        radioterrainParama.height = width * 10 / 100;
        radioterrainParama.topMargin = height * 4 / 100;
        radioterrainParama.gravity = Gravity.CENTER;
        radioterrainParama.leftMargin = height * 4 / 100;
        radioterrainParama.bottomMargin = height * 4 / 100;
        // $LastOneHour.setLayoutParams(radioterrainParama);
        $Custom.setLayoutParams(radioterrainParama);

        if (width >= 600) {
            $Custom.setTextSize(16);
            $Today.setTextSize(16);
            $Yesterday.setTextSize(16);
            $DayBeforeYesterday.setTextSize(16);
            $LastOneHour.setTextSize(16);
        } else if (width > 501 && width < 600) {
            $Custom.setTextSize(15);
            $Today.setTextSize(15);
            $Yesterday.setTextSize(15);
            $DayBeforeYesterday.setTextSize(15);
            $LastOneHour.setTextSize(15);

        } else if (width > 260 && width < 500) {
            $Custom.setTextSize(14);
            $Today.setTextSize(14);
            $Yesterday.setTextSize(14);
            $DayBeforeYesterday.setTextSize(14);
            $LastOneHour.setTextSize(14);
        } else if (width <= 260) {
            $Custom.setTextSize(13);
            $Today.setTextSize(13);
            $Yesterday.setTextSize(13);
            $DayBeforeYesterday.setTextSize(13);
            $LastOneHour.setTextSize(13);
        }
    }

    /**
     * Map Related code
     */
    private boolean showVehicleInfoDialog(String mReportType) {

//        mHistoryForData.clear();
//
//        mHistoryForData.add("Last One Hour");
//        mHistoryForData.add("Today");
//        mHistoryForData.add("Yesterday");
//        mHistoryForData.add("Day Before Yesterday");
//        mHistoryForData.add("Custom");

        TextView id_popup_history_labelid, id_popup_history_type_value,
                id_popup_to_time_label, id__popupto_date_label,
                id_popup_from_label,
                id_popup_from_time_label, id_txt_divider1, id_txt_divider2;
        Button id_history_selection_done, id_history_selection_cancel;
        Spinner history_for_spinner;
//        if (isFromList) {
//            marker_info_dialog = new Dialog(this, android.R.style.Theme_Light_NoTitleBar_Fullscreen);
//            marker_info_dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
//        } else {
        marker_info_dialog = new Dialog(HistoryGoogleMapActivity.this);
//        }
        marker_info_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        marker_info_dialog.setContentView(R.layout.history_popup);
        marker_info_dialog.setCancelable(false);
        //LinearLayout id_date_time_picker_layout;
        LinearLayout id_from_datetime_layout, id_to_datetime_layout, id_history_type_layout;
        ImageView id_history_vehicle_info_icon;
//        final TextView id_histroy_vehicleid, id_history_type_value, id_from_label, id_from_date, id_from_time, id_to_date, id_to_time;
        // Button id_history_selection_done, id_history_selection_cancel;

        id_from_datetime_layout = (LinearLayout) marker_info_dialog.findViewById(R.id.id_popup_from_datetime_layout);
        id_to_datetime_layout = (LinearLayout) marker_info_dialog.findViewById(R.id.id__popup_to_datetime_layout);
        id_history_type_layout = (LinearLayout) marker_info_dialog.findViewById(R.id.id_popup_history_type_labellayout);
        // id_date_time_picker_layout = (LinearLayout) marker_info_dialog.findViewById(R.id.id_popup_date_time_picker_layout);

        id_history_vehicle_info_icon = (ImageView) marker_info_dialog.findViewById(R.id.id_popup_history_vehicle_info_icon);
        id_histroy_vehicleid = (TextView) marker_info_dialog.findViewById(R.id.id_popup_histroy_vehicleid);

        id_txt_divider1 = (TextView) marker_info_dialog.findViewById(R.id.divider_txt_1);
        id_txt_divider2 = (TextView) marker_info_dialog.findViewById(R.id.divider_txt_1);

        history_for_spinner = (Spinner) marker_info_dialog.findViewById(R.id.id_history_for_spinner);

        // id_history_type_value = (TextView) marker_info_dialog.findViewById(R.id.id_popup_history_type_value);
        // id_from_label = (TextView) marker_info_dialog.findViewById(R.id.id_popup_from_label);

        // id_from_date = (TextView) marker_info_dialog.findViewById(R.id.id_popup_from_date);
        // id_from_time = (TextView) marker_info_dialog.findViewById(R.id.id_popup_from_time);
        // id_to_date = (TextView) marker_info_dialog.findViewById(R.id.id_popup_to_date);
        // id_to_time = (TextView) marker_info_dialog.findViewById(R.id.id_popup_to_time);
        //id_history_selection_done = (Button) marker_info_dialog.findViewById(R.id.id_popup_history_selection_done);
        //id_history_selection_cancel = (Button) marker_info_dialog.findViewById(R.id.id_popup_history_selection_cancel);


        id_popup_history_labelid = (TextView) marker_info_dialog.findViewById(R.id.id_popup_history_labelid);
        id_popup_history_type_value = (TextView) marker_info_dialog.findViewById(R.id.id_popup_history_type_value);
        id_popup_from_label = (TextView) marker_info_dialog.findViewById(R.id.id_popup_from_label);

        id_from_date = (TextView) marker_info_dialog.findViewById(R.id.id_popup_from_date);
        id_popup_from_time_label = (TextView) marker_info_dialog.findViewById(R.id.id_popup_from_time_label);
        id_from_time = (TextView) marker_info_dialog.findViewById(R.id.id_popup_from_time);
        id__popupto_date_label = (TextView) marker_info_dialog.findViewById(R.id.id__popupto_date_label);
        id_to_date = (TextView) marker_info_dialog.findViewById(R.id.id_popup_to_date);
        id_popup_to_time_label = (TextView) marker_info_dialog.findViewById(R.id.id_popup_to_time_label);
        id_to_time = (TextView) marker_info_dialog.findViewById(R.id.id_popup_to_time);

        id_history_selection_done = (Button) marker_info_dialog.findViewById(R.id.id_popup_history_selection_done);
        id_history_selection_cancel = (Button) marker_info_dialog.findViewById(R.id.id_popup_history_selection_cancel);


        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        // int height = metrics.heightPixels;
        //  int width = metrics.widthPixels;
        Constant.ScreenWidth = width;
        Constant.ScreenHeight = height;

        LinearLayout.LayoutParams history_for_layout = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        history_for_layout.width = width * 90 / 100;
        history_for_layout.height = LinearLayout.LayoutParams.WRAP_CONTENT;
        history_for_layout.gravity = Gravity.CENTER;
        history_for_layout.setMargins(width * 5 / 100, 0, width * 5 / 100, 0);
        id_history_type_layout.setLayoutParams(history_for_layout);

        LinearLayout.LayoutParams backImageParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        backImageParams.width = width * 80 / 100;
        backImageParams.height = LinearLayout.LayoutParams.WRAP_CONTENT;
        backImageParams.gravity = Gravity.CENTER;

        id_popup_history_labelid.setLayoutParams(backImageParams);
        id_popup_history_type_value.setLayoutParams(backImageParams);

        LinearLayout.LayoutParams backImageParams1 = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        backImageParams1.width = width * 40 / 100;
        backImageParams1.height = LinearLayout.LayoutParams.WRAP_CONTENT;

        id_popup_from_label.setLayoutParams(backImageParams1);
        id_from_date.setLayoutParams(backImageParams1);
        id_popup_from_time_label.setLayoutParams(backImageParams1);
        id_from_time.setLayoutParams(backImageParams1);

        id__popupto_date_label.setLayoutParams(backImageParams1);
        id_to_date.setLayoutParams(backImageParams1);
        id_popup_to_time_label.setLayoutParams(backImageParams1);
        id_to_time.setLayoutParams(backImageParams1);


        if (width >= 600) {
            id_popup_history_labelid.setTextSize(18);
            id_popup_history_type_value.setTextSize(17);
            id_from_date.setTextSize(17);
            id_from_time.setTextSize(17);
            id_to_date.setTextSize(17);
            id_to_time.setTextSize(17);
            //  $TxtOfflineVehicleValue.setTextSize(16);
        } else if (width > 501 && width < 600) {
            id_popup_history_labelid.setTextSize(17);
            id_popup_history_type_value.setTextSize(16);
            id_from_date.setTextSize(16);
            id_from_time.setTextSize(16);
            id_to_date.setTextSize(16);
            id_to_time.setTextSize(16);
            // $TxtOfflineVehicleValue.setTextSize(15);
        } else if (width > 260 && width < 500) {
            id_popup_history_labelid.setTextSize(16);
            id_popup_history_type_value.setTextSize(15);
            id_from_date.setTextSize(15);
            id_from_time.setTextSize(15);
            id_to_date.setTextSize(15);
            id_to_time.setTextSize(15);
            // $TxtOfflineVehicleValue.setTextSize(14);
        } else if (width <= 260) {
            id_popup_history_labelid.setTextSize(15);
            id_popup_history_type_value.setTextSize(14);
            id_from_date.setTextSize(14);
            id_from_time.setTextSize(14);
            id_to_date.setTextSize(14);
            id_to_time.setTextSize(14);
            //  $TxtOfflineVehicleValue.setTextSize(13);
        }

        // System.out.println("The vehicle short name is :::::" + Constant.SELECTED_VEHICLE_SHORT_NAME);
        id_histroy_vehicleid.setText(Constant.SELECTED_VEHICLE_SHORT_NAME);
//        id_histroy_vehicleid.setText("SBLT-TN02-AY-6661(13)");
        if (mReportType.equalsIgnoreCase("Custom")) {
            // id_date_time_picker_layout.setVisibility(View.VISIBLE);
            id_from_datetime_layout.setVisibility(View.VISIBLE);
            id_to_datetime_layout.setVisibility(View.VISIBLE);
            id_txt_divider1.setVisibility(View.VISIBLE);
            id_txt_divider2.setVisibility(View.VISIBLE);

            String today_date = cons.getDateandTime("today");
            id_from_date.setText(today_date);
            id_from_time.setText("00:00:00");
            id_to_date.setText(today_date);
            id_to_time.setText("23:59:00");

        } else if (mReportType.equalsIgnoreCase("Today")) {
            id_popup_history_type_value.setText("Today");
            // id_date_time_picker_layout.setVisibility(View.VISIBLE);
            id_from_datetime_layout.setVisibility(View.GONE);
            id_to_datetime_layout.setVisibility(View.GONE);
            id_txt_divider1.setVisibility(View.GONE);
            id_txt_divider2.setVisibility(View.GONE);
            String today_date = cons.getDateandTime("today");

            id_from_date.setText(today_date);
            id_from_time.setText("00:00:00");
            id_to_date.setText(today_date);
//            id_to_time.setText("23:59:00");
            id_to_time.setText(cons.getCurrentTime());

        } else if (mReportType.equalsIgnoreCase("Yesterday")) {

            id_popup_history_type_value.setText("Yesterday");
            //id_date_time_picker_layout.setVisibility(View.VISIBLE);
            id_from_datetime_layout.setVisibility(View.GONE);
            id_to_datetime_layout.setVisibility(View.GONE);
            id_txt_divider1.setVisibility(View.GONE);
            id_txt_divider2.setVisibility(View.GONE);
            String yesterday_date = cons.getDateandTime("yesterday");
            id_from_date.setText(yesterday_date);
            id_from_time.setText("00:00:00");
            id_to_date.setText(yesterday_date);
            id_to_time.setText("23:59:00");

        } else if (mReportType.equalsIgnoreCase("Day Before Yesterday")) {

            id_popup_history_type_value.setText("Day Before Yesterday");
            // id_date_time_picker_layout.setVisibility(View.VISIBLE);
            id_from_datetime_layout.setVisibility(View.GONE);
            id_to_datetime_layout.setVisibility(View.GONE);
            id_txt_divider1.setVisibility(View.GONE);
            id_txt_divider2.setVisibility(View.GONE);
            String daybeforeyesterday_date = cons.getDateandTime("daybeforeyesterday");
            id_from_date.setText(daybeforeyesterday_date);
            id_from_time.setText("00:00:00");
            id_to_date.setText(daybeforeyesterday_date);
            id_to_time.setText("23:59:00");

        } else if (mReportType.equalsIgnoreCase("Last One Hour")) {
            id_popup_history_type_value.setText("Last one hour");
            // id_date_time_picker_layout.setVisibility(View.VISIBLE);
            id_from_datetime_layout.setVisibility(View.GONE);
            id_to_datetime_layout.setVisibility(View.GONE);
            id_txt_divider1.setVisibility(View.GONE);
            id_txt_divider2.setVisibility(View.GONE);
            Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("GMT+5:30"));
            int hour = cal.get(Calendar.HOUR_OF_DAY);
            int min = cal.get(Calendar.MINUTE);
            int past_hour = cal.get(Calendar.HOUR_OF_DAY) - 1;
            String lastonehour = cons.getDateandTime("today");
            id_from_date.setText(lastonehour);
            id_from_time.setText(new StringBuilder().append(pad(past_hour)).append(":").append(pad(min)).append(":").append("00"));
            id_to_date.setText(lastonehour);
            id_to_time.setText(new StringBuilder().append(pad(hour)).append(":").append(pad(min)).append(":").append("00"));
        }

        /** listener to handle when history is canceled */
        id_history_selection_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                opptionPopUp();
                marker_info_dialog.hide();
            }
        });

        /** listener to handle when history is selected */
        id_history_selection_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (cd.isConnectingToInternet()) {
                    marker_info_dialog.hide();
//                    id_date_time_picker_layout.setVisibility(View.GONE);
//                    id_map_info_layout.setVisibility(View.GONE);
                    if (map != null) {
                        map.clear();
                    }

                    String string_from_date = id_from_date.getText().toString().trim() + " " + id_from_time.getText().toString().trim();
                    String string_to_date = id_to_date.getText().toString().trim() + " " + id_to_time.getText().toString().trim();

                    // System.out.println("The from date  :::::" + string_from_date);

                    //  System.out.println("The to date  :::::" + string_to_date);

                    SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    Date d = null, d1 = null;
                    try {
                        d = f.parse(string_from_date);

                        // System.out.println("The from time in millis :::::" + fromMilliSeconds);
                        d1 = f.parse(string_to_date);

                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    long fromMilliSeconds = d.getTime();
                    long toMilliSeconds = d1.getTime();


                    // System.out.println("The from date after converted  :::::" + d);


                    String url = Const.API_URL + "mobile/getVehicleHistory4MobileV2?userId=" + Constant.SELECTED_USER_ID + "&vehicleId=" + Constant.SELECTED_VEHICLE_ID + "&fromDate=" + id_from_date.getText().toString() + "&fromTime=" + id_from_time.getText().toString() + "&toDate=" + id_to_date.getText().toString() + "&toTime=" + id_to_time.getText().toString() + "&fromDateUTC=" + fromMilliSeconds + "&toDateUTC=" + toMilliSeconds +
                            "&macid=" + getStdTableValue("macid") + "&appid=" + getStdTableValue("appid");
                    String par[] = {Const.API_URL + "mobile/getGeoFenceView?vehicleId=" + Constant.SELECTED_VEHICLE_ID + "&userId=" + Constant.SELECTED_USER_ID + "&macid=" + getStdTableValue("macid") + "&appid=" + getStdTableValue("appid")};
                    new GetVehicleHistory().execute(url);
                    new GetPOIInformation().execute(par);
                } else {
                    Toast internet_toast = Toast.makeText(getApplicationContext(), "Please Check your Internet Connection", Toast.LENGTH_LONG);
                    internet_toast.show();
                }
            }
        });

        /** listener to handle when From date is clicked in history selection*/
        id_from_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //    Toast.makeText(HistoryActivity.this, "Hi from date clicked ::::", Toast.LENGTH_SHORT).show();
                from_date = true;
                to_date = false;
                from_time = false;
                to_time = false;
                showDialog(DATE_PICKER_ID);
            }
        });
        /** listener to handle when From Time is clicked in history selection*/
        id_from_time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                from_date = false;
                to_date = false;
                from_time = true;
                to_time = false;
                showDialog(TIME_DIALOG_ID);
            }
        });
        /** listener to handle when To date is clicked in history selection*/
        id_to_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                from_date = false;
                to_date = true;
                from_time = false;
                to_time = false;
                showDialog(DATE_PICKER_ID);
            }
        });
        /** listener to handle when To Time is clicked in history selection*/
        id_to_time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                from_date = false;
                to_date = false;
                from_time = false;
                to_time = true;
                showDialog(TIME_DIALOG_ID);
            }
        });


        marker_info_dialog.show();
        return true;
    }

    /**
     * Function to add 0 for time which is lesser that 10. Ex. 08.00, 05.00
     */
    private static String pad(int c) {
        if (c >= 10)
            return String.valueOf(c);
        else
            return "0" + String.valueOf(c);
    }

    public String getStdTableValue(String tag) {
        Cursor std_table_cur = null;
        String stdtablevalues = null;
        try {
            std_table_cur = dbhelper.get_std_table_info(tag);
            std_table_cur.moveToFirst();
            stdtablevalues = std_table_cur.getString(0);
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), "Something went wrong. Please try again", Toast.LENGTH_SHORT).show();
        } finally {
            if (std_table_cur != null) {
                std_table_cur.close();
            }
        }
        return stdtablevalues;
    }


    void dbSetup() {
        dbhelper = new DBHelper(this);
        try {
            dbhelper.createDataBase();
            dbhelper.openDataBase();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

//    public LatLng getLatLng(int pos) {
//        LatLng ll = geoPoints.get(pos);
//
//        return ll;
//    }

    private Location convertLatLngToLocation(LatLng latLng) {
        Location location = new Location("someLoc");
        location.setLatitude(latLng.latitude);
        location.setLongitude(latLng.longitude);
        return location;
    }

    private float bearingBetweenLatLngs(LatLng beginLatLng, LatLng endLatLng) {
        Location beginLocation = convertLatLngToLocation(beginLatLng);
        Location endLocation = convertLatLngToLocation(endLatLng);
        return beginLocation.bearingTo(endLocation);
    }

    /**
     * Code to make the Marker icon adjustable for multiple screen
     */
    public static Bitmap createDrawableFromView(Context context, View view) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        view.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        view.measure(displayMetrics.widthPixels, displayMetrics.heightPixels);
        view.layout(0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels);
        view.buildDrawingCache();
        Bitmap bitmap = Bitmap.createBitmap(view.getMeasuredWidth(), view.getMeasuredHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        view.draw(canvas);
        return bitmap;

    }

    /**
     * Code to make the Marker icon adjustable for multiple screen
     */
    public static Bitmap createParkingDrawableFromView(Context context, View view) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
//        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);

        /* screen arrangements */
        DisplayMetrics metrics = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(metrics);
        int height = metrics.heightPixels;
        int width = metrics.widthPixels;
        Constant.ScreenWidth = width;
        Constant.ScreenHeight = height;

        ViewGroup.LayoutParams backImageParams = new ViewGroup.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        backImageParams.width = width * 15 / 100;
        backImageParams.height = height * 8 / 100;
        //   backImageParams.gravity = Gravity.CENTER;
        view.setLayoutParams(backImageParams);
        // view.setPadding(width * 1 / 100, height * 1 / 100, width * 1 / 100, height * 1 / 100);


        view.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        view.measure(displayMetrics.widthPixels, displayMetrics.heightPixels);
        view.layout(0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels);
        view.buildDrawingCache();
        Bitmap bitmap = Bitmap.createBitmap(view.getMeasuredWidth(), view.getMeasuredHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        view.draw(canvas);
        return bitmap;

    }


    private Animator animator = new Animator();


    int currentPt;

    GoogleMap.CancelableCallback MyCancelableCallback =
            new GoogleMap.CancelableCallback() {

                @Override
                public void onCancel() {
                    // System.out.println("onCancelled called");
                }

                @Override
                public void onFinish() {


                    if (++currentPt < geoPoints.size()) {


                        float targetBearing = bearingBetweenLatLngs(map.getCameraPosition().target, geoPoints.get(currentPt));

                        LatLng targetLatLng = geoPoints.get(currentPt);

                        // System.out.println("Prabha CancelableCallback called :::::::::::::");
                        // System.out.println("currentPt  = " + currentPt);
                        //  System.out.println("size  = " + geoPoints.size());
                        //Create a new CameraPosition
//                        CameraPosition cameraPosition =
//                                new CameraPosition.Builder()
//                                        .target(targetLatLng)
//                                        .tilt(currentPt < geoPoints.size() - 1 ? 90 : 0)
//                                        .bearing(targetBearing)
//                                        .zoom(map.getCameraPosition().zoom)
//                                        .build();

                        CameraPosition cameraPosition;

                        if (m3Dmap) {
                            cameraPosition =
                                    new CameraPosition.Builder()
                                            .target(targetLatLng)
                                            .tilt(currentPt < geoPoints.size() - 1 ? 90 : 0)
                                            .bearing(targetBearing)
                                            .zoom(map.getCameraPosition().zoom)
                                            .build();
                        } else {
                            cameraPosition =
                                    new CameraPosition.Builder()
                                            .target(targetLatLng)
                                            .tilt(currentPt < geoPoints.size() - 1 ? 90 : 0)

                                            .zoom(map.getCameraPosition().zoom)
                                            .build();
                        }

//                        .zoom(history_zoom_level)

                        map.animateCamera(
                                CameraUpdateFactory.newCameraPosition(cameraPosition),
                                3000,
                                MyCancelableCallback);
//                        System.out.println("Animate to: " + geoPoints.get(currentPt) + "\n" +
//                                "Bearing: " + targetBearing);

                        geoPoints.get(currentPt);

                    } else {
                        //info.setText("onFinish()");
                    }

                }

            };

    @Override
    public void onMapReady(GoogleMap googleMap) {

        map = googleMap;
        map.setPadding(1, 1, 1, 150);
        map.getUiSettings().setZoomControlsEnabled(true);
        mMarkersHashMap = new HashMap<Marker, MarkerOptions>();
        mMarkersPositionHashMap = new HashMap<Marker, Integer>();

        map.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                // int childelement_position;
//                if (track) {
//                    id_speed_min_layout.setVisibility(View.VISIBLE);
//                    id_curr_vehicle_info_min.setVisibility(View.VISIBLE);
//                    return true;
//                } else {
//                    if (!(navigation_status.equals("Settings"))) {
//                if (mMarkersHashMap.containsKey(marker)) {
//                    if (cd.isConnectingToInternet()) {
//                        jSelectedVehicleObject = mMarkersHashMap.get(marker);
//                        showVehicleInfoDialog(jSelectedVehicleObject, false);
//                    } else {

//                dsfjhdjkf

//                marker = map.addMarker(new MarkerOptions()
//                        .position(0)
//                        .title("Title")
//                        .snippet("Snippet")
//                        .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_YELLOW)));
//
//                Toast internet_toast = Toast.makeText(getApplicationContext(), "Please Check your Internet Connection", Toast.LENGTH_LONG);
//                internet_toast.show();

//                System.out.println("Marker option size is :::::" + mMarkersHashMap.size());
                if (mMarkersHashMap.containsKey(marker)) {
//                    if (cd.isConnectingToInternet()) {

                    final int marPos = mMarkersPositionHashMap.get(marker);
                    map.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {

                        // Use default InfoWindow frame
                        @Override
                        public View getInfoWindow(Marker arg0) {
                            return null;
                        }

                        // Defines the contents of the InfoWindow
                        @Override
                        public View getInfoContents(Marker arg0) {

                            // Getting view from the layout file info_window_layout
                            View v = getLayoutInflater().inflate(R.layout.map_info_txt_layout, null);

                            // Getting the position from the marker
                            LatLng latLng = arg0.getPosition();

                            DisplayMetrics displayMetrics = new DisplayMetrics();
                            getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
                            height = displayMetrics.heightPixels;
                            width = displayMetrics.widthPixels;
                            LinearLayout layout = (LinearLayout) v.findViewById(R.id.map_info_layout);
                            LinearLayout.LayoutParams backImageParams = new LinearLayout.LayoutParams(
                                    LinearLayout.LayoutParams.WRAP_CONTENT,
                                    LinearLayout.LayoutParams.WRAP_CONTENT);
                            backImageParams.width = width * 80 / 100;
                            //backImageParams.height = height * 10 / 100;
                            backImageParams.gravity = Gravity.CENTER;
                            layout.setLayoutParams(backImageParams);

                            // Getting reference to the TextView to set latitude
                            TextView txtContent = (TextView) v.findViewById(R.id.tv_txt_content);
                            txtContent.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
//                            TextView tx = null;

//                            txtContent = tx;
//                        new GetAddressTask(txtContent).execute(geoPoints.get(currentIndex).latitude, geoPoints.get(currentIndex).longitude, 6.0);

                            StringBuffer addr = new StringBuffer();
                            Geocoder geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());
                            List<Address> addresses = null;
                            try {
                                addresses = geocoder.getFromLocation(geoPoints.get(marPos).latitude, geoPoints.get(marPos).longitude, 1);
                            } catch (IOException e1) {
                                e1.printStackTrace();
                            } catch (IllegalArgumentException e2) {
                                // Error message to post in the log
//                        String errorString = "Illegal arguments " +
//                                Double.toString(params[0]) +
//                                " , " +
//                                Double.toString(params[1]) +
//                                " passed to address service";
//                        e2.printStackTrace();
//                        return errorString;
                            } catch (NullPointerException np) {
                                // TODO Auto-generated catch block
                                np.printStackTrace();
                            }
                            // If the reverse geocode returned an address
                            if (addresses != null && addresses.size() > 0) {
                                // Get the first address
                                Address address = addresses.get(0);
                /*
                 * Format the first line of address (if available),
                 * city, and country name.
                 */
                                String addressText = null;

                                for (int i = 0; i < address.getMaxAddressLineIndex(); i++) {
                                    addressText = address.getAddressLine(i);
                                    addr.append(addressText + ",");
                                }
                                // Return the text
                                // return addr.toString();
                            } else {
                                addr.append("No address found");
                            }


//                            if (mPosList.get(marPos).equalsIgnoreCase("P")) {
//                                txtContent.setText(addr + " " + cons.getTimefromserver(datetimelist.get(marPos)) + " \n Parked Time : " + mIdleTimeList.get(marPos));
//                            } else if (mPosList.get(marPos).equalsIgnoreCase("S")) {
//                                txtContent.setText(addr + " " + cons.getTimefromserver(datetimelist.get(marPos)) + " \n Idle Time : " + mIdleTimeList.get(marPos));
//                            } else {
//                                txtContent.setText(addr + " " + cons.getTimefromserver(datetimelist.get(marPos)));
//                            }

                            String mAddsData = null;
                            if (mAddsList.get(marPos) != null) {
                                mAddsData = mAddsList.get(marPos);
                            }

                            if (mPosList.get(marPos).equalsIgnoreCase("P")) {
                                txtContent.setText(mAddsData + " " + cons.getTimefromserver(datetimelist.get(marPos)) + " \n Parked Time : " + mIdleTimeList.get(marPos));
                            } else if (mPosList.get(marPos).equalsIgnoreCase("S")) {
                                txtContent.setText(mAddsData + " " + cons.getTimefromserver(datetimelist.get(marPos)) + " \n Idle Time : " + mIdleTimeList.get(marPos));
                            } else {
                                txtContent.setText(mAddsData + " " + cons.getTimefromserver(datetimelist.get(marPos)));
                            }


                            if (mSpeedLimit != null && speedlist.get(marPos) != null) {
                                if (Integer.valueOf(speedlist.get(marPos)) > Integer.valueOf(mSpeedLimit)) {
                                    txtContent.setText(txtContent.getText().toString().trim() + " \n Over Speed");
                                }
                            }

                            return v;

                        }
                    });


                    LatLng markerPos = geoPoints.get(marPos);
//                    MarkerOptions markerOption = new MarkerOptions().position(markerPos);

                    MarkerOptions markerOptions2 = mMarkersHashMap.get(marker);
//                    System.out.println("Marker option laocation is :::::" + markerOptions2.getPosition());
                    Marker marker2 = map.addMarker(markerOptions2);
//                    markerOptions2.get
                    marker2.showInfoWindow();
//                        showVehicleInfoDialog(jSelectedVehicleObject, false);
//                    } else {
//                    Toast internet_toast = Toast.makeText(getApplicationContext(), "Please Check your Internet Connection", Toast.LENGTH_LONG);
//                    internet_toast.show();
//                    }
                    return true;
                }
                return true;


//                marker.showInfoWindow();
//                    }
//                    return true;
//                }
//                return true;

            }
        });


    }

//    @Override
//    public void onStart() {
//        super.onStart();
//
//        // ATTENTION: This was auto-generated to implement the App Indexing API.
//        // See https://g.co/AppIndexing/AndroidStudio for more information.
//        client.connect();
//        Action viewAction = Action.newAction(
//                Action.TYPE_VIEW, // TODO: choose an action type.
//                "HistoryActivity Page", // TODO: Define a title for the content shown.
//                // TODO: If you have web page content that matches this app activity's content,
//                // make sure this auto-generated web page URL is correct.
//                // Otherwise, set the URL to null.
//                Uri.parse("http://host/path"),
//                // TODO: Make sure this auto-generated app deep link URI is correct.
//                Uri.parse("android-app://com.vamosys.vamos/http/host/path")
//        );
//        AppIndex.AppIndexApi.start(client, viewAction);
//    }
//
//    @Override
//    public void onStop() {
//        super.onStop();
//
//        // ATTENTION: This was auto-generated to implement the App Indexing API.
//        // See https://g.co/AppIndexing/AndroidStudio for more information.
//        Action viewAction = Action.newAction(
//                Action.TYPE_VIEW, // TODO: choose an action type.
//                "HistoryActivity Page", // TODO: Define a title for the content shown.
//                // TODO: If you have web page content that matches this app activity's content,
//                // make sure this auto-generated web page URL is correct.
//                // Otherwise, set the URL to null.
//                Uri.parse("http://host/path"),
//                // TODO: Make sure this auto-generated app deep link URI is correct.
//                Uri.parse("android-app://com.vamosys.vamos/http/host/path")
//        );
//        AppIndex.AppIndexApi.end(client, viewAction);
//        client.disconnect();
//    }


    public class Animator implements Runnable {


        private final Interpolator interpolator = new LinearInterpolator();


        float tilt = 90;
        float zoom = 15.5f;
        boolean upward = true;

        long start = SystemClock.uptimeMillis();

        LatLng endLatLng = null;
        LatLng beginLatLng = null;

        boolean showPolyline = false;

        private Marker trackingMarker;

        public void reset() {
            //  System.out.println("Prabha reset called :::::::::::::");
            //    resetMarkers();
            start = SystemClock.uptimeMillis();
            if (currentIndex == geoPoints.size()) {
                currentIndex = 0;
            } else {

            }
            // System.out.println("the current index in reset method is ::::" + currentIndex);
            endLatLng = getEndLatLng();
            beginLatLng = getBeginLatLng();

        }

        public void stop() {
            if (trackingMarker != null) {
                trackingMarker.remove();
            }
            mHandler.removeCallbacks(animator);

        }

        public void initialize(boolean showPolyLine) {
            // System.out.println("Prabha initialize called :::::::::::::");
            reset();
            this.showPolyline = showPolyLine;

            // highLightMarker(0);

            if (showPolyLine) {
                polyLine = initializePolyLine();
            }
            // System.out.println("Initia lize method called ::::" + currentIndex);
            // We first need to put the camera in the correct position for the first run (we need 2 markers for this).....


            LatLng markerPos = geoPoints.get(currentIndex);
            LatLng secondPos = geoPoints.get(currentIndex + 1);
            if (mPosList.get(currentIndex) != null && !mPosList.get(currentIndex).equalsIgnoreCase("M")) {

                map.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {

                    // Use default InfoWindow frame
                    @Override
                    public View getInfoWindow(Marker arg0) {
                        return null;
                    }

                    // Defines the contents of the InfoWindow
                    @Override
                    public View getInfoContents(Marker arg0) {

                        // Getting view from the layout file info_window_layout
                        View v = getLayoutInflater().inflate(R.layout.map_info_txt_layout, null);

                        // Getting the position from the marker
                        LatLng latLng = arg0.getPosition();

                        DisplayMetrics displayMetrics = new DisplayMetrics();
                        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
                        height = displayMetrics.heightPixels;
                        width = displayMetrics.widthPixels;
                        LinearLayout layout = (LinearLayout) v.findViewById(R.id.map_info_layout);
                        LinearLayout.LayoutParams backImageParams = new LinearLayout.LayoutParams(
                                LinearLayout.LayoutParams.WRAP_CONTENT,
                                LinearLayout.LayoutParams.WRAP_CONTENT);
                        backImageParams.width = width * 80 / 100;
                        //backImageParams.height = height * 10 / 100;
                        backImageParams.gravity = Gravity.CENTER;
                        layout.setLayoutParams(backImageParams);

                        // Getting reference to the TextView to set latitude
                        TextView txtContent = (TextView) v.findViewById(R.id.tv_txt_content);
                        txtContent.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));

//                        new GetAddressTask(txtContent).execute(geoPoints.get(currentIndex).latitude, geoPoints.get(currentIndex).longitude, 6.0);

                        StringBuffer addr = new StringBuffer();
                        Geocoder geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());
                        List<Address> addresses = null;
                        try {
                            addresses = geocoder.getFromLocation(geoPoints.get(currentIndex).latitude, geoPoints.get(currentIndex).longitude, 1);
                        } catch (IOException e1) {
                            e1.printStackTrace();
                        } catch (IllegalArgumentException e2) {
                            // Error message to post in the log
//                        String errorString = "Illegal arguments " +
//                                Double.toString(params[0]) +
//                                " , " +
//                                Double.toString(params[1]) +
//                                " passed to address service";
//                        e2.printStackTrace();
//                        return errorString;
                        } catch (NullPointerException np) {
                            // TODO Auto-generated catch block
                            np.printStackTrace();
                        }
                        // If the reverse geocode returned an address
                        if (addresses != null && addresses.size() > 0) {
                            // Get the first address
                            Address address = addresses.get(0);
                /*
                 * Format the first line of address (if available),
                 * city, and country name.
                 */
                            String addressText = null;

                            for (int i = 0; i < address.getMaxAddressLineIndex(); i++) {
                                addressText = address.getAddressLine(i);
                                addr.append(addressText + ",");
                            }
                            // Return the text
                            // return addr.toString();
                        } else {
                            addr.append("No address found");
                        }


//                        txtContent.setText(addr + " " + cons.getTimefromserver(datetimelist.get(currentIndex)));
//                        if (mPosList.get(currentIndex).equalsIgnoreCase("P")) {
//                            txtContent.setText(addr + " " + cons.getTimefromserver(datetimelist.get(currentIndex)) + " \n Parked Time : " + mIdleTimeList.get(currentIndex));
//                        } else if (mPosList.get(currentIndex).equalsIgnoreCase("S")) {
//                            txtContent.setText(addr + " " + cons.getTimefromserver(datetimelist.get(currentIndex)) + " \n Idle Time : " + mIdleTimeList.get(currentIndex));
//                        } else {
//                            txtContent.setText(addr + " " + cons.getTimefromserver(datetimelist.get(currentIndex)));
//                        }

                        String mAddsData = null;
                        if (mAddsList.get(currentIndex) != null) {
                            mAddsData = mAddsList.get(currentIndex);
                        }

                        if (mPosList.get(currentIndex).equalsIgnoreCase("P")) {
                            txtContent.setText(mAddsData + " " + cons.getTimefromserver(datetimelist.get(currentIndex)) + " \n Parked Time : " + mIdleTimeList.get(currentIndex));
                        } else if (mPosList.get(currentIndex).equalsIgnoreCase("S")) {
                            txtContent.setText(mAddsData + " " + cons.getTimefromserver(datetimelist.get(currentIndex)) + " \n Idle Time : " + mIdleTimeList.get(currentIndex));
                        } else {
                            txtContent.setText(mAddsData + " " + cons.getTimefromserver(datetimelist.get(currentIndex)));
                        }
                        return v;

                    }
                });

                MarkerOptions markerOption = new MarkerOptions().position(markerPos);
                // markerOption.title(parse.getshortname());

                if (currentIndex == 0) {


                    ImageView id_custom_marker_icon = (ImageView) mCustomViewMarker.findViewById(R.id.id_custom_marker_icon_for_history);
                    ImageView img_mark1 = (ImageView) mCustomViewMarker.findViewById(R.id.id_history_marker_icon);
                    ImageView img_mark2 = (ImageView) mCustomViewMarker.findViewById(R.id.id_custom_marker_icon);
                    ImageView img_mark3 = (ImageView) mCustomViewMarker.findViewById(R.id.id_vehicle_in_marker);

                    img_mark1.setVisibility(View.GONE);
                    img_mark2.setVisibility(View.GONE);
                    img_mark3.setVisibility(View.GONE);
                    id_custom_marker_icon.setVisibility(View.VISIBLE);

//                ImageView img_mark4 = (ImageView) parking_marker.findViewById(R.id.id_vehicle_in_marker);


//                ImageView id_vehicle_in_marker = (ImageView) mCustomViewMarker.findViewById(R.id.id_vehicle_in_marker);
                    id_custom_marker_icon.setImageResource(R.drawable.start);
                    markerOption.icon(BitmapDescriptorFactory.fromBitmap(createDrawableFromView(HistoryGoogleMapActivity.this, mCustomViewMarker)));

                } else {

//                }

                    if (mPosList.get(currentIndex).equalsIgnoreCase("P")) {
//                    markerOption.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE));

                        // markerOption.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE));
//                    markerOption.icon(BitmapDescriptorFactory.fromResource(R.drawable.parking));
//                    ImageView id_vehicle_in_marker = (ImageView) mCustomViewMarker.findViewById(R.id.id_vehicle_in_marker);
//                    id_vehicle_in_marker.setImageResource(R.drawable.parking);
//                    markerOption.icon(BitmapDescriptorFactory.fromBitmap(createDrawableFromView(HistoryActivity.this, mCustomViewMarker)));

                        ImageView id_custom_marker_icon = (ImageView) mCustomViewMarker.findViewById(R.id.id_custom_marker_icon_for_history);
                        ImageView img_mark1 = (ImageView) mCustomViewMarker.findViewById(R.id.id_history_marker_icon);
                        ImageView img_mark2 = (ImageView) mCustomViewMarker.findViewById(R.id.id_custom_marker_icon);
                        ImageView img_mark3 = (ImageView) mCustomViewMarker.findViewById(R.id.id_vehicle_in_marker);

                        img_mark1.setVisibility(View.GONE);
                        img_mark2.setVisibility(View.GONE);
                        img_mark3.setVisibility(View.GONE);
                        id_custom_marker_icon.setVisibility(View.VISIBLE);

//                ImageView img_mark4 = (ImageView) parking_marker.findViewById(R.id.id_vehicle_in_marker);


//                ImageView id_vehicle_in_marker = (ImageView) mCustomViewMarker.findViewById(R.id.id_vehicle_in_marker);
//                        id_custom_marker_icon.setImageResource(R.drawable.parking);
                        id_custom_marker_icon.setImageResource(R.drawable.icon_parking);
                        markerOption.icon(BitmapDescriptorFactory.fromBitmap(createDrawableFromView(HistoryGoogleMapActivity.this, mCustomViewMarker)));


                    } else if (mPosList.get(currentIndex).equalsIgnoreCase("S")) {
                        //markerOption.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_YELLOW));

                        ImageView id_custom_marker_icon = (ImageView) mCustomViewMarker.findViewById(R.id.id_custom_marker_icon_for_history);
                        ImageView img_mark1 = (ImageView) mCustomViewMarker.findViewById(R.id.id_history_marker_icon);
                        ImageView img_mark2 = (ImageView) mCustomViewMarker.findViewById(R.id.id_custom_marker_icon);
                        ImageView img_mark3 = (ImageView) mCustomViewMarker.findViewById(R.id.id_vehicle_in_marker);

                        img_mark1.setVisibility(View.GONE);
                        img_mark2.setVisibility(View.GONE);
                        img_mark3.setVisibility(View.GONE);
                        id_custom_marker_icon.setVisibility(View.VISIBLE);

                        id_custom_marker_icon.setImageResource(R.drawable.icon_idle);
                        markerOption.icon(BitmapDescriptorFactory.fromBitmap(createDrawableFromView(HistoryGoogleMapActivity.this, mCustomViewMarker)));
                    }
                }
                //  markerOption.icon(BitmapDescriptorFactory.fromBitmap(createDrawableFromView(HistoryActivity.this, marker)));
                // if (current_vehicle_location_marker.isEmpty()) {


                Marker currentMarker = map.addMarker(markerOption);

//                Marker marker = map.addMarker(new MarkerOptions()
//                        .position(markerPos)
//                        .title("Title")
//                        .snippet("Snippet")
//                        .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_YELLOW)));

//                System.out.println("Marker option inserting laocation is :::::" + markerOption.getPosition());
                mMarkersHashMap.put(currentMarker, markerOption);
                mMarkersPositionHashMap.put(currentMarker, currentIndex);
                currentMarker.showInfoWindow();


            } else {

                // markerOption.title(parse.getshortname());

                if (currentIndex == 0) {
                    MarkerOptions markerOption = new MarkerOptions().position(markerPos);
                    map.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {

                        // Use default InfoWindow frame
                        @Override
                        public View getInfoWindow(Marker arg0) {
                            return null;
                        }

                        // Defines the contents of the InfoWindow
                        @Override
                        public View getInfoContents(Marker arg0) {

                            // Getting view from the layout file info_window_layout
                            View v = getLayoutInflater().inflate(R.layout.map_info_txt_layout, null);

                            // Getting the position from the marker
                            LatLng latLng = arg0.getPosition();

                            DisplayMetrics displayMetrics = new DisplayMetrics();
                            getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
                            height = displayMetrics.heightPixels;
                            width = displayMetrics.widthPixels;
                            LinearLayout layout = (LinearLayout) v.findViewById(R.id.map_info_layout);
                            LinearLayout.LayoutParams backImageParams = new LinearLayout.LayoutParams(
                                    LinearLayout.LayoutParams.WRAP_CONTENT,
                                    LinearLayout.LayoutParams.WRAP_CONTENT);
                            backImageParams.width = width * 80 / 100;
                            //backImageParams.height = height * 10 / 100;
                            backImageParams.gravity = Gravity.CENTER;
                            layout.setLayoutParams(backImageParams);

                            // Getting reference to the TextView to set latitude
                            TextView txtContent = (TextView) v.findViewById(R.id.tv_txt_content);
                            txtContent.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));

//                        new GetAddressTask(txtContent).execute(geoPoints.get(currentIndex).latitude, geoPoints.get(currentIndex).longitude, 6.0);

                            StringBuffer addr = new StringBuffer();
                            Geocoder geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());
                            List<Address> addresses = null;
                            try {
                                addresses = geocoder.getFromLocation(geoPoints.get(currentIndex).latitude, geoPoints.get(currentIndex).longitude, 1);
                            } catch (IOException e1) {
                                e1.printStackTrace();
                            } catch (IllegalArgumentException e2) {
                                // Error message to post in the log
//                        String errorString = "Illegal arguments " +
//                                Double.toString(params[0]) +
//                                " , " +
//                                Double.toString(params[1]) +
//                                " passed to address service";
//                        e2.printStackTrace();
//                        return errorString;
                            } catch (NullPointerException np) {
                                // TODO Auto-generated catch block
                                np.printStackTrace();
                            }
                            // If the reverse geocode returned an address
                            if (addresses != null && addresses.size() > 0) {
                                // Get the first address
                                Address address = addresses.get(0);
                /*
                 * Format the first line of address (if available),
                 * city, and country name.
                 */
                                String addressText = null;

                                for (int i = 0; i < address.getMaxAddressLineIndex(); i++) {
                                    addressText = address.getAddressLine(i);
                                    addr.append(addressText + ",");
                                }
                                // Return the text
                                // return addr.toString();
                            } else {
                                addr.append("No address found");
                            }


//                        txtContent.setText(addr + " " + cons.getTimefromserver(datetimelist.get(currentIndex)));
//                            if (mPosList.get(currentIndex).equalsIgnoreCase("P")) {
//                                txtContent.setText(addr + " " + cons.getTimefromserver(datetimelist.get(currentIndex)) + " \n Parked Time : " + mIdleTimeList.get(currentIndex));
//                            } else if (mPosList.get(currentIndex).equalsIgnoreCase("S")) {
//                                txtContent.setText(addr + " " + cons.getTimefromserver(datetimelist.get(currentIndex)) + " \n Idle Time : " + mIdleTimeList.get(currentIndex));
//                            } else {
//                                txtContent.setText(addr + " " + cons.getTimefromserver(datetimelist.get(currentIndex)));
//                            }

                            String mAddsData = null;
                            if (mAddsList.get(currentIndex) != null) {
                                mAddsData = mAddsList.get(currentIndex);
                            }

                            if (mPosList.get(currentIndex).equalsIgnoreCase("P")) {
                                txtContent.setText(mAddsData + " " + cons.getTimefromserver(datetimelist.get(currentIndex)) + " \n Parked Time : " + mIdleTimeList.get(currentIndex));
                            } else if (mPosList.get(currentIndex).equalsIgnoreCase("S")) {
                                txtContent.setText(mAddsData + " " + cons.getTimefromserver(datetimelist.get(currentIndex)) + " \n Idle Time : " + mIdleTimeList.get(currentIndex));
                            } else {
                                txtContent.setText(mAddsData + " " + cons.getTimefromserver(datetimelist.get(currentIndex)));
                            }

                            if (mSpeedLimit != null && speedlist.get(currentIndex) != null) {
                                if (Integer.valueOf(speedlist.get(currentIndex)) > Integer.valueOf(mSpeedLimit)) {
                                    txtContent.setText(txtContent.getText().toString().trim() + " \n Over Speed");
                                }
                            }

                            return v;

                        }
                    });


                    ImageView id_custom_marker_icon = (ImageView) mCustomViewMarker.findViewById(R.id.id_custom_marker_icon_for_history);
                    ImageView img_mark1 = (ImageView) mCustomViewMarker.findViewById(R.id.id_history_marker_icon);
                    ImageView img_mark2 = (ImageView) mCustomViewMarker.findViewById(R.id.id_custom_marker_icon);
                    ImageView img_mark3 = (ImageView) mCustomViewMarker.findViewById(R.id.id_vehicle_in_marker);

                    img_mark1.setVisibility(View.GONE);
                    img_mark2.setVisibility(View.GONE);
                    img_mark3.setVisibility(View.GONE);
                    id_custom_marker_icon.setVisibility(View.VISIBLE);

//                ImageView img_mark4 = (ImageView) parking_marker.findViewById(R.id.id_vehicle_in_marker);


//                ImageView id_vehicle_in_marker = (ImageView) mCustomViewMarker.findViewById(R.id.id_vehicle_in_marker);
                    id_custom_marker_icon.setImageResource(R.drawable.start);
                    markerOption.icon(BitmapDescriptorFactory.fromBitmap(createDrawableFromView(HistoryGoogleMapActivity.this, mCustomViewMarker)));

                    Marker currentMarker = map.addMarker(markerOption);

//                Marker marker = map.addMarker(new MarkerOptions()
//                        .position(markerPos)
//                        .title("Title")
//                        .snippet("Snippet")
//                        .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_YELLOW)));
//                    System.out.println("Marker option inserting laocation is :::::" + markerOption.getPosition());
                    mMarkersHashMap.put(currentMarker, markerOption);
                    mMarkersPositionHashMap.put(currentMarker, currentIndex);
                    currentMarker.showInfoWindow();


                }


            }


            setupCameraPositionForMovement(markerPos, secondPos);

        }

        private void setupCameraPositionForMovement(LatLng markerPos,
                                                    LatLng secondPos) {
            System.out.println("setup camera position called ::::");
            float bearing = bearingBetweenLatLngs(markerPos, secondPos);

            trackingMarker = map.addMarker(new MarkerOptions().position(markerPos)
                    .title("title")
                    .snippet("snippet"));

            //history_zoom_level = (map.getCameraPosition().zoom);
//            if animation required enable this method
//            CameraPosition cameraPosition =
//                    new CameraPosition.Builder()
//                            .target(markerPos)
//                            .bearing(bearing + BEARING_OFFSET)
//                            .tilt(90)
//                            .zoom(history_zoom_level)
//                            .build();

            CameraPosition cameraPosition;
            if (m3Dmap) {

                cameraPosition =
                        new CameraPosition.Builder()
                                .target(markerPos)
                                .bearing(bearing + BEARING_OFFSET)
                                .tilt(90)
                                .zoom(history_zoom_level)
                                .build();


            } else {

                //            if animation not required use this method
                cameraPosition =
                        new CameraPosition.Builder()
                                .target(markerPos)

                                .tilt(90)
                                .zoom(history_zoom_level)
                                .build();


            }


//            .zoom(history_zoom_level)
            map.animateCamera(
                    CameraUpdateFactory.newCameraPosition(cameraPosition),
                    ANIMATE_SPEEED_TURN,
                    new GoogleMap.CancelableCallback() {

                        @Override
                        public void onFinish() {
                            System.out.println("finished camera");
                            animator.reset();
                            Handler handler = new Handler();
                            handler.post(animator);
                        }

                        @Override
                        public void onCancel() {
                            //System.out.println("cancelling camera");
                        }
                    }
            );
        }

        private Polyline polyLine;

        private Polyline initializePolyLine() {
            PolylineOptions rectOptions = new PolylineOptions();
            rectOptions.add(geoPoints.get(currentIndex));
            rectOptions.color(getResources().getColor(R.color.history_polyline_color));

            return map.addPolyline(rectOptions);
        }

        /**
         * Add the marker to the polyline.
         */
        private void updatePolyLine(LatLng latLng) {
            // System.out.println("Prabha updatePolyLine called :::::::::::::");
            List<LatLng> points = polyLine.getPoints();
            points.add(latLng);
            //rectOptions.color(getResources().getColor(R.color.history_polyline_color));
            polyLine.setPoints(points);
        }


        public void stopAnimation() {
            isStopCalled = false;
            animator.stop();
        }

        public void startAnimation(boolean showPolyLine) {
            if (geoPoints.size() > 2) {

                //System.out.println("Start animation geo points size " + geoPoints.size());

                animator.initialize(showPolyLine);
            } else {
                replay_icon.setVisibility(View.GONE);
                play_icon.setVisibility(View.GONE);
                pause_icon.setVisibility(View.GONE);
            }
        }


        @Override
        public void run() {
            // System.out.println("Run called :::::");
            // System.out.println("Prabha run called :::::::::::::");
            long elapsed = SystemClock.uptimeMillis() - start;
            double t = interpolator.getInterpolation((float) elapsed / ANIMATE_SPEEED);

//			LatLng endLatLng = getEndLatLng();
//			LatLng beginLatLng = getBeginLatLng();

            double lat = t * endLatLng.latitude + (1 - t) * beginLatLng.latitude;
            double lng = t * endLatLng.longitude + (1 - t) * beginLatLng.longitude;
            LatLng newPosition = new LatLng(lat, lng);

            trackingMarker.setPosition(newPosition);

            if (showPolyline) {
//                System.out.println("Prabha updatePolyLine called :::::::::::::");
                updatePolyLine(newPosition);
            }

            // It's not possible to move the marker + center it through a cameraposition update while another camerapostioning was already happening.
            //navigateToPoint(newPosition,tilt,bearing,currentZoom,false);
            //navigateToPoint(newPosition,false);

            if (t < 1) {
                //System.out.println("Prabha t < 1 true called :::::::::::::");
                mHandler.postDelayed(this, 16);
            } else {
                // System.out.println("Prabha t >>>> 1 else called :::::::::::::");
                // System.out.println("Move to next marker.... current = " + currentIndex + " and size = " + geoPoints.size());
                // imagine 5 elements -  0|1|2|3|4 currentindex must be smaller than 4
                if (currentIndex < geoPoints.size() - 2) {

                    //  System.out.println("Prabha currentIndex < geoPoints.size() - 2 called :::::::::::::");

                    currentIndex++;

                    endLatLng = getEndLatLng();
                    beginLatLng = getBeginLatLng();


                    start = SystemClock.uptimeMillis();

                    LatLng begin = getBeginLatLng();
                    LatLng end = getEndLatLng();

                    float bearingL = bearingBetweenLatLngs(begin, end);

                    highLightMarker(currentIndex);


//                    if animation required enable this method

//                    CameraPosition cameraPosition =
//                            new CameraPosition.Builder()
//                                    .target(end) // changed this...
//                                    .bearing(bearingL + BEARING_OFFSET)
//                                    .tilt(tilt)
//                                    .zoom(map.getCameraPosition().zoom)
//                                    .build();

                    CameraPosition cameraPosition;
                    if (m3Dmap) {
                        cameraPosition =
                                new CameraPosition.Builder()
                                        .target(end) // changed this...
                                        .bearing(bearingL + BEARING_OFFSET)
                                        .tilt(tilt)
                                        .zoom(map.getCameraPosition().zoom)
                                        .build();
                    } else {
//                    if animation not required use this method
                        cameraPosition =
                                new CameraPosition.Builder()
                                        .target(end) // changed this...

                                        .tilt(tilt)
                                        .zoom(map.getCameraPosition().zoom)
                                        .build();
                    }


//                    .zoom(history_zoom_level)
                    map.animateCamera(
                            CameraUpdateFactory.newCameraPosition(cameraPosition),
                            ANIMATE_SPEEED_TURN,
                            null
                    );

                    start = SystemClock.uptimeMillis();
                    mHandler.postDelayed(animator, 16);

                } else {

                    // System.out.println("the current index in anim else method is :::::" + currentIndex);

                    currentIndex++;
                    highLightMarker(currentIndex);
                    stopAnimation();
                }

            }
        }


        private LatLng getEndLatLng() {

            // System.out.println("The current index is ::::" + currentIndex);
//if(currentIndex!=geoPoints.size()){
//    cur
//}
//            if(currentIndex+1==geoPoints.size()){
//                currentIndex=currentIndex-2;
//            }

            return geoPoints.get(currentIndex + 1);
        }

        private LatLng getBeginLatLng() {

//            if(currentIndex+1==geoPoints.size()){
//                currentIndex=currentIndex-1;
//            }

            return geoPoints.get(currentIndex);
        }

        private void adjustCameraPosition() {
            //System.out.println("tilt = " + tilt);
            //System.out.println("upward = " + upward);
            //System.out.println("zoom = " + zoom);
            if (upward) {

                if (tilt < 90) {
                    tilt++;
                    zoom -= 0.01f;
                } else {
                    upward = false;
                }

            } else {
                if (tilt > 0) {
                    tilt--;
                    zoom += 0.01f;
                } else {
                    upward = true;
                }
            }
        }
    }

    ;

    /**
     * Highlight the marker by index.
     */
    private void highLightMarker(int index) {
        highLightMarker(markers.get(index));
    }

    /**
     * Adds a marker to the map.
     */
    public void addMarkerToMap(LatLng latLng) {
        Marker marker = map.addMarker(new MarkerOptions().position(latLng)
                .title("title")
                .snippet("snippet"));
        marker.setVisible(false);
        markers.add(marker);

    }


    /**
     * Highlight the marker by marker.
     */
    private void highLightMarker(Marker marker) {

		/*
        for (Marker foundMarker : this.markers) {
			if (!foundMarker.equals(marker)) {
				foundMarker.setIcon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
			} else {
				foundMarker.setIcon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE));
				foundMarker.showInfoWindow();
			}
		}
		*/
//        marker.setIcon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE));
        // marker.showInfoWindow();


        if (geoPoints.size() - 1 == currentIndex) {
            //System.out.println("The current index is :::::" + currentIndex + " HIIIIIII 000000");
            ParsingClass parsingClass = new ParsingClass();
            parsingClass.historyParse(mHistoryResult);

            id_history_distance_value.setText((parsingClass.gettripDistance().equals("null") ? "No Data" : parsingClass.gettripDistance()));
            //  histroy_start_time_value.setText((parsingClass.getfromDateTime().equals("null") ? "No Data" : parsingClass.getfromDateTime()));
            id_history_direction_value.setText((parsingClass.gettoDateTime().equals("null") ? "No Data" : parsingClass.gettoDateTime()));

            //System.out.println("The history end time high light marker is 222222::::::" + (parsingClass.gettoDateTime().equals("null") ? "No Data" : parsingClass.gettoDateTime()));


            // id_history_distance_value.setText(distancelist.get(currentIndex));
            // id_history_direction_value.setText(cons.getTimefromserver(datetimelist.get(currentIndex)));
        } else {
            id_history_distance_value.setText(distancelist.get(currentIndex));
            id_history_direction_value.setText(cons.getTimefromserver(datetimelist.get(currentIndex)));
        }

        id_history_speed_info_value.setText(speedlist.get(currentIndex));
        //  id_history_distance_value.setText(distancelist.get(currentIndex));
        //  id_history_direction_value.setText(cons.getTimefromserver(datetimelist.get(currentIndex)));
        history_vehicle_info_layout.setVisibility(View.VISIBLE);

        //System.out.println("the position is highlight marker is ::::" + mPosList.get(currentIndex) + " pos is :::" + currentIndex + " and size is ::::" + mPosList + " size is ::::" + mPosList.size());

        if (mPosList.get(currentIndex) != null && !mPosList.get(currentIndex).equalsIgnoreCase("M")) {

            map.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {

                                         // Use default InfoWindow frame
                                         @Override
                                         public View getInfoWindow(Marker arg0) {
                                             return null;
                                         }

                                         // Defines the contents of the InfoWindow
                                         @Override
                                         public View getInfoContents(Marker arg0) {

                                             // Getting view from the layout file info_window_layout
                                             View v = getLayoutInflater().inflate(R.layout.map_info_txt_layout, null);

                                             // Getting the position from the marker
                                             LatLng latLng = arg0.getPosition();

                                             DisplayMetrics displayMetrics = new DisplayMetrics();
                                             getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
                                             height = displayMetrics.heightPixels;
                                             width = displayMetrics.widthPixels;
                                             LinearLayout layout = (LinearLayout) v.findViewById(R.id.map_info_layout);
                                             LinearLayout.LayoutParams backImageParams = new LinearLayout.LayoutParams(
                                                     LinearLayout.LayoutParams.WRAP_CONTENT,
                                                     LinearLayout.LayoutParams.WRAP_CONTENT);
                                             backImageParams.width = width * 80 / 100;
                                             //backImageParams.height = height * 10 / 100;
                                             backImageParams.gravity = Gravity.CENTER;
                                             layout.setLayoutParams(backImageParams);

                                             // Getting reference to the TextView to set latitude
                                             TextView txtContent = (TextView) v.findViewById(R.id.tv_txt_content);
                                             txtContent.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));

//                    new GetAddressTask(txtContent).execute(geoPoints.get(currentIndex).latitude, geoPoints.get(currentIndex).longitude, 6.0);

                                             StringBuffer addr = new StringBuffer();
                                             Geocoder geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());
                                             List<Address> addresses = null;
                                             try {
                                                 addresses = geocoder.getFromLocation(geoPoints.get(currentIndex).latitude, geoPoints.get(currentIndex).longitude, 1);
                                             } catch (IOException e1) {
                                                 e1.printStackTrace();
                                             } catch (IllegalArgumentException e2) {
                                                 // Error message to post in the log
//                        String errorString = "Illegal arguments " +
//                                Double.toString(params[0]) +
//                                " , " +
//                                Double.toString(params[1]) +
//                                " passed to address service";
//                        e2.printStackTrace();
//                        return errorString;
                                             } catch (NullPointerException np) {
                                                 // TODO Auto-generated catch block
                                                 np.printStackTrace();
                                             }
                                             // If the reverse geocode returned an address
                                             if (addresses != null && addresses.size() > 0) {
                                                 // Get the first address
                                                 Address address = addresses.get(0);
                /*
                 * Format the first line of address (if available),
                 * city, and country name.
                 */
                                                 String addressText = null;

                                                 for (int i = 0; i < address.getMaxAddressLineIndex(); i++) {
                                                     addressText = address.getAddressLine(i);
                                                     addr.append(addressText + ",");
                                                     Log.d("addraddr",""+addr);
                                                 }
                                                 // Return the text
                                                 // return addr.toString();
                                             } else {
                                                 addr.append("No address found");
                                             }


//                    txtContent.setText(addr + " " + cons.getTimefromserver(datetimelist.get(currentIndex)) + " \n Idle Time : " + mIdleTimeList.get(currentIndex));

                                             // System.out.println("The pos is ::::" + mPosList.get(currentIndex) + " and time is ::::::" + mIdleTimeList.get(currentIndex));

//                                             if (mPosList.get(currentIndex).equalsIgnoreCase("P")) {
//                                                 txtContent.setText(addr + " " + cons.getTimefromserver(datetimelist.get(currentIndex)) + " \n Parked Time : " + mIdleTimeList.get(currentIndex));
//                                             } else if (mPosList.get(currentIndex).equalsIgnoreCase("S")) {
//                                                 txtContent.setText(addr + " " + cons.getTimefromserver(datetimelist.get(currentIndex)) + " \n Idle Time : " + mIdleTimeList.get(currentIndex));
//                                             } else {
//                                                 txtContent.setText(addr + " " + cons.getTimefromserver(datetimelist.get(currentIndex)));
//                                             }

                                             String mAddsData = null;
                                             if (mAddsList.get(currentIndex) != null) {
                                                 mAddsData = mAddsList.get(currentIndex);
                                             }

                                             if (mPosList.get(currentIndex).equalsIgnoreCase("P")) {
                                                 txtContent.setText(mAddsData + " " + cons.getTimefromserver(datetimelist.get(currentIndex)) + " \n Parked Time : " + mIdleTimeList.get(currentIndex));
                                             } else if (mPosList.get(currentIndex).equalsIgnoreCase("S")) {
                                                 txtContent.setText(mAddsData + " " + cons.getTimefromserver(datetimelist.get(currentIndex)) + " \n Idle Time : " + mIdleTimeList.get(currentIndex));
                                             } else {
                                                 txtContent.setText(mAddsData + " " + cons.getTimefromserver(datetimelist.get(currentIndex)));
                                             }


                                             if (mSpeedLimit != null && speedlist.get(currentIndex) != null) {
                                                 if (Integer.valueOf(speedlist.get(currentIndex)) > Integer.valueOf(mSpeedLimit)) {
                                                     txtContent.setText(txtContent.getText().toString().trim() + " \n Over Speed");
                                                 }
                                             }

                                             return v;

                                         }
                                     }

            );

//            System.out.println("HIOIIIIIIIII");

            //  System.out.println("The position is :::::::" + mPosList.get(currentIndex));
            //  System.out.println("The current index is :::::" + currentIndex + " and geo points size is :::::" + geoPoints.size());
            MarkerOptions markerOption = new MarkerOptions().position(marker.getPosition());
            // markerOption.title(parse.getshortname());

            if (currentIndex == geoPoints.size() - 1) {

//                ParsingClass parsingClass = new ParsingClass();
//                parsingClass.historyParse(mHistoryResult);
//
//                id_history_distance_value.setText((parsingClass.gettripDistance().equals("null") ? "No Data" : parsingClass.gettripDistance()));
//                //  histroy_start_time_value.setText((parsingClass.getfromDateTime().equals("null") ? "No Data" : parsingClass.getfromDateTime()));
//                id_history_direction_value.setText((parsingClass.gettoDateTime().equals("null") ? "No Data" : parsingClass.gettoDateTime()));

                // System.out.println("The current index is :::::" + currentIndex + " HIIIIIII 1111111");
                ImageView id_custom_marker_icon = (ImageView) mCustomViewMarker.findViewById(R.id.id_custom_marker_icon_for_history);
                ImageView img_mark1 = (ImageView) mCustomViewMarker.findViewById(R.id.id_history_marker_icon);
                ImageView img_mark2 = (ImageView) mCustomViewMarker.findViewById(R.id.id_custom_marker_icon);
                ImageView img_mark3 = (ImageView) mCustomViewMarker.findViewById(R.id.id_vehicle_in_marker);

                img_mark1.setVisibility(View.GONE);
                img_mark2.setVisibility(View.GONE);
                img_mark3.setVisibility(View.GONE);
                id_custom_marker_icon.setVisibility(View.VISIBLE);

//                ImageView img_mark4 = (ImageView) parking_marker.findViewById(R.id.id_vehicle_in_marker);


//                ImageView id_vehicle_in_marker = (ImageView) mCustomViewMarker.findViewById(R.id.id_vehicle_in_marker);
                id_custom_marker_icon.setImageResource(R.drawable.stop);
                markerOption.icon(BitmapDescriptorFactory.fromBitmap(createDrawableFromView(HistoryGoogleMapActivity.this, mCustomViewMarker)));

            } else {

//                id_history_distance_value.setText(distancelist.get(currentIndex));
//                id_history_direction_value.setText(cons.getTimefromserver(datetimelist.get(currentIndex)));
                // System.out.println("HIIIIIIIIIII " + speedlist.get(currentIndex));
                if (mPosList.get(currentIndex).equalsIgnoreCase("P")) {
                    // markerOption.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE));


//                View parking_marker = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.custommarker, null);
                    ImageView id_custom_marker_icon = (ImageView) mCustomViewMarker.findViewById(R.id.id_custom_marker_icon_for_history);
                    ImageView img_mark1 = (ImageView) mCustomViewMarker.findViewById(R.id.id_history_marker_icon);
                    ImageView img_mark2 = (ImageView) mCustomViewMarker.findViewById(R.id.id_custom_marker_icon);
                    ImageView img_mark3 = (ImageView) mCustomViewMarker.findViewById(R.id.id_vehicle_in_marker);

                    img_mark1.setVisibility(View.GONE);
                    img_mark2.setVisibility(View.GONE);
                    img_mark3.setVisibility(View.GONE);
                    id_custom_marker_icon.setVisibility(View.VISIBLE);

//                ImageView img_mark4 = (ImageView) parking_marker.findViewById(R.id.id_vehicle_in_marker);


//                ImageView id_vehicle_in_marker = (ImageView) mCustomViewMarker.findViewById(R.id.id_vehicle_in_marker);
//                    id_custom_marker_icon.setImageResource(R.drawable.parking);
                    id_custom_marker_icon.setImageResource(R.drawable.icon_parking);
                    markerOption.icon(BitmapDescriptorFactory.fromBitmap(createDrawableFromView(HistoryGoogleMapActivity.this, mCustomViewMarker)));

//                markerOption.icon(BitmapDescriptorFactory.fromResource(R.drawable.parking));
                } else if (mPosList.get(currentIndex).equalsIgnoreCase("S")) {
//                    markerOption.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_YELLOW));

                    ImageView id_custom_marker_icon = (ImageView) mCustomViewMarker.findViewById(R.id.id_custom_marker_icon_for_history);
                    ImageView img_mark1 = (ImageView) mCustomViewMarker.findViewById(R.id.id_history_marker_icon);
                    ImageView img_mark2 = (ImageView) mCustomViewMarker.findViewById(R.id.id_custom_marker_icon);
                    ImageView img_mark3 = (ImageView) mCustomViewMarker.findViewById(R.id.id_vehicle_in_marker);

                    img_mark1.setVisibility(View.GONE);
                    img_mark2.setVisibility(View.GONE);
                    img_mark3.setVisibility(View.GONE);
                    id_custom_marker_icon.setVisibility(View.VISIBLE);

                    id_custom_marker_icon.setImageResource(R.drawable.icon_idle);
                    markerOption.icon(BitmapDescriptorFactory.fromBitmap(createDrawableFromView(HistoryGoogleMapActivity.this, mCustomViewMarker)));

                } else {
                    if (mSpeedLimit != null && speedlist.get(currentIndex) != null) {
                        if (Integer.valueOf(speedlist.get(currentIndex)) > Integer.valueOf(mSpeedLimit)) {
                            markerOption.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));

                        }
                    }
                }
            }

            //  markerOption.icon(BitmapDescriptorFactory.fromBitmap(createDrawableFromView(HistoryActivity.this, marker)));
            // if (current_vehicle_location_marker.isEmpty()) {
            Marker currentMarker = map.addMarker(markerOption);

//            Marker marker1 = map.addMarker(new MarkerOptions()
//                    .position(marker.getPosition())
//                    .title("Title")
//                    .snippet("Snippet")
//                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_YELLOW)));
//            System.out.println("Marker option inserting laocation is :::::" + markerOption.getPosition());
            mMarkersHashMap.put(currentMarker, markerOption);
            mMarkersPositionHashMap.put(currentMarker, currentIndex);
            currentMarker.showInfoWindow();

        } else {

            if (currentIndex == geoPoints.size() - 1) {
                MarkerOptions markerOption = new MarkerOptions().position(marker.getPosition());

                map.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {

                                             // Use default InfoWindow frame
                                             @Override
                                             public View getInfoWindow(Marker arg0) {
                                                 return null;
                                             }

                                             // Defines the contents of the InfoWindow
                                             @Override
                                             public View getInfoContents(Marker arg0) {

                                                 // Getting view from the layout file info_window_layout
                                                 View v = getLayoutInflater().inflate(R.layout.map_info_txt_layout, null);

                                                 // Getting the position from the marker
                                                 LatLng latLng = arg0.getPosition();

                                                 DisplayMetrics displayMetrics = new DisplayMetrics();
                                                 getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
                                                 height = displayMetrics.heightPixels;
                                                 width = displayMetrics.widthPixels;
                                                 LinearLayout layout = (LinearLayout) v.findViewById(R.id.map_info_layout);
                                                 LinearLayout.LayoutParams backImageParams = new LinearLayout.LayoutParams(
                                                         LinearLayout.LayoutParams.WRAP_CONTENT,
                                                         LinearLayout.LayoutParams.WRAP_CONTENT);
                                                 backImageParams.width = width * 80 / 100;
                                                 //backImageParams.height = height * 10 / 100;
                                                 backImageParams.gravity = Gravity.CENTER;
                                                 layout.setLayoutParams(backImageParams);

                                                 // Getting reference to the TextView to set latitude
                                                 TextView txtContent = (TextView) v.findViewById(R.id.tv_txt_content);
                                                 txtContent.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));

//                    new GetAddressTask(txtContent).execute(geoPoints.get(currentIndex).latitude, geoPoints.get(currentIndex).longitude, 6.0);

                                                 StringBuffer addr = new StringBuffer();
                                                 Geocoder geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());
                                                 List<Address> addresses = null;
                                                 try {
                                                     addresses = geocoder.getFromLocation(geoPoints.get(currentIndex).latitude, geoPoints.get(currentIndex).longitude, 1);
                                                 } catch (IOException e1) {
                                                     e1.printStackTrace();
                                                 } catch (IllegalArgumentException e2) {
                                                     // Error message to post in the log
//                        String errorString = "Illegal arguments " +
//                                Double.toString(params[0]) +
//                                " , " +
//                                Double.toString(params[1]) +
//                                " passed to address service";
//                        e2.printStackTrace();
//                        return errorString;
                                                 } catch (NullPointerException np) {
                                                     // TODO Auto-generated catch block
                                                     np.printStackTrace();
                                                 }
                                                 // If the reverse geocode returned an address
                                                 if (addresses != null && addresses.size() > 0) {
                                                     // Get the first address
                                                     Address address = addresses.get(0);
                /*
                 * Format the first line of address (if available),
                 * city, and country name.
                 */
                                                     String addressText = null;

                                                     for (int i = 0; i < address.getMaxAddressLineIndex(); i++) {
                                                         addressText = address.getAddressLine(i);
                                                         addr.append(addressText + ",");
                                                     }
                                                     // Return the text
                                                     // return addr.toString();
                                                 } else {
                                                     addr.append("No address found");
                                                 }


//                    txtContent.setText(addr + " " + cons.getTimefromserver(datetimelist.get(currentIndex)) + " \n Idle Time : " + mIdleTimeList.get(currentIndex));

                                                 // System.out.println("The pos is ::::" + mPosList.get(currentIndex) + " and time is ::::::" + mIdleTimeList.get(currentIndex));

//                                                 if (mPosList.get(currentIndex).equalsIgnoreCase("P")) {
//                                                     txtContent.setText(addr + " " + cons.getTimefromserver(datetimelist.get(currentIndex)) + " \n Parked Time : " + mIdleTimeList.get(currentIndex));
//                                                 } else if (mPosList.get(currentIndex).equalsIgnoreCase("S")) {
//                                                     txtContent.setText(addr + " " + cons.getTimefromserver(datetimelist.get(currentIndex)) + " \n Idle Time : " + mIdleTimeList.get(currentIndex));
//                                                 } else {
//                                                     txtContent.setText(addr + " " + cons.getTimefromserver(datetimelist.get(currentIndex)));
//                                                 }


                                                 String mAddsData = null;
                                                 if (mAddsList.get(currentIndex) != null) {
                                                     mAddsData = mAddsList.get(currentIndex);
                                                 }
                                                 Log.d("mAddsData",""+mAddsList.get(currentIndex));

                                                 if (mPosList.get(currentIndex).equalsIgnoreCase("P")) {
                                                     txtContent.setText(mAddsData + " " + cons.getTimefromserver(datetimelist.get(currentIndex)) + " \n Parked Time : " + mIdleTimeList.get(currentIndex));
                                                 } else if (mPosList.get(currentIndex).equalsIgnoreCase("S")) {
                                                     txtContent.setText(mAddsData + " " + cons.getTimefromserver(datetimelist.get(currentIndex)) + " \n Idle Time : " + mIdleTimeList.get(currentIndex));
                                                 } else {
                                                     txtContent.setText(mAddsData + " " + cons.getTimefromserver(datetimelist.get(currentIndex)));
                                                 }


                                                 if (mSpeedLimit != null && speedlist.get(currentIndex) != null) {
                                                     if (Integer.valueOf(speedlist.get(currentIndex)) > Integer.valueOf(mSpeedLimit)) {
                                                         txtContent.setText(txtContent.getText().toString().trim() + " \n Over Speed");
                                                     }
                                                 }

                                                 return v;

                                             }
                                         }

                );


//                ParsingClass parsingClass = new ParsingClass();
//                parsingClass.historyParse(mHistoryResult);
//
//                id_history_distance_value.setText((parsingClass.gettripDistance().equals("null") ? "No Data" : parsingClass.gettripDistance()));
//                //  histroy_start_time_value.setText((parsingClass.getfromDateTime().equals("null") ? "No Data" : parsingClass.getfromDateTime()));
//                id_history_direction_value.setText((parsingClass.gettoDateTime().equals("null") ? "No Data" : parsingClass.gettoDateTime()));

//                System.out.println("The current index is :::::" + currentIndex + " HIIIIIII 1111111");
                ImageView id_custom_marker_icon = (ImageView) mCustomViewMarker.findViewById(R.id.id_custom_marker_icon_for_history);
                ImageView img_mark1 = (ImageView) mCustomViewMarker.findViewById(R.id.id_history_marker_icon);
                ImageView img_mark2 = (ImageView) mCustomViewMarker.findViewById(R.id.id_custom_marker_icon);
                ImageView img_mark3 = (ImageView) mCustomViewMarker.findViewById(R.id.id_vehicle_in_marker);

                img_mark1.setVisibility(View.GONE);
                img_mark2.setVisibility(View.GONE);
                img_mark3.setVisibility(View.GONE);
                id_custom_marker_icon.setVisibility(View.VISIBLE);

//                ImageView img_mark4 = (ImageView) parking_marker.findViewById(R.id.id_vehicle_in_marker);


//                ImageView id_vehicle_in_marker = (ImageView) mCustomViewMarker.findViewById(R.id.id_vehicle_in_marker);
                id_custom_marker_icon.setImageResource(R.drawable.stop);
                markerOption.icon(BitmapDescriptorFactory.fromBitmap(createDrawableFromView(HistoryGoogleMapActivity.this, mCustomViewMarker)));
                Marker currentMarker = map.addMarker(markerOption);

//            Marker marker1 = map.addMarker(new MarkerOptions()
//                    .position(marker.getPosition())
//                    .title("Title")
//                    .snippet("Snippet")
//                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_YELLOW)));

//                System.out.println("Marker option inserting laocation is :::::" + markerOption.getPosition());

                mMarkersHashMap.put(currentMarker, markerOption);
                mMarkersPositionHashMap.put(currentMarker, currentIndex);
                currentMarker.showInfoWindow();
            } else {
                if (mSpeedLimit != null && speedlist.get(currentIndex) != null) {
                    if (Integer.valueOf(speedlist.get(currentIndex)) > Integer.valueOf(mSpeedLimit)) {
                        MarkerOptions markerOption = new MarkerOptions().position(marker.getPosition());

                        map.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {

                                                     // Use default InfoWindow frame
                                                     @Override
                                                     public View getInfoWindow(Marker arg0) {
                                                         return null;
                                                     }

                                                     // Defines the contents of the InfoWindow
                                                     @Override
                                                     public View getInfoContents(Marker arg0) {

                                                         // Getting view from the layout file info_window_layout
                                                         View v = getLayoutInflater().inflate(R.layout.map_info_txt_layout, null);

                                                         // Getting the position from the marker
                                                         LatLng latLng = arg0.getPosition();

                                                         DisplayMetrics displayMetrics = new DisplayMetrics();
                                                         getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
                                                         height = displayMetrics.heightPixels;
                                                         width = displayMetrics.widthPixels;
                                                         LinearLayout layout = (LinearLayout) v.findViewById(R.id.map_info_layout);
                                                         LinearLayout.LayoutParams backImageParams = new LinearLayout.LayoutParams(
                                                                 LinearLayout.LayoutParams.WRAP_CONTENT,
                                                                 LinearLayout.LayoutParams.WRAP_CONTENT);
                                                         backImageParams.width = width * 80 / 100;
                                                         //backImageParams.height = height * 10 / 100;
                                                         backImageParams.gravity = Gravity.CENTER;
                                                         layout.setLayoutParams(backImageParams);

                                                         // Getting reference to the TextView to set latitude
                                                         TextView txtContent = (TextView) v.findViewById(R.id.tv_txt_content);
                                                         txtContent.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));

//                    new GetAddressTask(txtContent).execute(geoPoints.get(currentIndex).latitude, geoPoints.get(currentIndex).longitude, 6.0);

                                                         StringBuffer addr = new StringBuffer();
                                                         Geocoder geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());
                                                         List<Address> addresses = null;
                                                         try {
                                                             addresses = geocoder.getFromLocation(geoPoints.get(currentIndex).latitude, geoPoints.get(currentIndex).longitude, 1);
                                                         } catch (IOException e1) {
                                                             e1.printStackTrace();
                                                         } catch (IllegalArgumentException e2) {
                                                             // Error message to post in the log
//                        String errorString = "Illegal arguments " +
//                                Double.toString(params[0]) +
//                                " , " +
//                                Double.toString(params[1]) +
//                                " passed to address service";
//                        e2.printStackTrace();
//                        return errorString;
                                                         } catch (NullPointerException np) {
                                                             // TODO Auto-generated catch block
                                                             np.printStackTrace();
                                                         }
                                                         // If the reverse geocode returned an address
                                                         if (addresses != null && addresses.size() > 0) {
                                                             // Get the first address
                                                             Address address = addresses.get(0);
                /*
                 * Format the first line of address (if available),
                 * city, and country name.
                 */
                                                             String addressText = null;

                                                             for (int i = 0; i < address.getMaxAddressLineIndex(); i++) {
                                                                 addressText = address.getAddressLine(i);
                                                                 addr.append(addressText + ",");
                                                             }
                                                             // Return the text
                                                             // return addr.toString();
                                                         } else {
                                                             addr.append("No address found");
                                                         }


//                    txtContent.setText(addr + " " + cons.getTimefromserver(datetimelist.get(currentIndex)) + " \n Idle Time : " + mIdleTimeList.get(currentIndex));

                                                         // System.out.println("The pos is ::::" + mPosList.get(currentIndex) + " and time is ::::::" + mIdleTimeList.get(currentIndex));

//                                                         if (mPosList.get(currentIndex).equalsIgnoreCase("P")) {
//                                                             txtContent.setText(addr + " " + cons.getTimefromserver(datetimelist.get(currentIndex)) + " \n Parked Time : " + mIdleTimeList.get(currentIndex));
//                                                         } else if (mPosList.get(currentIndex).equalsIgnoreCase("S")) {
//                                                             txtContent.setText(addr + " " + cons.getTimefromserver(datetimelist.get(currentIndex)) + " \n Idle Time : " + mIdleTimeList.get(currentIndex));
//                                                         } else {
//                                                             txtContent.setText(addr + " " + cons.getTimefromserver(datetimelist.get(currentIndex)));
//                                                         }

                                                         String mAddsData = null;
                                                         if (mAddsList.get(currentIndex) != null) {
                                                             mAddsData = mAddsList.get(currentIndex);
                                                         }

                                                         if (mPosList.get(currentIndex).equalsIgnoreCase("P")) {
                                                             txtContent.setText(mAddsData + " " + cons.getTimefromserver(datetimelist.get(currentIndex)) + " \n Parked Time : " + mIdleTimeList.get(currentIndex));
                                                         } else if (mPosList.get(currentIndex).equalsIgnoreCase("S")) {
                                                             txtContent.setText(mAddsData + " " + cons.getTimefromserver(datetimelist.get(currentIndex)) + " \n Idle Time : " + mIdleTimeList.get(currentIndex));
                                                         } else {
                                                             txtContent.setText(mAddsData + " " + cons.getTimefromserver(datetimelist.get(currentIndex)));
                                                         }

                                                         if (mSpeedLimit != null && speedlist.get(currentIndex) != null) {
                                                             if (Integer.valueOf(speedlist.get(currentIndex)) > Integer.valueOf(mSpeedLimit)) {
                                                                 txtContent.setText(txtContent.getText().toString().trim() + " \n Over Speed");
                                                             }
                                                         }

                                                         return v;

                                                     }
                                                 }

                        );


//                ParsingClass parsingClass = new ParsingClass();
//                parsingClass.historyParse(mHistoryResult);
//
//                id_history_distance_value.setText((parsingClass.gettripDistance().equals("null") ? "No Data" : parsingClass.gettripDistance()));
//                //  histroy_start_time_value.setText((parsingClass.getfromDateTime().equals("null") ? "No Data" : parsingClass.getfromDateTime()));
//                id_history_direction_value.setText((parsingClass.gettoDateTime().equals("null") ? "No Data" : parsingClass.gettoDateTime()));

//                        System.out.println("The current index is :::::" + currentIndex + " HIIIIIII 1111111");
                        ImageView id_custom_marker_icon = (ImageView) mCustomViewMarker.findViewById(R.id.id_custom_marker_icon_for_history);
                        ImageView img_mark1 = (ImageView) mCustomViewMarker.findViewById(R.id.id_history_marker_icon);
                        ImageView img_mark2 = (ImageView) mCustomViewMarker.findViewById(R.id.id_custom_marker_icon);
                        ImageView img_mark3 = (ImageView) mCustomViewMarker.findViewById(R.id.id_vehicle_in_marker);

                        img_mark1.setVisibility(View.GONE);
                        img_mark2.setVisibility(View.GONE);
                        img_mark3.setVisibility(View.GONE);
                        id_custom_marker_icon.setVisibility(View.VISIBLE);

//                ImageView img_mark4 = (ImageView) parking_marker.findViewById(R.id.id_vehicle_in_marker);


//                ImageView id_vehicle_in_marker = (ImageView) mCustomViewMarker.findViewById(R.id.id_vehicle_in_marker);
                        id_custom_marker_icon.setImageResource(R.drawable.over_speed_new_icon);
                        markerOption.icon(BitmapDescriptorFactory.fromBitmap(createDrawableFromView(HistoryGoogleMapActivity.this, mCustomViewMarker)));
//                        markerOption.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
                        Marker currentMarker = map.addMarker(markerOption);

//            Marker marker1 = map.addMarker(new MarkerOptions()
//                    .position(marker.getPosition())
//                    .title("Title")
//                    .snippet("Snippet")
//                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_YELLOW)));

//                        System.out.println("Marker option inserting laocation is :::::" + markerOption.getPosition());

                        mMarkersHashMap.put(currentMarker, markerOption);
                        mMarkersPositionHashMap.put(currentMarker, currentIndex);
                        currentMarker.showInfoWindow();
//                        currentMarker.setVisible(false);
                    }
                }
            }


        }

        if (currentIndex == geoPoints.size() - 1)

        {

//            System.out.println("the current index is for replay icon :::::");

            play_icon.setVisibility(View.GONE);
            pause_icon.setVisibility(View.GONE);
            replay_icon.setVisibility(View.VISIBLE);
            currentIndex = 0;
        }

        //Utils.bounceMarker(googleMap, marker);

        this.selectedMarker = marker;
    }

//    private void resetMarkers() {
//        for (Marker marker : this.markers) {
//            marker.setIcon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
//        }
//    }

    public void showCurrentAddress() {
        current_address__dialog = new Dialog(HistoryGoogleMapActivity.this);
        current_address__dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        current_address__dialog.setContentView(R.layout.custom_marker_dialog);
        current_address__dialog.setCancelable(true);
        TextView id_historyplayback_parkedtime_text_labelid = (TextView) current_address__dialog.findViewById(R.id.id_historyplayback_parkedtime_text_labelid);
        TextView id_view_parkedtime = (TextView) current_address__dialog.findViewById(R.id.id_view_parkedtime);
        TextView id_historyplayback_parkedduration_text_labelid = (TextView) current_address__dialog.findViewById(R.id.id_historyplayback_parkedduration_text_labelid);
        TextView id_view_parkedduration = (TextView) current_address__dialog.findViewById(R.id.id_view_parkedduration);
        TextView id_historyplayback_addres_text_labelid = (TextView) current_address__dialog.findViewById(R.id.id_historyplayback_addres_text_labelid);
        TextView id_view_address = (TextView) current_address__dialog.findViewById(R.id.id_view_address);
        id_historyplayback_parkedtime_text_labelid.setVisibility(View.GONE);
        id_view_parkedtime.setVisibility(View.GONE);
        id_historyplayback_parkedduration_text_labelid.setVisibility(View.GONE);
        id_view_parkedduration.setVisibility(View.GONE);
        id_historyplayback_addres_text_labelid.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        id_view_address.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        Log.d("geolatitide",""+geoPoints.get(currentIndex).latitude);
        Log.d("geolongitude",""+geoPoints.get(currentIndex).longitude);
        new GetAddressTask(id_view_address).execute(geoPoints.get(currentIndex).latitude, geoPoints.get(currentIndex).longitude, 6.0);
        current_address__dialog.show();
    }

    public void historyInfoPopupWindow() {


        current_address__dialog = new Dialog(HistoryGoogleMapActivity.this);
        current_address__dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        current_address__dialog.setContentView(R.layout.custom_marker_dialog);
        current_address__dialog.setCancelable(true);
        TextView id_historyplayback_parkedtime_text_labelid = (TextView) current_address__dialog.findViewById(R.id.id_historyplayback_parkedtime_text_labelid);
        TextView id_view_parkedtime = (TextView) current_address__dialog.findViewById(R.id.id_view_parkedtime);
        TextView id_historyplayback_parkedduration_text_labelid = (TextView) current_address__dialog.findViewById(R.id.id_historyplayback_parkedduration_text_labelid);
        TextView id_view_parkedduration = (TextView) current_address__dialog.findViewById(R.id.id_view_parkedduration);
        TextView id_historyplayback_addres_text_labelid = (TextView) current_address__dialog.findViewById(R.id.id_historyplayback_addres_text_labelid);
        TextView id_view_address = (TextView) current_address__dialog.findViewById(R.id.id_view_address);
        id_historyplayback_parkedtime_text_labelid.setVisibility(View.GONE);
        id_view_parkedtime.setVisibility(View.GONE);
        id_historyplayback_parkedduration_text_labelid.setVisibility(View.GONE);
        id_view_parkedduration.setVisibility(View.GONE);
        id_historyplayback_addres_text_labelid.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        id_view_address.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));

      //  new GetAddressTask(id_view_address).execute(geoPoints.get(currentIndex).latitude, geoPoints.get(currentIndex).longitude, 6.0);
        new GetAddressTask(id_view_address).execute(geoPoints.get(currentIndex).latitude, geoPoints.get(currentIndex).longitude, 6.0);
        current_address__dialog.show();


    }

    int mPopupStartCount = 1, mPopupEndCount = 1, mHistoryStartCount = 1, mHistoryEndCount = 1, mShowAddsPopUpCount = 1;

    private class GetAddressTask extends AsyncTask<Double, String, String> {
        TextView currenta_adddress_view;
        double getlat, getlng;
        double address_type;

        public GetAddressTask(TextView txtview) {
            super();
            currenta_adddress_view = txtview;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        /**
         * Get a Geocoder instance, get the latitude and longitude
         * look up the address, and return it
         *
         * @return A string containing the address of the current
         * location, or an empty string if no address can be found,
         * or an error message
         * @params params One or more Location objects
         */
        @Override
        protected String doInBackground(Double... params) {
            getlat = params[0];
            getlng = params[1];
            address_type = params[2];
            Geocoder geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());
            List<Address> addresses = null;

//            Constant.getAddress(String.valueOf(params[0]), String.valueOf(params[1]));

            try {
                addresses = geocoder.getFromLocation(params[0], params[1], 5);
            } catch (IOException e1) {
                e1.printStackTrace();
            } catch (IllegalArgumentException e2) {
                // Error message to post in the log
                String errorString = "Illegal arguments " +
                        Double.toString(params[0]) +
                        " , " +
                        Double.toString(params[1]) +
                        " passed to address service";
                e2.printStackTrace();
                return errorString;
            } catch (NullPointerException np) {
                // TODO Auto-generated catch block
                np.printStackTrace();
            }
            // If the reverse geocode returned an address
            if (addresses != null && addresses.size() > 0) {
                // Get the first address
                Address address = addresses.get(0);
                /*
                 * Format the first line of address (if available),
                 * city, and country name.
                 */
                String addressText = null;
                StringBuffer addr = new StringBuffer();
                for (int i = 0; i < address.getMaxAddressLineIndex(); i++) {
                    addressText = address.getAddressLine(i);
                    addr.append(addressText + ",");
                }
                // Return the text

                if (addr != null && addr.length() > 0) {
                    return addr.toString();
                } else {
                    return "No address found";
                }
            } else {
                return "No address found";
            }
        }

        protected void onProgressUpdate(String... progress) {
        }

        protected void onPostExecute(String result) {
            // sourceedit.setText(result);

            System.out.println("Hi geocoder address " + address_type + " " + result);

            if (result != null && !result.isEmpty()) {
//                if (!result.equals("IO Exception trying to get address") || !result.equals("No address found")) {
                /***
                 * 1.0 - vehicles current address when a vehicle is clicked
                 * 2.0 - vehicles origin address in history selection
                 * 3.0 - vehicles destination address in history selection
                 * 6.0 - vehicles current address when pause button is clicked in history selection
                 */

//                    System.out.println("The address is ::::" + result);

                if (address_type == 1.0) {
                    SharedPreferences.Editor editor = sp.edit();
                    editor.putString("address", result);
                    editor.commit();
                    currenta_adddress_view.setText(result.equals("null") ? "No Data" : result);
                } else if (address_type == 2.0) {
                    SharedPreferences.Editor editor = sp.edit();
                    editor.putString("vehicle_origin_address", result);
                    editor.commit();
                    history_start_location_value.setText(result.equals("null") ? "No Data" : result);

                    if (result.trim().contains("No address found") || result.equals("IO Exception trying to get address")) {
                        if (mHistoryStartCount < 5) {
                            new GetAddressTask(history_start_location_value).execute(mPopupStartLat, mPopupStartLng, 2.0);
                        }
                        mHistoryStartCount++;
                    }

                } else if (address_type == 3.0) {
                    SharedPreferences.Editor editor = sp.edit();
                    editor.putString("vehicle_destination_address", result);
                    editor.commit();
                    history_end_location_value.setText(result.equals("null") ? "No Data" : result);

                    if (result.trim().contains("No address found") || result.equals("IO Exception trying to get address")) {
                        if (mHistoryEndCount < 5) {
                            new GetAddressTask(history_end_location_value).execute(mPopupEndLat, mPopupEndLng, 3.0);
                        }
                        mHistoryEndCount++;
                    }

                } else if (address_type == 4.0) {
                    SharedPreferences.Editor editor = sp.edit();
                    editor.putString("current_vehicle_address", result);
                    editor.commit();

                    if (mPopupStartLat != 0 && mPopupStartLng != 0) {
                        currenta_adddress_view.setText(result.equals("null") ? "No Data" : result);
                        history_start_location_value.setText(result.equals("null") ? "No Data" : result);
                        if (result.trim().contains("No address found") || result.equals("IO Exception trying to get address")) {
                            if (mPopupStartCount < 5) {
                                new GetAddressTask(currenta_adddress_view).execute(mPopupStartLat, mPopupStartLng, 4.0);

                            } else {
                                new GetAddressTaskNew(currenta_adddress_view).execute(mPopupStartLat, mPopupStartLng, 4.0);
                            }
                            mPopupStartCount++;
                        }
                    } else {
                        currenta_adddress_view.setText("Start location not available");
                        history_start_location_value.setText("Start location not available");
                    }


                } else if (address_type == 5.0) {

                    SharedPreferences.Editor editor = sp.edit();
                    editor.putString("current_vehicle_address", result);
                    editor.commit();
                    if (mPopupEndLat != 0 && mPopupEndLng != 0) {
                        currenta_adddress_view.setText(result.equals("null") ? "No Data" : result);
                        history_end_location_value.setText(result.equals("null") ? "No Data" : result);
                        if (result.trim().contains("No address found") || result.equals("IO Exception trying to get address")) {
                            if (mPopupEndCount < 5) {
                                new GetAddressTask(currenta_adddress_view).execute(mPopupEndLat, mPopupEndLng, 5.0);
                            } else {
                                new GetAddressTaskNew(currenta_adddress_view).execute(mPopupEndLat, mPopupEndLng, 5.0);
                            }
                            mPopupEndCount++;
                        }
                    } else {
                        currenta_adddress_view.setText("End location not available");
                        history_end_location_value.setText("End location not available");
                    }


                } else if (address_type == 6.0) {
                    currenta_adddress_view.setText(result.equals("null") ? "No Data" : result);

                    if (result.trim().contains("No address found") || result.equals("IO Exception trying to get address")) {
                        if (mShowAddsPopUpCount < 5) {
                            new GetAddressTask(currenta_adddress_view).execute(getlat, getlng, 6.0);
                        } else {
                            new GetAddressTaskNew(currenta_adddress_view).execute(getlat, getlng, 6.0);
                        }
                        mShowAddsPopUpCount++;
                    }

                }
            }
//            } else {
//                Toast empty_fav = Toast.makeText(getApplicationContext(), "Please Try Again", Toast.LENGTH_LONG);
//                empty_fav.show();
//            }
        }
    }

    int mPopupStartCountNew = 1, mPopupEndCountNew = 1, mHistoryStartCountNew = 1, mHistoryEndCountNew = 1, mShowAddsPopUpCountNew = 1;

    private class GetAddressTaskNew extends AsyncTask<Double, String, String> {
        TextView currenta_adddress_view;
        double getlat, getlng;
        double address_type;

        public GetAddressTaskNew(TextView txtview) {
            super();
            currenta_adddress_view = txtview;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        /**
         * Get a Geocoder instance, get the latitude and longitude
         * look up the address, and return it
         *
         * @return A string containing the address of the current
         * location, or an empty string if no address can be found,
         * or an error message
         * @params params One or more Location objects
         */
        @Override
        protected String doInBackground(Double... params) {

            address_type = params[2];
            String addr = null;
            try {
                addr = Constant.getAddress(String.valueOf(params[0]), String.valueOf(params[1]),getStdTableValue("appid"),getString(R.string.address_api));
            } catch (Exception e1) {
                e1.printStackTrace();
            }


            if (addr != null && addr.length() > 0) {
                return addr.toString();
            } else {
                return "No address found";
            }

        }

        protected void onProgressUpdate(String... progress) {
        }

        protected void onPostExecute(String result) {
            // sourceedit.setText(result);

            System.out.println("Hi google address address22 " + address_type + " " + result);

            if (result != null && !result.isEmpty()) {
                if (!result.equals("IO Exception trying to get address") || !result.equals("No address found")) {


                    if (address_type == 1.0) {
                        SharedPreferences.Editor editor = sp.edit();
                        editor.putString("address", result);
                        editor.commit();
                        currenta_adddress_view.setText(result.equals("null") ? "No Data" : result);
                    } else if (address_type == 2.0) {
                        SharedPreferences.Editor editor = sp.edit();
                        editor.putString("vehicle_origin_address", result);
                        editor.commit();
                        history_start_location_value.setText(result.equals("null") ? "No Data" : result);


                    } else if (address_type == 3.0) {
                        SharedPreferences.Editor editor = sp.edit();
                        editor.putString("vehicle_destination_address", result);
                        editor.commit();
                        history_end_location_value.setText(result.equals("null") ? "No Data" : result);


                    } else if (address_type == 4.0) {
                        SharedPreferences.Editor editor = sp.edit();
                        editor.putString("current_vehicle_address", result);
                        editor.commit();
                        currenta_adddress_view.setText(result.equals("null") ? "No Data" : result);
                        history_start_location_value.setText(result.equals("null") ? "No Data" : result);

                        if (result.trim().contains("No address found") || result.equals("IO Exception trying to get address")) {
                            if (mPopupStartCountNew < 5) {
//                                new GetAddressTask(currenta_adddress_view).execute(mPopupStartLat, mPopupStartLng, 4.0);
//
//                            } else {
                                new GetAddressTaskNew(currenta_adddress_view).execute(mPopupStartLat, mPopupStartLng, 4.0);
                            }
                            mPopupStartCountNew++;
                        }


                    } else if (address_type == 5.0) {

                        SharedPreferences.Editor editor = sp.edit();
                        editor.putString("current_vehicle_address", result);
                        editor.commit();

                        currenta_adddress_view.setText(result.equals("null") ? "No Data" : result);
                        history_end_location_value.setText(result.equals("null") ? "No Data" : result);


                        if (result.trim().contains("No address found") || result.equals("IO Exception trying to get address")) {
                            if (mPopupEndCountNew < 5) {
//                                new GetAddressTask(currenta_adddress_view).execute(mPopupEndLat, mPopupEndLng, 5.0);
//                            } else {
                                new GetAddressTaskNew(currenta_adddress_view).execute(mPopupEndLat, mPopupEndLng, 5.0);
                            }
                            mPopupEndCountNew++;
                        }


                    } else if (address_type == 6.0) {
                        currenta_adddress_view.setText(result.equals("null") ? "No Data" : result);

                        if (result.trim().contains("No address found") || result.equals("IO Exception trying to get address")) {
                            if (mShowAddsPopUpCountNew < 5) {
                                new GetAddressTaskNew(currenta_adddress_view).execute(getlat, getlng, 6.0);
                            } else {

                            }
                            mShowAddsPopUpCountNew++;
                        }


                    }

                }
            } else {
                Toast empty_fav = Toast.makeText(getApplicationContext(), "Please Try Again", Toast.LENGTH_LONG);
                empty_fav.show();
            }
        }
    }

    public class GetVehicleHistory extends AsyncTask<String, Integer, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = MyCustomProgressDialog.ctor(HistoryGoogleMapActivity.this);
            progressDialog.show();
        }

        protected void onProgressUpdate(Integer... values) {
            progressDialog.show();
        }

        @Override
        public String doInBackground(String... urls) {
            String response_from_server = "";
            if (urls[0].equals("replay")) {
                response_from_server = sp.getString("historyplaybackdata", "");
            } else {
                try {
                    HttpConfig ht = new HttpConfig();
                    response_from_server = ht.httpGet(urls[0]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            return response_from_server;
        }

        @Override
        public void onPostExecute(String result) {
            if (result != null && !result.isEmpty()) {
                SharedPreferences.Editor editor = sp.edit();
                editor.putString("historyplaybackdata", result);
                editor.commit();
                historyPlayBack(result, progressDialog);
                historyRepeat = result;
            } else {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
//                header_layout.setVisibility(View.VISIBLE);
//                vehicle_info_layout.setVisibility(View.VISIBLE);
//                id_map_info_layout.setVisibility(View.GONE);
//                home_button_functionality();
                startActivity(new Intent(HistoryGoogleMapActivity.this, VehicleListActivity.class));
                finish();
                Toast.makeText(getApplicationContext(), "No History Data Available", Toast.LENGTH_SHORT).show();
            }
        }
    }


    public class GetPOIInformation extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        public String doInBackground(String... urls) {
            String response_from_server = "";
            try {

//                System.out.println("The POI url is :::::" + urls[0]);
                HttpConfig ht = new HttpConfig();
                response_from_server = ht.httpGet(urls[0]);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return response_from_server;
        }

        @Override
        public void onPostExecute(String result) {
            SharedPreferences.Editor editor = sp.edit();
            editor.putString("poidata", result);
            editor.commit();
            drawPoiPoints(result);
        }

    }

    public void drawPoiPoints(String poi_data) {
        if (poi_data != null && poi_data.length() != 0) {
            try {
                JSONObject parentobject = new JSONObject(poi_data);
                if (!parentobject.isNull("geoFence")) {
                    int vehicles_history_length;
                    final ArrayList<LatLng> points = new ArrayList<>();
                    JSONArray vehiclearray = parentobject.getJSONArray("geoFence");
                    vehicles_history_length = vehiclearray.length();
                    for (int z = 0; z < vehicles_history_length; z++) {
                        if (!vehiclearray.get(z).equals(null)) {
                            ParsingClass parse = new ParsingClass();
                            parse.getPoiInformation(vehiclearray.getJSONObject(z).toString());
                            double lat = parse.getpoiLatitude();
                            double lng = parse.getpoiLongitude();
                            LatLng position = new LatLng(lat, lng);

//                            System.out.println("The poi points are :::::" + lat + " " + lng);

                            points.add(position);
                            View marker = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.custompoimarker, null);
                            MarkerOptions markerOption = new MarkerOptions().position(new LatLng(lat, lng));
                            markerOption.icon(BitmapDescriptorFactory.fromBitmap(createDrawableFromView(HistoryGoogleMapActivity.this, marker)));
                            Marker currentMarker = map.addMarker(markerOption);
                        }
                    }
                }
            } catch (JSONException je) {
                je.printStackTrace();
            }
        }
    }

    public void historyPlayBack(final String result, final ProgressDialog dialog) {
//        runOnUiThread(new Runnable() {
//            @Override
//            public void run() {
        String totalrows = null;
        if (result != null && result.length() != 0) {
            mHistoryResult = result;
            if (map != null) {
                map.clear();
            }
            //  vehicle_info_layout.setVisibility(View.GONE);
            // id_refresh_tracking_icon.setVisibility(View.GONE);
            //  id_refresh_countdowntimer.setVisibility(View.GONE);
            history_detail_layout.setVisibility(View.VISIBLE);
            id_history_show_min_layout.setVisibility(View.GONE);
            pause_icon.setVisibility(View.GONE);
            play_icon.setVisibility(View.GONE);
            // header_layout.setVisibility(View.GONE);
//            if (zoom_location_marker != null) {
//                zoom_location_marker.clear();
//            }
            try {

                if (mMarkersHashMap != null) {
                    mMarkersHashMap.clear();
                }

                if (mMarkersPositionHashMap != null) {
                    mMarkersPositionHashMap.clear();
                }

                ParsingClass parsingClass = new ParsingClass();
                parsingClass.historyParse(result);
                int vehicles_history_length;
                geoPoints = new ArrayList<LatLng>();
                speedlist = new ArrayList<String>();
                datetimelist = new ArrayList<String>();
                distancelist = new ArrayList<String>();
                mIdleTimeList = new ArrayList<String>();
                if (!geoPoints.isEmpty()) {
                    geoPoints.clear();
                    //  directionlist.clear();
                    mPosList.clear();
                    speedlist.clear();
                    datetimelist.clear();
                    distancelist.clear();
                    mIdleTimeList.clear();
                    mAddsList.clear();
                }
                String vehicle_short_name;
                PolylineOptions options = new PolylineOptions().width(8);
                PolylineOptions overspeed_options = new PolylineOptions().width(4).geodesic(true);
                vehicle_short_name = parsingClass.getshortname();
                id_show_history_vehicleid.setText((parsingClass.getshortname().equals("null") ? "No Data" : parsingClass.getshortname()));

                // System.out.println("The live tracking vehicle type ::::" + parsingClass.getvehicleType());

                  Log.d("vehiclehistory",""+parsingClass.getvehicleType());
//                if (parsingClass.getvehicleType().equalsIgnoreCase("Bus")) {
//                    id_show_vehicle_info_icon.setImageResource(R.drawable.bus_east);
//                    //  id_histroy_vehicleid.setText(getcurrentmarker.getString("vehicleId"));
//                    // id_history_vehicle_info_icon.setImageResource(R.drawable.bus_east);
//                } else if (parsingClass.getvehicleType().equalsIgnoreCase("Car")) {
//                    // id_histroy_vehicleid.setText(getcurrentmarker.getString("vehicleId"));
//                    // id_history_vehicle_info_icon.setImageResource(R.drawable.car_east);
//                    id_show_vehicle_info_icon.setImageResource(R.drawable.car_east);
//                } else if (parsingClass.getvehicleType().equalsIgnoreCase("Truck")) {
//                    //  id_histroy_vehicleid.setText(getcurrentmarker.getString("vehicleId"));
//                    //  id_history_vehicle_info_icon.setImageResource(R.drawable.truck_east);
//                    id_show_vehicle_info_icon.setImageResource(R.drawable.truck_east);
//                } else if (parsingClass.getvehicleType().equalsIgnoreCase("Cycle")) {
//                    //  id_histroy_vehicleid.setText(getcurrentmarker.getString("vehicleId"));
//                    //  id_history_vehicle_info_icon.setImageResource(R.drawable.cycle_east);
//                    id_show_vehicle_info_icon.setImageResource(R.drawable.cycle_east);
//                } else if (parsingClass.getvehicleType().equalsIgnoreCase("Jcb")) {
//                    //  id_histroy_vehicleid.setText(getcurrentmarker.getString("vehicleId"));
//                    //  id_history_vehicle_info_icon.setImageResource(R.drawable.cycle_east);
//                    id_show_vehicle_info_icon.setImageResource(R.drawable.jcb_icon);
//                } else if (parsingClass.getvehicleType().equalsIgnoreCase("Bike")) {
//                    //  id_histroy_vehicleid.setText(getcurrentmarker.getString("vehicleId"));
//                    //  id_history_vehicle_info_icon.setImageResource(R.drawable.cycle_east);
//                    id_show_vehicle_info_icon.setImageResource(R.drawable.vehicle_bike);
//                } else if (parsingClass.getvehicleType().equalsIgnoreCase("HeavyVehicle")) {
//                    id_show_vehicle_info_icon.setImageResource(R.drawable.truck_east);
//                }


                history_distance_covered_value.setText((parsingClass.gettripDistance().equals("null") ? "No Data" : parsingClass.gettripDistance() + " KMS"));

//                histroy_start_time_value.setText((parsingClass.getfromDateTime().equals("null") ? "No Data" : parsingClass.getfromDateTime()));
//                history_endtime_value.setText((parsingClass.gettoDateTime().equals("null") ? "No Data" : parsingClass.gettoDateTime()));

                histroy_start_time_value.setText(cons.getTripTimefromserver((parsingClass.getFromDateTimeUTC().equals("null") ? "No Data" : parsingClass.getFromDateTimeUTC())));
                history_endtime_value.setText(cons.getTripTimefromserver((parsingClass.getToDateTimeUTC().equals("null") ? "No Data" : parsingClass.getToDateTimeUTC())));

                history_over_speed_count_value.setText(String.valueOf(parsingClass.getmOverSpeedCount()));
                history_parked_count_value.setText(String.valueOf(parsingClass.getmParkedCount()));


                mSpeedLimit = parsingClass.getOverSpeedLimit();
                mOdoDistance = parsingClass.getodoDistance();
                mParkedTime = parsingClass.getTotalParkedTime();
                mIdleTime = parsingClass.getTotalIdleTime();
                mMovingTime = parsingClass.getTotalMovingTime();
                mNoDataTime = parsingClass.getTotalNoDataTime();
                mDistanceTravelled = parsingClass.gettripDistance();

//                System.out.println("The over speed limit 123 is :::" + parsingClass.getOverSpeedLimit());
//                System.out.println("The history end time is 1111111::::::"+(parsingClass.gettoDateTime().equals("null") ? "No Data" : parsingClass.gettoDateTime()));

                totalrows = parsingClass.gettotalRows();
                history_timetaken_value.setText(cons.getVehicleTime(parsingClass.gettotalRunningTime()));
                /** Parsing all the history data*/
                try {
                    JsonFactory f = new MappingJsonFactory();
                    JsonParser historyjsonParser = f.createJsonParser(result);
                    JsonToken current;
                    current = historyjsonParser.nextToken();
                    if (current != JsonToken.START_OBJECT) {
                        return;
                    }
                    while (historyjsonParser.nextToken() != JsonToken.END_OBJECT) {
                        String fieldName = historyjsonParser.getCurrentName();
                        if (fieldName.equals("history4Mobile")) {
                            current = historyjsonParser.nextToken();
                            if (current == JsonToken.START_ARRAY) {
                                int current_node_index = 0;
                                while (historyjsonParser.nextToken() != JsonToken.END_ARRAY) {
                                    // read the record into a tree model,
                                    // this moves the parsing position to the end of it
                                    JsonNode node = historyjsonParser.readValueAsTree();
                                    // And now we have random access to everything in the object
                                    if (!node.isNull()) {
                                        parsingClass.historyobjectparsing(node);
                                        double lat = parsingClass.getCurr_lat();
                                        double lng = parsingClass.getCurr_long();
                                        LatLng position = new LatLng(lat, lng);
                                        geoPoints.add(position);
                                        mPosList.add(parsingClass.getPosition());
                                        mAddsList.add(parsingClass.getAddress());
                                        MarkerOptions markerOption = new MarkerOptions().position(position);
                                        options.add(position);
                                        options.color(getResources().getColor(R.color.history_polyline_color));
                                        speedlist.add(parsingClass.getSpeed());
                                        datetimelist.add(parsingClass.getlastseen());
                                        distancelist.add(parsingClass.getDistanCovered());
                                        if (parsingClass.getparked_time() != null) {
                                            mIdleTimeList.add(parsingClass.getparked_time());
                                        } else {
                                            mIdleTimeList.add("");
                                        }
//                                                View marker = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.custommarker, null);
//                                                ImageView id_history_marker_icon = (ImageView) marker.findViewById(R.id.id_history_marker_icon);
//                                                ImageView id_custom_marker_icon = (ImageView) marker.findViewById(R.id.id_custom_marker_icon);
//                                                ImageView id_vehicle_in_marker = (ImageView) marker.findViewById(R.id.id_vehicle_in_marker);
//                                                id_history_marker_icon.setVisibility(View.VISIBLE);
//                                                id_custom_marker_icon.setVisibility(View.GONE);
//                                                id_vehicle_in_marker.setVisibility(View.GONE);
                                        Marker currentMarker = null;
                                        if (Integer.parseInt(totalrows) == 1){
                                            mPopupEndLat = parsingClass.getCurr_lat();
                                            mPopupEndLng = parsingClass.getCurr_long();
                                            Log.d("mPopupEndLat",""+mPopupEndLat);
                                            Log.d("mPopupEndLng",""+mPopupEndLng);
                                        }
                                        if (current_node_index == 0) {
//                                                    id_history_marker_icon.setImageResource(R.drawable.startflag);
//                                                    markerOption.icon(BitmapDescriptorFactory.fromBitmap(createDrawableFromView(VehicleHistoryActivity.this, marker)));
//                                                    currentMarker = map.addMarker(markerOption);
//                                                    parker_mMarkersHashMap.put(currentMarker, cons.getHistoryTimefromserver(parsingClass.getlastseen()) + "," + "-" + "," + Double.toString(parsingClass.getCurr_lat()) + "," + Double.toString(parsingClass.getCurr_long()) + "," + "start");
                                            mPopupStartLat = parsingClass.getCurr_lat();
                                            mPopupStartLng = parsingClass.getCurr_long();
//                                            new GetAddressTask(history_start_location_value).execute(parsingClass.getCurr_lat(), parsingClass.getCurr_long(), 2.0);
//
//                                            final Handler handler2 = new Handler();
//                                            handler2.postDelayed(new Runnable() {
//                                                @Override
//                                                public void run() {
//                                                    //Do something after 100ms
//                                                    new GetAddressTask(history_start_location_value).execute(mPopupStartLat, mPopupStartLng, 2.0);
//                                                }
//                                            }, 1500);

                                        } else if (current_node_index == Integer.parseInt(totalrows) - 1) {
//                                                    id_history_marker_icon.setImageResource(R.drawable.endflag);
//                                                    markerOption.icon(BitmapDescriptorFactory.fromBitmap(createDrawableFromView(VehicleHistoryActivity.this, marker)));
//                                                    currentMarker = map.addMarker(markerOption);
//                                                    parker_mMarkersHashMap.put(currentMarker, cons.getHistoryTimefromserver(parsingClass.getlastseen()) + "," + "-" + "," + Double.toString(parsingClass.getCurr_lat()) + "," + Double.toString(parsingClass.getCurr_long()) + "," + "end");
                                            mPopupEndLat = parsingClass.getCurr_lat();
                                            mPopupEndLng = parsingClass.getCurr_long();
                                            Log.d("parsingend",""+mPopupEndLat);
                                            Log.d("parsingendlng",""+mPopupEndLat);

//                                            final Handler handler2 = new Handler();
//                                            handler2.postDelayed(new Runnable() {
//                                                @Override
//                                                public void run() {
//                                                    //Do something after 100ms
//                                                    new GetAddressTask(history_end_location_value).execute(mPopupEndLat, mPopupEndLng, 3.0);
//                                                }
//                                            }, 2500);


                                        }
//                                                else if (parsingClass.getPosition().equalsIgnoreCase("P")) {
//                                                    id_history_marker_icon.setImageResource(R.drawable.id_parker_icon);
//                                                    markerOption.icon(BitmapDescriptorFactory.fromBitmap(createDrawableFromView(VehicleHistoryActivity.this, marker)));
//                                                    currentMarker = map.addMarker(markerOption);
//                                                    parker_mMarkersHashMap.put(currentMarker, cons.getHistoryTimefromserver(parsingClass.getlastseen()) + "," + parsingClass.getparked_time() + "," + Double.toString(parsingClass.getCurr_lat()) + "," + Double.toString(parsingClass.getCurr_long()) + "," + "parked");
//                                                } else {
//                                                    markerOption.alpha(0);
//                                                    currentMarker = map.addMarker(markerOption);
//                                                    currentMarker.setVisible(false);
//                                                    parker_mMarkersHashMap.put(currentMarker, cons.getHistoryTimefromserver(parsingClass.getlastseen()) + "," + "-" + "," + "-" + "," + "-" + "," + "idle");
//                                                }
//                                                zoom_location_marker.add(currentMarker);
                                    }
                                    current_node_index++;
                                }
                            }
                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                    if (dialog.isShowing()) {
                        dialog.dismiss();
                    }
                }
                /** Parsing all the history data*/
                // map.addPolyline(options);
                //progressDialog.dismiss();
//                        if (!zoom_location_marker.isEmpty()) {
//                            LatLngBounds.Builder builder = new LatLngBounds.Builder();
//                            for (int marker = 0; marker < zoom_location_marker.size(); marker++) {
//                                builder.include(zoom_location_marker.get(marker).getPosition());
//                            }
//                            LatLngBounds bounds = builder.build();
//                            int padding = 100; // offset from edges of the map in pixels
//                            CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);
//                            map.animateCamera(cu);
//                        }
                pause = false;
                if (geoPoints.size() > 2) {
                    // history_seekbar.setMax(5);
                    replay_icon.setVisibility(View.GONE);
                    speed_seekbar.setVisibility(View.GONE);
                    play_icon.setVisibility(View.VISIBLE);
                    pause_icon.setVisibility(View.GONE);
                    history_seekbar.setVisibility(View.VISIBLE);
                } else {
                    // history_seekbar.setMax(5);
                    replay_icon.setVisibility(View.GONE);
                    play_icon.setVisibility(View.GONE);
                    pause_icon.setVisibility(View.GONE);
                    speed_seekbar.setVisibility(View.GONE);
                    history_seekbar.setVisibility(View.GONE);
                }
                backButtonPressed = false;
                //speed_seekbar.setProgress(7);
                // marker_loop(0);
            } catch (Exception je) {
                je.printStackTrace();
            } finally {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
            }
            addMarkerToMap();
        } else {
            if (dialog.isShowing()) {
                dialog.dismiss();
            }
            Toast.makeText(getApplicationContext(), "No History Data Available for the Selected Intervals", Toast.LENGTH_SHORT).show();
        }
//            }
//        });

        newCutomizedHistoryInfoPopupWindow();
    }

    /**
     * Date and Time picker code
     */
    @Override
    protected Dialog onCreateDialog(int id) {
        Calendar calNow = Calendar.getInstance();
        switch (id) {
            case DATE_PICKER_ID:
                // open datepicker dialog.
                // set date picker for current date
                // add pickerListener listner to date picker
                return new DatePickerDialog(this, datepickerListener, calNow.get(Calendar.YEAR), calNow.get(Calendar.MONTH), calNow.get(Calendar.DAY_OF_MONTH));
            case TIME_DIALOG_ID:
                // set time picker as current time
                return new TimePickerDialog(this, timePickerListener, calNow.get(Calendar.HOUR_OF_DAY), calNow.get(Calendar.MINUTE), false);
        }
        return null;
    }


    @Override
    protected void onPause() {
        super.onPause();
        //Log.i("onPause", "The onPause() event");
        animator.stopAnimation();
        // handler.removeCallbacksAndMessages(null);
        System.exit(0);

    }

    @Override
    protected void onResume() {
        super.onResume();


    }

    /**
     * Called when the activity is no longer visible.
     */
    @Override
    protected void onStop() {
        super.onStop();
        animator.stopAnimation();
        //   handler.removeCallbacksAndMessages(null);


    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        animator.stopAnimation();

//        System.out.println("The vehicle id is in history:::::" + Constant.SELECTED_VEHICLE_ID);

//        Bundle extras = getIntent().getExtras();
//        if (extras != null) {
//            Constant.SELECTED_VEHICLE_ID = mUserId;
//            Constant.SELECTED_USER_ID = mVehicleId;
//            Constant.SELECTED_VEHICLE_LOCATION_OBJECT = mSelectedJsonObj;
//
//            System.out.println("The vehicle id is :::::history " + Constant.SELECTED_VEHICLE_ID + " " + Constant.SELECTED_USER_ID + " " + Constant.SELECTED_VEHICLE_LOCATION_OBJECT);
//
//
//
//            startActivity(new Intent(HistoryActivity.this, TripSummaryActivity.class));
//            finish();
//        } else {
        startActivity(new Intent(HistoryGoogleMapActivity.this, VehicleListActivity.class));
        finish();
//        }


    }

    private void changeMapView() {

        // TODO Auto-generated method stub
        final Dialog dialog = new Dialog(HistoryGoogleMapActivity.this);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.radio_popup);
        dialog.show();

        RadioGroup rg_home = (RadioGroup) dialog.findViewById(R.id.rg_home_views);
        RadioGroup rg_history = (RadioGroup) dialog.findViewById(R.id.rg_history_views);
        rg_history.clearCheck();
        rg_history.setVisibility(View.GONE);
        rg_home.setVisibility(View.VISIBLE);

        RadioButton $Normal = (RadioButton) dialog
                .findViewById(R.id.rb_home_normal);
        RadioButton $Satelite = (RadioButton) dialog
                .findViewById(R.id.rb_home_satellite);
        RadioButton $Terrain = (RadioButton) dialog
                .findViewById(R.id.rb_home_terrain);
        RadioButton $Hybrid = (RadioButton) dialog
                .findViewById(R.id.rb_home_hybrid);


        if (mSELECTED_MAP_TYPE.equalsIgnoreCase("Normal")) {
            $Normal.setChecked(true);
            $Satelite.setChecked(false);
            $Terrain.setChecked(false);
            $Hybrid.setChecked(false);
        } else if (mSELECTED_MAP_TYPE.equalsIgnoreCase("Satelite")) {
            $Normal.setChecked(false);
            $Satelite.setChecked(true);
            $Terrain.setChecked(false);
            $Hybrid.setChecked(false);
        } else if (mSELECTED_MAP_TYPE.equalsIgnoreCase("Terrain")) {
            $Normal.setChecked(false);
            $Satelite.setChecked(false);
            $Terrain.setChecked(true);
            $Hybrid.setChecked(false);
        } else if (mSELECTED_MAP_TYPE.equalsIgnoreCase("Hybrid")) {
            $Normal.setChecked(false);
            $Satelite.setChecked(false);
            $Terrain.setChecked(false);
            $Hybrid.setChecked(true);
        }
        $Normal.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                map.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                mSELECTED_MAP_TYPE = "Normal";
                dialog.dismiss();
            }
        });
        $Satelite.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                mSELECTED_MAP_TYPE = "Satelite";
                map.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
                dialog.dismiss();
            }
        });
        $Terrain.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                mSELECTED_MAP_TYPE = "Terrain";
                map.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
                dialog.dismiss();
            }
        });

        $Hybrid.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                mSELECTED_MAP_TYPE = "Hybrid";
                map.setMapType(GoogleMap.MAP_TYPE_HYBRID);
                dialog.dismiss();
            }
        });

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        height = displayMetrics.heightPixels;
        width = displayMetrics.widthPixels;

        LinearLayout.LayoutParams radioParama = new LinearLayout.LayoutParams(
                FrameLayout.LayoutParams.WRAP_CONTENT,
                FrameLayout.LayoutParams.WRAP_CONTENT);
        radioParama.width = width * 50 / 100;
        radioParama.height = width * 10 / 100;
        radioParama.topMargin = height * 4 / 100;
        radioParama.gravity = Gravity.CENTER;
        radioParama.leftMargin = height * 4 / 100;
        $Normal.setLayoutParams(radioParama);
        $Satelite.setLayoutParams(radioParama);
        $Terrain.setLayoutParams(radioParama);

        LinearLayout.LayoutParams radioterrainParama = new LinearLayout.LayoutParams(
                FrameLayout.LayoutParams.WRAP_CONTENT,
                FrameLayout.LayoutParams.WRAP_CONTENT);
        radioterrainParama.width = width * 50 / 100;
        radioterrainParama.height = width * 10 / 100;
        radioterrainParama.topMargin = height * 4 / 100;
        radioterrainParama.gravity = Gravity.CENTER;
        radioterrainParama.leftMargin = height * 4 / 100;
        radioterrainParama.bottomMargin = height * 4 / 100;
        $Hybrid.setLayoutParams(radioterrainParama);

        if (width >= 600) {
            $Normal.setTextSize(16);
            $Satelite.setTextSize(16);
            $Terrain.setTextSize(16);
            $Hybrid.setTextSize(16);
        } else if (width > 501 && width < 600) {
            $Normal.setTextSize(15);
            $Satelite.setTextSize(15);
            $Terrain.setTextSize(15);
            $Hybrid.setTextSize(15);
        } else if (width > 260 && width < 500) {
            $Normal.setTextSize(14);
            $Satelite.setTextSize(14);
            $Terrain.setTextSize(14);
            $Hybrid.setTextSize(14);
        } else if (width <= 260) {
            $Normal.setTextSize(13);
            $Satelite.setTextSize(13);
            $Terrain.setTextSize(13);
            $Hybrid.setTextSize(13);
        }
    }





    /*
    The below all method is for to show customised history pop up window
     */

    public void drawChart() {

//        int[] x = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11};
//        int[] speed = {10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130};
//        int[] expense = { 2200, 1800, 2900, 3200, 2000, 3000, 3300, 3800, 3400, 3700,
//                3500, 4200 };

        // Creating an XYSeries for Income
        XYSeries incomeSeries = new XYSeries("");
        // Creating an XYSeries for Expense
//        XYSeries expenseSeries = new XYSeries("Expense");
        // Adding data to Income and Expense Series
        for (int i = 0; i < speedlist.size(); i++) {
            incomeSeries.add(i, Integer.valueOf(speedlist.get(i)));
//            expenseSeries.add(i, expense[i]);
        }

        // Creating a dataset to hold each series
        XYMultipleSeriesDataset dataset = new XYMultipleSeriesDataset();
        // Adding Income Series to the dataset
        dataset.addSeries(incomeSeries);
        // Adding Expense Series to dataset
//        dataset.addSeries(expenseSeries);

        // Creating XYSeriesRenderer to customize incomeSeries
        XYSeriesRenderer incomeRenderer = new XYSeriesRenderer();
        incomeRenderer.setColor(Color.CYAN); // color of the graph set to cyan
        incomeRenderer.setFillPoints(true);
        incomeRenderer.setLineWidth(2f);
        incomeRenderer.setDisplayChartValues(true);
        // setting chart value distance
        incomeRenderer.setDisplayChartValues(true);
        // setting line graph point style to circle
        incomeRenderer.setPointStyle(PointStyle.CIRCLE);
        // setting stroke of the line chart to solid
        incomeRenderer.setStroke(BasicStroke.SOLID);
        // for filling area
        incomeRenderer.setFillBelowLine(true);
        // incomeRenderer.setFillBelowLineColor(Color.YELLOW);

        // Creating XYSeriesRenderer to customize expenseSeries
//        XYSeriesRenderer expenseRenderer = new XYSeriesRenderer();
//        expenseRenderer.setColor(Color.GREEN);
//        expenseRenderer.setFillPoints(true);
//        expenseRenderer.setLineWidth(2f);
//        expenseRenderer.setDisplayChartValues(true);
//        // setting line graph point style to circle
//        expenseRenderer.setPointStyle(PointStyle.SQUARE);
//        // setting stroke of the line chart to solid
//        expenseRenderer.setStroke(BasicStroke.SOLID);
//        // for filling area
//        expenseRenderer.setFillBelowLine(true);
        // expenseRenderer.setFillBelowLineColor(Color.RED);

        // Creating a XYMultipleSeriesRenderer to customize the whole chart
        XYMultipleSeriesRenderer multiRenderer = new XYMultipleSeriesRenderer();
        multiRenderer.setXLabels(0);
        //   multiRenderer.setChartTitle("Income vs Expense Chart");
        // multiRenderer.setXTitle("Year 2014");
        // multiRenderer.setYTitle("Amount in Dollars");

        /***
         * Customizing graphs
         */
        // setting text size of the title
        multiRenderer.setChartTitleTextSize(28);
        // setting text size of the axis title
        multiRenderer.setAxisTitleTextSize(24);
        // setting text size of the graph lable
        multiRenderer.setLabelsTextSize(24);
        // setting zoom buttons visiblity
        multiRenderer.setZoomButtonsVisible(false);
        // setting pan enablity which uses graph to move on both axis
        multiRenderer.setPanEnabled(true, true);
        // setting click false on graph
        multiRenderer.setClickEnabled(true);
        // setting zoom to false on both axis
        multiRenderer.setZoomEnabled(true, false);
        // setting lines to display on y axis
        multiRenderer.setShowGridY(true);
        // setting lines to display on x axis
        multiRenderer.setShowGridX(true);
        // setting legend to fit the screen size
        multiRenderer.setFitLegend(true);
        // setting displaying line on grid
        multiRenderer.setShowGrid(true);
        // setting zoom to false
        multiRenderer.setZoomEnabled(false);
        // setting external zoom functions to false
        multiRenderer.setExternalZoomEnabled(false);
        // setting displaying lines on graph to be formatted(like using
        // graphics)
        multiRenderer.setAntialiasing(true);
        // setting to in scroll to false
        multiRenderer.setInScroll(false);
        // setting to set legend height of the graph
        multiRenderer.setLegendHeight(30);
        // setting x axis label align
        multiRenderer.setXLabelsAlign(Paint.Align.CENTER);
        // setting y axis label to align
        multiRenderer.setYLabelsAlign(Paint.Align.LEFT);
        // setting text style
        multiRenderer.setTextTypeface("sans_serif", Typeface.NORMAL);
        // setting no of values to display in y axis
        multiRenderer.setYLabels(10);
        // setting y axis max value, Since i'm using static values inside the
        // graph so i'm setting y max value to 4000.
        // if you use dynamic values then get the max y value and set here
        multiRenderer.setYAxisMax(100);
        multiRenderer.setYAxisMin(0.0);
        // setting used to move the graph on xaxiz to .5 to the right
        multiRenderer.setXAxisMin(-0.5);
        // setting used to move the graph on xaxiz to .5 to the right
        multiRenderer.setXAxisMax(11);

        multiRenderer.setPanLimits(new double[]{0.0, Double.MAX_VALUE, 0.0, Double.MAX_VALUE});

        // setting bar size or space between two bars
        // multiRenderer.setBarSpacing(0.5);
        // Setting background color of the graph to transparent
        multiRenderer.setBackgroundColor(Color.TRANSPARENT);
        // Setting margin color of the graph to transparent

        multiRenderer.setApplyBackgroundColor(true);
        multiRenderer.setScale(2f);
        // setting x axis point size
        multiRenderer.setPointSize(4f);
        // setting the margin size for the graph in the order top, left, bottom,
        // right
        multiRenderer.setMargins(new int[]{0, 0, 0, 0});
        multiRenderer.setMarginsColor(R.color.trans);
//        multiRenderer.setInScroll(true);

//        multiRenderer.
/*
For x axis values
 */
//        for (int i = 0; i < datetimelist.size(); i++) {
//            multiRenderer.addXTextLabel(i, cons.getTimefromserver(datetimelist.get(i)));
//        }

        // Adding incomeRenderer and expenseRenderer to multipleRenderer
        // Note: The order of adding dataseries to dataset and renderers to
        // multipleRenderer
        // should be same
        multiRenderer.addSeriesRenderer(incomeRenderer);
//        multiRenderer.addSeriesRenderer(expenseRenderer);

        // this part is used to display graph on the xml
//        LinearLayout chartContainer = (LinearLayout) findViewById(R.id.chart_container);
//        // remove any views before u paint the chart
//        chartContainer.removeAllViews();

        mLinearChartContainerLayout.removeAllViews();

        // drawing bar chart
        final GraphicalView Chart = ChartFactory.getLineChartView(HistoryGoogleMapActivity.this, dataset,
                multiRenderer);
        // adding the view to the linearlayout


        Chart.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // handle the click event on the chart
                SeriesSelection seriesSelection = Chart.getCurrentSeriesAndPoint();
                if (seriesSelection == null) {
                    //  Toast.makeText(CameraViewActivity.this, "No chart element", Toast.LENGTH_SHORT).show();
                } else {
                    // display information of the clicked point
                    Toast.makeText(
                            HistoryGoogleMapActivity.this,
                            "Vehicle Speed is : "
                                    + seriesSelection.getValue() + " Km/hr", Toast.LENGTH_SHORT).show();
                }
            }
        });


//        chartContainer.addView(Chart);
        mLinearChartContainerLayout.addView(Chart);

    }

    public void newCutomizedHistoryInfoPopupWindow() {


        final Dialog history_dialog = new Dialog(HistoryGoogleMapActivity.this);
        history_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        history_dialog.setContentView(R.layout.new_history_info_layout);
//        history_dialog.
        history_dialog.setCancelable(true);


        mLinearHeaderLayout = (LinearLayout) history_dialog.findViewById(R.id.new_info_header_layout);

        mLinearLayout1 = (LinearLayout) history_dialog.findViewById(R.id.new_history_info_linear_layout1);
        mSubLinearLayout1 = (LinearLayout) history_dialog.findViewById(R.id.new_history_info_linear_layout10);
        mSubLinearLayout2 = (LinearLayout) history_dialog.findViewById(R.id.new_history_info_linear_layout11);


        mTxtHeader = (TextView) history_dialog.findViewById(R.id.new_info_header_txt);
        mSpeedLimitTxtView10 = (TextView) history_dialog.findViewById(R.id.new_history_info_txt_view10);
        mOdoDistTxtView11 = (TextView) history_dialog.findViewById(R.id.new_history_info_txt_view11);
        mTxtDistanceTravelledView = (TextView) history_dialog.findViewById(R.id.new_history_info_txt_view12);
        //mTxtView12 = (TextView) findViewById(R.id.new_info_txt_view12);

        mTxtHeader.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        mSpeedLimitTxtView10.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        mOdoDistTxtView11.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        mTxtDistanceTravelledView.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));

        mSpeedLimitTxtValue10 = (TextView) history_dialog.findViewById(R.id.new_history_info_txt_value10);
        mOdoDistTxtValue11 = (TextView) history_dialog.findViewById(R.id.new_history_info_txt_value11);
        mTxtDistanceTravelledValue = (TextView) history_dialog.findViewById(R.id.new_history_info_txt_value12);
        //  mTxtValue12 = (TextView) findViewById(R.id.new_info_txt_value12);
        mSpeedLimitTxtValue10.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        mOdoDistTxtValue11.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        mTxtDistanceTravelledValue.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));

        mLinearLayout2 = (LinearLayout) history_dialog.findViewById(R.id.new_history_time_linear_layout);

        mStartTimeTxtView = (TextView) history_dialog.findViewById(R.id.new_history_start_time_txt_view);
        mEndTimeTxtView = (TextView) history_dialog.findViewById(R.id.new_history_end_time_txt_view);
        mStartTimeTxtView.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        mEndTimeTxtView.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));

        mTimeVerView = (View) history_dialog.findViewById(R.id.new_history_time_view);

        mLinearAddsLayout = (LinearLayout) history_dialog.findViewById(R.id.new_history_adds_linear_layout);

        mStartAddsTxtView = (TextView) history_dialog.findViewById(R.id.new_history_start_adds_txt_view);
        mEndAddsTxtView = (TextView) history_dialog.findViewById(R.id.new_history_end_adds_txt_view);
        mStartAddsTxtView.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        mEndAddsTxtView.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));

        mAddsVerView = (View) history_dialog.findViewById(R.id.new_history_adds_view);


        mLinearPMINLayout = (LinearLayout) history_dialog.findViewById(R.id.new_history_pmin_time_linear_layout3);
        mSubLinearLayout3 = (LinearLayout) history_dialog.findViewById(R.id.new_history_pmin_time_linear_layout30);
        mParkTimeTxtView = (TextView) history_dialog.findViewById(R.id.new_history_parked_time_txt_view30);
        mMovingTimeTxtView = (TextView) history_dialog.findViewById(R.id.new_history_moving_time_txt_view31);
        mIdleTimeTxtView = (TextView) history_dialog.findViewById(R.id.new_history_idle_time_txt_view32);
        mNoDataTimeTxtView = (TextView) history_dialog.findViewById(R.id.new_history_nodata_time_txt_view33);

        mParkTimeTxtView.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        mMovingTimeTxtView.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        mIdleTimeTxtView.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        mNoDataTimeTxtView.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));

        mSubLinearLayout4 = (LinearLayout) history_dialog.findViewById(R.id.new_history_pmin_time_value_linear_layout30);
        mParkedTimeValueTxtView = (TextView) history_dialog.findViewById(R.id.new_history_parked_time_txt_value);
        mMovingTimeValueTxtView = (TextView) history_dialog.findViewById(R.id.new_history_moving_time_txt_value);
        mIdleTimeValueTxtView = (TextView) history_dialog.findViewById(R.id.new_history_idle_time_txt_value);
        mNoDataTimeValueTxtView = (TextView) history_dialog.findViewById(R.id.new_history_nodata_time_txt_value);

        mParkedTimeValueTxtView.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        mMovingTimeValueTxtView.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        mIdleTimeValueTxtView.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        mNoDataTimeValueTxtView.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));

        mLinearChartContainerLayout = (LinearLayout) history_dialog.findViewById(R.id.chart_container);

        mHoriView1 = (View) history_dialog.findViewById(R.id.new_history_info_view100);
        mHoriView2 = (View) history_dialog.findViewById(R.id.new_history_info_view200);

        mLinearBottomLayout = (LinearLayout) history_dialog.findViewById(R.id.new_history_bottom_linear_layout21);

        mPlayBackTxtView = (TextView) history_dialog.findViewById(R.id.new_history_txt_play_back);
        mNaviTxtView = (TextView) history_dialog.findViewById(R.id.new_history_txt_navigation);

        mPlayBackTxtView.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        mNaviTxtView.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));

        mSubLinearLayout5 = (LinearLayout) history_dialog.findViewById(R.id.play_back_layout);
        mSubLinearLayout6 = (LinearLayout) history_dialog.findViewById(R.id.navigation_layout);

        mPlayBackImage = (ImageView) history_dialog.findViewById(R.id.play_back_imgview);
        mNavigationImage = (ImageView) history_dialog.findViewById(R.id.navigation_imgview);

        popupScreenArrange();



        for (int index = 0;index <mAddsList.size();index++){
            if (index == 0){
                mStartAddsTxtView.setText(mAddsList.get(index));
                history_start_location_value.setText(mAddsList.get(index));
                Log.d(" mStartAddsindex",""+mAddsList.get(index));
            } else if (index == mAddsList.size() -1){
                Log.d(" mEndAddsindex",""+mAddsList.get(index));
                mEndAddsTxtView.setText(mAddsList.get(index));
                history_end_location_value.setText(mAddsList.get(index));
            } else if (mAddsList.get(index).equalsIgnoreCase("-")){

                                  final Handler handler2 = new Handler();
             handler2.postDelayed(new Runnable() {
                 @Override
                 public void run() {

                     new GetAddressTask(mStartAddsTxtView).execute(mPopupStartLat, mPopupStartLng, 4.0);

                 }
             }, 100);

                     final Handler handler = new Handler();
             handler.postDelayed(new Runnable() {
                 @Override
                 public void run() {
                     //Do something after 100ms

//                if(mPopupEndLat!=0&&mPopupEndLng!=0) {
                     new GetAddressTask(mEndAddsTxtView).execute(mPopupEndLat, mPopupEndLng, 5.0);
//                }else{
//                    mEndAddsTxtView.setText("End location not available");
//                }
                 }
             }, 1500);
            }
//         if (index == mAddsList.size() -1){
//             Log.d(" mEndAddsindex",""+mAddsList.get(index));
//             mEndAddsTxtView.setText(mAddsList.get(index));
//         } else {
//
//         }

        }

//        final Handler handler2 = new Handler();
//        handler2.postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                //Do something after 100ms
//
////                if(mPopupStartLat!=0&&mPopupStartLng!=0) {
//                new GetAddressTask(mStartAddsTxtView).execute(mPopupStartLat, mPopupStartLng, 4.0);
////                }else{
////                    mStartAddsTxtView.setText("Start location not available");
////                }
//            }
//        }, 100);
//
//
//        final Handler handler = new Handler();
//        handler.postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                //Do something after 100ms
//
////                if(mPopupEndLat!=0&&mPopupEndLng!=0) {
//                Log.d("mPopupEndLat",""+mPopupEndLat);
//                Log.d("mPopupEndLng",""+mPopupEndLng);
//                new GetAddressTask(mEndAddsTxtView).execute(mPopupEndLat, mPopupEndLng, 5.0);
////                }else{
////                    mEndAddsTxtView.setText("End location not available");
////                }
//            }
//        }, 1500);


        if (mSpeedLimit != null) {
            mSpeedLimitTxtValue10.setText(mSpeedLimit + " Km/hr");
        } else {
            mSpeedLimitTxtValue10.setText("0 Km/hr");
        }

        if (mOdoDistance != null) {
            mOdoDistTxtValue11.setText(mOdoDistance + " Kms");
        } else {
            mOdoDistTxtValue11.setText("0 Km");
        }
        mTxtHeader.setText(Constant.SELECTED_VEHICLE_SHORT_NAME);

        if (mDistanceTravelled != null) {
            mTxtDistanceTravelledValue.setText(mDistanceTravelled + " " + "Kms");
        } else {
            mTxtDistanceTravelledValue.setText("0 Km");
        }
//        mEndAddsTxtView.setText(history_end_location_value.getText().toString().trim());
//        mStartAddsTxtView.setText(history_start_location_value.getText().toString().trim());

        mStartTimeTxtView.setText(histroy_start_time_value.getText().toString().trim());
        mEndTimeTxtView.setText(history_endtime_value.getText().toString().trim());

        mParkedTimeValueTxtView.setText(mParkedTime);
        mIdleTimeValueTxtView.setText(mIdleTime);
        mMovingTimeValueTxtView.setText(mMovingTime);
        mNoDataTimeValueTxtView.setText(mNoDataTime);

        mPlayBackImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {




                if (geoPoints.size() > 2) {
                    history_dialog.dismiss();
                    pause = false;
                    // marker_loop(progressChanged);
                    animator.startAnimation(true);
                    pause_icon.setVisibility(View.VISIBLE);
                    play_icon.setVisibility(View.GONE);
                } else {
                    Toast.makeText(getApplicationContext(), "Please select other options to view history ", Toast.LENGTH_SHORT).show();
                    history_dialog.dismiss();
                    opptionPopUp();
                }
            }
        });

        mNavigationImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                history_dialog.dismiss();
                Intent intent = new Intent(Intent.ACTION_VIEW,
                        Uri.parse("http://maps.google.com/maps?saddr=" + mPopupStartLat + "," + mPopupStartLng + ""));
                startActivity(intent);

            }
        });

//        if (speedlist != null && datetimelist != null) {
//            drawChart();
//        }
        mLinearChartContainerLayout.setVisibility(View.GONE);
        history_dialog.show();


    }


    public void popupScreenArrange() {
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        int height = metrics.heightPixels;
        int width = metrics.widthPixels;
        Constant.ScreenWidth = width;
        Constant.ScreenHeight = height;

        LinearLayout.LayoutParams headerLayoutParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        headerLayoutParams.width = width;
        headerLayoutParams.height = height * 8 / 100;
        mLinearHeaderLayout.setLayoutParams(headerLayoutParams);


        LinearLayout.LayoutParams headTxtParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        headTxtParams.width = width;
        headTxtParams.height = height * 8 / 100;
        mTxtHeader.setLayoutParams(headTxtParams);
        mTxtHeader.setPadding(width * 2 / 100, 0, width * 2 / 100, 0);
        mTxtHeader.setGravity(Gravity.CENTER);


        LinearLayout.LayoutParams linearMainParams1 = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        linearMainParams1.width = width * 89 / 100;
        linearMainParams1.height = height * 21 / 100;
        linearMainParams1.setMargins(width * 2 / 100, width * 2 / 100, width * 2 / 100, width * 1 / 100);
        mLinearLayout1.setLayoutParams(linearMainParams1);


        LinearLayout.LayoutParams linearSubParams10 = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        linearSubParams10.width = width * 45 / 100;
        linearSubParams10.height = height * 21 / 100;
        mSubLinearLayout1.setLayoutParams(linearSubParams10);
//        mSubLinearLayout1.setPadding(width * 1 / 100, width * 1 / 100, width * 1 / 100, width * 1 / 100);

        LinearLayout.LayoutParams txtViewParams10 = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        txtViewParams10.width = width * 45 / 100;
        txtViewParams10.height = height * 7 / 100;
        mSpeedLimitTxtView10.setLayoutParams(txtViewParams10);
        mSpeedLimitTxtView10.setPadding(width * 1 / 100, height * 1 / 2 / 100, height * 1 / 2 / 100, height * 1 / 2 / 100);

        LinearLayout.LayoutParams txtViewParams11 = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        txtViewParams11.width = width * 45 / 100;
        txtViewParams11.height = height * 7 / 100;
        mOdoDistTxtView11.setLayoutParams(txtViewParams11);
        mOdoDistTxtView11.setPadding(width * 1 / 100, height * 1 / 2 / 100, height * 1 / 2 / 100, height * 1 / 2 / 100);

        LinearLayout.LayoutParams txtViewParams12 = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        txtViewParams12.width = width * 45 / 100;
        txtViewParams12.height = height * 7 / 100;
        mTxtDistanceTravelledView.setLayoutParams(txtViewParams12);
        mTxtDistanceTravelledView.setPadding(width * 1 / 100, height * 1 / 2 / 100, height * 1 / 2 / 100, height * 1 / 2 / 100);


        LinearLayout.LayoutParams linearSubParams11 = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        linearSubParams11.width = width * 44 / 100;
        linearSubParams11.height = height * 21 / 100;
        mSubLinearLayout2.setLayoutParams(linearSubParams11);
        //  mSubLinearLayout2.setPadding(width * 1 / 100, width * 1 / 100, width * 1 / 100, width * 1 / 100);

        LinearLayout.LayoutParams txtValueParams10 = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        txtValueParams10.width = width * 44 / 100;
        txtValueParams10.height = height * 7 / 100;
        mSpeedLimitTxtValue10.setLayoutParams(txtValueParams10);
        mSpeedLimitTxtValue10.setGravity(Gravity.CENTER | Gravity.RIGHT);
        mSpeedLimitTxtValue10.setPadding(width * 1 / 2 / 100, height * 1 / 2 / 100, height * 2 / 100, height * 1 / 2 / 100);

        LinearLayout.LayoutParams txtValueParams11 = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        txtValueParams11.width = width * 44 / 100;
        txtValueParams11.height = height * 7 / 100;
        mOdoDistTxtValue11.setLayoutParams(txtValueParams11);
        mOdoDistTxtValue11.setGravity(Gravity.CENTER | Gravity.RIGHT);
        mOdoDistTxtValue11.setPadding(width * 1 / 2 / 100, height * 1 / 2 / 100, height * 2 / 100, height * 1 / 2 / 100);

        LinearLayout.LayoutParams txtValueParams12 = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        txtValueParams12.width = width * 44 / 100;
        txtValueParams12.height = height * 7 / 100;
        mTxtDistanceTravelledValue.setLayoutParams(txtValueParams12);
        mTxtDistanceTravelledValue.setGravity(Gravity.CENTER | Gravity.RIGHT);
        mTxtDistanceTravelledValue.setPadding(width * 1 / 2 / 100, height * 1 / 2 / 100, height * 2 / 100, height * 1 / 2 / 100);


        LinearLayout.LayoutParams timeLayoutParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        timeLayoutParams.width = width * 89 / 100;
//        headerLayoutParams.height = height * 8 / 100;
        timeLayoutParams.setMargins(width * 2 / 100, width * 1 / 100, width * 2 / 100, width * 1 / 100);
        mLinearLayout2.setLayoutParams(timeLayoutParams);

        LinearLayout.LayoutParams txtStartTimeParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        txtStartTimeParams.width = width * 45 / 100;
        //txtValueParams11.height = height * 5 / 100;
        mStartTimeTxtView.setLayoutParams(txtStartTimeParams);
        mStartTimeTxtView.setGravity(Gravity.CENTER);
        mStartTimeTxtView.setPadding(width * 1 / 2 / 100, height * 1 / 2 / 100, height * 1 / 100, height * 1 / 2 / 100);

        mEndTimeTxtView.setLayoutParams(txtStartTimeParams);
        mEndTimeTxtView.setWidth(width * 44 / 100);
        mEndTimeTxtView.setGravity(Gravity.CENTER);
        mEndTimeTxtView.setPadding(width * 1 / 2 / 100, height * 1 / 2 / 100, height * 1 / 100, height * 1 / 2 / 100);


        LinearLayout.LayoutParams addsLayoutParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        addsLayoutParams.width = width * 89 / 100;
//        headerLayoutParams.height = height * 8 / 100;
        addsLayoutParams.setMargins(width * 2 / 100, width * 1 / 100, width * 2 / 100, width * 1 / 100);
        mLinearAddsLayout.setLayoutParams(addsLayoutParams);

        LinearLayout.LayoutParams txtStartAddsParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        txtStartAddsParams.width = width * 45 / 100;
        //txtValueParams11.height = height * 5 / 100;
        mStartAddsTxtView.setLayoutParams(txtStartAddsParams);
        mStartAddsTxtView.setGravity(Gravity.CENTER);
        mStartAddsTxtView.setPadding(width * 1 / 2 / 100, height * 1 / 2 / 100, height * 1 / 100, height * 1 / 2 / 100);

        mEndAddsTxtView.setLayoutParams(txtStartAddsParams);
        mEndAddsTxtView.setWidth(width * 44 / 100);
        mEndAddsTxtView.setGravity(Gravity.CENTER);
        mEndAddsTxtView.setPadding(width * 1 / 2 / 100, height * 1 / 2 / 100, height * 1 / 100, height * 1 / 2 / 100);


        LinearLayout.LayoutParams linearPMINMainParams1 = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        linearPMINMainParams1.width = width * 89 / 100;
        linearPMINMainParams1.height = height * 20 / 100;
        linearPMINMainParams1.setMargins(width * 2 / 100, width * 1 / 100, width * 2 / 100, width * 1 / 100);
        mLinearPMINLayout.setLayoutParams(linearPMINMainParams1);


        LinearLayout.LayoutParams linearpminSubParams10 = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        linearpminSubParams10.width = width * 45 / 100;
        linearpminSubParams10.height = height * 20 / 100;
        mSubLinearLayout3.setLayoutParams(linearpminSubParams10);
        //  mSubLinearLayout3.setPadding(width * 1 / 100, width * 1 / 100, width * 1 / 100, width * 1 / 100);


        LinearLayout.LayoutParams linearpminSubParams101 = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        linearpminSubParams101.width = width * 44 / 100;
        linearpminSubParams101.height = height * 20 / 100;
        mSubLinearLayout4.setLayoutParams(linearpminSubParams10);

        //   mSubLinearLayout4.setPadding(width * 1 / 100, width * 1 / 100, width * 1 / 100, width * 1 / 100);

        LinearLayout.LayoutParams txtpminViewParams10 = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        txtpminViewParams10.width = width * 45 / 100;
        txtpminViewParams10.height = height * 5 / 100;
        mParkTimeTxtView.setLayoutParams(txtpminViewParams10);
        mParkTimeTxtView.setPadding(width * 1 / 2 / 100, height * 1 / 2 / 100, height * 1 / 2 / 100, height * 1 / 2 / 100);

        mParkedTimeValueTxtView.setLayoutParams(txtpminViewParams10);
        mParkedTimeValueTxtView.setWidth(width * 44 / 100);
        mParkedTimeValueTxtView.setGravity(Gravity.CENTER | Gravity.RIGHT);
        mParkedTimeValueTxtView.setPadding(width * 1 / 2 / 100, height * 1 / 2 / 100, height * 1 / 100, height * 1 / 2 / 100);

        mMovingTimeTxtView.setLayoutParams(txtpminViewParams10);
        mMovingTimeTxtView.setPadding(width * 1 / 2 / 100, height * 1 / 2 / 100, height * 1 / 2 / 100, height * 1 / 2 / 100);

        mMovingTimeValueTxtView.setLayoutParams(txtpminViewParams10);
        mMovingTimeValueTxtView.setWidth(width * 44 / 100);
        mMovingTimeValueTxtView.setGravity(Gravity.CENTER | Gravity.RIGHT);
        mMovingTimeValueTxtView.setPadding(width * 1 / 2 / 100, height * 1 / 100, height * 1 / 2 / 100, height * 1 / 2 / 100);

        mIdleTimeTxtView.setLayoutParams(txtpminViewParams10);
        mIdleTimeTxtView.setPadding(width * 1 / 2 / 100, height * 1 / 2 / 100, height * 1 / 2 / 100, height * 1 / 2 / 100);

        mIdleTimeValueTxtView.setLayoutParams(txtpminViewParams10);
        mIdleTimeValueTxtView.setWidth(width * 44 / 100);
        mIdleTimeValueTxtView.setGravity(Gravity.CENTER | Gravity.RIGHT);
        mIdleTimeValueTxtView.setPadding(width * 1 / 2 / 100, height * 1 / 2 / 100, height * 1 / 100, height * 1 / 2 / 100);

        mNoDataTimeTxtView.setLayoutParams(txtpminViewParams10);
        mNoDataTimeTxtView.setPadding(width * 1 / 2 / 100, height * 1 / 2 / 100, height * 1 / 2 / 100, height * 1 / 2 / 100);

        mNoDataTimeValueTxtView.setLayoutParams(txtpminViewParams10);
        mNoDataTimeValueTxtView.setWidth(width * 44 / 100);
        mNoDataTimeValueTxtView.setGravity(Gravity.CENTER | Gravity.RIGHT);
        mNoDataTimeValueTxtView.setPadding(width * 1 / 2 / 100, height * 1 / 100, height * 1 / 2 / 100, height * 1 / 2 / 100);


        LinearLayout.LayoutParams linearChartParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        linearChartParams.width = width * 89 / 100;
        linearChartParams.height = height * 25 / 100;
        linearChartParams.setMargins(width * 2 / 100, width * 1 / 100, width * 2 / 100, width * 1 / 100);
        mLinearChartContainerLayout.setLayoutParams(linearChartParams);
        mLinearChartContainerLayout.setGravity(Gravity.CENTER);

        LinearLayout.LayoutParams linearBottomLayoutParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        linearBottomLayoutParams.width = width * 89 / 100;
        linearBottomLayoutParams.height = height * 16 / 100;
        linearBottomLayoutParams.setMargins(width * 2 / 100, width * 1 / 100, width * 2 / 100, width * 2 / 100);
        mLinearBottomLayout.setLayoutParams(linearBottomLayoutParams);


        LinearLayout.LayoutParams linearPlayBackParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        linearPlayBackParams.width = width * 89 / 100;
        linearPlayBackParams.height = height * 8 / 100;

        mSubLinearLayout5.setLayoutParams(linearPlayBackParams);
        mSubLinearLayout6.setLayoutParams(linearPlayBackParams);

        LinearLayout.LayoutParams txtPlayNavParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        txtPlayNavParams.width = width * 74 / 100;
        txtPlayNavParams.height = height * 8 / 100;
        mPlayBackTxtView.setLayoutParams(txtPlayNavParams);
        mPlayBackTxtView.setGravity(Gravity.CENTER | Gravity.LEFT);
        mPlayBackTxtView.setPadding(width * 1 / 2 / 100, height * 1 / 2 / 100, height * 1 / 2 / 100, height * 1 / 2 / 100);
        mNaviTxtView.setLayoutParams(txtPlayNavParams);
        mNaviTxtView.setGravity(Gravity.CENTER | Gravity.LEFT);
        mNaviTxtView.setPadding(width * 1 / 2 / 100, height * 1 / 2 / 100, height * 1 / 2 / 100, height * 1 / 2 / 100);


        LinearLayout.LayoutParams txtImageParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        txtImageParams.width = width * 15 / 100;
        txtImageParams.height = height * 8 / 100;

        mPlayBackImage.setLayoutParams(txtImageParams);

        mPlayBackImage.setPadding(width * 1 / 2 / 100, height * 1 / 2 / 100, height * 1 / 2 / 100, height * 1 / 2 / 100);
        mNavigationImage.setLayoutParams(txtImageParams);
        mNavigationImage.setPadding(width * 1 / 2 / 100, height * 1 / 2 / 100, height * 1 / 2 / 100, height * 1 / 2 / 100);


//
        LinearLayout.LayoutParams horiViewParams1 = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        horiViewParams1.width = width;
        horiViewParams1.height = height * 1 / 3 / 100;
        mHoriView1.setLayoutParams(horiViewParams1);
        mHoriView2.setLayoutParams(horiViewParams1);

        LinearLayout.LayoutParams veriViewParams1 = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        veriViewParams1.width = width * 1 / 2 / 100;
//        veriViewParams1.height = height * 7 / 100;
        mTimeVerView.setLayoutParams(veriViewParams1);
        mAddsVerView.setLayoutParams(veriViewParams1);


        if (width >= 600) {

        } else if (width > 501 && width < 600) {


        } else if (width > 260 && width < 500) {


        } else if (width <= 260) {


        }

    }


}
