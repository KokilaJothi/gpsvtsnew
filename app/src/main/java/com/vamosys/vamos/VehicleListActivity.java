package com.vamosys.vamos;

import android.Manifest;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.github.mikephil.charting.utils.MPPointF;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.vamosys.adapter.PaymentDueAdapter;
import com.vamosys.adapter.VehicleListAdapter;
import com.vamosys.imageLoader.ImageLoader;
import com.vamosys.model.DataBaseHandler;
import com.vamosys.model.PaymentDueDto;
import com.vamosys.model.VehicleData;
import com.vamosys.model.VehicleLocationsEnv;
import com.vamosys.services.BackgroundService;
import com.vamosys.utils.CommonManager;
import com.vamosys.utils.ConnectionDetector;
import com.vamosys.utils.Constant;
import com.vamosys.utils.DaoHandler;
import com.vamosys.utils.DrawerArrowDrawable;
import com.vamosys.utils.HttpConfig;
import com.vamosys.utils.MyCustomProgressDialog;
import com.vamosys.utils.PiechartLabelFormatter;
import com.vamosys.utils.TypefaceUtil;
import com.vamosys.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.regex.Pattern;

public class VehicleListActivity extends AppCompatActivity implements View.OnClickListener, SwipeRefreshLayout.OnRefreshListener {
    //    protected static final int END = 8388613;
//    protected static final int START = 8388611;
    ListView mLstVehicleView;
    Spinner mGroupSpinner;
    List<VehicleLocationsEnv> mVehicleGroupList = new ArrayList<VehicleLocationsEnv>();
    List<VehicleData> mVehicleLocationList = new ArrayList<VehicleData>();
    List<VehicleData> mVehicleLocationListAdapter = new ArrayList<VehicleData>();
    List<String> mGroupList = new ArrayList<String>();
    List<String> mGroupSpinnerList = new ArrayList<String>();
    List<PaymentDueDto> mPaymentDueList = new ArrayList<PaymentDueDto>();
    Dialog dialog;
    Boolean isupdate = false;

    String mSelectedGroup, mUserId;
    ProgressDialog progressDialog;
    DBHelper dbhelper;
    LinearLayout lay_home, lay_setting, lay_notification, lay_vehicle, lay_vehicle_nearby, lay_signOut,lay_policy, lay_kmsSummary, lay_app_version, lay_geo_fences,
            layout_support, lay_driverList, lay_executive_report,lay_tollgate_report, lay_geo_fences_report,lay_fuel_summa_report, lay_add_geo_fences_report, lay_billing,lay_tank_summa_report;
    int width, height;
    TextView home, setting, notification, vehicle, vehicle_nearby, signOut, policy,navigation, schoolName, imgTitle, kms_Summary, app_version_txt, geo_fences, support_details,
            driver_list_txt, txt_exec_report,txt_toll_report, geo_fences_report, fuel_summary_report, add_geo_fences_report, billing,fuel_tank_report;
    ImageView slide_home, slide_settings, slide_notification, slide_kms, slide_sites, slide_support, slide_vehicle_list, slide_vehicle_list_nearby, slide_logout,slide_policy, slide_app_version,
            slide_driver_list, slide_exec_report,slide_toll_report, slide_geofence_report,slide_fuelsumm_report, slide_add_geofence_report, slide_billing,slide_tanksumm_report;
    TextView view_home, view_setting, view_notification, view_vehicle, view_vehicle_nearby, view_signOut, view_kms_summary, view_app_version, view_geo_fences, view_support_details,view_policy,
            view_driver_list, view_exec_report,view_toll_report, view_geo_fences_report,view_fuel_summary_report, view_add_geo_fences_report, view_billing,view_tank_summary_report;
    TextView $TxtNoOfVehicle, $TxtOnlineVehicle, $TxtOfflineVehicle,
            $TxtNoOfVehicleValue, $TxtOnlineVehicleValue, $TxtTitle,
            $TxtOfflineVehicleValue, $TxtNodataVehicle, $TxtNodataVehicleValue, $TxtMovingVehicle, $TxtMovingVehicleValue;


    LinearLayout $LayoutVehicle, $LayoutOnlineVehicle, $LayoutOfflineVehicle, $SideMenuLayout, $SideMenuLayoutEnd, drawer_layout, mVehicleListHeaderLAyout, $LayoutNodataVehicle, $LayoutMovingVehicle;


    View $View1, $View2, $View3, $View4;
    EditText mEdtSearch;
    ImageView mImgEdtClose, mImgSearch;

    ImageView $SideMenuImage;
    ListView $ListViewSideMenuEnd;
    DrawerLayout drawer;
    private float offset;
    private boolean flipped;
    SharedPreferences sp;

    static Timer timer;

    private DrawerArrowDrawable drawerArrowDrawable;

    boolean mIsSearchEnabled = false;
    LinearLayout nav_img_search_layout, nav_img_close_layout;
    String imgUrl = null;
    ImageLoader imgLoader;
    private SwipeRefreshLayout swipeRefreshLayout;
    ConnectionDetector cd;
    TextView mTxtLastSync;
    LinearLayout vehicle_list_header_title_layout, vehicle_list_header_title_edit_layout;

    boolean mIsAutRefreshEnabled = false;
    String mAutoRefreshValue = "10";
    PieChart pieChart;
    LinearLayout mChartContainerLayout;
    ImageView mImgChartClose;
    boolean mIsDashBoardShows = false;
    int mTotalVehi = 0, mParkedVehi = 0, mMovingVehicle = 0, mIdleVehicle = 0, mNodataVehicle = 0;
    private Toolbar mToolbar;

    private static final String TAG = VehicleListActivity.class.getName();
    private static final int WRITE_EXTERNAL_STORAGE_REQUEST_CODE = 101;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vehicle_list);
        sp = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mToolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_action_name));
        setSupportActionBar(mToolbar);


        imgUrl = sp.getString("app_logo", "");

        mIsAutRefreshEnabled = sp.getBoolean("is_auto_refresh_enabled", false);

        mAutoRefreshValue = sp.getString("auto_refresh_interval", "");

        String mFirstActivity = sp.getString("first_activity", "Vehicle");

        if (Constant.mIsFirstTime) {
            if (mFirstActivity != null && mFirstActivity.length() > 0) {
                if (mFirstActivity.equalsIgnoreCase("Vehicle")) {
                    mIsDashBoardShows = false;
                } else if (mFirstActivity.equalsIgnoreCase("DashBoard")) {
                    mIsDashBoardShows = true;
                }
            }
        } else {
            mIsDashBoardShows = false;

        }
        Constant.mIsFirstTime = false;
        if (sp.getString("ip_adds", "") != null) {
            if (sp.getString("ip_adds", "").trim().length() > 0) {
                Const.API_URL = sp.getString("ip_adds", "");
            }
        }

//        update3DmapEnabled(true);

//        System.out.println("The ip adds is ::::" + Const.API_URL);

        imgLoader = new ImageLoader(VehicleListActivity.this);
        cd = new ConnectionDetector(getApplicationContext());
        init();
        screenArrange();
        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        dbSetup();

        mIsSearchEnabled = false;
        queryUserDB();


        isStoragePermissionGranted();

        isMyServiceRunning(VehicleTrackingService.class);

        if (mIsAutRefreshEnabled) {
            long l = Integer.valueOf(mAutoRefreshValue) * 1000;
            loadDatas(l);
        } else {
            if (timer != null) {
                timer.cancel();
            }
        }

//        if (sp.getBoolean("payment_due_load", true)) {
//            //load invoice
//            new getPaymentDue().execute();
//        }

        mGroupSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                                               @Override
                                               public void onItemSelected(AdapterView<?> parent,
                                                                          View view, int pos, long id) {
                                                   // TODO Auto-generated method stub
                                                   mSelectedGroup = mGroupList.get(pos);
                                                   saveSelectedGroup(mGroupSpinnerList.get(pos));
                                                   String url = null;

                                                   if (mSelectedGroup.equalsIgnoreCase("Select")) {
                                                       url = Const.API_URL + "mobile/getVehicleLocations?userId=" + mUserId + "&macid=" + getStdTableValue("macid") + "&appid=" + getStdTableValue("appid");

                                                   } else {
                                                       url = Const.API_URL + "mobile/getVehicleLocations?userId=" + mUserId + "&group=" + mSelectedGroup + "&macid=" + getStdTableValue("macid") + "&appid=" + getStdTableValue("appid");
                                                   }
                                                   Constant.SELECTED_GROUP = mSelectedGroup;

                                                   if (cd.isConnectingToInternet()) {
                                                       new PullVehicleInformation().execute(url);
                                                   } else {
                                                       Toast.makeText(getApplicationContext(),
                                                               ""+getResources().getString(R.string.network_connection),
                                                               Toast.LENGTH_SHORT).show();
                                                       setData(""+getResources().getString(R.string.no_record));
                                                   }
                                               }

                                               @Override
                                               public void onNothingSelected(AdapterView<?> arg0) {
                                                   // TODO Auto-generated method stub

                                               }

                                           }

                );


        mLstVehicleView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View v, int pos,
                                    long arg3) {
                // TODO Auto-generated method stub
                Constant.SELECTED_VEHICLE_LOCATION_OBJECT = mVehicleLocationListAdapter.get(pos);
                if (Constant.SELECTED_VEHICLE_LOCATION_OBJECT != null) {
                    Constant.SELECTED_USER_ID = mUserId;
                    Constant.VEHILCE_LIST_DATA = mVehicleLocationListAdapter;

//                if (mVehicleLocationListAdapter.get(pos).getVehicleId() != null) {
                    Constant.SELECTED_VEHICLE_ID = mVehicleLocationListAdapter.get(pos).getVehicleId();

                    //   System.out.println("Hi vehicle short name is 0000" + mVehicleLocationListAdapter.get(pos).getShortName() + " :: " + mVehicleLocationListAdapter.get(pos).getVehicleId());
//                }
                    Constant.SELECTED_VEHICLE_SHORT_NAME = mVehicleLocationListAdapter.get(pos).getShortName();
                    Constant.SELECTED_ODO_DISTANCE = String.valueOf(mVehicleLocationListAdapter.get(pos).getOdoDistance());
                    Constant.SELECTED_AC_STATUS = mVehicleLocationListAdapter.get(pos).getVehicleBusy();
                    Constant.SELECTED_DRIVER_NAME = mVehicleLocationListAdapter.get(pos).getDriverName();
                    Constant.SELECTED_DRIVER_CONTACT_NUMBER = mVehicleLocationListAdapter.get(pos).getDriverMobile();
                    Log.d("vehiclemobile", "" + mVehicleLocationListAdapter.get(pos).getDriverMobile());
                    Constant.SELECTED_VEHICLE_TYPE = mVehicleLocationListAdapter.get(pos).getVehicleType();
                    Constant.SELECTED_MAIN_VEHICLE_LAT = mVehicleLocationListAdapter.get(pos).getLatitude();
                    Constant.SELECTED_MAIN_VEHICLE_LNG = mVehicleLocationListAdapter.get(pos).getLongitude();
                    Constant.SELECTED_VEHICLE_ADDRESS = mVehicleLocationListAdapter.get(pos).getAddress();
//                And datas for info activity
                    if (mVehicleLocationListAdapter.get(pos).getExpired() != null) {

                        if (mVehicleLocationListAdapter.get(pos).getExpired().equalsIgnoreCase("No")) {
                            startActivity(new Intent(VehicleListActivity.this,
                                    MapMenuActivity.class));
                            finish();
                        } else {
                            Toast.makeText(VehicleListActivity.this, ""+getResources().getString(R.string.contact_sales), Toast.LENGTH_SHORT).show();
//                            Toast.makeText(VehicleListActivity.this, "Vehicle expired! Please select another vehicle.", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        startActivity(new Intent(VehicleListActivity.this,
                                MapMenuActivity.class));
                        finish();
                    }
                }

            }
        });

        /**
         * Showing Swipe Refresh animation on activity create
         * As animation won't start on onCreate, post runnable is used
         */
//        swipeRefreshLayout.post(new Runnable() {
//                                    @Override
//                                    public void run() {
//                                        swipeRefreshLayout.setRefreshing(true);
//                                        String url = Const.API_URL + "mobile/getVehicleLocations?userId=" + mUserId + "&group=" + mSelectedGroup + "&macid=" + getStdTableValue("macid") + "&appid=" + getStdTableValue("appid");
//                                        if (cd.isConnectingToInternet()) {
//                                            new PullVehicleInformation().execute(url);
//                                        } else {
//                                            Toast.makeText(getApplicationContext(),
//                                                    "Please check your network connection",
//                                                    Toast.LENGTH_SHORT).show();
//                                            setData("No Record");
//                                        }
//                                    }
//                                }
//        );


        /**Drawwer Functions*/
        final ImageView imageMenu = (ImageView) findViewById(R.id.test_view_Back);
        LinearLayout nav_img_lay = (LinearLayout) findViewById(R.id.nav_img_layout);


        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        height = displayMetrics.heightPixels;
        width = displayMetrics.widthPixels;
        LinearLayout.LayoutParams imgMenu = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        imgMenu.height = height * 5 / 100;
        imgMenu.width = width * 12 / 100;
        imgMenu.gravity = (Gravity.CENTER);
        // imgMenu.bottomMargin = height * 1/2 / 100;
        imgMenu.leftMargin = width * 2 / 100;
        imageMenu.setLayoutParams(imgMenu);

        LinearLayout.LayoutParams navImgBck = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        navImgBck.height = height * 10 / 100;
        navImgBck.width = height * 10 / 100;
        // navImgBck.gravity = (Gravity.CENTER);
        // imgMenu.bottomMargin = height * 1/2 / 100;
        // imgMenu.leftMargin = width * 4 / 100;
        nav_img_lay.setLayoutParams(imgMenu);
//        nav_img_search_layout.setLayoutParams(imgMenu);
//        nav_img_close_layout.setLayoutParams(imgMenu);

//        LinearLayout.LayoutParams drawerParams = new LinearLayout.LayoutParams(
//                LinearLayout.LayoutParams.WRAP_CONTENT,
//                LinearLayout.LayoutParams.WRAP_CONTENT);
//        drawerParams.height = height;
//        drawerParams.width = width * 55 / 100;
//        // imgMenu.gravity = (Gravity.CENTER);
//        // imgMenu.bottomMargin = height * 1/2 / 100;
//        // imgMenu.leftMargin = width * 4 / 100;
//          drawer_layout.setLayoutParams(drawerParams);

        final Resources resources = getResources();
        drawerArrowDrawable = new DrawerArrowDrawable(resources);
        drawerArrowDrawable.setStrokeColor(resources.getColor(R.color.white));

        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //What to do on back clicked


                try {
                    if (drawer.isDrawerVisible(GravityCompat.START)) {
                        drawer.closeDrawer(GravityCompat.START);
                    } else {
                        drawer.openDrawer(GravityCompat.START);
                    }
                    if (drawer.isDrawerVisible(GravityCompat.END)) {
                        drawer.closeDrawer(GravityCompat.END);
                    }
                } catch (Exception e) {
                    // TODO: handle exception
                    e.printStackTrace();
                }

            }
        });

        //imageMenu.setImageDrawable(drawerArrowDrawable);
        drawer.setDrawerListener(new DrawerLayout.SimpleDrawerListener() {
            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                offset = slideOffset;

                // Sometimes slideOffset ends up so close to but not quite 1 or
                // 0.
                if (slideOffset >= 1.000) {
                    flipped = true;
                    drawerArrowDrawable.setFlip(flipped);
                } else if (slideOffset <= .000) {
                    flipped = false;
                    drawerArrowDrawable.setFlip(flipped);
                }

                drawerArrowDrawable.setParameter(offset);
            }
        });
//        imageMenu.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View arg0) {
//                // TODO Auto-generated method stub
//                try {
//                    if (drawer.isDrawerVisible(GravityCompat.START)) {
//                        drawer.closeDrawer(GravityCompat.START);
//                    } else {
//                        drawer.openDrawer(GravityCompat.START);
//                    }
//                    if (drawer.isDrawerVisible(GravityCompat.END)) {
//                        drawer.closeDrawer(GravityCompat.END);
//                    }
//                } catch (Exception e) {
//                    // TODO: handle exception
//                    e.printStackTrace();
//                }
//            }
//        });

        nav_img_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                try {
                    if (drawer.isDrawerVisible(GravityCompat.START)) {
                        drawer.closeDrawer(GravityCompat.START);
                    } else {
                        drawer.openDrawer(GravityCompat.START);
                    }
                    if (drawer.isDrawerVisible(GravityCompat.END)) {
                        drawer.closeDrawer(GravityCompat.END);
                    }
                } catch (Exception e) {
                    // TODO: handle exception
                    e.printStackTrace();
                }
            }
        });


        /**
         * Enabling Search Filter
         * */
        mEdtSearch.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence cs, int arg1, int arg2,
                                      int arg3) {
                // When user changed the Text
                // DailySearch.this.arrayAutoListAdapter.getFilter().filter(cs);
                if (cs.length() > 0) {
                    getSearchText(String.valueOf(cs).trim());
                } else {
                    // setData("ALL");
                    getSearchText(null);
                }
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1,
                                          int arg2, int arg3) {
                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub
            }

        });

        loadDatas(60000);

        userverification();
        isupdate = sp.getBoolean("force_update",false);
        Log.d("isupdate",""+isupdate);

        new reportrestrict().execute("");

//        if (isupdate == false){
//            forceUpdate();
//        }else {
//
//
//            Log.d("forceupdat","true");
//        }
//        forceUpdate();

    }
    private class reportrestrict extends AsyncTask<String, String, String> {

        protected void onPreExecute() {
            super.onPreExecute();
//            progressDialog = MyCustomProgressDialog.ctor(MapMenuActivity.this);
//            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {
            try {
                HttpConfig httpConfig = new HttpConfig();
                return httpConfig.httpGet(Const.API_URL +"mobile/getReportsRestrictForMobile?userId=" +mUserId);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {

            super.onPostExecute(result);
//            if (progressDialog.isShowing()) {
//                progressDialog.dismiss();
//            }
            if (result != null && result.length() > 0) {
                //  Log.d("parkingresult", "" + result);
                try {
                    JSONObject jsonObject = new JSONObject(result.trim());
                    if (jsonObject.has("TEMPERATURE")){
                        Log.d("TEMPERATURE",""+jsonObject.getString("TEMPERATURE"));
                        if (jsonObject.getString("TEMPERATURE").equalsIgnoreCase("false")){

                        }
                        if (jsonObject.getString("TEMPERATURE").equalsIgnoreCase("true")){
                        }
                    }
                    if (jsonObject.has("FUEL")){
                        Log.d("FUEL",""+jsonObject.getString("FUEL"));
                        if (jsonObject.getString("FUEL").equalsIgnoreCase("false")){
                            lay_fuel_summa_report.setVisibility(View.GONE);
                            view_fuel_summary_report.setVisibility(View.GONE);
                            view_tank_summary_report.setVisibility(View.GONE);
                            lay_tank_summa_report.setVisibility(View.GONE);

                        }
                        if (jsonObject.getString("FUEL").equalsIgnoreCase("true")){
                            lay_fuel_summa_report.setVisibility(View.VISIBLE);
                            view_fuel_summary_report.setVisibility(View.VISIBLE);
                            view_tank_summary_report.setVisibility(View.VISIBLE);
                            lay_tank_summa_report.setVisibility(View.VISIBLE);
                        }
                    }


                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        }
    }

    private void userverification() {
        startService(new Intent(this, BackgroundService.class));
    }

    public boolean isStoragePermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            if ((checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) && (checkSelfPermission(android.Manifest.permission.READ_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED)) {
//                Log.v(TAG, "Permission is granted");
                return true;
            } else {

//                Log.v(TAG, "Permission is revoked");
                ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE}, WRITE_EXTERNAL_STORAGE_REQUEST_CODE);
                return false;
            }
        } else { //permission is automatically granted on sdk<23 upon installation

//            Log.v(TAG, "Permission is granted");
            return true;
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {


        switch (requestCode) {

            case WRITE_EXTERNAL_STORAGE_REQUEST_CODE:
//                if (permissions.length != 1 || grantResults.length != 1) {
//                    throw new RuntimeException("Error on requesting file write permission.");
//                }
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                    Toast.makeText(this, "File write permission granted",
//                            Toast.LENGTH_SHORT).show();
//                    callCameraPermission();
                } else {
                    Toast.makeText(getApplicationContext(), ""+getResources().getString(R.string.storageMap), Toast.LENGTH_SHORT).show();

                    isStoragePermissionGranted();

                }
                // No need to start camera here; it is handled by onResume
                break;

        }


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_home, menu);
//        searchItem = menu.findItem(R.id.search);
//        searchItem.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
//            @Override
//            public boolean onMenuItemClick(MenuItem item) {
//                mSearchView.display();
//                openKeyboard();
//                return true;
//            }
//        });
//        if (searchActive)
//            mSearchView.display();
//        return true;


        MenuItem myActionMenuItem = menu.findItem(R.id.search);
        final SearchView searchView = (SearchView) myActionMenuItem.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (TextUtils.isEmpty(newText)) {
                    // adapter.filter("");
                    //  listView.clearTextFilter();
                    getSearchText(null);
                } else {
                    // adapter.filter(newText);


                    getSearchText(String.valueOf(newText).trim());


                }
                return true;
            }
        });

        return true;


    }

//    public void formChart() {
//        mIsDashBoardShows = true;
//        mChartContainerLayout.setVisibility(View.VISIBLE);
//        mLstVehicleView.setVisibility(View.GONE);
//
//        ArrayList<Entry> entries = new ArrayList<>();
//        // entries.add(new Entry(mTotalVehi, 0));
//        if (mParkedVehi > 0) {
//            entries.add(new Entry(mParkedVehi, 0));
//        }
//
//        if (mIdleVehicle > 0) {
//            entries.add(new Entry(mIdleVehicle, 1));
//        }
//
//        if (mMovingVehicle > 0) {
//            entries.add(new Entry(mMovingVehicle, 2));
//        }
//
//        if (mNodataVehicle > 0) {
//            entries.add(new Entry(mNodataVehicle, 3));
//        }
//
//
//        PieDataSet dataset = new PieDataSet(entries, "");
//
//        ArrayList<String> labels = new ArrayList<String>();
////        labels.add("All");
//        if (mParkedVehi > 0) {
//            labels.add("Parking");
//        }
//        if (mIdleVehicle > 0) {
//            labels.add("Idle");
//        }
//        if (mMovingVehicle > 0) {
//            labels.add("Moving");
//        }
//        if (mNodataVehicle > 0) {
//            labels.add("No Data");
//        }
//
//        PieData data = new PieData(labels, dataset);
//        dataset.setColors(ColorTemplate.COLORFUL_COLORS); //
//        pieChart.setDescription("Vehicles");
//        pieChart.setData(data);
//
//        pieChart.animateY(5000);
//
//
//    }

    public void formDataSet() {
        if (mParkedVehi > 0 || mMovingVehicle > 0 || mIdleVehicle > 0 || mNodataVehicle > 0) {
            mIsDashBoardShows = true;
            mChartContainerLayout.setVisibility(View.VISIBLE);
            mLstVehicleView.setVisibility(View.GONE);

            ArrayList<PieEntry> entries = new ArrayList<>();
            ArrayList<String> LabelList = new ArrayList<>();
            // entries.add(new Entry(mTotalVehi, 0));
            if (mParkedVehi > 0) {
                LabelList.add(""+getResources().getString(R.string.Parking));
//                LabelList.addAll(getLabelList("Parking", mParkedVehi));
                entries.add(new PieEntry(mParkedVehi, ""+getResources().getString(R.string.Parking)));

            }

            if (mIdleVehicle > 0) {
                LabelList.add(""+getResources().getString(R.string.idle));
//                LabelList.addAll(getLabelList("Idle", mIdleVehicle));
                entries.add(new PieEntry(mIdleVehicle, ""+getResources().getString(R.string.idle)));
//                LabelList.add("Idle");

            }
//            mMovingVehicle=2;
            if (mMovingVehicle > 0) {
                LabelList.add(""+getResources().getString(R.string.moving));
//                LabelList.addAll(getLabelList("Moving", mMovingVehicle));
                entries.add(new PieEntry(mMovingVehicle, ""+getResources().getString(R.string.moving)));
//                LabelList.add("Moving");

            }
//        mNodataVehicle = 2;
            if (mNodataVehicle > 0) {
                LabelList.add(""+getResources().getString(R.string.no_data));
//                LabelList.addAll(getLabelList("No Data", mNodataVehicle));
                entries.add(new PieEntry(mNodataVehicle, ""+getResources().getString(R.string.no_data)));
//                LabelList.add("No Data");

            }

//            Log.i("Label sizzz ", "Size " + LabelList.size() + " p" + mParkedVehi + " m" + mMovingVehicle + " u" + mIdleVehicle + " s" + mNodataVehicle);

            PieDataSet dataset = new PieDataSet(entries, "");
            dataset.setDrawIcons(false);

            dataset.setSliceSpace(3f);
            dataset.setIconsOffset(new MPPointF(0, 40));
            dataset.setSelectionShift(5f);


            ArrayList<Integer> colors = new ArrayList<Integer>();


//        for (int c : ColorTemplate.VORDIPLOM_COLORS)
//            colors.add(c);
//
//        for (int c : ColorTemplate.JOYFUL_COLORS)
//            colors.add(c);

            for (int c : ColorTemplate.COLORFUL_COLORS)
                colors.add(c);

//        for (int c : ColorTemplate.LIBERTY_COLORS)
//            colors.add(c);
//
//        for (int c : ColorTemplate.PASTEL_COLORS)
//            colors.add(c);

            colors.add(ColorTemplate.getHoloBlue());

            dataset.setColors(colors);

//        dataset.setColors(new int[]{R.color.black, R.color.green, R.color.red, R.color.yellow}, getApplicationContext());


            ArrayList<String> labels = new ArrayList<String>();
//        labels.add("All");

            if (mIdleVehicle > 0) {
                labels.add(""+getResources().getString(R.string.idle));
            }
            if (mParkedVehi > 0) {
                labels.add(""+getResources().getString(R.string.Parking));
            }
            if (mNodataVehicle > 0) {
                labels.add(""+getResources().getString(R.string.no_data));
            }
            if (mMovingVehicle > 0) {
                labels.add(""+getResources().getString(R.string.moving));
            }

//        dataset.setValueFormatter(new PiechartLabelFormatter(LabelList));

//        PieData data = new PieData(labels, dataset);
//        dataset.setColors(ColorTemplate.COLORFUL_COLORS); //
//        pieChart.setDescription("Vehicles");
//        pieChart.setData(data);
//
//        pieChart.animateY(5000);


            PieData data = new PieData(dataset);
//        data.setValueFormatter(new PiechartLabelFormatter());

            //  data.setValueFormatter(new PercentFormatter());
            data.setValueFormatter(new PiechartLabelFormatter(LabelList));
            data.setValueTextSize(11f);
            data.setValueTextColor(Color.WHITE);
//        data.setValueTypeface(mTfLight);
            pieChart.setData(data);

            // undo all highlights
            pieChart.highlightValues(null);

            pieChart.invalidate();
        }

    }

    private List<String> getLabelList(String position, int count) {
        List<String> mPosList = new ArrayList<>();
        for (int i = 0; i < count; i++) {
            mPosList.add(position);
        }
        return mPosList;
    }

    private void formBarchart() {

        pieChart.clear();
        pieChart.notifyDataSetChanged();


        //  pieChart.setUsePercentValues(true);
        pieChart.getDescription().setEnabled(false);
        pieChart.setExtraOffsets(5, 10, 5, 5);

        pieChart.setDragDecelerationFrictionCoef(0.95f);

        // mChart.setCenterTextTypeface(mTfLight);
//        pieChart.setCenterText(generateCenterSpannableText());
        pieChart.setCenterText(""+getResources().getString(R.string.vehicle_list));

        pieChart.setDrawHoleEnabled(true);
        // pieChart.setHoleColor(Color.WHITE);

        //   pieChart.setTransparentCircleColor(Color.WHITE);
        pieChart.setTransparentCircleAlpha(110);

        pieChart.setHoleRadius(58f);
        pieChart.setTransparentCircleRadius(61f);

        pieChart.setDrawCenterText(true);

        pieChart.setRotationAngle(0);
        // enable rotation of the chart by touch
        pieChart.setRotationEnabled(true);
        pieChart.setHighlightPerTapEnabled(true);

        // mChart.setUnit(" €");
        // mChart.setDrawUnitsInChart(true);

        // add a selection listener
        // pieChart.setOnChartValueSelectedListener(this);

        formDataSet();

        pieChart.animateY(1400, Easing.EasingOption.EaseInOutQuad);
        // mChart.spin(2000, 0, 360);


        Legend l = pieChart.getLegend();
        l.setVerticalAlignment(Legend.LegendVerticalAlignment.TOP);
        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.RIGHT);
        l.setOrientation(Legend.LegendOrientation.VERTICAL);
//        l.setEnabled(true);
//        LegendEntry le=new LegendEntry();
//
//        l.setCustom(new String[] { "Parking", "bbbbb", "ccccc"});

//        List<LegendEntry> entries = new ArrayList<>();
//
//        for (int i = 0; i < 4; i++) {
//            LegendEntry entry = new LegendEntry();
//            // entry.formColor = colorList.get(i);
//            entry.label = "Test " + i;
//            entries.add(entry);
//        }
//
//        l.setCustom(entries);

        l.setDrawInside(false);
//        l.setXEntrySpace(7f);
//        l.setYEntrySpace(0f);
        l.setYOffset(0f);

        // entry label styling
        pieChart.setEntryLabelColor(Color.WHITE);
        // mChart.setEntryLabelTypeface(mTfRegular);
        pieChart.setEntryLabelTextSize(12f);

//        pieChart.animateY(2500);

    }

//    private SpannableString generateCenterSpannableText() {
//
//        SpannableString s = new SpannableString("MPAndroidChart\ndeveloped by Philipp Jahoda");
//        s.setSpan(new RelativeSizeSpan(1.7f), 0, 14, 0);
//        s.setSpan(new StyleSpan(Typeface.NORMAL), 14, s.length() - 15, 0);
//        s.setSpan(new ForegroundColorSpan(Color.GRAY), 14, s.length() - 15, 0);
//        s.setSpan(new RelativeSizeSpan(.8f), 14, s.length() - 15, 0);
//        s.setSpan(new StyleSpan(Typeface.ITALIC), s.length() - 14, s.length(), 0);
//        s.setSpan(new ForegroundColorSpan(ColorTemplate.getHoloBlue()), s.length() - 14, s.length(), 0);
//        return s;
//    }


    public void init() {
        pieChart = (PieChart) findViewById(R.id.dashboard_piechart);
        mChartContainerLayout = (LinearLayout) findViewById(R.id.layout_vehicle_list_chart_container);
        mImgChartClose = (ImageView) findViewById(R.id.img_vehicle_list_close);
        mImgChartClose.setOnClickListener(this);
        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_layout);
        swipeRefreshLayout.setOnRefreshListener(this);
        mTxtLastSync = (TextView) findViewById(R.id.txt_last_sync);
        mTxtLastSync.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        vehicle_list_header_title_layout = (LinearLayout) findViewById(R.id.vehicle_list_header_title_layout);
        vehicle_list_header_title_edit_layout = (LinearLayout) findViewById(R.id.vehicle_list_header_title_edit_layout);
        mVehicleListHeaderLAyout = (LinearLayout) findViewById(R.id.vehicle_list_header_layout);
        $TxtTitle = (TextView) findViewById(R.id.vehicle_list_title);
        mLstVehicleView = (ListView) findViewById(R.id.navigation_list);
        mGroupSpinner = (Spinner) findViewById(R.id.navigation_spinner);
        $TxtNoOfVehicle = (TextView) findViewById(R.id.id_all_vehicles_label);
        $TxtNoOfVehicleValue = (TextView) findViewById(R.id.id_all_vehicles_value);
        $TxtOnlineVehicle = (TextView) findViewById(R.id.id_online_vehicles_label);
        $TxtOnlineVehicleValue = (TextView) findViewById(R.id.id_online_vehicles_value);
        $TxtOfflineVehicle = (TextView) findViewById(R.id.id_offline_vehicles_label);
        $TxtOfflineVehicleValue = (TextView) findViewById(R.id.id_offline_vehicles_value);

        $TxtNodataVehicle = (TextView) findViewById(R.id.id_nodata_vehicles_label);
        $TxtNodataVehicleValue = (TextView) findViewById(R.id.id_nodata_vehicles_value);

        $TxtMovingVehicle = (TextView) findViewById(R.id.id_moving_vehicles_label);
        $TxtMovingVehicleValue = (TextView) findViewById(R.id.id_moving_vehicles_value);

        $TxtTitle.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        $TxtTitle.setVisibility(View.GONE);
        $TxtNoOfVehicle.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        $TxtNoOfVehicleValue.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        $TxtOnlineVehicle.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        $TxtOnlineVehicleValue.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        $TxtOfflineVehicle.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        $TxtOfflineVehicleValue.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));

        $TxtNodataVehicle.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        $TxtNodataVehicleValue.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        $TxtMovingVehicle.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        $TxtMovingVehicleValue.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));

        $LayoutVehicle = (LinearLayout) findViewById(R.id.id_total_vehicles_layout);
        $LayoutOnlineVehicle = (LinearLayout) findViewById(R.id.id_online_vehicles_layout);
        $LayoutOfflineVehicle = (LinearLayout) findViewById(R.id.id_offline_vehicles_layout);
        $LayoutNodataVehicle = (LinearLayout) findViewById(R.id.id_nodata_vehicles_layout);
        $LayoutMovingVehicle = (LinearLayout) findViewById(R.id.id_moving_vehicles_layout);
        // drawer_layout = (LinearLayout) findViewById(R.id.layout_sidelist);
        $SideMenuLayout = (LinearLayout) findViewById(R.id.layout_sidelist);
        $SideMenuImage = (ImageView) findViewById(R.id.layout_sideImage);
        mEdtSearch = (EditText) findViewById(R.id.vehicle_list_search_edt);
        mEdtSearch.setVisibility(View.GONE);
        // mEdtSearch.setImeOptions(EditorInfo.IME_ACTION_DONE);
        mImgEdtClose = (ImageView) findViewById(R.id.vehicle_list_close);
        // mImgEdtClose.setVisibility(View.GONE);
        mImgSearch = (ImageView) findViewById(R.id.vehicle_list_search);

        $View1 = (View) findViewById(R.id.view1_home);
        $View2 = (View) findViewById(R.id.view2_home);
        $View3 = (View) findViewById(R.id.view1_nodata_home);
        $View4 = (View) findViewById(R.id.view1_moving_home);
        $LayoutVehicle.setOnClickListener(this);
        $LayoutOnlineVehicle.setOnClickListener(this);
        $LayoutOfflineVehicle.setOnClickListener(this);

        $LayoutNodataVehicle.setOnClickListener(this);
        $LayoutMovingVehicle.setOnClickListener(this);

        /**Side Menu Initialization*/
        home = (TextView) findViewById(R.id.textView_Home);
        setting = (TextView) findViewById(R.id.textView_settings);
        notification = (TextView) findViewById(R.id.textView_notification);
        vehicle = (TextView) findViewById(R.id.textView_vehicles);
        vehicle_nearby = (TextView) findViewById(R.id.textView_vehicles_nearby);
        signOut = (TextView) findViewById(R.id.textView_signOut);
        policy = (TextView) findViewById(R.id.textView_policy);
        kms_Summary = (TextView) findViewById(R.id.textView_kms_summary);
        app_version_txt = (TextView) findViewById(R.id.textView_application_version);
        geo_fences = (TextView) findViewById(R.id.textView_geo_fences);
        support_details = (TextView) findViewById(R.id.textView_support);
        imgTitle = (TextView) findViewById(R.id.img_title);
        driver_list_txt = (TextView) findViewById(R.id.textView_driver);
        txt_exec_report = (TextView) findViewById(R.id.textView_executive_report);
        txt_toll_report = (TextView) findViewById(R.id.textView_tollgate_report);
        geo_fences_report = (TextView) findViewById(R.id.textView_geo_fences_report);
        fuel_summary_report = (TextView) findViewById(R.id.textView_fuel_summ_report);
        add_geo_fences_report = (TextView) findViewById(R.id.textView_add_geo_fences);
        billing = (TextView) findViewById(R.id.textView_billing);
        fuel_tank_report = (TextView) findViewById(R.id.textView_tank_summ_report);

        view_home = (TextView) findViewById(R.id.view_home);
        view_setting = (TextView) findViewById(R.id.view_settings);
        view_notification = (TextView) findViewById(R.id.view_notification);
        view_vehicle = (TextView) findViewById(R.id.view_vehicles);
        view_vehicle_nearby = (TextView) findViewById(R.id.view_vehicles_nearby);
        view_signOut = (TextView) findViewById(R.id.view_signOut);
        view_kms_summary = (TextView) findViewById(R.id.view_kms_summary);
        view_geo_fences = (TextView) findViewById(R.id.view_geo_fences);
        view_app_version = (TextView) findViewById(R.id.view_application_version);
        view_support_details = (TextView) findViewById(R.id.view_support);
        view_policy = (TextView) findViewById(R.id.view_policy);
        view_driver_list = (TextView) findViewById(R.id.view_driver_list);
        view_exec_report = (TextView) findViewById(R.id.view_executive_report);
        view_toll_report = (TextView) findViewById(R.id.view_tollgate_report);
        view_geo_fences_report = (TextView) findViewById(R.id.view_geo_fences_report);
        view_fuel_summary_report = (TextView) findViewById(R.id.view_fuel_summ_report);
        view_tank_summary_report = (TextView) findViewById(R.id.view_tank_summ_report);
        view_add_geo_fences_report = (TextView) findViewById(R.id.view_add_geo_fences);

        view_billing = (TextView) findViewById(R.id.view_billing);

        lay_home = (LinearLayout) findViewById(R.id.layout_Home);
        lay_setting = (LinearLayout) findViewById(R.id.layout_settings);
        lay_notification = (LinearLayout) findViewById(R.id.layout_notification);
        lay_vehicle = (LinearLayout) findViewById(R.id.layout_vehicles);
        lay_vehicle_nearby = (LinearLayout) findViewById(R.id.layout_vehicles_nearby);
        lay_signOut = (LinearLayout) findViewById(R.id.layout_signOut);
        lay_policy = (LinearLayout) findViewById(R.id.layout_policy);
        lay_kmsSummary = (LinearLayout) findViewById(R.id.layout_kms_summary);
        lay_executive_report = (LinearLayout) findViewById(R.id.layout_executive_report);
        lay_tollgate_report = (LinearLayout) findViewById(R.id.layout_tollgate_report);
        lay_app_version = (LinearLayout) findViewById(R.id.layout_application_version);
        lay_geo_fences = (LinearLayout) findViewById(R.id.layout_geo_fences);
        layout_support = (LinearLayout) findViewById(R.id.layout_support);
        lay_driverList = (LinearLayout) findViewById(R.id.layout_driver_list);
        lay_geo_fences_report = (LinearLayout) findViewById(R.id.layout_geo_fences_report);
        lay_fuel_summa_report = (LinearLayout) findViewById(R.id.layout_fuel_summry_report);
        lay_tank_summa_report = (LinearLayout) findViewById(R.id.layout_tank_summry_report);

        lay_add_geo_fences_report = (LinearLayout) findViewById(R.id.layout_add_geo_fences);
        lay_billing = (LinearLayout) findViewById(R.id.layout_billing);

        nav_img_search_layout = (LinearLayout) findViewById(R.id.nav_img_search_layout);
        nav_img_close_layout = (LinearLayout) findViewById(R.id.nav_img_close_layout);
        nav_img_close_layout.setVisibility(View.GONE);

        lay_home.setOnClickListener(this);
        lay_setting.setOnClickListener(this);
        lay_notification.setOnClickListener(this);
        lay_vehicle.setOnClickListener(this);
        lay_vehicle_nearby.setOnClickListener(this);
        lay_signOut.setOnClickListener(this);
        lay_policy.setOnClickListener(this);
        lay_kmsSummary.setOnClickListener(this);
        lay_geo_fences.setOnClickListener(this);
        lay_app_version.setOnClickListener(this);
        layout_support.setOnClickListener(this);
        lay_driverList.setOnClickListener(this);
        lay_executive_report.setOnClickListener(this);
        lay_tollgate_report.setOnClickListener(this);
        lay_geo_fences_report.setOnClickListener(this);
        lay_fuel_summa_report.setOnClickListener(this);
        lay_tank_summa_report.setOnClickListener(this);
        lay_billing.setOnClickListener(this);
        lay_add_geo_fences_report.setOnClickListener(this);

        nav_img_search_layout.setOnClickListener(this);
        nav_img_close_layout.setOnClickListener(this);

        CommonManager co = new CommonManager(getApplicationContext());
        DaoHandler da = new DaoHandler(getApplicationContext(), false);
        $SideMenuImage.setImageBitmap(da.getBitmap());
        // BitmapDrawable ob = new BitmapDrawable(getResources(), co.getBitmap());
        //  $SideMenuImage.setBackgroundDrawable(ob);
//        $SideMenuImage.setBackgroundResource(R.drawable.ic_truck);
//        imgLoader.DisplayImage(imgUrl, $SideMenuImage);

        imgTitle.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        app_version_txt.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        kms_Summary.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        signOut.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        policy.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        view_vehicle.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        view_vehicle_nearby.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        notification.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        setting.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        home.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        vehicle.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        vehicle_nearby.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        support_details.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        driver_list_txt.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        geo_fences.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        mEdtSearch.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        txt_exec_report.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        txt_toll_report.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        geo_fences_report.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        fuel_summary_report.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        add_geo_fences_report.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        billing.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        fuel_tank_report.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));

        slide_app_version = (ImageView) findViewById(R.id.img_slide_app_version);
        slide_home = (ImageView) findViewById(R.id.img_slide_hole);
        slide_vehicle_list = (ImageView) findViewById(R.id.img_vehicle_list);
        slide_vehicle_list_nearby = (ImageView) findViewById(R.id.img_vehicle_list_nearby);
        slide_settings = (ImageView) findViewById(R.id.img_slide_settings);
        slide_notification = (ImageView) findViewById(R.id.img_slide_notifications);
        slide_kms = (ImageView) findViewById(R.id.img_slide_kms);
        slide_sites = (ImageView) findViewById(R.id.img_slide_sites);
        slide_support = (ImageView) findViewById(R.id.img_slide_support);
        slide_logout = (ImageView) findViewById(R.id.img_slide_logout);
        slide_policy = (ImageView) findViewById(R.id.img_policy);
        slide_driver_list = (ImageView) findViewById(R.id.img_driver_list);
        slide_exec_report = (ImageView) findViewById(R.id.img_slide_executive_report);
        slide_toll_report = (ImageView) findViewById(R.id.img_slide_tollgate_report);
        slide_geofence_report = (ImageView) findViewById(R.id.img_geofence_report);
        slide_fuelsumm_report = (ImageView) findViewById(R.id.img_fuelsumm_report);
        slide_add_geofence_report = (ImageView) findViewById(R.id.img_slide_add_sites);
        slide_tanksumm_report=findViewById(R.id.img_tanksumm_report);
        slide_billing = (ImageView) findViewById(R.id.img_billing);


    }

    private void loadDatas(long timeInterval) {
        // TODO Auto-generated method stub
        // preloaderStart();
        // Handler h = new Handler();
        // h.postDelayed(new Runnable() {
        // @Override
        // public void run() {
        // // TODO Auto-generated method stub
        // // homeProgressDialog.show();
        // new loadScreenData().execute();
        //
        // }
        // }, 200);

        if (timer != null) {
            timer.cancel();
        }
        timer = new Timer();
        timer.schedule(new TimerTask() {

            public void run() {

//                String url = null;
//                if (mSelectedGroup.equalsIgnoreCase("Select")) {
//                    url = Const.API_URL + "mobile/getVehicleLocations?userId=" + mUserId + "&macid=" + getStdTableValue("macid") + "&appid=" + getStdTableValue("appid");
//
//                } else {
//                    url = Const.API_URL + "mobile/getVehicleLocations?userId=" + mUserId + "&group=" + mSelectedGroup + "&macid=" + getStdTableValue("macid") + "&appid=" + getStdTableValue("appid");
//                }
//
//
//                if (cd.isConnectingToInternet()) {
//                    new PullVehicleInformation().execute(url);
//                } else {
//                    Toast.makeText(getApplicationContext(),
//                            "Please check your network connection",
//                            Toast.LENGTH_SHORT).show();
//                    setData("No Record");
//                }

                updateListData();

            }
        }, timeInterval, timeInterval);

    }

    public void updateListData() {


        runOnUiThread(new Runnable() {
            public void run() {
                // runs on UI thread

                String url = null;
                if (mSelectedGroup.equalsIgnoreCase("Select")) {
                    url = Const.API_URL + "mobile/getVehicleLocations?userId=" + mUserId + "&macid=" + getStdTableValue("macid") + "&appid=" + getStdTableValue("appid");

                } else {
                    url = Const.API_URL + "mobile/getVehicleLocations?userId=" + mUserId + "&group=" + mSelectedGroup + "&macid=" + getStdTableValue("macid") + "&appid=" + getStdTableValue("appid");
                }


                if (cd.isConnectingToInternet()) {
                    new PullVehicleInformation().execute(url);
                } else {
                    Toast.makeText(getApplicationContext(),
                            ""+getResources().getString(R.string.network_connection),
                            Toast.LENGTH_SHORT).show();
                    setData(""+getResources().getString(R.string.no_record));
                }

            }
        });


    }


    @Override
    protected void onPause() {
        super.onPause();


        if (timer != null) {
            timer.cancel();
        }

        // System.exit(0);


    }

    @Override
    protected void onStop() {
        super.onStop();
        if (timer != null) {
            timer.cancel();
        }
    }

    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub
        switch (v.getId()) {
            case R.id.id_total_vehicles_layout:
                setData("ALL");
                break;
            case R.id.id_online_vehicles_layout:
                setData("P");
                break;
            case R.id.id_offline_vehicles_layout:
                setData("S");
                break;
            case R.id.id_moving_vehicles_layout:
                setData("M");
                break;
            case R.id.id_nodata_vehicles_layout:
                setData("U");
                break;
            case R.id.layout_Home:
                startActivity(new Intent(VehicleListActivity.this, HomeActivity.class));
                finish();

                break;
            case R.id.layout_vehicles:
                startActivity(new Intent(VehicleListActivity.this, VehicleListActivity.class));
                finish();
                break;
            case R.id.layout_vehicles_nearby:
                startActivity(new Intent(VehicleListActivity.this, UserLocationNearVehicleList.class));
                finish();
                break;
            case R.id.layout_settings:
                startActivity(new Intent(VehicleListActivity.this, Settings.class));
                finish();
                break;

            case R.id.layout_billing:
                startActivity(new Intent(VehicleListActivity.this, PendingPaymentActivity.class));
                finish();
                break;

            case R.id.layout_notification:
                startActivity(new Intent(VehicleListActivity.this, NotificationListActivity.class));
                finish();
                break;
            case R.id.layout_signOut:
                new doSignout().execute("");
                break;
            case R.id.layout_application_version:
                showVersionToast();
                break;

            case R.id.layout_kms_summary:
                startActivity(new Intent(VehicleListActivity.this, KMSSummary.class));
                finish();
                break;
            case R.id.layout_executive_report:
                startActivity(new Intent(VehicleListActivity.this, ExecutiveReportNew.class));
                finish();
                break;
            case R.id.layout_tollgate_report:
                startActivity(new Intent(VehicleListActivity.this, TollgateReport.class));
                finish();
                break;
            case R.id.layout_geo_fences:
                startActivity(new Intent(VehicleListActivity.this, GeoFencesActivity.class));
                finish();
                break;
            case R.id.layout_geo_fences_report:
                startActivity(new Intent(VehicleListActivity.this, GeoFenceReportActivity.class));
                finish();
                break;
            case R.id.layout_fuel_summry_report:
                startActivity(new Intent(VehicleListActivity.this, FuelSummaryNew.class));
                finish();
                break;
            case R.id.layout_tank_summry_report:
                startActivity(new Intent(VehicleListActivity.this, TankBasedReport.class));
                finish();
                break;
            case R.id.layout_add_geo_fences:
                startActivity(new Intent(VehicleListActivity.this, AddSitesActivity.class));
                finish();
                break;
            case R.id.layout_support:
                startActivity(new Intent(VehicleListActivity.this, SupportDetailsActivity.class));
//                startActivity(new Intent(VehicleListActivity.this, SupportMailActivity.class));
                finish();
                break;
            case R.id.layout_driver_list:
                startActivity(new Intent(VehicleListActivity.this, DriverListActivity.class));
                finish();
                break;
            case R.id.nav_img_search_layout:
//                startActivity(new Intent(VehicleListActivity.this, SupportDetailsActivity.class));
//                finish();
                mTxtLastSync.setVisibility(View.GONE);
                //  mEdtSearch.requestFocus();
                mEdtSearch.setVisibility(View.VISIBLE);
                mEdtSearch.setText("");


                mEdtSearch.requestFocus();
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.showSoftInput(mEdtSearch, InputMethodManager.SHOW_IMPLICIT);

                nav_img_close_layout.setVisibility(View.VISIBLE);
                nav_img_search_layout.setVisibility(View.GONE);
                break;
            case R.id.nav_img_close_layout:
//                startActivity(new Intent(VehicleListActivity.this, DriverListActivity.class));
//                finish();
                InputMethodManager imm1 = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm1.hideSoftInputFromWindow(mEdtSearch.getWindowToken(), 0);
                mTxtLastSync.setVisibility(View.VISIBLE);
                mEdtSearch.setVisibility(View.GONE);
                //   mEdtSearch.sethi
                mEdtSearch.setText("");

                // mEdtSearch.requestFocus();
                //InputMethodManager imm1 = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                // imm1.hideSoftInputFromWindow(mEdtSearch.getWindowToken(), 0);

                nav_img_close_layout.setVisibility(View.GONE);
                nav_img_search_layout.setVisibility(View.VISIBLE);

                break;
            case R.id.img_vehicle_list_close:
                mIsDashBoardShows = false;
                mChartContainerLayout.setVisibility(View.GONE);
                mLstVehicleView.setVisibility(View.VISIBLE);
                break;
            case R.id.layout_policy:
                startActivity(new Intent(VehicleListActivity.this, WebViewActivity.class));
                break;

        }
    }

    public void showVersionToast() {

        String versionName = BuildConfig.VERSION_NAME;

//        Toast.makeText(getApplicationContext(), "You are using version : " + getResources().getString(R.string.app_version_name) + " of " + getResources().getString(R.string.app_name) + " application", Toast.LENGTH_LONG).show();
        Toast.makeText(getApplicationContext(), ""+getResources().getString(R.string.versionName) + versionName + ""+getResources().getString(R.string.of) + getResources().getString(R.string.app_name) + ""+getResources().getString(R.string.application), Toast.LENGTH_LONG).show();
    }

    void dbSetup() {
        dbhelper = new DBHelper(this);
        try {
            dbhelper.createDataBase();
            dbhelper.openDataBase();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setDropDownData() {
        //mGroupList = new ArrayList<String>();
        // List<String> list = new ArrayList<String>();
        // list.add("Select");
        if (mGroupList.size() > 0) {

        } else {
            mGroupList.add(""+getResources().getString(R.string.select));
        }

        for (int i = 0; i < mGroupList.size(); i++) {

            String[] str_msg_data_array = mGroupList.get(i).split(":");

            mGroupSpinnerList.add(str_msg_data_array[0]);

        }

        // R.layout.spinner_row

//        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(
//                VehicleListActivity.this, android.R.layout.simple_spinner_item,
//                mGroupList) {

        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(
                VehicleListActivity.this, R.layout.spinner_row,
                mGroupSpinnerList) {

            public View getView(int position, View convertView, ViewGroup parent) {
                View v = super.getView(position, convertView, parent);
                // TextView v = (TextView) super.getView(position,
                // convertView,parent);
                // Constant.face(DailyCalls.this, (TextView) v);
                return v;
            }

            public View getDropDownView(int position, View convertView,
                                        ViewGroup parent) {
                View v = super.getDropDownView(position, convertView, parent);
                // ((TextView)
                // v).setBackgroundColor(Color.parseColor("#BBfef3da"));
                // TextView v = (TextView) super.getView(position,
                // convertView,parent);
                // Constant.face(DailyCalls.this, (TextView) v);
                return v;
            }
        };
        spinnerArrayAdapter
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mGroupSpinner.setAdapter(spinnerArrayAdapter);

        String groupName = sp.getString("selected_group", "");
        int spinnerPosition = spinnerArrayAdapter.getPosition(groupName);

//set the default according to value
        mGroupSpinner.setSelection(spinnerPosition);


    }


    public void queryUserDB() {
//        DataBaseHandler db = new DataBaseHandler(this);
//        Cursor c = null;
//        // String qry = "SELECT DISTINCT patch_id,patch_name,mtp_id FROM "
//        // + DbHandler.TABLE_TM_DCP;
//        try {
//            // c = db.open().getDatabaseObj().rawQuery(qry, null);
//            c = db.open()
//                    .getDatabaseObj()
//                    .query(DataBaseHandler.TABLE_USER, null, null, null, null, null,
//                            null);
//
//            int user_id_index = c.getColumnIndex("user_id");
//            int group_list_index = c.getColumnIndex("group_list");
//
//            if (c.getCount() > 0) {
//
//                while (c.moveToNext()) {
//                    mUserId = c.getString(user_id_index);
//                    Constant.SELECTED_USER_ID = mUserId;
////                    System.out.println("The group list is :::::"
////                            + c.getString(group_list_index));
//
//                    if (c.getString(group_list_index) != null) {
//                        Gson g = new Gson();
//                        InputStream is = new ByteArrayInputStream(c.getString(
//                                group_list_index).getBytes());
//                        Reader reader = new InputStreamReader(is);
//                        Type fooType = new TypeToken<List<String>>() {
//                        }.getType();
//
//                        mGroupList = g.fromJson(reader, fooType);
//                    }
//
//                }
//            }
//
//        } finally {
//            c.close();
//            db.close();
//        }

        DaoHandler da = new DaoHandler(getApplicationContext(), false);
        da.queryUserDB();
        mUserId = Constant.mDbUserId;
        Constant.SELECTED_USER_ID = Constant.mDbUserId;
        mGroupList = Constant.mUserGroupList;

        setDropDownData();
    }

    public String getStdTableValue(String tag) {
        Cursor std_table_cur = null;
        String stdtablevalues = null;
        try {
            std_table_cur = dbhelper.get_std_table_info(tag);
            std_table_cur.moveToFirst();
            stdtablevalues = std_table_cur.getString(0);
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), ""+getResources().getString(R.string.please_try_again), Toast.LENGTH_SHORT).show();
        } finally {
            if (std_table_cur != null) {
                std_table_cur.close();
            }
        }
        return stdtablevalues;
    }

    public void setData(String status) {
//        OFF,ON
        mEdtSearch.setText("");
        List<VehicleData> mVehicleList = new ArrayList<VehicleData>();
        if (status.equalsIgnoreCase("ALL")) {

            mVehicleList = mVehicleLocationList;
        } else if (status.equalsIgnoreCase("No Record")) {
            mVehicleList = null;

        } else {
            for (int i = 0; i < mVehicleLocationList.size(); i++) {
                if (mVehicleLocationList.get(i).getPosition().equalsIgnoreCase(status)) {
                    mVehicleList.add(mVehicleLocationList.get(i));
                }
            }
        }
        Constant.SELECTED_VEHICLE_LOCATION_LIST_OBJECT = mVehicleList;
        mVehicleLocationListAdapter = mVehicleList;


        // System.out.println("The vehicle list is :::::" + mVehicleLocationListAdapter.size());

        if (mVehicleLocationListAdapter != null) {
            if (mVehicleLocationListAdapter.size() > 0) {
                VehicleListAdapter vehicleAdapter = new VehicleListAdapter(VehicleListActivity.this, mVehicleLocationListAdapter);
                mLstVehicleView.setAdapter(vehicleAdapter);
                vehicleAdapter.notifyDataSetChanged();
            } else {
                List<VehicleData> vList = new ArrayList<VehicleData>();
                mVehicleLocationListAdapter = vList;
                VehicleListAdapter vehicleAdapter = new VehicleListAdapter(VehicleListActivity.this, mVehicleLocationListAdapter);
                mLstVehicleView.setAdapter(vehicleAdapter);
                vehicleAdapter.notifyDataSetChanged();
            }
        } else {
//    Set no record data
            // setData("No Record");
            List<VehicleData> vList = new ArrayList<VehicleData>();
            mVehicleLocationListAdapter = vList;
            VehicleListAdapter vehicleAdapter = new VehicleListAdapter(VehicleListActivity.this, mVehicleLocationListAdapter);
            mLstVehicleView.setAdapter(vehicleAdapter);
            vehicleAdapter.notifyDataSetChanged();
        }

    }

    public void getSearchText(String ch) {

//        System.out.println("List size is ::::::" + mVehicleLocationListAdapter.size()
//                + " and the character is :::::" + ch);

        List<VehicleData> list = new ArrayList<VehicleData>();
        // list = null;
        if (ch != null && ch.length() > 0) {


            list.clear();

            for (int i = 0; i < mVehicleLocationList.size(); i++) {
                VehicleData dc = new VehicleData();


//                if (Pattern
//                        .compile(Pattern.quote(ch), Pattern.CASE_INSENSITIVE)
//                        .matcher(mVehicleLocationList.get(i).getShortName())
//                        .find()) {
//                    //  System.out.println("Hiiiiiii");
//                    dc = mVehicleLocationList.get(i);
//                    list.add(dc);
//                }

                if (mVehicleLocationList.get(i).getShortName() != null) {
                    if (Pattern
                            .compile(Pattern.quote(ch), Pattern.CASE_INSENSITIVE)
                            .matcher(mVehicleLocationList.get(i).getShortName())
                            .find()) {
//                        System.out.println("Hiiiiiii " + mVehicleLocationList.get(i).getShortName());
                        dc = mVehicleLocationList.get(i);
                        list.add(dc);
                    }
                }

            }

        } else {
            list = mVehicleLocationList;
        }

        // System.out.println("The searched list size is :::::" + list.size());


//        System.out.println("List size after filtered is :::::"
//                + mVehicleLocationListAdapter.size());

        if (list.size() > 0) {
            mVehicleLocationListAdapter = list;
            VehicleListAdapter vehicleAdapter = new VehicleListAdapter(VehicleListActivity.this, mVehicleLocationListAdapter);
            mLstVehicleView.setAdapter(vehicleAdapter);
            vehicleAdapter.notifyDataSetChanged();
        } else {
//    Set no record data
            List<VehicleData> vList = new ArrayList<VehicleData>();
            mVehicleLocationListAdapter = vList;
            VehicleListAdapter vehicleAdapter = new VehicleListAdapter(VehicleListActivity.this, mVehicleLocationListAdapter);
            mLstVehicleView.setAdapter(vehicleAdapter);
            vehicleAdapter.notifyDataSetChanged();
            // setData("No Record");

        }
        // $SearchListView.setAdapter(new DailySearchAdapter(DailySearch.this,
        // mFilteredListDailySearch));

    }

    @Override
    public void onRefresh() {
        mEdtSearch.setText("");

        String url = Const.API_URL + "mobile/getVehicleLocations?userId=" + mUserId + "&group=" + mSelectedGroup + "&macid=" + getStdTableValue("macid") + "&appid=" + getStdTableValue("appid");
        if (cd.isConnectingToInternet()) {
            userverification();
            new PullVehicleInformation().execute(url);
        } else {
            Toast.makeText(getApplicationContext(),
                    ""+getResources().getString(R.string.network_connection),
                    Toast.LENGTH_SHORT).show();
            setData(""+getResources().getString(R.string.no_record));
        }
    }


    private class doSignout extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = MyCustomProgressDialog.ctor(VehicleListActivity.this);
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                String gcmToken = sp.getString("gcmToken", null);
//                System.out.println("The gcm key is :::::" + gcmToken);
                HttpConfig ht = new HttpConfig();
                if (mUserId.equalsIgnoreCase("null")){
                    mUserId = Constant.mDbUserId;
                    return ht.httpGet(Const.API_URL + "mobile/logOutUser?userId=" + mUserId + "&gcmId=" + gcmToken);
                } else{
                    return ht.httpGet(Const.API_URL + "mobile/logOutUser?userId=" + mUserId + "&gcmId=" + gcmToken);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        public void onPostExecute(String result) {
            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
            try {
                /**
                 * Perform logout by clearing the login status and start registration activity.
                 */
                Log.d("logoutresult",""+result);
                if (result != null && result.length() > 0) {
                    if (result.equalsIgnoreCase("Success")) {
//                    CommonManager co = new CommonManager(getApplicationContext());
                        String qry = "DELETE FROM " + DataBaseHandler.TABLE_USER;
                        String qry1 = "DELETE FROM " + DataBaseHandler.TABLE_LOGO;
                        String qry2 = "DELETE FROM " + DataBaseHandler.TABLE_NOTIFICATIONS;

                        DaoHandler da = new DaoHandler(getApplicationContext(), true);
                        da.deleteSelectedNotificationFromDB(qry);
                        da.deleteSelectedNotificationFromDB(qry1);

//                    co.deleteDB(qry);
//                    co.deleteDB(qry1);
                        //  co.deleteDB(qry2);
                        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences("CamerPreference",0);
                        SharedPreferences.Editor edit = sharedPreferences.edit();
                        edit.remove("Clickin");
                        edit.commit();

                        try {
                            SharedPreferences.Editor editor = sp.edit();
                            editor.putBoolean("is_logged_in", false);
                            editor.commit();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        SharedPreferences.Editor editor= sp.edit();
                        editor.putBoolean("login_status",false);
                        editor.commit();
//                        dbhelper.update_StdTableValues("login_status", false);
                        Intent register = new Intent(VehicleListActivity.this, RegistrationPage.class);
                        startActivity(register);
                        finish();
                    } else if (result.equalsIgnoreCase("failure")) {
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.oops_error), Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.oops_error), Toast.LENGTH_SHORT).show();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.oops_error), Toast.LENGTH_SHORT).show();
            }
        }
    }


    /**F
     * AsyncTask Background API Calls
     */
    public class PullVehicleInformation extends AsyncTask<String, Integer, String> {
        int current_group_pos;
        String current_group_name;
        ProgressDialog progressDialog1;

        @Override
        public String doInBackground(String... urls) {


            String response_from_server = "";
            try {

                //   System.out.println("The pull vehicle info url is ::::"+urls[0]);
                HttpConfig ht = new HttpConfig();
                response_from_server = ht.httpGet(urls[0]);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return response_from_server;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog1 = new ProgressDialog(VehicleListActivity.this,
                    AlertDialog.THEME_HOLO_LIGHT);
            progressDialog1.setMessage(""+getResources().getString(R.string.pleaseWait));
            progressDialog1.setProgressDrawable(new ColorDrawable(
                    Color.BLUE));
            progressDialog1.setCancelable(true);
            progressDialog1.setCanceledOnTouchOutside(false);
            progressDialog1.show();
        }

        protected void onProgressUpdate(Integer... values) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    publishProgress(0);
                }
            });
        }

        @Override
        public void onPostExecute(String result) {
            Cursor vehicle_info = null;
            try {
                Date d = new Date();
//                String mTimeData = d.getDate() + "-" + d.getMonth() + 1 + "-" + d.getYear()
//                        + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();

                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");


                String mTimeData = sdf.format(d);
                mTxtLastSync.setText(getResources().getString(R.string.Last_Sync)+ mTimeData);

                // mToolbar.setTitle("Last Sync :" + mTimeData);
//                System.out.println("The result is ::::::" + result);
                if (result != null && result.length() > 0) {
                    Gson g = new Gson();
                    InputStream is = new ByteArrayInputStream(result.getBytes());
                    Reader reader = new InputStreamReader(is);
                    Type fooType = new TypeToken<List<VehicleLocationsEnv>>() {
                    }.getType();

                    mVehicleGroupList = g.fromJson(reader, fooType);


                    for (int i = 0; i < mVehicleGroupList.size(); i++) {


                        if (mVehicleGroupList.get(i).getGroup().equalsIgnoreCase(mSelectedGroup)) {

                            if (mVehicleGroupList.get(i).getTotalVehicles() != null) {
                                $TxtNoOfVehicleValue.setText(mVehicleGroupList.get(i).getTotalVehicles());
                                mTotalVehi = Integer.valueOf(mVehicleGroupList.get(i).getTotalVehicles());
                            }

//                            if (mVehicleGroupList.get(i).getTotalParkedVehicles() != null) {

//                            System.out.println("The total parked vehicle is ::::" + mVehicleGroupList.get(i).getTotalParkedVehicles());

                            $TxtOnlineVehicleValue.setText(String.valueOf(mVehicleGroupList.get(i).getTotalParkedVehicles()));
                            mParkedVehi = mVehicleGroupList.get(i).getTotalParkedVehicles();

//                            System.out.println("The total idle vehicle is ::::" + mVehicleGroupList.get(i).getTotalIdleVehicles());
//                            }
//                            if (mVehicleGroupList.get(i).getAttention() != null) {
                            $TxtOfflineVehicleValue.setText(String.valueOf(mVehicleGroupList.get(i).getTotalIdleVehicles()));
                            mIdleVehicle = mVehicleGroupList.get(i).getTotalIdleVehicles();
//                            }
//                            System.out.println("The total no data vehicle is ::::" + mVehicleGroupList.get(i).getTotalNoDataVehicles());
//                            if (mVehicleGroupList.get(i).getAttention() != null) {
                            $TxtNodataVehicleValue.setText(String.valueOf(mVehicleGroupList.get(i).getTotalNoDataVehicles()));
                            mNodataVehicle = mVehicleGroupList.get(i).getTotalNoDataVehicles();
//                            }

//                            if (mVehicleGroupList.get(i).getAttention() != null) {
//                            System.out.println("The total moving vehicle is ::::" + mVehicleGroupList.get(i).getTotalMovingVehicles());
                            $TxtMovingVehicleValue.setText(String.valueOf(mVehicleGroupList.get(i).getTotalMovingVehicles()));
                            mMovingVehicle = mVehicleGroupList.get(i).getTotalMovingVehicles();
//                            }


                            String qry1 = "UPDATE " + DataBaseHandler.TABLE_USER + " SET support_adds='" + mVehicleGroupList.get(i).getSupportDetails() + "' WHERE user_id='" + mUserId + "'";

//                            CommonManager co = new CommonManager(getApplicationContext());
//                            co.updateDB(qry1);

                            DaoHandler da = new DaoHandler(getApplicationContext(), true);
                            da.updateDB(qry1);


                            if (mVehicleGroupList.get(i).getVehicleLocations() != null) {
                                mVehicleLocationList = new ArrayList<VehicleData>(
                                        Arrays.asList(mVehicleGroupList.get(i).getVehicleLocations()));
                                // System.out.println("HIIIIIIII");
                                setData(""+getResources().getString(R.string.ALL));
                            } else {
                                setData(""+getResources().getString(R.string.no_record));
                            }


                        }


                    }

                }
                // if (progressDialog.isShowing()) {
                progressDialog1.dismiss();
                // }

                if (mIsDashBoardShows) {
//                    formChart();
                    formBarchart();
                } else {
                    mIsDashBoardShows = false;
                    mChartContainerLayout.setVisibility(View.GONE);
                    mLstVehicleView.setVisibility(View.VISIBLE);
                }

                // stopping swipe refresh
                swipeRefreshLayout.setRefreshing(false);

            } catch (Exception e) {
            } finally {
                if (progressDialog1.isShowing()) {
                    progressDialog1.dismiss();
                }
                if (vehicle_info != null) {
                    vehicle_info.close();
                }
            }
            if (sp.getBoolean("payment_due_load", true)) {
                //load invoice
                new getPaymentDue().execute();
            }
        }

    }


    /**
     * AsyncTask Background API Calls
     */
    public class getPaymentDue extends AsyncTask<String, Integer, String> {


        @Override
        public String doInBackground(String... urls) {


            String response_from_server = "";
            try {

//                   System.out.println("The getPaymentDue url is ::::"+Const.PAYMENT_DUE_API + mUserId);
                HttpConfig ht = new HttpConfig();
                response_from_server = ht.httpGet(Const.PAYMENT_DUE_API + mUserId);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return response_from_server;
        }


        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        protected void onProgressUpdate(Integer... values) {

        }

        @Override
        public void onPostExecute(String result) {

//            System.out.println("The getPaymentDue result  is ::::"+result);
            double mTotalDueAmnt = 0.0;
            String mCustName = null;
            if (result != null && result.length() > 0) {

                try {
                    JSONObject jObj = new JSONObject(result.trim());
                    if (jObj.has("hist") && jObj.get("hist") != null) {
                        JSONArray jArray = jObj.getJSONArray("hist");
                        mPaymentDueList.clear();
                        for (int i = 0; i < jArray.length(); i++) {
                            JSONObject jobj2 = jArray.getJSONObject(i);
                            PaymentDueDto pd = new PaymentDueDto();
                            if (jobj2.has("dueDate")) {
                                pd.setDueDate(jobj2.getString("dueDate"));
                            }
                            if (jobj2.has("dueDays")) {
                                pd.setDueDays(jobj2.getString("dueDays"));
                            }
                            if (jobj2.has("balanceAmount")) {
                                pd.setBalanceAmount(jobj2.getString("balanceAmount"));
                                mTotalDueAmnt = mTotalDueAmnt + Double.parseDouble(jobj2.getString("balanceAmount"));
                            }

                            if (jobj2.has("invoiceLink")) {
                                pd.setInvoiceLink(jobj2.getString("invoiceLink"));
                            }
                            mPaymentDueList.add(pd);
                        }

                    }

                    if (jObj.has("balanceAmount")) {
                        mTotalDueAmnt = Double.parseDouble(jObj.getString("balanceAmount"));
                    }

                    if (jObj.has("customerName")) {
                        mCustName = jObj.getString("customerName");
                    }
                    updatePaymentDue(false);

                    //show alert dialog
                    if (mPaymentDueList.size() > 0) {
                        showPaymentDueDialog(mTotalDueAmnt, mCustName);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

        }

    }

    private void showPaymentDueDialog(double mTotalAmnt, String mCustName) {
        // TODO Auto-generated method stub
        final Dialog dialog = new Dialog(VehicleListActivity.this);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_payment_due);
        dialog.setCancelable(false);
        dialog.show();

        RecyclerView mRecyclerView = (RecyclerView) dialog.findViewById(R.id.card_recycler_view);
        Button mBtnOk = (Button) dialog.findViewById(R.id.btn_payment_due_okay);
        TextView mTxtTotalDue = (TextView) dialog.findViewById(R.id.txt_total_balance);
        TextView mTxtCustomerName = (TextView) dialog.findViewById(R.id.txt_customer_name);

        mRecyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(layoutManager);

        PaymentDueAdapter mAadp = new PaymentDueAdapter(mPaymentDueList, VehicleListActivity.this);
        mRecyclerView.setAdapter(mAadp);
        mAadp.notifyDataSetChanged();

        mTxtTotalDue.setText(Utils.get2DecimalValue(String.valueOf(mTotalAmnt)));
        mTxtCustomerName.setText(mCustName);
        mBtnOk.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                dialog.dismiss();
            }
        });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        //Toast.makeText(getApplicationContext(), "Back btn pressed :::::", Toast.LENGTH_SHORT).show();
        System.exit(0);

    }

    private void screenArrange() {
        // TODO Auto-generated method stub
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        height = displayMetrics.heightPixels;
        width = displayMetrics.widthPixels;
        Constant.ScreenWidth = width;
        Constant.ScreenHeight = height;


        LinearLayout.LayoutParams txtParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        txtParams.width = width * 99 / 5 / 100;
        txtParams.height = height * 5 / 100;
        txtParams.gravity = Gravity.CENTER;
        $TxtNoOfVehicle.setLayoutParams(txtParams);
        $TxtOnlineVehicle.setLayoutParams(txtParams);
        $TxtOfflineVehicle.setLayoutParams(txtParams);
        $TxtNoOfVehicleValue.setLayoutParams(txtParams);
        $TxtOnlineVehicleValue.setLayoutParams(txtParams);
        $TxtOfflineVehicleValue.setLayoutParams(txtParams);

        $TxtNodataVehicle.setLayoutParams(txtParams);
        $TxtNodataVehicleValue.setLayoutParams(txtParams);
        $TxtMovingVehicle.setLayoutParams(txtParams);
        $TxtMovingVehicleValue.setLayoutParams(txtParams);


        $TxtNoOfVehicle.setGravity(Gravity.CENTER);
        $TxtOnlineVehicle.setGravity(Gravity.CENTER);
        $TxtOfflineVehicle.setGravity(Gravity.CENTER);
        $TxtNoOfVehicleValue.setGravity(Gravity.CENTER);
        $TxtOnlineVehicleValue.setGravity(Gravity.CENTER);
        $TxtOfflineVehicleValue.setGravity(Gravity.CENTER);

        $TxtNodataVehicle.setGravity(Gravity.CENTER);
        $TxtNodataVehicleValue.setGravity(Gravity.CENTER);
        $TxtMovingVehicle.setGravity(Gravity.CENTER);
        $TxtMovingVehicleValue.setGravity(Gravity.CENTER);

        LinearLayout.LayoutParams headLayParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        headLayParams.width = width;
        headLayParams.height = height * 10 / 100;
        // headTxtParams.setMargins(width * 2 / 100, 0, 1, width * 1 / 100);
        mVehicleListHeaderLAyout.setLayoutParams(headLayParams);

        LinearLayout.LayoutParams chartCloseImgParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
//        chartCloseImgParams.width = width;
//        chartCloseImgParams.height = height * 10 / 100;
        chartCloseImgParams.gravity = Gravity.CENTER | Gravity.RIGHT;
        chartCloseImgParams.setMargins(0, 0, height * 3 / 100, width * 1 / 100);
        mImgChartClose.setLayoutParams(chartCloseImgParams);

        LinearLayout.LayoutParams headTxtParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        headTxtParams.width = width * 74 / 100;
        headTxtParams.height = height * 7 / 100;
        headTxtParams.setMargins(width * 4 / 100, 0, 1, width * 1 / 100);
        $TxtTitle.setLayoutParams(headTxtParams);
        $TxtTitle.setPadding(width * 2 / 100, 0, 0, 0);
        $TxtTitle.setGravity(Gravity.CENTER);
        mEdtSearch.setLayoutParams(headTxtParams);
        mEdtSearch.setPadding(width * 2 / 100, 0, width * 4 / 100, 0);
        mEdtSearch.setGravity(Gravity.CENTER);

        Toolbar.LayoutParams headTxtLastSyncParams = new Toolbar.LayoutParams(
                Toolbar.LayoutParams.WRAP_CONTENT,
                Toolbar.LayoutParams.WRAP_CONTENT);
        headTxtLastSyncParams.width = width * 70 / 100;
        headTxtLastSyncParams.height = height * 6 / 100;
        // headTxtLastSyncParams.setMargins(width * 4 / 100, 0, 1, height * 3 / 100);
        headTxtLastSyncParams.gravity = Gravity.CENTER_HORIZONTAL | Gravity.LEFT;
        //  headTxtLastSyncParams.set
        mTxtLastSync.setLayoutParams(headTxtLastSyncParams);
        mTxtLastSync.setPadding(width * 2 / 100, 0, 0, 0);
//        mTxtLastSync.setGravity(Gravity.CENTER | Gravity.LEFT);
//        mTxtLastSync.setLay
        // mEdtSearch.setLayoutParams(headTxtParams);
        //mEdtSearch.setPadding(width * 2 / 100, 0, 0, 0);

        // mEdtSearch.leftm
        // mEdtSearch.setGravity(Gravity.CENTER);

        LinearLayout.LayoutParams imgSearchCloseParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        imgSearchCloseParams.height = height * 10 / 100;
        imgSearchCloseParams.width = height * 10 / 100;
        // navImgBck.gravity = (Gravity.CENTER);
        // imgMenu.bottomMargin = height * 1/2 / 100;
        // imgMenu.leftMargin = width * 4 / 100;
//        nav_img_lay.setLayoutParams(imgMenu);
        nav_img_search_layout.setLayoutParams(imgSearchCloseParams);
        nav_img_close_layout.setLayoutParams(imgSearchCloseParams);

        LinearLayout.LayoutParams imgMenu = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        imgMenu.height = height * 5 / 100;
        imgMenu.width = height * 5 / 100;
        imgMenu.gravity = (Gravity.CENTER);
        // imgMenu.rightMargin = height * 2 / 100;
        imgMenu.leftMargin = width * 1 / 100;
        mImgSearch.setLayoutParams(imgMenu);

        mImgEdtClose.setLayoutParams(imgMenu);

        LinearLayout.LayoutParams viewParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        viewParams.width = (int) (width * 0.5 / 100);
        viewParams.height = height * 10 / 100;
        $View1.setLayoutParams(viewParams);
        $View2.setLayoutParams(viewParams);
        $View3.setLayoutParams(viewParams);
        $View4.setLayoutParams(viewParams);

        LinearLayout.LayoutParams spinnerParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        spinnerParams.width = width * 98 / 100;
        spinnerParams.height = height * 5 / 100;
        spinnerParams.gravity = Gravity.CENTER | Gravity.TOP;
        spinnerParams.topMargin = (int) (height * 1 / 100);
        //spinnerParams.leftMargin = width * 1 / 100;
        //spinnerParams.rightMargin = width * 1 / 100;
        spinnerParams.bottomMargin = (int) (height * 0.25 / 100);
        mGroupSpinner.setLayoutParams(spinnerParams);

        //		LinearLayout.LayoutParams spinnerParams = new LinearLayout.LayoutParams(
        //				LinearLayout.LayoutParams.WRAP_CONTENT,
        //				LinearLayout.LayoutParams.WRAP_CONTENT);
        //		spinnerParams.width = width * 95 / 100;
        //		spinnerParams.height = height * 7 / 100;
        //		spinnerParams.topMargin = (int) (height * 1.25 / 100);
        //		spinnerParams.leftMargin = width * 2 / 100;
        //		spinnerParams.rightMargin = width * 2 / 100;
        //		spinnerParams.bottomMargin = (int) (height * 0.25 / 100);
        //		$SpinnerNavigation.setLayoutParams(spinnerParams);

        /** SideMenu Alignment */
        ScrollView.LayoutParams sideLayoutParams = new ScrollView.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        sideLayoutParams.width = width * 70 / 100;
        sideLayoutParams.height = height;
        $SideMenuLayout.setLayoutParams(sideLayoutParams);
//        $SideMenuLayoutEnd.setLayoutParams(sideLayoutParams);

//        LinearLayout.LayoutParams sideImagParams = new LinearLayout.LayoutParams(
//                LinearLayout.LayoutParams.WRAP_CONTENT,
//                LinearLayout.LayoutParams.WRAP_CONTENT);
//        sideImagParams.width = width * 75/ 100;
//        sideImagParams.height = height * 25 / 100;
//        $SideMenuImage.setLayoutParams(sideImagParams);

        /**SideMenu Alignment*/

        LinearLayout.LayoutParams layoutMenu1 = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        layoutMenu1.width = width * 70 / 100;
        layoutMenu1.height = height * 8 / 100;
        layoutMenu1.gravity = Gravity.CENTER | Gravity.LEFT;
        home.setLayoutParams(layoutMenu1);
        setting.setLayoutParams(layoutMenu1);
        notification.setLayoutParams(layoutMenu1);
        kms_Summary.setLayoutParams(layoutMenu1);
        app_version_txt.setLayoutParams(layoutMenu1);
        vehicle.setLayoutParams(layoutMenu1);
        vehicle_nearby.setLayoutParams(layoutMenu1);
        signOut.setLayoutParams(layoutMenu1);
        policy.setLayoutParams(layoutMenu1);
        geo_fences.setLayoutParams(layoutMenu1);
        imgTitle.setLayoutParams(layoutMenu1);
        support_details.setLayoutParams(layoutMenu1);
        driver_list_txt.setLayoutParams(layoutMenu1);
        txt_exec_report.setLayoutParams(layoutMenu1);
        txt_toll_report.setLayoutParams(layoutMenu1);
        geo_fences_report.setLayoutParams(layoutMenu1);
        fuel_summary_report.setLayoutParams(layoutMenu1);
        add_geo_fences_report.setLayoutParams(layoutMenu1);
        billing.setLayoutParams(layoutMenu1);
        fuel_tank_report.setLayoutParams(layoutMenu1);

        home.setPadding(width * 3 / 100, 0, 0, 0);
        setting.setPadding(width * 3 / 100, 0, 0, 0);
        notification.setPadding(width * 3 / 100, 0, 0, 0);
        kms_Summary.setPadding(width * 3 / 100, 0, 0, 0);
        app_version_txt.setPadding(width * 3 / 100, 0, 0, 0);
        vehicle.setPadding(width * 3 / 100, 0, 0, 0);
        vehicle_nearby.setPadding(width * 3 / 100, 0, 0, 0);
        signOut.setPadding(width * 3 / 100, 0, 0, 0);
        policy.setPadding(width * 3 / 100, 0, 0, 0);
        imgTitle.setPadding(width * 3 / 100, 0, 0, 0);
        geo_fences.setPadding(width * 3 / 100, 0, 0, 0);
        support_details.setPadding(width * 3 / 100, 0, 0, 0);
        driver_list_txt.setPadding(width * 3 / 100, 0, 0, 0);
        txt_exec_report.setPadding(width * 3 / 100, 0, 0, 0);
        txt_toll_report.setPadding(width * 3 / 100, 0, 0, 0);
        geo_fences_report.setPadding(width * 3 / 100, 0, 0, 0);
        fuel_summary_report.setPadding(width * 3 / 100, 0, 0, 0);
        add_geo_fences_report.setPadding(width * 3 / 100, 0, 0, 0);
        billing.setPadding(width * 3 / 100, 0, 0, 0);
        fuel_tank_report.setPadding(width * 3 / 100, 0, 0, 0);

        home.setGravity(Gravity.CENTER | Gravity.LEFT);
        setting.setGravity(Gravity.CENTER | Gravity.LEFT);
        notification.setGravity(Gravity.CENTER | Gravity.LEFT);
        kms_Summary.setGravity(Gravity.CENTER | Gravity.LEFT);
        app_version_txt.setGravity(Gravity.CENTER | Gravity.LEFT);
        vehicle.setGravity(Gravity.CENTER | Gravity.LEFT);
        vehicle_nearby.setGravity(Gravity.CENTER | Gravity.LEFT);
        signOut.setGravity(Gravity.CENTER | Gravity.LEFT);
        policy.setGravity(Gravity.CENTER | Gravity.LEFT);
        geo_fences.setGravity(Gravity.CENTER | Gravity.LEFT);
        support_details.setGravity(Gravity.CENTER | Gravity.LEFT);
        driver_list_txt.setGravity(Gravity.CENTER | Gravity.LEFT);
        txt_exec_report.setGravity(Gravity.CENTER | Gravity.LEFT);
        txt_toll_report.setGravity(Gravity.CENTER | Gravity.LEFT);
        geo_fences_report.setGravity(Gravity.CENTER | Gravity.LEFT);
        fuel_summary_report.setGravity(Gravity.CENTER | Gravity.LEFT);
        add_geo_fences_report.setGravity(Gravity.CENTER | Gravity.LEFT);
        billing.setGravity(Gravity.CENTER | Gravity.LEFT);
        fuel_tank_report.setGravity(Gravity.CENTER | Gravity.LEFT);

        LinearLayout.LayoutParams layoutImageMenu1 = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        layoutImageMenu1.width = width * 8 / 100;
        layoutImageMenu1.height = height * 8 / 100;
        layoutImageMenu1.gravity = Gravity.CENTER | Gravity.LEFT;
        layoutImageMenu1.setMargins(width * 1 / 100, 0, 0, 0);
        slide_home.setLayoutParams(layoutImageMenu1);
        slide_logout.setLayoutParams(layoutImageMenu1);
        slide_policy.setLayoutParams(layoutImageMenu1);
        slide_support.setLayoutParams(layoutImageMenu1);
        slide_sites.setLayoutParams(layoutImageMenu1);
        slide_notification.setLayoutParams(layoutImageMenu1);
        slide_kms.setLayoutParams(layoutImageMenu1);
        slide_app_version.setLayoutParams(layoutImageMenu1);
        slide_settings.setLayoutParams(layoutImageMenu1);
        slide_vehicle_list.setLayoutParams(layoutImageMenu1);
        slide_vehicle_list_nearby.setLayoutParams(layoutImageMenu1);
        slide_driver_list.setLayoutParams(layoutImageMenu1);
        slide_exec_report.setLayoutParams(layoutImageMenu1);
        slide_toll_report.setLayoutParams(layoutImageMenu1);
        slide_geofence_report.setLayoutParams(layoutImageMenu1);
        slide_fuelsumm_report.setLayoutParams(layoutImageMenu1);
        slide_add_geofence_report.setLayoutParams(layoutImageMenu1);
        slide_tanksumm_report.setLayoutParams(layoutImageMenu1);
        slide_billing.setLayoutParams(layoutImageMenu1);
        // imgTitle.setLayoutParams(layoutImageMenu1);
        //support_details.setLayoutParams(layoutImageMenu1);

        LinearLayout.LayoutParams layoutViewMenu1 = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        layoutViewMenu1.width = width * 70 / 100;
        layoutViewMenu1.height = (int) (height * 0.3 / 100);
        layoutViewMenu1.gravity = Gravity.CENTER | Gravity.LEFT;
        view_home.setLayoutParams(layoutViewMenu1);
        view_setting.setLayoutParams(layoutViewMenu1);
        view_notification.setLayoutParams(layoutViewMenu1);
        view_kms_summary.setLayoutParams(layoutViewMenu1);
        view_app_version.setLayoutParams(layoutViewMenu1);
        view_vehicle.setLayoutParams(layoutViewMenu1);
        view_vehicle_nearby.setLayoutParams(layoutViewMenu1);
        view_signOut.setLayoutParams(layoutViewMenu1);
        view_geo_fences.setLayoutParams(layoutViewMenu1);
        view_support_details.setLayoutParams(layoutViewMenu1);
        view_policy.setLayoutParams(layoutViewMenu1);
        view_driver_list.setLayoutParams(layoutViewMenu1);
        view_exec_report.setLayoutParams(layoutViewMenu1);
        view_toll_report.setLayoutParams(layoutViewMenu1);
        view_geo_fences_report.setLayoutParams(layoutViewMenu1);
        view_fuel_summary_report.setLayoutParams(layoutViewMenu1);
        view_add_geo_fences_report.setLayoutParams(layoutViewMenu1);
        view_tank_summary_report.setLayoutParams(layoutViewMenu1);
        view_billing.setLayoutParams(layoutViewMenu1);

        if (width >= 600) {
            $TxtNoOfVehicle.setTextSize(16);
            $TxtOnlineVehicle.setTextSize(16);
            $TxtOfflineVehicle.setTextSize(16);
            $TxtNoOfVehicleValue.setTextSize(16);
            $TxtOnlineVehicleValue.setTextSize(16);
            $TxtOfflineVehicleValue.setTextSize(16);

            $TxtMovingVehicle.setTextSize(16);
            $TxtMovingVehicleValue.setTextSize(16);
            $TxtNodataVehicle.setTextSize(16);
            $TxtNodataVehicleValue.setTextSize(16);

            home.setTextSize(16);
            setting.setTextSize(16);
            notification.setTextSize(16);
            kms_Summary.setTextSize(16);
            app_version_txt.setTextSize(16);
            vehicle.setTextSize(16);
            vehicle_nearby.setTextSize(16);
            signOut.setTextSize(16);
            policy.setTextSize(16);
            geo_fences.setTextSize(16);
            support_details.setTextSize(16);
            $TxtTitle.setTextSize(18);
            mEdtSearch.setTextSize(16);
            driver_list_txt.setTextSize(16);
            txt_exec_report.setTextSize(16);
            txt_toll_report.setTextSize(16);
            geo_fences_report.setTextSize(16);
            fuel_summary_report.setTextSize(16);
            add_geo_fences_report.setTextSize(16);
            fuel_tank_report.setTextSize(16);

            billing.setTextSize(16);

        } else if (width > 501 && width < 600) {
            $TxtNoOfVehicle.setTextSize(15);
            $TxtOnlineVehicle.setTextSize(15);
            $TxtOfflineVehicle.setTextSize(15);
            $TxtNoOfVehicleValue.setTextSize(15);
            $TxtOnlineVehicleValue.setTextSize(15);
            $TxtOfflineVehicleValue.setTextSize(15);

            $TxtMovingVehicle.setTextSize(15);
            $TxtMovingVehicleValue.setTextSize(15);
            $TxtNodataVehicle.setTextSize(15);
            $TxtNodataVehicleValue.setTextSize(15);

            home.setTextSize(15);
            setting.setTextSize(15);
            notification.setTextSize(15);
            kms_Summary.setTextSize(15);
            app_version_txt.setTextSize(15);
            geo_fences.setTextSize(15);
            support_details.setTextSize(15);
            vehicle.setTextSize(15);
            vehicle_nearby.setTextSize(15);
            signOut.setTextSize(15);
            $TxtTitle.setTextSize(16);
            mEdtSearch.setTextSize(15);
            mTxtLastSync.setTextSize(15);
            driver_list_txt.setTextSize(15);
            txt_exec_report.setTextSize(15);
            txt_toll_report.setTextSize(15);
            geo_fences_report.setTextSize(15);
            add_geo_fences_report.setTextSize(15);
            fuel_summary_report.setTextSize(15);
            policy.setTextSize(15);
            fuel_tank_report.setTextSize(15);

            billing.setTextSize(15);

        } else if (width > 260 && width < 500) {
            $TxtNoOfVehicle.setTextSize(14);
            $TxtOnlineVehicle.setTextSize(14);
            $TxtOfflineVehicle.setTextSize(14);
            $TxtNoOfVehicleValue.setTextSize(14);
            $TxtOnlineVehicleValue.setTextSize(14);
            $TxtOfflineVehicleValue.setTextSize(14);

            $TxtMovingVehicle.setTextSize(14);
            $TxtMovingVehicleValue.setTextSize(14);
            $TxtNodataVehicle.setTextSize(14);
            $TxtNodataVehicleValue.setTextSize(14);
            fuel_summary_report.setTextSize(14);
            fuel_tank_report.setTextSize(14);

            home.setTextSize(14);
            setting.setTextSize(14);
            notification.setTextSize(14);
            kms_Summary.setTextSize(14);
            app_version_txt.setTextSize(14);
            geo_fences.setTextSize(14);
            vehicle.setTextSize(14);
            vehicle_nearby.setTextSize(14);
            signOut.setTextSize(14);
            support_details.setTextSize(14);
            $TxtTitle.setTextSize(15);
            mEdtSearch.setTextSize(14);
            mTxtLastSync.setTextSize(14);
            driver_list_txt.setTextSize(14);
            txt_exec_report.setTextSize(14);
            txt_toll_report.setTextSize(14);
            geo_fences_report.setTextSize(14);
            add_geo_fences_report.setTextSize(14);
            policy.setTextSize(14);
            billing.setTextSize(14);

        } else if (width <= 260) {
            $TxtNoOfVehicle.setTextSize(13);
            $TxtOnlineVehicle.setTextSize(13);
            $TxtOfflineVehicle.setTextSize(13);
            $TxtNoOfVehicleValue.setTextSize(13);
            $TxtOnlineVehicleValue.setTextSize(13);
            $TxtOfflineVehicleValue.setTextSize(13);

            $TxtMovingVehicle.setTextSize(13);
            $TxtMovingVehicleValue.setTextSize(13);
            $TxtNodataVehicle.setTextSize(13);
            $TxtNodataVehicleValue.setTextSize(13);
            fuel_summary_report.setTextSize(13);
            fuel_tank_report.setTextSize(13);

            home.setTextSize(13);
            setting.setTextSize(13);
            notification.setTextSize(13);
            kms_Summary.setTextSize(13);
            geo_fences.setTextSize(13);
            app_version_txt.setTextSize(13);
            vehicle.setTextSize(13);
            vehicle_nearby.setTextSize(13);
            signOut.setTextSize(13);
            support_details.setTextSize(13);
            $TxtTitle.setTextSize(14);
            mEdtSearch.setTextSize(13);
            mTxtLastSync.setTextSize(13);
            driver_list_txt.setTextSize(13);
            txt_exec_report.setTextSize(13);
            txt_toll_report.setTextSize(13);
            geo_fences_report.setTextSize(13);
            add_geo_fences_report.setTextSize(13);
            policy.setTextSize(13);
            billing.setTextSize(13);
        }
    }

    private void saveSelectedGroup(String mGroupName) {
        try {
            SharedPreferences.Editor editor = sp.edit();
            editor.putString("selected_group", mGroupName);
            editor.commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {

                Intent cancel_intent = new Intent(getApplicationContext(), VehicleTrackingService.class);
                PendingIntent cancel_pendingIntent = PendingIntent.getService(getApplicationContext(), sp.getInt("notif_id", 0), cancel_intent, 0);
                AlarmManager alarmManager = (AlarmManager) getApplicationContext().getSystemService(Context.ALARM_SERVICE);
                alarmManager.cancel(cancel_pendingIntent);
                stopService(cancel_intent);
                return true;
            }
        }
        return false;
    }

//    public void update3DmapEnabled(boolean mIsEnabled) {
//
//        try {
//            SharedPreferences.Editor editor = sp.edit();
//
//            editor.putBoolean("animate_map_enabled", mIsEnabled);
//            editor.commit();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//    }

    public void updatePaymentDue(boolean mIsEnabled) {

        try {
            SharedPreferences.Editor editor = sp.edit();
            editor.putBoolean("payment_due_load", mIsEnabled);
            editor.commit();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    // check version on play store and force update
    public void forceUpdate(){
        PackageManager packageManager = this.getPackageManager();
        PackageInfo packageInfo = null;
        try {
            packageInfo =  packageManager.getPackageInfo(getPackageName(),0);
            String currentVersion = packageInfo.versionName;
            new ForceUpdateAsync(currentVersion,VehicleListActivity.this).execute();
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

    }

    public class ForceUpdateAsync extends AsyncTask<String, String, JSONObject>{

        private String latestVersion;
        private String currentVersion;
        private Context context;
        public ForceUpdateAsync(String currentVersion, Context context){
            this.currentVersion = currentVersion;
            this.context = context;
        }

        @Override
        protected JSONObject doInBackground(String... params) {
            try {
                Log.d("packagename",""+context.getPackageName());
                latestVersion = Jsoup.connect("https://play.google.com/store/apps/details?id=" + context.getPackageName()+ "&hl=en")
                        .timeout(30000)
                        .userAgent("Mozilla/5.0 (Windows; U; WindowsNT 5.1; en-US; rv1.8.1.6) Gecko/20070725 Firefox/2.0.0.6")
                        .referrer("http://www.google.com")
                        .get()
                        .select("div.hAyfc:nth-child(4) > span:nth-child(2) > div:nth-child(1) > span:nth-child(1)")
                        .first()
                        .ownText();
                Log.e("latestversion","---"+latestVersion);
//                latestVersion = Jsoup.connect("https://play.google.com/store/apps/details?id="+context.getPackageName()+"&hl=en")
//                        .timeout(30000)
//                        .userAgent("Mozilla/5.0 (Windows; U; WindowsNT 5.1; en-US; rv1.8.1.6) Gecko/20070725 Firefox/2.0.0.6")
//                        .referrer("http://www.google.com")
//                        .get()
//                        .select("div[itemprop=softwareVersion]")
//                        .first()
//                        .ownText();
//                Log.d("latestVersion",""+latestVersion);

            } catch (IOException e) {
                e.printStackTrace();
            }
            return new JSONObject();
        }

        @Override
        protected void onPostExecute(JSONObject jsonObject) {
            try {
                if (latestVersion != null) {
                    if (!currentVersion.equalsIgnoreCase(latestVersion)) {
                        // Toast.makeText(context,"update is available.",Toast.LENGTH_LONG).show();
                        if (!(context instanceof LandingPage)) {
                            if (!((Activity) context).isFinishing()) {
                                showForceUpdateDialog();
                            }
                        }
                    }
                }
                super.onPostExecute(jsonObject);
                compareVersion(currentVersion, latestVersion);
            }
            catch (Exception e){
                e.printStackTrace();
            }
        }

        public int compareVersion(String version1, String version2) {
            String[] arr1 = version1.split("\\.");
            String[] arr2 = version2.split("\\.");

//            int i=0;
            int res=0;
            int maxIndex = Math.min(arr1.length, arr2.length);

            for (int i = 0; i < maxIndex; i ++) {
                int oldVersionPart = Integer.valueOf(arr1[i]);
                int newVersionPart = Integer.valueOf(arr2[i]);

                System.out.println("version part 1"+oldVersionPart);
                System.out.println("version part 2"+newVersionPart);
                if (oldVersionPart < newVersionPart) {
                    res = -1;
                    System.out.println("version part same version");
                    showForceUpdateDialog();
                    break;
                } else if (oldVersionPart > newVersionPart) {
                    res = 1;
                    break;
                }

            }

            // If versions are the same so far, but they have different length...
            if (res == 0 && arr1.length != arr2.length) {
                res = (arr1.length > arr2.length)?1:-1;
            }
//            while(i<arr1.length || i<arr2.length){
//                if(i<arr1.length && i<arr2.length){
//                    if(Integer.parseInt(arr1[i]) < Integer.parseInt(arr2[i])){
//                        System.out.println("current version is less");
//                        System.out.println(arr1[i]);
//                        showForceUpdateDialog();
//                        return -1;
//                    }else if(Integer.parseInt(arr1[i]) > Integer.parseInt(arr2[i])){
//                        System.out.println("current version is higher ");
//                        System.out.println(i);
//                        return 1;
//                    }
//                }
//                i++;
//            }
            return 0;
        }

//        public int compareVersion(String version1, String version2) {
//            String[] arr1 = version1.split("\\.");
//            String[] arr2 = version2.split("\\.");
//
//            int i=0;
//            while(i<arr1.length || i<arr2.length){
//                if(i<arr1.length && i<arr2.length){
//                    if(Integer.parseInt(arr1[i]) < Integer.parseInt(arr2[i])){
//                        System.out.println("current version is less");
//                        System.out.println(arr1[i]);
//                        showForceUpdateDialog();
//                        return -1;
//                    }else if(Integer.parseInt(arr1[i]) > Integer.parseInt(arr2[i])){
//                        System.out.println("current version is higher ");
//                        System.out.println(i);
//                        return 1;
//                    }
//                }
//                i++;
//            }
//            return 0;
//        }

        public void showForceUpdateDialog(){

            dialog = new Dialog(VehicleListActivity.this);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.update_dialog);
            TextView now =(TextView)dialog.findViewById(R.id.update);
            Log.d("packagename",""+context.getPackageName());
            now.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    SharedPreferences.Editor editor = sp.edit();
//                    editor.putBoolean("force_update", true);
//                    editor.commit();
                    context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + context.getPackageName())));
                    dialog.dismiss();
                }
            });

            dialog.show();

//            final AlertDialog.Builder builder = new AlertDialog.Builder(context);
//            builder.setTitle("A New Update is Available");
//            builder.setPositiveButton("Update", new DialogInterface.OnClickListener() {
//                @Override
//                public void onClick(DialogInterface dialog, int which) {
////                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse
////                            ("market://details?id=yourAppPackageName")));
//                    context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + context.getPackageName())));
//                    dialog.dismiss();
//                }
//            });
//
//            builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
//                @Override
//                public void onClick(DialogInterface dialog, int which) {
//                    dialog.dismiss();
//                }
//            });
//
//            builder.setCancelable(false);
//            dialog = builder.show();
        }


    }

    @Override
    protected void onResume() {
        super.onResume();
        userverification();

    }
}

