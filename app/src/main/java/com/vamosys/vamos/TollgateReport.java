package com.vamosys.vamos;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.SQLException;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.preference.PreferenceManager;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import androidx.fragment.app.FragmentManager;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

import androidx.appcompat.widget.Toolbar;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.github.mikephil.charting.charts.BarChart;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.vamosys.model.TollgateDto;
import com.vamosys.model.Tollmodel;
import com.vamosys.utils.ConnectionDetector;
import com.vamosys.utils.Constant;
import com.vamosys.utils.DaoHandler;
import com.vamosys.utils.HorizontalScrollView;
import com.vamosys.utils.TypefaceUtil;
import com.vamosys.utils.UnCaughtException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.osmdroid.api.IMapController;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.infowindow.InfoWindow;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class TollgateReport extends AppCompatActivity implements View.OnClickListener,OnMapReadyCallback {

    static ListView lv;
    //    TextView fromdate, todate, fromdatevalue, todatevalue, mHeadTitle;
    //    EditText mEdtSearch;
    TextView mHeadTitle;
    Spinner groupSpinner;
    boolean isFromTime = false,ischecked = false;
    LinearLayout mGoogleMapLayout;
    ImageView $ChangeView;
    private static GoogleMap map;
    static float vehicle_zoom_level = 15.5F;
    View v1, v2, v3;
    static LinearLayout mFuelDataMapLayout;
    private int hour;
    private int minute;
    TextView mTxtNoRecord,mTxtFromDate,mTxtFromTime,mTxtEndDate,mTxtEndTime,mTxtstart,mTxtend,mBtnSubmit, fromdate, todate, fromdatevalue, todatevalue,id_from_time,id_to_time;
    ImageView mBackArrow, mImgChangeDate, mImgDataViewChange;
    LinearLayout bottomlyt;
    private int year, month, day;
    int mFromHourValue = 0, mFromMinuteValue = 0, mToHourValue = 0, mToMinuteValue = 0;
    boolean from_date, to_date;
    static Const cons;

    List<String> mGroupList = new ArrayList<String>();
    List<String> mGroupSpinnerList = new ArrayList<String>();
    String mSelectedGroup,firstname,lastname;
    String mUserId;
    static boolean isOsmEnabled = true;
    static String musername;
    String mStrFromDate, mStrToDate,mStrFromTime,mStrToTime,mEndTimeValue,mStartTimeValue;

    SimpleDateFormat timeFormat = new SimpleDateFormat(
            "hh:mm:ss aa");
    SimpleDateFormat timeFormatShow = new SimpleDateFormat(
            "hh:mm aa");

    ConnectionDetector cd;
    SharedPreferences sp;
    //OSM related
    static MapView mapview;
    private IMapController mapController;

  TableAdapter adapter;

    View bottomSheet;
    BottomSheetBehavior behavior;
    boolean bottomSheetEnabled = false;

    private static List<TollgateDto> mData = new ArrayList<TollgateDto>();
    private static List<TollgateDto> mDatalis = new ArrayList<TollgateDto>();

    Dialog marker_info_dialog;
    TextView id_from_date, id_to_date;
    static int width;
    static int height;
    static double mLatitude = 0;
    static double mLongitude = 0;

    boolean isHeaderPresent = false;
    private List<TollgateDto> mtollgateData = new ArrayList<TollgateDto>();
    List<Tollmodel> mtollGroupList = new ArrayList<Tollmodel>();
    List<Tollmodel> mtolldataList = new ArrayList<Tollmodel>();

    BarChart chart;
    private static final int SERIES_NR = 2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);



        Thread.setDefaultUncaughtExceptionHandler(new UnCaughtException(
                TollgateReport.this));
        setContentView(R.layout.activity_tollgate_report);


        cons = new Const();

        sp = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        cd = new ConnectionDetector(getApplicationContext());

        if (sp.getString("ip_adds", "") != null) {
            if (sp.getString("ip_adds", "").trim().length() > 0) {
                Const.API_URL = sp.getString("ip_adds", "");
            }
        }
        if (sp.getString("enabled_map", "").equalsIgnoreCase(getResources().getString(R.string.osm))) {
            isOsmEnabled = true;
        } else {
            isOsmEnabled = false;
        }
        init();
        screenArrange();

        adapter = new TableAdapter(this);

//        Calendar cal = Calendar.getInstance();
////        cal.add(Calendar.MONTH, -1);
//
//        cal.add(Calendar.DATE, -7);
//        Date result = cal.getTime();
//
//        SimpleDateFormat dfDate = new SimpleDateFormat("yyyy-MM-dd");
//
//        String mCurrentDate = dfDate.format(new Date());
//        final String mFromDate = dfDate.format(result);
////        mStrFromDate = mCurrentDate;
//        mStrFromDate = mFromDate;
//        mStrToDate = mCurrentDate;
////        fromdatevalue.setText(mFromDate);
////        todatevalue.setText(mCurrentDate);
        mStrFromDate = Const.getCurrentDate();
//        mEndDate = Const.getTripYesterdayDate2();

//        mStartDate = Const.getTripDate(String.valueOf(System.currentTimeMillis()));
        mStrToDate = Const.getTripDate(String.valueOf(System.currentTimeMillis()));


        long timeInMillis = System.currentTimeMillis();
        Calendar cal1 = Calendar.getInstance();
        cal1.setTimeInMillis(timeInMillis);

        Calendar cal = Calendar.getInstance();
//        Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);


        mFromHourValue = 00;
        mFromMinuteValue = 00;

//        System.out.println("Hi date is :::" + timeFormat.format(cal.getTime()) + " mFromHourValue " + mFromHourValue + " " + mFromMinuteValue);

//        mStartTimeValue = "12:00 AM";

//        mStartTime = "12:00:00 am";
        mStrFromTime = timeFormat.format(cal.getTime());


        final Calendar c = Calendar.getInstance();
        // Current Hour
        hour = c.get(Calendar.HOUR_OF_DAY);
        // Current Minute
        minute = c.get(Calendar.MINUTE);
        isFromTime = false;
        updateTime(hour, minute);

        fromdatevalue.setText(mStrFromDate);
        todatevalue.setText(mStrToDate);

        try {
            queryUserDB();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
//        bottomSheet = findViewById(R.id.bottom_sheet);
//        behavior = BottomSheetBehavior.from(bottomSheet);
//        behavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
//            @Override
//            public void onStateChanged(@NonNull View bottomSheet, int newState) {
//                // React to state change
////                System.out.println("Hi state 0000" + bottomSheetEnabled);
//
//
//            }
//
//            @Override
//            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
//                // React to dragging events
//            }
//        });


//        chart.setVisibility(View.GONE);
//        BarData data = new BarData(getXAxisValues(), getDataSet());
//        data.setGroupSpace(1f);
//        data.setValueTextSize(10f);
//        chart.setData(data);
//
//        chart.animateXY(2000, 2000);
//        chart.invalidate();
//        chart.setDrawBarShadow(false);
//        chart.setDrawValueAboveBar(true);
//
//        chart.setDescription("");
//
//        // if more than 60 entries are displayed in the chart, no values will be
//        // drawn
//        chart.setMaxVisibleValueCount(60);
//        chart.setScaleMinima(8f, 1f);
//        // scaling can now only be done on x- and y-axis separately
//        chart.setPinchZoom(false);
//        chart.setDoubleTapToZoomEnabled(true);
//        chart.setDrawGridBackground(false);
//
//        XAxis xAxis = chart.getXAxis();
//        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
////        xAxis.setTypeface(mTfLight);
//        xAxis.setDrawGridLines(false);
////        xAxis.setGranularity(1f); // only intervals of 1 day
////        xAxis.setLabelCount(7);
//
//
//        YAxis leftAxis = chart.getAxisLeft();
////        leftAxis.setTypeface(mTfLight);
////        leftAxis.setLabelCount(8, false);
////        leftAxis.setValueFormatter(custom);
//        leftAxis.setPosition(YAxis.YAxisLabelPosition.OUTSIDE_CHART);
//        leftAxis.setSpaceTop(15f);
////        leftAxis.setAxisMinimum(0f); // this replaces setStartAtZero(true)
//
//        YAxis rightAxis = chart.getAxisRight();
//        rightAxis.setDrawGridLines(false);
////        rightAxis.setTypeface(mTfLight);
////        rightAxis.setLabelCount(8, false);
//        //   rightAxis.setValueFormatter("Kms");
//        rightAxis.setSpaceTop(15f);
//        rightAxis.setAxisMinimum(0f); //


        // Setting up chart
//        setupChart();

        // Start plotting chart
        //  new ChartTask().execute();


//        XAxis xAxis = chart.getXAxis();
//        // xAxis.setTypeface(tf);
//        xAxis.setTextSize(12f);
//        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
//        xAxis.setTextColor(ColorTemplate.getHoloBlue());
//        xAxis.setEnabled(true);
//        xAxis.disableGridDashedLine();
//        xAxis.setSpaceBetweenLabels(5);
//        xAxis.setDrawGridLines(false);
//        xAxis.setAvoidFirstLastClipping(true);
////
////
//// - Y Axis
//        YAxis leftAxis = chart.getAxisLeft();
//        leftAxis.removeAllLimitLines();
//        //leftAxis.setTypeface(tf);
//        leftAxis.setPosition(YAxis.YAxisLabelPosition.OUTSIDE_CHART);
//        leftAxis.setTextColor(ColorTemplate.getHoloBlue());
//        leftAxis.setAxisMaxValue(1000f);
//        leftAxis.setAxisMinValue(0f); // to set minimum yAxis
//        leftAxis.setStartAtZero(false);
//        leftAxis.enableGridDashedLine(10f, 10f, 0f);
//        leftAxis.setDrawLimitLinesBehindData(true);
//        leftAxis.setDrawGridLines(true);
//        chart.getAxisRight().setEnabled(false);
//
//
////-----------------
//        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
//        leftAxis.setPosition(YAxis.YAxisLabelPosition.OUTSIDE_CHART);

        mTxtFromTime.setOnClickListener(this);
        mTxtFromDate.setOnClickListener(this);
        mTxtEndDate.setOnClickListener(this);
        mTxtEndTime.setOnClickListener(this);
        mBtnSubmit.setOnClickListener(this);
        mBackArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(TollgateReport.this, VehicleListActivity.class));
                finish();
            }
        });


//        mImgDataViewChange.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                if (mIsBarChartEnabled) {
////                    mImgDataViewChange.setBackgroundResource(R.drawable.bar_chart);
//                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//                        mImgDataViewChange.setImageDrawable(getResources().getDrawable(R.drawable.bar_chart, getApplicationContext().getTheme()));
//                    } else {
//                        mImgDataViewChange.setImageDrawable(getResources().getDrawable(R.drawable.bar_chart));
//                    }
//                    mIsBarChartEnabled = false;
////                    setupBarChart();
//                    chart.setVisibility(View.GONE);
//                    lv.setVisibility(View.VISIBLE);
//
//                    chart.clear();
//                    chart.setData(null);
//                    // chart.getData().notifyDataChanged();
//                    chart.notifyDataSetChanged();
//
//                } else {
////                    mImgDataViewChange.setBackgroundResource(R.drawable.data_table);
//
//                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//                        mImgDataViewChange.setImageDrawable(getResources().getDrawable(R.drawable.data_table, getApplicationContext().getTheme()));
//                    } else {
//                        mImgDataViewChange.setImageDrawable(getResources().getDrawable(R.drawable.data_table));
//                    }
//
//                    mIsBarChartEnabled = true;
//
//                    chart.setVisibility(View.VISIBLE);
//                    lv.setVisibility(View.GONE);
//
//
//                    setupBarChart();
//                }
//
//
//            }
//        });

        mImgChangeDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               // showVehicleInfoDialog();
                if (!bottomSheetEnabled) {
                    lv.setVisibility(View.GONE);
                    bottomlyt.setVisibility(View.VISIBLE);


                    mTxtFromDate.setText(mStrFromDate);
                    mTxtEndDate.setText(mStrToDate);
                    SimpleDateFormat timeDate = new SimpleDateFormat("HH:mm:ss");

                    String mCurrentTime = timeDate.format(new Date());

                    mTxtFromTime.setText("00:00:00");
                    mTxtEndTime.setText(mCurrentTime);

                  //  behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                } else {

                    bottomlyt.setVisibility(View.GONE);
                    lv.setVisibility(View.GONE);
                    // setData();
                   // behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                }

                if (bottomSheetEnabled) {
                    bottomSheetEnabled = false;
                } else {
                    bottomSheetEnabled = true;
                }
            }
        });

        groupSpinner
                .setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                    @Override
                    public void onItemSelected(AdapterView<?> parent,
                                               View view, int pos, long id) {
                        // TODO Auto-generated method stub
                        mSelectedGroup = mGroupList.get(pos);
                        saveSelectedGroup(mGroupSpinnerList.get(pos));
                        if (mSelectedGroup.equalsIgnoreCase("Select")) {

                        } else {
//                            mEdtSearch.setText("");
                            ConnectionDetector cd = new ConnectionDetector(getApplicationContext());
                            if (cd.isConnectingToInternet()) {
//                                mStrFromDate = fromdatevalue.getText().toString().trim();
//                                mStrToDate = todatevalue.getText().toString().trim();

                                new getTollgateReport().execute();
                            } else {
                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.network_connection),
                                        Toast.LENGTH_SHORT).show();
                            }
                        }

                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> arg0) {
                        // TODO Auto-generated method stub

                    }

                });
    }

    private void updateTime(int hours, int mins) {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.HOUR_OF_DAY, hours);
        cal.set(Calendar.MINUTE, mins);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
//        System.out.println("Hi date is 222:::" + timeFormat.format(cal.getTime()));

//        String aTime = new StringBuilder().append(hours).append(':')
//                .append(minutes).append(" ").append(timeSet).toString();
        String aTime = timeFormatShow.format(cal.getTime());
//        String aTime2 = new StringBuilder().append(hours).append(':')
//                .append(minutes).append(":00").append(minutes).append(" ").append(timeSet2).toString();

        String aTime2 = timeFormat.format(cal.getTime());
       // String aTime3 = timeFormatFuelFillReport.format(cal.getTime());

        SimpleDateFormat hourFormat = new SimpleDateFormat(
                "HH");
        SimpleDateFormat minuteFormat = new SimpleDateFormat(
                "mm");

        if (isFromTime) {
            mStrFromTime = aTime2;
            mStartTimeValue = aTime;



            mFromHourValue = Integer.valueOf(hourFormat.format(cal.getTime()));
            mFromMinuteValue = Integer.valueOf(minuteFormat.format(cal.getTime()));
            mTxtFromTime.setText(mStartTimeValue);
//            System.out.println("Hi from time value " + mFromHourValue + " " + mFromMinuteValue);
        } else {
            mStrToTime = aTime2;
            mEndTimeValue = aTime;

            mTxtEndTime.setText(mEndTimeValue);


            mToHourValue = Integer.valueOf(hourFormat.format(cal.getTime()));
            mToMinuteValue = Integer.valueOf(minuteFormat.format(cal.getTime()));

//            System.out.println("Hi to time value " + mToHourValue + " " + mToMinuteValue);
        }


    }

    public void init() {
        lv = (ListView) findViewById(R.id.executive_report_listView1);
//        lv.setVisibility(View.GONE);
        chart = (BarChart) findViewById(R.id.chart);
//        fromdate = (TextView) findViewById(R.id.executive_report_from_date_text);
//        todate = (TextView) findViewById(R.id.executive_report_to_date_text);
//        fromdatevalue = (TextView) findViewById(R.id.executive_report_from_date_value);
//        todatevalue = (TextView) findViewById(R.id.executive_report_to_date_value);
        groupSpinner = (Spinner) findViewById(R.id.executive_report_groupspinner);
        mGoogleMapLayout = (LinearLayout) findViewById(R.id.google_map_layout);
        RelativeLayout rl = (RelativeLayout) findViewById(R.id.map_view_relativelayout);
        $ChangeView = (ImageView) findViewById(R.id.temperature_data_map_ViewIcon);
        $ChangeView.setOnClickListener(this);

        fromdate = (TextView) findViewById(R.id.from_date_text);
        todate = (TextView) findViewById(R.id.to_date_text);
        fromdatevalue = (TextView) findViewById(R.id.from_date_value);
        todatevalue = (TextView) findViewById(R.id.to_date_value);
        bottomlyt = (LinearLayout)findViewById(R.id.bottom_layout);

        mTxtFromDate = (TextView) findViewById(R.id.txt_start_date_value);
        mTxtFromTime = (TextView) findViewById(R.id.txt_start_time_value);
        mTxtEndDate = (TextView) findViewById(R.id.txt_end_date_value);
        mTxtEndTime = (TextView) findViewById(R.id.txt_end_time_value);
        mBtnSubmit = (Button) findViewById(R.id.btn_done);

        mFuelDataMapLayout = (LinearLayout) findViewById(R.id.temperature_data_map_view_layout);
        mFuelDataMapLayout.setVisibility(View.GONE);

//        v1 = (View) findViewById(R.id.executive_report_view_vertical);
//        v2 = (View) findViewById(R.id.executive_report_view_vertical1);
//        v3 = (View) findViewById(R.id.executive_report_view_horizontal);
        v1 = (View) findViewById(R.id.view_vertical);
        v2 = (View) findViewById(R.id.view_vertical1);
        mTxtNoRecord = (TextView) findViewById(R.id.executive_report_no_record_txt);
        mBackArrow = (ImageView) findViewById(R.id.executive_report_view_Back);
        mHeadTitle = (TextView) findViewById(R.id.executive_report_tvTitle);
        mImgChangeDate = (ImageView) findViewById(R.id.executive_report_change_date);
        //mImgDataViewChange = (ImageView) findViewById(R.id.executive_report_change_data_view);

        fromdate.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        todate.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        fromdatevalue.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        todatevalue.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        mHeadTitle.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));

        if (isOsmEnabled) {
            $ChangeView.setVisibility(View.GONE);
            mGoogleMapLayout.setVisibility(View.GONE);
//            mFuelDataMapLayout.setVisibility(View.GONE);
            rl.setVisibility(View.VISIBLE);
            mapview = new MapView(this);
            mapview.setTilesScaledToDpi(true);
            rl.addView(mapview, new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.FILL_PARENT,
                    RelativeLayout.LayoutParams.FILL_PARENT));

            mapview.setBuiltInZoomControls(false);
            mapview.setMultiTouchControls(true);
            mapController = mapview.getController();
            mapController.setZoom(8);
            mapview.getOverlays().clear();
            mapview.invalidate();
            mapview.getOverlays().add(Constant.getTilesOverlay(getApplicationContext()));
        } else {
            Log.d("googlemap","success");
            $ChangeView.setVisibility(View.VISIBLE);
            mGoogleMapLayout.setVisibility(View.VISIBLE);
            rl.setVisibility(View.GONE);

            FragmentManager myFragmentManager = getSupportFragmentManager();
            SupportMapFragment myMapFragment = (SupportMapFragment) myFragmentManager
                   .findFragmentById(R.id.temperature_data_map);
            myMapFragment.getMapAsync(this);
        }


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            //mImgDataViewChange.setImageDrawable(getResources().getDrawable(R.drawable.bar_chart, getApplicationContext().getTheme()));
        } else {
           // mImgDataViewChange.setImageDrawable(getResources().getDrawable(R.drawable.bar_chart));
        }

    }

    private void saveSelectedGroup(String mGroupName) {
        try {
            SharedPreferences.Editor editor = sp.edit();
            editor.putString("selected_group", mGroupName);
            editor.commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private boolean showVehicleInfoDialog() {

        TextView
                id__popupto_date_label,
                id_popup_from_label,  id_popup_from_time_label,id__popupto_time_label,
                id_txt_divider1, id_txt_divider2, id_histroy_vehicleid;
        Button id_history_selection_done, id_history_selection_cancel;
//        if (isFromList) {
//            marker_info_dialog = new Dialog(this, android.R.style.Theme_Light_NoTitleBar_Fullscreen);
//            marker_info_dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
//        } else {
        marker_info_dialog = new Dialog(TollgateReport.this);
//        }
        marker_info_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        marker_info_dialog.setContentView(R.layout.trip_summary_popup_layout);
        marker_info_dialog.setCancelable(false);
        //LinearLayout id_date_time_picker_layout;
        LinearLayout id_from_datetime_layout;
//        ImageView id_history_vehicle_info_icon;
//        final TextView id_histroy_vehicleid, id_history_type_value, id_from_label, id_from_date, id_from_time, id_to_date, id_to_time;
        // Button id_history_selection_done, id_history_selection_cancel;

        id_from_datetime_layout = (LinearLayout) marker_info_dialog.findViewById(R.id.id_popup_trip_summ_from_datetime_layout);


        // id_history_vehicle_info_icon = (ImageView) marker_info_dialog.findViewById(R.id.id_popup_history_vehicle_info_icon);
        id_histroy_vehicleid = (TextView) marker_info_dialog.findViewById(R.id.id_popup_trip_summ_vehicleid);

        id_txt_divider1 = (TextView) marker_info_dialog.findViewById(R.id.trip_summ_divider_txt_1);
        id_txt_divider2 = (TextView) marker_info_dialog.findViewById(R.id.trip_summ_divider_txt_2);


        // id_popup_history_labelid = (TextView) marker_info_dialog.findViewById(R.id.id_popup_history_labelid);
        //   id_popup_history_type_value = (TextView) marker_info_dialog.findViewById(R.id.id_popup_history_type_value);
        id_popup_from_label = (TextView) marker_info_dialog.findViewById(R.id.id_popup_trip_summ_from_label);
        id__popupto_time_label = (TextView) marker_info_dialog.findViewById(R.id.id_date_time_to_time_label);

        id_from_date = (TextView) marker_info_dialog.findViewById(R.id.id_popup_trip_summ_from_date);
        id__popupto_date_label = (TextView) marker_info_dialog.findViewById(R.id.id_popup_trip_summ_to_date_label);
        id_popup_from_time_label = (TextView) marker_info_dialog.findViewById(R.id.id_date_time_from_time_label);
        id_to_date = (TextView) marker_info_dialog.findViewById(R.id.id_popup_trip_summ_to_date);
        id_from_time = (TextView) marker_info_dialog.findViewById(R.id.id_date_time_from_time);
        id_to_time = (TextView) marker_info_dialog.findViewById(R.id.id_date_time_to_time);


//        id__popupto_date_label = (TextView) marker_info_dialog.findViewById(R.id.id__popupto_date_label);
//        id_to_date = (TextView) marker_info_dialog.findViewById(R.id.id_popup_to_date);
//        id_popup_to_time_label = (TextView) marker_info_dialog.findViewById(R.id.id_popup_to_time_label);
//        id_to_time = (TextView) marker_info_dialog.findViewById(R.id.id_popup_to_time);

        id_history_selection_done = (Button) marker_info_dialog.findViewById(R.id.id_popup_trip_summ_selection_done);
        id_history_selection_cancel = (Button) marker_info_dialog.findViewById(R.id.id_popup_trip_summ_selection_cancel);


//        id_from_date.setText(cons.GetCurrentDate(Calendar.getInstance()));
//        id_to_date.setText(cons.GetCurrentDate(Calendar.getInstance()));


        id_from_date.setText(mStrFromDate);
        id_to_date.setText(mStrToDate);

        SimpleDateFormat timeDate = new SimpleDateFormat("HH:mm:ss");

        String mCurrentTime = timeDate.format(new Date());

        id_from_time.setText("00:00:00");
        id_to_time.setText(mCurrentTime);

        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        // int height = metrics.heightPixels;
        //  int width = metrics.widthPixels;
        Constant.ScreenWidth = width;
        Constant.ScreenHeight = height;

//        LinearLayout.LayoutParams history_for_layout = new LinearLayout.LayoutParams(
//                LinearLayout.LayoutParams.WRAP_CONTENT,
//                LinearLayout.LayoutParams.WRAP_CONTENT);
//        history_for_layout.width = width * 90 / 100;
//        history_for_layout.height = LinearLayout.LayoutParams.WRAP_CONTENT;
//        history_for_layout.gravity = Gravity.CENTER;
//        history_for_layout.setMargins(width * 5 / 100, 0, width * 5 / 100, 0);
//        id_history_type_layout.setLayoutParams(history_for_layout);

//        LinearLayout.LayoutParams backImageParams = new LinearLayout.LayoutParams(
//                LinearLayout.LayoutParams.WRAP_CONTENT,
//                LinearLayout.LayoutParams.WRAP_CONTENT);
//        backImageParams.width = width * 80 / 100;
//        backImageParams.height = LinearLayout.LayoutParams.WRAP_CONTENT;
//        backImageParams.gravity = Gravity.CENTER;
//
//        id_popup_history_labelid.setLayoutParams(backImageParams);
//        id_popup_history_type_value.setLayoutParams(backImageParams);

        LinearLayout.LayoutParams backImageParams1 = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        backImageParams1.width = width * 40 / 100;
        backImageParams1.height = LinearLayout.LayoutParams.WRAP_CONTENT;

        id_popup_from_label.setLayoutParams(backImageParams1);
        id_from_date.setLayoutParams(backImageParams1);
        id__popupto_date_label.setLayoutParams(backImageParams1);
        id_to_date.setLayoutParams(backImageParams1);

        id__popupto_date_label.setLayoutParams(backImageParams1);
        id_to_date.setLayoutParams(backImageParams1);
        id__popupto_date_label.setLayoutParams(backImageParams1);
        id_to_date.setLayoutParams(backImageParams1);

        id_popup_from_time_label.setLayoutParams(backImageParams1);
        id_from_time.setLayoutParams(backImageParams1);
        id__popupto_time_label.setLayoutParams(backImageParams1);
        id_to_time.setLayoutParams(backImageParams1);


        if (width >= 600) {
            // id_popup_history_labelid.setTextSize(18);
            // id_popup_history_type_value.setTextSize(17);
            id_from_date.setTextSize(17);
            //  id_from_time.setTextSize(17);
            id_to_date.setTextSize(17);
            id_from_time.setTextSize(17);
            id_to_time.setTextSize(17);
            // id_to_time.setTextSize(17);
            //  $TxtOfflineVehicleValue.setTextSize(16);
        } else if (width > 501 && width < 600) {
            // id_popup_history_labelid.setTextSize(17);
            // id_popup_history_type_value.setTextSize(16);
            id_from_date.setTextSize(16);
            //id_from_time.setTextSize(16);
            id_to_date.setTextSize(16);
            id_from_time.setTextSize(16);
            id_to_time.setTextSize(16);
            // id_to_time.setTextSize(16);
            // $TxtOfflineVehicleValue.setTextSize(15);
        } else if (width > 260 && width < 500) {
            //id_popup_history_labelid.setTextSize(16);
            // id_popup_history_type_value.setTextSize(15);
            id_from_date.setTextSize(15);
            // id_from_time.setTextSize(15);
            id_to_date.setTextSize(15);
            id_from_time.setTextSize(15);
            id_to_time.setTextSize(15);
            //  id_to_time.setTextSize(15);
            // $TxtOfflineVehicleValue.setTextSize(14);
        } else if (width <= 260) {
            //  id_popup_history_labelid.setTextSize(15);
            //  id_popup_history_type_value.setTextSize(14);
            id_from_date.setTextSize(14);
            //  id_from_time.setTextSize(14);
            id_to_date.setTextSize(14);
            id_from_time.setTextSize(14);
            id_to_time.setTextSize(14);
            // id_to_time.setTextSize(14);
            //  $TxtOfflineVehicleValue.setTextSize(13);
        }


//        id_histroy_vehicleid.setText(Constant.SELECTED_VEHICLE_SHORT_NAME);
        id_histroy_vehicleid.setText(""+getResources().getString(R.string.executive_report));


        /** listener to handle when history is canceled */
        id_history_selection_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                opptionPopUp();
                marker_info_dialog.hide();
            }
        });

        /** listener to handle when history is selected */
        id_history_selection_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (cd.isConnectingToInternet()) {
                    marker_info_dialog.hide();
//                    id_date_time_picker_layout.setVisibility(View.GONE);
//                    id_map_info_layout.setVisibility(View.GONE); "http://188.166.244.126:9000/
//
                    adapter = null;
                    adapter = new TableAdapter(TollgateReport.this);
//     String url = Const.API_URL + "/getTripHistorySummary?userId=" + Constant.SELECTED_USER_ID + "&vehicleId=" + Constant.SELECTED_VEHICLE_ID + "&fromDate=" + id_from_date.getText().toString() + "&toDate=" + id_to_date.getText().toString();
                    mtollgateData.clear();
                    lv.setAdapter(null);

                    mStrFromDate = id_from_date.getText().toString().trim();
                    mStrToDate = id_to_date.getText().toString().trim();
                    Log.d("fromdate",""+mStrFromDate );
                    Log.d("todate",""+mStrToDate);
                    mStrFromTime = id_from_time.getText().toString().trim();
                    mStrToTime = id_to_time.getText().toString().trim();
                    Log.d("fromtime",""+mStrFromTime );
                    Log.d("totime",""+mStrToTime);
//                    String url = Const.API_URL + "mobile/getTripHistorySummary?userId=" + Constant.SELECTED_USER_ID + "&vehicleId=" + Constant.SELECTED_VEHICLE_ID + "&fromDate=" + id_from_date.getText().toString() + "&toDate=" + id_to_date.getText().toString();
                    // String url = Const.API_URL + "mobile/getVehicleHistory4MobileV2?userId=" + Constant.SELECTED_USER_ID + "&vehicleId=" + Constant.SELECTED_VEHICLE_ID + "&fromDate=" + id_from_date.getText().toString() + "&fromTime=" + id_from_time.getText().toString() + "&toDate=" + id_to_date.getText().toString() + "&toTime=" + id_to_time.getText().toString() + "&macid=" + getStdTableValue("macid") + "&appid=" + getStdTableValue("appid");
                    // String par[] = {Const.API_URL + "mobile/getGeoFenceView?vehicleId=" + Constant.SELECTED_VEHICLE_ID + "&userId=" + Constant.SELECTED_USER_ID + "&macid=" + getStdTableValue("macid") + "&appid=" + getStdTableValue("appid")};
                    Toast.makeText(TollgateReport.this, getResources().getString(R.string.select_button), Toast.LENGTH_SHORT).show();
                    new getTollgateReport().execute();
                    //  new GetPOIInformation().execute(par);
                } else {
                    Toast internet_toast = Toast.makeText(getApplicationContext(), getResources().getString(R.string.network_connection), Toast.LENGTH_LONG);
                    internet_toast.show();
                }
            }
        });

        id_from_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar calNow = Calendar.getInstance();

                if (mStrFromDate != null) {
                    String[] dateArr = mStrFromDate.split("-");
                    int yearValue = 0, monthValue = 0, dateValue = 0;
                    if (dateArr.length > 0) {


                        yearValue = Integer.valueOf(dateArr[0]);
                        monthValue = Integer.valueOf(dateArr[1]);
                        dateValue = Integer.valueOf(dateArr[2]);

                        if (monthValue > 0) {
                            monthValue = monthValue - 1;
                        }

                    }
                    calNow.set(yearValue, monthValue, dateValue);
                }

                new DatePickerDialog(TollgateReport.this, new DatePickerDialog.OnDateSetListener() {
                    // when dialog box is closed, below method will be called.
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int day) {
                        id_from_date.setText(cons.getDateFormat(cons.getCalendarDate(year, month, day, 0, 0)));
                    }
                }, calNow.get(Calendar.YEAR), calNow.get(Calendar.MONTH), calNow.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        id_to_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar calNow = Calendar.getInstance();

                if (mStrToDate != null) {
                    String[] dateArr = mStrToDate.split("-");
                    int yearValue = 0, monthValue = 0, dateValue = 0;
                    if (dateArr.length > 0) {


                        yearValue = Integer.valueOf(dateArr[0]);
                        monthValue = Integer.valueOf(dateArr[1]);
                        dateValue = Integer.valueOf(dateArr[2]);

                        if (monthValue > 0) {
                            monthValue = monthValue - 1;
                        }

                    }
                    calNow.set(yearValue, monthValue, dateValue);
                }

                new DatePickerDialog(TollgateReport.this, new DatePickerDialog.OnDateSetListener() {
                    // when dialog box is closed, below method will be called.
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int day) {
                        id_to_date.setText(cons.getDateFormat(cons.getCalendarDate(year, month, day, 0, 0)));
                    }
                }, calNow.get(Calendar.YEAR), calNow.get(Calendar.MONTH), calNow.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        id_from_time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isFromTime = true;
                //   showDialog(TIME_DIALOG_ID);

                //showTimeDialog();
                Calendar calNow = Calendar.getInstance();
                new TimePickerDialog(TollgateReport.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hour, int minute) {
                        // TODO Auto-generated method stub
                        id_from_time.setText(new StringBuilder().append(pad(hour)).append(":").append(pad(minute)).append(":").append("00"));
                    }
                }, calNow.get(Calendar.HOUR_OF_DAY), calNow.get(Calendar.MINUTE), false).show();
            }
        });

        id_to_time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isFromTime = false;
                // showDialog(TIME_DIALOG_ID);

               // showTimeDialog();
                Calendar calNow = Calendar.getInstance();
                new TimePickerDialog(TollgateReport.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hour, int minute) {
                        // TODO Auto-generated method stub
                        id_to_time.setText(new StringBuilder().append(pad(hour)).append(":").append(pad(minute)).append(":").append("00"));
                    }
                }, calNow.get(Calendar.HOUR_OF_DAY), calNow.get(Calendar.MINUTE), false).show();
            }
        });



        marker_info_dialog.show();
        return true;
    }

    private void showTimeDialog() {
        if (isFromTime) {
            hour = mFromHourValue;
            minute = mFromMinuteValue;
//            System.out.println("Hi clicked hour minute value " + mFromHourValue + " " + mFromMinuteValue + " :: " + hour + " " + minute);
        } else {
            hour = mToHourValue;
            minute = mToMinuteValue;
//            System.out.println("Hi clicked hour minute value " + mToHourValue + " " + mToMinuteValue + " :: " + hour + " " + minute);
        }


//                hour=20;
//                minute=33;

        TimePickerDialog dialog2 = new TimePickerDialog(this, timePickerListener, hour, minute,
                false);
        dialog2.show();
    }
    private static String pad(int c) {
        if (c >= 10)
            return String.valueOf(c);
        else
            return "0" + String.valueOf(c);
    }
    public void queryUserDB() {
        DaoHandler da = new DaoHandler(getApplicationContext(), false);
        da.queryUserDB();

        mUserId = Constant.mDbUserId;
        mGroupList = Constant.mUserGroupList;
        setDropDownData();
    }

//    public void queryUserDB() {
//        DataBaseHandler db = new DataBaseHandler(this);
//        Cursor c = null;
//        // String qry = "SELECT DISTINCT patch_id,patch_name,mtp_id FROM "
//        // + DbHandler.TABLE_TM_DCP;
//        try {
//            // c = db.open().getDatabaseObj().rawQuery(qry, null);
//            c = db.open()
//                    .getDatabaseObj()
//                    .query(DataBaseHandler.TABLE_USER, null, null, null, null, null,
//                            null);
//            // int reporting_date_index = c
//            // .getColumnIndex("actual_reporting_date");
//            int user_id_index = c.getColumnIndex("user_id");
//            int group_list_index = c.getColumnIndex("group_list");
//            // int mtp_id_index = c.getColumnIndex("mtp_id");
//            // int dr_ch_name_index = c.getColumnIndex("dr_ch_name");
//            // System.out.println("The coubnt of daily count is ::::::"
//            // + c.getCount());
//            if (c.getCount() > 0) {
//
//                while (c.moveToNext()) {
//                    mUserId = c.getString(user_id_index);
//
////                    System.out.println("The group list is :::::"
////                            + c.getString(group_list_index));
//
//                    if (c.getString(group_list_index) != null) {
//                        Gson g = new Gson();
//                        InputStream is = new ByteArrayInputStream(c.getString(
//                                group_list_index).getBytes());
//                        Reader reader = new InputStreamReader(is);
//                        Type fooType = new TypeToken<List<String>>() {
//                        }.getType();
//
//                        mGroupList = g.fromJson(reader, fooType);
//                    }
//
//                }
//            }
//
//        } finally {
//            c.close();
//            db.close();
//        }
//        setDropDownData();
//    }

    private TimePickerDialog.OnTimeSetListener timePickerListener = new TimePickerDialog.OnTimeSetListener() {


        @Override
        public void onTimeSet(TimePicker view, int hourOfDay, int minutes) {
            // TODO Auto-generated method stub
            hour = hourOfDay;
            minute = minutes;

            updateTime(hour, minute);


        }

    };

    public void setDropDownData() {
        //mGroupList = new ArrayList<String>();
        // List<String> list = new ArrayList<String>();
        // list.add("Select");
        if (mGroupList.size() > 0) {

        } else {
            mGroupList.add("Select");
        }


        for (int i = 0; i < mGroupList.size(); i++) {

            String[] str_msg_data_array = mGroupList.get(i).split(":");

            mGroupSpinnerList.add(str_msg_data_array[0]);

        }


        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(
                TollgateReport.this, R.layout.spinner_row,
                mGroupSpinnerList) {

            public View getView(int position, View convertView, ViewGroup parent) {
                View v = super.getView(position, convertView, parent);
                // TextView v = (TextView) super.getView(position,
                // convertView,parent);
                // Constant.face(DailyCalls.this, (TextView) v);
                return v;
            }

            public View getDropDownView(int position, View convertView,
                                        ViewGroup parent) {
                View v = super.getDropDownView(position, convertView, parent);
                // ((TextView)
                // v).setBackgroundColor(Color.parseColor("#BBfef3da"));
                // TextView v = (TextView) super.getView(position,
                // convertView,parent);
                // Constant.face(DailyCalls.this, (TextView) v);
                return v;
            }
        };
        spinnerArrayAdapter
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        groupSpinner.setAdapter(spinnerArrayAdapter);
        String groupName = sp.getString("selected_group", "");
        int spinnerPosition = spinnerArrayAdapter.getPosition(groupName);

//set the default according to value
        groupSpinner.setSelection(spinnerPosition);
    }


    private void screenArrange() {
        // TODO Auto-generated method stub

        /* screen arrangements */
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        height = metrics.heightPixels;
        width = metrics.widthPixels;
        Constant.ScreenWidth = width;
        Constant.ScreenHeight = height;

        LinearLayout.LayoutParams backImageParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        backImageParams.width = width * 15 / 100;
        backImageParams.height = height * 7 / 100;
        backImageParams.gravity = Gravity.CENTER;
        mBackArrow.setLayoutParams(backImageParams);
        mBackArrow.setPadding(width * 1 / 100, height * 1 / 100, width * 1 / 100, height * 1 / 100);

        LinearLayout.LayoutParams headTxtParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        headTxtParams.width = width * 62 / 100;
        headTxtParams.height = height * 7 / 100;
        mHeadTitle.setLayoutParams(headTxtParams);
        mHeadTitle.setPadding(width * 2 / 100, 0, 0, 0);
        mHeadTitle.setGravity(Gravity.CENTER | Gravity.LEFT);


        LinearLayout.LayoutParams changeDataImageParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        changeDataImageParams.width = width * 9 / 100;
        changeDataImageParams.height = height * 7 / 100;
        changeDataImageParams.gravity = Gravity.CENTER;
//        mImgDataViewChange.setLayoutParams(changeDataImageParams);
//        mImgDataViewChange.setPadding(width * 1 / 100, height * 1 / 100,
//                width * 1 / 100, height * 1 / 100);


        LinearLayout.LayoutParams changeDateImageParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        changeDateImageParams.width = width * 9 / 100;
        changeDateImageParams.height = height * 7 / 100;
        changeDateImageParams.gravity = Gravity.CENTER;
        mImgChangeDate.setLayoutParams(changeDateImageParams);
        mImgChangeDate.setPadding(width * 1 / 100, height * 1 / 100,
                width * 1 / 100, height * 1 / 100);

//        LinearLayout.LayoutParams textParams = new LinearLayout.LayoutParams(
//                LinearLayout.LayoutParams.WRAP_CONTENT,
//                LinearLayout.LayoutParams.WRAP_CONTENT);
//        textParams.width = width * 45 / 100;
//        textParams.height = height * 9 / 2 / 100;
//        textParams.topMargin = (int) (height * 0.25 / 100);
//        textParams.leftMargin = width * 2 / 100;
//        textParams.rightMargin = width * 2 / 100;
//        fromdate.setLayoutParams(textParams);
//        todate.setLayoutParams(textParams);
//        fromdate.setGravity(Gravity.CENTER);
//        todate.setGravity(Gravity.CENTER);

//        LinearLayout.LayoutParams textValueParams = new LinearLayout.LayoutParams(
//                LinearLayout.LayoutParams.WRAP_CONTENT,
//                LinearLayout.LayoutParams.WRAP_CONTENT);
//        textValueParams.width = width * 45 / 100;
//        textValueParams.height = height * 9 / 2 / 100;
//        textValueParams.leftMargin = width * 2 / 100;
//        textValueParams.rightMargin = width * 2 / 100;
//        textValueParams.bottomMargin = (int) (height * 0.25 / 100);
//        fromdatevalue.setLayoutParams(textValueParams);
//        todatevalue.setLayoutParams(textValueParams);
//        fromdatevalue.setGravity(Gravity.CENTER);
//        todatevalue.setGravity(Gravity.CENTER);

//        LinearLayout.LayoutParams ViewParams = new LinearLayout.LayoutParams(
//                LinearLayout.LayoutParams.WRAP_CONTENT,
//                LinearLayout.LayoutParams.WRAP_CONTENT);
//        ViewParams.width = width * 1 / 100;
//        ViewParams.height = LinearLayout.LayoutParams.MATCH_PARENT;
//        v1.setLayoutParams(ViewParams);
//        v2.setLayoutParams(ViewParams);
//
//        LinearLayout.LayoutParams viewParams = new LinearLayout.LayoutParams(
//                LinearLayout.LayoutParams.WRAP_CONTENT,
//                LinearLayout.LayoutParams.WRAP_CONTENT);
//        viewParams.width = width;
//        viewParams.height = height * 1 / 2 / 100;
//        v3.setLayoutParams(viewParams);

        LinearLayout.LayoutParams spinnerParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        spinnerParams.width = width * 95 / 100;
        spinnerParams.height = height * 6 / 100;
        spinnerParams.topMargin = (int) (height * 1.25 / 100);
        spinnerParams.leftMargin = width * 2 / 100;
        spinnerParams.rightMargin = width * 2 / 100;
        spinnerParams.bottomMargin = (int) (height * 0.25 / 100);
        groupSpinner.setLayoutParams(spinnerParams);


        LinearLayout.LayoutParams textParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        textParams.width = width * 45 / 100;
        textParams.height = height * 5 / 100;
        textParams.topMargin = (int) (height * 0.25 / 100);
        textParams.leftMargin = width * 2 / 100;
        textParams.rightMargin = width * 2 / 100;
        fromdate.setLayoutParams(textParams);
        todate.setLayoutParams(textParams);
        fromdate.setGravity(Gravity.CENTER);
        todate.setGravity(Gravity.CENTER);

        LinearLayout.LayoutParams textValueParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        textValueParams.width = width * 45 / 100;
        textValueParams.height = height * 5 / 100;
        textValueParams.leftMargin = width * 2 / 100;
        textValueParams.rightMargin = width * 2 / 100;
        textValueParams.bottomMargin = (int) (height * 0.25 / 100);
        fromdatevalue.setLayoutParams(textValueParams);
        todatevalue.setLayoutParams(textValueParams);
        fromdatevalue.setGravity(Gravity.CENTER);
        todatevalue.setGravity(Gravity.CENTER);


        LinearLayout.LayoutParams lineParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        lineParams.width = width * 98 / 100;
        lineParams.height = LinearLayout.LayoutParams.WRAP_CONTENT;
        lineParams.topMargin = (int) (height * 0.25 / 100);
        lineParams.bottomMargin = height * 1 / 100;
        lineParams.leftMargin = width * 1 / 100;
        lineParams.rightMargin = width * 1 / 100;
        lv.setLayoutParams(lineParams);


        LinearLayout.LayoutParams ViewParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        ViewParams.width = width * 1 / 100;
        ViewParams.height = LinearLayout.LayoutParams.MATCH_PARENT;
        v1.setLayoutParams(ViewParams);
        v2.setLayoutParams(ViewParams);


//        LinearLayout.LayoutParams headTxtParams1 = new LinearLayout.LayoutParams(
//                LinearLayout.LayoutParams.WRAP_CONTENT,
//                LinearLayout.LayoutParams.WRAP_CONTENT);
//        headTxtParams1.width = width * 76 / 100;
//        headTxtParams1.height = height * 8 / 100;
//        headTxtParams1.setMargins(width * 4 / 100, width * 2 / 100, 1, width * 1 / 100);
////        headTxtParams1.setMargins();
////        $TxtTitle.setLayoutParams(headTxtParams);
////        $TxtTitle.setPadding(width * 2 / 100, 0, 0, 0);
////        $TxtTitle.setGravity(Gravity.CENTER);
//        mEdtSearch.setLayoutParams(headTxtParams1);
////        mEdtSearch.setm
//        mEdtSearch.setPadding(width * 2 / 100, 0, width * 4 / 100, 0);
//        mEdtSearch.setGravity(Gravity.CENTER | Gravity.CENTER);


        if (width >= 600) {
            fromdate.setTextSize(16);
            todate.setTextSize(16);
            fromdatevalue.setTextSize(15);
            todatevalue.setTextSize(15);
            mTxtNoRecord.setTextSize(18);
            mHeadTitle.setTextSize(18);
            //mEdtSearch.setTextSize(18);
        } else if (width > 501 && width < 600) {
            fromdate.setTextSize(15);
            todate.setTextSize(15);
            fromdatevalue.setTextSize(14);
            todatevalue.setTextSize(14);
            mTxtNoRecord.setTextSize(17);
            mHeadTitle.setTextSize(17);
            //  mEdtSearch.setTextSize(17);
        } else if (width > 260 && width < 500) {
            fromdate.setTextSize(14);
            todate.setTextSize(14);
            fromdatevalue.setTextSize(13);
            todatevalue.setTextSize(13);
            mTxtNoRecord.setTextSize(16);
            mHeadTitle.setTextSize(16);
            //  mEdtSearch.setTextSize(16);
        } else if (width <= 260) {
            fromdate.setTextSize(13);
            todate.setTextSize(13);
            fromdatevalue.setTextSize(12);
            todatevalue.setTextSize(12);
            mTxtNoRecord.setTextSize(15);
            mHeadTitle.setTextSize(15);
            //  mEdtSearch.setTextSize(15);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.txt_start_date_value:
                Calendar calNow = Calendar.getInstance();

                if (mStrFromDate != null) {
                    String[] dateArr = mStrFromDate.split("-");
                    int yearValue = 0, monthValue = 0, dateValue = 0;
                    if (dateArr.length > 0) {


                        yearValue = Integer.valueOf(dateArr[0]);
                        monthValue = Integer.valueOf(dateArr[1]);
                        dateValue = Integer.valueOf(dateArr[2]);

                        if (monthValue > 0) {
                            monthValue = monthValue - 1;
                        }

                    }
                    calNow.set(yearValue, monthValue, dateValue);
                }

                new DatePickerDialog(TollgateReport.this, new DatePickerDialog.OnDateSetListener() {
                    // when dialog box is closed, below method will be called.
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int day) {
                        mTxtFromDate.setText(cons.getDateFormat(cons.getCalendarDate(year, month, day, 0, 0)));
                    }
                }, calNow.get(Calendar.YEAR), calNow.get(Calendar.MONTH), calNow.get(Calendar.DAY_OF_MONTH)).show();
                break;
            case R.id.txt_end_date_value:
                Calendar calNow1 = Calendar.getInstance();

                if (mStrToDate != null) {
                    String[] dateArr = mStrToDate.split("-");
                    int yearValue = 0, monthValue = 0, dateValue = 0;
                    if (dateArr.length > 0) {


                        yearValue = Integer.valueOf(dateArr[0]);
                        monthValue = Integer.valueOf(dateArr[1]);
                        dateValue = Integer.valueOf(dateArr[2]);

                        if (monthValue > 0) {
                            monthValue = monthValue - 1;
                        }

                    }
                    calNow1.set(yearValue, monthValue, dateValue);
                }

                new DatePickerDialog(TollgateReport.this, new DatePickerDialog.OnDateSetListener() {
                    // when dialog box is closed, below method will be called.
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int day) {
                        mTxtEndDate.setText(cons.getDateFormat(cons.getCalendarDate(year, month, day, 0, 0)));
                    }
                }, calNow1.get(Calendar.YEAR), calNow1.get(Calendar.MONTH), calNow1.get(Calendar.DAY_OF_MONTH)).show();
                break;

            case R.id.txt_start_time_value:
                isFromTime = true;
                //   showDialog(TIME_DIALOG_ID);

                showTimeDialog();
                break;
            case R.id.txt_end_time_value:
                isFromTime = false;
                //   showDialog(TIME_DIALOG_ID);

                showTimeDialog();
                break;

            case R.id.btn_done:
                if (cd.isConnectingToInternet()) {
                   // marker_info_dialog.hide();
//                    id_date_time_picker_layout.setVisibility(View.GONE);
//                    id_map_info_layout.setVisibility(View.GONE); "http://188.166.244.126:9000/
                    bottomlyt.setVisibility(View.GONE);

                   lv.setVisibility(View.VISIBLE);
//
                    adapter = null;
                    adapter = new TableAdapter(TollgateReport.this);
//     String url = Const.API_URL + "/getTripHistorySummary?userId=" + Constant.SELECTED_USER_ID + "&vehicleId=" + Constant.SELECTED_VEHICLE_ID + "&fromDate=" + id_from_date.getText().toString() + "&toDate=" + id_to_date.getText().toString();
                    mtollgateData.clear();
                    lv.setAdapter(null);

                    mStrFromDate = mTxtFromDate.getText().toString().trim();
                    mStrToDate = mTxtEndDate.getText().toString().trim();
                    Log.d("fromdate",""+mStrFromDate );
                    Log.d("todate",""+mStrToDate);
//                    mStrFromTime = mTxtFromTime.getText().toString().trim();
//                    mStrToTime = mTxtEndTime.getText().toString().trim();
                    Log.d("fromtime",""+mStrFromTime );
                    Log.d("totime",""+mStrToTime);
//                    String url = Const.API_URL + "mobile/getTripHistorySummary?userId=" + Constant.SELECTED_USER_ID + "&vehicleId=" + Constant.SELECTED_VEHICLE_ID + "&fromDate=" + id_from_date.getText().toString() + "&toDate=" + id_to_date.getText().toString();
                    // String url = Const.API_URL + "mobile/getVehicleHistory4MobileV2?userId=" + Constant.SELECTED_USER_ID + "&vehicleId=" + Constant.SELECTED_VEHICLE_ID + "&fromDate=" + id_from_date.getText().toString() + "&fromTime=" + id_from_time.getText().toString() + "&toDate=" + id_to_date.getText().toString() + "&toTime=" + id_to_time.getText().toString() + "&macid=" + getStdTableValue("macid") + "&appid=" + getStdTableValue("appid");
                    // String par[] = {Const.API_URL + "mobile/getGeoFenceView?vehicleId=" + Constant.SELECTED_VEHICLE_ID + "&userId=" + Constant.SELECTED_USER_ID + "&macid=" + getStdTableValue("macid") + "&appid=" + getStdTableValue("appid")};
                    new getTollgateReport().execute();
                    //  new GetPOIInformation().execute(par);
                } else {
                    Toast internet_toast = Toast.makeText(getApplicationContext(), getResources().getString(R.string.network_connection), Toast.LENGTH_LONG);
                    internet_toast.show();
                }
                break;
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        map.setPadding(1, 1, 1, 150);
        map.getUiSettings().setZoomControlsEnabled(true);
    }


    private class getTollgateReport extends AsyncTask<String, String, String> {
        ProgressDialog progressDialog;

        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub

            String result = null;
            try {
                Log.d("tollgateresu","sucess");
                String string_from_date = mStrFromDate.trim() + " " + mStrFromTime.trim();
                Log.d("fromtrim",""+string_from_date);
                String string_to_date = mStrToDate.trim() + " " + mStrToTime.trim();
                Log.d("totrim",""+string_to_date);
                SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss aa");
                Date d = f.parse(string_from_date);
               long fromMilliSeconds = d.getTime();
              Date d1 = f.parse(string_to_date);
                long toMilliSeconds = d1.getTime();
                Const constaccess = new Const();
                Log.d("from_date",""+mStrFromDate);
                Log.d("to_date",""+mStrToDate);
                Log.d("from_time",""+mStrFromTime);
                Log.d("to_time",""+mStrToTime);
                Log.d("mSelectedGroup",""+mSelectedGroup);
                Log.d("fromMilliSeconds",""+fromMilliSeconds);
                Log.d("toMilliSeconds",""+toMilliSeconds);
                Log.d("mUserId",""+mUserId);

//New API
                result = constaccess
                        .sendGet(Const.API_URL + "/mobile/getTollgateReport?group=" + mSelectedGroup + "&fromTimeUtc=" + fromMilliSeconds + "&toTimeUtc=" + toMilliSeconds + "&userId=" + mUserId, 30000);


            } catch (Exception e) {
                e.printStackTrace();


            }

            return result;
        }

//        @Override
//        protected void onPostExecute(String result) {
//            // TODO Auto-generated method stub
//            super.onPostExecute(result);
//            progressDialog.dismiss();
//            mtollgateData = new ArrayList<>();
//            if (result != null && result.length() > 0) {
//                System.out.println("The kms result is ::::" + result);
//                try {
//                    JSONArray jsonArray = new JSONArray(result.trim());
//                    for (int i=0;i<jsonArray.length();i++){
//                        TollgateDto tollgateDto1 = new TollgateDto();
//                        JSONObject jsonObject = jsonArray.getJSONObject(i);
//                        if (jsonObject.getString("shortName").length()==0 && jsonObject.getString("vehicleId").length()==0){
//                            lv.setVisibility(View.GONE);
//                            mTxtNoRecord.setVisibility(View.VISIBLE);
//                        }
//
//                        if (jsonObject.getString("error").equalsIgnoreCase("Tollgate Report will be sent to the registered email Id as you selected more than 1 day")){
//                            lv.setVisibility(View.GONE);
//                            mTxtNoRecord.setVisibility(View.VISIBLE);
//                            Toast.makeText(TollgateReport.this, "Tollgate Report will be sent to the registered email Id as you selected more than 1 day", Toast.LENGTH_SHORT).show();
//
//                        }
//
//
//                           //Log.d("erroralue",""+jsonObject.getString("error"));
//                            if (jsonObject.getString("error").equalsIgnoreCase("No Data Found") || jsonObject.getString("error").equalsIgnoreCase("Tollgate Report will be sent to the registered email Id as you selected more than 1 day")) {
//                               Log.d("erroralue","success");
//                               lv.setVisibility(View.GONE);
//                               mTxtNoRecord.setVisibility(View.VISIBLE);
//
//
//
//                        } else {
//                                       lv.setVisibility(View.VISIBLE);
//                                       mTxtNoRecord.setVisibility(View.GONE);
//                                       Log.d("erroralue","failuer");
//                                if (jsonObject.has("history")) {
//
//
//                                    if (jsonObject.getJSONArray("history") != null) {
//                                        // JSONArray jsonArray1 = new JSONArray(jsonObject.getString("history"));
//                                        String getJsonARrayAsString = jsonObject.getString("history");
//
//
//
//
//
//                                   JSONArray jsonArray1 = new JSONArray(getJsonARrayAsString);
//                                       // JSONArray jsonArray1 = jsonObject.getJSONArray("history");
//
//
//                                        for (int j = 0; j < jsonArray1.length(); j++) {
//                                            TollgateDto tollgateDto = new TollgateDto();
//
//                                            JSONObject jsonObject1 = jsonArray1.getJSONObject(j);
//                                            if (jsonObject1.has("address")) {
//                                                tollgateDto.setAddress(jsonObject1.getString("address"));
//                                            }
//                                            if (jsonObject1.has("tollgateName")) {
//                                                tollgateDto.setTollgateName(jsonObject1.getString("tollgateName"));
//                                            }
//                                            if (jsonObject1.has("lat")) {
//                                                tollgateDto.setLat(jsonObject1.getDouble("lat"));
//                                            }
//
//                                            if (jsonObject1.has("lng")) {
//                                                tollgateDto.setLng(jsonObject1.getDouble("lng"));
//                                            }
//
//                                            if (jsonObject1.has("date")) {
//                                                tollgateDto.setDate(jsonObject1.getString("date"));
//                                            }
//
//                                            if (jsonObject.has("shortName")) {
//                                                tollgateDto.setShortName(jsonObject.getString("shortName"));
//                                            }
////
//
//
//                                            mtollgateData.add(tollgateDto);
//
//
//                                        }
//
//                                    }
//                                }
//                            }
//                      //  mtollgateData.add(tollgateDto);
//                    }
//
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//
//
//                //getNewMethod(result);
//
////
////                Gson g = new Gson();
////                InputStream is = new ByteArrayInputStream(result.getBytes());
////                Reader reader = new InputStreamReader(is);
////                Type fooType = new TypeToken<List<Tollmodel>>() {
////                }.getType();
////
////                mtollGroupList = g.fromJson(reader, fooType);
////                for (int i = 0; i < mtollGroupList.size(); i++) {
////                    TollgateDto dto = new TollgateDto();
////                    Log.d("shorttnamee",""+mtollGroupList.get(i).getShortName());
////                    musername = mtollGroupList.get(i).getShortName();
////                  if (mtollGroupList.get(i).getHistory() !=null){
////
////                      mData = new ArrayList<TollgateDto>(
////                            Arrays.asList(mtollGroupList.get(i).getHistory()));
//////                      dto.setDate(String.valueOf(mData.get(i).getDate()));
//////                      dto.setAddress(mData.get(i).getAddress());
//////                      dto.setTollgateName(mData.get(i).getTollgateName());
////                      Log.d("mDatasize",""+mData.size());
////                      Log.d("mDatalissst",""+mData);
////                    //  for (int j=0;j<mData.size();j++){
////                          setTableLayoutData();
////                     // }
////
////                  }
////                    //mtollgateData.add(dto);
//////                mData = new ArrayList<TollgateDto>();
//////                Tollgateevnt kmsResponse;
//////
//////                Gson g = new Gson();
//////
//////                kmsResponse = g.fromJson(result.trim(),
//////                        Tollgateevnt.class);
//////                if (kmsResponse.getHistory() != null) {
//////
//////
//////                    mData = new ArrayList<TollgateDto>(
//////                            Arrays.asList(kmsResponse.getHistory()));
////
////
////                }
//
//
//
//
//
//                fromdatevalue.setText(mStrFromDate);
//                todatevalue.setText(mStrToDate);
//                setTableLayoutData();
//
//
//
//
//
////                    JSONArray jsonArray = new JSONArray(result.trim());
////                    for (int i=0;i<jsonArray.length();i++){
////                        JSONObject jsonFuelObject = jsonArray.getJSONObject(i);
////                        TollgateDto tollgateDto = new TollgateDto();
////                        if (jsonFuelObject.has("shortName")){
////                            tollgateDto.setShortName(jsonFuelObject.getString("shortName"));
////                        }
////                        if (jsonFuelObject.has("error")){
////                            tollgateDto.setError(jsonFuelObject.getString("error"));
////                        }
////                        mtollgateData.add(tollgateDto);
////                    }
//            }
//
//        }
@Override
protected void onPostExecute(String result) {
//             TODO Auto-generated method stub
    super.onPostExecute(result);
    progressDialog.dismiss();
    try {
        JSONArray jsonArray=new JSONArray(result.trim());
        JSONObject jsonObject=new JSONObject();
        for(int i=0;i<jsonArray.length();i++){
            jsonObject=jsonArray.getJSONObject(i);
            if(jsonObject.has("error")&& jsonObject.getString("error").equalsIgnoreCase("Tollgate Report will be sent to the registered email Id as you selected more than 1 day")){
                Log.d("tollgate","error1"+jsonObject.getString("error"));
                lv.setVisibility(View.GONE);
                mTxtNoRecord.setVisibility(View.VISIBLE);
                Toast.makeText(TollgateReport.this,getResources().getString(R.string.tollgete_report_sent_email),Toast.LENGTH_LONG).show();
            }
            else if(jsonObject.has("error")&& jsonObject.getString("error").equalsIgnoreCase("No Data Found")){
                Log.d("tollgate","error2"+jsonObject.getString("error"));
                lv.setVisibility(View.GONE);
                mTxtNoRecord.setVisibility(View.VISIBLE);
            }
            else{
                if(jsonObject.has("history")&& jsonObject.getString("history").equals("null")){
                    Log.d("tollgate","null history");
                }
                else{
                    mTxtNoRecord.setVisibility(View.GONE);
                    String shrtName=jsonObject.getString("shortName");
                    Log.d("tollgate","history");
                    String history=jsonObject.getString("history");
                    System.out.println("tollgate "+history);
                    JSONArray jsonArray1=new JSONArray(history.trim());

                    for(int j=0;j<jsonArray1.length();j++){
                        Log.d("tollgate","print "+j);
                        JSONObject jsonObject1=jsonArray1.getJSONObject(j);
                        Log.d("tollgate","print 1 "+j);
                        Log.d("history detail","tollgateName "+jsonObject1.getString("tollgateName")+" date"+jsonObject1.getString("date"));
                        TollgateDto tollgateDtos=new TollgateDto();
                        tollgateDtos.setAddress(jsonObject1.getString("address"));
                        tollgateDtos.setTollgateName(jsonObject1.getString("tollgateName"));
                        tollgateDtos.setDate(jsonObject1.getString("date"));
                        tollgateDtos.setLat(jsonObject1.getDouble("lat"));
                        tollgateDtos.setLng(jsonObject1.getDouble("lng"));
                        tollgateDtos.setShortName(shrtName);
                        mtollgateData.add(tollgateDtos);
                    }
                    Log.d("tollgate","history length"+mtollgateData.size());
                }
            }
        }

    } catch (JSONException e) {
        e.printStackTrace();
    }
    fromdatevalue.setText(mStrFromDate);
    todatevalue.setText(mStrToDate);
    setTableLayoutData();
}
        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            progressDialog = new ProgressDialog(TollgateReport.this,
                    AlertDialog.THEME_HOLO_LIGHT);
            progressDialog.setMessage(getResources().getString(R.string.progress_dialog));
            progressDialog.setProgressDrawable(new ColorDrawable(
                    Color.BLUE));
            progressDialog.setCancelable(true);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.show();
        }

    }

    private void getNewMethod(String result) {

        try {
            JSONArray jsonArray=new JSONArray(result.trim());

            for(int i=0;i<jsonArray.length();i++){

                JSONObject jsonObject=jsonArray.getJSONObject(i);
                String shortName=jsonObject.getString("shortName");
                String historyString=jsonObject.getString("history");

                JSONArray jsonArray1=new JSONArray(historyString);
                for(int j=0;j<jsonArray1.length();j++){


                    JSONObject jsonObject1=jsonArray1.getJSONObject(j);



                }


            }

        } catch (JSONException e) {
            e.printStackTrace();
        }


    }


    public void setTableLayoutData() {

//        adapter.setHeader(mHeader);
//        ListView listView = (ListView) findViewById(R.id.executive_report_listView1);
        if (mtollgateData.size() > 0) {
            if (isHeaderPresent) {

            } else {

                lv.addHeaderView(adapter.getHeaderView(lv));
                isHeaderPresent = true;
            }
            // for (int i=0;i< mData.size();i++){
            lv.setVisibility(View.VISIBLE);
            lv.setAdapter(adapter);
            adapter.setData(mtollgateData);
            adapter.notifyDataSetChanged();
        }

    }

    public class TableAdapter extends BaseAdapter {

        // private static final String TAG = "TableAdapter";

        private Context mContext;

        private String[] mHeader;

        private boolean ischecked = false;
         private String lastname = "",firstname= "";

        private int mCurrentScroll;

        private int[] mColResources = {R.id.vehicle_name_textView,R.id.date_time_textView, R.id.tollname_textView,
                R.id.address_textView, R.id.gmap_count_textView};

        public TableAdapter(Context context) {
            super();
            mContext = context;
        }

        @Override
        public int getCount() {
            return mtollgateData != null ? mtollgateData.size() : 0;
        }

        @Override
        public Object getItem(int position) {
            return mtollgateData.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            HorizontalScrollView view = null;
            TextView col1=null,col2=null,col3=null,col4=null,col5=null;

            if (convertView == null) {
                LayoutInflater inflater = (LayoutInflater) mContext
                        .getSystemService(LAYOUT_INFLATER_SERVICE);
                view = (HorizontalScrollView) inflater.inflate(
                        R.layout.tollgate_list_item_table_row, parent, false);

//                view.setOnScrollChangeListener(new OnScrollListener() {
//                    @Override
//                    public void onScrollChanged(View view, int scrollX) {
//
//                    }
//                });


                view.setOnScrollListener(new HorizontalScrollView.OnScrollListener() {

                    @Override
                    public void onScrollChanged(View scrollView, int scrollX) {

                        mCurrentScroll = scrollX;
                        ListView listView = (ListView) scrollView.getParent();
                        if (listView == null)
                            return;

                        for (int i = 0; i < listView.getChildCount(); i++) {
                            View child = listView.getChildAt(i);
                            if (child instanceof HorizontalScrollView
                                    && child != scrollView) {
                                HorizontalScrollView scrollView2 = (HorizontalScrollView) child;
                                if (scrollView2.getScrollX() != mCurrentScroll) {
                                    scrollView2.setScrollX(mCurrentScroll);
                                }
                            }
                        }
                    }
                });

            } else {
                view = (HorizontalScrollView) convertView;
            }

            view.setScrollX(mCurrentScroll);

//            if (position % 2 == 0) {
//                view.setBackgroundColor(Color.WHITE);
//            } else {
//                view.setBackgroundColor(Color.LTGRAY);
//            }
           // for (int i=0;i<mData.size();i++){
                final TollgateDto data = mtollgateData.get(position);
            Log.d("tollsize",""+data.getShortName().length());

            firstname = data.getShortName();
            if(lastname.equals("")){
                lastname = firstname;
            }
            if ((firstname.equals(lastname)  && !ischecked ) ){
                view.setBackgroundColor(Color.LTGRAY);
            } else {
                if(!ischecked){
                    ischecked = true;
                    view.setBackgroundColor(Color.WHITE);
                }else if(!firstname.equals(lastname)){
                    ischecked = false;
                    view.setBackgroundColor(Color.LTGRAY);
                }else{
                    view.setBackgroundColor(Color.WHITE);
                }
            }
                 col1 = (TextView) view.findViewById(mColResources[0]);
                if (data.getShortName() !=null){
                    Log.d("musername",""+data.getShortName());

                    col1.setText(data.getShortName());
                }
                Log.d("tollname",""+data.getTollgateName());
                 col2 = (TextView) view.findViewById(mColResources[1]);
                col2.setText(String.valueOf(data.getDate()));
                 col3 = (TextView) view.findViewById(mColResources[2]);
                col3.setText(data.getTollgateName());
                 col4 = (TextView) view.findViewById(mColResources[3]);
                col4.setText(data.getAddress());
                 col5 = (TextView) view.findViewById(mColResources[4]);
                SpannableString content = new SpannableString("Link");
                content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
                col5.setText(content);
                lastname = firstname;
         //   }


            Log.d("mDataaaa",""+mtollgateData);
            Log.d("mDatasize1",""+mtollgateData.size());


//            TextView col1 = (TextView) view.findViewById(mColResources[0]);
//            if (musername !=null){
//                Log.d("musername",""+musername);
//
//                col1.setText(musername);
//            }
//            Log.d("tollname",""+data.getTollgateName());
//            TextView col2 = (TextView) view.findViewById(mColResources[1]);
//            col2.setText(String.valueOf(data.getDate()));
//            TextView col3 = (TextView) view.findViewById(mColResources[2]);
//            col3.setText(data.getTollgateName());
//            TextView col4 = (TextView) view.findViewById(mColResources[3]);
//            col4.setText(data.getAddress());
//            TextView col5 = (TextView) view.findViewById(mColResources[4]);
//            SpannableString content = new SpannableString("Link");
//            content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
//            col5.setText(content);


            col5.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mLatitude = data.getLat();
                    mLongitude = data.getLng();
                    mFuelDataMapLayout.setVisibility(View.VISIBLE);
                    lv.setVisibility(View.GONE);
                    if (isOsmEnabled) {
                        setupOsmMap();
                    } else {
                        setupGoogleMap();
                    }
                }
            });




            LinearLayout.LayoutParams vehicleNameParams = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT);
            vehicleNameParams.width = width * 37 / 100;
            vehicleNameParams.height = height * 12 / 100;
            col1.setLayoutParams(vehicleNameParams);
            col1.setPadding(width * 2 / 100, 0, 0, 0);
            col1.setGravity(Gravity.CENTER | Gravity.LEFT);

            col3.setLayoutParams(vehicleNameParams);
            col3.setPadding(width * 2 / 100, 0, 0, 0);
            col3.setGravity(Gravity.CENTER | Gravity.LEFT);


            col4.setLayoutParams(vehicleNameParams);
            col4.setPadding(width * 2 / 100, 0, 0, 0);
            col4.setGravity(Gravity.CENTER | Gravity.LEFT);


            LinearLayout.LayoutParams text2Params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT);
            text2Params.width = width * 25 / 100;
            text2Params.height = height * 7 / 100;
            col2.setLayoutParams(text2Params);
            col2.setPadding(width * 2 / 100, 0, 0, 0);
            col2.setGravity(Gravity.CENTER | Gravity.LEFT);



            col5.setLayoutParams(text2Params);
            col5.setPadding(width * 2 / 100, 0, 0, 0);
            col5.setGravity(Gravity.CENTER | Gravity.LEFT);



//            }

            return view;
        }



        public View getHeaderView(ViewGroup parent) {

            LayoutInflater inflater = (LayoutInflater) mContext
                    .getSystemService(LAYOUT_INFLATER_SERVICE);
            HorizontalScrollView view = (HorizontalScrollView) inflater
                    .inflate(R.layout.tollgate_list_item_table_header, parent, false);

//            for (int i = 0; i < mColResources.length; i++) {
//                TextView col = (TextView) view.findViewById(mColResources[i]);
//                col.setText(mHeader[i]);
//            }


            view.setOnScrollListener(new HorizontalScrollView.OnScrollListener() {

                @Override
                public void onScrollChanged(View scrollView, int scrollX) {

                    mCurrentScroll = scrollX;
                    ListView listView = (ListView) scrollView.getParent();
                    if (listView == null)
                        return;

                    for (int i = 0; i < listView.getChildCount(); i++) {
                        View child = listView.getChildAt(i);
                        if (child instanceof HorizontalScrollView
                                && child != scrollView) {
                            HorizontalScrollView scrollView2 = (HorizontalScrollView) child;
                            if (scrollView2.getScrollX() != mCurrentScroll) {
                                scrollView2.setScrollX(mCurrentScroll);
                            }
                        }
                    }
                }
            });


            TextView col1 = (TextView) view.findViewById(R.id.vehicle_name_textView_header);
            TextView col2 = (TextView) view.findViewById(R.id.date_time_textView_header);
            TextView col3 = (TextView) view.findViewById(R.id.tollgatename_textView_header);
            TextView col4 = (TextView) view.findViewById(R.id.address_textView_header);
            TextView col5 = (TextView) view.findViewById(R.id.gmap_textView_header);



            LinearLayout.LayoutParams vehicleNameParams = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT);
            vehicleNameParams.width = width * 35 / 100;
            vehicleNameParams.height = height * 7 / 100;
            col1.setLayoutParams(vehicleNameParams);
            col1.setPadding(width * 2 / 100, 0, 0, 0);
            col1.setGravity(Gravity.CENTER | Gravity.LEFT);

            col3.setLayoutParams(vehicleNameParams);
            col3.setPadding(width * 2 / 100, 0, 0, 0);
            col3.setGravity(Gravity.CENTER | Gravity.LEFT);

            col4.setLayoutParams(vehicleNameParams);
            col4.setPadding(width * 2 / 100, 0, 0, 0);
            col4.setGravity(Gravity.CENTER | Gravity.LEFT);


            LinearLayout.LayoutParams text2Params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT);
            text2Params.width = width * 25 / 100;
            text2Params.height = height * 7 / 100;
            col2.setLayoutParams(text2Params);
            col2.setPadding(width * 2 / 100, 0, 0, 0);
            col2.setGravity(Gravity.CENTER | Gravity.LEFT);


            col5.setLayoutParams(text2Params);
            col5.setPadding(width * 2 / 100, 0, 0, 0);
            col5.setGravity(Gravity.CENTER | Gravity.LEFT);

            return view;
        }

//        public void setHeader(String[] header) {
//            mHeader = header;
//        }

        public void setData(List<TollgateDto> data) {
            mtollgateData = data;
            notifyDataSetChanged();
        }

    }
    public void setupOsmMap() {

//        if (map != null) {
//            map.clear();
//        }

        if (mapview != null) {
            InfoWindow.closeAllInfoWindowsOn(mapview);
            mapview.getOverlays().clear();
            mapview.invalidate();
            mapview.getOverlays().add(Constant.getTilesOverlay(getApplicationContext()));
        }



        View markerView = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.marker_image_view, null);
        ImageView mImgMarkerIcon = (ImageView) markerView.findViewById(R.id.img_marker_icon);
        mImgMarkerIcon.setImageResource(R.drawable.green_custom_marker_icon);
//        animateMarker.setIcon(createDrawableFromViewNew(AcReport.this, markerView));
        org.osmdroid.views.overlay.Marker osmMarker = new org.osmdroid.views.overlay.Marker(mapview);
        osmMarker.setPosition(new GeoPoint(mLatitude, mLongitude));
        osmMarker.setAnchor(org.osmdroid.views.overlay.Marker.ANCHOR_CENTER, org.osmdroid.views.overlay.Marker.ANCHOR_BOTTOM);
//        osmMarker.setTitle(Constant.SELECTED_VEHICLE_SHORT_NAME);
        osmMarker.setIcon(Constant.createDrawableFromViewNew(TollgateReport.this, markerView));
//        InfoWindow infoWindow = new FuelFillNewReport().MyInfoWindow(R.layout.map_info_txt_layout, mapview, addressValue);
//        osmMarker.setInfoWindow(infoWindow);
//        osmMarker.showInfoWindow();

        final GeoPoint newPos = new GeoPoint(mLatitude, mLongitude);

        new Handler(Looper.getMainLooper()).post(
                new Runnable() {
                    public void run() {
                        mapview.getController().setCenter(newPos);
                    }
                }
        );

        mapview.getOverlays().add(osmMarker);
        mapview.invalidate();


//        Marker marker = map.addMarker(new MarkerOptions()
//                .position(new LatLng(mLatitude, mLongitude))
//                .title(mSelectedMsg)
//                        // .snippet("Snippet")
//                .icon(BitmapDescriptorFactory
//                        .fromResource(R.drawable.grey_custom_marker_icon)));
//
//        marker.showInfoWindow();


    }

    public void setupGoogleMap() {

        if (map != null) {
            map.clear();
        }

        View marker = null;

        marker = ((LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.custommarker, null);
        ImageView id_custom_marker_icon = (ImageView) marker.findViewById(R.id.id_custom_marker_icon);
        id_custom_marker_icon.setImageResource(R.drawable.grey_custom_marker_icon);
        ImageView id_vehicle_in_marker = (ImageView) marker.findViewById(R.id.id_vehicle_in_marker);
        id_vehicle_in_marker.setVisibility(View.GONE);





//        map.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
//
//            // Use default InfoWindow frame
//            @Override
//            public View getInfoWindow(Marker arg0) {
//                return null;
//            }

        // Defines the contents of the InfoWindow
        //  @Override
//            public View getInfoContents(Marker arg0) {
//
//                // Getting view from the layout file info_window_layout
//              //  View v = getLayoutInflater().inflate(R.layout.map_info_txt_layout, null);
//
//                // Getting the position from the marker
//                LatLng latLng = arg0.getPosition();
//
////                DisplayMetrics displayMetrics = new DisplayMetrics();
////                getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
////                height = displayMetrics.heightPixels;
////                width = displayMetrics.widthPixels;
////                LinearLayout layout = (LinearLayout) v.findViewById(R.id.map_info_layout);
////                LinearLayout.LayoutParams backImageParams = new LinearLayout.LayoutParams(
////                        LinearLayout.LayoutParams.WRAP_CONTENT,
////                        LinearLayout.LayoutParams.WRAP_CONTENT);
////                backImageParams.width = width * 80 / 100;
////                //backImageParams.height = height * 10 / 100;
////                backImageParams.gravity = Gravity.CENTER;
////                layout.setLayoutParams(backImageParams);
//
////                // Getting reference to the TextView to set latitude
////                TextView txtContent = (TextView) v.findViewById(R.id.tv_txt_content);
////                txtContent.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
//
////                new GetAddressTask(txtContent).execute(mLatitude, mLongitude, 1.0);
//
//
//                StringBuffer addr = new StringBuffer();
//                Geocoder geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());
//                List<Address> addresses = null;
//                try {
//                    addresses = geocoder.getFromLocation(mLatitude, mLongitude, 1);
//                } catch (IOException e1) {
//                    e1.printStackTrace();
//                } catch (IllegalArgumentException e2) {
//                    // Error message to post in the log
////                        String errorString = "Illegal arguments " +
////                                Double.toString(params[0]) +
////                                " , " +
////                                Double.toString(params[1]) +
////                                " passed to address service";
////                        e2.printStackTrace();
////                        return errorString;
//                } catch (NullPointerException np) {
//                    // TODO Auto-generated catch block
//                    np.printStackTrace();
//                }
//                // If the reverse geocode returned an address
//                if (addresses != null && addresses.size() > 0) {
//                    // Get the first address
//                    Address address = addresses.get(0);
//                    /*
//                     * Format the first line of address (if available),
//                     * city, and country name.
//                     */
//                    String addressText = null;
//
//                    for (int i = 0; i < address.getMaxAddressLineIndex(); i++) {
//                        addressText = address.getAddressLine(i);
//                        addr.append(addressText + ",");
//                    }
//                    // Return the text
//                    // return addr.toString();
//                } else {
//                    addr.append("No address found");
//                }
//
//                //txtContent.setText(Constant.SELECTED_VEHICLE_SHORT_NAME + " \n" + addressValue);
//               // txtContent.setText(Constant.SELECTED_VEHICLE_SHORT_NAME);
//
//               // return v;
//
//            }
        //});


        MarkerOptions markerOption = new MarkerOptions().position(new LatLng(mLatitude, mLongitude));
        markerOption.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE));
        Marker currentMarker = map.addMarker(markerOption);
        currentMarker.showInfoWindow();
        CameraPosition INIT = new CameraPosition.Builder().target(new LatLng(mLatitude, mLongitude)).zoom(vehicle_zoom_level).build();
        map.animateCamera(CameraUpdateFactory.newCameraPosition(INIT));

    }
    @Override
    protected void onResume() {
        super.onResume();
        setBottomLayoutData();
    }
    private void setBottomLayoutData() {
        mTxtFromDate.setText(mStrFromDate);
        mTxtEndDate.setText(mStrToDate);
        mTxtFromTime.setText(mStartTimeValue);
        mTxtEndTime.setText(mEndTimeValue);

    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(TollgateReport.this, VehicleListActivity.class));
        finish();
    }
}
