package com.vamosys.vamos;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.provider.Settings;

import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.appcompat.app.AlertDialog;

import android.util.Log;
import android.widget.Toast;

//import com.vamosys.services.RegistrationService;
import com.google.android.play.core.appupdate.AppUpdateInfo;
import com.google.android.play.core.appupdate.AppUpdateManager;
import com.google.android.play.core.appupdate.AppUpdateManagerFactory;
import com.google.android.play.core.install.model.AppUpdateType;
import com.google.android.play.core.install.model.UpdateAvailability;
import com.google.android.play.core.tasks.OnFailureListener;
import com.google.android.play.core.tasks.OnSuccessListener;
import com.google.android.play.core.tasks.Task;
import com.vamosys.services.MyFirebaseMessagingService;
import com.vamosys.utils.Constant;

import static com.google.android.play.core.install.model.AppUpdateType.IMMEDIATE;

public class LandingPage extends Activity {
    DBHelper dbhelper;
    Context context = this;
    SharedPreferences sp;
    AppUpdateManager appUpdateManager;
    private int REQUEST_CODE_UPDATE = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_landing_page);
        gcmSetup();
//        dbSetup();
        Constant.mIsFirstTime = true;

        sp = PreferenceManager.getDefaultSharedPreferences(context);
        Log.d("test ravi","commit");
        inappupdate();
//        setDefaultGoogleMap();

        // if (!checkStorageP3rm1ss10n(LandingPage.this) && !checkStorageP3rm1ss10n2(LandingPage.this)) {


//        DaoHandler da = new DaoHandler(getApplicationContext(), true);
//        da.checkPushList();

        /** Thread to wait for t seconds*/

//        } else {
//            showP3rmAlert(LandingPage.this);
//        }
    }
    private void inappupdate() {
        appUpdateManager = AppUpdateManagerFactory.create(LandingPage.this);
        Task<AppUpdateInfo> appUpdateInfoTask = appUpdateManager.getAppUpdateInfo();
        appUpdateInfoTask.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(Exception e) {
                System.out.println("Error  msg  "+e.getMessage());
                //Toast.makeText(Splash_Screen.this, " Error  msg  "+e.getMessage(), Toast.LENGTH_SHORT).show();

                splashScreenTimer();
            }
        })
                .addOnSuccessListener(new OnSuccessListener<AppUpdateInfo>() {
                    @Override
                    public void onSuccess(AppUpdateInfo appUpdateInfo) {

                        if (appUpdateInfo.updateAvailability() == UpdateAvailability.UPDATE_AVAILABLE && appUpdateInfo.isUpdateTypeAllowed(AppUpdateType.IMMEDIATE)) {

                            try {
                                appUpdateManager.startUpdateFlowForResult(appUpdateInfo, IMMEDIATE, LandingPage.this, REQUEST_CODE_UPDATE);
                                Log.d("Splash", "Update available");
                            } catch (IntentSender.SendIntentException e) {
                                System.out.println("Error  msg  " + e.getMessage());
                                // Toast.makeText(Splash_Screen.this, "update Error " + e.getMessage(), Toast.LENGTH_SHORT).show();
                                e.printStackTrace();
                                splashScreenTimer();
                            }
                        }else {
                            Log.d("Splash", "No Update available");
                            //System.out.println("Error  msg 3 ");
                            //  Toast.makeText(Splash_Screen.this, " Error  msg  "+appUpdateInfoTask.getException(), Toast.LENGTH_SHORT).show();

                            splashScreenTimer();
                        }
                    }
                });


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            if (requestCode == REQUEST_CODE_UPDATE) {
                if (requestCode != RESULT_OK) {
                    splashScreenTimer();
                }
            }
        } catch (Exception e) {

        }
    }
    private void splashScreenTimer() {
        try {
            Thread splashThread = new Thread() {
                @Override
                public void run() {
                    try {
                        sleep(2000);
                    } catch (InterruptedException e) {
                        // do nothing
                        Toast.makeText(getApplicationContext(), "Something went wrong. Please try again", Toast.LENGTH_SHORT).show();
                    }
                    catch (Exception e) {
                        // do nothing
                        Toast.makeText(getApplicationContext(), "Something went wrong. Please try again", Toast.LENGTH_SHORT).show();
                    }
                    finally {
//                    Cursor login_status_cur = dbhelper.get_std_table_info("login_status");
//                    login_status_cur.moveToFirst();
//                    boolean login_status = login_status_cur.getInt(0) > 0;
//                    login_status_cur.close();
                        boolean login_status = sp.getBoolean("login_status",false);

                        if (login_status) {
                            updatePaymentDue(true);
                            String mFirstActivity = sp.getString("first_activity", "Vehicle");

                            if (mFirstActivity != null && mFirstActivity.length() > 0) {
                                if (mFirstActivity.equalsIgnoreCase("Vehicle") || mFirstActivity.equalsIgnoreCase("DashBoard")) {
                                    Intent start = new Intent(LandingPage.this, VehicleListActivity.class);
                                    startActivity(start);
                                    finish();
                                } else if (mFirstActivity.equalsIgnoreCase("Home")) {
                                    Intent start = new Intent(LandingPage.this, HomeActivity.class);
                                    startActivity(start);
                                    finish();
                                } else {
                                    Intent start = new Intent(LandingPage.this, VehicleListActivity.class);
                                    startActivity(start);
                                    finish();
                                }
                            } else {
                                Intent start = new Intent(LandingPage.this, VehicleListActivity.class);
                                startActivity(start);
                                finish();
                            }

//                            Intent start = new Intent(LandingPage.this, VehicleListActivity.class);
//                            startActivity(start);
                        } else {

                            Intent register = new Intent(LandingPage.this, RegistrationPage.class);
                            startActivity(register);
                            finish();
                        }
                    }
                }
            };
            splashThread.start();
        } catch (Exception e) {

        }
    }
    public void updatePaymentDue(boolean mIsEnabled) {

        try {
            SharedPreferences.Editor editor = sp.edit();

            editor.putBoolean("payment_due_load", mIsEnabled);
            editor.commit();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static boolean checkStorageP3rm1ss10n(final Context context) {
        return ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED;
    }


    public static boolean checkStorageP3rm1ss10n2(final Context context) {
        return ActivityCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED;
    }


    public static void showP3rmAlert(final Activity myActivity) {
        AlertDialog.Builder builder = new AlertDialog.Builder(myActivity);
        builder.setTitle("Enable permisson");
//        builder.setMessage(getString(R.string.app_p3rm_dialog_message));
        builder.setMessage("We do not want disturb you while accessing all the features in your " + myActivity.getString((R.string.app_name)) + ".For better use enable all the permissons in the application permission settings.Do you want to continue?");

        String positiveText = myActivity.getString(android.R.string.ok);
        builder.setPositiveButton(positiveText,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // positive button logic
                        dialog.dismiss();
                        Intent intent = new Intent();
                        intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                        Uri uri = Uri.fromParts("package", myActivity.getApplicationContext().getPackageName(), null);
                        intent.setData(uri);
                        myActivity.getApplicationContext().startActivity(intent);

                    }
                });

        String negativeText = myActivity.getString(android.R.string.cancel);
        builder.setNegativeButton(negativeText,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // negative button logic
                        dialog.dismiss();
                        System.exit(0);
                    }
                });

        AlertDialog dialog = builder.create();
        // display dialog
        dialog.show();

    }


    void dbSetup() {
        dbhelper = new DBHelper(context);
        try {
            dbhelper.createDataBase();
            dbhelper.openDataBase();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void gcmSetup() {
        try {
            Constant.is_logged_in = false;
            Constant.gcm_count = 1;
//            Intent intent = new Intent(this, RegistrationService.class);
//            startService(intent);
            startService(new Intent(LandingPage.this, MyFirebaseMessagingService.class));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

     /*delete once osm ready*/

    private void setDefaultGoogleMap() {
        SharedPreferences.Editor editor = sp.edit();
        editor.putString("enabled_map", getResources().getString(R.string.google));
        editor.commit();
    }
}
