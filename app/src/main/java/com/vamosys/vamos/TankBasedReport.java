package com.vamosys.vamos;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.vamosys.adapter.FuelSummaryAdapter;
import com.vamosys.adapter.TankSummaryAdapter;
import com.vamosys.interfaces.ApiClient;
import com.vamosys.interfaces.ApiInterface;
import com.vamosys.model.FuelSumReportDto;
import com.vamosys.utils.ConnectionDetector;
import com.vamosys.utils.Constant;
import com.vamosys.utils.TransparentProgressDialog;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TankBasedReport extends AppCompatActivity implements View.OnClickListener {
    private ImageView img_back,img_calendar;
    private Spinner spinner;
    List<String> mGroupSpinnerList = new ArrayList<String>();
    List<String> mGroupList = new ArrayList<String>();
    SharedPreferences sp;
    ConnectionDetector cd;
    Const cons;
    View bottomSheet;
    BottomSheetBehavior behavior;
    boolean bottomSheetEnabled = false;
    TextView mTxtFromDate;
    String mStartDate;
    private int year, month, day;
    private boolean isFromDate;
    private TextView mTxtNoRecord, mTxtfill,mTxtconsumption,mTxttheft,mTextdist,mTxtdate,mTxtnorcrd;
    Button mButton;
    List<FuelSumReportDto> fuelDataList=new ArrayList<>();
    String mSelectedGroup;
    float totconsum=0,totalfill=0,totaltheft=0,totaldisance=0;
    static RecyclerView lv;
    private LinearLayout card_layout;
    private TankSummaryAdapter adapter;
    private HorizontalScrollView horizontalScrollView;
    TransparentProgressDialog transparentProgressDialog;
    ProgressBar progress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tank_based_report);
        try {
            cons = new Const();
            init();
        }
        catch (Exception e){
            Log.d("exception ",""+e.getMessage());
        }

    }

    private void init() {
        sp = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        cd = new ConnectionDetector(getApplicationContext());
        img_back=findViewById(R.id.fuel_summary_view_Back);
        img_back.setOnClickListener(this);
        img_calendar=findViewById(R.id.fuel_summary_change_date);
        img_calendar.setOnClickListener(this);
        spinner = findViewById(R.id.spinner);
        mButton = findViewById(R.id.btn_done);
        mButton.setOnClickListener(this);
        horizontalScrollView=findViewById(R.id.horizontalView);
        mTxtNoRecord=findViewById(R.id.temperature_report_no_record_txt);
        lv =  findViewById(R.id.tstoppage_report_listView1);
        card_layout =(LinearLayout)findViewById(R.id.card_layout);
        mTxtconsumption = (TextView)findViewById(R.id.total_consumption);
        mTxtfill = (TextView)findViewById(R.id.total_fill);
        mTxttheft = (TextView)findViewById(R.id.total_theft);
        mTextdist = (TextView)findViewById(R.id.total_distance);
        progress=findViewById(R.id.progress);
        queryUserDB();
        progress.setVisibility(View.VISIBLE);
//        mTxtNoRecord.setText("Loading.......");
//        mTxtNoRecord.setVisibility(View.VISIBLE);

        mStartDate = Const.getTripYesterdayDate2();
        Log.d("mStartDate",""+mStartDate);
        long timeInMillis = System.currentTimeMillis();
        Calendar cal1 = Calendar.getInstance();
        cal1.setTimeInMillis(timeInMillis);

        Calendar cal = Calendar.getInstance();
//        Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);

        bottomSheet = findViewById(R.id.bottom_sheet);
        behavior = BottomSheetBehavior.from(bottomSheet);
        behavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
            }
        });
        mTxtFromDate = (TextView)findViewById(R.id.txt_start_date_value);
        mTxtdate=findViewById(R.id.total_date);
        setBottomLayoutData();
        mTxtFromDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isFromDate = true;
                showDateDialog();
            }
        });
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mSelectedGroup = mGroupList.get(position);
                saveSelectedGroup(mGroupSpinnerList.get(position));
                String url = null;

//                url = Const.API_URL + "/mobile/getConsolidatedFuelReport?userId=" + Constant.SELECTED_USER_ID + "&groupName=" + mSelectedGroup + "&date=" + mStartDate;

                if (cd.isConnectingToInternet()) {
                    getTankData();
                } else {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.network_connection),
                            Toast.LENGTH_SHORT).show();
                }
                //url = Const.API_URL + "/mobile/getConsolidatedFuelReport?userId=" + Constant.SELECTED_USER_ID + "&groupName=" + mSelectedGroup + "&date=" + mStartDate;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }
    private void saveSelectedGroup(String mGroupName) {
        try {
            SharedPreferences.Editor editor = sp.edit();
            editor.putString("selected_group", mGroupName);
            editor.commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    private void getTankData() {
        try {
//            Log.d("getTankData","progressDialog "+progressDialog.isShowing());
//            progressDialog.show();
            fuelDataList.clear();
            ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<List<FuelSumReportDto>> call = apiInterface.getConsolidatedFuelReport(Constant.SELECTED_USER_ID, mSelectedGroup, mStartDate.trim());
            Log.d("res", call.request().url().toString());
            call.enqueue(new Callback<List<FuelSumReportDto>>() {
                @Override
                public void onResponse(Call<List<FuelSumReportDto>> call, Response<List<FuelSumReportDto>> response) {
                    try {
                        fuelDataList = response.body();
                        if(fuelDataList!=null&&fuelDataList.size()>0) {
//                        Log.d("res", "size " + fuelDataList.size());
                            setTotalFuel();
                            setTableLayout();
                            progress.setVisibility(View.GONE);
                            mTxtNoRecord.setVisibility(View.GONE);
                        }
                    }
                    catch (Exception e){
                        Log.d("res", "fail "+e.getMessage());
//                        setTableLayout();
                        progress.setVisibility(View.GONE);
                        mTxtNoRecord.setText(""+getResources().getString(R.string.no_record));
                        mTxtNoRecord.setVisibility(View.VISIBLE);
                        horizontalScrollView.setVisibility(View.GONE);
                    }
                }
                @Override
                public void onFailure(Call<List<FuelSumReportDto>> call, Throwable t) {
                    Log.d("res", "fail " + t.getMessage());
//                    setTableLayout();
                    progress.setVisibility(View.GONE);
                    mTxtNoRecord.setText(getResources().getString(R.string.no_record));
                    mTxtNoRecord.setVisibility(View.VISIBLE);
                    horizontalScrollView.setVisibility(View.GONE);
                }
            });

        }
        catch (Exception e){
            Log.d("res", "error "+e.getMessage() );
            setTableLayout();
            progress.setVisibility(View.GONE);
            mTxtNoRecord.setText(getResources().getString(R.string.no_record));
            mTxtNoRecord.setVisibility(View.VISIBLE);
            horizontalScrollView.setVisibility(View.GONE);
        }
    }
    private void setTotalFuel() {
//        Log.d("res","tot1 "+totconsum+" "+totalfill+" "+totaltheft+" "+totaldisance);
        totaldisance=0;
        totalfill=0;
        totaltheft=0;
        totconsum=0;
        try {
            for (int i = 0; i <= fuelDataList.size()-1; i++) {
                FuelSumReportDto f=fuelDataList.get(i);
                if(f.getTankData().size()>0) {
                    Log.d("res","tot2 "+f.getTankData().size());
                    for (int j = 0; j < f.getTankData().size(); j++) {
                        FuelSumReportDto.Fuelsensor f1 = f.getTankData().get(j);
                        Log.d("res","tot44 "+f1.getFuelConsumption()+" "+f1.getFuelFilling()+" "+f1.getFuelTheft());
                        if (f1.getFuelConsumption() != null && f1.getFuelConsumption().length() > 0) {
                            totconsum = totconsum + Float.parseFloat(f1.getFuelConsumption());
                        }
                        if (f1.getFuelFilling() != null && f1.getFuelFilling().length() > 0) {
                            totalfill = totalfill + Float.parseFloat(f1.getFuelFilling());
                        }
                        if (f1.getFuelTheft() != null && f1.getFuelTheft().length() > 0) {
                            totaltheft = totaltheft + Float.parseFloat(f1.getFuelTheft());
                        }
                    }
                }
                else {
                    Log.d("res","tot3 "+f.getSensor());
                    Log.d("res","tot44 "+"vehiclename"+f.getVehicleName()+" "+f.getFuelConsumption()+" "+f.getFuelFilling()+" "+f.getFuelTheft());
                    if (f.getFuelConsumption() != null && f.getFuelConsumption().length() > 0) {
                        totconsum = totconsum + Float.parseFloat(f.getFuelConsumption());
                    }
                    if (f.getFuelFilling() != null && f.getFuelFilling().length() > 0) {
                        totalfill = totalfill + Float.parseFloat(f.getFuelFilling());
                    }
                    if (f.getFuelTheft() != null && f.getFuelTheft().length() > 0) {
                        totaltheft = totaltheft + Float.parseFloat(f.getFuelTheft());
                    }
                }
                if(f.getDist()>0) {
                    totaldisance = totaldisance + Float.parseFloat(""+f.getDist());
                }
            }
            Log.d("res","tot "+totconsum+" "+totalfill+" "+totaltheft+" "+totaldisance);
        }
        catch (Exception e){
            Log.d("res","error1 "+e.getMessage());
        }
    }
    private void setTableLayout() {
        try {
            if (fuelDataList.size() > 0) {
                lv.setVisibility(View.VISIBLE);
                card_layout.setVisibility(View.VISIBLE);
                horizontalScrollView.setVisibility(View.VISIBLE);
                mTxtNoRecord.setVisibility(View.GONE);
                mTxtdate.setText(mStartDate);
                mTxttheft.setText(String.valueOf(new DecimalFormat("##.##").format(totaltheft)) + " ltrs");
                mTxtfill.setText(String.valueOf(new DecimalFormat("##.##").format(totalfill)) + " ltrs");
                mTxtconsumption.setText(String.valueOf(new DecimalFormat("##.##").format(totconsum)) + " ltrs");
                mTextdist.setText(String.valueOf(new DecimalFormat("##.##").format(totaldisance)) + " Kms");
                Log.d("res", "initial adapter");
                adapter = new TankSummaryAdapter(TankBasedReport.this, fuelDataList);
                lv.setAdapter(adapter);
                adapter.notifyDataSetChanged();
            } else {
//                mFuelFillData.clear();
                card_layout.setVisibility(View.GONE);
                lv.setVisibility(View.GONE);
                horizontalScrollView.setVisibility(View.GONE);
                mTxtNoRecord.setText(getResources().getString(R.string.no_record));
                mTxtNoRecord.setVisibility(View.VISIBLE);
                Toast.makeText(getApplicationContext(), ""+getResources().getString(R.string.diff_date_time), Toast.LENGTH_SHORT).show();
            }
        }catch(Exception e){

        }
    }



    private void showDateDialog() {
        if (isFromDate) {
            String[] dateArray = mStartDate.split("-");
            year = Integer.parseInt(dateArray[0].trim());
            month = Integer.parseInt(dateArray[1].trim()) - 1;
            day = Integer.parseInt(dateArray[2].trim());
        }
        /** set date picker as current date */
        DatePickerDialog dialog = new DatePickerDialog(this,
                datePickerListener, year, month, day);
        dialog.getDatePicker().setMaxDate(System.currentTimeMillis());
        dialog.show();
    }
    private DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {

        // when dialog box is closed, below method will be called.
        public void onDateSet(DatePicker view, int selectedYear,
                              int selectedMonth, int selectedDay) {

            year = selectedYear;
            month = selectedMonth;
            day = selectedDay;
            StringBuilder mDate = new StringBuilder().append(year).append(":")
                    .append(getMonthValue(month + 1)).append(":").append(getMonthValue(day))
                    .append("");

            StringBuilder mDate2 = new StringBuilder().append(year).append("-")
                    .append(getMonthValue(month + 1)).append("-").append(getMonthValue(day))
                    .append(" ");
            if (isFromDate) {
                mStartDate = String.valueOf(mDate2);
                //  mStartTimeValue=String.valueOf(mDate2);
                Log.d("fuelDate2",""+mDate2);
                mTxtFromDate.setText(mDate2);
                mTxtdate.setText(mDate2);
            }
        }
    };

    private String getMonthValue(int month) {
        String mMonthValue = String.valueOf(month);
        if (mMonthValue.length() == 1) {
            mMonthValue = "0" + mMonthValue;
        }
        return mMonthValue;
    }
    private void queryUserDB() {
        mGroupList = Constant.mUserGroupList;
        setDropDownData();
    }
    private void setDropDownData() {
        if (mGroupList.size() > 0) {
        } else {
            mGroupList.add("Select");
        }
        for (int i = 0; i < mGroupList.size(); i++) {
            String[] str_msg_data_array = mGroupList.get(i).split(":");
            mGroupSpinnerList.add(str_msg_data_array[0]);
        }


        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(
                TankBasedReport.this, R.layout.spinner_fuel,
                mGroupSpinnerList) {

            public View getView(int position, View convertView, ViewGroup parent) {
                View v = super.getView(position, convertView, parent);

                return v;
            }

            public View getDropDownView(int position, View convertView,
                                        ViewGroup parent) {
                View v = super.getDropDownView(position, convertView, parent);

                return v;
            }
        };
        spinnerArrayAdapter
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(spinnerArrayAdapter);

        String groupName = sp.getString("selected_group", "");
        int spinnerPosition = spinnerArrayAdapter.getPosition(groupName);

//set the default according to value
        spinner.setSelection(spinnerPosition);
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.fuel_summary_view_Back:
                startActivity(new Intent(TankBasedReport.this,VehicleListActivity.class));
                break;
            case R.id.fuel_summary_change_date:
                if (!bottomSheetEnabled) {
                    behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                } else {
                    behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                }
                if (bottomSheetEnabled) {
                    bottomSheetEnabled = false;
                } else {
                    bottomSheetEnabled = true;
                }
                break;
            case R.id.btn_done:
                behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                String url = null;
                if (cd.isConnectingToInternet()) {
                    getTankData();
                } else {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.network_connection),
                            Toast.LENGTH_SHORT).show();
                }
                break;
        }

    }
    private void setBottomLayoutData() {
        mTxtFromDate.setText(mStartDate);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(TankBasedReport.this,VehicleListActivity.class));
        finish();
    }
}