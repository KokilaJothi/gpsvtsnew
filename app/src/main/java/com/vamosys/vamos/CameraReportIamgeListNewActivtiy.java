package com.vamosys.vamos;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import androidx.annotation.NonNull;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.vamosys.adapter.DataAdapter;
import com.vamosys.model.CameraReportDto;
import com.vamosys.utils.ConnectionDetector;
import com.vamosys.utils.Constant;
import com.vamosys.utils.HttpConfig;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class CameraReportIamgeListNewActivtiy extends AppCompatActivity implements View.OnClickListener {
    Toolbar mToolbar;

    ConnectionDetector cd;
    SharedPreferences sp;
    String mFromDate = null, mFromTime, mToTime, mFromDateTxtValue;
    private RecyclerView mRecyclerView;
    List<CameraReportDto> mCameraReportList;
    BottomSheetBehavior behavior;
    boolean bottomSheetEnabled = false;
    // TextView mTxtDate, mTxtFromTime, mTxtToTime;
    Button mBtnDone;
    TextView mTxtFromDate, mTxtEndDate, mTxtFromTime, mTxtEndTime, mTxtSelectedVehicle, mTxtNoRecord;
    String mStartDate, mEndDate, mStartTime, mStartTimeValue, mEndDateValue, mEndTime, mEndTimeValue;
    //    private CameraImageAdapter mAdapter;
    View bottomSheet;
    SimpleDateFormat timeFormat = new SimpleDateFormat(
            "hh:mm:ss aa");

    SimpleDateFormat timeFormatShow = new SimpleDateFormat(
            "hh:mm aa");

    private int year, month, day;
    private int hour;
    private int minute;
//    Spinner spinnerFromHour, spinnerEndHour;

    static final int DATE_DIALOG_FROMID = 1;
    int mFromHourValue = 0, mFromMinuteValue = 0, mToHourValue = 0, mToMinuteValue = 0;
    boolean isFromTime = false, isFromDate = false;
    NumberPicker numberpicker;
    SimpleDateFormat dftime = new SimpleDateFormat("kk");
    List<Integer> mHourList = new ArrayList<Integer>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera_report_iamge_list_new_activtiy);
        initViews();
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mToolbar.setTitle(getResources().getString(R.string.camera_report));
        mToolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_action_back));
        setSupportActionBar(mToolbar);

        cd = new ConnectionDetector(getApplicationContext());

        sp = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        if (sp.getString("ip_adds", "") != null) {
            if (sp.getString("ip_adds", "").trim().length() > 0) {
                Const.API_URL = sp.getString("ip_adds", "");
            }
        }


//        mHourList = formHourValue();
//
//        SimpleDateFormat dfDate = new SimpleDateFormat("yyyyMMdd");
//
//
//        mFromDateTxtValue = Const.getTripDate(String.valueOf(System.currentTimeMillis()));
//        mFromDate = dfDate.format(new Date());
//        mFromTime = dftime.format(new Date(System.currentTimeMillis() - 3600 * 1000));
//        mToTime = dftime.format(new Date());

        mStartDate = Const.getCurrentDate();
//        mEndDate = Const.getTripYesterdayDate2();

//        mStartDate = Const.getTripDate(String.valueOf(System.currentTimeMillis()));
        mEndDate = Const.getTripDate(String.valueOf(System.currentTimeMillis()));


        long timeInMillis = System.currentTimeMillis();
        Calendar cal1 = Calendar.getInstance();
        cal1.setTimeInMillis(timeInMillis);

        Calendar cal = Calendar.getInstance();
//        Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);


        mFromHourValue = 00;
        mFromMinuteValue = 00;

//        System.out.println("Hi date is :::" + timeFormat.format(cal.getTime()) + " mFromHourValue " + mFromHourValue + " " + mFromMinuteValue);

//        mStartTimeValue = "12:00 AM";
        mStartTimeValue = timeFormatShow.format(cal.getTime());
//        mStartTime = "12:00:00 am";
        mStartTime = timeFormat.format(cal.getTime());


        final Calendar c = Calendar.getInstance();
        // Current Hour
        hour = c.get(Calendar.HOUR_OF_DAY);
        // Current Minute
        minute = c.get(Calendar.MINUTE);
        isFromTime = false;
        updateTime(hour, minute);


        if (cd.isConnectingToInternet()) {

            new getCameraReport().execute();
        } else {
            Toast.makeText(getApplicationContext(),
                    "Please check your network connection",
                    Toast.LENGTH_SHORT).show();
        }

        setBottomLayoutData();
        bottomSheet = findViewById(R.id.bottom_sheet);
        behavior = BottomSheetBehavior.from(bottomSheet);
        behavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                // React to state change
//                System.out.println("Hi state 0000" + bottomSheetEnabled);


            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
                // React to dragging events
            }
        });
//

//        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
////                System.out.println("Hi state " + bottomSheetEnabled);
//
//
//                if (!bottomSheetEnabled) {
//                    behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
//                } else {
//                    setData();
//                    behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
//                }
//
//                if (bottomSheetEnabled) {
//                    bottomSheetEnabled = false;
//                } else {
//                    bottomSheetEnabled = true;
//                }
//
//
//            }
//        });


        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                startActivity(new Intent(CameraReportIamgeListNewActivtiy.this, MapMenuActivity.class));
                finish();


            }
        });
    }


    private void initViews() {
        mRecyclerView = (RecyclerView) findViewById(R.id.card_recycler_view);
        mRecyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(layoutManager);

        mTxtFromDate = (TextView) findViewById(R.id.txt_start_date_value);
        mTxtFromTime = (TextView) findViewById(R.id.txt_start_time_value);
        mTxtEndDate = (TextView) findViewById(R.id.txt_end_date_value);
        mTxtEndTime = (TextView) findViewById(R.id.txt_end_time_value);
        mBtnDone = (Button) findViewById(R.id.btn_done);

        mTxtSelectedVehicle = (TextView) findViewById(R.id.selected_vehicle_txt);
        mTxtSelectedVehicle.setText(Constant.SELECTED_VEHICLE_SHORT_NAME);

//        numberpicker = (NumberPicker) findViewById(R.id.numberPicker1);
//
//        spinnerFromHour = (Spinner) findViewById(R.id.spinner_from_time);
//        spinnerEndHour = (Spinner) findViewById(R.id.spinner_end_time);

        mTxtFromDate.setOnClickListener(this);
        mTxtFromTime.setOnClickListener(this);
        mTxtEndDate.setOnClickListener(this);
        mTxtEndTime.setOnClickListener(this);
        mBtnDone.setOnClickListener(this);
    }


    private void updateTime(int hours, int mins) {

        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.HOUR_OF_DAY, hours);
        cal.set(Calendar.MINUTE, mins);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
//        System.out.println("Hi date is 222:::" + timeFormat.format(cal.getTime()));

//        String aTime = new StringBuilder().append(hours).append(':')
//                .append(minutes).append(" ").append(timeSet).toString();
        String aTime = timeFormatShow.format(cal.getTime());
//        String aTime2 = new StringBuilder().append(hours).append(':')
//                .append(minutes).append(":00").append(minutes).append(" ").append(timeSet2).toString();

        String aTime2 = timeFormat.format(cal.getTime());

        SimpleDateFormat hourFormat = new SimpleDateFormat(
                "HH");
        SimpleDateFormat minuteFormat = new SimpleDateFormat(
                "mm");

        if (isFromTime) {
            mStartTime = aTime2;
            mStartTimeValue = aTime;


            mFromHourValue = Integer.valueOf(hourFormat.format(cal.getTime()));
            mFromMinuteValue = Integer.valueOf(minuteFormat.format(cal.getTime()));
            mTxtFromTime.setText(mStartTimeValue);
//            System.out.println("Hi from time value " + mFromHourValue + " " + mFromMinuteValue);
        } else {
            mEndTime = aTime2;
            mEndTimeValue = aTime;
//            mToHourValue = cal.HOUR_OF_DAY;
//            mToMinuteValue = cal.MINUTE;
            mTxtEndTime.setText(mEndTimeValue);

//            SimpleDateFormat hourFormat = new SimpleDateFormat(
//                    "hh");
//            SimpleDateFormat minuteFormat = new SimpleDateFormat(
//                    "mm");

            mToHourValue = Integer.valueOf(hourFormat.format(cal.getTime()));
            mToMinuteValue = Integer.valueOf(minuteFormat.format(cal.getTime()));

//            System.out.println("Hi to time value " + mToHourValue + " " + mToMinuteValue);
        }


    }


    private void showTimeDialog() {
        if (isFromTime) {
            hour = mFromHourValue;
            minute = mFromMinuteValue;
//            System.out.println("Hi clicked hour minute value " + mFromHourValue + " " + mFromMinuteValue + " :: " + hour + " " + minute);
        } else {
            hour = mToHourValue;
            minute = mToMinuteValue;
//            System.out.println("Hi clicked hour minute value " + mToHourValue + " " + mToMinuteValue + " :: " + hour + " " + minute);
        }


//                hour=20;
//                minute=33;

        TimePickerDialog dialog2 = new TimePickerDialog(this, timePickerListener, hour, minute,
                false);
        dialog2.show();
    }

    private void showDateDialog() {
        if (isFromDate) {

            String[] dateArray = mStartDate.split("-");
            year = Integer.parseInt(dateArray[0].trim());
            month = Integer.parseInt(dateArray[1].trim()) - 1;
            day = Integer.parseInt(dateArray[2].trim());
        } else {

            String[] dateArray = mEndDate.split("-");
            year = Integer.parseInt(dateArray[0].trim());
            month = Integer.parseInt(dateArray[1].trim()) - 1;
            day = Integer.parseInt(dateArray[2].trim());

        }
//        System.out.println("Hi year " + year + " mo " + month + " da " + day);
//        final Calendar c = Calendar.getInstance();
//        year = c.get(Calendar.YEAR);
//        month = c.get(Calendar.MONTH);
//        day = c.get(Calendar.DAY_OF_MONTH);
        /** set date picker as current date */
        DatePickerDialog dialog = new DatePickerDialog(this,
                datePickerListener, year, month, day);
        dialog.show();
    }


    private void setBottomLayoutData() {
        mTxtFromDate.setText(mStartDate);
        mTxtEndDate.setText(mEndDate);
        mTxtFromTime.setText(mStartTimeValue);
        mTxtEndTime.setText(mEndTimeValue);


    }


    private DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {

        // when dialog box is closed, below method will be called.
        public void onDateSet(DatePicker view, int selectedYear,
                              int selectedMonth, int selectedDay) {

            year = selectedYear;
            month = selectedMonth;
            day = selectedDay;
            StringBuilder mDate = new StringBuilder().append(year).append(":")
                    .append(getMonthValue(month + 1)).append(":").append(getMonthValue(day))
                    .append("");

            StringBuilder mDate2 = new StringBuilder().append(year).append("-")
                    .append(getMonthValue(month + 1)).append("-").append(getMonthValue(day))
                    .append(" ");
            if (isFromDate) {
                mStartDate = String.valueOf(mDate2);
                //  mStartTimeValue=String.valueOf(mDate2);
                mTxtFromDate.setText(mDate2);
            } else {
                mEndDate = String.valueOf(mDate2);

                mTxtEndDate.setText(mDate2);
            }
//            mFromDate = String.valueOf(mDate);
//            mFromDateTxtValue = String.valueOf(mDate2);
//            mTxtDate.setText(mDate2);
            // }
        }
    };

    private String getMonthValue(int month) {
        String mMonthValue = String.valueOf(month);
        if (mMonthValue.length() == 1) {
            mMonthValue = "0" + mMonthValue;
        }
        return mMonthValue;
    }


    private TimePickerDialog.OnTimeSetListener timePickerListener = new TimePickerDialog.OnTimeSetListener() {


        @Override
        public void onTimeSet(TimePicker view, int hourOfDay, int minutes) {
            // TODO Auto-generated method stub
            hour = hourOfDay;
            minute = minutes;

            updateTime(hour, minute);


        }

    };


    @Override
    public void onBackPressed() {
        super.onBackPressed();

        startActivity(new Intent(CameraReportIamgeListNewActivtiy.this, MapMenuActivity.class));
        finish();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_calendar, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // action with ID action_refresh was selected
            case R.id.action_calendar:
//                storeFavourtite();

                if (!bottomSheetEnabled) {
                    behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                } else {
                    // setData();
                    behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                }

                if (bottomSheetEnabled) {
                    bottomSheetEnabled = false;
                } else {
                    bottomSheetEnabled = true;
                }

                break;

            default:
                break;
        }

        return true;
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.btn_done:
//                createNumberDialog(0);
//                show(0);
                behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);

                if (cd.isConnectingToInternet()) {
//                                mStrFromDate = fromdatevalue.getText().toString().trim();
//                                mStrToDate = todatevalue.getText().toString().trim();
//                    mStoppageData = new ArrayList<StoppageReportDto>();
                    // mStoppageData.clear();
                    //  lv.setAdapter(null);
                    //  adapter.notifyDataSetChanged();
                    new getCameraReport().execute();
                } else {
                    Toast.makeText(getApplicationContext(),
                            "Please check your network connection",
                            Toast.LENGTH_SHORT).show();
                }

                break;
            case R.id.txt_start_date_value:

                isFromDate = true;
//                showDialog(DATE_DIALOG_ID);

                showDateDialog();
                break;
            case R.id.txt_start_time_value:
//                showDialog(DATE_DIALOG_FROMID);
                isFromTime = true;
                //   showDialog(TIME_DIALOG_ID);

                showTimeDialog();

                break;
            case R.id.txt_end_date_value:
//                createNumberDialog(1);
                isFromDate = false;
//                showDialog(DATE_DIALOG_ID);


                showDateDialog();
//                show(1);

                break;
            case R.id.txt_end_time_value:
//                createNumberDialog(0);
                isFromTime = false;
                // showDialog(TIME_DIALOG_ID);

                showTimeDialog();
//                show(0);
                break;

        }
    }

//    private void createNumberDialog(int mType) {
//        if (Const.daysDiff(mFromDateTxtValue) > 0) {
//
//            showNumberDialog(23, mType);
//
//        } else {
//            if (mType == 0) {
//                String mToTimeValue = dftime.format(new Date());
//
//                int mTovalue = Integer.valueOf(mToTimeValue);
//                if (mTovalue > 0) {
//                    mTovalue = mTovalue - 1;
//                }
//
//                showNumberDialog(mTovalue, mType);
//            } else {
//                String mToTimeValue = dftime.format(new Date());
//
//                int mTovalue = Integer.valueOf(mToTimeValue);
//
//
//                showNumberDialog(mTovalue, mType);
//            }
//        }
//
//    }

    private class getCameraReport extends AsyncTask<String, String, String> {
        ProgressDialog progressDialog;

        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub

            String result = null;
            try {

                String string_from_date = mStartDate.trim() + " " + mStartTime.trim();
                String string_to_date = mEndDate.trim() + " " + mEndTime.trim();
                SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss aa");

                Date mFromDate = f.parse(string_from_date);
                Date mToDate = f.parse(string_to_date);

                SimpleDateFormat fNew = new SimpleDateFormat("yyyyMMddHHmmss");
                String mStrFromDate = fNew.format(mFromDate);
                String mStrToDate = fNew.format(mToDate);


                HttpConfig ht = new HttpConfig();
//                result = ht.httpGet(Const.API_URL + "/mobile/getPictures?vehicleId=" + Constant.SELECTED_VEHICLE_ID + "&date=" + mFromDate + "&sTime=" + mFromTime + "&eTime=" + mToTime + "&userId=" + Constant.SELECTED_USER_ID);
                result = ht.httpGet(Const.API_URL + "/mobile/getImages?vehicleId=" + Constant.SELECTED_VEHICLE_ID + "&fromTime=" + mStrFromDate + "&toTime=" + mStrToDate + "&userId=" + Constant.SELECTED_USER_ID);

            } catch (Exception e) {

            }

            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            progressDialog.dismiss();
            mCameraReportList = new ArrayList<CameraReportDto>();
            if (result != null && result.length() > 0) {


                try {
                    JSONArray jArr = new JSONArray(result.trim());


                    for (int i = 0; i < jArr.length(); i++) {
                        JSONObject jsonCameraObject = jArr.getJSONObject(i);
                        CameraReportDto c = new CameraReportDto();

                        if (jsonCameraObject.has("address")) {

                            c.setAddress(jsonCameraObject.getString("address"));
                        }
                        if (jsonCameraObject.has("imageName")) {

                            c.setImageName(jsonCameraObject.getString("imageName").trim());
                        }
                        if (jsonCameraObject.has("lat")) {

                            c.setLat(jsonCameraObject.getString("lat"));
                        }
                        if (jsonCameraObject.has("lng")) {

                            c.setLng(jsonCameraObject.getString("lng"));
                        }
                        if (jsonCameraObject.has("timeStamp")) {

                            c.setTimeStamp(jsonCameraObject.getLong("timeStamp"));

                        }

                        mCameraReportList.add(c);

                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }

            if (mCameraReportList != null && mCameraReportList.size() > 0) {

                //mAdapter = new CameraImageAdapter(mCameraReportList, CameraReportIamgeListActivtiy.this);

                DataAdapter mAadp = new DataAdapter(mCameraReportList, CameraReportIamgeListNewActivtiy.this);
                mRecyclerView.setAdapter(mAadp);
                mAadp.notifyDataSetChanged();

            } else {
                mRecyclerView.setAdapter(null);
                Toast.makeText(CameraReportIamgeListNewActivtiy.this, "No record found", Toast.LENGTH_SHORT).show();
                //show date selection layout
            }

        }

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            progressDialog = new ProgressDialog(CameraReportIamgeListNewActivtiy.this,
                    AlertDialog.THEME_HOLO_LIGHT);
            progressDialog.setMessage("Please Wait...");
            progressDialog.setProgressDrawable(new ColorDrawable(
                    android.graphics.Color.BLUE));
            progressDialog.setCancelable(true);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.show();
        }

    }

}
