package com.vamosys.vamos;

import android.Manifest;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.preference.PreferenceManager;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.appcompat.app.AlertDialog;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.GroundOverlayOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.vamosys.model.VehicleData;
import com.vamosys.utils.ConnectionDetector;
import com.vamosys.utils.Constant;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by prabhakaran on 1/23/2016.
 */
public class NearByGoogleMapViewActivity extends FragmentActivity implements GoogleMap.OnInfoWindowClickListener, View.OnClickListener, OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, ResultCallback<LocationSettingsResult>, com.google.android.gms.location.LocationListener {

    //    Popup dialog initialize


    protected GoogleApiClient mGoogleApiClient;
    protected LocationRequest locationRequest;
    int REQUEST_CHECK_SETTINGS = 100;
    double mNewLat = 0.0, mNewLng = 0.0, mOldLat = 0.0, mOldLng = 0.0;
    int mTrackOldLatLngPosition = 0;
    boolean isTrackingStarted = false;
    ArrayList<LatLng> mTrackLatLngList = new ArrayList<LatLng>();
    boolean mCursorAlreadyInitialized = false;
    private Marker trackingMarker;
    View mIconMarker = null;
    int width, height;
    ImageView $ImageBack, $ChangeView, mImgNavigation;
    //  TextView $TxtTitle;
    Handler handler = new Handler();
    private final Handler mHandler = new Handler();

    LatLng mStartLocation = null, mEndLocation = null;


    private long startTime = 2 * 1000;
    private static int ANIMATE_SPEEED = 2000;
    private static int ANIMATE_SPEEED_TURN = 2000;


    //String[] mSpinnerVehicleId = {"Select"};

    private GoogleMap map;
    TextView mTxtHeader;
    Button mBtnStartTracking;

    /**
     * POI declarations
     */
    double lat, lng;
    float vehicle_zoom_level = 13.5F;
    float group_zoom_level = 13.5F;
    private HashMap<Marker, VehicleData> mMarkersHashMap;
    //    private JSONObject jSelectedVehicleObject;
    ConnectionDetector cd;
    Dialog marker_info_dialog;
    SharedPreferences sp;
    Const cons;

    ArrayList<String> child = new ArrayList<String>();
    ArrayList<String> vehicle = new ArrayList<String>();

    private ArrayList<Object> childelements = new ArrayList<Object>();

    int map_marker_vechile_position, position;

    VehicleData mVehicleObj = new VehicleData();
    String mSELECTED_MAP_TYPE = "Normal";

    String mAddsData1, mAddsData2;

    Polyline polyline;

    LinearLayout mGoogleMapLayout;
    //    MapView mMapViewNew;


    /**
     * Runnable to update the map with the seleced Vehicle and track the vehicle
     */
    Runnable runable = new Runnable() {
        @Override
        public void run() {
//            Cursor getcurrent_vehicle_info = null;
            try {

                System.out.println("Hi The Live vehicle lat and lng count is ::::::" + mTrackLatLngList.size() + " :: cursor already initialized " + mCursorAlreadyInitialized);
                if (mTrackLatLngList.size() > 1) {
                    mStartLocation = mTrackLatLngList.get(mTrackLatLngList.size() - 2);
                    mEndLocation = mTrackLatLngList.get(mTrackLatLngList.size() - 1);

                } else if (mTrackLatLngList.size() == 1) {
                    mStartLocation = mTrackLatLngList.get(mTrackLatLngList.size() - 1);
                    mEndLocation = mTrackLatLngList.get(mTrackLatLngList.size() - 1);
                } else {

                }
//                if (mTrackOldLatLngPosition < mTrackLatLngList.size()) {
//                    mStartLocation = mTrackLatLngList.get(mTrackOldLatLngPosition);
//                    mEndLocation = mTrackLatLngList.get(mTrackLatLngList.size() - 1);
//                    mTrackOldLatLngPosition++;
//                } else {
//                    mStartLocation = mTrackLatLngList.get(mTrackLatLngList.size() - 1);
//                    mEndLocation = mTrackLatLngList.get(mTrackLatLngList.size() - 1);
//                }


                if (mCursorAlreadyInitialized) {
                    animator.moveVehicleWithoutInit();
                } else {
                    animator.startAnimation(true);
                }

                handler.removeCallbacksAndMessages(null);
                handler.postDelayed(this, startTime);
            } catch (
                    Exception e)

            {
                // TODO: handle exception

            } finally

            {

                handler.removeCallbacksAndMessages(null);
                handler.postDelayed(this, startTime);
            }
        }
    };


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.nearby_map_view_activity_layout);
        sp = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());


        init();
        screenArrange();

//        System.out.println("Home activity created ::::::");
        cons = new Const();
        cd = new ConnectionDetector(getApplicationContext());


        if (sp.getString("ip_adds", "") != null) {
            if (sp.getString("ip_adds", "").trim().length() > 0) {
                Const.API_URL = sp.getString("ip_adds", "");
            }
        }
//        setupMap();

    }

    public void init() {
        mCursorAlreadyInitialized = false;
//        map = ((MapFragment) getFragmentManager().findFragmentById(R.id.mapView_layout_map)).getMap();
//        map = ((SupportMapFragment) this.getSupportFragmentManager().findFragmentById(R.id.mapView_layout_map)).getMap();

        mGoogleMapLayout = (LinearLayout) findViewById(R.id.google_map_layout);
        RelativeLayout rl = (RelativeLayout) findViewById(R.id.map_view_relativelayout);
        mGoogleMapLayout.setVisibility(View.VISIBLE);
        rl.setVisibility(View.GONE);

//        FragmentManager myFragmentManager = getSupportFragmentManager();
//        SupportMapFragment myMapFragment = (SupportMapFragment) myFragmentManager
//                .findFragmentById(R.id.nearby_mapView_layout_map);
//        map = myMapFragment.getMap();

        FragmentManager myFragmentManager = getSupportFragmentManager();
        SupportMapFragment myMapFragment = (SupportMapFragment) myFragmentManager
                .findFragmentById(R.id.nearby_mapView_layout_map);
//        map = myMapFragment.getMap();
        myMapFragment.getMapAsync(this);


//        map.setPadding(1, 1, 1, 150);
//        map.getUiSettings().setZoomControlsEnabled(true);
        mMarkersHashMap = new HashMap<Marker, VehicleData>();
//        map.setOnInfoWindowClickListener(this);
        mBtnStartTracking = (Button) findViewById(R.id.btn_start_tracking);
        mTxtHeader = (TextView) findViewById(R.id.nearby_map_title);

        $ImageBack = (ImageView) findViewById(R.id.nearby_map_view_Back);
        $ChangeView = (ImageView) findViewById(R.id.nearby_map_changeViewIcon);
        mImgNavigation = (ImageView) findViewById(R.id.nearby_map_navigation);
        // $TxtTitle = (TextView) findViewById(R.id.map_title);
//        $SpinnerNavigation = (Spinner) findViewById(R.id.map_spinner);
        mImgNavigation.setVisibility(View.GONE);
        mBtnStartTracking.setVisibility(View.GONE);
        $ImageBack.setOnClickListener(this);
        $ChangeView.setOnClickListener(this);
        mImgNavigation.setOnClickListener(this);
        mBtnStartTracking.setOnClickListener(this);

        View marker = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.custommarker, null);
        ImageView id_custom_marker_icon = (ImageView) marker.findViewById(R.id.id_custom_marker_icon);
        ImageView id_vehicle_in_marker = (ImageView) marker.findViewById(R.id.id_vehicle_in_marker);

        id_custom_marker_icon.setImageResource(R.drawable.green_custom_marker_icon);
        id_vehicle_in_marker.setImageResource(R.drawable.ic_person);
//
        mIconMarker = marker;

//        id_all_vehicles_value = (TextView) findViewById(R.id.id_home_all_vehicles_value);
//        id_online_vehicles_value = (TextView) findViewById(R.id.id_home_online_vehicles_value);
//        id_offline_vehicles_value = (TextView) findViewById(R.id.id_home_offline_vehicles_value);
    }

    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub
        switch (v.getId()) {
            case R.id.nearby_map_view_Back:
                if (!Constant.mUserLocationNearbyActivityCalled) {
                    startActivity(new Intent(NearByGoogleMapViewActivity.this, NearByVehicleListActivity.class));
                    finish();
                } else {
                    stopLocationUpdate();
                    finish();
                }
                break;
            case R.id.nearby_map_changeViewIcon:
                opptionPopUp();
                break;
            case R.id.nearby_map_navigation:
                openNavigationApp();
                break;
            case R.id.btn_start_tracking:
                openNavigationApp();
                break;
        }
    }

    @Override
    public void onInfoWindowClick(Marker marker) {

    }


    /**
     * Code to make the Marker icon adjustable for multiple screen
     */
    public static Bitmap createDrawableFromView(Context context, View view) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        view.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        view.measure(displayMetrics.widthPixels, displayMetrics.heightPixels);
        view.layout(0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels);
        view.buildDrawingCache();
        Bitmap bitmap = Bitmap.createBitmap(view.getMeasuredWidth(), view.getMeasuredHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        view.draw(canvas);
        return bitmap;

    }


    private void openNavigationApp() {


//        String startLatLng = Constant.SELECTED_USER_LAT + "," + Constant.SELECTED_USER_LNG;
//        String endLatLng = UserLocationNearVehicleList.mSELCETED_NEARBY_VEHICLE_LAT + "," + UserLocationNearVehicleList.mSELCETED_NEARBY_VEHICLE_LNG;
//
//        Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
//                Uri.parse("http://maps.google.com/maps?saddr=" + startLatLng + "&daddr=" + endLatLng));
//        startActivity(intent);


        //start live tracking


        if (mBtnStartTracking.getText().toString().equalsIgnoreCase(getString(R.string.start_tracking))) {

            if (!checkLocationPermission()) {
                locationCheck();

                isTrackingStarted = true;
                mBtnStartTracking.setText(getString(R.string.stop_tracking));
                vehicletracking();
            } else {
                showP3rmAlert();
            }
        } else {
            stopLocationUpdate();
//            cancelVehicleTracking();
            mBtnStartTracking.setText(getString(R.string.start_tracking));
            isTrackingStarted = false;
        }

    }


    public void setupUserNearbyMap() {
        mImgNavigation.setVisibility(View.GONE);
        mBtnStartTracking.setVisibility(View.VISIBLE);
        if (map != null) {
            map.clear();
        }


        MarkerOptions markerOption = new MarkerOptions().position(new LatLng(UserLocationNearVehicleList.mSELCETED_NEARBY_VEHICLE_LAT, UserLocationNearVehicleList.mSELCETED_NEARBY_VEHICLE_LNG));
        markerOption.title(UserLocationNearVehicleList.mSELECTED_VEHICLE_SHORT_NAME);
        markerOption.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
        Marker currentMarker = map.addMarker(markerOption);
        currentMarker.hideInfoWindow();

//        MarkerOptions markerOption1 = new MarkerOptions().position(new LatLng(Double.valueOf(Constant.SELECTED_USER_LAT), Double.valueOf(Constant.SELECTED_USER_LNG)));
//        markerOption1.title("You are here");
//        markerOption1.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
//        Marker currentMarker2 = map.addMarker(markerOption1);
//        currentMarker2.hideInfoWindow();


        MarkerOptions markerOption1 = new MarkerOptions().position(new LatLng(Double.valueOf(Constant.SELECTED_USER_LAT), Double.valueOf(Constant.SELECTED_USER_LNG)));
        markerOption1.title(getResources().getString(R.string.you_are) + (double) Math.round(UserLocationNearVehicleList.mSELECTED_VEHICLE_DISTANCE * 10d) / 10d + getResources().getString(R.string.kms_away));
        markerOption1.icon(BitmapDescriptorFactory.fromBitmap(createDrawableFromView(NearByGoogleMapViewActivity.this, mIconMarker)));
//                                if (current_vehicle_location_marker.isEmpty()) {
//        trackingMarker = map.addMarker(markerOption1);
        Marker currentMarker2 = map.addMarker(markerOption1);
        currentMarker2.showInfoWindow();
//        currentMarker2.hideInfoWindow();


        CameraPosition INIT = new CameraPosition.Builder().target(new LatLng(Double.valueOf(Constant.SELECTED_USER_LAT), Double.valueOf(Constant.SELECTED_USER_LNG))).zoom(vehicle_zoom_level).build();
        map.animateCamera(CameraUpdateFactory.newCameraPosition(INIT));

        addCircleToMap(Double.valueOf(Constant.SELECTED_USER_LAT), Double.valueOf(Constant.SELECTED_USER_LNG));
        createDefaultPolyLine(Double.valueOf(Constant.SELECTED_USER_LAT), Double.valueOf(Constant.SELECTED_USER_LNG),
                UserLocationNearVehicleList.mSELCETED_NEARBY_VEHICLE_LAT, UserLocationNearVehicleList.mSELCETED_NEARBY_VEHICLE_LNG);

    }


    public void setupMap() {
        //  JSONObject vehiclejson = Constant.SELECTED_VEHICLE_JSON_OBJECT;


        // System.out.println("The selected vehicle object is :::::" + Constant.SELECTED_VEHICLE_JSON_OBJECT);


        if (map != null) {
            map.clear();
        }
        String adds1, adds2;

        MarkerOptions markerOption = new MarkerOptions().position(new LatLng(NearByVehicleListActivity.mSELCETED_NEARBY_VEHICLE_LAT, NearByVehicleListActivity.mSELCETED_NEARBY_VEHICLE_LNG));

        markerOption.title(NearByVehicleListActivity.mSELECTED_VEHICLE_SHORT_NAME);
//        markerOption.icon(BitmapDescriptorFactory.fromBitmap(createDrawableFromView(NearByGoogleMapViewActivity.this, marker)));
        markerOption.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
        Marker currentMarker = map.addMarker(markerOption);
        currentMarker.hideInfoWindow();

        MarkerOptions markerOption1 = new MarkerOptions().position(new LatLng(Double.valueOf(Constant.SELECTED_MAIN_VEHICLE_LAT), Double.valueOf(Constant.SELECTED_MAIN_VEHICLE_LNG)));


        markerOption1.title(Constant.SELECTED_VEHICLE_SHORT_NAME);
        markerOption1.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
        Marker currentMarker2 = map.addMarker(markerOption1);
        currentMarker2.hideInfoWindow();

        CameraPosition INIT = new CameraPosition.Builder().target(new LatLng(Double.valueOf(Constant.SELECTED_MAIN_VEHICLE_LAT), Double.valueOf(Constant.SELECTED_MAIN_VEHICLE_LNG))).zoom(vehicle_zoom_level).build();
        map.animateCamera(CameraUpdateFactory.newCameraPosition(INIT));

        addCircleToMap(Double.valueOf(Constant.SELECTED_MAIN_VEHICLE_LAT), Double.valueOf(Constant.SELECTED_MAIN_VEHICLE_LNG));
        createDefaultPolyLine(Double.valueOf(Constant.SELECTED_MAIN_VEHICLE_LAT), Double.valueOf(Constant.SELECTED_MAIN_VEHICLE_LNG),
                NearByVehicleListActivity.mSELCETED_NEARBY_VEHICLE_LAT, NearByVehicleListActivity.mSELCETED_NEARBY_VEHICLE_LNG);

    }

    private void addCircleToMap(double latitude, double longitude) {

        // circle settings
        int radiusM = UserLocationNearVehicleList.mCircleMeters;// your radius in meters
        // double latitude = // your center latitude
//        double longitude = // your center longitude
        LatLng latLng = new LatLng(latitude, longitude);

        // draw circle
        int d = 500; // diameter
        Bitmap bm = Bitmap.createBitmap(d, d, Bitmap.Config.ARGB_8888);
        Canvas c = new Canvas(bm);
        Paint p = new Paint();
        p.setColor(getResources().getColor(R.color.light_green));
        c.drawCircle(d / 2, d / 2, d / 2, p);

        // generate BitmapDescriptor from circle Bitmap
        BitmapDescriptor bmD = BitmapDescriptorFactory.fromBitmap(bm);

// mapView is the GoogleMap
        map.addGroundOverlay(new GroundOverlayOptions().
                image(bmD).
                position(latLng, radiusM * 2, radiusM * 2).
                transparency(0.4f));
    }

    public void createDefaultPolyLine(double lat1, double lng1, double lat2, double lng2) {
        if (polyline != null) {
            polyline.remove();
        }
        PolylineOptions options = new PolylineOptions().width(8).color(getResources().getColor(R.color.red)).geodesic(true);
        try {


            LatLng position = new LatLng(lat1, lng1);
            LatLng position1 = new LatLng(lat2, lng2);
            options.add(position);
            options.add(position1);


        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }


        polyline = map.addPolyline(options);

    }


    private void opptionPopUp() {
// TODO Auto-generated method stub
        final Dialog dialog = new Dialog(NearByGoogleMapViewActivity.this);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.radio_popup);
        dialog.show();

        RadioGroup rg_home = (RadioGroup) dialog.findViewById(R.id.rg_home_views);
        RadioGroup rg_history = (RadioGroup) dialog.findViewById(R.id.rg_history_views);
        rg_history.clearCheck();
        rg_history.setVisibility(View.GONE);
        rg_home.setVisibility(View.VISIBLE);

        RadioButton $Normal = (RadioButton) dialog
                .findViewById(R.id.rb_home_normal);
        RadioButton $Satelite = (RadioButton) dialog
                .findViewById(R.id.rb_home_satellite);
        RadioButton $Terrain = (RadioButton) dialog
                .findViewById(R.id.rb_home_terrain);
        RadioButton $Hybrid = (RadioButton) dialog
                .findViewById(R.id.rb_home_hybrid);


        if (mSELECTED_MAP_TYPE.equalsIgnoreCase("Normal")) {
            $Normal.setChecked(true);
            $Satelite.setChecked(false);
            $Terrain.setChecked(false);
            $Hybrid.setChecked(false);
        } else if (mSELECTED_MAP_TYPE.equalsIgnoreCase("Satelite")) {
            $Normal.setChecked(false);
            $Satelite.setChecked(true);
            $Terrain.setChecked(false);
            $Hybrid.setChecked(false);
        } else if (mSELECTED_MAP_TYPE.equalsIgnoreCase("Terrain")) {
            $Normal.setChecked(false);
            $Satelite.setChecked(false);
            $Terrain.setChecked(true);
            $Hybrid.setChecked(false);
        } else if (mSELECTED_MAP_TYPE.equalsIgnoreCase("Hybrid")) {
            $Normal.setChecked(false);
            $Satelite.setChecked(false);
            $Terrain.setChecked(false);
            $Hybrid.setChecked(true);
        }
        $Normal.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                map.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                mSELECTED_MAP_TYPE = "Normal";
                dialog.dismiss();
            }
        });
        $Satelite.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                mSELECTED_MAP_TYPE = "Satelite";
                map.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
                dialog.dismiss();
            }
        });
        $Terrain.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                mSELECTED_MAP_TYPE = "Terrain";
                map.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
                dialog.dismiss();
            }
        });

        $Hybrid.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                mSELECTED_MAP_TYPE = "Hybrid";
                map.setMapType(GoogleMap.MAP_TYPE_HYBRID);
                dialog.dismiss();
            }
        });

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        height = displayMetrics.heightPixels;
        width = displayMetrics.widthPixels;

        LinearLayout.LayoutParams radioParama = new LinearLayout.LayoutParams(
                FrameLayout.LayoutParams.WRAP_CONTENT,
                FrameLayout.LayoutParams.WRAP_CONTENT);
        radioParama.width = width * 50 / 100;
        radioParama.height = width * 10 / 100;
        radioParama.topMargin = height * 4 / 100;
        radioParama.gravity = Gravity.CENTER;
        radioParama.leftMargin = height * 4 / 100;
        $Normal.setLayoutParams(radioParama);
        $Satelite.setLayoutParams(radioParama);
        $Terrain.setLayoutParams(radioParama);

        LinearLayout.LayoutParams radioterrainParama = new LinearLayout.LayoutParams(
                FrameLayout.LayoutParams.WRAP_CONTENT,
                FrameLayout.LayoutParams.WRAP_CONTENT);
        radioterrainParama.width = width * 50 / 100;
        radioterrainParama.height = width * 10 / 100;
        radioterrainParama.topMargin = height * 4 / 100;
        radioterrainParama.gravity = Gravity.CENTER;
        radioterrainParama.leftMargin = height * 4 / 100;
        radioterrainParama.bottomMargin = height * 4 / 100;
        $Hybrid.setLayoutParams(radioterrainParama);

        if (width >= 600) {
            $Normal.setTextSize(16);
            $Satelite.setTextSize(16);
            $Terrain.setTextSize(16);
            $Hybrid.setTextSize(16);
        } else if (width > 501 && width < 600) {
            $Normal.setTextSize(15);
            $Satelite.setTextSize(15);
            $Terrain.setTextSize(15);
            $Hybrid.setTextSize(15);
        } else if (width > 260 && width < 500) {
            $Normal.setTextSize(14);
            $Satelite.setTextSize(14);
            $Terrain.setTextSize(14);
            $Hybrid.setTextSize(14);
        } else if (width <= 260) {
            $Normal.setTextSize(13);
            $Satelite.setTextSize(13);
            $Terrain.setTextSize(13);
            $Hybrid.setTextSize(13);
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        map.setPadding(1, 1, 1, 150);
        map.getUiSettings().setZoomControlsEnabled(true);
//        mMarkersHashMap = new HashMap<Marker, VehicleData>();
        map.setOnInfoWindowClickListener(this);
        map.getUiSettings().setMapToolbarEnabled(false);
//        map.getUiSettings().set
        if (Constant.mUserLocationNearby) {
//            Toast.makeText(this, "usernearmap", Toast.LENGTH_SHORT).show();
            setupUserNearbyMap();
        } else {
//            Toast.makeText(this, "setmap", Toast.LENGTH_SHORT).show();
            setupMap();
        }
    }


    private void locationCheck() {

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this).build();
        mGoogleApiClient.connect();

        locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(1000);
        locationRequest.setFastestInterval(1000);
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest);
        builder.setAlwaysShow(true);
        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(
                        mGoogleApiClient,
                        builder.build()
                );

        result.setResultCallback(this);

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Toast.makeText(getApplicationContext(), getResources().getString(R.string.please_try_again), Toast.LENGTH_LONG).show();
        startActivity(new Intent(NearByGoogleMapViewActivity.this, VehicleListActivity.class));
        finish();
    }

    @Override
    public void onResult(@NonNull LocationSettingsResult locationSettingsResult) {
        final Status status = locationSettingsResult.getStatus();
        switch (status.getStatusCode()) {
            case LocationSettingsStatusCodes.SUCCESS:

                // NO need to show the dialog;
                System.out.println("Hi gps already enabled true");
                startLocationUpdate();
//                getLocationData();
                break;

            case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                //  Location settings are not satisfied. Show the user a dialog

                try {
                    // Show the dialog by calling startResolutionForResult(), and check the result
                    // in onActivityResult().
                    System.out.println("Hi gps already enabled false");
                    status.startResolutionForResult(NearByGoogleMapViewActivity.this, REQUEST_CHECK_SETTINGS);

                } catch (IntentSender.SendIntentException e) {

                    //failed to show
                }
                break;

            case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                // Location settings are unavailable so not possible to show any dialog now
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CHECK_SETTINGS) {

            if (resultCode == RESULT_OK) {
//                getLocationData();
//                Toast.makeText(getApplicationContext(), "GPS enabled", Toast.LENGTH_LONG).show();
                startLocationUpdate();
            } else {

                Toast.makeText(getApplicationContext(), getResources().getString(R.string.enable_location), Toast.LENGTH_LONG).show();
                startActivity(new Intent(NearByGoogleMapViewActivity.this, VehicleListActivity.class));
                finish();
            }

        }
    }

    @Override
    public void onLocationChanged(Location location) {
        if (location != null) {
//            latLng = location.getLatitude() + "," + location.getLongitude();
            mNewLat = location.getLatitude();
            mNewLng = location.getLongitude();
            Log.d("mNewLat",""+mNewLat);
            Log.d("mNewLng",""+mNewLng);
            LatLng la = new LatLng(location.getLatitude(), location.getLongitude());


//            MarkerOptions markerOption1 = new MarkerOptions().position(new LatLng(Double.valueOf(Constant.SELECTED_USER_LAT), Double.valueOf(Constant.SELECTED_USER_LNG)));
//            markerOption1.title("You are " + Const.distance(UserLocationNearVehicleList.mSELCETED_NEARBY_VEHICLE_LAT, UserLocationNearVehicleList.mSELCETED_NEARBY_VEHICLE_LNG, mNewLat, mNewLng) + " away");
//            markerOption1.icon(BitmapDescriptorFactory.fromBitmap(createDrawableFromView(NearByGoogleMapViewActivity.this, mIconMarker)));
////                                if (current_vehicle_location_marker.isEmpty()) {
//            trackingMarker = map.addMarker(markerOption1);


            createDefaultPolyLine(mNewLat, mNewLng,
                    UserLocationNearVehicleList.mSELCETED_NEARBY_VEHICLE_LAT, UserLocationNearVehicleList.mSELCETED_NEARBY_VEHICLE_LNG);

            trackingMarker.setTitle(getResources().getString(R.string.you_are) + (double) Math.round(Const.distance(UserLocationNearVehicleList.mSELCETED_NEARBY_VEHICLE_LAT, UserLocationNearVehicleList.mSELCETED_NEARBY_VEHICLE_LNG, mNewLat, mNewLng) * 10d) / 10d + getResources().getString(R.string.kms_away));
            trackingMarker.showInfoWindow();

            trackingMarker.setPosition(la);
//            la
//            LatLng laOld = mTrackLatLngList.get(mTrackLatLngList.size() - 1);
//            if (!(laOld.latitude == la.latitude && laOld.longitude == la.longitude)) {
//                mTrackLatLngList.add(la);
//            }
            Log.i("LOGSERVICE", "Hi latlng " + la);

            // start animate marker

//            latLng = latitude + "," + longitude;
//            Log.i("LOGSERVICE", "Hi latlng " + la + " :: latlng list size is ::" + mTrackLatLngList.size() + " " + laOld + " " + laOld.latitude + " " + laOld.longitude);

//            if (mUserLocationNearestVehicleList.size() > 0) {
//
//            } else {
////                getKmsData();

////                openBottomSheet();
//            }


        } else {
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.location_not_available), Toast.LENGTH_LONG).show();
        }
//        stopLocationUpdate();
    }

    private void startLocationUpdate() {
//        initLocationRequest();

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, locationRequest, this);
    }


    private void stopLocationUpdate() {
        if (mGoogleApiClient != null) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
        }

    }

    public boolean checkLocationPermission() {
        return (ActivityCompat.checkSelfPermission(NearByGoogleMapViewActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(NearByGoogleMapViewActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED);

    }

    public void showP3rmAlert() {
        AlertDialog.Builder builder = new AlertDialog.Builder(NearByGoogleMapViewActivity.this);
        builder.setTitle(getResources().getString(R.string.app_permission));
//        builder.setMessage(getString(R.string.app_p3rm_dialog_message));
        builder.setMessage(getResources().getString(R.string.location_permission));

        String positiveText = NearByGoogleMapViewActivity.this.getString(android.R.string.ok);
        builder.setPositiveButton(positiveText,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // positive button logic
                        dialog.dismiss();
                        Intent intent = new Intent();
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.setAction(android.provider.Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                        Uri uri = Uri.fromParts("package", getApplicationContext().getPackageName(), null);
                        intent.setData(uri);
                        getApplicationContext().startActivity(intent);
                    }
                });

        String negativeText = NearByGoogleMapViewActivity.this.getString(android.R.string.cancel);
        builder.setNegativeButton(negativeText,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // negative button logic
                        dialog.dismiss();
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.location_permission), Toast.LENGTH_LONG).show();
                        startActivity(new Intent(NearByGoogleMapViewActivity.this, VehicleListActivity.class));
                        finish();
                    }
                });

        AlertDialog dialog = builder.create();
        // display dialog
        dialog.show();

    }


    public void vehicletracking() {

        try {
            if (map != null) {
                map.clear();
            }

            //draw end marker and starting marker(which is animating marker)

            System.out.println("Hi marker create lat ::" + UserLocationNearVehicleList.mSELCETED_NEARBY_VEHICLE_LAT + " lng " + UserLocationNearVehicleList.mSELCETED_NEARBY_VEHICLE_LNG);
            MarkerOptions markerOption = new MarkerOptions().position(new LatLng(UserLocationNearVehicleList.mSELCETED_NEARBY_VEHICLE_LAT, UserLocationNearVehicleList.mSELCETED_NEARBY_VEHICLE_LNG));
            markerOption.title(UserLocationNearVehicleList.mSELECTED_VEHICLE_SHORT_NAME);
            markerOption.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
            Marker currentMarker = map.addMarker(markerOption);
            currentMarker.hideInfoWindow();

            MarkerOptions markerOption1 = new MarkerOptions().position(new LatLng(Double.valueOf(Constant.SELECTED_USER_LAT), Double.valueOf(Constant.SELECTED_USER_LNG)));
            markerOption1.title("You are " + (double) Math.round(UserLocationNearVehicleList.mSELECTED_VEHICLE_DISTANCE * 10d) / 10d + "KMs away)");
            markerOption1.icon(BitmapDescriptorFactory.fromBitmap(createDrawableFromView(NearByGoogleMapViewActivity.this, mIconMarker)));
//                                if (current_vehicle_location_marker.isEmpty()) {
            trackingMarker = map.addMarker(markerOption1);
            trackingMarker.showInfoWindow();
            mCursorAlreadyInitialized = true;
            LatLng laLn = new LatLng(Double.valueOf(Constant.SELECTED_USER_LAT), Double.valueOf(Constant.SELECTED_USER_LNG));
            mTrackLatLngList.add(laLn);

//            if (Constant.SELECTED_VEHICLE_ID != null && Constant.SELECTED_USER_ID != null) {
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.tracking), Toast.LENGTH_LONG).show();


            createDefaultPolyLine(Double.valueOf(Constant.SELECTED_USER_LAT), Double.valueOf(Constant.SELECTED_USER_LNG),
                    UserLocationNearVehicleList.mSELCETED_NEARBY_VEHICLE_LAT, UserLocationNearVehicleList.mSELCETED_NEARBY_VEHICLE_LNG);


            cancelVehicleTracking();

//            handler.postDelayed(runable, startTime);


//            } else {
//
//                cancelVehicleTracking();
//                animator.stopAnimation();
////                startActivity(new Intent(VehicleTrackingActivity.this, VehicleListActivity.class));
////                finish();
//            }
        } catch (Exception e) {
            isTrackingStarted = false;
            mBtnStartTracking.setText(getResources().getString(R.string.start_tracking));
            cancelVehicleTracking();
            Toast.makeText(NearByGoogleMapViewActivity.this, getResources().getString(R.string.please_try_again), Toast.LENGTH_SHORT).show();
        }
    }


    public void cancelVehicleTracking() {

        handler.removeCallbacksAndMessages(null);
//        handler.removeCallbacksAndMessages(runable);
    }

    private Animator animator = new Animator();


    int currentPt;

    GoogleMap.CancelableCallback MyCancelableCallback =
            new GoogleMap.CancelableCallback() {

                @Override
                public void onCancel() {
                    // System.out.println("onCancelled called");
                }

                @Override
                public void onFinish() {


                }

            };


    public class Animator implements Runnable {


        private final Interpolator interpolator = new LinearInterpolator();


        float tilt = 90;
        float zoom = 13.5f;
        boolean upward = true;

        long start = SystemClock.uptimeMillis();

        LatLng endLatLng = null;
        LatLng beginLatLng = null;

        boolean showPolyline = false;


        public void stop() {
            if (trackingMarker != null) {
                trackingMarker.remove();
            }
            mHandler.removeCallbacks(animator);

        }

        public void initialize(boolean showPolyLine) {
            mCursorAlreadyInitialized = true;
            System.out.println("Prabha initialize called :::::::::::::");
            reset();
            this.showPolyline = showPolyLine;

            // highLightMarker(0);

            if (showPolyLine) {
                polyLine = initializePolyLine();
            }
            System.out.println("Initia lize method called ::::" + mTrackOldLatLngPosition);
            // We first need to put the camera in the correct position for the first run (we need 2 markers for this).....


            LatLng markerPos = mStartLocation;
            LatLng secondPos = mEndLocation;


            MarkerOptions markerOption2 = new MarkerOptions().position(new LatLng(UserLocationNearVehicleList.mSELCETED_NEARBY_VEHICLE_LAT, UserLocationNearVehicleList.mSELCETED_NEARBY_VEHICLE_LNG));
            markerOption2.title(UserLocationNearVehicleList.mSELECTED_VEHICLE_SHORT_NAME);
            markerOption2.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
            Marker currentMarker = map.addMarker(markerOption2);


            MarkerOptions markerOption = new MarkerOptions().position(new LatLng(Double.valueOf(Constant.SELECTED_USER_LAT), Double.valueOf(Constant.SELECTED_USER_LNG)));
            markerOption.title("You are here");
            markerOption.icon(BitmapDescriptorFactory.fromBitmap(createDrawableFromView(NearByGoogleMapViewActivity.this, mIconMarker)));
//                                if (current_vehicle_location_marker.isEmpty()) {
            trackingMarker = map.addMarker(markerOption);


            // System.out.println("The start location is ::::::" + mStartLocation + " end location is :::::" + mEndLocation);


            setupCameraPositionForMovement(markerPos, secondPos);

        }

        private void setupCameraPositionForMovement(LatLng markerPos,
                                                    LatLng secondPos) {
            System.out.println("setup camera position called ::::");
            float bearing = bearingBetweenLatLngs(markerPos, secondPos);

//            trackingMarker = map.addMarker(new MarkerOptions().position(markerPos)
//                    .title("title")
//                    .snippet("snippet"));

            if (!(trackingMarker != null)) {

//                MarkerOptions markerOption2 = new MarkerOptions().position(new LatLng(UserLocationNearVehicleList.mSELCETED_NEARBY_VEHICLE_LAT, UserLocationNearVehicleList.mSELCETED_NEARBY_VEHICLE_LNG));
//                markerOption2.title(UserLocationNearVehicleList.mSELECTED_VEHICLE_SHORT_NAME);
//                markerOption2.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
//                Marker currentMarker = map.addMarker(markerOption2);


                MarkerOptions markerOption = new MarkerOptions().position(new LatLng(Double.valueOf(Constant.SELECTED_USER_LAT), Double.valueOf(Constant.SELECTED_USER_LNG)));
                markerOption.title(getResources().getString(R.string.you_are_here));
                markerOption.icon(BitmapDescriptorFactory.fromBitmap(createDrawableFromView(NearByGoogleMapViewActivity.this, mIconMarker)));
//                                if (current_vehicle_location_marker.isEmpty()) {
                trackingMarker = map.addMarker(markerOption);
            }


            CameraPosition cameraPosition;

//            if (m3Dmap) {
//                cameraPosition =
//                        new CameraPosition.Builder()
//                                .target(markerPos)
//                                .bearing(bearing + BEARING_OFFSET)
//                                .tilt(90)
//                                .zoom(map.getCameraPosition().zoom)
//                                .build();
//            } else {
            cameraPosition =
                    new CameraPosition.Builder()
                            .target(markerPos)

                            .tilt(90)
                            .zoom(map.getCameraPosition().zoom)
                            .build();
//            }


//            .zoom(history_zoom_level)

            map.animateCamera(
                    CameraUpdateFactory.newCameraPosition(cameraPosition),
                    ANIMATE_SPEEED_TURN,
                    new GoogleMap.CancelableCallback() {

                        @Override
                        public void onFinish() {
                            //System.out.println("finished camera");
                            animator.reset();
                            Handler handler = new Handler();
                            handler.post(animator);
                        }

                        @Override
                        public void onCancel() {
                            //System.out.println("cancelling camera");
                        }
                    }
            );
        }

        private Polyline polyLine;
//        private PolylineOptions rectOptions = new PolylineOptions();

        public void reset() {
            //System.out.println("Prabha reset called :::::::::::::");
            //    resetMarkers();
            start = SystemClock.uptimeMillis();

            // System.out.println("the current index in reset method is ::::" + currentIndex);
            endLatLng = getEndLatLng();
            beginLatLng = getBeginLatLng();

        }

        private Polyline initializePolyLine() {
//            System.out.println("Prabha initializePolyLine called :::::::::::::");
            //polyLinePoints = new ArrayList<LatLng>();
            PolylineOptions rectOptions = new PolylineOptions();
            //  System.out.println("The current index in polyline is :::" + currentIndex);

            rectOptions.add(mStartLocation);

            // System.out.println("The polyline start location is ::::is " + mStartLocation + " and end position is ::::::" + mEndLocation);

            //options.add(position);
            // System.out.println("The poly called ");
            rectOptions.color(getResources().getColor(R.color.history_polyline_color));

            return map.addPolyline(rectOptions);
        }

        /**
         * Add the marker to the polyline.
         */
        private void updatePolyLine(LatLng latLng) {
            // System.out.println("Prabha updatePolyLine called :::::::::::::");
            List<LatLng> points = polyLine.getPoints();
            points.add(latLng);
            //rectOptions.color(getResources().getColor(R.color.history_polyline_color));
            polyLine.setPoints(points);
        }


        public void stopAnimation() {
            //  isStopCalled = false;
            animator.stop();
        }

        public void startAnimation(boolean showPolyLine) {
            showPolyLine = false;
//            System.out.println("Start animation called :::::" + mStartLocation + " end location is ::::" + mEndLocation);
            if (mStartLocation != null && mEndLocation != null) {

                //System.out.println("Start animation geo points size " + geoPoints.size());
                // System.out.println("Start animation called :::::123454");
                if (mStartLocation != mEndLocation) {
                    // System.out.println("Prabha HIIIIIIIIIII");
//                    if (mCursorIndexSize == 0 || mCursorIndexSize == 1) {
                    if (!mCursorAlreadyInitialized) {
                        animator.initialize(showPolyLine);
                    } else {
                        moveVehicleWithoutInit();
                    }

//                    }
//                    else {
//
//                    }
                }
            } else if (mStartLocation != null && mEndLocation == null) {

            } else {

                System.out.println("Hi startAnimation map clear called ::::");

                if (map != null) {
                    map.clear();
                }
            }
        }


        public void moveVehicleWithoutInit() {
            endLatLng = getEndLatLng();
            beginLatLng = getBeginLatLng();


            start = SystemClock.uptimeMillis();

            LatLng begin = getBeginLatLng();
            LatLng end = getEndLatLng();

            float bearingL = bearingBetweenLatLngs(begin, end);

            // highLightMarker(currentIndex);
//            trackingMarker = map.addMarker(new MarkerOptions().position(mStartLocation)
//                    .title("title")
//                    .snippet("snippet"));

//            CameraPosition cameraPosition =
//                    new CameraPosition.Builder()
//                            .target(end) // changed this...
//                            .bearing(bearingL + BEARING_OFFSET)
//                            .tilt(tilt)
//                            .zoom(map.getCameraPosition().zoom)
//                            .build();

            CameraPosition cameraPosition;
//            if (m3Dmap) {
//                cameraPosition =
//                        new CameraPosition.Builder()
//                                .target(end) // changed this...
//                                .bearing(bearingL + BEARING_OFFSET)
//                                .tilt(tilt)
//                                .zoom(map.getCameraPosition().zoom)
//                                .build();
//            } else {
            cameraPosition =
                    new CameraPosition.Builder()
                            .target(end) // changed this...

                            .tilt(tilt)
                            .zoom(map.getCameraPosition().zoom)
                            .build();
//            }


            map.animateCamera(
                    CameraUpdateFactory.newCameraPosition(cameraPosition),
                    ANIMATE_SPEEED_TURN,
                    null
            );

            start = SystemClock.uptimeMillis();
            mHandler.postDelayed(animator, 100);
        }


        @Override
        public void run() {
            // System.out.println("Run called :::::");
//            System.out.println("Prabha run called :::::::::::::");
            long elapsed = SystemClock.uptimeMillis() - start;
            double t = interpolator.getInterpolation((float) elapsed / ANIMATE_SPEEED);

            //	LatLng endLatLng = getEndLatLng();
            //LatLng beginLatLng = getBeginLatLng();

//            System.out.println("The start latlng is :::::" + beginLatLng + " end latlng is :::::" + endLatLng);

            double lat = t * endLatLng.latitude + (1 - t) * beginLatLng.latitude;
            double lng = t * endLatLng.longitude + (1 - t) * beginLatLng.longitude;
            LatLng newPosition = new LatLng(lat, lng);
            // System.out.println("the new position is :::::" + newPosition);
            trackingMarker.setPosition(newPosition);

            if (showPolyline) {
                // System.out.println("Prabha updatePolyLine called :::::::::::::neew lat lng is ::::" + newPosition);
                updatePolyLine(newPosition);
            }

            // It's not possible to move the marker + center it through a cameraposition update while another camerapostioning was already happening.
            //navigateToPoint(newPosition,tilt,bearing,currentZoom,false);
            //navigateToPoint(newPosition,false);

            if (t < 1) {
                //System.out.println("Prabha t < 1 true called :::::::::::::");
                mHandler.postDelayed(this, 100);
            } else {

                // System.out.println("The previous start and end location in t>?>>>1 is :::" + mStartLocation + " :::" + mEndLocation);
                // System.out.println("Prabha t  >>>>>>> 1 else called :::::::::::::");
                mStartLocation = mEndLocation;
                // System.out.println("Move to next marker.... current = " + currentIndex + " and size = " + geoPoints.size());
                // imagine 5 elements -  0|1|2|3|4 currentindex must be smaller than 4
//                if (currentIndex < geoPoints.size() - 2) {
//
//                    System.out.println("Prabha currentIndex < geoPoints.size() - 2 called :::::::::::::");
//
//                    currentIndex++;
//
//                    endLatLng = getEndLatLng();
//                    beginLatLng = getBeginLatLng();
//
//
//                    start = SystemClock.uptimeMillis();
//
//                    LatLng begin = getBeginLatLng();
//                    LatLng end = getEndLatLng();
//
//                    float bearingL = bearingBetweenLatLngs(begin, end);
//
//                    highLightMarker(currentIndex);
//
//                    CameraPosition cameraPosition =
//                            new CameraPosition.Builder()
//                                    .target(end) // changed this...
//                                    .bearing(bearingL + BEARING_OFFSET)
//                                    .tilt(tilt)
//                                    .zoom(history_zoom_level)
//                                    .build();
//
//
//                    map.animateCamera(
//                            CameraUpdateFactory.newCameraPosition(cameraPosition),
//                            ANIMATE_SPEEED_TURN,
//                            null
//                    );
//
//                    start = SystemClock.uptimeMillis();
//                    mHandler.postDelayed(animator, 16);

//                } else {

                // System.out.println("the current index in anim else method is :::::" + currentIndex);

//                    currentIndex++;
//                    highLightMarker(currentIndex);
//                stopAnimation();
//                }

            }
        }


        private LatLng getEndLatLng() {


            return mEndLocation;
        }

        private LatLng getBeginLatLng() {


            return mStartLocation;
        }

        private void adjustCameraPosition() {
            //System.out.println("tilt = " + tilt);
            //System.out.println("upward = " + upward);
            //System.out.println("zoom = " + zoom);
            if (upward) {

                if (tilt < 90) {
                    tilt++;
                    zoom -= 0.01f;
                } else {
                    upward = false;
                }

            } else {
                if (tilt > 0) {
                    tilt--;
                    zoom += 0.01f;
                } else {
                    upward = true;
                }
            }
        }
    }

    ;

    private float bearingBetweenLatLngs(LatLng beginLatLng, LatLng endLatLng) {
        Location beginLocation = convertLatLngToLocation(mStartLocation);
        Location endLocation = convertLatLngToLocation(mEndLocation);
        return beginLocation.bearingTo(endLocation);
    }

    private Location convertLatLngToLocation(LatLng latLng) {
        Location location = new Location("someLoc");
        location.setLatitude(latLng.latitude);
        location.setLongitude(latLng.longitude);
        return location;
    }

    /**
     * Highlight the marker by index.
     */
    private void highLightMarker(int index) {
//        highLightMarker(markers.get(index));
    }

    /**
     * Highlight the marker by marker.
     */
//    private void highLightMarker(Marker marker) {

		/*
        for (Marker foundMarker : this.markers) {
			if (!foundMarker.equals(marker)) {
				foundMarker.setIcon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
			} else {
				foundMarker.setIcon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE));
				foundMarker.showInfoWindow();
			}
		}
		*/
//        marker.setIcon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE));
// marker.showInfoWindow();


//Utils.bounceMarker(googleMap, marker);

//        this.selectedMarker = marker;
//    }
    public void createDefaultPolyLine(JSONArray jArray) {
        PolylineOptions options = new PolylineOptions().width(8).color(getResources().getColor(R.color.history_polyline_color)).geodesic(true);
        try {

            if (jArray != null) {
                for (int i = 0; i < jArray.length(); i++) {
                    String latLngData = jArray.get(i).toString();
                    String[] str_msg_data_array = latLngData.split(",");

                    double mLat = Double.valueOf(str_msg_data_array[0]);
                    double mLng = Double.valueOf(str_msg_data_array[1]);
                    LatLng position = new LatLng(mLat, mLng);
                    options.add(position);

                }
            }
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }


        map.addPolyline(options);

    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();

//        startActivity(new Intent(NearByGoogleMapViewActivity.this, NearByVehicleListActivity.class));
//        finish();

        if (!Constant.mUserLocationNearbyActivityCalled) {
            startActivity(new Intent(NearByGoogleMapViewActivity.this, NearByVehicleListActivity.class));
            finish();
        } else {
            stopLocationUpdate();
            finish();
        }

    }

    @Override
    protected void onPause() {
        super.onPause();
        //Log.i("onPause", "The onPause() event");
//        cancelVehicleTracking();
        // handler.removeCallbacksAndMessages(null);
//        animator.stopAnimation();
        stopLocationUpdate();
        finish();
//        System.exit(0);

    }

    @Override
    protected void onResume() {
        super.onResume();
//        startActivity(new Intent(VehicleTrackingActivity.this, VehicleListActivity.class));
//        finish();

    }

    /**
     * Called when the activity is no longer visible.
     */
    @Override
    protected void onStop() {
        super.onStop();
//        cancelVehicleTracking();
//        animator.stopAnimation();
        stopLocationUpdate();

    }

    private void screenArrange() {
        // TODO Auto-generated method stub
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        height = displayMetrics.heightPixels;
        width = displayMetrics.widthPixels;

        LinearLayout.LayoutParams backImageParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        backImageParams.width = width * 15 / 100;
        backImageParams.height = height * 10 / 100;
        backImageParams.gravity = Gravity.CENTER;
        $ImageBack.setLayoutParams(backImageParams);
        $ImageBack.setPadding(width * 1 / 100, height * 1 / 100,
                width * 1 / 100, height * 1 / 100);

        LinearLayout.LayoutParams headTxtParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        headTxtParams.width = width * 85 / 100;
        headTxtParams.height = height * 10 / 100;
        mTxtHeader.setLayoutParams(headTxtParams);
        mTxtHeader.setPadding(width * 2 / 100, 0, 0, 0);
        mTxtHeader.setGravity(Gravity.CENTER | Gravity.LEFT);

        LinearLayout.LayoutParams spinnerParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        spinnerParams.width = width;
        spinnerParams.height = height * 7 / 100;
        spinnerParams.gravity = Gravity.CENTER | Gravity.TOP;
        spinnerParams.topMargin = (int) (height * 0.6 / 100);
//        $SpinnerNavigation.setLayoutParams(spinnerParams);

        FrameLayout.LayoutParams imgeChangeView = new FrameLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        imgeChangeView.width = width * 10 / 100;
        imgeChangeView.height = height * 15 / 100;
        imgeChangeView.gravity = Gravity.TOP | Gravity.RIGHT;
        imgeChangeView.rightMargin = (int) (width * 0.5 / 100);
        $ChangeView.setLayoutParams(imgeChangeView);

        if (width >= 600) {
            mTxtHeader.setTextSize(18);
        } else if (width > 501 && width < 600) {
            mTxtHeader.setTextSize(17);
        } else if (width > 260 && width < 500) {
            mTxtHeader.setTextSize(16);
        } else if (width <= 260) {
            mTxtHeader.setTextSize(15);
        }
    }

}
