package com.vamosys.vamos;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.SQLException;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.vamosys.adapter.KMSSummaryAdapter;
import com.vamosys.model.ExecutiveReportData;
import com.vamosys.model.ExecutiveReportDataEnv;
import com.vamosys.utils.ConnectionDetector;
import com.vamosys.utils.Constant;
import com.vamosys.utils.DaoHandler;
import com.vamosys.utils.TypefaceUtil;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;

public class KMSSummary extends AppCompatActivity {
    ListView lv;
    TextView fromdate, todate, fromdatevalue, todatevalue, mHeadTitle;
    EditText mEdtSearch;
    Spinner groupSpinner;
    View v1, v2, v3;
    TextView mTxtNoRecord;
    ImageView mBackArrow;

    List<ExecutiveReportData> mKmsSummaryList = new ArrayList<ExecutiveReportData>();
    List<ExecutiveReportData> mKmsSummaryAdapterList = new ArrayList<ExecutiveReportData>();
    List<String> mGroupList = new ArrayList<String>();
    List<String> mGroupSpinnerList = new ArrayList<String>();
    String mSelectedGroup, mUserId;
    //List<String> mGroupList;
    //String mSelectedGroup;
    ConnectionDetector cd;
    SharedPreferences sp;

    //    List<KMSSummary>
    private Toolbar mToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.kms_summary_main);
        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mToolbar.setTitle(getResources().getString(R.string.kms_summary));
        mToolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_action_back));
        setSupportActionBar(mToolbar);

        init();
        screenArrange();
        sp = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        cd = new ConnectionDetector(getApplicationContext());

        if (sp.getString("ip_adds", "") != null) {
            if (sp.getString("ip_adds", "").trim().length() > 0) {
                Const.API_URL = sp.getString("ip_adds", "");
            }
        }

        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.MONTH, -1);
        Date result = cal.getTime();

        SimpleDateFormat dfDate = new SimpleDateFormat("dd-MMM-yyyy");

        String mCurrentDate = dfDate.format(new Date());
        String mFromDate = dfDate.format(result);
        fromdatevalue.setText(mFromDate);
        todatevalue.setText(mCurrentDate);
//        float l1 =
//        System.out.println("the distance is :::" + distFrom((float) 13.0709714, (float) 80.1968834, (float) 12.9064699, (float) 80.0939879));
        try {
            queryUserDB();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //What to do on back clicked

                startActivity(new Intent(KMSSummary.this, VehicleListActivity.class));
                finish();


            }
        });


        mBackArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(KMSSummary.this, VehicleListActivity.class));
                finish();
            }
        });

        groupSpinner
                .setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                    @Override
                    public void onItemSelected(AdapterView<?> parent,
                                               View view, int pos, long id) {
                        // TODO Auto-generated method stub
                        mSelectedGroup = mGroupList.get(pos);
                        saveSelectedGroup(mGroupSpinnerList.get(pos));
                        if (mSelectedGroup.equalsIgnoreCase("Select")) {

                        } else {
                            mEdtSearch.setText("");
                            ConnectionDetector cd = new ConnectionDetector(getApplicationContext());
                            if (cd.isConnectingToInternet()) {
                                mKmsSummaryList.clear();
                                new getKmsSummaryData().execute();
                            } else {
                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.network_connection),
                                        Toast.LENGTH_SHORT).show();
                            }
                        }

                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> arg0) {
                        // TODO Auto-generated method stub

                    }

                });


        /**
         * Enabling Search Filter
         * */
        mEdtSearch.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence cs, int arg1, int arg2,
                                      int arg3) {
                // When user changed the Text
                // DailySearch.this.arrayAutoListAdapter.getFilter().filter(cs);
                if (cs.length() > 0) {
                    getSearchText(String.valueOf(cs).trim());
                } else {
                    // setData("ALL");
                    getSearchText(null);
                }
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1,
                                          int arg2, int arg3) {
                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub
            }

        });


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_home, menu);


        MenuItem myActionMenuItem = menu.findItem(R.id.search);
        final SearchView searchView = (SearchView) myActionMenuItem.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (TextUtils.isEmpty(newText)) {
                    // adapter.filter("");
                    //  listView.clearTextFilter();
                    getSearchText(null);
                } else {
                    // adapter.filter(newText);


                    getSearchText(String.valueOf(newText).trim());


                }
                return true;
            }
        });

        return true;


    }


    private void saveSelectedGroup(String mGroupName) {
        try {
            SharedPreferences.Editor editor = sp.edit();
            editor.putString("selected_group", mGroupName);
            editor.commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void getSearchText(String ch) {

//        System.out.println("List size is ::::::" + mVehicleLocationListAdapter.size()
//                + " and the character is :::::" + ch);

        List<ExecutiveReportData> list = new ArrayList<ExecutiveReportData>();
        // list = null;
        if (ch != null && ch.length() > 0) {


            list.clear();

            for (int i = 0; i < mKmsSummaryList.size(); i++) {
                ExecutiveReportData dc = new ExecutiveReportData();


                if (Pattern
                        .compile(Pattern.quote(ch), Pattern.CASE_INSENSITIVE)
                        .matcher(mKmsSummaryList.get(i).getShortName())
                        .find()) {
                    //  System.out.println("Hiiiiiii");
                    dc = mKmsSummaryList.get(i);
                    list.add(dc);
                }

            }

        } else {
            list = mKmsSummaryList;
        }

        // System.out.println("The searched list size is :::::" + list.size());


//        System.out.println("List size after filtered is :::::"
//                + mVehicleLocationListAdapter.size());

        if (list.size() > 0) {
            mKmsSummaryAdapterList = list;


            if (mKmsSummaryAdapterList.size() > 0) {

                KMSSummaryAdapter notiAdapter = new KMSSummaryAdapter(KMSSummary.this, mKmsSummaryAdapterList);
//                lv.setAdapter(new NotificationListAdapter(
//                        NotificationListActivity.this, Constant.mPushDataList));

                lv.setAdapter(notiAdapter);
                notiAdapter.notifyDataSetChanged();

                //   lv.setVisibility(View.VISIBLE);

                mTxtNoRecord.setVisibility(View.GONE);
                lv.setVisibility(View.VISIBLE);

                //  mTxtView.setVisibility(View.GONE);
                //  mSelectAllLayout.setVisibility(View.VISIBLE);
            } else {
                lv.setVisibility(View.GONE);
                //mTxtView.setVisibility(View.VISIBLE);
                // mSelectAllLayout.setVisibility(View.GONE);
                mTxtNoRecord.setVisibility(View.VISIBLE);
                lv.setVisibility(View.GONE);
            }


        } else {
//    Set no record data
            List<ExecutiveReportData> vList = new ArrayList<ExecutiveReportData>();
            mKmsSummaryAdapterList = vList;
            if (mKmsSummaryAdapterList.size() > 0) {

                KMSSummaryAdapter notiAdapter = new KMSSummaryAdapter(KMSSummary.this, mKmsSummaryAdapterList);
//                lv.setAdapter(new NotificationListAdapter(
//                        NotificationListActivity.this, Constant.mPushDataList));

                lv.setAdapter(notiAdapter);
                notiAdapter.notifyDataSetChanged();

//                lv.setVisibility(View.VISIBLE);
                //  mTxtView.setVisibility(View.GONE);
                mTxtNoRecord.setVisibility(View.GONE);
                lv.setVisibility(View.VISIBLE);
                //  mSelectAllLayout.setVisibility(View.VISIBLE);
            } else {
//                lv.setVisibility(View.GONE);

                mTxtNoRecord.setVisibility(View.VISIBLE);
                lv.setVisibility(View.GONE);
                // mTxtView.setVisibility(View.VISIBLE);
                // mSelectAllLayout.setVisibility(View.GONE);
            }
            // setData("No Record");

        }
        // $SearchListView.setAdapter(new DailySearchAdapter(DailySearch.this,
        // mFilteredListDailySearch));


    }


    public void setDropDownData() {
        //mGroupList = new ArrayList<String>();
        // List<String> list = new ArrayList<String>();
        // list.add("Select");
        if (mGroupList.size() > 0) {

        } else {
            mGroupList.add("Select");
        }


        for (int i = 0; i < mGroupList.size(); i++) {

            String[] str_msg_data_array = mGroupList.get(i).split(":");

            mGroupSpinnerList.add(str_msg_data_array[0]);

        }


        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(
                KMSSummary.this, R.layout.spinner_row,
                mGroupSpinnerList) {

            public View getView(int position, View convertView, ViewGroup parent) {
                View v = super.getView(position, convertView, parent);
                // TextView v = (TextView) super.getView(position,
                // convertView,parent);
                // Constant.face(DailyCalls.this, (TextView) v);
                return v;
            }

            public View getDropDownView(int position, View convertView,
                                        ViewGroup parent) {
                View v = super.getDropDownView(position, convertView, parent);
                // ((TextView)
                // v).setBackgroundColor(Color.parseColor("#BBfef3da"));
                // TextView v = (TextView) super.getView(position,
                // convertView,parent);
                // Constant.face(DailyCalls.this, (TextView) v);
                return v;
            }
        };
        spinnerArrayAdapter
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        groupSpinner.setAdapter(spinnerArrayAdapter);
        String groupName = sp.getString("selected_group", "");
        int spinnerPosition = spinnerArrayAdapter.getPosition(groupName);

//set the default according to value
        groupSpinner.setSelection(spinnerPosition);
    }

    public void setData() {
        mKmsSummaryAdapterList = mKmsSummaryList;
        if (mKmsSummaryAdapterList.size() > 0) {
            mTxtNoRecord.setVisibility(View.GONE);
            lv.setVisibility(View.VISIBLE);
            KMSSummaryAdapter adapter = new KMSSummaryAdapter(KMSSummary.this,
                    mKmsSummaryAdapterList);
            lv.setAdapter(adapter);
        } else {
            mTxtNoRecord.setVisibility(View.VISIBLE);
            lv.setVisibility(View.GONE);
        }
    }

    private class getKmsSummaryData extends AsyncTask<String, String, String> {
        ProgressDialog progressDialog;

        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub

            String result = null;
            try {
                Const constaccess = new Const();
                /*
New API
 */
                result = constaccess
                        .sendGet(Const.API_URL + "/mobile/getKmsSummary?userId=" + mUserId + "&groupId="
                                + mSelectedGroup, 30000);
                //  + "&fromDate=2016-01-05&toDate=2016-01-30", 30000);
                        /*
                        The below is old API
                         */
//                result = constaccess
//                        .sendGet("http://vamosys.com/mobile/getExecutiveReport?userId=" + mUserId + "&groupId="
//                                + mSelectedGroup
//                                + "&fromDate=2016-01-05&toDate=2016-01-30", 30000);
                // result = ht.doPostEcpl(mLoginData, "www.test.com");
                // Constant.printMsg("Response........" + result);
            } catch (Exception e) {

            }

            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            progressDialog.dismiss();
            if (result != null && result.length() > 0) {
                System.out.println("The kms result is ::::" + result);
                ExecutiveReportDataEnv kmsResponse;

                Gson g = new Gson();

                kmsResponse = g.fromJson(result.trim(),
                        ExecutiveReportDataEnv.class);
                if (kmsResponse.getExecReportData() != null) {

                    mKmsSummaryList = new ArrayList<ExecutiveReportData>(
                            Arrays.asList(kmsResponse.getExecReportData()));

                }
                setData();
            }

        }

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            progressDialog = new ProgressDialog(KMSSummary.this,
                    AlertDialog.THEME_HOLO_LIGHT);
            progressDialog.setMessage(getResources().getString(R.string.progress_dialog));
            progressDialog.setProgressDrawable(new ColorDrawable(
                    android.graphics.Color.BLUE));
            progressDialog.setCancelable(true);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.show();
        }

    }

//    public void queryUserDB() {
//        DataBaseHandler db = new DataBaseHandler(this);
//        Cursor c = null;
//        // String qry = "SELECT DISTINCT patch_id,patch_name,mtp_id FROM "
//        // + DbHandler.TABLE_TM_DCP;
//        try {
//            // c = db.open().getDatabaseObj().rawQuery(qry, null);
//            c = db.open()
//                    .getDatabaseObj()
//                    .query(DataBaseHandler.TABLE_USER, null, null, null, null, null,
//                            null);
//            // int reporting_date_index = c
//            // .getColumnIndex("actual_reporting_date");
//            int user_id_index = c.getColumnIndex("user_id");
//            int group_list_index = c.getColumnIndex("group_list");
//            // int mtp_id_index = c.getColumnIndex("mtp_id");
//            // int dr_ch_name_index = c.getColumnIndex("dr_ch_name");
//            // System.out.println("The coubnt of daily count is ::::::"
//            // + c.getCount());
//            if (c.getCount() > 0) {
//
//                while (c.moveToNext()) {
//                    mUserId = c.getString(user_id_index);
//
////                    System.out.println("The group list is :::::"
////                            + c.getString(group_list_index));
//
//                    if (c.getString(group_list_index) != null) {
//                        Gson g = new Gson();
//                        InputStream is = new ByteArrayInputStream(c.getString(
//                                group_list_index).getBytes());
//                        Reader reader = new InputStreamReader(is);
//                        Type fooType = new TypeToken<List<String>>() {
//                        }.getType();
//
//                        mGroupList = g.fromJson(reader, fooType);
//                    }
//
//                }
//            }
//
//        } finally {
//            c.close();
//            db.close();
//        }
//        setDropDownData();
//    }

    public void queryUserDB() {
        DaoHandler da = new DaoHandler(getApplicationContext(), false);
        da.queryUserDB();

        mUserId = Constant.mDbUserId;
        mGroupList = Constant.mUserGroupList;
        setDropDownData();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        startActivity(new Intent(KMSSummary.this, VehicleListActivity.class));
        finish();

    }

    private void screenArrange() {
        // TODO Auto-generated method stub

		/* screen arrangements */
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        int height = metrics.heightPixels;
        int width = metrics.widthPixels;
        Constant.ScreenWidth = width;
        Constant.ScreenHeight = height;

        LinearLayout.LayoutParams backImageParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        backImageParams.width = width * 15 / 100;
        backImageParams.height = height * 10 / 100;
        backImageParams.gravity = Gravity.CENTER;
        mBackArrow.setLayoutParams(backImageParams);
        mBackArrow.setPadding(width * 1 / 100, height * 1 / 100, width * 1 / 100, height * 1 / 100);

        LinearLayout.LayoutParams headTxtParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        headTxtParams.width = width * 85 / 100;
        headTxtParams.height = height * 10 / 100;
        mHeadTitle.setLayoutParams(headTxtParams);
        mHeadTitle.setPadding(width * 2 / 100, 0, 0, 0);
        mHeadTitle.setGravity(Gravity.CENTER | Gravity.LEFT);

        LinearLayout.LayoutParams textParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        textParams.width = width * 45 / 100;
        textParams.height = height * 6 / 100;
        textParams.topMargin = (int) (height * 0.25 / 100);
        textParams.leftMargin = width * 2 / 100;
        textParams.rightMargin = width * 2 / 100;
        fromdate.setLayoutParams(textParams);
        todate.setLayoutParams(textParams);
        fromdate.setGravity(Gravity.CENTER);
        todate.setGravity(Gravity.CENTER);

        LinearLayout.LayoutParams textValueParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        textValueParams.width = width * 45 / 100;
        textValueParams.height = height * 6 / 100;
        textValueParams.leftMargin = width * 2 / 100;
        textValueParams.rightMargin = width * 2 / 100;
        textValueParams.bottomMargin = (int) (height * 0.25 / 100);
        fromdatevalue.setLayoutParams(textValueParams);
        todatevalue.setLayoutParams(textValueParams);
        fromdatevalue.setGravity(Gravity.CENTER);
        todatevalue.setGravity(Gravity.CENTER);

        LinearLayout.LayoutParams ViewParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        ViewParams.width = width * 1 / 100;
        ViewParams.height = LinearLayout.LayoutParams.MATCH_PARENT;
        v1.setLayoutParams(ViewParams);
        v2.setLayoutParams(ViewParams);

        LinearLayout.LayoutParams viewParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        viewParams.width = width;
        viewParams.height = height * 1 / 2 / 100;
        v3.setLayoutParams(viewParams);

        LinearLayout.LayoutParams spinnerParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        spinnerParams.width = width * 95 / 100;
        spinnerParams.height = height * 8 / 100;
        spinnerParams.topMargin = (int) (height * 1.25 / 100);
        spinnerParams.leftMargin = width * 2 / 100;
        spinnerParams.rightMargin = width * 2 / 100;
        spinnerParams.bottomMargin = (int) (height * 0.25 / 100);
        groupSpinner.setLayoutParams(spinnerParams);

        LinearLayout.LayoutParams lineParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        lineParams.width = width * 98 / 100;
        lineParams.height = LinearLayout.LayoutParams.WRAP_CONTENT;
        lineParams.topMargin = height * 2 / 100;
        lineParams.bottomMargin = height * 1 / 100;
        lineParams.leftMargin = width * 1 / 100;
        lineParams.rightMargin = width * 1 / 100;
        lv.setLayoutParams(lineParams);

        LinearLayout.LayoutParams headTxtParams1 = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        headTxtParams1.width = width * 76 / 100;
        headTxtParams1.height = height * 8 / 100;
        headTxtParams1.setMargins(width * 4 / 100, width * 2 / 100, 1, width * 1 / 100);
//        headTxtParams1.setMargins();
//        $TxtTitle.setLayoutParams(headTxtParams);
//        $TxtTitle.setPadding(width * 2 / 100, 0, 0, 0);
//        $TxtTitle.setGravity(Gravity.CENTER);
        mEdtSearch.setLayoutParams(headTxtParams1);
//        mEdtSearch.setm
        mEdtSearch.setPadding(width * 2 / 100, 0, width * 4 / 100, 0);
        mEdtSearch.setGravity(Gravity.CENTER | Gravity.CENTER);


        if (width >= 600) {
            fromdate.setTextSize(16);
            todate.setTextSize(16);
            fromdatevalue.setTextSize(16);
            todatevalue.setTextSize(16);
            mTxtNoRecord.setTextSize(18);
            mHeadTitle.setTextSize(18);
            mEdtSearch.setTextSize(18);
        } else if (width > 501 && width < 600) {
            fromdate.setTextSize(15);
            todate.setTextSize(15);
            fromdatevalue.setTextSize(15);
            todatevalue.setTextSize(15);
            mTxtNoRecord.setTextSize(17);
            mHeadTitle.setTextSize(17);
            mEdtSearch.setTextSize(17);
        } else if (width > 260 && width < 500) {
            fromdate.setTextSize(14);
            todate.setTextSize(14);
            fromdatevalue.setTextSize(14);
            todatevalue.setTextSize(14);
            mTxtNoRecord.setTextSize(16);
            mHeadTitle.setTextSize(16);
            mEdtSearch.setTextSize(16);
        } else if (width <= 260) {
            fromdate.setTextSize(13);
            todate.setTextSize(13);
            fromdatevalue.setTextSize(13);
            todatevalue.setTextSize(13);
            mTxtNoRecord.setTextSize(15);
            mHeadTitle.setTextSize(15);
            mEdtSearch.setTextSize(15);
        }
    }
//	public void insertData(ContentValues cv) throws SQLException {
//		DbHandler db = new DbHandler(getApplicationContext());
//		try {
//			int c = (int) db.open().getDatabaseObj()
//					.insert(DbHandler.TABLE_LOCATION, null, cv);
//
//			System.out.println("No of inserted rows is :::::::" + c);
//		} finally {
//
//			db.close();
//		}
//	}

    private void init() {
        // TODO Auto-generated method stub
        lv = (ListView) findViewById(R.id.from_to_list);
        fromdate = (TextView) findViewById(R.id.from_date_text);
        todate = (TextView) findViewById(R.id.to_date_text);
        fromdatevalue = (TextView) findViewById(R.id.from_date_value);
        todatevalue = (TextView) findViewById(R.id.to_date_value);
        groupSpinner = (Spinner) findViewById(R.id.groupspinner);
        v1 = (View) findViewById(R.id.view_vertical);
        v2 = (View) findViewById(R.id.view_vertical1);
        v3 = (View) findViewById(R.id.view_horizontal);
        mTxtNoRecord = (TextView) findViewById(R.id.kms_no_record_txt);
        mBackArrow = (ImageView) findViewById(R.id.noti_list_view_Back);
        mHeadTitle = (TextView) findViewById(R.id.tvTitle);
        mEdtSearch = (EditText) findViewById(R.id.kms_list_search_activity);

        fromdate.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        todate.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        fromdatevalue.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        todatevalue.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        mTxtNoRecord.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        mHeadTitle.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        mEdtSearch.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
    }


    public static float distFrom(float lat1, float lng1, float lat2, float lng2) {
        double earthRadius = 6371000; //meters
        double dLat = Math.toRadians(lat2 - lat1);
        double dLng = Math.toRadians(lng2 - lng1);
        double a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
                Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2)) *
                        Math.sin(dLng / 2) * Math.sin(dLng / 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        float dist = (float) (earthRadius * c);

        return dist;
    }

}
