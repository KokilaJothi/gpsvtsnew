package com.vamosys.vamos;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.vamosys.imageLoader.ImageLoader;
import com.vamosys.utils.TypefaceUtil;

import java.io.InputStream;

public class NotificationViewerActivity extends Activity {

    TextView mTxtMsgContent;
    com.vamosys.utils.ScaleImageView mImgLogo;
    ImageView mImgBack;
    //Button mBtnOk;
    SharedPreferences sp;

    public static String mMsgContent = null;
    ImageLoader imgLoader;
    String imgUrl = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification_viewer);
        /**Shared Preference to access local data*/
        sp = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        imgUrl = sp.getString("app_logo", "");
        imgLoader = new ImageLoader(NotificationViewerActivity.this);
        init();
        String appLogoUrl = sp.getString("app_logo", null);

        if (sp.getString("ip_adds", "") != null) {
            if (sp.getString("ip_adds", "").trim().length() > 0) {
                Const.API_URL = sp.getString("ip_adds", "");
            }
        }
        // if(ConnectionDetector.isConnectingToInternet)
//        new getAppLogo().execute(appLogoUrl);

//        CommonManager co = new CommonManager(getApplicationContext());
//        mImgLogo.setImageBitmap(co.getBitmap());

        imgLoader.DisplayImage(imgUrl, mImgLogo);

        mImgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(NotificationViewerActivity.this, NotificationListActivity.class));
                finish();
            }
        });
    }

    public void init() {
        mTxtMsgContent = (TextView) findViewById(R.id.push_msg_content_txt);
        mImgLogo = (com.vamosys.utils.ScaleImageView) findViewById(R.id.mainlogo);
//mBtnOk=(Button)findViewById(R.id.push_ok);
        mImgBack = (ImageView) findViewById(R.id.noti_ms_view_Back);
        mTxtMsgContent.setText(mMsgContent);
        mTxtMsgContent.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
    }

    private class getAppLogo extends AsyncTask<String, Void, Bitmap> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Bitmap doInBackground(String... URL) {
            String imageURL = URL[0];
            Bitmap bitmap = null;
            try {
                // Download Image from URL
                InputStream input = new java.net.URL(imageURL).openStream();
                // Decode Bitmap
                bitmap = BitmapFactory.decodeStream(input);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return bitmap;
        }

        @Override
        protected void onPostExecute(Bitmap result) {
            // Set the bitmap into app logo
            mImgLogo.setImageBitmap(result);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        startActivity(new Intent(NotificationViewerActivity.this, VehicleListActivity.class));
        finish();

    }

}
