package com.vamosys.vamos;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.preference.PreferenceManager;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.vamosys.adapter.NotificationListAdapter;
import com.vamosys.app.MyApplication;
import com.vamosys.model.DataBaseHandler;
import com.vamosys.model.PushNotificationDto;
import com.vamosys.notification.NotificationUtils;
import com.vamosys.utils.CommonManager;
import com.vamosys.utils.Constant;
import com.vamosys.utils.DaoHandler;
import com.vamosys.utils.TypefaceUtil;

import org.osmdroid.api.IMapController;
import org.osmdroid.config.Configuration;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.infowindow.InfoWindow;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

public class NotificationListActivity extends FragmentActivity implements OnMapReadyCallback,SwipeRefreshLayout.OnRefreshListener  {

    ListView lv;
    ImageView mImgBack, mImgDelete, mImgRefresh;
    public static List<String> mMsgList = new ArrayList<String>();

    TextView mTxtView, mTxtHead,selectall;
    CheckBox mChkSelectAll;
    LinearLayout mSelectAllLayout, mPushListLayout, mPushMapLayout;
    int width, height;
    private GoogleMap map;
    ImageView $ChangeView;
    float vehicle_zoom_level = 15.5F;
    double mLatitude = 0, mLongitude = 0;
    String mSelectedMsg = null;
    String mSELECTED_MAP_TYPE = "Normal";
    EditText mEdtSearch;
    SharedPreferences sp;
    //OSM related
    MapView mapview;
    LinearLayout mGoogleMapLayout;
    Boolean login_status=false;

    //    MapView mMapViewNew;
    private IMapController mapController;
    boolean isOsmEnabled = true;
    private SwipeRefreshLayout swipeRefreshLayout;
    //    List<PushNotificationDto> mPushDataListAdapter = new ArrayList<PushNotificationDto>();
    DBHelper dbhelper;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Context ctx = getApplicationContext();
        //important! set your user agent to prevent getting banned from the osm servers
        Configuration.getInstance().load(ctx, PreferenceManager.getDefaultSharedPreferences(ctx));

        setContentView(R.layout.activity_notification_list);

        sp = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        if (sp.getString("enabled_map", "") != null) {
            if (sp.getString("enabled_map", "").trim().length() > 0) {

//                System.out.println("hi enabled map is " + sp.getString("enabled_map", ""));

                if (sp.getString("enabled_map", "").equalsIgnoreCase(getResources().getString(R.string.osm))) {
                    isOsmEnabled = true;
                } else {
                    isOsmEnabled = false;
                }
            }
        }

        init();

        screenArrange();
        dbSetup(getApplicationContext());
        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);


//        Cursor login_status_cur = dbhelper.get_std_table_info("login_status");
//        login_status_cur.moveToFirst();
//        boolean login_status = login_status_cur.getInt(0) > 0;
//        login_status_cur.close();
        login_status=sp.getBoolean("login_status",false);
        if (login_status) {
            checkPushList();
        } else {
            startActivity(new Intent(NotificationListActivity.this, RegistrationPage.class));
            finish();
        }



        NotificationUtils.clearNotifications();
        MyApplication.getInstance().getPrefManager().updateNotificationsNull();

        mImgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (mPushListLayout.getVisibility() == View.VISIBLE) {
                    startActivity(new Intent(NotificationListActivity.this,
                            VehicleListActivity.class));
//                    mPushListLayout.setVisibility(View.VISIBLE);
//                    mPushMapLayout.setVisibility(View.GONE);
                    finish();
                } else {
                    checkPushList();
                    mPushListLayout.setVisibility(View.VISIBLE);
                    mPushMapLayout.setVisibility(View.GONE);
                }


            }
        });

        mImgDelete.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (checkMsgSelected() > 0) {
                    deleteMsgData();
                } else {
                    Toast.makeText(getApplicationContext(),
                            ""+getResources().getString(R.string.message_to_delete),
                            Toast.LENGTH_SHORT).show();
                }
            }
        });

        mImgRefresh.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                checkPushList();
            }
        });

        mChkSelectAll
                .setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

                    @Override
                    public void onCheckedChanged(CompoundButton buttonView,
                                                 boolean isChecked) {
                        updateListCheckStatus(isChecked);
                    }
                });

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View v, int pos,
                                    long arg3) {
                // TODO Auto-generated method stub
                // PendingShopListDto obj = (PendingShopListDto)
                // arg0.getAdapter()
                // .getItem(pos);
                // String id = obj.getId();
                mChkSelectAll.setChecked(false);
                if (!Constant.mPushDataListAdapter.get(pos).getMsgContent().contains("GeoCode:")) {
                    Log.d("geocodee","success");
                    NotificationViewerActivity.mMsgContent = Constant.mPushDataListAdapter.get(pos).getMsgContent();
                    Intent i = new Intent(NotificationListActivity.this,
                            NotificationViewerActivity.class);
                    i.putExtra("type", "manual");
                    startActivity(i);
                    finish();
                } else {
                    Log.d("geocodee","failure");
                    mPushListLayout.setVisibility(View.GONE);
                    mPushMapLayout.setVisibility(View.VISIBLE);

                    String mMsgData = Constant.mPushDataListAdapter.get(pos).getMsgContent();
                    //String mLatLngData = mMsgData.substring(mMsgData.lastIndexOf("GeoCode:") + 1);

                    System.out.println("the location alert data is :::" + Constant.mPushDataListAdapter.get(pos).getMsgContent());
                    String[] str_msg_data_array = mMsgData.split("GeoCode:");
//int a=str_msg_data_array.length;
                    mSelectedMsg = str_msg_data_array[0];

                    if (str_msg_data_array.length > 1) {
                        String mLatLngData = str_msg_data_array[1];
                        //  System.out.println("The lat lng data is ::::" + mLatLngData);
                        String[] str_array = mLatLngData.split(",");

                        // System.out.println("The lat str data is ::::" + str_array[0].trim());
                        // System.out.println("The  lng str data is ::::" + str_array[1]);

                        mLatitude = Double.valueOf(str_array[0].trim());
                        mLongitude = Double.valueOf(str_array[1].trim());

                        // System.out.println("The lat double data is ::::" + mLatitude);
                        // System.out.println("The  lng double data is ::::" + mLongitude);

                        if (isOsmEnabled) {
                            setupOsmMap();
                        } else {
                            setupGoogleMap();
                        }



                    }


                }
//				startActivity(new Intent(PushMessageListActivity.this,
//						PushMessageActivity.class));


            }
        });

        $ChangeView.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                opptionPopUp();
            }
        });


        /**
         * Enabling Search Filter
         * */
        mEdtSearch.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence cs, int arg1, int arg2,
                                      int arg3) {
                // When user changed the Text
                // DailySearch.this.arrayAutoListAdapter.getFilter().filter(cs);
                if (cs.length() > 0) {
                    Log.d("searchedit","success");
                    getSearchText(String.valueOf(cs).trim());
                } else {
                    // setData("ALL");
                    Log.d("searchedit","failure");
                    getSearchText(null);
                }
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1,
                                          int arg2, int arg3) {
                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub
            }

        });
    }


    void dbSetup(Context context) {
        dbhelper = new DBHelper(context);
        try {
            dbhelper.createDataBase();
            dbhelper.openDataBase();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void init() {
//        map = ((MapFragment) getFragmentManager().findFragmentById(R.id.push_data_map)).getMap();
//        map.setPadding(1, 1, 1, 150);
//        map.getUiSettings().setZoomControlsEnabled(true);

//        FragmentManager myFragmentManager = getSupportFragmentManager();
//        SupportMapFragment myMapFragment = (SupportMapFragment) myFragmentManager
//                .findFragmentById(R.id.push_data_map);
//        myMapFragment.getMapAsync(this);


        $ChangeView = (ImageView) findViewById(R.id.push_data_map_ViewIcon);

        lv = (ListView) findViewById(R.id.push_list_details);
        mImgBack = (ImageView) findViewById(R.id.noti_list_view_Back);
        mTxtHead = (TextView) findViewById(R.id.tvTitle);
        mImgRefresh = (ImageView) findViewById(R.id.noti_list_img_refresh);
        mTxtView = (TextView) findViewById(R.id.no_record_txtview);
        selectall = (TextView) findViewById(R.id.select_all);
        mImgDelete = (ImageView) findViewById(R.id.noti_img_delete);
        mChkSelectAll = (CheckBox) findViewById(R.id.noti_chk_select_all);
        mSelectAllLayout = (LinearLayout) findViewById(R.id.select_all_layout);
        mPushListLayout = (LinearLayout) findViewById(R.id.push_data_list_layout);
        mPushMapLayout = (LinearLayout) findViewById(R.id.push_data_map_view_layout);
        mPushListLayout.setVisibility(View.VISIBLE);
        mPushMapLayout.setVisibility(View.GONE);
        mEdtSearch = (EditText) findViewById(R.id.notification_list_search_edt);
        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_layout);
        swipeRefreshLayout.setOnRefreshListener(this);

        mTxtHead.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        mTxtView.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
        mEdtSearch.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));

        mGoogleMapLayout = (LinearLayout) findViewById(R.id.google_map_layout);
        RelativeLayout rl = (RelativeLayout) findViewById(R.id.map_view_relativelayout);
        if (isOsmEnabled) {
            $ChangeView.setVisibility(View.GONE);
            mGoogleMapLayout.setVisibility(View.GONE);
            rl.setVisibility(View.VISIBLE);
            mapview = new MapView(this);
            mapview.setTilesScaledToDpi(true);
            rl.addView(mapview, new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.FILL_PARENT,
                    RelativeLayout.LayoutParams.FILL_PARENT));
//        mapview.setTileSource(TileSourceFactory.MAPNIK);
            mapview.setBuiltInZoomControls(false);
            mapview.setMultiTouchControls(true);
            mapController = mapview.getController();
            mapController.setZoom(8);
            mapview.getOverlays().clear();
            mapview.invalidate();
            mapview.getOverlays().add(Constant.getTilesOverlay(getApplicationContext()));
        } else {
            $ChangeView.setVisibility(View.VISIBLE);
            mGoogleMapLayout.setVisibility(View.VISIBLE);
            rl.setVisibility(View.GONE);
            FragmentManager myFragmentManager = getSupportFragmentManager();
            SupportMapFragment myMapFragment = (SupportMapFragment) myFragmentManager
                    .findFragmentById(R.id.push_data_map);
            myMapFragment.getMapAsync(this);
        }


    }

    public void getSearchText(String ch) {

//        System.out.println("List size is ::::::" + mVehicleLocationListAdapter.size()
//                + " and the character is :::::" + ch);

        List<PushNotificationDto> list = new ArrayList<PushNotificationDto>();
        // list = null;
        if (ch != null && ch.length() > 0) {


            list.clear();

            for (int i = 0; i < Constant.mPushDataList.size(); i++) {
                PushNotificationDto dc = new PushNotificationDto();


                if (Pattern
                        .compile(Pattern.quote(ch), Pattern.CASE_INSENSITIVE)
                        .matcher(Constant.mPushDataList.get(i).getMsgContent())
                        .find()) {
                    //  System.out.println("Hiiiiiii");
                    dc = Constant.mPushDataList.get(i);
                    list.add(dc);
                } else {
                    dc = Constant.mPushDataList.get(i);
                    list.add(dc);
                }

            }

        } else {
            list = Constant.mPushDataList;
        }

        // System.out.println("The searched list size is :::::" + list.size());


//        System.out.println("List size after filtered is :::::"
//                + mVehicleLocationListAdapter.size());

        if (list.size() > 0) {
            Constant.mPushDataListAdapter = list;
            if (Constant.mPushDataListAdapter.size() > 0) {

                NotificationListAdapter notiAdapter = new NotificationListAdapter(NotificationListActivity.this, Constant.mPushDataListAdapter);
//                lv.setAdapter(new NotificationListAdapter(
//                        NotificationListActivity.this, Constant.mPushDataList));

                lv.setAdapter(notiAdapter);
                notiAdapter.notifyDataSetChanged();

                lv.setVisibility(View.VISIBLE);
                mTxtView.setVisibility(View.GONE);
                mSelectAllLayout.setVisibility(View.VISIBLE);
            } else {
                lv.setVisibility(View.GONE);
                mTxtView.setVisibility(View.VISIBLE);
                mSelectAllLayout.setVisibility(View.GONE);
            }


        } else {
//    Set no record data
            List<PushNotificationDto> vList = new ArrayList<PushNotificationDto>();
            Constant.mPushDataListAdapter = vList;
            if (Constant.mPushDataListAdapter.size() > 0) {

                NotificationListAdapter notiAdapter = new NotificationListAdapter(NotificationListActivity.this, Constant.mPushDataListAdapter);
//                lv.setAdapter(new NotificationListAdapter(
//                        NotificationListActivity.this, Constant.mPushDataList));

                lv.setAdapter(notiAdapter);
                notiAdapter.notifyDataSetChanged();

                lv.setVisibility(View.VISIBLE);
                mTxtView.setVisibility(View.GONE);
                mSelectAllLayout.setVisibility(View.VISIBLE);
            } else {
                lv.setVisibility(View.GONE);
                mTxtView.setVisibility(View.VISIBLE);
                mSelectAllLayout.setVisibility(View.GONE);
            }
            // setData("No Record");

        }


        // $SearchListView.setAdapter(new DailySearchAdapter(DailySearch.this,
        // mFilteredListDailySearch));


    }


    private void opptionPopUp() {
        // TODO Auto-generated method stub
        final Dialog dialog = new Dialog(NotificationListActivity.this);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.radio_popup);
        dialog.show();

        RadioGroup rg_home = (RadioGroup) dialog.findViewById(R.id.rg_home_views);
        RadioGroup rg_history = (RadioGroup) dialog.findViewById(R.id.rg_history_views);
        rg_history.clearCheck();
        rg_history.setVisibility(View.GONE);
        rg_home.setVisibility(View.VISIBLE);

        RadioButton $Normal = (RadioButton) dialog
                .findViewById(R.id.rb_home_normal);
        RadioButton $Satelite = (RadioButton) dialog
                .findViewById(R.id.rb_home_satellite);
        RadioButton $Terrain = (RadioButton) dialog
                .findViewById(R.id.rb_home_terrain);
        RadioButton $Hybrid = (RadioButton) dialog
                .findViewById(R.id.rb_home_hybrid);


        if (mSELECTED_MAP_TYPE.equalsIgnoreCase("Normal")) {
            $Normal.setChecked(true);
            $Satelite.setChecked(false);
            $Terrain.setChecked(false);
            $Hybrid.setChecked(false);
        } else if (mSELECTED_MAP_TYPE.equalsIgnoreCase("Satelite")) {
            $Normal.setChecked(false);
            $Satelite.setChecked(true);
            $Terrain.setChecked(false);
            $Hybrid.setChecked(false);
        } else if (mSELECTED_MAP_TYPE.equalsIgnoreCase("Terrain")) {
            $Normal.setChecked(false);
            $Satelite.setChecked(false);
            $Terrain.setChecked(true);
            $Hybrid.setChecked(false);
        } else if (mSELECTED_MAP_TYPE.equalsIgnoreCase("Hybrid")) {
            $Normal.setChecked(false);
            $Satelite.setChecked(false);
            $Terrain.setChecked(false);
            $Hybrid.setChecked(true);
        }
        $Normal.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                map.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                mSELECTED_MAP_TYPE = "Normal";
                dialog.dismiss();
            }
        });
        $Satelite.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                mSELECTED_MAP_TYPE = "Satelite";
                map.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
                dialog.dismiss();
            }
        });
        $Terrain.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                mSELECTED_MAP_TYPE = "Terrain";
                map.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
                dialog.dismiss();
            }
        });

        $Hybrid.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                mSELECTED_MAP_TYPE = "Hybrid";
                map.setMapType(GoogleMap.MAP_TYPE_HYBRID);
                dialog.dismiss();
            }
        });

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        height = displayMetrics.heightPixels;
        width = displayMetrics.widthPixels;

        LinearLayout.LayoutParams radioParama = new LinearLayout.LayoutParams(
                FrameLayout.LayoutParams.WRAP_CONTENT,
                FrameLayout.LayoutParams.WRAP_CONTENT);
        radioParama.width = width * 50 / 100;
        radioParama.height = width * 10 / 100;
        radioParama.topMargin = height * 4 / 100;
        radioParama.gravity = Gravity.CENTER;
        radioParama.leftMargin = height * 4 / 100;
        $Normal.setLayoutParams(radioParama);
        $Satelite.setLayoutParams(radioParama);
        $Terrain.setLayoutParams(radioParama);

        LinearLayout.LayoutParams radioterrainParama = new LinearLayout.LayoutParams(
                FrameLayout.LayoutParams.WRAP_CONTENT,
                FrameLayout.LayoutParams.WRAP_CONTENT);
        radioterrainParama.width = width * 50 / 100;
        radioterrainParama.height = width * 10 / 100;
        radioterrainParama.topMargin = height * 4 / 100;
        radioterrainParama.gravity = Gravity.CENTER;
        radioterrainParama.leftMargin = height * 4 / 100;
        radioterrainParama.bottomMargin = height * 4 / 100;
        $Hybrid.setLayoutParams(radioterrainParama);

        if (width >= 600) {
            $Normal.setTextSize(16);
            $Satelite.setTextSize(16);
            $Terrain.setTextSize(16);
            $Hybrid.setTextSize(16);
        } else if (width > 501 && width < 600) {
            $Normal.setTextSize(15);
            $Satelite.setTextSize(15);
            $Terrain.setTextSize(15);
            $Hybrid.setTextSize(15);
        } else if (width > 260 && width < 500) {
            $Normal.setTextSize(14);
            $Satelite.setTextSize(14);
            $Terrain.setTextSize(14);
            $Hybrid.setTextSize(14);
        } else if (width <= 260) {
            $Normal.setTextSize(13);
            $Satelite.setTextSize(13);
            $Terrain.setTextSize(13);
            $Hybrid.setTextSize(13);
        }
    }

    public void setupGoogleMap() {

        if (map != null) {
            map.clear();
        }

        View marker = null;

        marker = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.custommarker, null);
        ImageView id_custom_marker_icon = (ImageView) marker.findViewById(R.id.id_custom_marker_icon);
        id_custom_marker_icon.setImageResource(R.drawable.grey_custom_marker_icon);
        ImageView id_vehicle_in_marker = (ImageView) marker.findViewById(R.id.id_vehicle_in_marker);
        id_vehicle_in_marker.setVisibility(View.GONE);

        CameraPosition INIT = new CameraPosition.Builder().target(new LatLng(mLatitude, mLongitude)).zoom(vehicle_zoom_level).build();
        map.animateCamera(CameraUpdateFactory.newCameraPosition(INIT));


        map.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {

            // Use default InfoWindow frame
            @Override
            public View getInfoWindow(Marker arg0) {
                return null;
            }

            // Defines the contents of the InfoWindow
            @Override
            public View getInfoContents(Marker arg0) {

                // Getting view from the layout file info_window_layout
                View v = getLayoutInflater().inflate(R.layout.map_info_txt_layout, null);

                // Getting the position from the marker
                LatLng latLng = arg0.getPosition();

                DisplayMetrics displayMetrics = new DisplayMetrics();
                getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
                height = displayMetrics.heightPixels;
                width = displayMetrics.widthPixels;
                LinearLayout layout = (LinearLayout) v.findViewById(R.id.map_info_layout);
                LinearLayout.LayoutParams backImageParams = new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.WRAP_CONTENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT);
                backImageParams.width = width * 80 / 100;
                //backImageParams.height = height * 10 / 100;
                backImageParams.gravity = Gravity.CENTER;
                layout.setLayoutParams(backImageParams);

                // Getting reference to the TextView to set latitude
                TextView txtContent = (TextView) v.findViewById(R.id.tv_txt_content);
                txtContent.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));


                txtContent.setText(mSelectedMsg);


                return v;

            }
        });


        MarkerOptions markerOption = new MarkerOptions().position(new LatLng(mLatitude, mLongitude));
        // markerOption.title(mSelectedMsg);
        // markerOption.icon(BitmapDescriptorFactory.fromBitmap(createDrawableFromView(NotificationListActivity.this, marker)));


        markerOption.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE));
        Marker currentMarker = map.addMarker(markerOption);
        currentMarker.showInfoWindow();

//        Marker marker = map.addMarker(new MarkerOptions()
//                .position(new LatLng(mLatitude, mLongitude))
//                .title(mSelectedMsg)
//                        // .snippet("Snippet")
//                .icon(BitmapDescriptorFactory
//                        .fromResource(R.drawable.grey_custom_marker_icon)));
//
//        marker.showInfoWindow();


    }


    public void setupOsmMap() {

//        if (map != null) {
//            map.clear();
//        }

        if (mapview != null) {
            InfoWindow.closeAllInfoWindowsOn(mapview);
            mapview.getOverlays().clear();
            mapview.invalidate();
            mapview.getOverlays().add(Constant.getTilesOverlay(getApplicationContext()));
        }

//        View marker = null;
//
//        marker = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.custommarker, null);
//        ImageView id_custom_marker_icon = (ImageView) marker.findViewById(R.id.id_custom_marker_icon);
//        id_custom_marker_icon.setImageResource(R.drawable.grey_custom_marker_icon);
//        ImageView id_vehicle_in_marker = (ImageView) marker.findViewById(R.id.id_vehicle_in_marker);
//        id_vehicle_in_marker.setVisibility(View.GONE);
//
//        CameraPosition INIT = new CameraPosition.Builder().target(new LatLng(mLatitude, mLongitude)).zoom(vehicle_zoom_level).build();
//        map.animateCamera(CameraUpdateFactory.newCameraPosition(INIT));
//
//
//        map.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
//
//            // Use default InfoWindow frame
//            @Override
//            public View getInfoWindow(Marker arg0) {
//                return null;
//            }
//
//            // Defines the contents of the InfoWindow
//            @Override
//            public View getInfoContents(Marker arg0) {
//
//                // Getting view from the layout file info_window_layout
//                View v = getLayoutInflater().inflate(R.layout.map_info_txt_layout, null);
//
//                // Getting the position from the marker
//                LatLng latLng = arg0.getPosition();
//
//                DisplayMetrics displayMetrics = new DisplayMetrics();
//                getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
//                height = displayMetrics.heightPixels;
//                width = displayMetrics.widthPixels;
//                LinearLayout layout = (LinearLayout) v.findViewById(R.id.map_info_layout);
//                LinearLayout.LayoutParams backImageParams = new LinearLayout.LayoutParams(
//                        LinearLayout.LayoutParams.WRAP_CONTENT,
//                        LinearLayout.LayoutParams.WRAP_CONTENT);
//                backImageParams.width = width * 80 / 100;
//                //backImageParams.height = height * 10 / 100;
//                backImageParams.gravity = Gravity.CENTER;
//                layout.setLayoutParams(backImageParams);
//
//                // Getting reference to the TextView to set latitude
//                TextView txtContent = (TextView) v.findViewById(R.id.tv_txt_content);
//                txtContent.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
//
//
//                txtContent.setText(mSelectedMsg);
//
//
//                return v;
//
//            }
//        });
//
//
//        MarkerOptions markerOption = new MarkerOptions().position(new LatLng(mLatitude, mLongitude));
//        // markerOption.title(mSelectedMsg);
//        // markerOption.icon(BitmapDescriptorFactory.fromBitmap(createDrawableFromView(NotificationListActivity.this, marker)));
//
//
//        markerOption.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE));
//        Marker currentMarker = map.addMarker(markerOption);
//        currentMarker.showInfoWindow();

//        Marker marker = map.addMarker(new MarkerOptions()
//                .position(new LatLng(mLatitude, mLongitude))
//                .title(mSelectedMsg)
//                        // .snippet("Snippet")
//                .icon(BitmapDescriptorFactory
//                        .fromResource(R.drawable.grey_custom_marker_icon)));
//
//        marker.showInfoWindow();


        View markerView = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.marker_image_view, null);
        ImageView mImgMarkerIcon = (ImageView) markerView.findViewById(R.id.img_marker_icon);
        mImgMarkerIcon.setImageResource(R.drawable.green_custom_marker_icon);
//        animateMarker.setIcon(createDrawableFromViewNew(AcReport.this, markerView));
        org.osmdroid.views.overlay.Marker osmMarker = new org.osmdroid.views.overlay.Marker(mapview);
        osmMarker.setPosition(new GeoPoint(mLatitude, mLongitude));
        osmMarker.setAnchor(org.osmdroid.views.overlay.Marker.ANCHOR_CENTER, org.osmdroid.views.overlay.Marker.ANCHOR_BOTTOM);
//        osmMarker.setTitle(Constant.SELECTED_VEHICLE_SHORT_NAME);
        osmMarker.setIcon(Constant.createDrawableFromViewNew(NotificationListActivity.this, markerView));
//        InfoWindow infoWindow = new MyInfoWindow(w);
//        osmMarker.setInfoWindow(infoWindow);
//        osmMarker.showInfoWindow();

        GeoPoint newPos = new GeoPoint(mLatitude, mLongitude);
        mapController.setCenter(newPos);
        mapview.getOverlays().add(osmMarker);
        mapview.invalidate();

    }

    public static Drawable createDrawableFromViewNew(Context context, View view) {


        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        view.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));


        view.measure(displayMetrics.widthPixels, displayMetrics.heightPixels);
        view.layout(0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels);
        view.buildDrawingCache();
//        System.out.println("Hi view width " + displayMetrics.widthPixels + " " + view.getMeasuredWidth() + " height " + displayMetrics.heightPixels + " " + view.getMeasuredHeight());
        Bitmap bitmap = Bitmap.createBitmap(view.getMeasuredWidth(), view.getMeasuredHeight(), Bitmap.Config.ARGB_8888);
//        Bitmap bitmap = Bitmap.createBitmap(250, 250, Bitmap.Config.ARGB_8888);
//        Bitmap bitmap = Bitmap.crea(96, 96, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        view.draw(canvas);
//        return bitmap;


        return new BitmapDrawable(bitmap);
    }
    public void setupMap() {

        if (map != null) {
            map.clear();
        }

        View marker = null;

        marker = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.custommarker, null);
        ImageView id_custom_marker_icon = (ImageView) marker.findViewById(R.id.id_custom_marker_icon);
        id_custom_marker_icon.setImageResource(R.drawable.grey_custom_marker_icon);
        ImageView id_vehicle_in_marker = (ImageView) marker.findViewById(R.id.id_vehicle_in_marker);
        id_vehicle_in_marker.setVisibility(View.GONE);

        CameraPosition INIT = new CameraPosition.Builder().target(new LatLng(mLatitude, mLongitude)).zoom(vehicle_zoom_level).build();
        map.animateCamera(CameraUpdateFactory.newCameraPosition(INIT));


        map.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {

            // Use default InfoWindow frame
            @Override
            public View getInfoWindow(Marker arg0) {
                return null;
            }

            // Defines the contents of the InfoWindow
            @Override
            public View getInfoContents(Marker arg0) {

                // Getting view from the layout file info_window_layout
                View v = getLayoutInflater().inflate(R.layout.map_info_txt_layout, null);

                // Getting the position from the marker
                LatLng latLng = arg0.getPosition();

                DisplayMetrics displayMetrics = new DisplayMetrics();
                getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
                height = displayMetrics.heightPixels;
                width = displayMetrics.widthPixels;
                LinearLayout layout = (LinearLayout) v.findViewById(R.id.map_info_layout);
                LinearLayout.LayoutParams backImageParams = new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.WRAP_CONTENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT);
                backImageParams.width = width * 80 / 100;
                //backImageParams.height = height * 10 / 100;
                backImageParams.gravity = Gravity.CENTER;
                layout.setLayoutParams(backImageParams);

                // Getting reference to the TextView to set latitude
                TextView txtContent = (TextView) v.findViewById(R.id.tv_txt_content);
                txtContent.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));


                txtContent.setText(mSelectedMsg);


                return v;

            }
        });


        MarkerOptions markerOption = new MarkerOptions().position(new LatLng(mLatitude, mLongitude));
        // markerOption.title(mSelectedMsg);
        // markerOption.icon(BitmapDescriptorFactory.fromBitmap(createDrawableFromView(NotificationListActivity.this, marker)));


        markerOption.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE));
        Marker currentMarker = map.addMarker(markerOption);
        currentMarker.showInfoWindow();

//        Marker marker = map.addMarker(new MarkerOptions()
//                .position(new LatLng(mLatitude, mLongitude))
//                .title(mSelectedMsg)
//                        // .snippet("Snippet")
//                .icon(BitmapDescriptorFactory
//                        .fromResource(R.drawable.grey_custom_marker_icon)));
//
//        marker.showInfoWindow();


    }
//    public void setupMap() {
//
//        if (map != null) {
//            map.clear();
//        }
//
//        View marker = null;
//
//        marker = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.custommarker, null);
//        ImageView id_custom_marker_icon = (ImageView) marker.findViewById(R.id.id_custom_marker_icon);
//        id_custom_marker_icon.setImageResource(R.drawable.grey_custom_marker_icon);
//        ImageView id_vehicle_in_marker = (ImageView) marker.findViewById(R.id.id_vehicle_in_marker);
//        id_vehicle_in_marker.setVisibility(View.GONE);
//
//        CameraPosition INIT = new CameraPosition.Builder().target(new LatLng(mLatitude, mLongitude)).zoom(vehicle_zoom_level).build();
//        map.animateCamera(CameraUpdateFactory.newCameraPosition(INIT));
//
//
//        map.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
//
//            // Use default InfoWindow frame
//            @Override
//            public View getInfoWindow(Marker arg0) {
//                return null;
//            }
//
//            // Defines the contents of the InfoWindow
//            @Override
//            public View getInfoContents(Marker arg0) {
//
//                // Getting view from the layout file info_window_layout
//                View v = getLayoutInflater().inflate(R.layout.map_info_txt_layout, null);
//
//                // Getting the position from the marker
//                LatLng latLng = arg0.getPosition();
//
//                DisplayMetrics displayMetrics = new DisplayMetrics();
//                getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
//                height = displayMetrics.heightPixels;
//                width = displayMetrics.widthPixels;
//                LinearLayout layout = (LinearLayout) v.findViewById(R.id.map_info_layout);
//                LinearLayout.LayoutParams backImageParams = new LinearLayout.LayoutParams(
//                        LinearLayout.LayoutParams.WRAP_CONTENT,
//                        LinearLayout.LayoutParams.WRAP_CONTENT);
//                backImageParams.width = width * 80 / 100;
//                //backImageParams.height = height * 10 / 100;
//                backImageParams.gravity = Gravity.CENTER;
//                layout.setLayoutParams(backImageParams);
//
//                // Getting reference to the TextView to set latitude
//                TextView txtContent = (TextView) v.findViewById(R.id.tv_txt_content);
//                txtContent.setTypeface(TypefaceUtil.getMyFont(getApplicationContext()));
//
//
//                txtContent.setText(mSelectedMsg);
//
//
//                return v;
//
//            }
//        });
//
//
//        MarkerOptions markerOption = new MarkerOptions().position(new LatLng(mLatitude, mLongitude));
//        // markerOption.title(mSelectedMsg);
//        // markerOption.icon(BitmapDescriptorFactory.fromBitmap(createDrawableFromView(NotificationListActivity.this, marker)));
//
//
//        markerOption.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE));
//        Marker currentMarker = map.addMarker(markerOption);
//        currentMarker.showInfoWindow();
//
////        Marker marker = map.addMarker(new MarkerOptions()
////                .position(new LatLng(mLatitude, mLongitude))
////                .title(mSelectedMsg)
////                        // .snippet("Snippet")
////                .icon(BitmapDescriptorFactory
////                        .fromResource(R.drawable.grey_custom_marker_icon)));
////
////        marker.showInfoWindow();
//
//
//    }

    /**
     * Code to make the Marker icon adjustable for multiple screen
     */
    public static Bitmap createDrawableFromView(Context context, View view) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        view.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        view.measure(displayMetrics.widthPixels, displayMetrics.heightPixels);
        view.layout(0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels);
        view.buildDrawingCache();
        Bitmap bitmap = Bitmap.createBitmap(view.getMeasuredWidth(), view.getMeasuredHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        view.draw(canvas);
        return bitmap;

    }


    public void screenArrange() {
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        int height = metrics.heightPixels;
        int width = metrics.widthPixels;
        Constant.ScreenWidth = width;
        Constant.ScreenHeight = height;

        LinearLayout.LayoutParams backImageParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        backImageParams.width = width * 10 / 100;
        backImageParams.height = height * 10 / 100;
        backImageParams.gravity = Gravity.CENTER;
        mImgBack.setLayoutParams(backImageParams);
        mImgBack.setPadding(width * 1 / 100, height * 1 / 100, width * 1 / 100, height * 1 / 100);

        LinearLayout.LayoutParams headTxtParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        headTxtParams.width = width * 70 / 100;
        headTxtParams.height = height * 10 / 100;
        mTxtHead.setLayoutParams(headTxtParams);
        mTxtHead.setPadding(width * 2 / 100, 0, 0, 0);
        mTxtHead.setGravity(Gravity.CENTER);
        LinearLayout.LayoutParams refreshImageParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        refreshImageParams.width = width * 10 / 100;
        refreshImageParams.height = height * 10 / 100;
        refreshImageParams.gravity = Gravity.CENTER;
        mImgRefresh.setLayoutParams(refreshImageParams);
        mImgRefresh.setPadding(width * 1 / 100, height * 1 / 100, width * 1 / 100, height * 1 / 100);

        FrameLayout.LayoutParams imgeChangeView = new FrameLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        imgeChangeView.width = width * 10 / 100;
        imgeChangeView.height = height * 15 / 100;
        imgeChangeView.gravity = Gravity.TOP | Gravity.RIGHT;
        imgeChangeView.rightMargin = (int) (width * 0.5 / 100);
        $ChangeView.setLayoutParams(imgeChangeView);

        LinearLayout.LayoutParams headTxtParams1 = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        headTxtParams1.width = width * 72 / 100;
        headTxtParams1.height = height * 8 / 100;
        headTxtParams1.setMargins(width * 4 / 100, width * 2 / 100, 1, width * 1 / 100);
//        headTxtParams1.setMargins();
//        $TxtTitle.setLayoutParams(headTxtParams);
//        $TxtTitle.setPadding(width * 2 / 100, 0, 0, 0);
//        $TxtTitle.setGravity(Gravity.CENTER);
        mEdtSearch.setLayoutParams(headTxtParams1);
//        mEdtSearch.setm
        mEdtSearch.setPadding(width * 2 / 100, 0, width * 4 / 100, 0);
        mEdtSearch.setGravity(Gravity.CENTER | Gravity.CENTER);

    }

    public int checkMsgSelected() {
        int count = 0;

        for (int i = 0; i < Constant.mPushDataListAdapter.size(); i++) {
            if (Constant.mPushDataListAdapter.get(i).isAlreadySelected()) {
                count++;
            }
        }
        return count;
    }

//    public List<PushNotificationDto> getPushList() {
//
//        List<PushNotificationDto> mPushDataListAdapter = new ArrayList<PushNotificationDto>();
//
//        // List<String> list = new ArrayList<String>();
//        DataBaseHandler db = new DataBaseHandler(getApplicationContext());
//        Cursor c = null;
//
////		String qry = "SELECT * FROM " + DataBaseHandler.TABLE_NOTIFICATIONS
////				+ " ORDER BY Datetime(time_stamp) DESC LIMIT 1000";
//
//        String qry = "SELECT * FROM " + DataBaseHandler.TABLE_NOTIFICATIONS
//                + " ORDER BY id DESC LIMIT 1000";
//
//
//        try {
//
//            c = db.open().getDatabaseObj().rawQuery(qry, null);
//            int push_msg_index = c.getColumnIndex("msg_data");
//            int id_index = c.getColumnIndex("id");
//            int time_stamp_index = c.getColumnIndex("time_stamp");
//
//            //System.out.println("Msg count is ::::::" + c.getCount());
//
//            while (c.moveToNext()) {
//                // String push_msg = c.getString(push_msg_index);
//
//                //System.out.println("The date and pos is ::::"+c.getString(time_stamp_index));
//
//                PushNotificationDto pu = new PushNotificationDto();
//                pu.setAlreadySelected(false);
//                pu.setId(c.getString(id_index).trim());
//                pu.setMsgContent(c.getString(push_msg_index));
//                pu.setTimeStamp(c.getString(time_stamp_index));
//                mPushDataListAdapter.add(pu);
//                // list.add(push_msg);
//            }
//
//            Constant.mPushDataList = mPushDataListAdapter;
//            Constant.mPushDataListAdapter = Constant.mPushDataList;
//        } catch (SQLException e) {
//
//        } finally {
//            c.close();
//            db.close();
//        }
//        return Constant.mPushDataList;
//    }


    public void checkPushList() {
        DaoHandler da = new DaoHandler(getApplicationContext(), true);
        Constant.mPushDataList = da.getPushList();

//        System.out.println("Msg count is 111::::::" + Constant.mPushDataList.size());

        Constant.mPushDataListAdapter = Constant.mPushDataList;

//        System.out.println("Msg count is 222::::::" + Constant.mPushDataListAdapter.size());
        setData();
    }

//    public void checkPushList() {
//        DataBaseHandler db = new DataBaseHandler(getBaseContext());
//        String qry = "DELETE FROM " + DataBaseHandler.TABLE_NOTIFICATIONS
//                + " WHERE id NOT IN (SELECT id FROM "
//                + DataBaseHandler.TABLE_NOTIFICATIONS
//                + " ORDER BY id DESC LIMIT 1000 ) ";
//
//        // String qry1="DELETE FROM "++"\n" +
//        /*
//         * String qry1="DELETE FROM "+DataBaseHandler.TABLE_NOTIFICATIONS+"\n" +
//		 * "WHERE id NOT IN (\n" + "  SELECT id\n" + "  FROM (\n" +
//		 * "    SELECT id\n" +
//		 * "    FROM \"+DataBaseHandler.TABLE_NOTIFICATIONS+\"\n" +
//		 * "    ORDER BY id DESC\n" + "    LIMIT 2 -- keep this many records\n"
//		 * + "  ) foo\n" + ");";
//		 */
//        Cursor c = null;
//        try {
//            c = db.open().getDatabaseObj().rawQuery(qry, null);
//            System.out.println("No of deleted rows in notification table is ::::::" + c.getCount());
//
//        } catch (SQLException e) {
//
//        } finally {
//            db.close();
//        }
//
//        Constant.mPushDataList = getPushList();
//        Constant.mPushDataListAdapter = Constant.mPushDataList;
//
//        setData();
//    }

    public void setData() {
        Log.d("pushlistadapter",""+Constant.mPushDataListAdapter.size());

        if (Constant.mPushDataListAdapter.size() > 0) {
            Log.d("setada","success");
            lv.setAdapter(new NotificationListAdapter(
                    NotificationListActivity.this, Constant.mPushDataListAdapter));
            lv.setVisibility(View.VISIBLE);
            mTxtView.setVisibility(View.GONE);
            mChkSelectAll.setVisibility(View.VISIBLE);
            mImgDelete.setVisibility(View.VISIBLE);
            selectall.setVisibility(View.VISIBLE);
            //mSelectAllLayout.setVisibility(View.VISIBLE);
            // stopping swipe refresh
            swipeRefreshLayout.setRefreshing(false);
        } else {
            Log.d("setada","failure");
            Toast.makeText(this, ""+getResources().getString(R.string.no_record), Toast.LENGTH_SHORT).show();
            lv.setVisibility(View.GONE);
           mTxtView.setVisibility(View.VISIBLE);
            mChkSelectAll.setVisibility(View.GONE);
            mImgDelete.setVisibility(View.GONE);
            selectall.setVisibility(View.GONE);
           // mSelectAllLayout.setVisibility(View.GONE);
        }
    }

    public List<PushNotificationDto> updateListCheckStatus(boolean mIsChecked) {
//		System.out.println("Check status ::::::" + mIsChecked);
        List<PushNotificationDto> managerDat = new ArrayList<PushNotificationDto>();
        if (Constant.mPushDataListAdapter != null && Constant.mPushDataListAdapter.size() > 0) {
            for (int i = 0; i < Constant.mPushDataListAdapter.size(); i++) {
                // Constant.mManagerToAddsList.remove(i);
                // System.out.println("checked status is :::::" + mIsChecked);
                PushNotificationDto m = new PushNotificationDto();
                m.setAlreadySelected(mIsChecked);
                m.setId(Constant.mPushDataListAdapter.get(i).getId());
                m.setMsgContent(Constant.mPushDataListAdapter.get(i).getMsgContent());
                m.setTimeStamp(Constant.mPushDataListAdapter.get(i).getTimeStamp());

                managerDat.add(m);
            }

        }


//		System.out.println("Manager list size is ::::::" + managerDat.size());
        Constant.mPushDataListAdapter = managerDat;
        // mPushDataListAdapter = managerDat;

        // for()

        NotificationListAdapter msgAdapter = new NotificationListAdapter(
                NotificationListActivity.this, Constant.mPushDataListAdapter);
        lv.setAdapter(msgAdapter);
        msgAdapter.notifyDataSetChanged();
        return Constant.mPushDataListAdapter;
    }

    public void deleteMsgData() {
        for (int i = 0; i < Constant.mPushDataListAdapter.size(); i++) {

//			System.out.println("The selected msg status is :::::"
//					+ Constant.mPushDataList.get(i).isAlreadySelected());
            if (Constant.mPushDataListAdapter.get(i).isAlreadySelected()) {
                Log.d("deletedata","success");

//				System.out.println("HIIIIIIIIIIIIIIIIIIIIIIIIIIIi");
                CommonManager co = new CommonManager(getApplicationContext());
                String qry = "DELETE FROM "
                        + DataBaseHandler.TABLE_NOTIFICATIONS + " WHERE id="
                        + Constant.mPushDataListAdapter.get(i).getId();
//                co.deleteDB(qry);

                DaoHandler da = new DaoHandler(getApplicationContext(), true);
                da.deleteSelectedNotificationFromDB(qry);
            }

        }

        checkPushList();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        startActivity(new Intent(NotificationListActivity.this, VehicleListActivity.class));
        finish();

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        map.setPadding(1, 1, 1, 150);
        map.getUiSettings().setZoomControlsEnabled(true);
    }

    @Override
    public void onRefresh() {
        checkPushList();
    }

}
