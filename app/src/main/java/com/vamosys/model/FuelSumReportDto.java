package com.vamosys.model;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FuelSumReportDto {

    @SerializedName("vehicleId")
    @Expose
    private String vehicleId;
    @SerializedName("vehicleName")
    @Expose
    private String vehicleName;
    @SerializedName("orgId")
    @Expose
    private String orgId;
    @SerializedName("startFuel")
    @Expose
    private String startFuel;
    @SerializedName("endFuel")
    @Expose
    private String endFuel;
    @SerializedName("fuelConsumption")
    @Expose
    private String fuelConsumption;
    @SerializedName("fuelFilling")
    @Expose
    private String fuelFilling;
    @SerializedName("fuelTheft")
    @Expose
    private String fuelTheft;
    @SerializedName("startKms")
    @Expose
    private String startKms;
    @SerializedName("endKms")
    @Expose
    private String endKms;
    @SerializedName("dist")
    @Expose
    private double dist;
    @SerializedName("kmpl")
    @Expose
    private String kmpl;
    @SerializedName("engineRunningHrs")
    @Expose
    private Integer engineRunningHrs;
    @SerializedName("engineIdleHrs")
    @Expose
    private String engineIdleHrs;
    @SerializedName("ltrsPerHrs")
    @Expose
    private String ltrsPerHrs;
    @SerializedName("ignitionHrs")
    @Expose
    private Integer ignitionHrs;
    @SerializedName("remarks")
    @Expose
    private String remarks;
    @SerializedName("error")
    @Expose
    private String error;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("secondaryEngineDuration")
    @Expose
    private String secondaryEngineDuration;
    @SerializedName("startLoc")
    @Expose
    private String startLoc;
    @SerializedName("endLoc")
    @Expose
    private String endLoc;
    @SerializedName("driverName")
    @Expose
    private String driverName;
    @SerializedName("driverMobileNo")
    @Expose
    private String driverMobileNo;
    @SerializedName("startAddress")
    @Expose
    private String startAddress;
    @SerializedName("endAddress")
    @Expose
    private String endAddress;
    @SerializedName("fuelsensors")
    @Expose
    private List<Fuelsensor> fuelsensors = null;
    @SerializedName("sensor")
    @Expose
    private Integer sensor;
    @SerializedName("tankData")
    @Expose
    private List<Fuelsensor> tankData = null;


    public String getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(String vehicleId) {
        this.vehicleId = vehicleId;
    }

    public String getVehicleName() {
        return vehicleName;
    }

    public void setVehicleName(String vehicleName) {
        this.vehicleName = vehicleName;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public String getStartFuel() {
        return startFuel;
    }

    public void setStartFuel(String startFuel) {
        this.startFuel = startFuel;
    }

    public String getEndFuel() {
        return endFuel;
    }

    public void setEndFuel(String endFuel) {
        this.endFuel = endFuel;
    }

    public String getFuelConsumption() {
        return fuelConsumption;
    }

    public void setFuelConsumption(String fuelConsumption) {
        this.fuelConsumption = fuelConsumption;
    }

    public String getFuelFilling() {
        return fuelFilling;
    }

    public void setFuelFilling(String fuelFilling) {
        this.fuelFilling = fuelFilling;
    }

    public String getFuelTheft() {
        return fuelTheft;
    }

    public void setFuelTheft(String fuelTheft) {
        this.fuelTheft = fuelTheft;
    }

    public String getStartKms() {
        return startKms;
    }

    public void setStartKms(String startKms) {
        this.startKms = startKms;
    }

    public String getEndKms() {
        return endKms;
    }

    public void setEndKms(String endKms) {
        this.endKms = endKms;
    }

    public double getDist() {
        return dist;
    }

    public void setDist(double dist) {
        this.dist = dist;
    }

    public String getKmpl() {
        return kmpl;
    }

    public void setKmpl(String kmpl) {
        this.kmpl = kmpl;
    }

    public Integer getEngineRunningHrs() {
        return engineRunningHrs;
    }

    public void setEngineRunningHrs(Integer engineRunningHrs) {
        this.engineRunningHrs = engineRunningHrs;
    }

    public String getEngineIdleHrs() {
        return engineIdleHrs;
    }

    public void setEngineIdleHrs(String engineIdleHrs) {
        this.engineIdleHrs = engineIdleHrs;
    }

    public String getLtrsPerHrs() {
        return ltrsPerHrs;
    }

    public void setLtrsPerHrs(String ltrsPerHrs) {
        this.ltrsPerHrs = ltrsPerHrs;
    }

    public Integer getIgnitionHrs() {
        return ignitionHrs;
    }

    public void setIgnitionHrs(Integer ignitionHrs) {
        this.ignitionHrs = ignitionHrs;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getSecondaryEngineDuration() {
        return secondaryEngineDuration;
    }

    public void setSecondaryEngineDuration(String secondaryEngineDuration) {
        this.secondaryEngineDuration = secondaryEngineDuration;
    }

    public String getStartLoc() {
        return startLoc;
    }

    public void setStartLoc(String startLoc) {
        this.startLoc = startLoc;
    }

    public String getEndLoc() {
        return endLoc;
    }

    public void setEndLoc(String endLoc) {
        this.endLoc = endLoc;
    }

    public String getDriverName() {
        return driverName;
    }

    public void setDriverName(String driverName) {
        this.driverName = driverName;
    }

    public String getDriverMobileNo() {
        return driverMobileNo;
    }

    public void setDriverMobileNo(String driverMobileNo) {
        this.driverMobileNo = driverMobileNo;
    }

    public String getStartAddress() {
        return startAddress;
    }

    public void setStartAddress(String startAddress) {
        this.startAddress = startAddress;
    }

    public String getEndAddress() {
        return endAddress;
    }

    public void setEndAddress(String endAddress) {
        this.endAddress = endAddress;
    }

    public List<Fuelsensor> getFuelsensors() {
        return fuelsensors;
    }

    public void setFuelsensors(List<Fuelsensor> fuelsensors) {
        this.fuelsensors = fuelsensors;
    }

    public Integer getSensor() {
        return sensor;
    }

    public void setSensor(Integer sensor) {
        this.sensor = sensor;
    }

    public List<Fuelsensor> getTankData() {
        return tankData;
    }

    public void setTankData(List<Fuelsensor> tankData) {
        this.tankData = tankData;
    }

    public static class Fuelsensor {

        @SerializedName("startFuel")
        @Expose
        private String startFuel;
        @SerializedName("endFuel")
        @Expose
        private String endFuel;
        @SerializedName("fuelConsumption")
        @Expose
        private String fuelConsumption;
        @SerializedName("fuelFilling")
        @Expose
        private String fuelFilling;
        @SerializedName("fuelTheft")
        @Expose
        private String fuelTheft;
        @SerializedName("ltrsPerHrs")
        @Expose
        private String ltrsPerHrs;
        @SerializedName("kmpl")
        @Expose
        private String kmpl;
        @SerializedName("sensor")
        @Expose
        private int sensor;
        public String getStartFuel() {
            return startFuel;
        }

        public void setStartFuel(String startFuel) {
            this.startFuel = startFuel;
        }

        public String getEndFuel() {
            return endFuel;
        }

        public void setEndFuel(String endFuel) {
            this.endFuel = endFuel;
        }

        public String getFuelConsumption() {
            return fuelConsumption;
        }

        public void setFuelConsumption(String fuelConsumption) {
            this.fuelConsumption = fuelConsumption;
        }

        public String getFuelFilling() {
            return fuelFilling;
        }

        public void setFuelFilling(String fuelFilling) {
            this.fuelFilling = fuelFilling;
        }

        public String getFuelTheft() {
            return fuelTheft;
        }

        public void setFuelTheft(String fuelTheft) {
            this.fuelTheft = fuelTheft;
        }

        public String getLtrsPerHrs() {
            return ltrsPerHrs;
        }

        public void setLtrsPerHrs(String ltrsPerHrs) {
            this.ltrsPerHrs = ltrsPerHrs;
        }

        public String getKmpl() {
            return kmpl;
        }

        public void setKmpl(String kmpl) {
            this.kmpl = kmpl;
        }

        public int getSensor() {
            return sensor;
        }

        public void setSensor(int sensor) {
            this.sensor = sensor;
        }
    }
}