package com.vamosys.model;


public class AlarmReportDto {

    public long startTime;
    public double longitude;
    public double latitude;
    public String alaryType;
    public String address;

    public long getStartTime() {
        return startTime;
    }

    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public String getAlaryType() {
        return alaryType;
    }

    public void setAlaryType(String alaryType) {
        this.alaryType = alaryType;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
