package com.vamosys.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class ExecutiveDataPojo {
    @SerializedName("excutiveData")
    @Expose
    private ArrayList<ExcutiveDatum> excutiveData = null;
    @SerializedName("vehicleId")
    @Expose
    private String vehicleId;
    @SerializedName("fromDate")
    @Expose
    private String fromDate;
    @SerializedName("toDate")
    @Expose
    private String toDate;
    @SerializedName("kms")
    @Expose
    private String kms;
    @SerializedName("totalParkCount")
    @Expose
    private String totalParkCount;
    @SerializedName("overSpeedCount")
    @Expose
    private String overSpeedCount;
    @SerializedName("totalMoveTime")
    @Expose
    private Long totalMoveTime;
    @SerializedName("totalParkTime")
    @Expose
    private Long totalParkTime;
    @SerializedName("totalNoDataTime")
    @Expose
    private Long totalNoDataTime;
    @SerializedName("totalIdleTime")
    @Expose
    private Long totalIdleTime;
    @SerializedName("startLoc")
    @Expose
    private String startLoc;
    @SerializedName("endLoc")
    @Expose
    private String endLoc;
    @SerializedName("power")
    @Expose
    private String power;
    @SerializedName("error")
    @Expose
    private Object error;

    public ArrayList<ExcutiveDatum> getExcutiveData() {
        return excutiveData;
    }

    public void setExcutiveData(ArrayList<ExcutiveDatum> excutiveData) {
        this.excutiveData = excutiveData;
    }

    public String getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(String vehicleId) {
        this.vehicleId = vehicleId;
    }

    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getToDate() {
        return toDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }

    public String getKms() {
        return kms;
    }

    public void setKms(String kms) {
        this.kms = kms;
    }

    public String getTotalParkCount() {
        return totalParkCount;
    }

    public void setTotalParkCount(String totalParkCount) {
        this.totalParkCount = totalParkCount;
    }

    public String getOverSpeedCount() {
        return overSpeedCount;
    }

    public void setOverSpeedCount(String overSpeedCount) {
        this.overSpeedCount = overSpeedCount;
    }

    public Long getTotalMoveTime() {
        return totalMoveTime;
    }

    public void setTotalMoveTime(Long totalMoveTime) {
        this.totalMoveTime = totalMoveTime;
    }

    public Long getTotalParkTime() {
        return totalParkTime;
    }

    public void setTotalParkTime(Long totalParkTime) {
        this.totalParkTime = totalParkTime;
    }

    public Long getTotalNoDataTime() {
        return totalNoDataTime;
    }

    public void setTotalNoDataTime(Long totalNoDataTime) {
        this.totalNoDataTime = totalNoDataTime;
    }

    public Long getTotalIdleTime() {
        return totalIdleTime;
    }

    public void setTotalIdleTime(Long totalIdleTime) {
        this.totalIdleTime = totalIdleTime;
    }

    public String getStartLoc() {
        return startLoc;
    }

    public void setStartLoc(String startLoc) {
        this.startLoc = startLoc;
    }

    public String getEndLoc() {
        return endLoc;
    }

    public void setEndLoc(String endLoc) {
        this.endLoc = endLoc;
    }

    public String getPower() {
        return power;
    }

    public void setPower(String power) {
        this.power = power;
    }

    public Object getError() {
        return error;
    }

    public void setError(Object error) {
        this.error = error;
    }


}
