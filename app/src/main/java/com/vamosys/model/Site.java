package com.vamosys.model;

public class Site {

	String siteName;
	String siteType;
	String userId;
	String latLng;
	String OrgId;
	
	public String getOrgId() {
		return OrgId;
	}
	public void setOrgId(String orgId) {
		this.OrgId = orgId;
	}
	public String getSiteName() {
		return siteName;
	}
	public void setSiteName(String siteName) {
		this.siteName = siteName;
	}
	public String getSiteType() {
		return siteType;
	}
	public void setSiteType(String siteType) {
		this.siteType = siteType;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getLatLng() {
		return latLng;
	}
	public void setLatLng(String latLng) {
		this.latLng = latLng;
	}
	
}
