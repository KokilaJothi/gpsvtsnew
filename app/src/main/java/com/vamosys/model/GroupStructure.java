package com.vamosys.model;

public class GroupStructure {
    String latitude;
    String longitude;
    String groupname;
    String total_vehicles;
    public GroupStructure(String latitude, String longitude, String groupname, String total_vehicles) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.groupname = groupname;
        this.total_vehicles = total_vehicles;
    }
    public GroupStructure() {
        this.latitude = "";
        this.longitude = "";
        this.groupname = "";
        this.total_vehicles = "";
    }
    //getters
    public String getlatitude() {
        return latitude;
    }
    public String getlongitude() {
        return longitude;
    }
    public String getgroupname() {
        return groupname;
    }
    public String gettotal_vehicles() {
        return total_vehicles;
    }
    //setters
    public void setlatitude(String latitude) {
        this.latitude = latitude;
    }
    public void setlongitude(String longitude) {
        this.longitude = longitude;
    }
    public void setgroupname(String groupname) {
        this.groupname = groupname;
    }
    public void settotal_vehicles(String total_vehicles) {
        this.total_vehicles = total_vehicles;
    }
}

