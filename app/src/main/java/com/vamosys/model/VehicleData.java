package com.vamosys.model;

import android.util.Log;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Comparator;


public class VehicleData extends VehicleLocation implements Comparator<VehicleData> {

    private String vehicleMake;
    private String oprName;
    private String regNo;
    private String vehicleType;
    private String vehicleId;

    private String driverMobile;
    private String customMarker;
    private String deviceModel;
    private String shortName;
    private double overSpeedLimit;
    private String driverName;
    private String vehicleBusy;

    private String fuel;
    private String powerStatus;
    private String totalTruck;
    private double temperature;
    private String expired;
    private String error;
    protected String licenceExpiry;
    protected String fuelLitres;
    protected int sensorCount;
    protected String nTankSize;
    protected int noOfTank;

    public String getnTankSize() {
        return nTankSize;
    }

    public void setnTankSize(String nTankSize) {
        this.nTankSize = nTankSize;
    }

    public int getSensorCount() {
        return sensorCount;
    }

    public void setSensorCount(int sensorCount) {
        this.sensorCount = sensorCount;
    }

    public String getFuelLitres() {
        return fuelLitres;
    }

    public void setFuelLitres(String fuelLitres) {
        this.fuelLitres = fuelLitres;
    }

    public String getLicenceExpiry() {
        return licenceExpiry;
    }

    public void setLicenceExpiry(String licenceExpiry) {
        this.licenceExpiry = licenceExpiry;
    }

    public String getSerial1() {
        return serial1;
    }

    public void setSerial1(String serial1) {
        this.serial1 = serial1;
    }

    private String serial1;

//    public VehicleData(String first){
//        this.shortName = first;
//
//    }




    public String getDriverMobile() {
        Log.d("driverMobile",""+driverMobile);
        return driverMobile;
    }

    public void setDriverMobile(String driverMobile) {
        this.driverMobile = driverMobile;
    }


    public String getExpired() {
        return expired;
    }

    public void setExpired(String expired) {
        this.expired = expired;
    }

    public double getOverSpeedLimit() {
        return overSpeedLimit;
    }

    public void setOverSpeedLimit(double overSpeedLimit) {
        try {
            this.overSpeedLimit = new BigDecimal(overSpeedLimit).setScale(2, RoundingMode.HALF_UP).doubleValue();
        } catch (Exception e) {
            this.overSpeedLimit = 40;
        }
    }

    public double getTemperature() {
        return temperature;
    }

    public void setTemperature(double temperature) {
        this.temperature = temperature;
    }

    public String getFuel() {
        return fuel;
    }

    public void setFuel(String fuel) {
        this.fuel = fuel;
    }

    public String getPowerStatus() {
        return powerStatus;
    }

    public void setPowerStatus(String powerStatus) {
        this.powerStatus = powerStatus;
    }

    public String getTotalTruck() {
        return totalTruck;
    }

    public void setTotalTruck(String totalTruck) {
        this.totalTruck = totalTruck;
    }

    public String getVehicleMake() {
        return vehicleMake;
    }

    public void setVehicleMake(String vehicleMake) {
        this.vehicleMake = vehicleMake;
    }

    public String getOprName() {
        return oprName;
    }

    public void setOprName(String operatorName) {
        this.oprName = operatorName;
    }

    public String getRegNo() {
        return regNo;
    }

    public void setRegNo(String regNumber) {
        regNo = regNumber;
    }

    public String getVehicleType() {
        return vehicleType;
    }

    public void setVehicleType(String vehicleType) {
        this.vehicleType = vehicleType;
    }

    public String getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(String vehicleId) {
        this.vehicleId = vehicleId;
    }



    public String getCustomMarker() {
        return "car";
        //	return customMarker;
    }

    public void setCustomMarker(String customMarker) {
        this.customMarker = customMarker;
    }

    public String getDeviceModel() {
        return deviceModel;
    }

    public void setDeviceModel(String deviceModel) {
        this.deviceModel = deviceModel;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }


    /**
     * @return the billingStatus
     */
    /*public String getBillingStatus() {
		return billingStatus;
	}
	*//**
     * @param billingStatus the billingStatus to set
     *//*
	public void setBillingStatus(String billingStatus) {
		this.billingStatus = billingStatus;
	}*/

    /**
     * @return the driverName
     */
    public String getDriverName() {
        return driverName;
    }

    /**
     * @param driverName the driverName to set
     */
    public void setDriverName(String driverName) {
        this.driverName = driverName;
    }


    /**
     * @return the error
     */
    public String getError() {
        return error;
    }

    /**
     * @param error the error to set
     */
    public void setError(String error) {
        this.error = error;
    }


    public String getVehicleBusy() {
        return vehicleBusy;
    }

    public void setVehicleBusy(String vehicleBusy) {
        this.vehicleBusy = vehicleBusy;
    }

    public int getNoOfTank() {
        return noOfTank;
    }

    public void setNoOfTank(int noOfTank) {
        this.noOfTank = noOfTank;
    }

    @Override
    public String toString() {
        return "VehicleData [vehicleMake=" + vehicleMake + ", oprName=" + oprName + ", regNo=" + regNo
                + ", vehicleType=" + vehicleType + ", vehicleId=" + vehicleId + ", driverMobile=" + driverMobile
                + ", customMarker=" + customMarker + ", deviceModel=" + deviceModel + ", shortName=" + shortName
                + ", overSpeedLimit=" + overSpeedLimit + ", driverName=" + driverName + ", error=" + error + ", rowId="
                + rowId + ", latitude=" + latitude + ", longitude=" + longitude + ", speed=" + speed + ", date=" + date
                + ", alert=" + alert + ", direction=" + direction + ", position=" + position + ", distanceCovered="
                + distanceCovered + ", odoDistance=" + odoDistance + ", status=" + status + ", altitude=" + altitude
                + ", color=" + color + ", lastSeen=" + lastSeen + ", ignitionStatus=" + ignitionStatus
                + ", serial1=" + serial1 +", insideGeoFence=" + insideGeoFence + ", isOverSpeed=" + isOverSpeed + ", address=" + address+", nTankSize"+nTankSize
                + ", sensorCount"+ sensorCount +", fuelLitres"+ fuelLitres +", licenceExpiry"+licenceExpiry+", expiryStatus" + expiryStatus + ",noOfTank"+noOfTank+"]";

    }


    @Override
    public int compare(VehicleData s1, VehicleData s2) {
        Integer i1 = null;
        Integer i2 = null;
        try {
            i1 = Integer.parseInt(String.valueOf(s1));
        } catch (NumberFormatException e) {
        }

        try {
            i2 = Integer.parseInt(String.valueOf(s2));
        } catch (NumberFormatException e) {
        }

        if(i1!=null && i2!=null){
            return i1.compareTo(i2);
        }else{
            return s1.getShortName().compareTo(s2.getShortName());
        }
//        return o1.getShortName().compareTo(o2.getShortName());
    }
}
