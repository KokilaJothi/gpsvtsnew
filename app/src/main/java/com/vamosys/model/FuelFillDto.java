package com.vamosys.model;


public class FuelFillDto {

    public double fuelConsume;
    public double fuelFrom;
    public double fuelTo;
    public long startTime;

    public long getDt() {
        return dt;
    }

    public void setDt(long dt) {
        this.dt = dt;
    }

    public long dt;


    public double odo;


    public long endTime;
    public double longitude;
    public double latitude;
    public String address;
    public String fuelLtr;
    public String newSpeed;

    public String getTankSize() {
        return tankSize;
    }

    public void setTankSize(String tankSize) {
        this.tankSize = tankSize;
    }

    public String tankSize;
    public double odoMeterReading;
    public String ignitionStatus;

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String error;

    public double getFuelConsume() {
        return fuelConsume;
    }

    public void setFuelConsume(double fuelConsume) {
        this.fuelConsume = fuelConsume;
    }

    public double getFuelFrom() {
        return fuelFrom;
    }

    public void setFuelFrom(double fuelFrom) {
        this.fuelFrom = fuelFrom;
    }

    public long getStartTime() {
        return startTime;
    }

    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }


    public long getEndTime() {
        return endTime;
    }

    public void setEndTime(long endTime) {
        this.endTime = endTime;
    }


    public double getOdo() {
        return odo;
    }

    public void setOdo(double odo) {
        this.odo = odo;
    }


    public double getFuelTo() {
        return fuelTo;
    }

    public void setFuelTo(double fuelTo) {
        this.fuelTo = fuelTo;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getFuelLtr() {
        return fuelLtr;
    }

    public void setFuelLtr(String fuelLtr) {
        this.fuelLtr = fuelLtr;
    }

    public String getNewSpeed() {
        return newSpeed;
    }

    public void setNewSpeed(String newSpeed) {
        this.newSpeed = newSpeed;
    }

    public double getOdoMeterReading() {
        return odoMeterReading;
    }

    public void setOdoMeterReading(double odoMeterReading) {
        this.odoMeterReading = odoMeterReading;
    }

    public String getIgnitionStatus() {
        return ignitionStatus;
    }

    public void setIgnitionStatus(String ignitionStatus) {
        this.ignitionStatus = ignitionStatus;
    }
}
