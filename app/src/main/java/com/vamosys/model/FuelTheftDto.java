package com.vamosys.model;


public class FuelTheftDto {


    public String prevLitr;
    public String currLitr;
    public double theftLitr;



    public double odoLitr;
    public String theftLoc;
    public String date;



    public String startdate;
    public String latitude;
    public String longitude;

    public double getOdoLitr() {
        return odoLitr;
    }

    public void setOdoLitr(double odoLitr) {
        this.odoLitr = odoLitr;
    }

    public String getPrevLitr() {
        return prevLitr;
    }

    public void setPrevLitr(String prevLitr) {
        this.prevLitr = prevLitr;
    }

    public String getCurrLitr() {
        return currLitr;
    }

    public void setCurrLitr(String currLitr) {
        this.currLitr = currLitr;
    }

    public double getTheftLitr() {
        return theftLitr;
    }

    public void setTheftLitr(double theftLitr) {
        this.theftLitr = theftLitr;
    }
    public String getStartdate() {
        return startdate;
    }

    public void setStartdate(String startdate) {
        this.startdate = startdate;
    }

    public String getTheftLoc() {
        return theftLoc;
    }

    public void setTheftLoc(String theftLoc) {
        this.theftLoc = theftLoc;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }
}
