package com.vamosys.model;


public class FuelsumDto {

    public String date;
    public String vehicleId;
    public String vehicleName;
    public String startFuel;
    public String endFuel;
    public String fuelConsumption;
    public String fuelTheft;

    public String getFuelfilling() {
        return fuelfilling;
    }

    public void setFuelfilling(String fuelfilling) {
        this.fuelfilling = fuelfilling;
    }

    public String fuelfilling;
    public String dist;
    public String kmpl;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(String vehicleId) {
        this.vehicleId = vehicleId;
    }

    public String getVehicleName() {
        return vehicleName;
    }

    public void setVehicleName(String vehicleName) {
        this.vehicleName = vehicleName;
    }

    public String getStartFuel() {
        return startFuel;
    }

    public void setStartFuel(String startFuel) {
        this.startFuel = startFuel;
    }

    public String getEndFuel() {
        return endFuel;
    }

    public void setEndFuel(String endFuel) {
        this.endFuel = endFuel;
    }

    public String getFuelConsumption() {
        return fuelConsumption;
    }

    public void setFuelConsumption(String fuelConsumption) {
        this.fuelConsumption = fuelConsumption;
    }

    public String getFuelTheft() {
        return fuelTheft;
    }

    public void setFuelTheft(String fuelTheft) {
        this.fuelTheft = fuelTheft;
    }

    public String getDist() {
        return dist;
    }

    public void setDist(String dist) {
        this.dist = dist;
    }

    public String getKmpl() {
        return kmpl;
    }

    public void setKmpl(String kmpl) {
        this.kmpl = kmpl;
    }





}
