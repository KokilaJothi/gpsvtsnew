package com.vamosys.interfaces;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.vamosys.vamos.Const;
import com.vamosys.vamos.R;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiClient {
//    http://209.97.163.4:9000/getConsolidatedFuelReport?userId=DHARIKAZEEM&groupName=DHARIKAZEEM:VAM&date=2020-09-04
//    public static String BASE_URL ="http://209.97.163.4:9000/";
    public static String BASE_URL = Const.API_URL;
    private static Retrofit retrofit;
    public static Retrofit getClient(){
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        if(retrofit == null){
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .build();
        }
        return retrofit;
    }

}
