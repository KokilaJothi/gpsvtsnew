package com.vamosys.interfaces;

import com.vamosys.model.ExecutiveDataPojo;
import com.vamosys.model.FuelSumReportDto;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ApiInterface {
//    http://209.97.163.4:9000/getConsolidatedFuelReport?userId=DHARIKAZEEM&groupName=DHARIKAZEEM:VAM&date=2020-09-04
    @GET("mobile/getConsolidatedFuelReport")
    Call<List<FuelSumReportDto>> getConsolidatedFuelReport(
          @Query("userId") String userId,
          @Query("groupName") String groupName,
          @Query("date") String date
    );
//    http://209.97.163.4:9000/getPerformanceReport?groupId=MGMSAFL:SMP&fromDate=2020-09-21&toDate=2020-09-27&userId=MGMSAFL
    @GET("mobile/getPerformanceReport")
    Call<ArrayList<ExecutiveDataPojo>> getPerformanceReport(
            @Query("groupId") String groupId,
            @Query("fromDate") String fromDate,
            @Query("toDate") String toDate,
            @Query("userId") String userId
    );
}
