package com.vamosys.adapter;

import android.content.Context;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.vamosys.interfaces.ShowMapInterface;
import com.vamosys.model.FuelFillDto;
import com.vamosys.vamos.Const;
import com.vamosys.vamos.R;

import java.util.ArrayList;
import java.util.List;

public class SensorFuelAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<FuelFillDto> mFuelFillData = new ArrayList<FuelFillDto>();
    private static final int TYPE_HEADER = 0;
    private static final int TYPE_ITEM = 1;
    ShowMapInterface showMap;
    public SensorFuelAdapter(Context context, List<FuelFillDto> mFuelFillData, ShowMapInterface show) {
        this.mFuelFillData = mFuelFillData;
        showMap=show;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Log.d("fueldata","oncreate");
        if (viewType == TYPE_ITEM) {
            // Here Inflating your recyclerview item layout
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.fuel_fill_sensor_row, parent, false);
            return new RowViewHolder(view);
        } else if (viewType == TYPE_HEADER) {
            // Here Inflating your header view
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.fuel_fill_sensor_head, parent, false);
            return new HeaderViewHolder(itemView);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        Log.d("fueldata","onBind");
        if (holder instanceof HeaderViewHolder){
            HeaderViewHolder headerViewHolder = (HeaderViewHolder) holder;
        }
        else if (holder instanceof RowViewHolder){

            final RowViewHolder holder1 = (RowViewHolder) holder;
            // Following code give a row of header and decrease the one position from listview items
            FuelFillDto f=getItem(position);
            holder1.txt_date.setText(Const.getAcReportTime(f.getStartTime()));
            holder1.txt_ltr.setText(String.valueOf(f.getFuelLtr()));
            SpannableString content = new SpannableString("Link");
            content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
            holder1.txt_adds.setText(content);
            holder1.txt_speed.setText(String.valueOf(f.getNewSpeed()));
            holder1.txt_odo.setText(String.valueOf(f.getOdoMeterReading()));

            holder1.txt_adds.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String[] value1 = f.getAddress().split(",");
                    double mLatitude,mLongitude;
                    mLatitude = Double.valueOf(value1[0]);
                    mLongitude = Double.valueOf(value1[1]);
                    Log.d("fueldata","address "+ mLatitude+" "+ mLongitude);
                    showMap.showLatLng(mLatitude,mLongitude);
                }
            });
        }

    }
    @Override
    public int getItemViewType(int position) {
        if (isPositionHeader(position)) {
            return TYPE_HEADER;
        }
        return TYPE_ITEM;
    }

    private boolean isPositionHeader(int position) {
        return position == 0;
    }

    private FuelFillDto getItem(int position) {
        return mFuelFillData.get(position-1);
    }
    @Override
    public int getItemCount() {
        return mFuelFillData.size()+1;
    }

    public class RowViewHolder extends RecyclerView.ViewHolder{
        TextView txt_date,txt_ltr,txt_adds,txt_speed,txt_odo;
        public RowViewHolder(@NonNull View itemView) {
            super(itemView);
            txt_date=itemView.findViewById(R.id.fuelfill_date_textView);
            txt_ltr=itemView.findViewById(R.id.fuelfill_fuel_ltr_textView);
            txt_adds=itemView.findViewById(R.id.fuelfill_adds_textView);
            txt_speed=itemView.findViewById(R.id.fuelfill_speed_textView);
            txt_odo=itemView.findViewById(R.id.fuelfill_odo_distance_textView);
        }
    }

    private class HeaderViewHolder extends RecyclerView.ViewHolder {
        TextView Htxt_date,Htxt_ltr,Htxt_adds,Htxt_speed,Htxt_odo;
        public HeaderViewHolder(View view) {
            super(view);
            Htxt_date=view.findViewById(R.id.fuelsumm__textView_header);
            Htxt_ltr=view.findViewById(R.id.fuelfill_fuel_ltr_textView_header);
            Htxt_adds=view.findViewById(R.id.fuelfill_adds_textView_header);
            Htxt_speed=view.findViewById(R.id.fuelfill_odo_distance_textView_header);
            Htxt_odo=view.findViewById(R.id.fuelfill_speed_textView_header);
        }
    }

}
