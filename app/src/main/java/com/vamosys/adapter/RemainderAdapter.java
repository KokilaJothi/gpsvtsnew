package com.vamosys.adapter;

import android.app.Activity;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.vamosys.model.FmsDto;
import com.vamosys.vamos.R;

import java.util.List;

public class RemainderAdapter extends RecyclerView.Adapter<RemainderAdapter.ViewHolder> {
    private List<FmsDto> mArrayList;
    private Activity myActivity;

    public RemainderAdapter(List<FmsDto> arrayList, Activity act) {
        mArrayList = arrayList;
        myActivity = act;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.remainder_adapter, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {


        viewHolder.tv_document_type.setText(mArrayList.get(i).getDocumentType());
        viewHolder.tv_due_date.setText(mArrayList.get(i).getDueDate());
        viewHolder.tv_amount.setText(mArrayList.get(i).getAmount());
        viewHolder.tv_company.setText(mArrayList.get(i).getCompanyName());
        viewHolder.tv_description.setText(mArrayList.get(i).getDescription());


    }

    @Override
    public int getItemCount() {
        return mArrayList.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        //  private TextView tv_name, tv_version, tv_api_level;
        private TextView tv_document_type, tv_due_date, tv_amount, tv_company, tv_description;
        public RelativeLayout viewBackground;
        public LinearLayout viewForeground;

        public ViewHolder(View view) {
            super(view);

            tv_document_type = (TextView) view.findViewById(R.id.txt_document_type);
            tv_due_date = (TextView) view.findViewById(R.id.txt_date);
            tv_amount = (TextView) view.findViewById(R.id.txt_amount);
            tv_company = (TextView) view.findViewById(R.id.txt_company_name);
            tv_description = (TextView) view.findViewById(R.id.txt_description);

            viewBackground = (RelativeLayout)view.findViewById(R.id.view_background);
            viewForeground = (LinearLayout)view.findViewById(R.id.view_foreground);

        }
    }

}