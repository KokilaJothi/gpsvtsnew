//package com.vamosys.adapter;
//
//import android.app.Activity;
//import android.content.Context;
//import android.text.SpannableString;
//import android.text.style.UnderlineSpan;
//import android.view.Gravity;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.BaseAdapter;
//import android.widget.LinearLayout;
//import android.widget.TextView;
//
//import com.vamosys.model.EngineReportDto;
//import com.vamosys.vamos.Const;
//import com.vamosys.vamos.R;
//
//import java.util.ArrayList;
//import java.util.List;
//
//public class SecondaryEngineReportAdapter extends BaseAdapter {
//
//    //    Context mContext;
////    int layoutResourceId;
//    List<EngineReportDto> mEngineReportList = new ArrayList<EngineReportDto>();
//    private static LayoutInflater inflater = null;
//    Activity myacActivity;
//    int width, height;
//    ViewHolder holder;
//    //    Const cons;
//    private final OnItemClickListener listener;
//
//    public SecondaryEngineReportAdapter(Activity act, List<EngineReportDto> mList, OnItemClickListener listener) {
////        cons = new Const();
//        myacActivity = act;
//        this.mEngineReportList = mList;
//        inflater = (LayoutInflater) myacActivity
//                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//        this.listener = listener;
//    }
//
//    public interface OnItemClickListener {
//        void onStartMapClicked(int i);
//
//        void onEndMapClicked(int i);
//
////        void onLongClicked(int i);
//    }
//
//    @Override
//    public int getCount() {
//        // TODO Auto-generated method stub
//        return mEngineReportList.size();
//    }
//
//    @Override
//    public Object getItem(int position) {
//        // TODO Auto-generated method stub
//        return mEngineReportList.get(position);
//    }
//
//    @Override
//    public long getItemId(int position) {
//        // TODO Auto-generated method stub
//        return position;
//    }
//
//    @Override
//    public View getView(final int position, View convertView, ViewGroup parent) {
//        // TODO Auto-generated method stub
//        View vi = convertView;
//
//        convertView = null;
//        if (convertView == null) {
//
//            vi = inflater.inflate(R.layout.secondary_engine_report_list_adapter, null);
//            holder = new ViewHolder();
//
//            holder.mTxtStartDateTime = (TextView) vi.findViewById(R.id.engine_report_datetime_textView);
//            holder.mTxtEndDateTime = (TextView) vi.findViewById(R.id.engine_report_end_datetime_textView);
//            holder.mTxtStartStaus = (TextView) vi.findViewById(R.id.engine_report_status_textView);
//            holder.mTxtEndStatus = (TextView) vi.findViewById(R.id.engine_report_end_status_textView);
//            holder.mTxtDuration = (TextView) vi.findViewById(R.id.engine_report_duration_textView);
//            holder.mTxtStartAdds = (TextView) vi.findViewById(R.id.engine_report_nearest_location_textView);
//            holder.mTxtEndAdds = (TextView) vi.findViewById(R.id.engine_report_nearest_end_location_textView);
//            holder.mTxtStartMap = (TextView) vi.findViewById(R.id.engine_report_gmap_textView);
//            holder.mTxtEndMap = (TextView) vi.findViewById(R.id.engine_report_end_gmap_textView);
//
//
//            LinearLayout.LayoutParams vehicleNameParams = new LinearLayout.LayoutParams(
//                    LinearLayout.LayoutParams.WRAP_CONTENT,
//                    LinearLayout.LayoutParams.WRAP_CONTENT);
//            vehicleNameParams.width = width * 35 / 100;
//            vehicleNameParams.height = height * 11 / 100;
//            col0.setLayoutParams(vehicleNameParams);
//            col0.setPadding(width * 2 / 100, height * (int)0.5 / 100, width * 2 / 100, height * (int)0.5 / 100);
//            col0.setGravity(Gravity.CENTER );
//
//            col1.setLayoutParams(vehicleNameParams);
//            col1.setPadding(width * 2 / 100, height * (int)0.5 / 100, width * 2 / 100, height * (int)0.5 / 100);
//            col1.setGravity(Gravity.CENTER);
//
//
//            LinearLayout.LayoutParams text3Params = new LinearLayout.LayoutParams(
//                    LinearLayout.LayoutParams.WRAP_CONTENT,
//                    LinearLayout.LayoutParams.WRAP_CONTENT);
//            text3Params.width = width * 30 / 100;
//            text3Params.height = height * 11 / 100;
//
//            col2.setLayoutParams(text3Params);
//            //   col3.setWidth(width * 75 / 100);
//            col2.setPadding(width * 2 / 100, height * (int)0.5 / 100, width * 2 / 100, height * (int)0.5 / 100);
//            col2.setGravity(Gravity.CENTER );
//
//            col3.setLayoutParams(text3Params);
//            //   col3.setWidth(width * 75 / 100);
//            col3.setPadding(width * 2 / 100, height * (int)0.5 / 100, width * 2 / 100, height * (int)0.5 / 100);
//            col3.setGravity(Gravity.CENTER );
//
//            col4.setLayoutParams(text3Params);
//            //  col3.setWidth(width * 75 / 100);
//            col4.setPadding(width * 2 / 100, height * (int)0.5 / 100, width * 2 / 100, height * (int)0.5 / 100);
//            col4.setGravity(Gravity.CENTER );
//
//            col7.setLayoutParams(text3Params);
//            //   col3.setWidth(width * 75 / 100);
//            col7.setPadding(width * 2 / 100, height * (int)0.5 / 100, width * 2 / 100, height * (int)0.5 / 100);
//            col7.setGravity(Gravity.CENTER);
//
//            col8.setLayoutParams(text3Params);
//            //   col3.setWidth(width * 75 / 100);
//            col8.setPadding(width * 2 / 100, height * (int)0.5 / 100, width * 2 / 100, height * (int)0.5 / 100);
//            col8.setGravity(Gravity.CENTER);
//
//
//            LinearLayout.LayoutParams text2Params = new LinearLayout.LayoutParams(
//                    LinearLayout.LayoutParams.WRAP_CONTENT,
//                    LinearLayout.LayoutParams.WRAP_CONTENT);
//            text2Params.width = width * 60 / 100;
//            text2Params.height = height * 11 / 100;
//            col5.setLayoutParams(text2Params);
//            col5.setPadding(width * 2 / 100, height * (int)0.5 / 100, width * 2 / 100, height * (int)0.5 / 100);
//            col5.setGravity(Gravity.CENTER | Gravity.LEFT);
//
//            col6.setLayoutParams(text2Params);
//            col6.setPadding(width * 2 / 100, height * (int)0.5 / 100, width * 2 / 100, height * (int)0.5 / 100);
//            col6.setGravity(Gravity.CENTER | Gravity.LEFT);
//
//
//            //screenArrange();
//            vi.setTag(holder);
//
//
//        } else {
//            holder = (ViewHolder) vi.getTag();
//        }
//
//        EngineReportDto data = mEngineReportList.get(position);
//
//        holder.mTxtStartDateTime.setText(Const.getAcReportTime((data.getStartTime())));
//        holder.mTxtEndDateTime.setText(Const.getAcReportTime((data.getEndTime())));
//        holder.mTxtStartStaus.setText(data.getStartStatus());
//        holder.mTxtEndStatus.setText(data.getEndStatus());
//
//        long timevalue = 0;
//        if (data.getEndTime() > 0 && data.getStartTime() > 0) {
//            if (data.getEndTime() > data.getStartTime()) {
//                timevalue = data.getEndTime() - data.getStartTime();
//            }
//        } else {
//            timevalue = System.currentTimeMillis() - data.getStartTime();
//        }
//        holder.mTxtDuration.setText(Const.getreportTimeValue(timevalue));
//
//        holder.mTxtStartAdds.setText(data.getStartingAddress());
//        holder.mTxtEndAdds.setText(data.getEndAddress());
//
//
//        SpannableString content = new SpannableString("G-Map");
//        content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
//        holder.mTxtStartMap.setText(content);
//        holder.mTxtEndMap.setText(content);
//
//        holder.mTxtStartMap.setOnClickListener(new View.OnClickListener() {
//            public void onClick(View v) {
//
//                System.out.println("Hi start map  onclick " + position);
//                listener.onStartMapClicked(position);
//            }
//        });
//
//        holder.mTxtEndMap.setOnClickListener(new View.OnClickListener() {
//            public void onClick(View v) {
//
//                System.out.println("Hi end map  onclick " + position);
//                listener.onEndMapClicked(position);
////                    }
//            }
//        });
//
//        return vi;
//    }
//
//    public static class ViewHolder {
//        public TextView mTxtStartDateTime = null, mTxtEndDateTime = null, mTxtStartMap, mTxtEndMap, mTxtStartAdds, mTxtEndAdds, mTxtDuration, mTxtStartStaus, mTxtEndStatus;
//    }
//}