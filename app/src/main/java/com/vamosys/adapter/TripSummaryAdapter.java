package com.vamosys.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.vamosys.model.TripSummaryMobile;
import com.vamosys.utils.Constant;
import com.vamosys.utils.TypefaceUtil;
import com.vamosys.vamos.Const;
import com.vamosys.vamos.R;

import java.util.ArrayList;
import java.util.List;

public class TripSummaryAdapter extends BaseAdapter {

    Context mContext;
    int layoutResourceId;
    List<TripSummaryMobile> list = new ArrayList<TripSummaryMobile>();
    private static LayoutInflater inflater = null;
    Activity myacActivity;
    int width, height;
    ViewHolder holder;

    public TripSummaryAdapter(Activity act, List<TripSummaryMobile> mList) {
//System.out.println("Report adapter called ::::::");
        myacActivity = act;
        list = mList;
        inflater = (LayoutInflater) myacActivity
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
//        final ViewHolder holder;
//        if (convertView == null) {
//            convertView = inflater.inflate(R.layout.report_row, parent, false);
//            holder = new ViewHolder();
//            holder.tvStartTime = (TextView) convertView.findViewById(R.id.tvStartTime);
//            holder.tvState = (TextView) convertView.findViewById(R.id.tvState);
//            holder.tvAddress = (TextView) convertView.findViewById(R.id.tvAddress);
//            convertView.setTag(holder);
//        } else {
//            holder = (ViewHolder) convertView.getTag();
//        }
//        ReportData mReportData = list.get(position);
//
//        System.out.println("address is "+mReportData.getAddress()+"   "+mReportData.getStartTime());
//
//        if (mReportData.getStartTime() != null) {
//            holder.tvStartTime.setText(Const.getReportTimefromserver(mReportData.getStartTime()) + "\n" + mReportData.getState().replace("Location", ""));
//            holder.tvState.setText("");
//            holder.tvAddress.setText(mReportData.getAddress());
//        }
//        return convertView;


        View vi = convertView;

        convertView = null;
        if (convertView == null) {

            vi = inflater.inflate(R.layout.trip_summ_adapter_new_layout, null);
            holder = new ViewHolder();
//            holder.tvStartTime = (TextView) vi.findViewById(R.id.trip_adap_startlocation);
            // holder.tvState = (TextView) vi.findViewById(R.id.trip_adap_startendlocation);
//            holder.tvAddress = (TextView) vi.findViewById(R.id.trip_adap_endlocation);


            holder.mContentLayout = (LinearLayout) vi.findViewById(R.id.trip_summ_adap_cont_layout);
            holder.mTxtContentLayout = (LinearLayout) vi.findViewById(R.id.trip_summ_adap_txt_layout);
            holder.mRightContentLayout = (LinearLayout) vi.findViewById(R.id.trip_summ_adap_right_layout);

            holder.mLeftImage = (ImageView) vi.findViewById(R.id.trip_summ_adap_left_image);
            holder.mRightImage = (ImageView) vi.findViewById(R.id.trip_summ_adap_right_image);

            holder.mView1 = (View) vi.findViewById(R.id.trip_summ_adap_view1);
            holder.mView2 = (View) vi.findViewById(R.id.trip_summ_adap_view2);

            holder.mTxtStartTime = (TextView) vi.findViewById(R.id.trip_summ_adap_start_time_txt);
            holder.mTxtStartAdds = (TextView) vi.findViewById(R.id.trip_summ_adap_start_adds_txt);
            holder.mTxtEndTime = (TextView) vi.findViewById(R.id.trip_summ_adap_end_time_txt);
            holder.mTxtEndAdds = (TextView) vi.findViewById(R.id.trip_summ_adap_end_adds_txt);
            holder.mTxtDistance = (TextView) vi.findViewById(R.id.trip_summ_adap_dist_txt);

            holder.mTxtStartTime.setTypeface(TypefaceUtil.getMyFont(myacActivity.getApplicationContext()));
            holder.mTxtStartAdds.setTypeface(TypefaceUtil.getMyFont(myacActivity.getApplicationContext()));
            holder.mTxtEndTime.setTypeface(TypefaceUtil.getMyFont(myacActivity.getApplicationContext()));
            holder.mTxtEndAdds.setTypeface(TypefaceUtil.getMyFont(myacActivity.getApplicationContext()));
            holder.mTxtDistance.setTypeface(TypefaceUtil.getMyFont(myacActivity.getApplicationContext()));


            screenArrange();
            vi.setTag(holder);


        } else {
            holder = (ViewHolder) vi.getTag();
        }
        /*
         * if the doctor is already present in the dcp list.then it should not
		 * again checked
		 */

        TripSummaryMobile mReportData = list.get(position);

//        System.out.println("address is "+mReportData.getAddress()+"   "+mReportData.getStartTime());

        if (mReportData.getfT() != 0) {
//            holder.tvStartTime.setText(Const.getReportTimefromserver(String.valueOf(mReportData.getfT())) + "\n" + mReportData.getsLoc());
            // holder.tvState.setText("");
//            holder.tvAddress.setText(Const.getReportTimefromserver(String.valueOf(mReportData.gettT())) + "\n" + mReportData.geteLoc());

            holder.mTxtStartTime.setText(Const.getTripTimefromserver(String.valueOf(mReportData.getfT())) + " Start");
        }

        if (mReportData.gettT() != 0) {
            holder.mTxtEndTime.setText(Const.getTripTimefromserver(String.valueOf(mReportData.gettT())) + " End");
        }

        if (mReportData.getsLoc() != null) {
            holder.mTxtStartAdds.setText(mReportData.getsLoc().trim());
        }

        if (mReportData.geteLoc() != null) {
            holder.mTxtEndAdds.setText(mReportData.geteLoc().trim());
        }


        if (mReportData.getDt() != 0) {
            holder.mTxtDistance.setText(mReportData.getDt() + " Km");
        }
        return vi;


    }

//    public void setItemList(ArrayList<ReportData> olist) {
//        this.list = olist;
//        notifyDataSetChanged();
//    }

    static class ViewHolder {
//        TextView tvStartTime;
//        TextView tvState;
//        TextView tvAddress;


        LinearLayout mContentLayout, mTxtContentLayout, mRightContentLayout;
        ImageView mLeftImage, mRightImage;
        View mView1, mView2;
        TextView mTxtStartTime, mTxtStartAdds, mTxtEndTime, mTxtEndAdds,
                mTxtDistance;

    }

    private void screenArrange() {
//        // TODO Auto-generated method stub
        width = Constant.ScreenWidth;
        height = Constant.ScreenHeight;
//
        LinearLayout.LayoutParams adap_content_lay_params = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        adap_content_lay_params.width = width;
//		adap_content_lay_params.height = height * 25 / 100;
        adap_content_lay_params.gravity = Gravity.CENTER;
        holder.mContentLayout.setLayoutParams(adap_content_lay_params);

        LinearLayout.LayoutParams adap_txt_content_lay_params = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        adap_txt_content_lay_params.width = width * 65 / 100;
        //adap_txt_content_lay_params.height = height * 25 / 100;
        adap_txt_content_lay_params.gravity = Gravity.LEFT;
        holder.mTxtContentLayout.setLayoutParams(adap_txt_content_lay_params);

        LinearLayout.LayoutParams adap_right_content_lay_params = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        adap_right_content_lay_params.width = width * 18 / 100;
        //adap_right_content_lay_params.height = height * 25 / 100;
        adap_right_content_lay_params.gravity = Gravity.CENTER;
        holder.mRightContentLayout.setLayoutParams(adap_right_content_lay_params);

        LinearLayout.LayoutParams left_image_params = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        left_image_params.width = width * 15 / 100;
        //left_image_params.height = height * 19 / 100;
        left_image_params.gravity = Gravity.CENTER;
        holder.mLeftImage.setLayoutParams(left_image_params);

        LinearLayout.LayoutParams right_image_params = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        right_image_params.width = width * 10 / 100;
        right_image_params.height = width * 10 / 100;
        // right_image_params.gravity = Gravity.CENTER;
        holder.mRightImage.setLayoutParams(right_image_params);

        LinearLayout.LayoutParams start_time_layout = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        start_time_layout.width = width * 65 / 100;
        //start_time_layout.height = height * 5 / 100;
        start_time_layout.gravity = Gravity.LEFT;
        holder.mTxtStartTime.setLayoutParams(start_time_layout);
        holder.mTxtStartTime.setPadding(width * 1 / 100, height * 1 / 100, width * 1 / 100, 0);
//        holder.mTxtStartAdds.setpa

        LinearLayout.LayoutParams start_adds_layout = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        start_adds_layout.width = width * 65 / 100;
        //start_adds_layout.height = height * 5 / 100;
        start_adds_layout.gravity = Gravity.LEFT;
        holder.mTxtStartAdds.setLayoutParams(start_adds_layout);

        holder.mTxtStartAdds.setPadding(width * 1 / 100, height * 1/2 / 100, width * 1 / 100, height * 1/2 / 100);

        LinearLayout.LayoutParams end_time_layout = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        end_time_layout.width = width * 65 / 100;
        //end_time_layout.height = height * 5 / 100;
        end_time_layout.gravity = Gravity.LEFT;
        holder.mTxtEndTime.setLayoutParams(end_time_layout);
        holder.mTxtEndTime.setPadding(width * 1 / 100, height * 1 / 100, width * 1 / 100, 0);

        LinearLayout.LayoutParams end_adds_layout = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        end_adds_layout.width = width * 65 / 100;
        //end_adds_layout.height = height * 5 / 100;
        end_adds_layout.gravity = Gravity.LEFT;
        holder.mTxtEndAdds.setLayoutParams(end_adds_layout);
        holder.mTxtEndAdds.setPadding(width * 1 / 100, height * 1/2 / 100, width * 1 / 100, height * 1/2 / 100);

        LinearLayout.LayoutParams distance_txt_layout = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        distance_txt_layout.width = width * 18 / 100;
        // distance_txt_layout.height = height * 6 / 100;
        distance_txt_layout.gravity = Gravity.CENTER;
        holder.mTxtDistance.setLayoutParams(distance_txt_layout);
        holder.mTxtDistance.setPadding(width * 1 / 100, height * 1 / 100, 0, 0);

        LinearLayout.LayoutParams head_sublayLine = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        head_sublayLine.width = width * 65 / 100;
        head_sublayLine.height = height * 1 / 3 / 100;
        holder.mView1.setLayoutParams(head_sublayLine);

        LinearLayout.LayoutParams head_sublayLine2 = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        head_sublayLine2.width = width * 1 / 2 / 100;
        //head_sublayLine2.height = height * 25 / 100;
        holder.mView2.setLayoutParams(head_sublayLine2);

        if (width >= 600) {
            holder.mTxtStartTime.setTextSize(16);
            holder.mTxtStartAdds.setTextSize(15);
            holder.mTxtEndTime.setTextSize(16);
            holder.mTxtEndAdds.setTextSize(15);
            holder.mTxtDistance.setTextSize(15);
        } else if (width > 501 && width < 600) {

            holder.mTxtStartTime.setTextSize(15);
            holder.mTxtStartAdds.setTextSize(14);
            holder.mTxtEndTime.setTextSize(15);
            holder.mTxtEndAdds.setTextSize(14);
            holder.mTxtDistance.setTextSize(14);

        } else if (width > 260 && width < 500) {

            holder.mTxtStartTime.setTextSize(14);
            holder.mTxtStartAdds.setTextSize(13);
            holder.mTxtEndTime.setTextSize(14);
            holder.mTxtEndAdds.setTextSize(13);
            holder.mTxtDistance.setTextSize(13);

        } else if (width <= 260) {

            holder.mTxtStartTime.setTextSize(13);
            holder.mTxtStartAdds.setTextSize(12);
            holder.mTxtEndTime.setTextSize(13);
            holder.mTxtEndAdds.setTextSize(12);
            holder.mTxtDistance.setTextSize(12);

        }
    }

//    public static class ViewHolder {
//        public TextView mTxtToday, mTxtTodayValue, mTxtYesterday,
//                mTxtYesterdayValue, mTxtMonth, mTxtMonthValue, mTxtAdapHeader,
//                mColon1, mColon2, mColon3;
//    }
}