package com.vamosys.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.vamosys.model.ReportData;
import com.vamosys.vamos.Const;
import com.vamosys.vamos.R;

import java.util.ArrayList;

/**
 * Created by Smart Team.
 */
public class ReportListAdapter extends ArrayAdapter<ReportData> {
    private ArrayList<ReportData> list;
    private LayoutInflater mInflater = null;
    public ReportListAdapter(Activity oActivity, ArrayList<ReportData> list) {
        super(oActivity, R.layout.report_row);
        this.list = list;
        Activity mActivity = oActivity;
        mInflater = (LayoutInflater) mActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
    @Override
    public int getCount() {
        if (this.list != null) {
            return this.list.size();
        } else {
            return 0;
        }
    }
    @Override
    public ReportData getItem(int position) {
        if (this.list != null) {
            return list.get(position);
        } else {
            return null;
        }
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.report_row, parent, false);
            holder = new ViewHolder();
            holder.tvStartTime = (TextView) convertView.findViewById(R.id.tvStartTime);
            holder.tvState = (TextView) convertView.findViewById(R.id.tvState);
            holder.tvAddress = (TextView) convertView.findViewById(R.id.tvAddress);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        ReportData mReportData = list.get(position);

//        System.out.println(" "+mReportData.getAddress()+"   "+mReportData.getStartTime());

        if (mReportData.getStartTime() != null) {
            holder.tvStartTime.setText(Const.getReportTimefromserver(mReportData.getStartTime()) + "\n" + mReportData.getState().replace("Location", ""));
            holder.tvState.setText("");
            holder.tvAddress.setText(mReportData.getAddress());
        }
        return convertView;
    }
    public void setItemList(ArrayList<ReportData> olist) {
        this.list = olist;
        notifyDataSetChanged();
    }

    static class ViewHolder {
        TextView tvStartTime;
        TextView tvState;
        TextView tvAddress;
    }
}
