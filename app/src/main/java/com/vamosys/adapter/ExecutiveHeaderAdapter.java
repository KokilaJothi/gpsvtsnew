
package com.vamosys.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.vamosys.model.ExecutiveDataPojo;
import com.vamosys.model.FuelFillDto;
import com.vamosys.utils.Constant;
import com.vamosys.utils.CustomLayoutManager;
import com.vamosys.vamos.Const;
import com.vamosys.vamos.ExecutiveRowAdapter;
import com.vamosys.vamos.R;

import java.util.ArrayList;
import java.util.List;

import static com.bluecam.demo.MyApplication.getContext;

public class ExecutiveHeaderAdapter  extends RecyclerView.Adapter<RecyclerView.ViewHolder>{
    ArrayList<ExecutiveDataPojo> mExeData=new ArrayList<>();
    private static final int TYPE_HEADER = 0;
    private static final int TYPE_ITEM = 1;
    Context mcontxt;
    List<Boolean> expand=new ArrayList<>();
    public ExecutiveHeaderAdapter(ArrayList<ExecutiveDataPojo> mExecutive, Context context, List<Boolean> expand) {
        mExeData=mExecutive;
        mcontxt=context;
        this.expand=expand;
    }

    private boolean isPositionHeader(int position) {
        return position == 0;
    }

    private ExecutiveDataPojo getItem(int position) {
        return mExeData.get(position-1);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == TYPE_ITEM) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.executive_report_new_parent_row, parent, false);
            return new RowViewHolder(view);
        } else if (viewType == TYPE_HEADER) {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.executive_report_new_parent_header, parent, false);
            return new HeaderViewHolder(itemView);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        try {
            if (holder instanceof HeaderViewHolder) {
                HeaderViewHolder headerViewHolder = (HeaderViewHolder) holder;
            } else if (holder instanceof RowViewHolder) {
                final RowViewHolder holder1 = (RowViewHolder) holder;
                ExecutiveDataPojo e = getItem(position);
                holder1.txt_vehicleName.setText(e.getExcutiveData().get(0).getShortName());
                holder1.txt_Fdate.setText(e.getFromDate());
                holder1.txt_Tdate.setText(e.getToDate());
                holder1.txt_Kms.setText("" + Float.parseFloat(e.getKms()));
                holder1.txt_park.setText(e.getTotalParkCount());
                holder1.txt_over.setText(e.getOverSpeedCount());
                holder1.txt_running.setText(Const.getVehicleTimeNew(String.valueOf(e.getTotalMoveTime())));
                holder1.txt_idel.setText(Const.getVehicleTimeNew(String.valueOf(e.getTotalIdleTime())));
                ExecutiveRowAdapter eadapter = new ExecutiveRowAdapter(e.getExcutiveData(),mcontxt);
                holder1.rv.setAdapter(eadapter);
                eadapter.notifyDataSetChanged();
//                holder1.lay_rv.setVisibility(View.GONE);
                holder1.lay_rv.setVisibility(View.GONE);
                Log.d("res", "kok 1 " + position + "expand   test  " + expand.get(position));
                if (expand.get(position)) {
                    Log.d("res", "kok 1 " + position + " " + expand.get(position));
                    holder1.lay_rv.setVisibility(View.VISIBLE);
                } else {
                    holder1.imgArrw.setImageResource(R.drawable.ic_black_arrow_down_24);
                }
                holder1.lay_arw.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
//                    if(position!=0){
                        Log.d("res", "kok " + position+" expand " + expand.get(position));
                        if (!expand.get(position)) {
                            if (holder1.lay_rv.getVisibility() == View.GONE) {
                                holder1.lay_rv.setVisibility(View.VISIBLE);
                                expand.set(position, true);
                            }
                        } else {
                            holder1.lay_rv.setVisibility(View.GONE);
                            expand.set(position, false);
                        }
//                        if (getItem(position).getExcutiveData().size() > 0) {
//                            for (int j = 0; j < mExeData.size() + 1; j++) {
//                                if (j == position) {
//                                    Log.d("res", "kok j == position " + position  );
//                                    if (expand.get(position) == 0) {
//                                        Log.d("res", "kok j == position " + position + " rv visible");
//                                        expand.set(position, 1);
//                                        holder1.lay_rv.setVisibility(View.VISIBLE);
//                                        Log.d("res", "kok j == position " + position + " rv visible");
//                                    } else {
//                                        Log.d("res", "kok j == position " + position + " rv gone");
//                                        expand.set(position, 0);
//                                        holder1.lay_rv.setVisibility(View.GONE);
//                                    }
//                                    holder1.imgArrw.setImageResource(R.drawable.ic_black_uparrow_up_24);
//                                } else {
//                                    Log.d("res", "kok j != position " + position);
//                                    if (expand.get(position) == 0) {
//                                        expand.set(position, 1);
//                                        holder1.lay_rv.setVisibility(View.VISIBLE);
//                                        holder1.imgArrw.setImageResource(R.drawable.ic_black_uparrow_up_24);
//                                    } else {
//                                        expand.set(position, 0);
//                                        holder1.lay_rv.setVisibility(View.GONE);
//                                        holder1.imgArrw.setImageResource(R.drawable.ic_black_arrow_down_24);
//                                    }
//                                }
//
//                            }
//                        }
//                        else {
//                            if (expand.get(position) == 0) {
//                                Log.d("res", "kok " + position + " " + expand.get(position));
//                                expand.set(position, 1);
//                                holder1.lay_rv.setVisibility(View.VISIBLE);
//                                holder1.imgArrw.setImageResource(R.drawable.ic_black_uparrow_up_24);
//                            } else if (expand.get(position) == 1) {
//                                Log.d("res", "kok 1 " + position + " " + expand.get(position));
//                                expand.set(position, 0);
//                                holder1.lay_rv.setVisibility(View.GONE);
//                                holder1.imgArrw.setImageResource(R.drawable.ic_black_arrow_down_24);
//                            }
//                        }

//                    }
                    }
                });
            }
        }
        catch (Exception e){
            Log.d("Exception ","e "+e.getMessage());
        }
    }
    @Override
    public int getItemViewType(int position) {
        if (isPositionHeader(position)) {
            return TYPE_HEADER;
        }
        return TYPE_ITEM;
    }
    @Override
    public int getItemCount() {
        return mExeData.size()+1;
    }

    public class RowViewHolder extends RecyclerView.ViewHolder{
        TextView txt_vehicleName,txt_Fdate,txt_Tdate,txt_Kms,txt_park,txt_over,txt_running,txt_idel;
        RecyclerView rv;
        AppCompatImageView imgArrw;
        LinearLayout lay_rv,lay_arw;
        public RowViewHolder(@NonNull View itemView) {
            super(itemView);
            txt_vehicleName=itemView.findViewById(R.id.vehicle_name_textView_row);
            txt_Fdate=itemView.findViewById(R.id.fdate_textView_row);
            txt_Tdate=itemView.findViewById(R.id.tdate_textView_row);
            txt_Kms=itemView.findViewById(R.id.kms_textView_row);
            txt_park=itemView.findViewById(R.id.park_count_textView_row);
            txt_over=itemView.findViewById(R.id.over_speed_count_textView_row);
            txt_running=itemView.findViewById(R.id.running_textView_row);
            txt_idel=itemView.findViewById(R.id.idle_textView_row);
            rv=itemView.findViewById(R.id.rv_sub);
            rv.setLayoutManager(new CustomLayoutManager(mcontxt));
            imgArrw=itemView.findViewById(R.id.imgArow);
            lay_arw=itemView.findViewById(R.id.lay_arrow);
            lay_rv=itemView.findViewById(R.id.lay_rv);
        }
    }

    private class HeaderViewHolder extends RecyclerView.ViewHolder {
        TextView Htxt_vehicleName,Htxt_Fdate,Htxt_Tdate,Htxt_Kms,Htxt_park,Htxt_over,Htxt_running,Htxt_idel;
        public HeaderViewHolder(View view) {
            super(view);
            Htxt_vehicleName=itemView.findViewById(R.id.vehicle_name_textView_header);
            Htxt_Fdate=itemView.findViewById(R.id.fdate_textView_header);
            Htxt_Tdate=itemView.findViewById(R.id.tdate_textView_header);
            Htxt_Kms=itemView.findViewById(R.id.kms_textView_header);
            Htxt_park=itemView.findViewById(R.id.park_count_textView_header);
            Htxt_over=itemView.findViewById(R.id.over_speed_count_textView_header);
            Htxt_running=itemView.findViewById(R.id.running_textView_header);
            Htxt_idel=itemView.findViewById(R.id.idle_textView_header);
        }
    }
}
