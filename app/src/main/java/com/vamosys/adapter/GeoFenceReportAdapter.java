package com.vamosys.adapter;


import android.content.Context;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.vamosys.vamos.Const;
import com.vamosys.vamos.R;

import java.util.HashMap;
import java.util.List;

public class GeoFenceReportAdapter extends BaseExpandableListAdapter {

    private Context _context;
    private List<String> _listDataHeader; // header titles
    // child data in format of header title, child title
    private HashMap<String, List<String>> _listDataChild;


    public GeoFenceReportAdapter(Context context, List<String> listDataHeader,
                                 HashMap<String, List<String>> listChildData) {
        this._context = context;
        this._listDataHeader = listDataHeader;
        this._listDataChild = listChildData;
    }

    @Override
    public Object getChild(int groupPosition, int childPosititon) {
        return this._listDataChild.get(this._listDataHeader.get(groupPosition))
                .get(childPosititon);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(int groupPosition, final int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {

        final String childText = (String) getChild(groupPosition, childPosition);

        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.expandable_list_item, null);
        }

        TextView txtCount = (TextView) convertView
                .findViewById(R.id.txt_count_value);
        TextView txtVehicleNumber = (TextView) convertView
                .findViewById(R.id.txt_vehicle_number_value);
        TextView txtStatus = (TextView) convertView
                .findViewById(R.id.txt_status_value);
        TextView txtDuration = (TextView) convertView
                .findViewById(R.id.txt_duration_value);
        ImageView imgIcon = (ImageView) convertView.findViewById(R.id.img_position_icon);

        txtCount.setText(String.valueOf(childPosition+1));

        String[] geoData = childText.split(":");

        if (geoData[0] != null) {
            txtVehicleNumber.setText(geoData[0]);
        }

        if (geoData[1] != null) {
            txtStatus.setText(geoData[1]);
        }

        if (geoData[2] != null) {
            if (geoData[2].equalsIgnoreCase("P")) {
                imgIcon.setBackgroundResource(R.drawable.grey_custom_marker_icon);
            } else if (geoData[2].equalsIgnoreCase("M")) {
                imgIcon.setBackgroundResource(R.drawable.green_custom_marker_icon);
            } else if (geoData[2].equalsIgnoreCase("U")) {
                imgIcon.setBackgroundResource(R.drawable.red_custom_marker_icon);
            } else if (geoData[2].equalsIgnoreCase("S")) {
                imgIcon.setBackgroundResource(R.drawable.orange_custom_marker_icon);
            }
        }

        if (geoData[3] != null) {
            txtDuration.setText(Const.getreportTimeValue(Long.valueOf(geoData[3])));
        }


//        txtListChild.setText(childText);
        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return this._listDataChild.get(this._listDataHeader.get(groupPosition))
                .size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return this._listDataHeader.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return this._listDataHeader.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
        String headerTitle = (String) getGroup(groupPosition);

        int vehicleCount = (int) getChildrenCount(groupPosition);
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.expandable_list_group, null);
        }

//        TextView lblListHeader = (TextView) convertView
//                .findViewById(R.id.mytextview1);
//        lblListHeader.setTypeface(null, Typeface.BOLD);
//        lblListHeader.setText(headerTitle);

        TextView lblListHeader2 = (TextView) convertView
                .findViewById(R.id.mytextview2);
//        lblListHeader2.setTypeface(null, Typeface.BOLD);
        lblListHeader2.setText(headerTitle);
        TextView lblListHeader3 = (TextView) convertView
                .findViewById(R.id.mytextview3);
        lblListHeader3.setTypeface(null, Typeface.BOLD);
        Log.d("vehicleCount",""+vehicleCount);
        if (vehicleCount > 1) {
            lblListHeader3.setText(vehicleCount + " "+_context.getResources().getString(R.string.vehicles));
        } else {
            lblListHeader3.setText(vehicleCount + " "+_context.getResources().getString(R.string.vehicle));
        }
        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

}
