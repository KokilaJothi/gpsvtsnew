package com.vamosys.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.vamosys.model.ReportData;
import com.vamosys.vamos.Const;
import com.vamosys.vamos.R;

import java.util.ArrayList;
import java.util.List;

public class ReportDataAdapter extends BaseAdapter {

    Context mContext;
    int layoutResourceId;
    List<ReportData> list = new ArrayList<ReportData>();
    private static LayoutInflater inflater = null;
    Activity myacActivity;
    int width, height;
    ViewHolder holder;

    public ReportDataAdapter(Activity act, List<ReportData> mList) {

        myacActivity = act;
        list = mList;
        inflater = (LayoutInflater) myacActivity
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
//        final ViewHolder holder;
//        if (convertView == null) {
//            convertView = inflater.inflate(R.layout.report_row, parent, false);
//            holder = new ViewHolder();
//            holder.tvStartTime = (TextView) convertView.findViewById(R.id.tvStartTime);
//            holder.tvState = (TextView) convertView.findViewById(R.id.tvState);
//            holder.tvAddress = (TextView) convertView.findViewById(R.id.tvAddress);
//            convertView.setTag(holder);
//        } else {
//            holder = (ViewHolder) convertView.getTag();
//        }
//        ReportData mReportData = list.get(position);
//
//        System.out.println("address is "+mReportData.getAddress()+"   "+mReportData.getStartTime());
//
//        if (mReportData.getStartTime() != null) {
//            holder.tvStartTime.setText(Const.getReportTimefromserver(mReportData.getStartTime()) + "\n" + mReportData.getState().replace("Location", ""));
//            holder.tvState.setText("");
//            holder.tvAddress.setText(mReportData.getAddress());
//        }
//        return convertView;


        View vi = convertView;

        convertView = null;
        if (convertView == null) {

            vi = inflater.inflate(R.layout.report_row, null);
            holder = new ViewHolder();
            holder.tvStartTime = (TextView) vi.findViewById(R.id.tvStartTime);
            holder.tvState = (TextView) vi.findViewById(R.id.tvState);
            holder.tvAddress = (TextView) vi.findViewById(R.id.tvAddress);
            vi.setTag(holder);


//            vi = inflater.inflate(R.layout.kms_summary_adapter, null);
//            holder = new ViewHolder();
//            holder.mTxtMonth = (TextView) vi.findViewById(R.id.month_text);
//            holder.mTxtMonthValue = (TextView) vi
//                    .findViewById(R.id.month_date_value);
//            holder.mTxtToday = (TextView) vi.findViewById(R.id.today_text);
//            holder.mTxtTodayValue = (TextView) vi
//                    .findViewById(R.id.today_date_value);
//            holder.mTxtYesterday = (TextView) vi
//                    .findViewById(R.id.yesterday_text);
//            holder.mTxtYesterdayValue = (TextView) vi
//                    .findViewById(R.id.yesterday_date_value);
//            holder.mTxtAdapHeader = (TextView) vi
//                    .findViewById(R.id.list_header_text);
//            holder.mColon1 = (TextView) vi.findViewById(R.id.today_text_colon);
//            holder.mColon2 = (TextView) vi
//                    .findViewById(R.id.yesterday_text_colon);
//            holder.mColon3 = (TextView) vi.findViewById(R.id.month_text_colon);
//            screenArrange();
//            vi.setTag(holder);
//            holder.mTxtMonthValue.setText(String.valueOf(mKmsSummaryList.get(
//                    position).getDistanceMonth()));
//            holder.mTxtTodayValue.setText(String.valueOf(mKmsSummaryList.get(
//                    position).getDistanceToday()));
//            holder.mTxtYesterdayValue.setText(String.valueOf(mKmsSummaryList
//                    .get(position).getDistanceWeek()));
//            holder.mTxtAdapHeader.setText(mKmsSummaryList.get(position)
//                    .getVehicleId());

        } else {
            holder = (ViewHolder) vi.getTag();
        }
        /*
		 * if the doctor is already present in the dcp list.then it should not
		 * again checked
		 */

        ReportData mReportData = list.get(position);

//        System.out.println("address is "+mReportData.getAddress()+"   "+mReportData.getStartTime());

        if (mReportData.getStartTime() != null) {
            holder.tvStartTime.setText(Const.getReportTimefromserver(mReportData.getStartTime()) + "\n" + mReportData.getState().replace("Location", ""));
            holder.tvState.setText("");
            holder.tvAddress.setText(mReportData.getAddress());
        }

        return vi;


    }

//    public void setItemList(ArrayList<ReportData> olist) {
//        this.list = olist;
//        notifyDataSetChanged();
//    }

    static class ViewHolder {
        TextView tvStartTime;
        TextView tvState;
        TextView tvAddress;
    }

//    private void screenArrange() {
//        // TODO Auto-generated method stub
//        width = Constant.ScreenWidth;
//        height = Constant.ScreenHeight;
//
//        LinearLayout.LayoutParams headTxtParams = new LinearLayout.LayoutParams(
//                LinearLayout.LayoutParams.WRAP_CONTENT,
//                LinearLayout.LayoutParams.WRAP_CONTENT);
//        headTxtParams.width = width * 95 / 100;
//        headTxtParams.height = height * 6 / 100;
//        headTxtParams.topMargin = height * 1 / 100;
//        headTxtParams.gravity = Gravity.CENTER;
//        holder.mTxtAdapHeader.setLayoutParams(headTxtParams);
//        holder.mTxtAdapHeader.setPadding(width * 2 / 100, 0, 0, 0);
//        holder.mTxtAdapHeader.setGravity(Gravity.CENTER | Gravity.LEFT);
//
//        LinearLayout.LayoutParams dayParams = new LinearLayout.LayoutParams(
//                LinearLayout.LayoutParams.MATCH_PARENT,
//                LinearLayout.LayoutParams.MATCH_PARENT);
//        dayParams.width = width * 30 / 100;
//        dayParams.height = height * 6 / 100;
//        dayParams.topMargin = height * 1 / 100;
//        dayParams.gravity = Gravity.CENTER;
//        holder.mTxtToday.setLayoutParams(dayParams);
//        holder.mTxtYesterday.setLayoutParams(dayParams);
//        holder.mTxtMonth.setLayoutParams(dayParams);
//
//        holder.mTxtToday.setPadding(width * 2 / 100, 0, 0, 0);
//        holder.mTxtYesterday.setPadding(width * 2 / 100, 0, 0, 0);
//        holder.mTxtMonth.setPadding(width * 2 / 100, 0, 0, 0);
//
//        holder.mTxtToday.setGravity(Gravity.CENTER | Gravity.LEFT);
//        holder.mTxtYesterday.setGravity(Gravity.CENTER | Gravity.LEFT);
//        holder.mTxtMonth.setGravity(Gravity.CENTER | Gravity.LEFT);
//
//        LinearLayout.LayoutParams headTxtColonParams = new LinearLayout.LayoutParams(
//                LinearLayout.LayoutParams.WRAP_CONTENT,
//                LinearLayout.LayoutParams.WRAP_CONTENT);
//        headTxtColonParams.height = height * 6 / 100;
//        headTxtColonParams.topMargin = height * 1 / 100;
//        headTxtColonParams.gravity = Gravity.CENTER;
//        holder.mColon1.setLayoutParams(headTxtColonParams);
//        holder.mColon2.setLayoutParams(headTxtColonParams);
//        holder.mColon3.setLayoutParams(headTxtColonParams);
//
//        holder.mColon1.setGravity(Gravity.CENTER | Gravity.LEFT);
//        holder.mColon2.setGravity(Gravity.CENTER | Gravity.LEFT);
//        holder.mColon3.setGravity(Gravity.CENTER | Gravity.LEFT);
//
//        LinearLayout.LayoutParams dayValueParams = new LinearLayout.LayoutParams(
//                LinearLayout.LayoutParams.MATCH_PARENT,
//                LinearLayout.LayoutParams.MATCH_PARENT);
//        dayValueParams.height = height * 6 / 100;
//        dayValueParams.topMargin = height * 1 / 100;
//        dayValueParams.gravity = Gravity.CENTER;
//        holder.mTxtTodayValue.setLayoutParams(dayValueParams);
//        holder.mTxtMonthValue.setLayoutParams(dayValueParams);
//        holder.mTxtYesterdayValue.setLayoutParams(dayValueParams);
//
//        holder.mTxtTodayValue.setPadding(width * 2 / 100, 0, 0, 0);
//        holder.mTxtMonthValue.setPadding(width * 2 / 100, 0, 0, 0);
//        holder.mTxtYesterdayValue.setPadding(width * 2 / 100, 0, 0, 0);
//
//        holder.mTxtTodayValue.setGravity(Gravity.CENTER | Gravity.LEFT);
//        holder.mTxtMonthValue.setGravity(Gravity.CENTER | Gravity.LEFT);
//        holder.mTxtYesterdayValue.setGravity(Gravity.CENTER | Gravity.LEFT);
//
//        if (width >= 600) {
//            holder.mTxtAdapHeader.setTextSize(16);
//            holder.mTxtToday.setTextSize(16);
//            holder.mTxtYesterday.setTextSize(16);
//            holder.mTxtMonth.setTextSize(16);
//            holder.mTxtMonthValue.setTextSize(16);
//            holder.mTxtTodayValue.setTextSize(16);
//            holder.mTxtYesterdayValue.setTextSize(16);
//            holder.mColon1.setTextSize(16);
//            holder.mColon2.setTextSize(16);
//            holder.mColon3.setTextSize(16);
//
//        } else if (width > 501 && width < 600) {
//            holder.mTxtAdapHeader.setTextSize(15);
//            holder.mTxtToday.setTextSize(15);
//            holder.mTxtYesterday.setTextSize(15);
//            holder.mTxtMonth.setTextSize(15);
//            holder.mTxtMonthValue.setTextSize(15);
//            holder.mTxtTodayValue.setTextSize(15);
//            holder.mTxtYesterdayValue.setTextSize(15);
//            holder.mColon1.setTextSize(15);
//            holder.mColon2.setTextSize(15);
//            holder.mColon3.setTextSize(15);
//        } else if (width > 260 && width < 500) {
//            holder.mTxtAdapHeader.setTextSize(14);
//            holder.mTxtToday.setTextSize(14);
//            holder.mTxtYesterday.setTextSize(14);
//            holder.mTxtMonth.setTextSize(14);
//            holder.mTxtMonthValue.setTextSize(14);
//            holder.mTxtTodayValue.setTextSize(14);
//            holder.mTxtYesterdayValue.setTextSize(14);
//            holder.mColon1.setTextSize(14);
//            holder.mColon2.setTextSize(14);
//            holder.mColon3.setTextSize(14);
//
//        } else if (width <= 260) {
//            holder.mTxtAdapHeader.setTextSize(13);
//            holder.mTxtToday.setTextSize(13);
//            holder.mTxtYesterday.setTextSize(13);
//            holder.mTxtMonth.setTextSize(13);
//            holder.mTxtMonthValue.setTextSize(13);
//            holder.mTxtTodayValue.setTextSize(13);
//            holder.mTxtYesterdayValue.setTextSize(13);
//            holder.mColon1.setTextSize(13);
//            holder.mColon2.setTextSize(13);
//            holder.mColon3.setTextSize(13);
//        }
//    }

//    public static class ViewHolder {
//        public TextView mTxtToday, mTxtTodayValue, mTxtYesterday,
//                mTxtYesterdayValue, mTxtMonth, mTxtMonthValue, mTxtAdapHeader,
//                mColon1, mColon2, mColon3;
//    }
}