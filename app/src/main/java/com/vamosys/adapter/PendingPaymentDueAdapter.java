package com.vamosys.adapter;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.avenues.lib.testotpappnew.PaymentActivity;
import com.vamosys.model.PaymentDueDto;
import com.vamosys.utils.Constant;
import com.vamosys.vamos.Const;
import com.vamosys.vamos.R;

import java.util.List;

import Utility.AvenuesParams;

public class PendingPaymentDueAdapter extends RecyclerView.Adapter<PendingPaymentDueAdapter.ViewHolder> {
    private List<PaymentDueDto> mArrayList;
    //    private ArrayList<AndroidVersion> mFilteredList;
    private Activity myActivity;

    public PendingPaymentDueAdapter(List<PaymentDueDto> arrayList, Activity act) {
        mArrayList = arrayList;
        //  mFilteredList = arrayList;
        myActivity = act;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.pending_payment_overdue_adapter, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int i) {

        if (mArrayList.get(i).getDueDate() != null && Const.daysDiff(mArrayList.get(i).getDueDate().trim()) > 0) {
            viewHolder.tv_payment_due_alert.setText("Payment overdue for " + Const.daysDiff(mArrayList.get(i).getDueDate().trim()) + " days");
        }
        viewHolder.tv_payment_due_amount.setText("Amount : " + mArrayList.get(i).getBalanceAmount());


        viewHolder.mImg_invoice_download.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (mArrayList.get(i).getInvoiceLink() != null) {
                    //open
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(mArrayList.get(i).getInvoiceLink()));
                    myActivity.startActivity(browserIntent);
//                    myActivity.finish();
                }
            }
        });

        viewHolder.mBtnPaynow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mArrayList.get(i).getBalanceAmount() != null && mArrayList.get(i).getBalanceAmount().trim().length() > 0) {
                    Intent intent = new Intent(myActivity, PaymentActivity.class);

                    String mGroupName = null;
                    if (Constant.SELECTED_GROUP.contains(":")) {
                        String[] str_msg_data_array = Constant.SELECTED_GROUP.split(":");
                        mGroupName = str_msg_data_array[1];
                    } else {
                        mGroupName = Constant.SELECTED_GROUP;
                    }


                    intent.putExtra(AvenuesParams.AMOUNT, mArrayList.get(i).getBalanceAmount());

                    intent.putExtra(AvenuesParams.BILLING_NAME, mArrayList.get(i).getCustomerName());
                    intent.putExtra(AvenuesParams.F_CODE, mGroupName);
                    intent.putExtra(AvenuesParams.USER_ID, Constant.SELECTED_USER_ID);


                    myActivity.startActivity(intent);
                }
            }
        });

        viewHolder.mContentLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mArrayList.get(i).getBalanceAmount() != null && mArrayList.get(i).getBalanceAmount().trim().length() > 0) {
                    Intent intent = new Intent(myActivity, PaymentActivity.class);
                    String mGroupName = null;
                    if (Constant.SELECTED_GROUP.contains(":")) {
                        String[] str_msg_data_array = Constant.SELECTED_GROUP.split(":");
                        mGroupName = str_msg_data_array[1];
                    } else {
                        mGroupName = Constant.SELECTED_GROUP;
                    }


                    intent.putExtra(AvenuesParams.AMOUNT, mArrayList.get(i).getBalanceAmount());

                    intent.putExtra(AvenuesParams.BILLING_NAME, mArrayList.get(i).getCustomerName());
                    intent.putExtra(AvenuesParams.F_CODE, mGroupName);
                    intent.putExtra(AvenuesParams.USER_ID, Constant.SELECTED_USER_ID);


                    myActivity.startActivity(intent);
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return mArrayList.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView tv_payment_due_alert, tv_payment_due_amount;
        private ImageView mImg_invoice_download;
        Button mBtnPaynow;
        LinearLayout mContentLayout;

        public ViewHolder(View view) {
            super(view);


            tv_payment_due_alert = (TextView) view.findViewById(R.id.txt_payment_due_alert);
            tv_payment_due_amount = (TextView) view.findViewById(R.id.txt_payment_due_amount);
            mImg_invoice_download = (ImageView) view.findViewById(R.id.img_invoice_download);
            mBtnPaynow = (Button) view.findViewById(R.id.btn_payment_paynow);
            mContentLayout=(LinearLayout)view.findViewById(R.id.pending_payment_overdue_linearlayout);


        }
    }

}