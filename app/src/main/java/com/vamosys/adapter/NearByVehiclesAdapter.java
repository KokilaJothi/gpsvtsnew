package com.vamosys.adapter;

import android.app.Activity;
import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.vamosys.model.NearestVehicle;
import com.vamosys.utils.Constant;
import com.vamosys.utils.TypefaceUtil;
import com.vamosys.vamos.Const;
import com.vamosys.vamos.R;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class NearByVehiclesAdapter extends BaseAdapter {

    Context mContext;
    int layoutResourceId;
    List<NearestVehicle> mNearestVehicleList = new ArrayList<NearestVehicle>();
    private static LayoutInflater inflater = null;
    Activity myacActivity;
    int width, height;
    ViewHolder holder;
    Const cons;

    public NearByVehiclesAdapter(Activity act, List<NearestVehicle> mList) {
        cons = new Const();
        myacActivity = act;
        mNearestVehicleList = mList;
        inflater = (LayoutInflater) myacActivity
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return mNearestVehicleList.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return mNearestVehicleList.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        View vi = convertView;

        convertView = null;
        if (convertView == null) {

            vi = inflater.inflate(R.layout.nearby_vehicle_adapter_layout, null);
            holder = new ViewHolder();
            holder.mTxtNearbyVehicleName = (TextView) vi.findViewById(R.id.id_nearby_vehicle_name);
            holder.mTxtNearbyVehicleAdds = (TextView) vi.findViewById(R.id.id_nearby_vehicle_adds);
            holder.mTxtNearbyVehicleDist = (TextView) vi.findViewById(R.id.id_nearby_vehicle_distance);
            holder.id_adds_layout = (LinearLayout) vi.findViewById(R.id.id_adds_layout);
            holder.id_kms_layout = (LinearLayout) vi.findViewById(R.id.id_kms_layout);
            holder.id_nearby_adapter_layout = (LinearLayout) vi.findViewById(R.id.id_nearby_vehicles_adpter_layout);
            holder.mTxtNearbyVehicleAdds.setTypeface(TypefaceUtil.getMyFont(myacActivity.getApplicationContext()));
            holder.mTxtNearbyVehicleDist.setTypeface(TypefaceUtil.getMyFont(myacActivity.getApplicationContext()));
            holder.mTxtNearbyVehicleName.setTypeface(TypefaceUtil.getMyFont(myacActivity.getApplicationContext()));
//            holder.id_vehicle_ac_status.setTypeface(TypefaceUtil.getMyFont(myacActivity.getApplicationContext()));
//            holder.id_vehicles_layout = (LinearLayout) vi.findViewById(R.id.id_vehicles_layout);
//            holder.id_sub_vehicles_layout = (LinearLayout) vi.findViewById(R.id.id_sub_vehicles_layout);
            screenArrange();
            vi.setTag(holder);

//            if (mVehicleLocationList.get(position).getStatus().equals("ON")) {
//                holder.id_lastseen_vehicle.setTextAppearance(myacActivity, R.style.vehicles_list_style_normal);
//            } else {
//                holder.id_lastseen_vehicle.setTextAppearance(myacActivity, R.style.offline_vehicles_list_style_normal);
//            }
//            holder.id_lastseen_vehicle.setText(mVehicleLocationList.get(position).getShortName() + " " + "(" + mVehicleLocationList.get(position).getLastSeen() + ")");
//            holder.id_lastseen_address.setText(mVehicleLocationList.get(position).getAddress());
//            String mShortName = mVehicleLocationList.get(position).getShortName();
//            String vehicle_position = null, vehicle_position_timimg = null;
//
//
//            vehicle_position = mVehicleLocationList.get(position).getPosition();
//            if (vehicle_position.equalsIgnoreCase("P")) {
//                vehicle_position_timimg = String.valueOf(mVehicleLocationList.get(position).getParkedTime());
//                holder.id_vehicle_position_timimg.setText("Parked " + cons.getVehicleTime(vehicle_position_timimg));
//            } else if (vehicle_position.equalsIgnoreCase("M")) {
//                vehicle_position_timimg = String.valueOf(mVehicleLocationList.get(position).getMovingTime());
//                // vehicle_position_timimg = getVehicleInfo(jsonChild.get(childPosition).toString(), "movingTime");
//                holder.id_vehicle_position_timimg.setText("Moving " + cons.getVehicleTime(vehicle_position_timimg));
//            } else if (vehicle_position.equalsIgnoreCase("U")) {
//                vehicle_position_timimg = String.valueOf(mVehicleLocationList.get(position).getNoDataTime());
//                //vehicle_position_timimg = getVehicleInfo(jsonChild.get(childPosition).toString(), "noDataTime");
//                holder.id_vehicle_position_timimg.setText("No Data " + cons.getVehicleTime(vehicle_position_timimg));
//            } else if (vehicle_position.equalsIgnoreCase("S")) {
//                vehicle_position_timimg = String.valueOf(mVehicleLocationList.get(position).getIdleTime());
//                // vehicle_position_timimg = getVehicleInfo(jsonChild.get(childPosition).toString(), "idleTime");
//                holder.id_vehicle_position_timimg.setText("Idle " + cons.getVehicleTime(vehicle_position_timimg));
//            } else {
//                holder.id_vehicle_position_timimg.setText("-");
//            }
//
//            if (mVehicleLocationList.get(position).getVehicleBusy() != null) {
//                if (mVehicleLocationList.get(position).getVehicleBusy().toString().trim().equalsIgnoreCase("yes")) {
//                    holder.id_vehicle_ac_status.setVisibility(View.VISIBLE);
//                    holder.id_vehicle_ac_status.setText("A/C Status : ON");
//                } else {
//                    holder.id_vehicle_ac_status.setVisibility(View.GONE);
////                    holder.id_vehicle_ac_status.setVisibility(View.VISIBLE);
////                    holder.id_vehicle_ac_status.setText("A/C Status : " + mVehicleLocationList.get(position).getVehicleBusy());
//                }
//            } else {
//                holder.id_vehicle_ac_status.setVisibility(View.GONE);
//            }
//
//            if (mVehicleLocationList.get(position).getVehicleType().toString().equalsIgnoreCase("Bus")) {
//                holder.vehicle_type.setImageDrawable(setVehicleImage(myacActivity.getResources().getDrawable(R.drawable.bus_east), vehicle_position));
//            } else if (mVehicleLocationList.get(position).getVehicleType().toString().equalsIgnoreCase("Truck")) {
//                holder.vehicle_type.setImageDrawable(setVehicleImage(myacActivity.getResources().getDrawable(R.drawable.truck_east), vehicle_position));
//            } else if (mVehicleLocationList.get(position).getVehicleType().toString().equalsIgnoreCase("Car")) {
//                holder.vehicle_type.setImageDrawable(setVehicleImage(myacActivity.getResources().getDrawable(R.drawable.car_east), vehicle_position));
//            } else if (mVehicleLocationList.get(position).getVehicleType().toString().equalsIgnoreCase("Cycle")) {
//                holder.vehicle_type.setImageDrawable(setVehicleImage(myacActivity.getResources().getDrawable(R.drawable.cycle_east), vehicle_position));
//            } else if (mVehicleLocationList.get(position).getVehicleType().toString().equalsIgnoreCase("Jcb")) {
//                holder.vehicle_type.setImageDrawable(setVehicleImage(myacActivity.getResources().getDrawable(R.drawable.jcb_icon), vehicle_position));
//            }


        } else {
            holder = (ViewHolder) vi.getTag();
        }

        if (mNearestVehicleList.get(position).getVehicleId().equalsIgnoreCase(Constant.SELECTED_VEHICLE_ID)) {
            holder.id_nearby_adapter_layout.setBackgroundColor(myacActivity.getResources().getColor(R.color.light_green));
        } else {

        }

        if (mNearestVehicleList.get(position).getShortName() != null) {

            if (mNearestVehicleList.get(position).getVehicleId().equalsIgnoreCase(Constant.SELECTED_VEHICLE_ID)) {
                holder.mTxtNearbyVehicleName.setText(mContext.getResources().getString(R.string.selected_vehicle)+"\n" + mNearestVehicleList.get(position).getShortName());
            } else {
                holder.mTxtNearbyVehicleName.setText(mNearestVehicleList.get(position).getShortName());
            }


        }

        if (mNearestVehicleList.get(position).getAddress() != null && mNearestVehicleList.get(position).getAddress().trim().length() > 0) {
            holder.mTxtNearbyVehicleAdds.setText(mNearestVehicleList.get(position).getAddress());
        } else {
//get adds from geocoder

            new GetAddressTask(holder.mTxtNearbyVehicleAdds).execute(mNearestVehicleList.get(position).getLatitude(), mNearestVehicleList.get(position).getLongitude(), 2.0);

        }
        holder.mTxtNearbyVehicleDist.setText((double) Math.round(mNearestVehicleList.get(position).getDistance() * 10d) / 10d + "\n KMs away");


        return vi;
    }

//    private Drawable setVehicleImage(Drawable drawable, String vehicle_position) {
//        //  Log.d(TAG, "setVehicleImage() called with: " + "vehicle_position = [" + vehicle_position + "]");
//        if (vehicle_position.equalsIgnoreCase("P")) {
//            return setVehicleDrawable(drawable, myacActivity.getResources().getColor(R.color.black));
//        } else if (vehicle_position.equalsIgnoreCase("M")) {
//            return setVehicleDrawable(drawable, myacActivity.getResources().getColor(R.color.green));
//        } else if (vehicle_position.equalsIgnoreCase("U")) {
//            return setVehicleDrawable(drawable, myacActivity.getResources().getColor(R.color.red));
//        } else if (vehicle_position.equalsIgnoreCase("S")) {
//            return setVehicleDrawable(drawable, myacActivity.getResources().getColor(R.color.yellow));
//        } else {
//            return setVehicleDrawable(drawable, myacActivity.getResources().getColor(R.color.grey));
//        }
//    }
//
//    private Drawable setVehicleDrawable(Drawable d, int color) {
//        Drawable wrappedDrawable = DrawableCompat.wrap(d);
//        DrawableCompat.setTint(wrappedDrawable, color);
//        return wrappedDrawable;
//    }


    private class GetAddressTask extends AsyncTask<Double, String, String> {
        TextView currenta_adddress_view;
        double getlat, getlng;
        double address_type;

        public GetAddressTask(TextView txtview) {
            super();
            currenta_adddress_view = txtview;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        /**
         * Get a Geocoder instance, get the latitude and longitude
         * look up the address, and return it
         *
         * @return A string containing the address of the current
         * location, or an empty string if no address can be found,
         * or an error message
         * @params params One or more Location objects
         */
        @Override
        protected String doInBackground(Double... params) {
            getlat = params[0];
            getlng = params[1];
            address_type = params[2];
            Geocoder geocoder = new Geocoder(myacActivity.getApplicationContext(), Locale.getDefault());
            List<Address> addresses = null;
            try {
                addresses = geocoder.getFromLocation(params[0], params[1], 1);
            } catch (IOException e1) {
                e1.printStackTrace();
            } catch (IllegalArgumentException e2) {
                // Error message to post in the log
                String errorString = "Illegal arguments " +
                        Double.toString(params[0]) +
                        " , " +
                        Double.toString(params[1]) +
                        " passed to address service";
                e2.printStackTrace();
                return errorString;
            } catch (NullPointerException np) {
                // TODO Auto-generated catch block
                np.printStackTrace();
            }
            // If the reverse geocode returned an address
            if (addresses != null && addresses.size() > 0) {
                // Get the first address
                Address address = addresses.get(0);
                /*
                 * Format the first line of address (if available),
                 * city, and country name.
                 */
                String addressText = null;
                StringBuffer addr = new StringBuffer();
                for (int i = 0; i < address.getMaxAddressLineIndex(); i++) {
                    addressText = address.getAddressLine(i);
                    addr.append(addressText + ",");
                }
                // Return the text
                return addr.toString();
            } else {
                return mContext.getResources().getString(R.string.no_address);
            }
        }

        protected void onProgressUpdate(String... progress) {
        }

        protected void onPostExecute(String result) {
            // sourceedit.setText(result);
            if (result != null && !result.isEmpty()) {
                if (!result.equals("IO Exception trying to get address") || !result.equals("No address found")) {
                    /***
                     * 1.0 - vehicles current address when a vehicle is clicked
                     * 2.0 - vehicles origin address in history selection
                     * 3.0 - vehicles destination address in history selection
                     * 6.0 - vehicles current address when pause button is clicked in history selection
                     */

//                    System.out.println("The address is ::::" + result);

//                    if (address_type == 1.0) {
//                        SharedPreferences.Editor editor = sp.edit();
//                        editor.putString("address", result);
//                        editor.commit();
                    currenta_adddress_view.setText(result.equals("null") ? mContext.getResources().getString(R.string.no_data) : result);
//                    } else if (address_type == 2.0) {
//                        SharedPreferences.Editor editor = sp.edit();
//                        editor.putString("vehicle_origin_address", result);
//                        editor.commit();
//                        history_start_location_value.setText(result.equals("null") ? "No Data" : result);
//                    } else if (address_type == 3.0) {
//                        SharedPreferences.Editor editor = sp.edit();
//                        editor.putString("vehicle_destination_address", result);
//                        editor.commit();
//                        history_end_location_value.setText(result.equals("null") ? "No Data" : result);
//                    } else if (address_type == 4.0) {
//                        SharedPreferences.Editor editor = sp.edit();
//                        editor.putString("current_vehicle_address", result);
//                        editor.commit();
//                        currenta_adddress_view.setText(result.equals("null") ? "No Data" : result);
//                    } else if (address_type == 5.0) {
//                        currenta_adddress_view.setText(result.equals("null") ? "No Data" : result);
//                    } else if (address_type == 6.0) {
//                        currenta_adddress_view.setText(result.equals("null") ? "No Data" : result);
//                    }
                }
            } else {
                currenta_adddress_view.setText(mContext.getResources().getString(R.string.no_address));
//                Toast empty_fav = Toast.makeText(getApplicationContext(), "Please Try Again", Toast.LENGTH_LONG);
//                empty_fav.show();
            }
        }
    }


    public static class ViewHolder {
        public TextView mTxtNearbyVehicleName = null, mTxtNearbyVehicleAdds = null, mTxtNearbyVehicleDist;
        public LinearLayout id_adds_layout, id_kms_layout, id_nearby_adapter_layout;
//        public ImageView vehicle_type;
//        public CheckedTextView checkedtextView;
    }

    public void screenArrange() {

        width = Constant.ScreenWidth;
        height = Constant.ScreenHeight;

        LinearLayout.LayoutParams addressLayoutParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        addressLayoutParams.width = width * 80 / 100;
        holder.id_adds_layout.setLayoutParams(addressLayoutParams);


        LinearLayout.LayoutParams kmLayoutParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        kmLayoutParams.width = width * 20 / 100;
        kmLayoutParams.height = height * 10 / 100;
        holder.id_kms_layout.setLayoutParams(kmLayoutParams);
        holder.id_kms_layout.setGravity(Gravity.CENTER);

        LinearLayout.LayoutParams txtVehicleNameParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        txtVehicleNameParams.width = width * 80 / 100;

        holder.mTxtNearbyVehicleName.setLayoutParams(txtVehicleNameParams);
        holder.mTxtNearbyVehicleName.setPadding(width * 1 / 100, 0, 0, 0);
        holder.mTxtNearbyVehicleName.setGravity(Gravity.CENTER | Gravity.LEFT);

        LinearLayout.LayoutParams txtVehicleAddsParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        txtVehicleAddsParams.width = width * 80 / 100;

        holder.mTxtNearbyVehicleAdds.setLayoutParams(txtVehicleAddsParams);
        holder.mTxtNearbyVehicleAdds.setPadding(width * 1 / 100, 0, 0, 0);
        holder.mTxtNearbyVehicleAdds.setGravity(Gravity.CENTER | Gravity.LEFT);

        LinearLayout.LayoutParams txtKmsParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        txtKmsParams.width = width * 18 / 100;
        txtKmsParams.height = height * 9 / 100;
        txtKmsParams.setMargins(width * 1 / 100, width * 1 / 100, width * 1 / 100, width * 1 / 100);
        holder.mTxtNearbyVehicleDist.setLayoutParams(txtKmsParams);
        holder.mTxtNearbyVehicleDist.setGravity(Gravity.CENTER);
        holder.mTxtNearbyVehicleDist.setPadding(width * 1 / 100, width * 1 / 100, width * 1 / 100, width * 1 / 100);

    }
}