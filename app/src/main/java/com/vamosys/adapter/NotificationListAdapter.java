package com.vamosys.adapter;

import android.app.Activity;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.vamosys.model.PushNotificationDto;
import com.vamosys.utils.Constant;
import com.vamosys.utils.TypefaceUtil;
import com.vamosys.vamos.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;

/**
 * @author prabhakaran
 */
public class NotificationListAdapter extends BaseAdapter {
    Activity myAct;
    LayoutInflater l_Inflater;
    ArrayList<PushNotificationDto> detailList;

    public NotificationListAdapter(Activity context,
                                   List<PushNotificationDto> prList) {
        // TODO Auto-generated constructor stub
        detailList = (ArrayList<PushNotificationDto>) prList;
        l_Inflater = LayoutInflater.from(context);
        myAct = context;
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return detailList.size();
    }

    @Override
    public Object getItem(int pos) {
        // TODO Auto-generated method stub
        return detailList.get(pos);
    }

    @Override
    public long getItemId(int pos) {
        // TODO Auto-generated method stub
        return pos;
    }

    @Override
    public View getView(final int pos, View v, ViewGroup arg2) {
        // TODO Auto-generated method stub
        final ViewHolder holder;
        if (v == null) {
            v = l_Inflater.inflate(R.layout.push_list_item, null);
            holder = new ViewHolder();

            holder.txt_name = (TextView) v.findViewById(R.id.push_txt2);
            holder.txt_time = (TextView) v.findViewById(R.id.noti_time_txt);
            holder.mTypeImage = (ImageView) v.findViewById(R.id.noti_type_image);
            holder.txt_location = (TextView)v.findViewById(R.id.push_txt3);
            holder.mDeleteChkBox = (CheckBox) v
                    .findViewById(R.id.ad_adap_checkbox);
            v.setTag(holder);
        } else {
            holder = (ViewHolder) v.getTag();
        }
        // holder.txt_id.setText(detailList.get(pos).getId());
        String push_txt = null;
         Log.d("msgcontent",""+ Constant.mPushDataListAdapter.get(pos).getMsgContent());


         if (Constant.mPushDataListAdapter.get(pos).getMsgContent().contains("GeoCode")) {
            String[] str_msg_data_array = Constant.mPushDataListAdapter.get(pos).getMsgContent().split("GeoCode:");

            push_txt = str_msg_data_array[0];
        }else {

            push_txt = Constant.mPushDataListAdapter.get(pos).getMsgContent();
      }
        if (!Constant.mPushDataListAdapter.get(pos).getMsgContent().contains("GeoCode")){
            String str_msg_data_array = Constant.mPushDataListAdapter.get(pos).getMsgContent();
            Log.d("geocodecontains",""+str_msg_data_array);
            String sourcelocation = "<b>" + str_msg_data_array + "</b> ";
            holder.txt_location.setVisibility(View.GONE);
           // holder.txt_location.setText(Html.fromHtml(sourcelocation));

        }else if (Constant.mPushDataListAdapter.get(pos).getMsgContent().contains("GeoCode")){
            holder.txt_location.setVisibility(View.VISIBLE);
            String[] str_msg_data_array = Constant.mPushDataListAdapter.get(pos).getMsgContent().split("GeoCode:");
            Log.d("geocodecontains1",""+str_msg_data_array[1]);
            String sourcelocation = "<b>" + str_msg_data_array[1] + "</b> ";
            holder.txt_location.setText(Html.fromHtml("GeoCode:"+sourcelocation));
        }





        SimpleDateFormat myFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        //Date date = new Date();

        //String toDate = myFormat.format(date);

        long diff = 0;

        try {
            Date date1 = myFormat.parse(Constant.mPushDataListAdapter.get(pos).getTimeStamp());
            //Date date2 = myFormat.parse(toDate);
            //diff = ((date2.getTime() - date1.getTime()));

            // System.out.println("The diff is :::::" + diff);

            String toDate = myFormat.format(date1);
            holder.txt_time.setText(toDate);

//			System.out.println("Days: "
//					+ TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS));
        } catch (ParseException e) {
            e.printStackTrace();
        }


        // holder.mDeleteChkBox.setChecked(checkAlreadyCheckedStatus(detailList
        // .get(pos).getId()));

        if (checkAlreadyCheckedStatus(Constant.mPushDataListAdapter.get(pos).getId())) {
            holder.mDeleteChkBox.setChecked(true);
        } else {
            holder.mDeleteChkBox.setChecked(false);
        }


        holder.mDeleteChkBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                final boolean isChecked = holder.mDeleteChkBox.isChecked();
//                System.out.println("check checked status is :::::"
//                        + isChecked);

                PushNotificationDto m = new PushNotificationDto();
                m.setAlreadySelected(isChecked);
                m.setId(Constant.mPushDataListAdapter.get(pos).getId());
                m.setMsgContent(Constant.mPushDataListAdapter.get(pos).getMsgContent());
                m.setTimeStamp(Constant.mPushDataListAdapter.get(pos).getTimeStamp());

                updateManagerList(m);
            }
        });

        String mPushFinalData = null, mPushFinalData2 = null;
        if (push_txt.contains(".")) {
           // String[] str_msg_data_array1 = push_txt.trim().split("\\.");
            String[] str_msg_data_array1 = push_txt.trim().split("");
            System.out.println("the bold text size is ::::" + str_msg_data_array1.length + " " + push_txt);

            mPushFinalData = str_msg_data_array1[0];

            // System.out.println("the bold text is ::::" + mPushFinalData);

            if (str_msg_data_array1.length > 1) {
                mPushFinalData2 = null;
                for (int i = 1; i < str_msg_data_array1.length; i++) {
                    if (mPushFinalData2 != null) {
                        mPushFinalData2 += str_msg_data_array1[i];
                    } else {
                        mPushFinalData2 = str_msg_data_array1[i];
                    }
                }
            }

        } else {
            mPushFinalData = push_txt;
        }

//        final SpannableStringBuilder str = new SpannableStringBuilder(mPushFinalData);
//        str.setSpan(new android.text.style.StyleSpan(android.graphics.Typeface.BOLD), 0, mPushFinalData.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
//        str.setSpan(new ForegroundColorSpan(Color.BLUE), 0, mPushFinalData.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);


        String sourceString = "<b>" + mPushFinalData + "</b> ";
        //mytextview.setText(Html.fromHtml(sourceString));


        if (sourceString != null) {
            if (mPushFinalData2 != null) {
                if (mPushFinalData !=null){
                    holder.txt_name.setText(mPushFinalData2);
                }else {
                    holder.txt_name.setText(Html.fromHtml(sourceString) + ". " + mPushFinalData2);
                }

            } else {
                holder.txt_name.setText(Html.fromHtml(sourceString) + ".");
            }
        } else {
            holder.txt_name.setText(push_txt);
        }

        holder.txt_name.setTypeface(TypefaceUtil.getMyFont(myAct));
        holder.txt_time.setTypeface(TypefaceUtil.getMyFont(myAct));
        holder.txt_location.setTypeface(TypefaceUtil.getMyFont(myAct));

        if (Pattern
                .compile(Pattern.quote("power cut off"), Pattern.CASE_INSENSITIVE)
                .matcher(Constant.mPushDataListAdapter.get(pos).getMsgContent())
                .find()) {
            holder.mTypeImage.setImageDrawable(myAct.getResources().getDrawable(R.drawable.alarm));

        } else if (Pattern
                .compile(Pattern.quote("ignition on"), Pattern.CASE_INSENSITIVE)
                .matcher(Constant.mPushDataListAdapter.get(pos).getMsgContent())
                .find()) {
            holder.mTypeImage.setImageDrawable(myAct.getResources().getDrawable(R.drawable.ignition_on));

        } else if (Pattern
                .compile(Pattern.quote("ignition off"), Pattern.CASE_INSENSITIVE)
                .matcher(Constant.mPushDataListAdapter.get(pos).getMsgContent())
                .find()) {
            holder.mTypeImage.setImageDrawable(myAct.getResources().getDrawable(R.drawable.ignition_off));
        } else if (Pattern
                .compile(Pattern.quote("location alert"), Pattern.CASE_INSENSITIVE)
                .matcher(Constant.mPushDataListAdapter.get(pos).getMsgContent())
                .find()) {
            holder.mTypeImage.setImageDrawable(myAct.getResources().getDrawable(R.drawable.location));
        } else if (Pattern
                .compile(Pattern.quote("offline"), Pattern.CASE_INSENSITIVE)
                .matcher(Constant.mPushDataListAdapter.get(pos).getMsgContent())
                .find()) {
            holder.mTypeImage.setImageDrawable(myAct.getResources().getDrawable(R.drawable.offline));
        } else if (Pattern
                .compile(Pattern.quote("offline 2 online"), Pattern.CASE_INSENSITIVE)
                .matcher(Constant.mPushDataListAdapter.get(pos).getMsgContent())
                .find()) {
            holder.mTypeImage.setImageDrawable(myAct.getResources().getDrawable(R.drawable.online));
        } else if (Pattern
                .compile(Pattern.quote("over Speed"), Pattern.CASE_INSENSITIVE)
                .matcher(Constant.mPushDataListAdapter.get(pos).getMsgContent())
                .find()) {
            holder.mTypeImage.setImageDrawable(myAct.getResources().getDrawable(R.drawable.overspeed));
        } else if (Pattern
                .compile(Pattern.quote("site alert"), Pattern.CASE_INSENSITIVE)

                .matcher(Constant.mPushDataListAdapter.get(pos).getMsgContent())
                .find()) {
            holder.mTypeImage.setImageDrawable(myAct.getResources().getDrawable(R.drawable.siteentry1));
        } else if (Pattern
                .compile(Pattern.quote("site exit"), Pattern.CASE_INSENSITIVE)
                .matcher(Constant.mPushDataListAdapter.get(pos).getMsgContent())
                .find()) {
            holder.mTypeImage.setImageDrawable(myAct.getResources().getDrawable(R.drawable.siteexit));
        }

        // holder.txt_name.setText(Html.fromHtml("<h2>Title</h2><br><p>Description here</p>"));
        // Date d = new Date();
        // String time
        // holder.txt_time_stamp.setVisibility(View.INVISIBLE);
        return v;
    }

    public boolean checkAlreadyCheckedStatus(String id) {
        boolean isAlready = false;

        for (int i = 0; i < Constant.mPushDataListAdapter.size(); i++) {
            //  System.out.println("The checked status is 0000000::::" + Constant.mPushDataList.get(i).isAlreadySelected());
            if (Constant.mPushDataListAdapter.get(i).getId().trim().equalsIgnoreCase(id.trim())) {

                //   System.out.println("The id is :::111" + Constant.mPushDataList.get(i).getId());

                if (Constant.mPushDataListAdapter.get(i).isAlreadySelected()) {
                    // System.out.println("The checked status is ::::" + Constant.mPushDataList.get(i).isAlreadySelected());
                    // Constant.DOCTOR_CHEMIST_LIST.remove(i);
                    isAlready = true;
                }
            }

        }

        return isAlready;
    }

    public void updateManagerList(PushNotificationDto m) {
        for (int i = 0; i < Constant.mPushDataListAdapter.size(); i++) {
            if (Constant.mPushDataListAdapter.get(i).getId().equalsIgnoreCase(m.getId().trim())) {

                // System.out.println("Updated manager name is :::::"
                // + m.getManagerId() + " :::" + m.getManagerName()
                // + "::::" + m.is_isChecked());

                // System.out.println("The updated id is :::::" + m.getId() + " " + m.getMsgContent()+" "+m.isAlreadySelected());

                Constant.mPushDataListAdapter.set(i, m);
            } else {

            }
        }
    }

    static class ViewHolder {

        // TextView txt_id;
        TextView txt_name,txt_location, txt_time;
        CheckBox mDeleteChkBox;
        ImageView mTypeImage;
        // TextView txt_time_stamp;

    }

}
