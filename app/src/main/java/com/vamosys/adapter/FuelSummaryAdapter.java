package com.vamosys.adapter;

import android.content.Context;
import android.graphics.Color;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.vamosys.model.FuelFillDto;
import com.vamosys.model.FuelSumReportDto;
import com.vamosys.vamos.Const;
import com.vamosys.vamos.FuelSummaryNew;
import com.vamosys.vamos.R;

import java.util.ArrayList;
import java.util.List;

public class FuelSummaryAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{
    Context mContext;
    List<FuelSumReportDto> fuelList=new ArrayList<>();
    private static final int TYPE_HEADER = 0;
    private static final int TYPE_ITEM = 1;
    private static final int TYPE_ITEM2 = 2;

    public FuelSummaryAdapter(Context fuelSummaryNew, List<FuelSumReportDto> fuelDataList) {
        Log.d("res","adapter "+fuelDataList.size());
        mContext=fuelSummaryNew;
        fuelList=fuelDataList;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Log.d("fueldata","oncreate");
        if (viewType == TYPE_ITEM) {
            // Here Inflating your recyclerview item layout
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.tank_row_parent, parent, false);
            return new RowViewHolder(view);
        }
//        else if(viewType == TYPE_ITEM2){
//            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.fuel_sum_row2_new, parent, false);
//            return new RowViewHolder1(view);
//        }
        else if (viewType == TYPE_HEADER) {
            // Here Inflating your header view
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.tank_header, parent, false);
            return new HeaderViewHolder(itemView);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        Log.d("fueldata","onBind");
        try {
            if (holder instanceof HeaderViewHolder) {
                HeaderViewHolder headerViewHolder = (HeaderViewHolder) holder;
                headerViewHolder.sensor_textView_header.setText(mContext.getResources().getString(R.string.sensor));
            } else if (holder instanceof RowViewHolder) {
                final RowViewHolder holder1 = (RowViewHolder) holder;
                // Following code give a row of header and decrease the one position from listview items
                FuelSumReportDto f = getItem(position);
                holder1.txt_vehicleName.setText(f.getVehicleName());
                holder1.txt_distance.setText(""+f.getDist());

                if(f.getFuelsensors() != null && f.getFuelsensors().size() > 1){
                    TankSubAdapter adapter =new TankSubAdapter(f.getFuelsensors(),mContext,f.getDist());
                    holder1.rv_loadTank.setAdapter(adapter);
                }
                else {
                    List<FuelSumReportDto.Fuelsensor> fuelsensors=new ArrayList<>();
                    FuelSumReportDto.Fuelsensor fs=new FuelSumReportDto.Fuelsensor();
                    fs.setEndFuel(f.getEndFuel());
                    fs.setFuelConsumption(f.getFuelConsumption());
                    fs.setFuelFilling(f.getFuelFilling());
                    fs.setFuelTheft(f.getFuelTheft());
                    fs.setKmpl(f.getKmpl());
                    fs.setLtrsPerHrs(f.getLtrsPerHrs());
                    fs.setSensor(f.getSensor());
                    fs.setStartFuel(f.getStartFuel());
                    fuelsensors.add(fs);
                    TankSubAdapter adapter =new TankSubAdapter(fuelsensors,mContext,f.getDist());
                    holder1.rv_loadTank.setAdapter(adapter);
                }
                if (f.getDist()==0) {
                    holder1.txt_vehicleName.setTextColor(Color.parseColor("#fa315b"));
                    holder1.txt_distance.setTextColor(Color.parseColor("#fa315b"));
                } else {
                    holder1.txt_vehicleName.setTextColor(Color.parseColor("#000000"));
                    holder1.txt_distance.setTextColor(Color.parseColor("#000000"));
                }
            }
//            else if (holder instanceof RowViewHolder1) {
//                final RowViewHolder1 holder2 = (RowViewHolder1) holder;
//                // Following code give a row of header and decrease the one position from listview items
//                FuelSumReportDto f = getItem(position);
//                holder2.txt_vehicleName.setText(f.getVehicleName());
//                holder2.txt_distance.setText(""+f.getDist());
//                FuelSumReportDto.Fuelsensor fs = f.getFuelsensors().get(0);
//                FuelSumReportDto.Fuelsensor f1 = f.getFuelsensors().get(1);
//                if (f.getFuelsensors() != null && f.getFuelsensors().size() > 0) {
//                    holder2.txt_fuelStart.setText(fs.getStartFuel());
//                    holder2.txt_fuelStart1.setText(f1.getStartFuel());
//                    holder2.txt_fuelFill.setText(fs.getFuelFilling());
//                    holder2.txt_fuelFill1.setText(f1.getFuelFilling());
//                    holder2.txt_fuelCons.setText(fs.getFuelConsumption());
//                    holder2.txt_fuelCons1.setText(f1.getFuelConsumption());
//                    holder2.txt_fuelend.setText(fs.getEndFuel());
//                    holder2.txt_fuelend1.setText(f1.getEndFuel());
//                    holder2.txt_theft.setText(fs.getFuelTheft());
//                    holder2.txt_theft1.setText(f1.getFuelTheft());
//                    holder2.txt_kmpl.setText(fs.getKmpl());
//                    holder2.txt_kmpl1.setText(f1.getKmpl());
//                }
//                if (f.getDist()==0.0) {
//                    holder2.txt_fuelStart.setTextColor(Color.parseColor("#fa315b"));
//                    holder2.txt_fuelStart1.setTextColor(Color.parseColor("#fa315b"));
//                    holder2.txt_fuelFill.setTextColor(Color.parseColor("#fa315b"));
//                    holder2.txt_fuelFill1.setTextColor(Color.parseColor("#fa315b"));
//                    holder2.txt_fuelCons.setTextColor(Color.parseColor("#fa315b"));
//                    holder2.txt_fuelCons1.setTextColor(Color.parseColor("#fa315b"));
//                    holder2.txt_fuelend.setTextColor(Color.parseColor("#fa315b"));
//                    holder2.txt_fuelend1.setTextColor(Color.parseColor("#fa315b"));
//                    holder2.txt_theft.setTextColor(Color.parseColor("#fa315b"));
//                    holder2.txt_theft1.setTextColor(Color.parseColor("#fa315b"));
//                    holder2.txt_kmpl.setTextColor(Color.parseColor("#fa315b"));
//                    holder2.txt_kmpl1.setTextColor(Color.parseColor("#fa315b"));
//                    holder2.txt_vehicleName.setTextColor(Color.parseColor("#fa315b"));
//                    holder2.txt_distance.setTextColor(Color.parseColor("#fa315b"));
//                } else {
//                    holder2.txt_fuelStart.setTextColor(Color.parseColor("#000000"));
//                    holder2.txt_fuelFill.setTextColor(Color.parseColor("#000000"));
//                    holder2.txt_fuelCons.setTextColor(Color.parseColor("#000000"));
//                    holder2.txt_fuelend.setTextColor(Color.parseColor("#000000"));
//                    holder2.txt_theft.setTextColor(Color.parseColor("#000000"));
//                    holder2.txt_kmpl.setTextColor(Color.parseColor("#000000"));
//                    holder2.txt_fuelStart1.setTextColor(Color.parseColor("#000000"));
//                    holder2.txt_fuelFill1.setTextColor(Color.parseColor("#000000"));
//                    holder2.txt_fuelCons1.setTextColor(Color.parseColor("#000000"));
//                    holder2.txt_fuelend1.setTextColor(Color.parseColor("#000000"));
//                    holder2.txt_theft1.setTextColor(Color.parseColor("#000000"));
//                    holder2.txt_kmpl1.setTextColor(Color.parseColor("#000000"));
//                    holder2.txt_vehicleName.setTextColor(Color.parseColor("#000000"));
//                    holder2.txt_distance.setTextColor(Color.parseColor("#000000"));
//                }
//
//            }
        }
        catch (Exception e){

        }

    }

    @Override
    public int getItemViewType(int position) {
        if (isPositionHeader(position)) {
            return TYPE_HEADER;
        }
//        else if(fuelList.get(position-1).getSensor()>1){
//            return TYPE_ITEM2;
//        }
        else{
            return TYPE_ITEM;
        }
    }
    private boolean isPositionHeader(int position) {
        return position == 0;
    }

    private FuelSumReportDto getItem(int position) {
        return fuelList.get(position-1);
    }
    @Override
    public int getItemCount() {
        return fuelList.size()+1;
    }

    public class RowViewHolder extends RecyclerView.ViewHolder{
        TextView txt_vehicleName,txt_distance;
        RecyclerView rv_loadTank;

        public RowViewHolder(@NonNull View itemView) {
            super(itemView);
            txt_vehicleName=itemView.findViewById(R.id.fuelsum_vehicle_textView);
            txt_distance=itemView.findViewById(R.id.fuelsumm_fuel_distance_textView);
            rv_loadTank=itemView.findViewById(R.id.rv_tank);
        }
    }

//    public class RowViewHolder1 extends RecyclerView.ViewHolder{
//        TextView txt_vehicleName,txt_fuelStart,txt_fuelStart1,txt_fuelFill,txt_fuelFill1,txt_fuelCons,txt_fuelCons1,txt_fuelend,txt_fuelend1,txt_theft,txt_theft1,txt_distance,txt_kmpl,txt_kmpl1;
//        public RowViewHolder1(@NonNull View itemView) {
//            super(itemView);
//            txt_vehicleName=itemView.findViewById(R.id.fuelsum_vehicle_textView);
//            txt_fuelStart=itemView.findViewById(R.id.fuelsumm_start_textView1);
//            txt_fuelStart1=itemView.findViewById(R.id.fuelsumm_start_textView2);
//            txt_fuelFill=itemView.findViewById(R.id.fuelsumm_fuel_fill_textView1);
//            txt_fuelFill1=itemView.findViewById(R.id.fuelsumm_fuel_fill_textView2);
//            txt_fuelCons=itemView.findViewById(R.id.fuelsumm_fuel_consume_textView1);
//            txt_fuelCons1=itemView.findViewById(R.id.fuelsumm_fuel_consume_textView2);
//            txt_fuelend=itemView.findViewById(R.id.fuelsumm_end_textView1);
//            txt_fuelend1=itemView.findViewById(R.id.fuelsumm_end_textView2);
//            txt_theft=itemView.findViewById(R.id.fuelsumm_fuel_theft_textView1);
//            txt_theft1=itemView.findViewById(R.id.fuelsumm_fuel_theft_textView2);
//            txt_distance=itemView.findViewById(R.id.fuelsumm_fuel_distance_textView);
//            txt_kmpl=itemView.findViewById(R.id.fuelsumm_fuel_milage_textView1);
//            txt_kmpl1=itemView.findViewById(R.id.fuelsumm_fuel_milage_textView2);
//
//        }
//    }
    private class HeaderViewHolder extends RecyclerView.ViewHolder {
        TextView sensor_textView_header;
        public HeaderViewHolder(View view) {
            super(view);
            sensor_textView_header=view.findViewById(R.id.fuelsumm_sensor_textView_header);
        }
    }

    public class TankSubAdapter extends RecyclerView.Adapter<TankSubAdapter.SubViewHolder> {
        List<FuelSumReportDto.Fuelsensor> tankData=new ArrayList<>();
        Context mcontext;
        private Double dist;

        public TankSubAdapter(List<FuelSumReportDto.Fuelsensor> tankData, Context mContext,Double dist) {
            this.tankData=tankData;
            this.mcontext=mcontext;
            this.dist=dist;
        }

        @NonNull
        @Override
        public TankSubAdapter.SubViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.tank_row_child, parent, false);
            return new TankSubAdapter.SubViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull TankSubAdapter.SubViewHolder holder1, int position) {
            FuelSumReportDto.Fuelsensor f=tankData.get(position);
            if(position==tankData.size()-1){
                holder1.lay_view.setVisibility(View.GONE);
            }
            holder1.txt_fuelStart.setText(f.getStartFuel());
            holder1.txt_fuelFill.setText(f.getFuelFilling());
            holder1.txt_fuelCons.setText(f.getFuelConsumption());
            holder1.txt_fuelend.setText(f.getEndFuel());
            holder1.txt_theft.setText(f.getFuelTheft());
            holder1.txt_kmpl.setText(f.getKmpl());
            holder1.txt_sensor.setText(""+f.getSensor());
            if (dist==0) {
                holder1.txt_fuelStart.setTextColor(Color.parseColor("#fa315b"));
                holder1.txt_fuelFill.setTextColor(Color.parseColor("#fa315b"));
                holder1.txt_fuelCons.setTextColor(Color.parseColor("#fa315b"));
                holder1.txt_fuelend.setTextColor(Color.parseColor("#fa315b"));
                holder1.txt_theft.setTextColor(Color.parseColor("#fa315b"));
                holder1.txt_kmpl.setTextColor(Color.parseColor("#fa315b"));
                holder1.txt_sensor.setTextColor(Color.parseColor("#fa315b"));
            } else {
                holder1.txt_fuelStart.setTextColor(Color.parseColor("#000000"));
                holder1.txt_fuelFill.setTextColor(Color.parseColor("#000000"));
                holder1.txt_fuelCons.setTextColor(Color.parseColor("#000000"));
                holder1.txt_fuelend.setTextColor(Color.parseColor("#000000"));
                holder1.txt_theft.setTextColor(Color.parseColor("#000000"));
                holder1.txt_kmpl.setTextColor(Color.parseColor("#000000"));
                holder1.txt_sensor.setTextColor(Color.parseColor("#000000"));
            }
        }

        @Override
        public int getItemCount() {
            return tankData.size();
        }

        public class SubViewHolder extends RecyclerView.ViewHolder{
            TextView txt_fuelStart,txt_fuelFill,txt_fuelCons,txt_fuelend,txt_theft,txt_kmpl,txt_sensor;
            LinearLayout lay_view;
            public SubViewHolder(@NonNull View itemView) {
                super(itemView);
                txt_fuelStart=itemView.findViewById(R.id.fuelsumm_start_textView);
                txt_fuelFill=itemView.findViewById(R.id.fuelsumm_fuel_fill_textView);
                txt_fuelCons=itemView.findViewById(R.id.fuelsumm_fuel_consume_textView);
                txt_fuelend=itemView.findViewById(R.id.fuelsumm_end_textView);
                txt_theft=itemView.findViewById(R.id.fuelsumm_fuel_theft_textView);
                txt_kmpl=itemView.findViewById(R.id.fuelsumm_fuel_milage_textView);
                lay_view=itemView.findViewById(R.id.lay_view);
                txt_sensor=itemView.findViewById(R.id.fuelsumm_sensor_textView_);
            }
        }
    }

}
