package com.vamosys.adapter;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;

import androidx.core.graphics.drawable.DrawableCompat;

import android.os.Handler;
import android.text.SpannableString;
import android.text.style.RelativeSizeSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckedTextView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.vamosys.model.VehicleData;
import com.vamosys.utils.Constant;
import com.vamosys.utils.DaoHandler;
import com.vamosys.utils.TypefaceUtil;
import com.vamosys.vamos.Const;
import com.vamosys.vamos.DBHelper;
import com.vamosys.vamos.R;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class  VehicleListAdapter extends BaseAdapter{

    Context mContext;
    int layoutResourceId;
    List<VehicleData> mVehicleLocationList = new ArrayList<VehicleData>();
    private static LayoutInflater inflater = null;
    Activity myacActivity;
    int width, height;
    ViewHolder holder;
    Const cons;
    DBHelper dbhelper;
    List<String> mSupportList = new ArrayList<String>();

    public void queryUserDB() {
        DaoHandler da = new DaoHandler(myacActivity, false);
        da.queryUserDB();
        mSupportList = Constant.mSupportDbList;
    }
    public VehicleListAdapter(Activity act, List<VehicleData> mList) {
        cons = new Const();
        myacActivity = act;
        mVehicleLocationList = mList;
        inflater = (LayoutInflater) myacActivity
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        queryUserDB();
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return mVehicleLocationList.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return mVehicleLocationList.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        View vi = convertView;

        convertView = null;
        if (convertView == null) {

            vi = inflater.inflate(R.layout.vehicle_group, null);
            holder = new ViewHolder();
            holder.id_lastseen_vehicle = (TextView) vi.findViewById(R.id.id_vehicle_name);
            holder.id_lastseen_address = (TextView) vi.findViewById(R.id.id_lastseen_vehicle);
            holder.id_lastseen_status=vi.findViewById(R.id.id_lastseen_vehicleStatus);
            holder.id_vehicle_position_timimg = (TextView) vi.findViewById(R.id.id_vehicle_position_timimg);
            holder.id_vehicle_fuel_level = (TextView) vi.findViewById(R.id.id_vehicle_fuel_level);
            holder.id_vehicle_fuel_level1= (TextView) vi.findViewById(R.id.id_vehicle_fuel_level1);
            holder.id_vehicle_fuel_level2=vi.findViewById(R.id.id_vehicle_fuel_level2);
            holder.id_temperature_level = (TextView)vi.findViewById(R.id.id_vehicle_temp_level);
            holder.id_vehicle_ac_status = (TextView) vi.findViewById(R.id.id_vehicle_ac_status);
            holder.id_speed_value = (TextView) vi.findViewById(R.id.vehilcle_speed_value);
            holder.id_distance_value = (TextView) vi.findViewById(R.id.vehilcle_distance_value);
            holder.vehicle_type = (ImageView) vi.findViewById(R.id.id_vehicle_icon);
            holder.id_lastseen_status.setTypeface(TypefaceUtil.getMyFont(myacActivity.getApplicationContext()));
            holder.id_lastseen_address.setTypeface(TypefaceUtil.getMyFont(myacActivity.getApplicationContext()));
            holder.id_lastseen_vehicle.setTypeface(TypefaceUtil.getMyFont(myacActivity.getApplicationContext()));
            holder.id_vehicle_position_timimg.setTypeface(TypefaceUtil.getMyFont(myacActivity.getApplicationContext()));
            holder.id_vehicle_fuel_level.setTypeface(TypefaceUtil.getMyFont(myacActivity.getApplicationContext()));
            holder.id_vehicle_fuel_level1.setTypeface(TypefaceUtil.getMyFont(myacActivity.getApplicationContext()));
            holder.id_vehicle_fuel_level2.setTypeface(TypefaceUtil.getMyFont(myacActivity.getApplicationContext()));
            holder.id_temperature_level.setTypeface(TypefaceUtil.getMyFont(myacActivity.getApplicationContext()));
            holder.id_vehicle_ac_status.setTypeface(TypefaceUtil.getMyFont(myacActivity.getApplicationContext()));
            holder.id_vehicles_layout = (LinearLayout) vi.findViewById(R.id.id_vehicles_layout);
            holder.id_sub_vehicles_layout = (LinearLayout) vi.findViewById(R.id.id_sub_vehicles_layout);
            //screenArrange();
            vi.setTag(holder);

            dbSetup();

//            if (mVehicleLocationList.get(position).getStatus().equals("ON")) {
//                holder.id_lastseen_vehicle.setTextAppearance(myacActivity, R.style.vehicles_list_style_normal);
//            } else {
//                holder.id_lastseen_vehicle.setTextAppearance(myacActivity, R.style.offline_vehicles_list_style_normal);
//            }
//
//
//            String s = mVehicleLocationList.get(position).getShortName() + " " + "(" + mVehicleLocationList.get(position).getLastSeen() + ")";
//            SpannableString ss1 = new SpannableString(s);
//            ss1.setSpan(new RelativeSizeSpan(0.8f), mVehicleLocationList.get(position).getShortName().length(),s.length(), 0); // set size
////            ss1.setSpan(new ForegroundColorSpan(Color.RED), 0, 5, 0);// set color
////            TextView tv= (TextView) findViewById(R.id.textview);
////            tv.setText(ss1);
//
//            holder.id_lastseen_vehicle.setText(ss1);
////            holder.id_lastseen_vehicle.setText(mVehicleLocationList.get(position).getShortName() + " " + "(" + mVehicleLocationList.get(position).getLastSeen() + ")");
//            holder.id_lastseen_address.setText(mVehicleLocationList.get(position).getAddress());
//            String mShortName = mVehicleLocationList.get(position).getShortName();
//            String vehicle_position = null, vehicle_position_timimg = null;
//
//
//            vehicle_position = mVehicleLocationList.get(position).getPosition();
//            if (vehicle_position.equalsIgnoreCase("P")) {
//                vehicle_position_timimg = String.valueOf(mVehicleLocationList.get(position).getParkedTime());
//                holder.id_vehicle_position_timimg.setText("Parked " + cons.getVehicleTime(vehicle_position_timimg));
//            } else if (vehicle_position.equalsIgnoreCase("M")) {
//                vehicle_position_timimg = String.valueOf(mVehicleLocationList.get(position).getMovingTime());
//                // vehicle_position_timimg = getVehicleInfo(jsonChild.get(childPosition).toString(), "movingTime");
//                holder.id_vehicle_position_timimg.setText("Moving " + cons.getVehicleTime(vehicle_position_timimg));
//            } else if (vehicle_position.equalsIgnoreCase("U")) {
//                vehicle_position_timimg = String.valueOf(mVehicleLocationList.get(position).getNoDataTime());
//                //vehicle_position_timimg = getVehicleInfo(jsonChild.get(childPosition).toString(), "noDataTime");
//                holder.id_vehicle_position_timimg.setText("No Data " + cons.getVehicleTime(vehicle_position_timimg));
//            } else if (vehicle_position.equalsIgnoreCase("S")) {
//                vehicle_position_timimg = String.valueOf(mVehicleLocationList.get(position).getIdleTime());
//                // vehicle_position_timimg = getVehicleInfo(jsonChild.get(childPosition).toString(), "idleTime");
//                holder.id_vehicle_position_timimg.setText("Idle " + cons.getVehicleTime(vehicle_position_timimg));
//            } else {
//                holder.id_vehicle_position_timimg.setText("-");
//            }
//
//            if (mVehicleLocationList.get(position).getVehicleBusy() != null) {
//                if (mVehicleLocationList.get(position).getVehicleBusy().toString().trim().equalsIgnoreCase("yes")) {
//                    holder.id_vehicle_ac_status.setVisibility(View.VISIBLE);
//                    holder.id_vehicle_ac_status.setText("A/C Status : ON");
//                } else {
//                    holder.id_vehicle_ac_status.setVisibility(View.GONE);
////                    holder.id_vehicle_ac_status.setVisibility(View.VISIBLE);
////                    holder.id_vehicle_ac_status.setText("A/C Status : " + mVehicleLocationList.get(position).getVehicleBusy());
//                }
//            } else {
//                holder.id_vehicle_ac_status.setVisibility(View.GONE);
//            }
//
//            if (mVehicleLocationList.get(position).getVehicleType().toString().equalsIgnoreCase("Bus")) {
//                holder.vehicle_type.setImageDrawable(setVehicleImage(myacActivity.getResources().getDrawable(R.drawable.bus_east), vehicle_position));
//            } else if (mVehicleLocationList.get(position).getVehicleType().toString().equalsIgnoreCase("Truck")) {
//                holder.vehicle_type.setImageDrawable(setVehicleImage(myacActivity.getResources().getDrawable(R.drawable.truck_east), vehicle_position));
//            } else if (mVehicleLocationList.get(position).getVehicleType().toString().equalsIgnoreCase("Car")) {
//                holder.vehicle_type.setImageDrawable(setVehicleImage(myacActivity.getResources().getDrawable(R.drawable.car_east), vehicle_position));
//            } else if (mVehicleLocationList.get(position).getVehicleType().toString().equalsIgnoreCase("Cycle")) {
//                holder.vehicle_type.setImageDrawable(setVehicleImage(myacActivity.getResources().getDrawable(R.drawable.cycle_east), vehicle_position));
//            } else if (mVehicleLocationList.get(position).getVehicleType().toString().equalsIgnoreCase("Jcb")) {
//                holder.vehicle_type.setImageDrawable(setVehicleImage(myacActivity.getResources().getDrawable(R.drawable.jcb_icon), vehicle_position));
//            }


        } else {

            holder = (ViewHolder) vi.getTag();
        }
//        List<VehicleData> dataList = new ArrayList<VehicleData>();
//        dataList.add(new VehicleData());
//        Collections.sort(dataList);
//        for(VehicleData str:dataList){
//            Log.d("sortlistname",""+str.getShortName());
//        }
    Collections.sort(mVehicleLocationList,new VehicleData());

        if (mVehicleLocationList.get(position).getStatus().equals("ON")) {
            holder.id_lastseen_vehicle.setTextAppearance(myacActivity, R.style.vehicles_list_style_normal);
        } else{
            holder.id_lastseen_vehicle.setTextAppearance(myacActivity, R.style.offline_vehicles_list_style_normal);
        }


//        String s = mVehicleLocationList.get(position).getShortName() + " " + "(" + mVehicleLocationList.get(position).getLastSeen() + ")";
/*
If not working uncomment the above one
 */

        String s = mVehicleLocationList.get(position).getShortName() + " " + "(" + cons.getTripTimefromserver(String.valueOf(mVehicleLocationList.get(position).getDate())) + ")";


        SpannableString ss1 = new SpannableString(s);
        ss1.setSpan(new RelativeSizeSpan(0.8f), mVehicleLocationList.get(position).getShortName().length(), s.length(), 0); // set size
//            ss1.setSpan(new ForegroundColorSpan(Color.RED), 0, 5, 0);// set color
//            TextView tv= (TextView) findViewById(R.id.textview);
//            tv.setText(ss1);
        Log.d("lastseen", "" + ss1);

        holder.id_lastseen_vehicle.setText(ss1);
//            holder.id_lastseen_vehicle.setText(mVehicleLocationList.get(position).getShortName() + " " + "(" + mVehicleLocationList.get(position).getLastSeen() + ")");


        Log.d("response","lic  "+mVehicleLocationList.get(position).getLicenceExpiry()+" exp "+mVehicleLocationList.get(position).getExpiryStatus());
        Log.d("response","expired "+mVehicleLocationList.get(position).getExpired()+" "+mVehicleLocationList.get(position).getShortName());
        String liceStatus=mVehicleLocationList.get(position).getLicenceExpiry();
        String expiryStatus = mVehicleLocationList.get(position).getExpiryStatus();
        String expired=mVehicleLocationList.get(position).getExpired();
        Log.d("response","detail "+mSupportList.size());
        Log.d("response",""+  Constant.mSupportDbList);
        if(expired.equals("No")){
            if(expiryStatus!=null&&expiryStatus.equals("no")){
                if(liceStatus.trim()!=null&&!liceStatus.equals("-"))
                {
                    if (liceStatus.contains(mVehicleLocationList.get(position).getShortName())) {
                        liceStatus=liceStatus.replace(mVehicleLocationList.get(position).getShortName()+" ", "");
                        liceStatus=liceStatus.substring(0, 1).toUpperCase() + liceStatus.substring(1);
                        if(liceStatus.contains("expir")&&mSupportList.size()>0){
                          liceStatus= liceStatus+". "+mSupportList.get(1)+", "+mSupportList.get(2);
                        }
                        Log.d("response  ",liceStatus);
                    }
                    holder.id_lastseen_status.setText(liceStatus);
                    holder.id_lastseen_status.setTextColor(Color.parseColor("#f22031"));
                    holder.id_lastseen_status.setVisibility(View.VISIBLE);
                }
                if (mVehicleLocationList.get(position).getAddress() != null && mVehicleLocationList.get(position).getAddress().trim().length() > 5) {
                    holder.id_lastseen_address.setText(mVehicleLocationList.get(position).getAddress());
                }
                else {
                    Log.d("url","val "+mVehicleLocationList.get(position).getAddress()+" "+mVehicleLocationList.get(position).getExpiryStatus()+" "+mVehicleLocationList.get(position).getLatitude()+" "+mVehicleLocationList.get(position).getLongitude());
//                    new GetAddressTaskNew(holder.id_lastseen_address, Double.parseDouble(mVehicleLocationList.get(position).getLatitude()), Double.parseDouble(mVehicleLocationList.get(position).getLongitude())).execute();
                    getCompleteAddressString(holder.id_lastseen_address,Double.parseDouble(mVehicleLocationList.get(position).getLatitude()),Double.parseDouble(mVehicleLocationList.get(position).getLongitude()),position);
                }
            }
            else {
                if (mVehicleLocationList.get(position).getAddress() != null && mVehicleLocationList.get(position).getAddress().trim().length() > 5) {
                    holder.id_lastseen_address.setText(mVehicleLocationList.get(position).getAddress());
                }
                else {
                    Log.d("url","val "+mVehicleLocationList.get(position).getAddress()+" "+mVehicleLocationList.get(position).getExpiryStatus()+" "+mVehicleLocationList.get(position).getLatitude()+" "+mVehicleLocationList.get(position).getLongitude());
//                    new GetAddressTaskNew(holder.id_lastseen_address, Double.parseDouble(mVehicleLocationList.get(position).getLatitude()), Double.parseDouble(mVehicleLocationList.get(position).getLongitude())).execute();
                    getCompleteAddressString(holder.id_lastseen_address,Double.parseDouble(mVehicleLocationList.get(position).getLatitude()),Double.parseDouble(mVehicleLocationList.get(position).getLongitude()),position);

                }

                if(expiryStatus.contains("expir")&&mSupportList.size()>0){
                    Log.d("response  ",expiryStatus);
                    expiryStatus= expiryStatus+". "+mSupportList.get(1)+", "+mSupportList.get(2);
                }
                holder.id_lastseen_status.setText(expiryStatus);
                holder.id_lastseen_status.setTextColor(Color.parseColor("#f22031"));
                holder.id_lastseen_status.setVisibility(View.VISIBLE);
            }
        }
        else {
            Log.d("response  ","expired yes");
            if (liceStatus.contains(mVehicleLocationList.get(position).getShortName())) {
                liceStatus=liceStatus.replace(mVehicleLocationList.get(position).getShortName()+" ", "");
                liceStatus=liceStatus.substring(0, 1).toUpperCase() + liceStatus.substring(1);
                if(liceStatus.contains("expired")&&mSupportList.size()>0){
                    Log.d("response  ",liceStatus);
                    liceStatus= liceStatus+" "+mSupportList.get(1)+", "+mSupportList.get(2);
                }
                Log.d("response  ",liceStatus);
            }
            holder.id_lastseen_address.setText(liceStatus);
            holder.id_lastseen_address.setTextColor(Color.parseColor("#f22031"));
        }

//        if (mVehicleLocationList.get(position).getAddress() != null && mVehicleLocationList.get(position).getAddress().trim().length() > 5) {
//            Log.d("response","lic  "+mVehicleLocationList.get(position).getLicenceExpiry()+" exp "+mVehicleLocationList.get(position).getExpiryStatus());
//
//            if (expiryStatus.contains("Vehicle expiring")){
//                Log.d("vehicleexpired","sucess");
//                holder.id_lastseen_address.setText(mVehicleLocationList.get(position).getExpiryStatus());
//                holder.id_lastseen_address.setTextColor(Color.parseColor("#f22031"));
//                if(liceStatus.trim()!=null&&!liceStatus.equals("-")){
//                    holder.id_lastseen_status.setText(liceStatus);
//                    holder.id_lastseen_status.setTextColor(Color.parseColor("#f22031"));
//                    holder.id_lastseen_status.setVisibility(View.VISIBLE);
//                }
//                else {
//                    holder.id_lastseen_status.setVisibility(View.GONE);
//                }
//            }
//            else{
//                Log.d("vehicleexpired","failure");
//                holder.id_lastseen_address.setText(mVehicleLocationList.get(position).getAddress());
//                if(liceStatus.trim()!=null&&!liceStatus.equals("-")){
//                    holder.id_lastseen_status.setText(liceStatus);
//                    holder.id_lastseen_status.setTextColor(Color.parseColor("#f22031"));
//                    holder.id_lastseen_status.setVisibility(View.VISIBLE);
//                }
//                else {
//                    holder.id_lastseen_status.setVisibility(View.GONE);
//                }
//            }
//
//        } else {
//            Log.d("url","val "+mVehicleLocationList.get(position).getAddress()+" "+mVehicleLocationList.get(position).getExpiryStatus()+" "+mVehicleLocationList.get(position).getLatitude()+" "+mVehicleLocationList.get(position).getLongitude());
//            new GetAddressTaskNew(holder.id_lastseen_address, Double.parseDouble(mVehicleLocationList.get(position).getLatitude()), Double.parseDouble(mVehicleLocationList.get(position).getLongitude())).execute();
//        }


//        if (mVehicleLocationList.get(position).getAddress() != null && mVehicleLocationList.get(position).getAddress().trim().length() > 5 && containsSpecialCharacter(mVehicleLocationList.get(position).getAddress().trim())) {
//            holder.id_lastseen_address.setText(mVehicleLocationList.get(position).getAddress());
//        } else {
//            new GetAddressTaskNew(holder.id_lastseen_address, Double.parseDouble(mVehicleLocationList.get(position).getLatitude()), Double.parseDouble(mVehicleLocationList.get(position).getLongitude())).execute();
//        }


//        new GetAddressTaskNew(holder.id_lastseen_address, Double.parseDouble(mVehicleLocationList.get(position).getLatitude()), Double.parseDouble(mVehicleLocationList.get(position).getLongitude())).execute();

        String mShortName = mVehicleLocationList.get(position).getShortName();
        String vehicle_position = null, vehicle_position_timimg = null;

        Log.d("drivermobilenumber",""+mVehicleLocationList.get(position).getDriverMobile());


        vehicle_position = mVehicleLocationList.get(position).getPosition();
        Log.d("vehicleposition",""+ mVehicleLocationList.get(position).getPosition());
        if (vehicle_position.equalsIgnoreCase("P")) {
            vehicle_position_timimg = String.valueOf(mVehicleLocationList.get(position).getParkedTime());
            holder.id_vehicle_position_timimg.setText(mContext.getResources().getString(R.string.parked) + cons.getVehicleTime(vehicle_position_timimg));
        } else if (vehicle_position.equalsIgnoreCase("M")) {
            vehicle_position_timimg = String.valueOf(mVehicleLocationList.get(position).getMovingTime());
            // vehicle_position_timimg = getVehicleInfo(jsonChild.get(childPosition).toString(), "movingTime");
            holder.id_vehicle_position_timimg.setText(mContext.getResources().getString(R.string.movving) + cons.getVehicleTime(vehicle_position_timimg));
        } else if (vehicle_position.equalsIgnoreCase("U")) {
            vehicle_position_timimg = String.valueOf(mVehicleLocationList.get(position).getNoDataTime());
            //vehicle_position_timimg = getVehicleInfo(jsonChild.get(childPosition).toString(), "noDataTime");
            holder.id_vehicle_position_timimg.setText(mContext.getResources().getString(R.string.no_data)+" " + cons.getVehicleTime(vehicle_position_timimg));
        } else if (vehicle_position.equalsIgnoreCase("S")) {
            vehicle_position_timimg = String.valueOf(mVehicleLocationList.get(position).getIdleTime());
            // vehicle_position_timimg = getVehicleInfo(jsonChild.get(childPosition).toString(), "idleTime");
            holder.id_vehicle_position_timimg.setText(mContext.getResources().getString(R.string.idle)+" " + cons.getVehicleTime(vehicle_position_timimg));
        } else {
            holder.id_vehicle_position_timimg.setText("-");
        }

        if (mVehicleLocationList.get(position).getVehicleBusy() != null) {
            if (mVehicleLocationList.get(position).getVehicleBusy().toString().trim().equalsIgnoreCase("yes")) {
                holder.id_vehicle_ac_status.setVisibility(View.VISIBLE);
                holder.id_vehicle_ac_status.setText(""+mContext.getResources().getString(R.string.ac_status));
            } else {
                holder.id_vehicle_ac_status.setVisibility(View.GONE);
//                    holder.id_vehicle_ac_status.setVisibility(View.VISIBLE);
//                    holder.id_vehicle_ac_status.setText("A/C Status : " + mVehicleLocationList.get(position).getVehicleBusy());
            }
        } else{
            holder.id_vehicle_ac_status.setVisibility(View.GONE);
        }


//        System.out.println("The pos " + position + " Vehicle short name " + mVehicleLocationList.get(position).getShortName() + " " + " vehicle position " + vehicle_position);


//        if (mVehicleLocationList.get(position).getVehicleType().toString().equalsIgnoreCase("Bus")) {
//            holder.vehicle_type.setImageDrawable(setVehicleImage(myacActivity.getResources().getDrawable(R.drawable.bus_east), vehicle_position));
//        } else if (mVehicleLocationList.get(position).getVehicleType().toString().equalsIgnoreCase("Truck")) {
//            holder.vehicle_type.setImageDrawable(setVehicleImage(myacActivity.getResources().getDrawable(R.drawable.truck_east), vehicle_position));
//        } else if (mVehicleLocationList.get(position).getVehicleType().toString().equalsIgnoreCase("Car")) {
//            holder.vehicle_type.setImageDrawable(setVehicleImage(myacActivity.getResources().getDrawable(R.drawable.car_east), vehicle_position));
//        } else if (mVehicleLocationList.get(position).getVehicleType().toString().equalsIgnoreCase("Cycle")) {
//            holder.vehicle_type.setImageDrawable(setVehicleImage(myacActivity.getResources().getDrawable(R.drawable.cycle_east), vehicle_position));
//        } else if (mVehicleLocationList.get(position).getVehicleType().toString().equalsIgnoreCase("Jcb")) {
//            holder.vehicle_type.setImageDrawable(setVehicleImage(myacActivity.getResources().getDrawable(R.drawable.jcb_icon), vehicle_position));
//        }else if (mVehicleLocationList.get(position).getVehicleType().toString().equalsIgnoreCase("Bike")) {
//            holder.vehicle_type.setImageDrawable(setVehicleImage(myacActivity.getResources().getDrawable(R.drawable.vehicle_bike), vehicle_position));
//        }
        Log.d("vehiclelisttype", "" + mVehicleLocationList.get(position).getVehicleType());
        Log.d("vehiclelistpos", "" + mVehicleLocationList.get(position));


        if (mVehicleLocationList.get(position).getVehicleType().toString().equalsIgnoreCase("Bus")) {
            holder.vehicle_type.setImageDrawable(setVehicleImage(myacActivity.getResources().getDrawable(R.drawable.bus_east), vehicle_position, mVehicleLocationList.get(position).getIgnitionStatus()));
        } else if (mVehicleLocationList.get(position).getVehicleType().toString().equalsIgnoreCase("Truck")) {
            holder.vehicle_type.setImageDrawable(setVehicleImage(myacActivity.getResources().getDrawable(R.drawable.truck_east), vehicle_position, mVehicleLocationList.get(position).getIgnitionStatus()));
        } else if (mVehicleLocationList.get(position).getVehicleType().toString().equalsIgnoreCase("Car")) {
            holder.vehicle_type.setImageDrawable(setVehicleImage(myacActivity.getResources().getDrawable(R.drawable.car_east), vehicle_position, mVehicleLocationList.get(position).getIgnitionStatus()));
        } else if (mVehicleLocationList.get(position).getVehicleType().toString().equalsIgnoreCase("Cycle")) {
            holder.vehicle_type.setImageDrawable(setVehicleImage(myacActivity.getResources().getDrawable(R.drawable.cycle_east), vehicle_position, mVehicleLocationList.get(position).getIgnitionStatus()));
        } else if (mVehicleLocationList.get(position).getVehicleType().toString().equalsIgnoreCase("Jcb")) {
            holder.vehicle_type.setImageDrawable(setVehicleImage(myacActivity.getResources().getDrawable(R.drawable.jcb_icon), vehicle_position, mVehicleLocationList.get(position).getIgnitionStatus()));
        } else if (mVehicleLocationList.get(position).getVehicleType().toString().equalsIgnoreCase("Bike")) {
            holder.vehicle_type.setImageDrawable(setVehicleImage(myacActivity.getResources().getDrawable(R.drawable.vehicle_bike), vehicle_position, mVehicleLocationList.get(position).getIgnitionStatus()));
        } else if (mVehicleLocationList.get(position).getVehicleType().toString().equalsIgnoreCase("HeavyVehicle")) {
            holder.vehicle_type.setImageDrawable(setVehicleImage(myacActivity.getResources().getDrawable(R.drawable.truck_east), vehicle_position, mVehicleLocationList.get(position).getIgnitionStatus()));
        } else if (mVehicleLocationList.get(position).getVehicleType().toString().equalsIgnoreCase("Ambulance")) {
            holder.vehicle_type.setImageDrawable(setVehicleImage(myacActivity.getResources().getDrawable(R.drawable.ambulance), vehicle_position, mVehicleLocationList.get(position).getIgnitionStatus()));
        } else if (mVehicleLocationList.get(position).getVehicleType().toString().equalsIgnoreCase("Van")) {
            holder.vehicle_type.setImageDrawable(setVehicleImage(myacActivity.getResources().getDrawable(R.drawable.van1), vehicle_position, mVehicleLocationList.get(position).getIgnitionStatus()));
        }

        holder.id_speed_value.setText(mVehicleLocationList.get(position).getSpeed() + mContext.getResources().getString(R.string.Km_hr));
        Log.d("fueldata",""+mVehicleLocationList.get(position).getVehicleId()+" "+mVehicleLocationList.get(position).getNoOfTank());
        Log.d("fueldata",""+mVehicleLocationList.get(position).getFuel()+" "+mVehicleLocationList.get(position).getSensorCount()+" "+mVehicleLocationList.get(position).getFuelLitres());
        if(mVehicleLocationList.get(position).getSensorCount()>1&&mVehicleLocationList.get(position).getFuel().equalsIgnoreCase("yes")){
            try {
                if(mVehicleLocationList.get(position).getNoOfTank()>1) {
                    String split[] = mVehicleLocationList.get(position).getFuelLitres().split(":");
                    String splitTtank[] = mVehicleLocationList.get(position).getnTankSize().split(":");
                    double tnk1 = Double.parseDouble(splitTtank[0]);
                    double tnk2 = Double.parseDouble(splitTtank[1]);
                    double fuel1 = Double.parseDouble(split[0]);
                    double fuel2 = Double.parseDouble(split[1]);
                    Log.d("fueldata", "tank size " + (int) tnk1 + " " + (int) tnk2 + " fuel " + (int) fuel1 + " " + (int) fuel2);
                    if (fuel1 >=0) {
                        holder.id_vehicle_fuel_level.setVisibility(View.VISIBLE);
                        holder.id_vehicle_fuel_level.setText(mContext.getResources().getString(R.string.fuelsensor1)+"" + (int) fuel1 +  mContext.getResources().getString(R.string.ltr)+" / " + (int) tnk1);
                    }
                    if (fuel2 >=0) {
                        holder.id_vehicle_fuel_level1.setVisibility(View.VISIBLE);
                        holder.id_vehicle_fuel_level1.setText(mContext.getResources().getString(R.string.fuelSensor2)+"" + (int) fuel2 + mContext.getResources().getString(R.string.ltr)+" / " + (int) tnk2);
                    }
//                    if(mVehicleLocationList.get(position).getSensorCount()>2){
//                        holder.id_vehicle_fuel_level2.setVisibility(View.VISIBLE);
//                        holder.id_vehicle_fuel_level2.setText("Fuel Level (Sensor 3) : " + (int) Double.parseDouble(split[1] + " ltr / " + (int) tnk2);
//                    }
                }
                else {
                    int tanksize = (int) mVehicleLocationList.get(position).getTankSize();
                    holder.id_vehicle_fuel_level.setText(mContext.getResources().getString(R.string.fuelSensor)+"" + mVehicleLocationList.get(position).getFuelLitre() + mContext.getResources().getString(R.string.ltr)+" / " + tanksize);
                    holder.id_vehicle_fuel_level.setVisibility(View.VISIBLE);
                }
            }
            catch (Exception e){
                Log.d("fueldata","er "+e.getMessage());
            }
        }
        else {
            if (mVehicleLocationList.get(position).getFuel().equalsIgnoreCase("yes")) {

                int tanksize = (int) mVehicleLocationList.get(position).getTankSize();
                holder.id_vehicle_fuel_level.setText(mContext.getResources().getString(R.string.fuelSensor)+"" + mVehicleLocationList.get(position).getFuelLitre() + mContext.getResources().getString(R.string.ltr)+" / " + tanksize);
                holder.id_vehicle_fuel_level.setVisibility(View.VISIBLE);

            }
            else {

                holder.id_vehicle_fuel_level.setVisibility(View.GONE);
                Log.d("vehicleserial", "" + mVehicleLocationList.get(position).getSerial1());
            }
        }
        if (mVehicleLocationList.get(position).getSerial1().equalsIgnoreCase("temperature")){
            holder.id_temperature_level.setVisibility(View.VISIBLE);
            if (mVehicleLocationList.get(position).getTemperature() > 0) {

                holder.id_temperature_level.setText(mContext.getResources().getString(R.string.temperature)+"" + mVehicleLocationList.get(position).getTemperature() + mContext.getResources().getString(R.string.degree_c));
                holder.id_temperature_level.setVisibility(View.VISIBLE);
            } else {

                holder.id_temperature_level.setVisibility(View.GONE);
            }
        }else {
            holder.id_temperature_level.setVisibility(View.GONE);
        }



//        } else {
//            holder.id_vehicle_fuel_level.setText("Fuel Level : 0 ltr");
//        }

        int dist = (int) Math.round(mVehicleLocationList.get(position).getDistanceCovered());

//        holder.id_distance_value.setText(dist + " kms");
        //testBlink(position);



        if (mVehicleLocationList.get(position).getDistanceCovered() > 1) {
            holder.id_distance_value.setText(String.format("%.2f", mVehicleLocationList.get(position).getDistanceCovered()) + " "+mContext.getResources().getString(R.string.kms));
        } else {
            holder.id_distance_value.setText(String.format("%.2f", mVehicleLocationList.get(position).getDistanceCovered()) + " "+mContext.getResources().getString(R.string.kms));
        }

        return vi;
    }

//    private void testBlink(int position) {
//        final Handler handler = new Handler();
//        new Thread(new Runnable() {
//            @Override
//            public void run() {
//                int timeToBlink = 1000;
//                try {
//                    Thread.sleep(timeToBlink);
//                } catch (Exception e) {
//                }
//                handler.post(new Runnable() {
//                    @Override
//                    public void run() {
//
//
//                        if (mVehicleLocationList.get(position).getDistanceCovered() > 1) {
//                            holder.id_distance_value.setText(String.format("%.2f", mVehicleLocationList.get(position).getDistanceCovered()) + " Kms");
//                        } else {
//                            holder.id_distance_value.setText(String.format("%.2f", mVehicleLocationList.get(position).getDistanceCovered()) + " Km");
//                        }
//                        testBlink(position);
//                    }
//                });
//            }
//        }).start();
//
//    }


    void dbSetup() {
        dbhelper = new DBHelper(myacActivity);
        try {
            dbhelper.createDataBase();
            dbhelper.openDataBase();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean containsSpecialCharacter(String s) {


        Pattern p = Pattern.compile("[^a-z0-9 ]", Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(s);
        boolean b = m.find();

        return b;


    }

    private Drawable setVehicleImage(Drawable drawable, String vehicle_position, String mIgnitionStatus) {
        Log.d("GPSVTS", "setVehicleImage() called with: " + "vehicle_position = [" + vehicle_position + "]");
        if (vehicle_position.equalsIgnoreCase("P")) {
            // System.out.println("The position is PPPP");
            return setVehicleDrawable(drawable, myacActivity.getResources().getColor(R.color.black));
        } else if (vehicle_position.equalsIgnoreCase("M")) {
            //System.out.println("The position is MMMM");
            if (mIgnitionStatus != null && mIgnitionStatus.trim().equalsIgnoreCase("OFF")) {
                return setVehicleDrawable(drawable, myacActivity.getResources().getColor(R.color.grey));
            } else {
                return setVehicleDrawable(drawable, myacActivity.getResources().getColor(R.color.green));
            }

        } else if (vehicle_position.equalsIgnoreCase("U")) {

            // System.out.println("The position is UUUU");

            return setVehicleDrawable(drawable, myacActivity.getResources().getColor(R.color.red));
        } else if (vehicle_position.equalsIgnoreCase("S")) {
            //System.out.println("The position is SSSS");
            return setVehicleDrawable(drawable, myacActivity.getResources().getColor(R.color.yellow));
        } else {
            //System.out.println("The position is Nothing");
            return setVehicleDrawable(drawable, myacActivity.getResources().getColor(R.color.grey));
        }
    }

    private Drawable setVehicleDrawable(Drawable d, int color) {
        Drawable wrappedDrawable = DrawableCompat.wrap(d);
        DrawableCompat.setTint(wrappedDrawable, color);
        return wrappedDrawable;
    }

    public void screenArrange() {
        width = Constant.ScreenWidth;
        height = Constant.ScreenHeight;
    }

    int mPopupStartCountNew = 1;


    private class GetAddressTaskNew extends AsyncTask<Double, String, String> {
        TextView currenta_adddress_view;
        double mAddsLat, mAddsLng;
//        double address_type;

        public GetAddressTaskNew(TextView txtview, double la, double lo) {
            super();
            currenta_adddress_view = txtview;
            mAddsLat = la;
            mAddsLng = lo;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        /**
         * Get a Geocoder instance, get the latitude and longitude
         * look up the address, and return it
         *
         * @return A string containing the address of the current
         * location, or an empty string if no address can be found,
         * or an error message
         * @params params One or more Location objects
         */
        @Override
        protected String doInBackground(Double... params) {

//            address_type = params[2];
            String addr = null;
            Log.d("url","address "+mAddsLat+" "+mAddsLng);
            try {
                addr = Constant.getAddress(String.valueOf(mAddsLat), String.valueOf(mAddsLng), getStdTableValue("appid"), myacActivity.getString(R.string.address_api));
            } catch (Exception e1) {
                e1.printStackTrace();
            }


            if (addr != null && addr.length() > 0) {
                return addr.toString();
            } else {
                return ""+mContext.getResources().getString(R.string.loadingaddress);
            }

        }

        protected void onProgressUpdate(String... progress) {
        }

        protected void onPostExecute(String result) {
            // sourceedit.setText(result);

//            System.out.println("Hi google address address22 " + " " + result);

            if (result != null && !result.isEmpty()) {
                if (!result.equals("IO Exception trying to get address") || !result.equals("Loading Address...")) {


                    if (result.trim().contains("Loading Address...") || result.equals("IO Exception trying to get address")) {
                        if (mPopupStartCountNew < 10) {
                            new GetAddressTaskNew(currenta_adddress_view, mAddsLat, mAddsLng).execute(mAddsLat, mAddsLng, 4.0);
                        } else {

                            if (currenta_adddress_view.getText().toString().trim() != null && currenta_adddress_view.getText().toString().trim().length() < 5) {

                                currenta_adddress_view.setText(result.equals("null") ? ""+mContext.getResources().getString(R.string.no_data) : result);
                            }
                        }
                        mPopupStartCountNew++;
                    } else {
                        if (currenta_adddress_view.getText().toString().trim() != null && currenta_adddress_view.getText().toString().trim().length() < 5) {

                            currenta_adddress_view.setText(result.equals("null") ? ""+mContext.getResources().getString(R.string.no_data) : result);
                        }
                    }
                }
            } else {
                Toast empty_fav = Toast.makeText(myacActivity, ""+mContext.getResources().getString(R.string.please_try_again), Toast.LENGTH_LONG);
                empty_fav.show();
            }
        }
    }

    public String getStdTableValue(String tag) {
        Cursor std_table_cur = null;
        String stdtablevalues = null;
        try {
            std_table_cur = dbhelper.get_std_table_info(tag);
            std_table_cur.moveToFirst();
            stdtablevalues = std_table_cur.getString(0);
        } catch (Exception e) {
            Toast empty_fav = Toast.makeText(myacActivity, "Something went wrong. Please try again", Toast.LENGTH_LONG);
            empty_fav.show();
        } finally {
            if (std_table_cur != null) {
                std_table_cur.close();
            }
        }
        return stdtablevalues;
    }

    public static class ViewHolder {
        public TextView id_lastseen_vehicle = null, id_lastseen_address = null, id_lastseen_status=null,id_vehicle_position_timimg, id_vehicle_ac_status, id_speed_value, id_distance_value,id_temperature_level, id_vehicle_fuel_level,id_vehicle_fuel_level1,id_vehicle_fuel_level2;
        public LinearLayout id_vehicles_layout, id_sub_vehicles_layout;
        public ImageView vehicle_type;
        public CheckedTextView checkedtextView;
    }

    private void getCompleteAddressString(TextView address,double LATITUDE, double LONGITUDE,int pos) {
        String strAdd = "";
        Geocoder geocoder = new Geocoder(myacActivity, Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(LATITUDE, LONGITUDE, 1);
            if (addresses != null) {
                Address returnedAddress = addresses.get(0);
                StringBuilder strReturnedAddress = new StringBuilder("");

//                if(!TextUtils.isEmpty(returnedAddress.getLocality())){
//                    strReturnedAddress.append(returnedAddress.getLocality()).append(",\n");
//                }
//                if(!TextUtils.isEmpty(returnedAddress.getSubAdminArea())){
//                    strReturnedAddress.append(returnedAddress.getSubAdminArea()).append(",\n");
//                }
//                if(!TextUtils.isEmpty(returnedAddress.getAdminArea())){
//                    strReturnedAddress.append(returnedAddress.getAdminArea()).append(",\n");
//                }
//                if(!TextUtils.isEmpty(returnedAddress.getFeatureName())){
//                    strReturnedAddress.append(returnedAddress.getFeatureName());
//                }
//                Log.d("con","result"+ strReturnedAddress.toString());
//
//                if(!(strReturnedAddress.toString()!=null)&&!(strReturnedAddress.toString().length()>7)){
//                    for (int i = 0; i <= returnedAddress.getMaxAddressLineIndex(); i++) {
//                        Log.d("con", returnedAddress.getAddressLine(i));
//                        strReturnedAddress.append(returnedAddress.getAddressLine(i)).append("\n");
//                        Log.d("con", strReturnedAddress.toString());
//                    }
//                }
                StringBuilder sb = new StringBuilder();
                Log.d("con","result "+ returnedAddress.getAddressLine(0));
                if(returnedAddress.getAddressLine(0).length()>10) {
                    sb.append(returnedAddress.getAddressLine(0));
                }
                else {
                    sb.append(returnedAddress.getFeatureName()).append(",");
                    sb.append(returnedAddress.getLocality()).append(",");
                    sb.append(returnedAddress.getSubAdminArea()).append(",");
                    sb.append(returnedAddress.getAdminArea()).append(",");
                    sb.append(returnedAddress.getPostalCode()).append(",");
                    sb.append(returnedAddress.getCountryName()).append(".");
                }
                strAdd = sb.toString();
                Log.d("con","result2 "+ strAdd);
                Log.d("con", returnedAddress.toString());
                address.setText(strAdd);
                mVehicleLocationList.get(pos).setAddress(strAdd);
            } else {
                Log.d("con", "No Address returned!");
                address.setText(""+mContext.getResources().getString(R.string.loadingaddress));
                new GetAddressTaskNew(address,LATITUDE,LONGITUDE).execute();
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.d("con", "Canont get Address!");
            address.setText(""+mContext.getResources().getString(R.string.loadingaddress));
            new GetAddressTaskNew(address,LATITUDE,LONGITUDE).execute();
        }
    }

}