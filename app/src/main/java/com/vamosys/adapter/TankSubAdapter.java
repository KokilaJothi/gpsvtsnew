package com.vamosys.adapter;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.anychart.scales.Linear;
import com.bluenet.utils.StringUtil;
import com.vamosys.model.FuelSumReportDto;
import com.vamosys.vamos.R;

import java.util.ArrayList;
import java.util.List;

public class TankSubAdapter extends RecyclerView.Adapter<TankSubAdapter.SubViewHolder> {
    List<FuelSumReportDto.Fuelsensor> tankData=new ArrayList<>();
    Context mcontext;
    private Double dist;

    public TankSubAdapter(List<FuelSumReportDto.Fuelsensor> tankData, Context mContext,Double dist) {
        this.tankData=tankData;
        this.mcontext=mcontext;
        this.dist=dist;
    }

    @NonNull
    @Override
    public SubViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.tank_row_child, parent, false);
        return new SubViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SubViewHolder holder1, int position) {
        try {
            FuelSumReportDto.Fuelsensor f = tankData.get(position);
            if (position == tankData.size() - 1) {
                holder1.lay_view.setVisibility(View.GONE);
            }
            holder1.txt_fuelStart.setText(f.getStartFuel());
            holder1.txt_fuelFill.setText(f.getFuelFilling());
            holder1.txt_fuelCons.setText(f.getFuelConsumption());
            holder1.txt_fuelend.setText(f.getEndFuel());
            holder1.txt_theft.setText(f.getFuelTheft());
            holder1.txt_kmpl.setText(f.getKmpl());
            holder1.txt_sensor.setText("" + f.getSensor());
            if (dist == 0) {
                holder1.txt_fuelStart.setTextColor(Color.parseColor("#fa315b"));
                holder1.txt_fuelFill.setTextColor(Color.parseColor("#fa315b"));
                holder1.txt_fuelCons.setTextColor(Color.parseColor("#fa315b"));
                holder1.txt_fuelend.setTextColor(Color.parseColor("#fa315b"));
                holder1.txt_theft.setTextColor(Color.parseColor("#fa315b"));
                holder1.txt_kmpl.setTextColor(Color.parseColor("#fa315b"));
                holder1.txt_sensor.setTextColor(Color.parseColor("#fa315b"));
            } else {
                holder1.txt_fuelStart.setTextColor(Color.parseColor("#000000"));
                holder1.txt_fuelFill.setTextColor(Color.parseColor("#000000"));
                holder1.txt_fuelCons.setTextColor(Color.parseColor("#000000"));
                holder1.txt_fuelend.setTextColor(Color.parseColor("#000000"));
                holder1.txt_theft.setTextColor(Color.parseColor("#000000"));
                holder1.txt_kmpl.setTextColor(Color.parseColor("#000000"));
                holder1.txt_sensor.setTextColor(Color.parseColor("#000000"));
            }
        }catch (Exception e){
            Log.d("EXCEPTION",""+e.getMessage());
        }
    }

    @Override
    public int getItemCount() {
        return tankData.size();
    }

    public class SubViewHolder extends RecyclerView.ViewHolder{
        TextView txt_fuelStart,txt_fuelFill,txt_fuelCons,txt_fuelend,txt_theft,txt_kmpl,txt_sensor;
        LinearLayout lay_view;
        public SubViewHolder(@NonNull View itemView) {
            super(itemView);
            txt_fuelStart=itemView.findViewById(R.id.fuelsumm_start_textView);
            txt_fuelFill=itemView.findViewById(R.id.fuelsumm_fuel_fill_textView);
            txt_fuelCons=itemView.findViewById(R.id.fuelsumm_fuel_consume_textView);
            txt_fuelend=itemView.findViewById(R.id.fuelsumm_end_textView);
            txt_theft=itemView.findViewById(R.id.fuelsumm_fuel_theft_textView);
            txt_kmpl=itemView.findViewById(R.id.fuelsumm_fuel_milage_textView);
            lay_view=itemView.findViewById(R.id.lay_view);
            txt_sensor=itemView.findViewById(R.id.fuelsumm_sensor_textView_);
        }
    }
}
