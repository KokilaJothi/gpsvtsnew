package com.vamosys.adapter;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.vamosys.model.FuelSumReportDto;
import com.vamosys.vamos.R;

import java.util.ArrayList;
import java.util.List;

public class TankSummaryAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{
    Context mContext;
    List<FuelSumReportDto> fuelList=new ArrayList<>();
    private static final int TYPE_HEADER = 0;
    private static final int TYPE_ITEM = 1;
    private static final int TYPE_ITEM2 = 2;

    public TankSummaryAdapter(Context fuelSummaryNew, List<FuelSumReportDto> fuelDataList) {
        Log.d("res","adapter "+fuelDataList.size());
        mContext=fuelSummaryNew;
        fuelList=fuelDataList;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Log.d("fueldata","oncreate");
        if (viewType == TYPE_ITEM) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.tank_row_parent, parent, false);
            return new RowViewHolder(view);
        }
        else if (viewType == TYPE_HEADER) {
            // Here Inflating your header view
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.tank_header, parent, false);
            return new HeaderViewHolder(itemView);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        Log.d("fueldata","onBind");
        try {
            if (holder instanceof HeaderViewHolder) {
                HeaderViewHolder headerViewHolder = (HeaderViewHolder) holder;
                headerViewHolder.sensor_textView_header.setText("Tank");
            }
            else if (holder instanceof RowViewHolder) {
                final RowViewHolder holder1 = (RowViewHolder) holder;
                FuelSumReportDto f = getItem(position);
                holder1.txt_vehicleName.setText(f.getVehicleName());
                holder1.txt_distance.setText(""+f.getDist());
                if(f.getDist()==0){
                    holder1.txt_vehicleName.setTextColor(Color.parseColor("#fa315b"));
                    holder1.txt_distance.setTextColor(Color.parseColor("#fa315b"));
                }
                else {
                    holder1.txt_vehicleName.setTextColor(Color.parseColor("#000000"));
                    holder1.txt_distance.setTextColor(Color.parseColor("#000000"));

                }
                if(f.getTankData()!=null) {
                    TankSubAdapter adapter = new TankSubAdapter(f.getTankData(), mContext, f.getDist());
                    holder1.rv_loadTank.setAdapter(adapter);
                }

            }
        }
        catch (Exception e){

        }

    }

    @Override
    public int getItemViewType(int position) {
        if (isPositionHeader(position)) {
            return TYPE_HEADER;
        }
        else{
            return TYPE_ITEM;
        }

    }
    private boolean isPositionHeader(int position) {
        return position == 0;
    }

    private FuelSumReportDto getItem(int position) {
        return fuelList.get(position-1);
    }
    @Override
    public int getItemCount() {
        return fuelList.size()+1;
    }

    public class RowViewHolder extends RecyclerView.ViewHolder{
        TextView txt_vehicleName,txt_distance;
        RecyclerView rv_loadTank;
        public RowViewHolder(@NonNull View itemView) {
            super(itemView);
            txt_vehicleName=itemView.findViewById(R.id.fuelsum_vehicle_textView);
            txt_distance=itemView.findViewById(R.id.fuelsumm_fuel_distance_textView);
            rv_loadTank=itemView.findViewById(R.id.rv_tank);
        }
    }

    private class HeaderViewHolder extends RecyclerView.ViewHolder {
        TextView sensor_textView_header;
        public HeaderViewHolder(View view) {
            super(view);
            sensor_textView_header=view.findViewById(R.id.fuelsumm_sensor_textView_header);
        }
    }
}
