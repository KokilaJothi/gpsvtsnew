package com.vamosys.adapter;

import android.app.Activity;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.vamosys.model.AcReportDto;
import com.vamosys.vamos.Const;
import com.vamosys.vamos.R;

import java.util.List;

public class AcReportNewAdapter extends RecyclerView.Adapter<AcReportNewAdapter.ViewHolder> {
    private List<AcReportDto> mArrayList;
    //    private ArrayList<AndroidVersion> mFilteredList;
    private Activity myActivity;

    public AcReportNewAdapter(List<AcReportDto> arrayList, Activity act) {
        mArrayList = arrayList;
        //  mFilteredList = arrayList;
        myActivity = act;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.ac_report_new_adapter, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {


        if (mArrayList.get(i).getEndAddress() != null && mArrayList.get(i).getEndTime() > 0) {

            if (mArrayList.get(i).getStartingAddress() != null && !mArrayList.get(i).getStartingAddress().equalsIgnoreCase("null")) {
                viewHolder.txt_start_adds.setVisibility(View.VISIBLE);
                viewHolder.txt_start_adds.setText(mArrayList.get(i).getStartingAddress());
            }

            if (mArrayList.get(i).getEndAddress() != null && !mArrayList.get(i).getEndAddress().equalsIgnoreCase("null")) {
                viewHolder.txt_end_adds.setVisibility(View.VISIBLE);
                viewHolder.txt_end_adds.setText(mArrayList.get(i).getEndAddress());
            }

            long timevalue = 0;
            if (mArrayList.get(i).getEndTime() > mArrayList.get(i).getStartTime()) {
                timevalue = mArrayList.get(i).getEndTime() - mArrayList.get(i).getStartTime();
            }
//        System.out.println("Hi st add " + mArrayList.get(i).getStartingAddress() + " end adds " + mArrayList.get(i).getEndAddress() + " " + timevalue);
//            System.out.println("Hi time  ::" + timevalue);
            viewHolder.txt_duration.setText(Const.getreportTimeValue(timevalue));

            viewHolder.txt_start_date_time.setText(Const.getAcReportTime(mArrayList.get(i).getStartTime()));

            viewHolder.txt_end_date_time.setText(Const.getAcReportTime(mArrayList.get(i).getEndTime()));
        }

    }

    @Override
    public int getItemCount() {
        return mArrayList.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView txt_duration, txt_start_adds, txt_end_adds, txt_start_date_time, txt_end_date_time;


        public ViewHolder(View view) {
            super(view);


            txt_start_adds = (TextView) view.findViewById(R.id.ac_report_adap_start_adds);
            txt_end_adds = (TextView) view.findViewById(R.id.ac_report_adap_end_adds);

            txt_start_date_time = (TextView) view.findViewById(R.id.txt_start_date_time);
            txt_end_date_time = (TextView) view.findViewById(R.id.txt_end_date_time);

            txt_duration = (TextView) view.findViewById(R.id.ac_report_adap_duration);


        }
    }

}