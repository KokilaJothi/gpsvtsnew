package com.vamosys.utils;


import com.vamosys.model.FmsDto;

import java.util.ArrayList;

public class SingleTonClass {

    private static SingleTonClass mInstance;
    private ArrayList<FmsDto> mFmsList = null;
    private ArrayList<FmsDto> mFmsReminderList = null;
    private ArrayList<String> mRemainingList = null;
    private ArrayList<String> mServiceList = null;

    private ArrayList<FmsDto> mServicedata = null;


    private SingleTonClass() {
        mFmsList = new ArrayList<FmsDto>();
        mFmsReminderList = new ArrayList<FmsDto>();
        mRemainingList = new ArrayList<String>();
        mServiceList = new ArrayList<String>();
        mServicedata = new ArrayList<FmsDto>();

    }

    public static SingleTonClass getInstance() {
        if (mInstance == null)
            mInstance = new SingleTonClass();

        return mInstance;
    }
    public ArrayList<String> getmServiceList() {
        return mServiceList;
    }

    public void setmServiceList(ArrayList<String> mServiceList) {
        this.mServiceList = mServiceList;
    }
    public void setFmsDataEmptyList(ArrayList<FmsDto> list) {
        this.mFmsList = list;
    }

    public ArrayList<FmsDto> getFmsData() {
        return this.mFmsList;
    }

    public void addToFmsData(FmsDto value) {
        mFmsList.add(value);
    }

    public void addToFmsDataSelectedPos(int pos, FmsDto value) {
        mFmsList.set(pos, value);
    }


    public void setFmsReminderEmptyList(ArrayList<FmsDto> list) {
        this.mFmsReminderList = list;
    }

    public ArrayList<FmsDto> getFmsReminderList() {
        return this.mFmsReminderList;
    }

    public void addToFmsReminderList(FmsDto value) {
        mFmsReminderList.add(value);
    }

    public void addToFmsReminderListSelectedPos(int pos, FmsDto value) {
        mFmsReminderList.set(pos, value);
    }
    public ArrayList<FmsDto> getmServicedata() {
        return mServicedata;
    }

    public void setmServicedata(ArrayList<FmsDto> mServicedata) {
        this.mServicedata = mServicedata;
    }



    public void setRemainingEmptyList(ArrayList<String> list) {
        this.mRemainingList = list;
    }

    public ArrayList<String> getRemainingList() {
        return this.mRemainingList;
    }

    public void addToRemainingList(String value) {
        mRemainingList.add(value);
    }
    public void addToServicedata(FmsDto value) {
        mServicedata.add(value);
    }

    public void addToServiceList(String value) {
        mServiceList.add(value);
    }


    public void addToRemainingListSelectedPos(int pos, String value) {
        mRemainingList.set(pos, value);
    }


}
