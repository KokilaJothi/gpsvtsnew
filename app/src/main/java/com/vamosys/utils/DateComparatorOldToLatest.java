package com.vamosys.utils;

import com.vamosys.model.TripSummaryMobile;
import com.vamosys.vamos.Const;

import java.util.Comparator;

public class DateComparatorOldToLatest implements Comparator {


    @Override
    public int compare(Object o1, Object o2) {
        TripSummaryMobile dd1=(TripSummaryMobile)o1;
        TripSummaryMobile dd2=(TripSummaryMobile)o2;

        return Const.getTripTimefromserver(dd2.getfT()).compareTo(Const.getTripTimefromserver(dd1.getfT()));
    }
}
