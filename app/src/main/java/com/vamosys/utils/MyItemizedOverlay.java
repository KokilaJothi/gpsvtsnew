package com.vamosys.utils;


import android.graphics.Point;
import android.graphics.drawable.Drawable;

import org.osmdroid.api.IMapView;
import org.osmdroid.views.overlay.ItemizedOverlay;
import org.osmdroid.views.overlay.OverlayItem;

import java.util.ArrayList;

public class MyItemizedOverlay extends ItemizedOverlay {

    private ArrayList <OverlayItem> mOverlays = new ArrayList<OverlayItem>();

    public MyItemizedOverlay(Drawable defaultMarker) {
        super(defaultMarker);
    }

    public void addOverlay(OverlayItem overlay) {
        mOverlays.add(overlay);
        populate();
    }

    @Override
    protected OverlayItem createItem(int i) {
        return mOverlays.get(i);
    }

    @Override
    public int size() {
        return mOverlays.size();
    }

    public void remove(int position) {
        mOverlays.remove(position);
        populate();
    }

    public void removeAll() {
        mOverlays.clear();
        populate();
    }

    @Override
    public boolean onSnapToItem(int x, int y, Point snapPoint, IMapView mapView) {
        return false;
    }
}