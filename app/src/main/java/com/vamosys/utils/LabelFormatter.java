package com.vamosys.utils;


import android.util.Log;

import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;

import java.util.List;

public class LabelFormatter implements IAxisValueFormatter {

    private final List<String> mLabels;

    public LabelFormatter(List<String> labels) {
        Log.i("Line chart 11", " label size " + labels.size());
        mLabels = labels;
    }

//    @Override
//    public String getFormattedValue(float value, Entry entry, int dataSetIndex, ViewPortHandler viewPortHandler) {
//        Log.i("Line chart ", String.valueOf(value) + " index " + dataSetIndex + " entry " + String.valueOf(entry.getData()));
//        return "";
//    }

    @Override
    public String getFormattedValue(float value, AxisBase axis) {

        Log.i("Line chart ", String.valueOf(value) + " " + axis + " label size " + mLabels.size());

        if (!((int) value < 0) && (mLabels.size() < (int) value)) {
            return mLabels.get((int) value);
        } else if ((mLabels.size() > (int) value)) {
            return mLabels.get(mLabels.size() - 1);
        }
        return "";
    }


}
