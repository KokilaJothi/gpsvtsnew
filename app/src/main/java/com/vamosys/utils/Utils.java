package com.vamosys.utils;

import java.text.DecimalFormat;

public class Utils {

    public static String get2DecimalValue(String value) {
        Float data = Float.valueOf(Float.parseFloat(value));
        DecimalFormat df = new DecimalFormat("0.00");
        df.setMaximumFractionDigits(2);
        return df.format(data);
    }
}
