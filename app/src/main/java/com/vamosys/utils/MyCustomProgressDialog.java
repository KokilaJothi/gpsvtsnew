package com.vamosys.utils;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;

import com.vamosys.vamos.R;

public class MyCustomProgressDialog extends ProgressDialog {
    static Context context = null;
    AnimationDrawable animation;
    ProgressDialog dialog = null;
    public MyCustomProgressDialog(Context context) {
        super(context);
        dialog = new ProgressDialog(context);
        dialog.setMessage(""+context.getResources().getString(R.string.loading));
        dialog.setIndeterminate(true);
        dialog.setCancelable(true);
    }
    public static ProgressDialog ctor(Context ctxt) {
        MyCustomProgressDialog dialog = new MyCustomProgressDialog(ctxt);
        dialog.setIndeterminate(true);
        dialog.setCancelable(false);
        context = ctxt;
        return dialog;
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.view_custom_progress_dialog);
    }
    @Override
    public void show() {
        super.show();
        dialog.show();
    }
    @Override
    public void dismiss() {
        super.dismiss();
        dialog.dismiss();
    }
}
