package com.vamosys.utils;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.vamosys.model.DataBaseHandler;
import com.vamosys.model.LatLngDto;
import com.vamosys.model.PushNotificationDto;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

public class DaoHandler {
    private DataBaseHandler dbHandler;
    private Context mContext;
    private SQLiteDatabase db;
    String TAG = "App";

    public DaoHandler(Context context, boolean isWritable) {
        mContext = context;
        dbHandler = new DataBaseHandler(mContext);
        if (isWritable) {
            db = dbHandler.getWritableDatabase();
        } else {
            db = dbHandler.getReadableDatabase();
        }
    }


    public void insertPushMsg(String msg) {

        if (!db.isOpen()) {
            db = mContext.openOrCreateDatabase(DataBaseHandler.getDatabasePath(mContext, DataBaseHandler.DATABASE_NAME),
                    SQLiteDatabase.OPEN_READWRITE, null);
        }
        if (db == null) {
            db = dbHandler.getWritableDatabase();
        }


        try {

            Date d = new Date();
            String time = d.getDate() + "-" + d.getMonth() + 1 + "-" + d.getYear()
                    + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();

            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss:SSS");


            String DateToStr = sdf.format(d);


            ContentValues cv = new ContentValues();
            cv.put("msg_data", msg);
            cv.put("time_stamp", DateToStr);
//        DataBaseHandler db = new DataBaseHandler(getBaseContext());

//            int a = (int) db.open().getDatabaseObj()
//                    .insert(DataBaseHandler.TABLE_NOTIFICATIONS, null, cv);

            db.insert(DataBaseHandler.TABLE_NOTIFICATIONS, null, cv);


        } catch (SQLException e) {

        } finally {

            db.close();
        }

    }

//    public void checkPushList() {
//
//        if (!db.isOpen()) {
//            db = mContext.openOrCreateDatabase(DataBaseHandler.getDatabasePath(mContext, DataBaseHandler.DATABASE_NAME),
//                    SQLiteDatabase.OPEN_READWRITE, null);
//        }
//        if (db == null) {
//            db = dbHandler.getWritableDatabase();
//        }
//        try {
////        DataBaseHandler db = new DataBaseHandler(getBaseContext());
//            String qry = "DELETE FROM " + DataBaseHandler.TABLE_NOTIFICATIONS + " WHERE id NOT IN (SELECT id FROM " + DataBaseHandler.TABLE_NOTIFICATIONS + " ORDER BY id DESC LIMIT 1000 ) ";
//            try {
//                Cursor c = db.rawQuery(qry, null);
//                c.getCount();
//                c.close();
//            } catch (SQLException e) {
//
//            } finally {
//                db.close();
//            }
//        } catch (SQLException e) {
//
//        } finally {
//
//            db.close();
//        }
//    }

    public void insertBitmap(Bitmap bm) {

        if (!db.isOpen()) {
            db = mContext.openOrCreateDatabase(DataBaseHandler.getDatabasePath(mContext, DataBaseHandler.DATABASE_NAME),
                    SQLiteDatabase.OPEN_READWRITE, null);
        }
        if (db == null) {
            db = dbHandler.getWritableDatabase();
        }


        try {

            ByteArrayOutputStream out = new ByteArrayOutputStream();
            bm.compress(Bitmap.CompressFormat.PNG, 100, out);
            byte[] buffer = out.toByteArray();


            ContentValues values = new ContentValues();
            values.put("app_logo_image", buffer);
//        DataBaseHandler db = new DataBaseHandler(getBaseContext());

//            int a = (int) db.open().getDatabaseObj()
//                    .insert(DataBaseHandler.TABLE_NOTIFICATIONS, null, cv);

            db.insert(DataBaseHandler.TABLE_LOGO, null, values);


        } catch (SQLException e) {

        } finally {

            db.close();
        }

    }


    public void queryUserDB() {

        if (!db.isOpen()) {
            db = mContext.openOrCreateDatabase(DataBaseHandler.getDatabasePath(mContext, DataBaseHandler.DATABASE_NAME),
                    SQLiteDatabase.OPEN_READWRITE, null);
        }
        if (db == null) {
            db = dbHandler.getReadableDatabase();
        }
//        DataBaseHandler db = new DataBaseHandler(this);
        Cursor c = null;
        // String qry = "SELECT DISTINCT patch_id,patch_name,mtp_id FROM "
        // + DbHandler.TABLE_TM_DCP;
        try {

            String qry = "SELECT * FROM " + DataBaseHandler.TABLE_USER;
            c = db.rawQuery(qry, null);
            // int reporting_date_index = c
            // .getColumnIndex("actual_reporting_date");
            int user_id_index = c.getColumnIndex("user_id");
            int group_list_index = c.getColumnIndex("group_list");
            int refresh_rate_index = c.getColumnIndex("refresh_rate");
            int history_interval_index = c.getColumnIndex("history_interval");
            int support_adds_index = c.getColumnIndex("support_adds");


            if (c.getCount() > 0) {

                while (c.moveToNext()) {
                    Constant.mDbUserId = c.getString(user_id_index);
                    if (c.getString(group_list_index) != null) {
                        Gson g = new Gson();
                        InputStream is = new ByteArrayInputStream(c.getString(
                                group_list_index).getBytes());
                        Reader reader = new InputStreamReader(is);
                        Type fooType = new TypeToken<List<String>>() {
                        }.getType();

                        Constant.mUserGroupList = g.fromJson(reader, fooType);
                    }


//                    Settings.mSelectedUserId = c.getString(user_id_index);

                    if (c.getString(refresh_rate_index) != null) {
                        Constant.db_refresh_rate = Long.parseLong(c.getString(refresh_rate_index));
                    } else {
                        Constant.db_refresh_rate = 10;
                    }
                    if (c.getString(history_interval_index) != null) {
                        Constant.db_history_interval = Long.parseLong(c.getString(history_interval_index));
                    } else {
                        Constant.db_history_interval = 1;
                    }


                    if (c.getString(support_adds_index) != null) {
                        Constant.mSupportDbList = Arrays.asList(c.getString(support_adds_index).split(","));
                        Log.d("mSupportDbList",""+  Constant.mSupportDbList);
                    }


                }
            }

        } catch (SQLException e) {

        } finally {
            c.close();
            db.close();
        }
//        setDropDownData();
    }

    public String queryUserDB2(String qry) {

        if (!db.isOpen()) {
            db = mContext.openOrCreateDatabase(DataBaseHandler.getDatabasePath(mContext, DataBaseHandler.DATABASE_NAME),
                    SQLiteDatabase.OPEN_READWRITE, null);
        }
        if (db == null) {
            db = dbHandler.getReadableDatabase();
        }
//        DataBaseHandler db = new DataBaseHandler(this);
        Cursor c = null;
        // String qry = "SELECT DISTINCT patch_id,patch_name,mtp_id FROM "
        // + DbHandler.TABLE_TM_DCP;
        String mRefreshRate = "10";
        try {

//            String qry = "SELECT * FROM " + DataBaseHandler.TABLE_USER;
            c = db.rawQuery(qry, null);
            int refresh_rate_index = c.getColumnIndex("refresh_rate");

            if (c.getCount() > 0) {

                while (c.moveToNext()) {


                    if (c.getString(refresh_rate_index) != null) {
                        mRefreshRate = (c.getString(refresh_rate_index));
                    } else {
                        mRefreshRate = "10";
                    }


                }
            }

        } catch (SQLException e) {

        } finally {
            c.close();
            db.close();
        }
//        setDropDownData();

        return mRefreshRate;
    }


    public List<PushNotificationDto> getPushList() {

        if (!db.isOpen()) {
            db = mContext.openOrCreateDatabase(DataBaseHandler.getDatabasePath(mContext, DataBaseHandler.DATABASE_NAME),
                    SQLiteDatabase.OPEN_READWRITE, null);
        }
        if (db == null) {
            db = dbHandler.getWritableDatabase();
        }


        List<PushNotificationDto> mPushDataListAdapter = new ArrayList<PushNotificationDto>();
        Cursor c = null;
        try {

            String qry1 = "DELETE FROM " + DataBaseHandler.TABLE_NOTIFICATIONS
                    + " WHERE id NOT IN (SELECT id FROM "
                    + DataBaseHandler.TABLE_NOTIFICATIONS
                    + " ORDER BY id DESC LIMIT 1000 ) ";

            Cursor c2 = db.rawQuery(qry1, null);
            c2.getCount();
            c2.close();

            String qry = "SELECT * FROM " + DataBaseHandler.TABLE_NOTIFICATIONS
                    + " ORDER BY id DESC LIMIT 1000";

            c = db.rawQuery(qry, null);
//            c = db.open().getDatabaseObj().rawQuery(qry, null);
            int push_msg_index = c.getColumnIndex("msg_data");
            int id_index = c.getColumnIndex("id");
            int time_stamp_index = c.getColumnIndex("time_stamp");

            //System.out.println("Msg count is ::::::" + c.getCount());

            while (c.moveToNext()) {
                PushNotificationDto pu = new PushNotificationDto();
                pu.setAlreadySelected(false);
                pu.setId(c.getString(id_index).trim());
                pu.setMsgContent(c.getString(push_msg_index));
                pu.setTimeStamp(c.getString(time_stamp_index));
                mPushDataListAdapter.add(pu);
                // list.add(push_msg);
            }

//            Constant.mPushDataList = mPushDataListAdapter;
//            Constant.mPushDataListAdapter = Constant.mPushDataList;
        } catch (SQLException e) {

        } finally {
            c.close();
            db.close();
        }
        System.out.println("Msg count is 0000::::::" + mPushDataListAdapter.size());
        return mPushDataListAdapter;
    }

    /*
        * To delete inbox record except recent 5 from db
        */
    public void deleteSelectedNotificationFromDB(String qry) {
        if (!db.isOpen()) {
            db = mContext.openOrCreateDatabase(DataBaseHandler.getDatabasePath(mContext, DataBaseHandler.DATABASE_NAME),
                    SQLiteDatabase.OPEN_READWRITE, null);
        }
        if (db == null) {
            db = dbHandler.getWritableDatabase();
        }

//        DataBaseHandler db = new DataBaseHandler(mCommonContext);
//        Cursor c = null;
        try {

            // db.open().getDatabaseObj()
            // .delete(DbHandler.TABLE_REGIONAL_MANAGER, null, null);
            Cursor c = db.rawQuery(qry, null);
            c.getCount();
            c.close();
//            c = db.open().getDatabaseObj().rawQuery(qry, null);
//            System.out.println("No of deleted rows is ::::" + c.getCount());
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
//            if (c != null) {
//                c.close();
//            }
            db.close();

        }

    }


    public void insertUserData(ContentValues cv) {
        if (!db.isOpen()) {
            db = mContext.openOrCreateDatabase(DataBaseHandler.getDatabasePath(mContext, DataBaseHandler.DATABASE_NAME),
                    SQLiteDatabase.OPEN_READWRITE, null);
        }
        if (db == null) {
            db = dbHandler.getWritableDatabase();
        }
        try {
            db.insert(DataBaseHandler.TABLE_USER, null, cv);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
//            if (c != null) {
//                c.close();
//            }
            db.close();

        }

//        startActivity(new Intent(CameraViewActivity.this, KMSSummary.class));
//        finish();
    }


    /*
    * To update db
    */
    public void updateDB(String qry) {
        if (!db.isOpen()) {
            db = mContext.openOrCreateDatabase(DataBaseHandler.getDatabasePath(mContext, DataBaseHandler.DATABASE_NAME),
                    SQLiteDatabase.OPEN_READWRITE, null);
        }
        if (db == null) {
            db = dbHandler.getWritableDatabase();
        }
        //   System.out.println("The update qry is :::::" + qry);
//        DataBaseHandler db = new DataBaseHandler(mCommonContext);
//        Cursor c = null;
        try {

            // db.open().getDatabaseObj()
            // .delete(DbHandler.TABLE_REGIONAL_MANAGER, null, null);

            Cursor c = db.rawQuery(qry, null);
            c.getCount();
            c.close();

//            c = db.open().getDatabaseObj().rawQuery(qry, null);
//            Log.e("")
//            System.out.println("No of updated rows is ::::" + c.getCount());
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
//            if (c != null) {
//                c.close();
//            }
            db.close();

        }
    }


    /*
  * To delete user table
  */
    public void deleteUserDB() {

        if (!db.isOpen()) {
            db = mContext.openOrCreateDatabase(DataBaseHandler.getDatabasePath(mContext, DataBaseHandler.DATABASE_NAME),
                    SQLiteDatabase.OPEN_READWRITE, null);
        }
        if (db == null) {
            db = dbHandler.getWritableDatabase();
        }

//        DataBaseHandler db = new DataBaseHandler(mCommonContext);
        //  Cursor c = null;
        try {

            String qry = "DELETE FROM " + DataBaseHandler.TABLE_USER;

//            int c = db.open().getDatabaseObj()
//                    .delete(DataBaseHandler.TABLE_USER, null, null);
            Cursor c = db.rawQuery(qry, null);
            c.getCount();
            c.close();

            // c = db.open().getDatabaseObj().rawQuery(qry, null);
//            System.out.println("No of deleted rows is ::::" + c);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
//            if (c != null) {
//                c.close();
//            }
            db.close();

        }
    }


    public Bitmap getBitmap() {


        if (!db.isOpen()) {
            db = mContext.openOrCreateDatabase(DataBaseHandler.getDatabasePath(mContext, DataBaseHandler.DATABASE_NAME),
                    SQLiteDatabase.OPEN_READWRITE, null);
        }
        if (db == null) {
            db = dbHandler.getReadableDatabase();
        }

//        DataBaseHandler db = new DataBaseHandler(mCommonContext);

        Cursor cursor = null;
        Bitmap bitmap = null;
        try {

            String qry = "SELECT * FROM " + DataBaseHandler.TABLE_LOGO;

//            int c = db.open().getDatabaseObj()
//                    .delete(DataBaseHandler.TABLE_USER, null, null);
            cursor = db.rawQuery(qry, null);
            if (cursor.getCount() > 0) {
                while (cursor.moveToNext()) {
                    // Convert blob data to byte array
                    byte[] blob = cursor.getBlob(cursor.getColumnIndex("app_logo_image"));
                    // Convert the byte array to Bitmap
                    bitmap = BitmapFactory.decodeByteArray(blob, 0, blob.length);

                }

            }
//            cursor.close();
            // c = db.open().getDatabaseObj().rawQuery(qry, null);
//            System.out.println("No of deleted rows is ::::" + c);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
            }
            db.close();

        }


        // Open the database for reading
//        DataBaseHandler db = new DataBaseHandler(mCommonContext);
//        // Start the transaction.
//        // db.beginTransaction();
//
//        try {
//            String selectQuery = "SELECT * FROM " + DataBaseHandler.TABLE_LOGO;
//            Cursor cursor = db.open().getDatabaseObj().rawQuery(selectQuery, null);
//            if (cursor.getCount() > 0) {
//                while (cursor.moveToNext()) {
//                    // Convert blob data to byte array
//                    byte[] blob = cursor.getBlob(cursor.getColumnIndex("app_logo_image"));
//                    // Convert the byte array to Bitmap
//                    bitmap = BitmapFactory.decodeByteArray(blob, 0, blob.length);
//
//                }
//
//            }
//
//
//        } catch (SQLiteException e) {
//            e.printStackTrace();
//
//        } finally {
//
//            db.close();
//
//            // Close database
//        }
//        System.out.println("The returning bitmap is ::::" + bitmap);

        return bitmap;

    }


    public List<LatLngDto> getOfflineStoredLatLng() {
        List<LatLngDto> mLatLngList = new ArrayList<LatLngDto>();
        if (!db.isOpen()) {
            db = mContext.openOrCreateDatabase(DataBaseHandler.getDatabasePath(mContext, DataBaseHandler.DATABASE_NAME),
                    SQLiteDatabase.OPEN_READWRITE, null);
        }
        if (db == null) {
            db = dbHandler.getReadableDatabase();
        }


        try {

            String selectQry = "SELECT * FROM " + DataBaseHandler.TABLE_LAT_LNG;

            Cursor c1 = db.rawQuery(selectQry, null);

            System.out.println("Hi stored data list size is " + c1.getCount());

            int latIndex = c1.getColumnIndex("latitude");
            int idIndex = c1.getColumnIndex("id");
            int lngIndex = c1.getColumnIndex("longitude");
            int dateTimeIndex = c1.getColumnIndex("date_time");
            int idValue = 0;
            if (c1.getCount() > 0) {

                if (c1.moveToFirst()) {

//                while (c1.moveToNext()) {
                    LatLngDto ll = new LatLngDto();
                    idValue = c1.getInt(idIndex);
                    ll.setId(c1.getString(idIndex));
                    ll.setLat(Double.parseDouble(c1.getString(latIndex)));
                    ll.setLng(Double.parseDouble(c1.getString(lngIndex)));
                    ll.setmDateTime(Long.parseLong(c1.getString(dateTimeIndex)));
                    mLatLngList.add(ll);
                }
            }
            if (c1 != null) {
                c1.close();
            }

            //  String qry = "DELETE FROM " + DataBaseHandler.TABLE_LAT_LNG + " WHERE id=" + idValue;

//            int c = db.open().getDatabaseObj()
//                    .delete(DataBaseHandler.TABLE_USER, null, null);
            //Cursor c = db.rawQuery(qry, null);
            // c.getCount();
            //  c.close();

            // c = db.open().getDatabaseObj().rawQuery(qry, null);
//            System.out.println("No of deleted rows is ::::" + c);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
//            if (c != null) {
//                c.close();
//            }
            db.close();

        }


        // delete the record from db
        return mLatLngList;
    }

    public void deleteLocationData(String idValue) {

        if (!db.isOpen()) {
            db = mContext.openOrCreateDatabase(DataBaseHandler.getDatabasePath(mContext, DataBaseHandler.DATABASE_NAME),
                    SQLiteDatabase.OPEN_READWRITE, null);
        }
        if (db == null) {
            db = dbHandler.getReadableDatabase();
        }


        try {
            String qry = "DELETE FROM " + DataBaseHandler.TABLE_LAT_LNG + " WHERE id=" + idValue;
            System.out.println("Hi latlng delete qry " + qry);
//            int c = db.open().getDatabaseObj()
//                    .delete(DataBaseHandler.TABLE_USER, null, null);
            Cursor c = db.rawQuery(qry, null);
            c.getCount();
            c.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
//            if (c != null) {
//                c.close();
//            }
            db.close();

        }
    }


    public void insertLiveTrackingData(String lat, String lng, String dateTime) {
        if (!db.isOpen()) {
            db = mContext.openOrCreateDatabase(DataBaseHandler.getDatabasePath(mContext, DataBaseHandler.DATABASE_NAME),
                    SQLiteDatabase.OPEN_READWRITE, null);
        }
        if (db == null) {
            db = dbHandler.getWritableDatabase();
        }
        try {
            ContentValues cv = new ContentValues();
            cv.put("latitude", lat);
            cv.put("longitude", lng);
            cv.put("date_time", dateTime);
            System.out.println("Hi the inserted values are " + cv);
            db.insert(DataBaseHandler.TABLE_LAT_LNG, null, cv);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
//            if (c != null) {
//                c.close();
//            }
            db.close();

        }
    }

}
