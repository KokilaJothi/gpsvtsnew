package com.vamosys.app;

import android.content.Context;
import androidx.multidex.MultiDex;
import androidx.multidex.MultiDexApplication;

import com.vamosys.utils.NotificationsPreferenceManager;



public class MyApplication extends MultiDexApplication {

    public static final String TAG = MyApplication.class
            .getSimpleName();



    private static MyApplication mInstance;

    private NotificationsPreferenceManager pref;

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
    }

    @Override
    protected void attachBaseContext(Context context) {
        super.attachBaseContext(context);
        MultiDex.install(this);
    }

    public static synchronized MyApplication getInstance() {
        return mInstance;
    }



    public NotificationsPreferenceManager getPrefManager() {
        if (pref == null) {
            pref = new NotificationsPreferenceManager(this);
        }

        return pref;
    }





}
