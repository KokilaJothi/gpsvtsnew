package com.bluenet.camManager.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.bluenet.camManager.CameraBean;
import com.bluenet.utils.StringUtil;

import java.util.ArrayList;
import java.util.List;

public class CameraDao extends BaseDao<CameraBean> {

    private static CameraDao instance;
    private CameraDao(Context mContext)
    {
        this.context = mContext;
        helper = DatabaseHelper.getInstance(context);
    }

    public static CameraDao getInstance(Context mContext)
    {
        if(null ==instance)
            instance = new CameraDao(mContext);
        return instance;
    }

    @Override
    public int insert(CameraBean deviceBean) {
        ContentValues values = new ContentValues();
        values.put(CameraColumn.DID, deviceBean.getDevID());
        values.put(CameraColumn.NAME, deviceBean.getDevName());
        values.put(CameraColumn.MAC, deviceBean.getDevMac());
        values.put(CameraColumn.PASSWD, deviceBean.getPassword());
        values.put(CameraColumn.USERNAME, deviceBean.getUsername());
        values.put(CameraColumn.IP, deviceBean.getDevIP());
        values.put(CameraColumn.TYPE, deviceBean.getDeviceType());
        values.put(CameraColumn.PORT, deviceBean.getPort());
        int id = (int) helper.getWritableDatabase().insert(DatabaseHelper.TABLE.DEVICE_TABLE, null, values);
        deviceBean.setId(id);
        return id;
    }

    @Override
    public int update(CameraBean deviceBean) {
        ContentValues values = new ContentValues();
        values.put(CameraColumn.DID, deviceBean.getDevID());
        if(!StringUtil.isEmpty(deviceBean.getDevName())){
            values.put(CameraColumn.NAME, deviceBean.getDevName());
        }
        values.put(CameraColumn.MAC, deviceBean.getDevMac());
        values.put(CameraColumn.PASSWD, deviceBean.getPassword());
        values.put(CameraColumn.USERNAME, deviceBean.getUsername());
        values.put(CameraColumn.IP, deviceBean.getDevIP());
        values.put(CameraColumn.TYPE, deviceBean.getDeviceType());
        values.put(CameraColumn.PORT, deviceBean.getPort());
        int id = (int)helper.getWritableDatabase().update(DatabaseHelper.TABLE.DEVICE_TABLE, values, "_id = ?", new String[]{String.valueOf(deviceBean.getId())});
        return id;
    }

    @Override
    public boolean delete(int id) {
        long ret =  helper.getWritableDatabase().delete(DatabaseHelper.TABLE.DEVICE_TABLE, "_id = ?", new String[]{String.valueOf(id)});
        if(ret>0)
        {
            return true;
        }
        return false;
    }

    @Override
    public List<CameraBean> queryAll() {
        List<CameraBean> list = new ArrayList<CameraBean>();
        String sql = "select * from camera";
        Cursor cursor = helper.getReadableDatabase().rawQuery(sql, null);
        while(cursor.moveToNext())
        {
            CameraBean model = new CameraBean();
            model.setId(getInt(cursor, CameraColumn.ID));
            model.setDeviceType(getInt(cursor, CameraColumn.TYPE));
            model.setDevID(getString(cursor, CameraColumn.DID));
            model.setDevName(getString(cursor, CameraColumn.NAME));
            model.setDevIP(getString(cursor, CameraColumn.IP));
            model.setDevMac(getString(cursor, CameraColumn.MAC));
            model.setPort(getInt(cursor, CameraColumn.PORT));
            model.setUsername(getString(cursor, CameraColumn.USERNAME));
            model.setPassword(getString(cursor, CameraColumn.PASSWD));
            list.add(model);
        }
        cursor.close();
        return list;
    }

    @Override
    public boolean isExist(String did)
    {
        boolean isExist = false;
        String sql = "select * from camera where did = ?";
        Cursor cursor = helper.getReadableDatabase().rawQuery(sql, new String[]{did});
        if(cursor.getCount()>0)
        {
            isExist = true;
        }
        cursor.close();
        return isExist;
    }

    @Override
    public CameraBean queryByID(int id) {
        return null;
    }

    @Override
    public CameraBean queryByDID(String did) {
        return null;
    }
}
