package com.bluenet.camManager.db;

import android.content.Context;
import android.database.Cursor;

import java.util.List;

public abstract class BaseDao<T>
{
    protected DatabaseHelper helper;
    protected Context context;

    public String getString(Cursor cursor, String param) {
        return cursor.getString(cursor.getColumnIndex(param));
    }

    public int getInt(Cursor cursor, String param) {
        return cursor.getInt(cursor.getColumnIndex(param));
    }

    public long getLong(Cursor cursor, String param) {
        return cursor.getLong(cursor.getColumnIndex(param));
    }

    public abstract int insert(T t);
    public abstract int update(T t);
    public abstract boolean delete(int id);
    public abstract List<T> queryAll();
    public abstract T queryByID(int id);
    public abstract T queryByDID(String did);
    public abstract boolean isExist(String did);

}
