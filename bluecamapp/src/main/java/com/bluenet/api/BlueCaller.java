package com.bluenet.api;

import com.bluecam.libs.Device;
import com.bluecam.libs.Search;

/**
 * Created by Administrator on 2017/7/17.
 */

public class BlueCaller {

    public  static int Init(){
        return Device.getInstance().Init();
    }

    public  static int Cleanup(){
       Device.getInstance().Cleanup();
       return 0;
    }

    public  static int NetDetect(){
        return Device.getInstance().NetDetect();
    }

    public static long CreateDevice(String uName, String uPasswd, String ip, int port, String camID){
        return Device.getInstance().CreateDevice(camID,uName,uPasswd);
    }

    public static int setReconnect(long camHandler,int bOpen,int reconnectCount){//bOpen 1开启
        return Device.getInstance().SetAutoReconnect((int)camHandler,bOpen,reconnectCount);
    }

    public  static int DeleteDevice(long camHandler){
        return Device.getInstance().DeleteDevice((int)camHandler);
    }

    public  static int Login(long camHandler,int timeout ){
        return Device.getInstance().Login((int)camHandler,timeout);
    }
    public  static int Logout(long camHandler){
        return Device.getInstance().Logout((int)camHandler);
    }
    public  static int SetSDKCallBack(long camHandler,Object callBackObj){
       return Device.getInstance().SetSDKCallBack((int)camHandler,callBackObj);
    }

    public  static int ConfigData(long camHandler, int ParamNewKey)
    {
        return Device.getInstance().ConfigData((int)camHandler,ParamNewKey,null);
    }

    public  static int ConfigData(long camHandler, int ParamNewKey, String params){
        return Device.getInstance().ConfigData((int)camHandler,ParamNewKey,params);
    }

    public  static int OpenStream(long camHandler, int streamId,int resolution){
        return Device.getInstance().OpenStream((int)camHandler,resolution);
    }
    public  static int CloseStream(long camHandler){
        return Device.getInstance().CloseStream((int)camHandler);
    }

    public  static int OpenSound(long camHandler, int AudioId){
        return Device.getInstance().OpenSound((int)camHandler,AudioId);
    }
    public  static int CloseSound(long camHandler){
        return Device.getInstance().CloseSound((int)camHandler);
    }

    public  static int OpenTalk(long camHandler){
        return Device.getInstance().OpenTalk((int)camHandler);
    }
    public  static int CloseTalk(long camHandler){
        return Device.getInstance().CloseTalk((int)camHandler);
    }
    public  static int SendTalkData(long camHandler, byte[] data, int size)
    {
        return Device.getInstance().SendTalkData((int)camHandler,data,size);
    }

    public static int TransmitFile(int camHandler, String transInfo, String filePath){
        return Device.getInstance().TransmitFile((int)camHandler,transInfo,filePath);
    }

    public  static int SetSearchDeviceCallBack(Object objSearch){
        return Search.getInstance().SetSearchDeviceCallBack(objSearch);
    }
    public  static int InitSearchEnv(){
        return  Search.getInstance().InitSearchEnv();
    }
    public  static void CleanupSearchEnv(){
        Search.getInstance().CleanupSearchEnv();
    }
    public  static int SearchDevice(){
        return Search.getInstance().SearchDevice();
    }

    public  static int OpenPlayBack(long camHandler, String filename, int index){
        return Device.getInstance().OpenPlayBack((int)camHandler,filename,index);
    }
    public  static int ClosePlayBack(long camHandler, String filename){
        return Device.getInstance().ClosePlayBack((int)camHandler);
    }
}
