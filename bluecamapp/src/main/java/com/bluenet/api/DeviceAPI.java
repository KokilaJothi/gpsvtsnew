
// IntelliJ API Decompiler stub source generated from a class file
// Implementation of methods is not available

package com.bluenet.api;

public class DeviceAPI {
    public DeviceAPI() { /* compiled code */ }

    public static void initializeApiLib() { /* compiled code */ }

    public static void exitLib() { /* compiled code */ }

    public static void checkNet() { /* compiled code */ }

    public static void initSearchCamera() { /* compiled code */ }

    public static void registerSearchCallBack(java.lang.Object obj) { /* compiled code */ }

    public static void destorySearchCamera() { /* compiled code */ }

    public static void searchCamera() { /* compiled code */ }

    public static void registerCallBack(java.lang.Object object) { /* compiled code */ }

    public static long createCamera(String username, String passwd, String ip, int port, String camID) { /* compiled code */
        return 0;
    }

    public static void destoryCamera(long camHandler) { /* compiled code */ }

    public static int openCamera(long camHandler) { /* compiled code */
        return 0;
    }

    public static void closeCamera(long camHandler) { /* compiled code */ }

    public static void getCameraParam(long camHandler, int paramKey) { /* compiled code */ }

    public static void setCameraParam(long camHandler, int paramKey, java.lang.String param) { /* compiled code */ }

    public static void openStream(long camHandler, int streamId, int resolution) { /* compiled code */ }

    public static void closeStream(long camHandler) { /* compiled code */ }

    public static void registerStreamCallBack(long camHandler, java.lang.Object streamCallbackObj) { /* compiled code */ }

    public static void control(long camHandler, int cmd) { /* compiled code */ }

    public static void openAudio(long camHandler, int AudioId) { /* compiled code */ }

    public static void closeAudio(long camHandler) { /* compiled code */ }

    public static void openTalk(long camHandler) { /* compiled code */ }

    public static void closeTalk(long camHandler) { /* compiled code */ }

    public static void sendTalkData(long camHandler, byte[] data, int size) { /* compiled code */ }

    public static void queryRecordFileList(long camHandler, int bYear, int bMon, int bDay, int eYear, int eMon, int eDay) { /* compiled code */ }

    public static void registerRecordCallBack(long camHandler, java.lang.Object streamCallbackObj) { /* compiled code */ }

    public static void openPlayRecord(long camHandler, java.lang.String filename, int position) { /* compiled code */ }

    public static void closePlayRecord(long camHandler, java.lang.String filename) { /* compiled code */ }

    public static void playRecordPosition(long camHandler, java.lang.String filename, int position) { /* compiled code */ }

    public static void startRecorder(long camHandler, java.lang.String filename, int width, int height, int frame) { /* compiled code */ }

    public static void stopRecorder(long camHandler) { /* compiled code */ }

    public static void takePicture(long camHandler, java.lang.String filename) { /* compiled code */ }

    public static void downloadFile(long camHandler, java.lang.String srcFilename, java.lang.String dstfilename) { /* compiled code */ }

    public static int queryDownloadPosition(long camHandler) { /* compiled code */
        return 0;
    }

    public static void stopDownloadFile(long camHandler) { /* compiled code */ }
}