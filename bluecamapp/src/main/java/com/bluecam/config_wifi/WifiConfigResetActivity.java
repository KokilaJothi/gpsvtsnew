//package com.bluecam.config_wifi;
//
//import android.media.MediaPlayer;
//import android.os.Bundle;
//import android.text.Html;
//import android.view.View;
//import android.widget.Button;
//import android.widget.TextView;
//
//import com.bluecam.demo.BaseActivity;
//import com.bluecam.demo.R;
//import com.bluenet.utils.CommonUtils;
//import com.bluenet.utils.HeaderBar;
//
//import java.util.Locale;
//
///**
// * Created by Administrator on 2017/7/1.
// */
//
//public class WifiConfigResetActivity extends BaseActivity
//{
//
//    private int lanIndex=0;
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.wifi_config_reset_screen);
//        lanIndex = PreferencesUtil.getInt(this, CommonUtils.APP_SETTING_LANGUAGE_TOAST,0);
//        initView();
//        removeWifiConfigActivity();
//        addWifiConfigActvity(this);
//    }
//
//    @Override
//    protected void onDestroy() {
//        super.onDestroy();
//        stopAudio();
//    }
//
//    private void initView()
//    {
//        header_bar = (HeaderBar)findViewById(R.id.header_bar);
//        header_bar.setTitleText(getText(R.string.wifi_config_reset_title));
//        header_bar.setClickListener(new HeaderBar.OnHeaderBarClickListener() {
//            @Override
//            public void back() {
//                finish();
//            }
//        });
//
//        TextView reset_toast = (TextView)findViewById(R.id.reset_toast);
//        String str1 = CommonUtils.getFond(getTextString(R.string.wifi_config_reset_en),"#EB7373");//"<font color=\"#EB7373\">"+getTextString(R.string.wifi_config_reset_en)+"</font>";
//        reset_toast.setText(Html.fromHtml(getTextString(R.string.wifi_config_reset_toast1)+str1));
//        Button done_btn = (Button)findViewById(R.id.done_btn);
//        done_btn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                openActivity(WifiConfigSetup2Activity.class);
//                finish();
//            }
//        });
//        playAudio();
//
//    }
//    MediaPlayer mediaPlayer = null;
//    public void stopAudio() {
//        if (mediaPlayer != null ) {
//            mediaPlayer.stop();
//            //mediaPlayer.release();
//        }
//    }
//
//    public void playAudio() {
//        try {
//            if(lanIndex >1){
//                mediaPlayer = MediaPlayer.create(this, R.raw.wifi_config_setup4_en);
//                mediaPlayer.start();
//            }
//            else{
//                if (Locale.getDefault().getLanguage().toString().toUpperCase().equals("ZH") || Locale.getDefault().getLanguage().toString().equals("zh_CN")) {
//                    mediaPlayer = MediaPlayer.create(this, R.raw.wifi_config_setup4_cn);
//                    mediaPlayer.start();
//                } else {
//                    mediaPlayer = MediaPlayer.create(this, R.raw.wifi_config_setup4_en);
//                    mediaPlayer.start();
//                }
//            }
//            /*if (Locale.getDefault().getLanguage().toString().equals("zh") || Locale.getDefault().getLanguage().toString().equals("zh_CN")) {
//                mediaPlayer = MediaPlayer.create(this, R.raw.wifi_config_setup4_cn);
//                mediaPlayer.start();
//            } else {
//                mediaPlayer = MediaPlayer.create(this, R.raw.wifi_config_setup4_en);
//                mediaPlayer.start();
//            }*/
//        } catch (IllegalArgumentException e) {
//            e.printStackTrace();
//        } catch (IllegalStateException e) {
//            e.printStackTrace();
//        }
//    }
//
//}
