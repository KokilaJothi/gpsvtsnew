//package com.bluecam.config_wifi;
//
//import android.media.MediaPlayer;
//import android.os.Bundle;
//import android.view.KeyEvent;
//import android.view.View;
//import android.widget.Button;
//import android.widget.TextView;
//
//import com.bluecam.demo.BaseActivity;
//import com.bluecam.demo.R;
//import com.bluecam.permission.PermissionUtils;
//import com.bluenet.utils.HeaderBar;
//
//import java.util.Locale;
//
///**
// * Created by Administrator on 2017/7/1.
// */
//
//public class WifiConfigSetup1Activity extends BaseActivity {
//    private TextView reset_btn;
//    private Button done_btn;
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.wifi_config_setup1_screen);
//        //lanIndex = PreferencesUtil.getInt(this, CommonUtils.APP_SETTING_LANGUAGE_TOAST,0);
//        addWifiConfigActvity(this);
//        autoSetAudioVolumn(0.6f);
//        initView();
//
//        PermissionUtils.requestGpsPermission(this, this);
//    }
//
//    @Override
//    protected void onDestroy() {
//        super.onDestroy();
//        removeWifiConfigActivity();
//    }
//
//    private void initView() {
//        header_bar = (HeaderBar) findViewById(R.id.header_bar);
//        header_bar.setTitleText(getTextString(R.string.wifi_config_setup1_title));
//        header_bar.setClickListener(new HeaderBar.OnHeaderBarClickListener() {
//            @Override
//            public void back() {
//                stopAudio();
//                finish();
//            }
//        });
//
//        reset_btn = (TextView) findViewById(R.id.reset_btn);
//        done_btn = (Button) findViewById(R.id.done_btn);
//
//        reset_btn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                stopAudio();
//                openActivity(WifiConfigResetActivity.class);
//            }
//        });
//        done_btn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                stopAudio();
//                openActivity(WifiConfigSetup2Activity.class);
//            }
//        });
//        playAudio();
//
//    }
//
//    MediaPlayer mediaPlayer = null;
//
//    public void stopAudio() {
//        if (mediaPlayer != null ) {
//            mediaPlayer.stop();
//            //mediaPlayer.release();
//        }
//    }
//
//    public void playAudio() {
//        try {
//            if(lanIndex >1){
//                mediaPlayer = MediaPlayer.create(this, R.raw.wifi_config_setup1_en);
//                mediaPlayer.start();
//            }
//            else{
//
//                String strLanguage = Locale.getDefault().getLanguage().toString();
//
//                if (strLanguage.toUpperCase().equals("ZH") || strLanguage.equals("zh_CN")) {
//                    mediaPlayer = MediaPlayer.create(this, R.raw.wifi_config_setup1_cn);
//                    mediaPlayer.start();
//                } else {
//                    mediaPlayer = MediaPlayer.create(this, R.raw.wifi_config_setup1_en);
//                    mediaPlayer.start();
//                }
//
//            }
//
//        } catch (IllegalArgumentException e) {
//            e.printStackTrace();
//        } catch (IllegalStateException e) {
//            e.printStackTrace();
//        }
//    }
//    @Override
//    public boolean onKeyDown(int keyCode, KeyEvent event) {
//        if (keyCode == KeyEvent.KEYCODE_BACK) {
//            stopAudio();
//            finish();
//        }
//        return super.onKeyDown(keyCode, event);
//    }
//}
//
