package com.bluecam.demo;

import android.graphics.drawable.ColorDrawable;
import android.opengl.GLSurfaceView;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;


import androidx.annotation.Nullable;

import com.bluecam.view.ShaderRender;
import com.bluenet.camManager.BCamera;
import com.bluenet.camManager.CameraContants;
import com.bluenet.camManager.CameraManager;
import com.decode.utils.AudioPlayer;
import com.decode.utils.CustomAudioRecorder;
import com.decode.utils.CustomBuffer;
import com.decode.utils.CustomBufferData;
import com.decode.utils.CustomBufferHead;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Administrator on 2017/8/1.
 */

public class CameraPlayActivity extends BaseActivity implements ShaderRender.RenderListener , CustomAudioRecorder.AudioRecordResult{
    private static final int      AUDIO_BUFFER_START_CODE = 0xff00ff;
    public final static String IMAGE_PATH = Environment.getExternalStorageDirectory().getAbsolutePath() + "/cameraDemo/image/";
    public final static String VIDEO_PATH = Environment.getExternalStorageDirectory().getAbsolutePath() + "/cameraDemo/video/";
    private GLSurfaceView gls_view;
    private ShaderRender render;
    private Button audio_btn,record_btn,ircut_btn,talk_btn;

    private CameraManager cameraManager;
    private BCamera camera;
    private boolean       isLoaded = false;
    private int           videoW;
    private int           videoH;
    private AudioPlayer audioPlayer;
    private CustomBuffer audioBuffer ;
    private CustomAudioRecorder     audioRecorder = null;

    private boolean isOpenAudio  = false;
    private boolean isStartRecord = false;
    private int flip;  //镜像状态
    private int ircut; //红外状态
    private boolean   m_bUpDownMirror = false;
    private boolean   m_bLeftRightMirror = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_play);
        initView();
        String camID  = getIntent().getStringExtra("camID");
        cameraManager = CameraManager.getDeviceManager(this.getApplicationContext());
        cameraManager.registerEventListener(this);
        camera        = cameraManager.getCamera(camID);
        if(camera == null){
            finish();
        }

        //audioBuffer = new CustomBuffer();
        //audioRecorder = new CustomAudioRecorder(this);

        camera.setStreamCallBack(render);
        camera.playStream(CameraContants.Resolution.MID);
        camera.getCameraParam(CameraContants.ParamKey.GET_CAMERA_PARAM_KEY);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        camera.closeStream();
        stopPlayAudio();
    }

    private void initView() {
        initHeaderBar();
        setTitle_txt("Video playback");
        gls_view = (GLSurfaceView)findViewById(R.id.gls_view);
        render   = new ShaderRender(gls_view);
        gls_view.setRenderer(render);
        render.setListener(this);

        audio_btn = (Button)findViewById(R.id.audio_btn);
        record_btn = (Button)findViewById(R.id.record_btn);
        ircut_btn = (Button)findViewById(R.id.ircut_btn);
        talk_btn = (Button)findViewById(R.id.talk_btn);
        talk_btn.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                if (event.getAction() == MotionEvent.ACTION_DOWN)
                {
                    virbate();
                    stopPlayAudio();
                    camera.openTalk();
                    if(audioRecorder != null){
                        audioRecorder.startRecord();
                    }
                }
                else if (event.getAction() == MotionEvent.ACTION_UP)
                {
                    //结束说话
                    playAudio();
                    camera.closeTalk();
                    if(audioRecorder != null){
                        audioRecorder.stopRecord();
                    }
                }
                return true;
            }
        });
    }

    /**
     * 上下镜像事件
     * @param view
     */
    public void upDownFun(View view){
        setUpDownFlip();
    }
    /**
     * 左右镜像事件
     * @param view
     */
    public void leftRightFun(View view){
        setLefgRightFlip();
    }
    /*
        红外开关事件
     */
    public void openIrcut(View view){
        if(ircut == 0){
            camera.setCameraParam(14,1); //打开
        }
        else{
            camera.setCameraParam(14,0); //关闭
        }
    }

    /**
     * 选择分辨率事件
     * @param view
     */
    public void selectResolution(View view){
        LayoutInflater inflater = LayoutInflater.from(this);
        View viewRoot = inflater.inflate(R.layout.resolution_window, null);

        final PopupWindow editWindow = new PopupWindow(viewRoot, LinearLayout.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.WRAP_CONTENT);
        editWindow.setFocusable(true);
        editWindow.setOutsideTouchable(true);
        editWindow.setBackgroundDrawable(new ColorDrawable(0));
        editWindow.showAtLocation(gls_view, Gravity.BOTTOM,0, 0);

        TextView high_tv = (TextView)viewRoot.findViewById(R.id.high_tv);
        TextView mid_tv  = (TextView)viewRoot.findViewById(R.id.mid_tv);
        TextView low_tv  = (TextView)viewRoot.findViewById(R.id.low_tv);
        high_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editWindow.dismiss();
                camera.closeStream();
                stopPlayAudio();
                camera.playStream(CameraContants.Resolution.HIGH);
            }
        });
        mid_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editWindow.dismiss();
                camera.closeStream();
                stopPlayAudio();
                camera.playStream(CameraContants.Resolution.MID);
            }
        });
        low_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editWindow.dismiss();
                camera.closeStream();
                stopPlayAudio();
                camera.playStream(CameraContants.Resolution.LOW);
            }
        });
    }

    /**
     * 录像事件监听
     * @param view
     */
    public void recordVideo(View view){
        if(camera == null){
            return;
        }
        if(!isStartRecord){
            SimpleDateFormat f = new SimpleDateFormat("yyyyMMdd_HHmmss");
            String strDate = f.format(new Date());
            File videoDir = new File(VIDEO_PATH);
            if(!videoDir.exists())
                videoDir.mkdirs();
            String fileName = VIDEO_PATH + strDate+"_"+camera.getCameraBean().getDevID()+".mp4";
            System.out.println("----------------fileName == "+fileName);
            camera.startRecordVideo(fileName, videoW, videoH);
            record_btn.setText("Recording");
        }
        else{
            camera.stopRecordVideo();
            record_btn.setText("Video");
        }

    }

    /*
    拍照事件
     */
    public  void takePicture(View view){
        SimpleDateFormat f = new SimpleDateFormat("yyyyMMdd_HHmmss");
        String strDate = f.format(new Date());
        String fileName = strDate+"_"+camera.getCameraBean().getDevID()+".jpg";
        File imageDir = new File(IMAGE_PATH);
        if(!imageDir.exists())
            imageDir.mkdirs();
        String path = IMAGE_PATH + fileName;
        System.out.println("path =="+path);
        camera.takePicture(path);
        showToast("拍照成功");
    }
    /**
     * 打开声音事件
     * @param view
     */
    public void openAudio(View view){
        if(!isOpenAudio){
            isOpenAudio = true;
            playAudio();
            audio_btn.setText("关闭声音");
        }
        else{
            isOpenAudio = false;
            stopPlayAudio();
            audio_btn.setText("打开声音");
        }
    }

    public void playAudio()
    {
        if(camera == null) {
            return;
        }
        if(camera.getStatus() != CameraContants.DeviceStatus.DEVICE_STATUS_ON_LINE)
            return;
        audioBuffer.ClearAll();
        if(audioPlayer == null)
        {
            audioPlayer = new AudioPlayer(audioBuffer);
        }
        audioPlayer.AudioPlayStart();
        camera.openAudio();
    }

    public void stopPlayAudio()
    {
        audioBuffer.ClearAll();
        if(audioPlayer != null)
        {
            if(camera != null)
                camera.closeAudio();
            audioPlayer.AudioPlayStop();
            audioPlayer = null;
        }
    }

    public void setUpDownFlip()
    {
        int value;
        if (m_bUpDownMirror)
        {
            if (m_bLeftRightMirror)
                value = 2;
            else
                value = 0;
        }
        else
        {
            if (m_bLeftRightMirror)
                value = 3;
            else
                value = 1;
        }
        camera.setCameraParam(5,value);

    }


    public void setLefgRightFlip()
    {
        int value;
        if (m_bLeftRightMirror)
        {
            if (m_bUpDownMirror)
                value = 1;
            else
                value = 0;
        }
        else
        {
            if (m_bUpDownMirror)
                value = 3;
            else
                value = 2;
        }
        camera.setCameraParam(5,value);
    }
    @Override
    public void loadComplete(int size, int width, int height) {
        if(!isLoaded){
            isLoaded = true;
            videoW = width;
            videoH = height;
        }
    }

    @Override
    public void AudioRecordData(byte[] data, int len) {
        if(camera == null)
            return;
        //发送说话数据
        camera.sendTalkData(data,len);
    }

    @Override
    public void onAudioDataEvent(long camHandler, byte[] pcm, int size) {
        if(camera == null)
            return;
        if(camHandler == camera.getCamHandler())
        {
            CustomBufferHead head = new CustomBufferHead();
            CustomBufferData data = new CustomBufferData();
            head.length = size;
            head.startcode = AUDIO_BUFFER_START_CODE;
            data.head = head;
            data.data = pcm;
            if(audioPlayer != null) {
                if(audioPlayer.isAudioPlaying())
                    audioBuffer.addData(data);
            }
        }
    }

    @Override
    public void onRecordFileList(long camHandler, int filecount, String fileName, String strDate, int size) {

    }

    @Override
    public void onGetParamsEvent(long camHandler, long paramKey, String params) {
        if(camera == null){
            return;
        }
        if(camHandler == camera.getCamHandler()){
            if(paramKey == CameraContants.ParamKey.GET_CAMERA_PARAM_KEY){
                JSONObject obj = null;
                try {
                    obj = new JSONObject(params);
                    flip = obj.getInt("flip");   // 镜像属性
                    ircut = obj.getInt("ircut"); //红外开关属性
                    this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if(ircut == 0){
                                ircut_btn.setText("打开红外");
                            }
                            else{
                                ircut_btn.setText("关闭红外");
                            }
                            if(flip == 0){
                                //normal
                                m_bUpDownMirror = false;
                                m_bLeftRightMirror = false;
                            }
                            else if(flip == 1){
                                // up down mirror
                                m_bUpDownMirror = true;
                                m_bLeftRightMirror = false;
                            }
                            else if(flip == 2){
                                // left right mirror
                                m_bUpDownMirror = false;
                                m_bLeftRightMirror = true;
                            }
                            else if(flip == 3){
                                // all mirror
                                m_bUpDownMirror = true;
                                m_bLeftRightMirror = true;
                            }
                        }
                    });
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }
    }



    public void up(View view){
        new ControlCameraTask(CameraContants.Control.CMD_PTZ_UP).execute();
    }
    public void down(View view){
        new ControlCameraTask(CameraContants.Control.CMD_PTZ_DOWN).execute();
    }
    public void left(View view){
        new ControlCameraTask(CameraContants.Control.CMD_PTZ_LEFT).execute();
    }
    public void right(View view){
        new ControlCameraTask(CameraContants.Control.CMD_PTZ_RIGHT).execute();
    }
    /**
     * 处理上下左右控制
     */
    private class ControlCameraTask extends AsyncTask<Void, Void, Integer>
    {
        private int type;
        public ControlCameraTask(int type) {
            this.type = type;
        }
        @Override
        protected Integer doInBackground(Void... arg0)
        {

            if(type == CameraContants.Control.CMD_PTZ_RIGHT)
            {
                camera.controlCamera(CameraContants.Control.CMD_PTZ_RIGHT);
                try {
                    Thread.sleep(1500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                camera.controlCamera(CameraContants.Control.CMD_PTZ_RIGHT_STOP);
            }
            else if(type ==CameraContants.Control.CMD_PTZ_LEFT)
            {
                camera.controlCamera(CameraContants.Control.CMD_PTZ_LEFT);
                try {
                    Thread.sleep(1500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                camera.controlCamera(CameraContants.Control.CMD_PTZ_LEFT_STOP);
            }
            else if(type ==CameraContants.Control.CMD_PTZ_UP)
            {
                camera.controlCamera(CameraContants.Control.CMD_PTZ_UP);
                try {
                    Thread.sleep(1500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                camera.controlCamera(CameraContants.Control.CMD_PTZ_UP_STOP);
            }
            else if(type ==CameraContants.Control.CMD_PTZ_DOWN)
            {
                camera.controlCamera(CameraContants.Control.CMD_PTZ_DOWN);
                try {
                    Thread.sleep(1500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                camera.controlCamera(CameraContants.Control.CMD_PTZ_DOWN_STOP);
            }
            return 0;
        }

        @Override
        protected void onPostExecute(Integer result) {
            super.onPostExecute(result);

        }
    }
}
