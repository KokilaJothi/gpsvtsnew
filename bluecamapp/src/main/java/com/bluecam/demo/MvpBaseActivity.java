package com.bluecam.demo;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.os.Bundle;

import com.bluecam.presenter.BasePresenter;
import com.bluenet.utils.LogUtil;

@SuppressWarnings("unchecked")
public abstract class MvpBaseActivity<V,T extends BasePresenter<V>> extends BaseActivity {

    protected T mPresenter;
    protected HeadSetPlugListenner   headSetPlugListenner;

    protected AudioManager audioManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPresenter = createPresenter();
        mPresenter.bindView((V)this,this.getApplicationContext());
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mPresenter.unBindView();
    }

    /**
     *创建Presenter
     * @return
     */
    protected abstract T createPresenter();

    protected void headPlugIn(){

    }
    protected void headPlugOut(){

    }

    class HeadSetPlugListenner extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            // TODO Auto-generated method stub
            if (intent.hasExtra("state")) {
                if (intent.getIntExtra("state", 2) == 0){
                    //拔出
                    LogUtil.printLog("HeadSetPlugListenner----------------耳机 拔出------------------");
                    headPlugOut();
                }else if (intent.getIntExtra("state", 2) == 1) {
                    //插入
                    LogUtil.printLog("HeadSetPlugListenner----------------耳机 插入------------------");
                    headPlugIn();
                }
            }
        }
    }

}
