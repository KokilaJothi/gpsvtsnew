//package com.bluecam.demo;
//
//import android.app.Activity;
//import android.app.ProgressDialog;
//import android.content.Context;
//import android.content.Intent;
//import android.content.SharedPreferences;
//import android.content.pm.PackageManager;
//import android.graphics.PixelFormat;
//import android.media.MediaPlayer;
//import android.opengl.GLSurfaceView;
//import android.os.AsyncTask;
//import android.os.Environment;
//import androidx.core.app.ActivityCompat;
//import androidx.appcompat.app.AppCompatActivity;
//import androidx.recyclerview.widget.RecyclerView;
//
//import android.os.Bundle;
//import android.os.Handler;
//import android.util.Log;
//import android.view.MenuItem;
//import android.view.MotionEvent;
//import android.view.SurfaceHolder;
//import android.view.View;
//import android.widget.Button;
//import android.widget.ImageView;
//import android.widget.Toast;
//
//import com.bluecam.adapter.BlueCamListAdapter;
//import com.bluecam.libs.Search;
//import com.bluenet.camManager.BCamera;
//import com.bluenet.camManager.Camera;
//import com.bluenet.camManager.CameraBean;
//import com.bluenet.camManager.CameraContants;
//import com.bluenet.camManager.CameraManager;
//import com.bluenet.component.ShaderRender;
//import com.bluenet.utils.FileUtil;
//import com.bluenet.utils.LogUtil;
//import com.decode.utils.CustomAudioRecorder;
//
//import org.json.JSONArray;
//import org.json.JSONException;
//import org.json.JSONObject;
//
//import java.text.SimpleDateFormat;
//import java.util.ArrayList;
//import java.util.Date;
//import java.util.List;
//
//public class CameraViewActivity extends AppCompatActivity implements CustomAudioRecorder.AudioRecordResult{
//
//    private static final String TAG = "MainActivity";
//    private int m_uid = -1;
//    private GLSurfaceView surfaceView = null;
//    private SurfaceHolder holder;
//    private Button bt_talk_id;
//    private ShaderRender render;
//    List<BlueCamResponse> blueCamResponses;
//    ProgressDialog progressBar;
//    RecyclerView recyclerViewCameraList;
//    ImageView imageView;
//    Button cameraview;
//    String camid;
//    private boolean firstclick = false;
//
//    public static final int DEVICE_STATUS_CHECK_ACCOUNT = 12;
//
//    public static final int DEVICE_STATUS_CONNECTING = 0;
//
//    public static final int DEVICE_STATUS_CONNECT_ERRER = 101;
//
//    public static final int DEVICE_STATUS_CONNECT_TIMEOUT = 10;
//
//    public static final int DEVICE_STATUS_CREATE_ERR = 20;
//
//    public static final int DEVICE_STATUS_INVALID_ID = 5;
//
//    public static final int DEVICE_STATUS_NEWSDK_CONNECT_ERRER = 3;
//
//    public static final int DEVICE_STATUS_NEWSDK_DISCONNECT = 2;
//
//    public static final int DEVICE_STATUS_NEWSDK_ON_LINE = 1;
//
//    public static final int DEVICE_STATUS_NOT_ON_LINE = 9;
//
//    public static final int DEVICE_STATUS_ON_LINE = 1;
//
//    public static final int DEVICE_STATUS_OPEN_ERR = 21;
//
//    public static final int DEVICE_STATUS_OPEN_PWD_ERR = -23;
//
//    public static final int DEVICE_STATUS_UNKNOWN = -1;
//
//    public static final int DEVICE_STATUS_USER_ERR = -23;
//
////    BroadcastReceiver appendChatScreenMsgReceiver = new BroadcastReceiver() {
////        @Override
////        public void onReceive(Context context, Intent intent) {
////
////
////            int position=intent.getIntExtra("position",0);
////            setUpCam(blueCamResponses.get(position));
////            // Toast.makeText(context, ""+position, Toast.LENGTH_SHORT).show();
////
////        }
////    };
//
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_camera);
//        final SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences("CamerPreference",0);
//
//
//        blueCamResponses=new ArrayList<>();
//        //recyclerViewCameraList=(RecyclerView)findViewById(R.id.recyclerViewCameraList);
//        imageView = (ImageView)findViewById(R.id.back_icon);
//        cameraview = findViewById(R.id.viewcamera);
//        progressBar = new ProgressDialog(this);
//        progressBar.setCancelable(false);
//        progressBar.setMessage("Loading");
//
//        imageView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                try {
////                    Camera camera = getCamera();
////                    camera.closeStream();
//                    Intent myIntent = new Intent(CameraViewActivity.this,Class.forName("com.vamosys.vamos.MapMenuActivity"));
//                    startActivity(myIntent );
//                } catch (ClassNotFoundException e) {
//                    e.printStackTrace();
//                }
//            }
//        });
//        cameraview.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                boolean clickvalue  = sharedPreferences.getBoolean("Clickin",false);
//                Log.d("booleanvalue",""+clickvalue);
//                Log.d("firstclickvalue",""+firstclick);
//
//                if (!clickvalue) {
//                   // Toast.makeText(CameraViewActivity.this, "sharedfalse", Toast.LENGTH_SHORT).show();
//                    Log.d("Clickvalue","sucesss");
//
//                   SharedPreferences.Editor editor = sharedPreferences.edit();
//                   editor.putBoolean("Clickin",true);
//                   editor.commit();
//                   if (!firstclick) {
//                     //  Toast.makeText(CameraViewActivity.this, "Firstfalse", Toast.LENGTH_SHORT).show();
//                       Log.d("Clickvalue","sucesss1");
//                       firstclick = true;
//                       Log.d("blueCamResponsessize",""+blueCamResponses.size());
//                       addCamera(blueCamResponses.get(0));
//                       new Handler().postDelayed(new Runnable() {
//                           @Override
//                           public void run() {
//
//                               Camera camera = getCamera(blueCamResponses.get(0));
//                               if (camera !=null){
//                                   camera.connectCamera();
//                                   camera.setStreamCallBack(render, 0);
//                                   camera.playStream(0);
//                                   int stat = camera.getStatus();
//                                   Log.d("camerastatus",""+camera.getStatus());
//                                   if (stat == 0){
//
//                                       Toast.makeText(CameraViewActivity.this, "DEVICE CONNECTING", Toast.LENGTH_SHORT).show();
//
//                                   }else if (stat == 1){
//
//                                       Toast.makeText(CameraViewActivity.this, "DEVICE ONLINE", Toast.LENGTH_SHORT).show();
//                                   }else if (stat == 2){
//
//                                       Toast.makeText(CameraViewActivity.this, "DEVICE DISCONNECT", Toast.LENGTH_SHORT).show();
//                                   }else if (stat == 3){
//
//                                       Toast.makeText(CameraViewActivity.this, "DEVICE CONNECT ERROR", Toast.LENGTH_SHORT).show();
//                                   }else if (stat == 5){
//                                       Toast.makeText(CameraViewActivity.this, "DEVICE INVALID ID", Toast.LENGTH_SHORT).show();
//
//                                   } else if (stat == 9){
//
//                                       Toast.makeText(CameraViewActivity.this, "DEVICE NOT ONLINE", Toast.LENGTH_SHORT).show();
//
//                                   }else if (stat == 12){
//
//                                       Toast.makeText(CameraViewActivity.this, "DEVICE CHECK ACCOUNT", Toast.LENGTH_SHORT).show();
//
//                                   }else if (stat == 20){
//
//                                       Toast.makeText(CameraViewActivity.this, "DEVICE CREATE ERR", Toast.LENGTH_SHORT).show();
//
//                                   }else if (stat == 21){
//
//                                       Toast.makeText(CameraViewActivity.this, "DEVICE OPEN ERROR", Toast.LENGTH_SHORT).show();
//
//                                   }else if (stat == 101){
//
//                                       Toast.makeText(CameraViewActivity.this, "DEVICE CONNECT ERROR", Toast.LENGTH_SHORT).show();
//
//                                   }
//                                   else if (stat == -23){
//
//                                       Toast.makeText(CameraViewActivity.this, "DEVICE USER ERROR", Toast.LENGTH_SHORT).show();
//
//                                   }
//                                   while (stat == 10){
//                                       try{
//                                           //  SystemClock.sleep(2000);
//                                           // camera.closeStream();
//                                           Toast.makeText(CameraViewActivity.this, "DEVICE CONNECT TIMEOUT", Toast.LENGTH_SHORT).show();
//                                           camera = getCamera(blueCamResponses.get(0));
//                                           camera.connectCamera();
//                                           camera.setStreamCallBack(render,0);
//                                           camera.playStream(2);
//                                           stat = camera.getStatus();
//                                       }
//                                       catch(Exception  ce) {
//                                           ce.printStackTrace();
//                                       }
//
//                                   }
//
//                               }
//
//                           }
//                       }, 2000);
//
//                   }else {
//                     //  Toast.makeText(CameraViewActivity.this, "FirstTrue", Toast.LENGTH_SHORT).show();
//                       Log.d("Clickvalue","sucesss2");
//                       new Handler().postDelayed(new Runnable() {
//                           @Override
//                           public void run() {
//                               Camera camera = getCamera(blueCamResponses.get(0));
//                               if (camera !=null) {
//                                   camera.connectCamera();
//                                   camera.setStreamCallBack(render, 0);
//                                   camera.playStream(0);
//                                   int stat = camera.getStatus();
//                                   Log.d("camerastatus1", "" + camera.getStatus());
//                                   if (stat == 0) {
//
//                                       Toast.makeText(CameraViewActivity.this, "DEVICE CONNECTING", Toast.LENGTH_SHORT).show();
//
//                                   } else if (stat == 1) {
//
//                                       Toast.makeText(CameraViewActivity.this, "DEVICE ONLINE", Toast.LENGTH_SHORT).show();
//                                   } else if (stat == 2) {
//
//                                       Toast.makeText(CameraViewActivity.this, "DEVICE DISCONNECT", Toast.LENGTH_SHORT).show();
//                                   } else if (stat == 3) {
//
//                                       Toast.makeText(CameraViewActivity.this, "DEVICE CONNECT ERROR", Toast.LENGTH_SHORT).show();
//                                   } else if (stat == 5) {
//                                       Toast.makeText(CameraViewActivity.this, "DEVICE INVALID ID", Toast.LENGTH_SHORT).show();
//
//                                   } else if (stat == 9) {
//
//                                       Toast.makeText(CameraViewActivity.this, "DEVICE NOT ONLINE", Toast.LENGTH_SHORT).show();
//
//                                   } else if (stat == 12) {
//
//                                       Toast.makeText(CameraViewActivity.this, "DEVICE CHECK ACCOUNT", Toast.LENGTH_SHORT).show();
//
//                                   } else if (stat == 20) {
//
//                                       Toast.makeText(CameraViewActivity.this, "DEVICE CREATE ERR", Toast.LENGTH_SHORT).show();
//
//                                   } else if (stat == 21) {
//
//                                       Toast.makeText(CameraViewActivity.this, "DEVICE OPEN ERROR", Toast.LENGTH_SHORT).show();
//
//                                   } else if (stat == 101) {
//
//                                       Toast.makeText(CameraViewActivity.this, "DEVICE CONNECT ERROR", Toast.LENGTH_SHORT).show();
//
//                                   } else if (stat == -23) {
//
//                                       Toast.makeText(CameraViewActivity.this, "DEVICE USER ERROR", Toast.LENGTH_SHORT).show();
//
//                                   }
//                                   while (stat == 10) {
//
//                                       try {
//                                           //  SystemClock.sleep(2000);
//                                           //  camera.closeStream();
//                                           Toast.makeText(CameraViewActivity.this, "DEVICE CONNECT TIMEOUT", Toast.LENGTH_SHORT).show();
//                                           camera = getCamera(blueCamResponses.get(0));
//                                           camera.connectCamera();
//                                           camera.setStreamCallBack(render, 0);
//                                           camera.playStream(2);
//                                           stat = camera.getStatus();
//                                       } catch (Exception ce) {
//                                           ce.printStackTrace();
//                                       }
//
//                                   }
//                               }
//                           }
//                       },2000);
//                   }
//
//                } else{
//             // Toast.makeText(CameraViewActivity.this, "saredtrue", Toast.LENGTH_SHORT).show();
//                    Log.d("Clickvalue","sucesss3");
//              new Handler().postDelayed(new Runnable() {
//                  @Override
//                  public void run() {
//                      Camera camera = getCamera(blueCamResponses.get(0));
//                      if (camera !=null){
//                          camera.connectCamera();
//                          camera.setStreamCallBack(render,0);
//                          camera.playStream(0);
//                          int stat = camera.getStatus();
//                          Log.d("camerastatus2",""+camera.getStatus());
//                          if (stat == 0){
//
//                              Toast.makeText(CameraViewActivity.this, "DEVICE CONNECTING", Toast.LENGTH_SHORT).show();
//
//                          }else if (stat == 1){
//
//                              Toast.makeText(CameraViewActivity.this, "DEVICE ONLINE", Toast.LENGTH_SHORT).show();
//                          }else if (stat == 2){
//
//                              Toast.makeText(CameraViewActivity.this, "DEVICE DISCONNECT", Toast.LENGTH_SHORT).show();
//                          }else if (stat == 3){
//
//                              Toast.makeText(CameraViewActivity.this, "DEVICE CONNECT ERROR", Toast.LENGTH_SHORT).show();
//                          }else if (stat == 5){
//                              Toast.makeText(CameraViewActivity.this, "DEVICE INVALID ID", Toast.LENGTH_SHORT).show();
//
//                          } else if (stat == 9){
//
//                              Toast.makeText(CameraViewActivity.this, "DEVICE NOT ONLINE", Toast.LENGTH_SHORT).show();
//
//                          }else if (stat == 12){
//
//                              Toast.makeText(CameraViewActivity.this, "DEVICE CHECK ACCOUNT", Toast.LENGTH_SHORT).show();
//
//                          }else if (stat == 20){
//
//                              Toast.makeText(CameraViewActivity.this, "DEVICE CREATE ERR", Toast.LENGTH_SHORT).show();
//
//                          }else if (stat == 21){
//
//                              Toast.makeText(CameraViewActivity.this, "DEVICE OPEN ERROR", Toast.LENGTH_SHORT).show();
//
//                          }else if (stat == 101){
//
//                              Toast.makeText(CameraViewActivity.this, "DEVICE CONNECT ERROR", Toast.LENGTH_SHORT).show();
//
//                          }
//                          else if (stat == -23){
//
//                              Toast.makeText(CameraViewActivity.this, "DEVICE USER ERROR", Toast.LENGTH_SHORT).show();
//
//                          }
//                          while (stat == 10){
//                              try{
//                                  // SystemClock.sleep(2000);
//                                  // camera.closeStream();
//                                  Toast.makeText(CameraViewActivity.this, "DEVICE CONNECT TIMEOUT", Toast.LENGTH_SHORT).show();
//                                  camera = getCamera(blueCamResponses.get(0));
//                                  camera.connectCamera();
//                                  camera.setStreamCallBack(render,0);
//                                  camera.playStream(2);
//                                  stat = camera.getStatus();
//                              }
//                              catch(Exception  ce) {
//                                  ce.printStackTrace();
//                              }
//
//                          }
//                      }
//
//                  }
//              },2000);
//            }
//
//
//            }
//        });
//
//
//
//        //registerReceiver(this.appendChatScreenMsgReceiver, new IntentFilter("appendChatScreenMsg"));
//
//        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//        checkPermission();
//        initSurface();
//        getCameraFromIntent(getIntent().getStringExtra("cameraJsonString"));
//        CameraManager.getDeviceManager(CameraViewActivity.this).initialize();
//        CameraManager.getDeviceManager(CameraViewActivity.this).initCameraManager();
//        Search.getInstance().InitSearchEnv();
//        Search.getInstance().SetSearchDeviceCallBack(this);
//
////
////        addCamera(blueCamResponses.get(0));
////        Camera camera = getCamera();
////        camera.connectCamera();
////        camera.setStreamCallBack(render,0);
////        camera.playStream(0);
//
//        //setUpCam(blueCamResponses.get(0));
//
////        new Handler().postDelayed(new Runnable() {
////            @Override
////            public void run() {
////                setUpCam(blueCamResponses.get(0));
////            }
////        },1000);
//
//
//    }
//
//
//
//
//    @Override
//    protected void onResume() {
//        super.onResume();
//
//       // getCameraFromIntent(getIntent().getStringExtra("cameraJsonString"));
//       // addCamera(blueCamResponses.get(0));
//        //setUpCam(blueCamResponses.get(0));
//    }
//
//    @Override
//    protected void onDestroy() {
//        super.onDestroy();
//       // DeviceAPI.Cleanup();
//    }
//
//    private void setUpCam(BlueCamResponse upCam) {
//
//        addCamera(upCam);
//        Camera camera = getCamera(blueCamResponses.get(0));
//        camera.connectCamera();
//        camera.setStreamCallBack(render,0);
//        camera.playStream(0);
////        new Handler().postDelayed(new Runnable() {
////            @Override
////            public void run() {
////
////                Camera camera = getCamera();
////                camera.setStreamCallBack(render,0);
////                camera.playStream(0);
////            }
////        },1000);
//
//    }
//
//    private void getCameraFromIntent(String jsonString) {
//
//        try {
//            JSONArray jsonArray=new JSONArray(jsonString);
//
//            for(int i=0;i<jsonArray.length();i++){
//                JSONObject jsonObject=jsonArray.getJSONObject(i);
//                BlueCamResponse blueCamResponse=new BlueCamResponse();
//                blueCamResponse.setCameraType(jsonObject.getString("cameraType"));
//                blueCamResponse.setCamId(jsonObject.getString("camId"));
//                blueCamResponse.setCamName(jsonObject.getString("camName"));
//                blueCamResponse.setUserName(jsonObject.getString("userName"));
//                blueCamResponse.setPwd(jsonObject.getString("pwd"));
//                blueCamResponse.setShortName(jsonObject.getString("shortName"));
//                Log.d("cameratype",""+jsonObject.getString("cameraType"));
//                Log.d("camId",""+jsonObject.getString("camId"));
//                Log.d("camName",""+jsonObject.getString("camName"));
//                Log.d("userName",""+jsonObject.getString("userName"));
//                Log.d("pwd",""+jsonObject.getString("pwd"));
//                Log.d("shortName",""+jsonObject.getString("shortName"));
//                blueCamResponses.add(blueCamResponse);
//
//            }
//            Log.d("blueCamResponseslist",""+blueCamResponses);
//
//
//        }catch (JSONException e) {
//            Log.wtf("ERRES",""+e);
//            e.printStackTrace();
//        }
//
//        BlueCamListAdapter blueCamListAdapter=new BlueCamListAdapter(CameraViewActivity.this,blueCamResponses);
//        //recyclerViewCameraList.setAdapter(blueCamListAdapter);
//
//    }
//
//
//    private void addCamera(String camName, String camID, String userName, String passwd, final Context context){
//        Log.d("addcamName",""+camName);
//        Log.d("addcamid",""+camID);
//        Log.d("addusername",""+userName);
//        Log.d("addpwd",""+passwd);
//
//        CameraBean bean = new CameraBean();
//        bean.setDevName(camName);
//        bean.setDevID(camID.toUpperCase());
//        bean.setUsername(userName);
//        bean.setPassword(passwd);
//        bean.setDeviceType(CameraContants.CameraType.NET_CAMERA);
//        final BCamera camera = new Camera();
//        camera.setCameraBean(bean);
//        LogUtil.printLog("camID=="+bean.getDevID());
//
//        new AsyncTask<Void ,Void,Void>(){
//            @Override
//            protected Void doInBackground(Void... voids)
//            {
//                CameraManager.getDeviceManager(context).addCamera(camera);
//                return null;
//            }
//
//            @Override
//            protected void onPostExecute(Void aVoid) {
//                super.onPostExecute(aVoid);
//                progressBar.dismiss();
//                //Intent intent = new Intent();
//                //((Activity)context).setResult(Activity.RESULT_OK, intent);
//                //((Activity)context).finish();
//            }
//        }.execute();
//    }
//
//    //初始化播放相关
//    private void initSurface() {
//
//        surfaceView = this.findViewById(R.id.surfaceView);
//
//        render = new ShaderRender(surfaceView);
//
//        surfaceView.setRenderer(render);
//        surfaceView.getHolder().setFormat(PixelFormat.RGBX_8888);
//    }
//
//
//    public void SDKEventCallBack(int uid, int nType){
//        // Log.i("SDKEventCallBack",  "uid:" + uid + " SDKEvent:" + nType);
//        int status = new Long(nType).intValue();
//        Log.v("SDKEventCallBack","----------status:" + status );
//    }
//
//    public void AlarmCallBack(int uid, int eventid, String data){
//        Log.i(TAG, "uid:" + uid + " eventid:" + eventid + " data:" + data);
//    }
//
//    public void ConfigCallBack(int uid, int cmd, byte[] data, int len){
//
//    }
//
//    public void RealDataCallBack(int uid, int type, byte[] data, int size){
//        Log.i("RealDataCallBack", "uid:" + uid + " RealData type:" + type + " size:" + size);
//
//    }
//    public void PlayBackCallBack(int uid, int type, byte[] data, int size){
//        Log.i(TAG, "uid:" + uid + " PlayBack type:" + type + " size:" + size);
//    }
//
//    //搜索回调方法
//    public void SearchCallBack(String data)
//    {
//        Log.i(TAG, data);
//    }
//
//    private void checkPermission()
//    {
//        final int REQUEST_EXTERNAL_STORAGE = 1;
//        String[] PERMISSIONS_STORAGE = {
//                "android.permission.READ_EXTERNAL_STORAGE",
//                "android.permission.WRITE_EXTERNAL_STORAGE"
//        };
//
//        try {
//            //检测是否有写的权限
//            int permission = ActivityCompat.checkSelfPermission(this,
//                    "android.permission.WRITE_EXTERNAL_STORAGE");
//            if (permission != PackageManager.PERMISSION_GRANTED) {
//                // 没有写的权限，去申请写的权限，会弹出对话框
//                ActivityCompat.requestPermissions(this, PERMISSIONS_STORAGE,REQUEST_EXTERNAL_STORAGE);
//            }
//
//            if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.CAMERA)!= PackageManager.PERMISSION_GRANTED){
//                ActivityCompat.requestPermissions((Activity) this,new String[]{android.Manifest.permission.CAMERA},1);
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//
//    @Override
//    public void AudioRecordData(byte[] data, int len) {
//        Camera camera = getCamera(blueCamResponses.get(0));
//        if(camera == null)
//            return;
//        camera.SendTalkData(data,len);
//    }
//
//
//
//    /**
//     * 处理上下左右控制
//     */
//    private class ControlCameraTask extends AsyncTask<Void, Void, Integer>
//    {
//        private int type;
//        public ControlCameraTask(int type) {
//            this.type = type;
//        }
//        @Override
//        protected void onPreExecute()
//        {
//            super.onPreExecute();
//        }
//        @Override
//        protected Integer doInBackground(Void... arg0)
//        {
//
//            if(type == CameraContants.Control.CMD_PTZ_RIGHT)
//            {
//                ControlCameraPTZ(CameraContants.Control.CMD_PTZ_RIGHT);
//                try {
//                    Thread.sleep(1000);
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
//                ControlCameraPTZ(CameraContants.Control.CMD_PTZ_RIGHT_STOP);
//            }
//            else if(type ==CameraContants.Control.CMD_PTZ_LEFT)
//            {
//                ControlCameraPTZ(CameraContants.Control.CMD_PTZ_LEFT);
//                try {
//                    Thread.sleep(1000);
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
//                ControlCameraPTZ(CameraContants.Control.CMD_PTZ_LEFT_STOP);
//            }
//            else if(type ==CameraContants.Control.CMD_PTZ_UP)
//            {
//                ControlCameraPTZ(CameraContants.Control.CMD_PTZ_UP);
//                try {
//                    Thread.sleep(1000);
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
//                ControlCameraPTZ(CameraContants.Control.CMD_PTZ_UP_STOP);
//            }
//            else if(type ==CameraContants.Control.CMD_PTZ_DOWN)
//            {
//                ControlCameraPTZ(CameraContants.Control.CMD_PTZ_DOWN);
//                try {
//                    Thread.sleep(1000);
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
//                ControlCameraPTZ(CameraContants.Control.CMD_PTZ_DOWN_STOP);
//            }
//            return 0;
//        }
//
//        @Override
//        protected void onPostExecute(Integer result) {
//            super.onPostExecute(result);
//        }
//    }
//
//    public void ControlCameraPTZ(int cmd){
//        Camera camera = getCamera(blueCamResponses.get(0));
//        if(camera != null){
//            camera.ControlCameraPTZ(cmd);
//        }
//    }
//
//   String DID = "HOW-246157-FUZBB";
//    // String DID = "HOW-215556-CYXUJ";
//
//    //  String DID = "HOW-215556-CYXUJ";
//    // String DID =  "HOW-277519-UHXWD";
//    String pwd  = "admin123";
//    public Camera getCamera(BlueCamResponse blueCamResponse){
//        Log.d("getcamid",""+blueCamResponse.getCamId());
//        return (Camera) CameraManager.getDeviceManager(CameraViewActivity.this).getCamera(blueCamResponse.getCamId());
////        return (Camera) CameraManager.getDeviceManager(CameraViewActivity.this).getCamera("HOW-249522-MHBVP");
//    }
//    public void addCamera(BlueCamResponse blueCamResponse){
//
//         camid = blueCamResponse.getCamId();
//       addCamera(blueCamResponse.getCamName(),blueCamResponse.getCamId(),blueCamResponse.getUserName(),blueCamResponse.getPwd(),CameraViewActivity.this);
//       // addCamera("test","HOW-215556-CYXUJ","admin","admin123",CameraViewActivity.this);
//        //addCamera("IPCAM","HOW-127411-MYGBX","admin","admin",CameraViewActivity.this);
//        audioRecorder = new CustomAudioRecorder(this);
//    }
//
//    public void capture(){//抓拍
//        Camera camera = getCamera(blueCamResponses.get(0));
//
//        if(FileUtil.isSDCardMounted())
//        {
//            render.setTakeImage(true);
//        }
//        else
//        {
//            //showToast(getResources().getString(R.string.camera_play_no_sd));
//            return;
//        }
//
//        try
//        {
//            final MediaPlayer mediaPlayer = MediaPlayer.create(this, R.raw.photoshutter);
//            mediaPlayer.start();
//            mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
//                @Override
//                public void onCompletion(MediaPlayer mp) {
//                    mediaPlayer.stop();
//                    mediaPlayer.release();
//                }
//            });
//        } catch (IllegalArgumentException e) {
//            e.printStackTrace();
//        } catch (IllegalStateException e) {
//            e.printStackTrace();
//        }
//    }
//
//    public String record(){//录像
//        final Camera camera = getCamera(blueCamResponses.get(0));
//        if(camera == null)
//            return "";
//        if(camera.getStatus() != CameraContants.DeviceStatus.DEVICE_STATUS_ON_LINE)
//            return "";
//        SimpleDateFormat f = new SimpleDateFormat("yyyyMMdd_HHmmss");
//        String strDate = f.format(new Date());
//        String PACKAGE = "com.demo";
//        String VIDEO_PATH = Environment.getExternalStorageDirectory().getAbsolutePath() + "/"+PACKAGE+"/video/";
//        String fileName = VIDEO_PATH + strDate+"_"+camera.getCameraBean().getDevID()+"_" + ".mp4";
//        System.out.println("----------------fileName == "+fileName);
//        camera.startRecordVideo(fileName,"", 0, 0);
//        return fileName;
//    }
//
//    boolean isPlayAudio = false;
//    CustomAudioRecorder audioRecorder;
//    public void talk(){//对讲
//
//        //final Camera camera = getCamera();
//
//        bt_talk_id.setOnTouchListener(new View.OnTouchListener() {
//            @Override
//            public boolean onTouch(View v, MotionEvent event) {
//                if (event.getAction() == MotionEvent.ACTION_DOWN)
//                {
//
//                    final Camera camera = getCamera(blueCamResponses.get(0));
//                    //开始说话
//                    isPlayAudio = false;
//
//                    if(camera != null){
//                        camera.CloseSound();
//                        camera.getCameraDecoder().unInitAudioPlayer();
//                    }
//
//                    camera.OpenTalk();
//                    if(audioRecorder != null){
//                        LogUtil.printLog("openTalk 3+++++++++++");
//                        audioRecorder.startRecord();
//                    }else{
//                        audioRecorder = new CustomAudioRecorder(CameraViewActivity.this);
//                        audioRecorder.startRecord();
//                    }
//                }
//                else if (event.getAction() == MotionEvent.ACTION_UP)
//                {
//                    final Camera camera = getCamera(blueCamResponses.get(0));
//                    //结束说话
//                    isPlayAudio = true;
//
//                    if(camera != null) {
//                        camera.CloseTalk();
//                        if(audioRecorder != null){
//                            audioRecorder.stopRecord();
//                            audioRecorder.release();
//                            audioRecorder = null;
//                        }
//                    }
//
//                    //需要异步处理
//                    if(camera != null) {
//                        if(camera.getStatus() == CameraContants.DeviceStatus.DEVICE_STATUS_ON_LINE){
//                            camera.getCameraDecoder().initAudioPlayer();
//                            camera.OpenSound();
//                        }
//                    }
//                }
//                return true;
//            }
//        });
//    }
//    public void voice(){//监听
//
//        final Camera camera = getCamera(blueCamResponses.get(0));
//
//        if(!isPlayAudio) {
//            isPlayAudio = true;
//
//            //需要异步处理
//            if(camera != null) {
//                if(camera.getStatus() == CameraContants.DeviceStatus.DEVICE_STATUS_ON_LINE){
//                    camera.getCameraDecoder().initAudioPlayer();
//                    camera.OpenSound();
//                }
//            }
//        }
//        else {
//            isPlayAudio = false;
//
//            if(camera != null){
//                camera.CloseSound();
//                camera.getCameraDecoder().unInitAudioPlayer();
//            }
//        }
//    }
//
//    public void resolution(){//分辨率
//
//        final int HIGH = 0; //高清
//        final int MID = 1;  //标清
//        final int LOW = 2;  //普通
//
//        final Camera camera = getCamera(blueCamResponses.get(0));
//        if(camera != null){
//            camera.closeStream();
//        }
//
//        camera.playStream(HIGH);
//    }
//    public void InfraRed(){//红外
//
//        final Camera camera = getCamera(blueCamResponses.get(0));
//        if (camera == null){
//            return;
//        }
//
//        if( camera.getCameraBean().getDeviceType() == CameraContants.CameraType.CAMERA_QJ_Double_Light){
//            int close = 0;//关闭
//            int yans = 3;//夜视彩色
//            int blackwhite = 1;//夜视黑白
//
//            camera.SetCameraParam(14,close);
//        }else{
//            int close = 0;
//            int auto = 1;
//            int open = 2;
//
//            camera.SetCameraParam(14,auto);
//        }
//    }
//
//    boolean m_bUpDownMirror = false;
//    private void setUpDownFlip(){
//        if (m_bUpDownMirror)
//        {
//            //up_down_btn.setBackgroundResource(R.drawable.play_bottom_bar_bg);
//        }
//        else
//        {
//            //up_down_btn.setBackgroundResource(R.drawable.play_bottom_bar_pressed);
//        }
//        setUpDownFlip(m_bUpDownMirror,m_bLeftRightMirror);
//        m_bUpDownMirror = !m_bUpDownMirror;
//    }
//
//    boolean m_bLeftRightMirror = false;
//    private void setLefgRightFlip(){
//        if (m_bLeftRightMirror)
//        {
//            //left_rigth_btn.setBackgroundResource(R.drawable.play_bottom_bar_bg);
//        }
//        else
//        {
//            //left_rigth_btn.setBackgroundResource(R.drawable.play_bottom_bar_pressed);
//        }
//        setLefgRightFlip(m_bUpDownMirror,m_bLeftRightMirror);
//        m_bLeftRightMirror = !m_bLeftRightMirror;
//    }
//
//    public void setLefgRightFlip(boolean m_bUpDownMirror,boolean m_bLeftRightMirror)
//    {
//
//        final Camera camera = getCamera(blueCamResponses.get(0));
//        if (camera == null){
//            return;
//        }
//        int value;
//        if (m_bLeftRightMirror)
//        {
//            if (m_bUpDownMirror)
//                value = 1;
//            else
//                value = 0;
//        }
//        else
//        {
//            if (m_bUpDownMirror)
//                value = 3;
//            else
//                value = 2;
//        }
//        camera.SetCameraParam(5,value);
//    }
//
//    public void setUpDownFlip(boolean m_bUpDownMirror,boolean m_bLeftRightMirror)
//    {
//        final Camera camera = getCamera(blueCamResponses.get(0));
//        if (camera == null){
//            return;
//        }
//        int value;
//        if (m_bUpDownMirror)
//        {
//            if (m_bLeftRightMirror)
//                value = 2;
//            else
//                value = 0;
//        }
//        else
//        {
//            if (m_bLeftRightMirror)
//                value = 3;
//            else
//                value = 1;
//        }
//        camera.SetCameraParam(5,value);
//
//    }
//
//    public void setName(String name){
//        final BCamera camera = getCamera(blueCamResponses.get(0));
//        if (camera == null){
//            return;
//        }
//
//        camera.getCameraBean().setDevName(name);
//        editCamera(camera);
//    }
//
//    public void editCamera(final BCamera camera)
//    {
//        if(camera == null){
//            return;
//        }
//
//        JSONObject obj = new JSONObject();
//        try
//        {
//            obj.put("alias", camera.getCameraBean().getDevName());
//            camera.SetCameraParam(CameraContants.ParamNewKey.BA_CGI_SET_ALIAS,obj.toString());
//            //Thread.sleep(500);
//            //camera.GetCameraParam(CameraContants.ParamNewKey.BA_CGI_SET_SNAPSHOT);
//            //Thread.sleep(500);iv
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//
//
//        int id=item.getItemId();
//
//
//        switch (id){
//
//            case android.R.id.home:
//                onBackPressed();
//                break;
//
//
//        }
//
//
//
//        return super.onOptionsItemSelected(item);
//    }
//
//    @Override
//    public void onBackPressed() {
//        super.onBackPressed();
//       finishAffinity();
////        try {
////            Intent myIntent = new Intent(this,Class.forName("com.vamosys.vamos.MapMenuActivity"));
////            startActivity(myIntent );
////        } catch (ClassNotFoundException e) {
////            e.printStackTrace();
////            Log.d("bachpressvaluefa",""+e);
////        }
//
//
//    }
//}
